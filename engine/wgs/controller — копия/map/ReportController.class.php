<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
//lmb_require('Spreadsheet/Excel/Writer.php');

require_once('Spreadsheet/Excel/Writer.php');

class Tree
{
    private $data;

    public function __construct($data = array())
    {
        $this->data = $data;
    }

    public function getNodes()
    {
        return array_values($this->data);
    }

    public function hasNode($node_id)
    {
        return isset($this->data[$node_id]);
    }

    public function getNode($node_id)
    {
        return $this->data[$node_id];
    }

    public function getRandomNode()
    {
        $result = null;
        if (!empty($this->data)) {
            $ids = array_keys($this->data);
            $result = $this->getNode($ids[rand(0, sizeof($ids) - 1)]);
        }
        return $result;
    }

    public function add($node, $parent_id = null)
    {
        $node['_id'] = $this->generateId();

        if ($parent_id) {
            $parent = &$this->data[$parent_id];
            $this->shiftNodesRight($parent['_rgt']);
            $node['_parent'] = $parent_id;
            $node['_level'] = $parent['_level'] + 1;
            $node['_lft'] = $parent['_rgt'] - 2;
            $node['_rgt'] = $node['_lft'] + 1;
            $parent['_is_leaf'] = false;
            $node['_is_leaf'] = true;
        }
        else {
            $node['_parent'] = null;
            $node['_level'] = 1;
            $node['_lft'] = $this->getMaxRightPos() + 1;
            $node['_rgt'] = $node['_lft'] + 1;
            $node['_is_leaf'] = true;
        }

        $this->data[$node['_id']] = $node;
    }

    public function getParent($node_id)
    {
        $parent = null;

        if (
            isset($this->data[$node_id]) &&
            isset($this->data[$this->data[$node_id]['_parent']])
        ) {
            $parent = $this->data[$this->data[$node_id]['_parent']];
        }

        return $parent;
    }

    public function getChildren($node_id = null)
    {
        $children = null;

        if ($node_id)
        {
            $children = array();
            foreach ($this->data as $id => $node) {
                if ($node['_parent'] == $node_id) {
                    $children[] = $node;
                }
            }
        }
        else {
            $children = $this->getRoots();
        }

        return $children;
    }

    public function getRoots()
    {
        $roots = array();

        foreach ($this->data as $id => $node) {
            if ($node['_parent'] === null) {
                $roots[] = $node;
            }
        }

        return $roots;
    }

    public function getChildrenPaged($node_id = null, $start = null, $limit = null)
    {
        $children = $this->getChildren($node_id);
        if (!$start) {
            $start = 0;
        }
        if (!$limit) {
            $limit = sizeof($children);
        }
        return array_slice($children, $start, $limit);
    }

    public function getChildrenCount($node_id = null)
    {
        $children = $this->getChildren($node_id);
        return sizeof($children);
    }

    private function generateId()
    {
        return empty($this->data) ? 1 : max(array_keys($this->data)) + 1;
    }

    private function getMinLeftPos()
    {
        $result = 0;

        if (!empty($this->data)) {
            $values = array();
            foreach ($this->data as $id => $node) {
                $values[] = $node['_lft'];
            }
            $result = min($values);
        }

        return $result;
    }

    private function getMaxRightPos()
    {
        $result = 0;

        if (!empty($this->data)) {
            $values = array();
            foreach ($this->data as $id => $node) {
                $values[] = $node['_rgt'];
            }
            $result = max($values);
        }

        return $result;
    }

    private function shiftNodesRight($start_from, $offset = 2)
    {
        foreach ($this->data as $id => &$node) {
            if ($node['_lft'] >= $start_from) {
                $node['_lft'] += $offset;
            }
            if ($node['_rgt'] >= $start_from) {
                $node['_rgt'] += $offset;
            }
        }
    }
}

class ReportController extends wgsFrontController
{
    function doGetFeatureTypesByFeatureIds()
    {
    	$objectsArray = array();
        if($this->request->getPost('featureIds')) {
            $featureIds = $this->request->getPost('featureIds');
            $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByFeatures($featureIds);

            foreach ($objectTypesList as $objectType) {
                 $objectsArray[] = array(
                    'featureTypeName' => lmb_win1251_to_utf8($objectType->get('type_name')),
                    'featureTypeId' => $objectType->get('object_type_id')
                 );
            }
        }
        $this->response->write(json_encode(array_values($objectsArray)));
    }

    function getReport($featureIds, $featureTypes, $property, $addProperties) {
        $result = $this->wgsTier->getProjectTier()->getSearchService()->getReportStatSum($featureIds, $featureTypes, $property, $addProperties);
        $resultArray = array();
        foreach ($result as $record) {
             $resultArray[] = array(
                'generalValue' => lmb_win1251_to_utf8($record->get('general_value')),
                'addPropertyName' => lmb_win1251_to_utf8($record->get('add_property_name')),
                'addValue' => lmb_win1251_to_utf8(str_replace(',','.',$record->get('add_value'))),
                'count' => str_replace(',','.',$record->get('count')),
                'sum' => str_replace(',','.',$record->get('sum'))
             );
        }

        $general_values = array();
        $key_index = array();
        $index = 0;

        foreach ($resultArray as $resultRow) {
            $md5_1 = md5($resultRow['generalValue'].'_');
            $key_index[$md5_1] = $key_index[$md5_1]? $key_index[$md5_1]: ++$index;

            $md5_2 = md5($resultRow['generalValue'].'_'.$resultRow['addPropertyName'].'_');
            $key_index[$md5_2] = $key_index[$md5_2]? $key_index[$md5_2]: ++$index;

            $md5_3 = md5($resultRow['generalValue'].'_'.$resultRow['addPropertyName'].'_'.$resultRow['addValue'].'_');
            $key_index[$md5_3] = $key_index[$md5_3]? $key_index[$md5_3]: ++$index;

            $general_values[$key_index[$md5_1]] = array(
                'value' => $resultRow['generalValue'],
                'count' => $general_values[$key_index[$md5_2]]['count']+$resultRow['count'],
                'sum' => '',
                '_id' => $key_index[$md5_1],
                '_level' => 1,
                '_parent' => null
            );

            $general_values[$key_index[$md5_2]] = array(
                'value' => $resultRow['addPropertyName'],
                'count' => $general_values[$key_index[$md5_2]]['count']+$resultRow['count'],
                'sum' => $general_values[$key_index[$md5_2]]['sum']+$resultRow['sum'],
                '_id' => $key_index[$md5_2],
                '_level' => 2,
                '_parent' => $key_index[$md5_1]
            );

            $general_values[$key_index[$md5_3]] = array(
                'value' => $resultRow['addValue'],
                'count' => $resultRow['count'],
                'sum' => $resultRow['sum'],
                '_id' => $key_index[$md5_3],
                '_level' => 3,
                '_parent' => $key_index[$md5_2]
            );
        }

        return $general_values;
    }

    function doGetResult()
    {
        $featureIds = $this->request->getPost('featureIds');
        $featureTypes = $this->request->getPost('featureTypes');
        $property = $this->request->getPost('property');
        $addProperties = $this->request->getPost('addProperties');
        $general_values = array();

        if ($featureIds && $featureTypes && $property) {
            $general_values = $this->getReport($featureIds, $featureTypes, $property, $addProperties);

            $tree = new Tree();

            foreach($general_values as $general_value) {
                $tree->add($general_value, $general_value['_parent']);
            }

            $this->response->write(json_encode($tree->getNodes()));
        } else {
            $this->response->write(json_encode(array()));
        }
    }

    function doGetExcelResult() {
        $featureIds = $this->request->getPost('featureIds');
        $featureTypes = $this->request->getPost('featureTypes');
        $featureTypeName = lmb_utf8_to_win1251($this->request->getPost('featureTypeName'));
        $property = $this->request->getPost('property');
        $addProperties = $this->request->getPost('addProperties');
        $propertyName = lmb_utf8_to_win1251($this->request->getPost('propertyName'));

        if ($featureIds && $featureTypes && $property) {
            $general_values = $this->getReport($featureIds, $featureTypes, $property, $addProperties);
            $level = 3;
            while ($level > 0) {
                for ($i = 0; $i <= count($general_values); $i++) {
                    if ($general_values[$i]['_level'] == $level && $general_values[$i]['_parent'] != '') {
                        $general_values[$general_values[$i]['_parent']]['_children'][] = $general_values[$i];
                    }
                }
                $level--;
            }
            /////////////////////////////////////////////////
            $seq = rand();
            $fileName = "report-$seq.xls";
            $xls =& new Spreadsheet_Excel_Writer(WGS3_PRINTCACHE_DIR.$fileName);

            // �������� �����
            $cart =& $xls->addWorksheet('report');

            $titleText = $propertyName;
            // �������� ������� ��������������
            $titleFormat =& $xls->addFormat();
            // ����������� ������ - Helvetica �������� � OpenOffice calc ����...
            $titleFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
            $titleFormat->setBold();
            // ����������� ������� ������
            $titleFormat->setSize('13');
            // ����������� ����� ������
            $titleFormat->setColor('navy');
            // ����������� ������ ������� ��������� � "thick"
            $titleFormat->setBottom(2);
            // ����������� ����� ������� ���������
            $titleFormat->setBottomColor('navy');
            // ����������� ������������ � ����������� ��������
            $titleFormat->setAlign('merge');
            $titleFormat->setVAlign('vcenter');
            // ���������� ��������� � ���x��� ����� ������ ����� ,
            // ��������� ��� ������ ��������� � ����� ������ ��������������

            $cart->write(0,0,$featureTypeName,$titleFormat);
            $cart->write(0,1,'',$titleFormat);
            $cart->write(0,2,'',$titleFormat);
            $cart->write(0,3,'',$titleFormat);

            // ������ ������
            $cart->setRow(0,30);
            // ����������� ������ ������� ��� ������ 4 �������
            $cart->setColumn(0,3,20);

            $cart->write(1,0,$titleText,$titleFormat);
            $cart->write(1,1,'',$titleFormat);
            $cart->write(1,2,'����������',$titleFormat);
            $cart->write(1,3,'�����',$titleFormat);
            // ������ ������
            $cart->setRow(1,30);
            // ����������� ������ ������� ��� ������ 4 �������
            $cart->setColumn(0,3,20);

            // ����������� ���������� ��������������
            $colHeadingFormat =& $xls->addFormat();
            $colHeadingFormat->setBold();
            $colHeadingFormat->setFontFamily('Helvetica');
            $colHeadingFormat->setBold();
            $colHeadingFormat->setSize('10');
            $colHeadingFormat->setAlign('center');

            // ������ � ������� ��������� ��� ��������
            //$colNames = array('Item','Price($)','Quantity','Total');

            // ���������� ���� ��������� ������ �������
            // ��������� ������ ������ ��� ����� ��������� ����
            //$cart->writeRow(2,0,$colNames,$colHeadingFormat);

            // ������ ����� ��� �������������
            // 1-�� �������� - ������� ������������� �����������
            // 2-�� �������� - ������� ��������������� �����������
            // (0 = ��� ��������������� �����������)
            // 3-�� �������� - ������� ������� ������ ����� ������������� �����������
            // 4-�� �������� - ����� ������� ������� ����� ��������������� �����������
            $freeze = array(2,0,2,0);

            // ���������� ��� ������!
            $cart->freezePanes($freeze);

            // ������ ������

            // ����������� ��� ��� ������������ �������� ������ ������
            $currentRow = 2;

            // �������� ����� ������, �������� �� � ����
            //foreach ( $items as $item ) {
               // Write each item to the sheet
            //$cart->writeRow($currentRow,0,$item);
            //$currentRow++;
            //}

            $level1Format = & $xls->addFormat();
            $level1Format->setAlign('merge');
            $level1Format->setFontFamily('Helvetica');
            // ����������� ������� ������
            $level1Format->setBold();
            // ����������� ������� ������
            $level1Format->setSize('13');
            // ����������� ����� ������
            $level1Format->setColor('navy');
            //$level1Format->setFgColor('silver');
            $level1Format->setVAlign('vcenter');
            $level1Format->setTop(1);
            $level1Format->setBottom(1);

            $level2ValueFormat = & $xls->addFormat();
            $level2ValueFormat->setAlign('merge');
            $level2ValueFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
            $level2ValueFormat->setBold();
            // ����������� ������� ������
            $level2ValueFormat->setSize('11');
            $level2ValueFormat->setVAlign('vcenter');
            // ����������� ����� ������
            $level2ValueFormat->setTop(1);
            $level2ValueFormat->setBottom(1);
            //$level2Format->setColor('black');

            $level2Format = & $xls->addFormat();
            $level2Format->setAlign('merge');
            $level2Format->setFontFamily('Helvetica');
            // ����������� ������� ������
            $level2Format->setBold();
            // ����������� ������� ������
            $level2Format->setSize('10');
            // ����������� ����� ������
            $level2Format->setColor('green');
            $level2Format->setVAlign('vcenter');
            $level2Format->setTop(1);
            $level2Format->setBottom(1);

            $level3ValueFormat = & $xls->addFormat();
            $level3ValueFormat->setAlign('center');
            $level3ValueFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
            //$level3ValueFormat->setBold();
            // ����������� ������� ������
            $level3ValueFormat->setSize('11');
            // ����������� ����� ������
            $level3ValueFormat->setColor('red');

            $level3Format = & $xls->addFormat();
            $level3Format->setAlign('center');
            $level3Format->setFontFamily('Helvetica');
            // ����������� ������� ������

            // ����������� ������� ������
            $level3Format->setSize('10');
            // ����������� ����� ������
            //$level3Format->setColor('green');

            for ($i = 0; $i < count($general_values); $i++) {
                if ($general_values[$i]['_level'] == 1) {
                    $generalValue = ($general_values[$i]['value'])?(lmb_utf8_to_win1251($general_values[$i]['value'])): '-';
                    $cart->setRow($currentRow, 20);
                    $cart->write($currentRow, 0, $generalValue, $level1Format);
                    $cart->write($currentRow, 1, '', $level1Format);
                    $cart->write($currentRow, 2, $general_values[$i]['count'], $level1Format);
                    $cart->write($currentRow, 3, ' ', $level1Format);
                    $currentRow++;

                    if (count($general_values[$i]['_children'])) {
                        foreach ($general_values[$i]['_children'] as $childLevel2) {
                            $childLevel2value = ($childLevel2['value'])? (lmb_utf8_to_win1251($childLevel2['value'])): '-';
                            $cart->setRow($currentRow, 20);
                            $cart->write($currentRow, 0, $childLevel2value, $level2ValueFormat);
                            $cart->write($currentRow, 1, '', $level2ValueFormat);
                            $cart->write($currentRow, 2, $childLevel2['count'], $level2Format);
                            $cart->write($currentRow, 3, $childLevel2['sum'], $level2Format);
                            $currentRow++;

                            if (count($childLevel2['_children'])) {
                                foreach($childLevel2['_children'] as $childLevel3) {
                                    $childLevel3value = ($childLevel3['value'])? (lmb_utf8_to_win1251($childLevel3['value'])): '-';
                                    $cart->write($currentRow, 1, $childLevel3value, $level3ValueFormat);
                                    $cart->write($currentRow, 2, $childLevel3['count'], $level3Format);
                                    $cart->write($currentRow, 3, $childLevel3['sum'], $level3Format);
                                    $currentRow++;
                                }
                            }
                        }
                    }
                }
            }

            $xls->send($fileName);
            $xls->close();

            echo $seq;
        }
    }

    function doGetExcelFile() {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."report-$seq.xls");
        $len = strlen($buf);
        $this->response->header("Content-type: application/vnd.ms-excel");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: attachment; filename=report-$seq.xls");
        $this->response->header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        $this->response->header("Pragma: public");
        $this->response->write($buf);
        @unlink(WGS3_PRINTCACHE_DIR."report-$seq.xls");
    }

	function doGetExportFile() {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."iad.xls");
        $len = strlen($buf);
        $this->response->header("Content-type: application/vnd.ms-excel");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: attachment; filename=iad.xls");
        $this->response->header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        $this->response->header("Pragma: public");
        $this->response->write($buf);
        @unlink(WGS3_PRINTCACHE_DIR."iad.xls");
    }
	
    function doGetExcel() {
    	$id = $this->request->getRequest('id');
    	$this->response->write("<html><head>" .
                    		'<script language="javascript">' .
                    		'window.location.href = "/map/report/GetExcelFile/'.$id.'";'.
                    		'</script>'.
                    		"</head><body></body></html>");
    }

	public function doGetResultPPR()
	{
		
		$featureIds = $this->request->getPost('featureIds');
		$date = getdate(time());
		$year = $this->request->getPost('year');
		$year = isset($year)&&$year ? $year : $date['year'];
		//$featureIds = array(388787, 388786, 388809, 388853, 388852, 388812, 388851, 388811, 388810, 388850, 388849, 388788);
		//$featureIds = array(388853, 388852, 388812, 388851, 388811, 388850);
		$objects = array();
		
		if ($featureIds) {
			$objects = $this->getResultPPR($featureIds, $year);
		}
		$this->response->write(json_encode(array_values($objects)));
	}

	public function doGetExcelResultPPR()
	{
		$date = getdate(time());
		$year = $this->request->getPost('year');
		$year = isset($year)&&$year ? $year : $date['year'];
		$featureIds = $this->request->getPost('featureIds');
		//$featureIds = array(388787, 388786, 388809, 388853, 388852, 388812, 388851, 388811, 388810, 388850, 388849, 388788);
		//$featureIds = array(388853, 388852, 388812, 388851, 388811, 388850);
		$objects = array();

		if ($featureIds) {
			$objects = $this->getResultPPR($featureIds, $year);
		}

		$seq = rand();
		$fileName = "report-$seq.xls";
		$xls =& new Spreadsheet_Excel_Writer(WGS3_PRINTCACHE_DIR.$fileName);

		$cart =& $xls->addWorksheet('phpPetstore');

		$titleText = "����� ��� �� {$year} ���";

		$titleFormat =& $xls->addFormat();
		$titleFormat->setFontFamily('Helvetica');
		$titleFormat->setBold();
		$titleFormat->setSize('13');
		$titleFormat->setColor('navy');
		$titleFormat->setBottom(2);
		$titleFormat->setBottomColor('navy');
		$titleFormat->setAlign('merge');
		$titleFormat->setVAlign('vcenter');

		$cart->write(0,0,$titleText,$titleFormat);
		for ($i=1; $i<13; $i++) {
			$cart->write(0,$i,'',$titleFormat);
		}

		$cart->setRow(0,30);
		$cart->setColumn(0,0,30);
		$cart->setColumn(1,12,10);

		$colHeadingFormat =& $xls->addFormat();
		$colHeadingFormat->setBold();
		$colHeadingFormat->setFontFamily('Helvetica');
		$colHeadingFormat->setBold();
		$colHeadingFormat->setSize('10');
		$colHeadingFormat->setAlign('center');

		// ������ � ������� ��������� ��� ��������
		$colNames = array(
			'������������ �������',
			'������',
			'�������',
			'����',
			'������',
			'���',
			'����',
			'����',
			'������',
			'��������',
			'�������',
			'������',
			'�������'
		);

		$cart->writeRow(2,0,$colNames,$colHeadingFormat);

		$freeze = array(3,0,4,0);
		$cart->freezePanes($freeze);

		// ����������� ��� ��� ������������ �������� ������ ������
		$currentRow = 4;
		$formatObjects = array();
		foreach ($objects as $object) {
			$formatObjects[] = array(
				'objectName' => isset($object['objectName']) ? lmb_utf8_to_win1251($object['objectName']) : '��� ������������',
				'January' 	=> isset($object['January']) ? lmb_utf8_to_win1251($object['January']) : ' ',
		        'February'	=> isset($object['February']) ? lmb_utf8_to_win1251($object['February']) : ' ',
		        'March'		=> isset($object['March']) ? lmb_utf8_to_win1251($object['March']) : ' ',
		        'April'		=> isset($object['April']) ? lmb_utf8_to_win1251($object['April']) : ' ',
		        'May'		=> isset($object['May']) ? lmb_utf8_to_win1251($object['May']) : ' ',
		        'June'		=> isset($object['June']) ? lmb_utf8_to_win1251($object['June']) : ' ',
		        'July'		=> isset($object['July']) ? lmb_utf8_to_win1251($object['July']) : ' ',
		        'August'	=> isset($object['August']) ? lmb_utf8_to_win1251($object['August']) : ' ',
		        'September'	=> isset($object['September']) ? lmb_utf8_to_win1251($object['September']) : ' ',
		        'October'	=> isset($object['October']) ? lmb_utf8_to_win1251($object['October']) : ' ',
		        'November'	=> isset($object['November']) ? lmb_utf8_to_win1251($object['November']) : ' ',
		        'December'	=> isset($object['December']) ? lmb_utf8_to_win1251($object['December']) : ' '
			);
		}

		$rowFormat =& $xls->addFormat();
		$rowFormat->setFontFamily('Helvetica');
		$rowFormat->setSize('10');
		$rowFormat->setAlign('center');

		// �������� ����� ������, �������� �� � ����
		for ($i = 0; $i < count($formatObjects)-1; $i++) {
			$object = $formatObjects[$i];
			$cart->writeRow($currentRow, 0, $object, $rowFormat);
			$currentRow++;
		}

		$rowFormatBold =& $xls->addFormat();
		$rowFormatBold->setFontFamily('Helvetica');
		$rowFormatBold->setSize('10');
		$rowFormatBold->setBold();
		$rowFormatBold->setAlign('center');
		$cart->writeRow($currentRow, 0, $formatObjects[count($formatObjects)-1], $rowFormatBold);

		$xls->send($fileName);
		$xls->close();

		echo $seq;
	}

	private function getResultPPR($featureIds, $year) {

		$pprObjectType = PPR_OBJECT_TYPE;
		$pprProperties = array(
				PPR_INSTALLATION_DATE,
				PPR_REVISION,
				PPR_CURRENT_REPAIR,
				PPR_CAPITAL_REPAIR,
				PPR_LAST_CAPITAL_REPAIR_DATE,
				PPR_REVISION_COST,
				PPR_CURRENT_REPAIR_COST,
				PPR_CAPITAL_REPAIR_COST,
				PPR_INSTALL_COST,
				PPR_ER,
				PPR_TO,
				PPR_TT,
				PPR_TK);

		$pprObjectTypeId = 0;
		$objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
		$objects = array();
		$result = array();
		$objectsArray = array();
		$objectIds = array();



		foreach ($objectList as $object) {
            $objectTypeName = $object->get('object_type_name');
            if ($objectTypeName == $pprObjectType) {
            	$pprObjectTypeId = $object->get('object_type_id');
            	$objectId = $object->get('object_id');
            	$objectsArray[] = array (
                    'featureId'=>$object->get('feature_id'),
            		'objectTypeName'  => lmb_win1251_to_utf8($object->get('object_type_name')),
                    'objectId'  => $objectId
                );
                $objectIds[] = $objectId;
            }
        }

		$objectList1 = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
		$objectsNames = array();
            foreach ($objectList1 as $object) {
            $objectName = '';
            if ($object->get('is_current')) {
            	if($object->get('object_name') != '')
	            	$objectName = lmb_win1251_to_utf8($object->get('object_name'));
	            else
	            	$objectName = lmb_win1251_to_utf8('��� ������������');

	            $objectsNames[]	= array (
            		'featureId'  => $object->get('feature_id'),
            		'objectName' => $objectName,
	            	'parentObjectId' => $object->get('object_id'),
            	);
            }
            }
		//�������� ��������, id � ����
		foreach ($objectsArray as $val) {
            foreach ($objectsNames as $val1) {
            	if ($val['featureId'] == $val1['featureId']) {
            		$objects[] = array_merge($val, $val1);
            	}
            }
            }
		// �������� �������������� ���

            if ($pprObjectTypeId) {
            $props = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($pprObjectTypeId, null);
            $propsList = array();

            foreach($props as $property) {
                $propsList[] = array(
                    'propertyId'            => $property->get('Property_Id'),
                        'propertyName'          => /*lmb_win1251_to_utf8(*/$property->get('Name')/*)*/
                        //'valueTypeId'   => $property->get('Value_properties_type_id'),
                        //'groupId'       => $property->get('Properties_group_id'),
                        //'groupName'     => lmb_win1251_to_utf8($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($property->get('Properties_group_id')))
                );
            }
            //�������� ������ ��, ������� ���������� ��� ���������� ���
			//print_r($propsList);

            if(count($objectIds) > 0) {
            	foreach ($objects as &$object) {

            		$propValueList = array();
            		$propsValue = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValuePropertiesByObject($object['objectId']);
		            foreach ($propsValue as $property) {
		                $propValueList[] = array (
		                    'propertyId' => $property->get('property_id'),
		                    'propertyValue' => /*lmb_win1251_to_utf8(*/$property->get('property_value')/*)*/
		                );
		            }

		            //echo '\n��������: ', $object["objectId"] ,'\n';
					$params = array();
		            foreach ($propsList as $prop) {
		            	if (in_array($prop['propertyName'], $pprProperties)) {
				            foreach ($propValueList as $propValue) {
				            	if ($prop['propertyId'] == $propValue['propertyId'])
				            		$params[] = array_merge($prop, $propValue);
				            }
			            }
		            }

		            foreach ($params as $param) {
		            	$value = $param['propertyValue'];
		            	//echo $object['objectId'], ' : ', $param['propertyName'], ' = ', $param['propertyValue'], '<br>';
		            	switch ($param['propertyName']) {
		            		case PPR_INSTALLATION_DATE :
		            			$pprInstallDate = $value;
		            			break;
							case PPR_LAST_CAPITAL_REPAIR_DATE :
		            			$pprLastCapitalRepairDate = $value;
		            			break;
		            		case PPR_REVISION :
		            			$pprRevision = $value;
		            			break;
		            		case PPR_CURRENT_REPAIR :
		            			$pprCurrentRepair = $value;
		            			break;
		            		case PPR_CAPITAL_REPAIR :
		            			$pprCapitalRepair = $value;
		            			break;
		            		case PPR_REVISION_COST :
		            			$pprRevisionCost = floatval(str_replace(',','.',$value));
		            			break;
		            		case PPR_CURRENT_REPAIR_COST :
		            			$pprCurrentRepairCost = floatval(str_replace(',','.',$value));
		            			break;
		            		case PPR_CAPITAL_REPAIR_COST :
		            			$pprCapitalRepairCost = floatval(str_replace(',','.',$value));
		            			break;
		            		case PPR_INSTALL_COST :
		            			$pprInstallCost = floatval(str_replace(',','.',$value));
		            			break;
		            		case  PPR_ER:
		            			$pprER = $value;
		            			break;
		            		case  PPR_TO:
		            			$pprTO = floatval(str_replace(',','.',$value));
		            			break;
		            		case  PPR_TT:
		            			$pprTT = floatval(str_replace(',','.',$value));
		            			break;
		            		case  PPR_TK:
		            			$pprTK = floatval(str_replace(',','.',$value));
		            			break;
		            	}
		            }
					//echo $pprLastCapitalRepairDate;

		            $startDate = getdate(strtotime("01.01.{$year}"));
		            $endDate = getdate(strtotime("31.12.{$year}"));
					//echo $pprInstallDate; exit;

		            $pprLastCapitalRepairDate = ($pprLastCapitalRepairDate) ? $pprLastCapitalRepairDate : NULL;
					$startMark = '�';
		            if($pprLastCapitalRepairDate != NULL) {
		            	$pprInstallDate = $pprLastCapitalRepairDate;
		            	$startMark = '���';
		            } else {
		            	$pprInstallDate = ($pprInstallDate) ? $pprInstallDate : NULL;
		            }
					//echo $pprInstallDate;
		            $cost = array();
		            $tto = array();
		            $tt = array();
		            $tk = array();
		            $countRevision = array();
            		$countCurrentRepair = array();
            		$countCapitalRepair = array();

		            if ($pprInstallDate != NULL) {
			            $pprInstallDate = getdate(strtotime($pprInstallDate));

		            		if ($pprRevision) {
			            		$t = $pprER*$pprTO;
		            			$this->setRevizion(&$object, $pprRevision, $pprInstallDate, $endDate, lmb_win1251_to_utf8('�'));
			            		$this->setRevizion(&$cost, $pprRevision, $pprInstallDate, $endDate, $pprRevisionCost);
								//echo $pprER*$pprTO, ' ';
			            		$this->setRevizion(&$tto, $pprRevision, $pprInstallDate, $endDate, $pprER*$pprTO);

			            		$this->setRevizion(&$countRevision, $pprRevision, $pprInstallDate, $endDate, 1);
							}
			            	if ($pprCurrentRepair) {
								$this->setRevizion(&$object, $pprCurrentRepair, $pprInstallDate, $endDate, lmb_win1251_to_utf8('�'));
								$this->setRevizion(&$cost, $pprCurrentRepair, $pprInstallDate, $endDate, $pprCurrentRepairCost);
								$this->setRevizion(&$countRevision, $pprCurrentRepair, $pprInstallDate, $endDate, 0);
								$this->setRevizion(&$tto, $pprCurrentRepair, $pprInstallDate, $endDate, 0);
								$this->setRevizion(&$tt, $pprCurrentRepair, $pprInstallDate, $endDate, $pprER*$pprTT);
								$this->setRevizion(&$countCurrentRepair, $pprCurrentRepair, $pprInstallDate, $endDate, 1);
			            	}
			            	if ($pprCapitalRepair) {
								$this->setRevizion(&$object, $pprCapitalRepair, $pprInstallDate, $endDate, lmb_win1251_to_utf8('�'));
								$this->setRevizion(&$cost, $pprCapitalRepair, $pprInstallDate, $endDate, $pprCapitalRepairCost);
								$this->setRevizion(&$countRevision, $pprCapitalRepair, $pprInstallDate, $endDate, 0);
								$this->setRevizion(&$tk, $pprCapitalRepair, $pprInstallDate, $endDate, $pprER*$pprTK);
								$this->setRevizion(&$countCurrentRepair, $pprCapitalRepair, $pprInstallDate, $endDate, 0);
								$this->setRevizion(&$countCapitalRepair, $pprCapitalRepair, $pprInstallDate, $endDate, 1);
								$this->setRevizion(&$tto, $pprCapitalRepair, $pprInstallDate, $endDate, 0);
								$this->setRevizion(&$tt, $pprCapitalRepair, $pprInstallDate, $endDate, 0);
			            	}

			            if ($startDate[0] <= $pprInstallDate[0] && $pprInstallDate[0] <= $endDate[0]) {
							$month = $pprInstallDate['month'];
			            	$object[$month] = lmb_win1251_to_utf8($startMark);
							$cost[$month] = ($startMark=='y') ? $pprInstallCost : $pprCapitalRepairCost;
			            }
		            } else {
		            	$object['rowcls'] = 'red-grid-row';
		            }
		            if ($endDate[0] < $pprInstallDate[0]) {
		            	$object['rowcls'] = 'red-grid-row';
		            }

		            $buf = array('January', 'February', 'March', 'April', 'May', 'June',
		            				'July', 'August', 'September', 'October', 'November', 'December');
		            $t = array();
		            foreach($buf as $v) {
		            	$t[$v] = $tto[$v]+$tt[$v] + $tk[$v];
		            }

					$costs[] = $cost;

            		$ttos[] = $tto;
            		$tts[] = $tt;
            		$tks[] = $tk;
            		$ts[] = $t;

            		$countRevisions[] = $countRevision;
            		$countCurrentRepairs[] = $countCurrentRepair;
            		$countCapitalRepairs[] = $countCapitalRepair;
            	}
            }

            $totalTto = $this->getTotalArray('���', $ttos);
			$totalTt = $this->getTotalArray('��', $tts);
            $totalTk = $this->getTotalArray('��', $tks);
            $totalT = $this->getTotalArray('������������, �/���', $ts);

            $totalCost = $this->getTotalArray('���������', $costs);
            $total = 0;
            foreach ($totalCost[0] as $t) {
            	$total	+= isset($t) ? $t : 0;
            }

            $totalArray = array( 0 => array(
				'objectName'	=> lmb_win1251_to_utf8('����� ���������'),
	            'rowcls'		=> "total-cost-ppr",
	            'January'		=> $total,
	        ));

            $totalCountRevisions = $this->getTotalArray('���-�� ��������', $countRevisions);
			$totalCountCurrentRepairs = $this->getTotalArray('���-�� ���. ��������', $countCurrentRepairs);
			$totalCountCapitalRepairs = $this->getTotalArray('���-�� ���. ��������', $countCapitalRepairs);
		}
		return  array_merge($objects, $totalCountRevisions,$totalCountCurrentRepairs, $totalCountCapitalRepairs, $totalT, $totalCost, $totalArray);
	}

	function getTotalArray($name, $values) {

		$totalArray = array( 0 => array(
			'objectName'	=> lmb_win1251_to_utf8($name),
            'rowcls'		=> "total-cost-ppr",
            'January'		=> 0,
            'February'		=> 0,
            'March'			=> 0,
            'April'			=> 0,
            'May'			=> 0,
            'June'			=> 0,
            'July'			=> 0,
            'August'		=> 0,
            'September'		=> 0,
            'October'		=> 0,
            'November'		=> 0,
            'December'		=> 0
        ));

        foreach ($values as $value) {
			$totalArray[0]['January']	+= isset($value['January']) ? $value['January'] : 0;
			$totalArray[0]['February']	+= isset($value['February']) ? $value['February'] : 0;
			$totalArray[0]['March']		+= isset($value['March']) ? $value['March'] : 0;
			$totalArray[0]['April']		+= isset($value['April']) ? $value['April'] : 0;
			$totalArray[0]['May']		+= isset($value['May']) ? $value['May'] : 0;
			$totalArray[0]['June']		+= isset($value['June']) ? $value['June'] : 0;
			$totalArray[0]['July']		+= isset($value['July']) ? $value['July'] : 0;
			$totalArray[0]['August']	+= isset($value['August']) ? $value['August'] : 0;
			$totalArray[0]['September']	+= isset($value['September']) ? $value['September'] : 0;
			$totalArray[0]['October']	+= isset($value['October']) ? $value['October'] : 0;
			$totalArray[0]['November']	+= isset($value['November']) ? $value['November'] : 0;
			$totalArray[0]['December']	+= isset($value['December']) ? $value['December'] : 0;
		}
		return $totalArray;
	}

	function setRevizion(&$object, $duration, $defDate, $endDate, $value) {
		$revizion = $defDate;
		while ($revizion['year'] <= $endDate["year"]) {
			$hours = $revizion['hours'];
			$minutes = $revizion['minutes'];
			$seconds = $revizion['seconds'];
			$month = $revizion['mon'];
			$day = $revizion['mday'];
			$year = $revizion['year'];
			$revizion = getdate(mktime($hours,$minutes,$seconds,$month,$day+$duration,$year));
			if ($revizion['year'] == $endDate['year']) {
				$object[$revizion['month']] = $value;
			}
		}
	}

	function getStamp($date) {
		$hours = $date['hours'];
		$minutes = $date['minutes'];
		$seconds = $date['seconds'];
		$month = $date['mon'];
		$day = $date['mday'];
		$year = $date['year'];
		return mktime($hours,$minutes,$seconds,$month,$day,$year);
	}
}
?>