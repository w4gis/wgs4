<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
require_once('wgs/lib/fpdf/fpdf.class.php');

class PDF extends FPDF
{
//Page header

//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Times','',8);
        //Page number
        $this->Cell(0,10,'�������� '.$this->PageNo().'/{nb}',0,0,'R');
    }
}


class PassportController extends wgsFrontController
{
    protected $pdf;
    protected $rh = 7;
    protected $seq;
    protected $passportService;

    function doDisplay()
    {
    }

    function doPrint()
    {
        $this->seq = rand();
        $this->pdf = new PDF();
        $this->passportService = $this->wgsTier->getProjectObjectService()->getPassportService();
        $this->pdf->Open();
        $this->pdf ->SetMargins(15, 20, 15);
        $this->pdf ->AddFont('Times','','times.php');
        $this->pdf ->AddFont('TimesB','','timesbd.php');
        $this->pdf ->AliasNbPages();

        $featureId = $this->request->getPost('featureId');

        if($featureId) {
            /*
             * ��������� ����
             */
            $titleList = $this->passportService->getTitleList($featureId);
            //print_r($titleList);
            //$i=0;
            if (count($titleList > 0)) {
                foreach ($titleList as $title) {
                    $this->createTitlePage($title);
                    $passportId = $title->get('PASSPORT_ID');
                    break;
                }
            }
            $result = array();
            if ($passportId) {
                /*
                 * ����� ��������
                 */
                $this->createGeneralInfoPage($passportId);
                /*
                 * �������������� ��������������
                 */
                $this->createConstructivePage($passportId);
                /*
                 * ������� ���������
                 */
                $this->createAreaPage($passportId);
                /*
                 * ������� �����
                 */
                $this->createAreaFloorPage($passportId);
                /*
                 * ������� �������� ������������ ����������� �����������
                 */
                $this->createOutsideSurfacesPage($passportId);
                /*
                 * ������� ���������� ������������ ����������� � ������� ����������� ����������� �����
                 */
                $this->createInsideSurfacesPage($passportId);

                /*
                 * ��������� ������ � ����������� ��������� �� �������� ����������� ������ � ����������
                 */
                $this->createProjectDataAndNotePage($passportId);

                /*
                 * ��������� �����
                 */
                $this->pdf->output(WGS3_PRINTCACHE_DIR."passport-$this->seq.pdf");

                $result[0]["seq"] = $this->seq;
                $result[0]["passport"] = 'passport';
                $result[0]["passportId"] = $passportId;
                if ($this->getFloorPlan($passportId)) {
                    $result[0]["floorplan"] = 'floorplan';
                } else {
                    $result[0]["floorplan"] = null;
                }
            } else {
                $result[0]["seq"] = -1;
            }

            $this->response->write(json_encode(array_values($result)));
        }

        /*if($featureId) {
            $seq = rand();
            $pdf = new FPDF();
            $pdf->Open();
            $pdf->SetMargins(15, 10, 15);
            $pdf->AddFont('Times','','times.php');
            $pdf->AddFont('TimesB','','timesbd.php');
            $this->previewCard($objectId, $pdf);
            $pdf->output(WGS3_PRINTCACHE_DIR."passport-$seq.pdf");
            echo $seq;
        }*/
    }

    function getRowHeight($values, $widths, $h = 0) {

    	$result = array();
    	$rvalues = array();
    	foreach ($values as $k => $value ) {
    		$h = $h ? $h : $this->rh;
    		$v = $value;
    		if ($value) {
    			$sw = $this->pdf->GetStringWidth($value);
    			if ($sw > $widths[$k]) {
    				$c = (ceil($sw/$widths[$k]));
    				$h = $c*$h;
    				$l = strlen($value);
    				$step = round($sw/$c);
    				$v = "";
    				for ($i=0; $i<$c; $i++) {
    					if ($i != $c-1) {
    						$v .= substr($value, $i*$step, $step)."\n";
    					} else {
    						$v .= substr($value, $i*$step);
    					}
    				}
    			}
    		}
    		array_push($rvalues, $v);
    		array_push($result, $h);
    	}
        return array(
        	'h' => max($result),
        	'values' => $rvalues
        );
    }

    function writeXY($text, $dx=0, $dy = 0)
    {
    	$x = $this->pdf->GetX()+$dx;
        $y = $this->pdf->GetY()+$dy;
        $this->pdf->WriteXY(5, $text, '', $x, $y);
        $this->pdf->SetXY($x, $y);
    }
/*
 * ��������� ����
 */
    function createTitlePage($title)
    {
        $pdf = $this->pdf;
        $pdf->AddPage();
        $pdf->SetFont('TimesB','',18);
        $pdf->MultiCell(0, 10, '��� "�������������"', 0, 'C' ,0);
        $pdf->SetFont('TimesB','',14);
        $pdf->MultiCell(0, 20, '����������� ������� �� ���������������� ������', 0, 'C', 0);
        $pdf->SetFont('TimesB','U',14);
        $pdf->MultiCell(0, 15, ' '.$title->get('name_object'), 0, 'C', 0);
        $pdf->SetFont('TimesB','',12);
        $pdf->MultiCell(0, 15, '������ � '.$title->get('inventory_number'), 0, 'C', 0);

        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7, ' ������������:', 0, 0, 'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('PRODUCTION_NAME'), 0, 'J' ,0);
        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7, ' ����� ������������:', 0, 0, 'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('PLACE_OF_USE_NAME'), 0, 'J', 0);
        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7, ' ������ ������������:', 0, 0, 'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('COMPOUND_OBJECT_NAME'), 0, 'J' , 0);
        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7,' �����: ',0,0,'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('COMPOUND_OBJECT_ADDRESS'), 0, 'J' , 0);
        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7,' ��������:', 0, 0, 'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('COMPOUND_OBJECT_BUILDING'), 0, 'J', 0);
        $pdf->SetFont('TimesB','',10);
        $pdf->Cell(50, 7,' ����� �������������:', 0, 0, 'L', 0);
        $pdf->SetFont('Times','',10);
        $pdf->MultiCell(0, 7, ' '.$title->get('COMPOUND_OBJECT_LICENCE'), 0, 'J', 0);
        //$pdf->Cell(0, 1, '', 1, 1, '�', 0);
        $pdf->Cell(20, 7, '', 0, 0, '�', 0);
        $pdf->MultiCell(0, 7, ' ', 0, 'J', 0);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Line($x+10, $y, 190,$y);
        $pdf->SetXY($x, $y);
        $pdf->Cell(20, 7, '', 0, 0, '�', 0);
        $pdf->MultiCell(0, 7, ' ', 0, 'J', 0);

        $pdf->Cell(100, 7,' ���������', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7, ' '.$title->get('CREATE_DATE'), 0, 'C', 0);
        $pdf->Cell(100, 7,' ���������� (���������-�) ��������� � �������� ��� ��', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7, $title->get('DATE_STANDART_OF_PRICE'), 0, 'C', 0);

        $pdf->Cell(100, 7,' ', 0, 0, 'L', 0);
        $pdf->Cell(20, 7,' ����� :', 0, 0, 'L', 0);
        $pdf->Cell(40, 7, $title->get('ALL_COST'), 0, 0, 'C', 0);
        $pdf->MultiCell(0, 7,' ���. ���.', 0, 'C', 0);

        $pdf->Cell(0, 7,' � ��� ����� :', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7,'', 0, 'L', 0);

        $pdf->Cell(70, 7,' ', 0, 0, 'L', 0);
        $pdf->Cell(50, 7,' 1. ���������������� ����� :', 0, 0, 'L', 0);
        $pdf->Cell(40, 7, $title->get('PRODUCTION_PART'), 0, 0, 'C', 0);
        $pdf->MultiCell(0, 7,' ���. ���.', 0, 'C', 0);

        $pdf->Cell(70, 7,' ', 0, 0, 'L', 0);
        $pdf->Cell(50, 7,' 2. ��������-������� ����� :', 0, 0, 'L', 0);
        $pdf->Cell(40, 7, $title->get('SERVICE_PART'), 0, 0, 'C', 0);
        $pdf->MultiCell(0, 7,' ���. ���.', 0, 'C', 0);

        $pdf->Cell(120, 7,' ��������� ���� (���������)', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7, $title->get('SUPERINTENDENT'), 0, 'C', 0);

        $pdf->Cell(120, 7,' ���. �������� �������� �����������(�����������) �� ���. ������� ', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7,' ', 0, 'L', 0);
        $pdf->Cell(120, 7,' (��� ������ ����������� ����, ���������� �� ����������', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7, $title->get('DEPUTY_CHIEF_ENGINEER'), 0, 'C', 0);
        $pdf->Cell(120, 7,' ���. ������� �� ����������� � �����)', 0, 0, 'L', 0);
        $pdf->MultiCell(0, 7,' ', 0, 'L', 0);
    }
/*
 * ���������
 */
    function createTitle($title)
    {
        $pdf = $this->pdf;
        $pdf->AddPage();
        $pdf->SetFont('TimesB','',12);
        $pdf->MultiCell(0, 10, $title, 1, 'C' ,0);
    }
/*
 * ����� ��������
 */
    function createGeneralInfoPage($passportId)
    {
        $generalInfo = $this->passportService->getGeneralInfo($passportId);
        if ($generalInfo) {
            $pdf = $this->pdf;
            $this->createTitle("����� ��������");
            $pdf->SetFont('TimesB','',10);
            $pdf->MultiCell(0, 10, $generalInfo->get('NAME_GENERAL_INFO'), 1, 'C' ,0);

            $pdf->SetFont('Times','',10);
            $pdf->Cell(110, 7,' 1. ��� ��������� :', 1, 0, 'L', 0);
            $pdf->MultiCell(0, 7, $generalInfo->get('YEAR_OF_BUILDING'), 1, 'C' ,0);
            $pdf->MultiCell(0, 3, '', 1, 'C' ,0);

            $pdf->Cell(110, 7, ' 2. ��������� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('NUMBER_OF_STOREYS'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '��.', 1, 'C', 0);

            $pdf->Cell(40, 7, '  � ��� ����� :', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ���������������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('NOS_PPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '��.', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('NOS_PPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '��.', 1, 'C', 0);

            $pdf->Cell(40, 7, '  ', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ��������-������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('NOS_SPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '��.', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('NOS_SPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '��.', 1, 'C', 0);
            $pdf->MultiCell(0, 3, '', 1, 'C' ,0);

            $pdf->Cell(110, 7, ' 3. ������� ���������, ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('AREA_OF_BUILDING_ALL'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�2', 1, 'C', 0);

            $pdf->Cell(40, 7, '  � ��� ����� :', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ���������������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('AOB_PPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�2', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('AOB_PPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�2', 1, 'C', 0);

            $pdf->Cell(40, 7, '  ', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ��������-������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('AOB_SPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�2', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('AOB_SPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�2', 1, 'C', 0);
            $pdf->MultiCell(0, 3, '', 1, 'C' ,0);

            $pdf->Cell(110, 7, ' 4. ������������ �����, ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('VOLUME_OF_BUILDING_ALL'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�3', 1, 'C', 0);

            $pdf->Cell(40, 7, '  � ��� ����� :', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ���������������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('VOB_PPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�3', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('VOB_PPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�3', 1, 'C', 0);

            $pdf->Cell(40, 7, '  ', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '  �) ��������-������� ����� :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('VOB_SPART'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�3', 1, 'C', 0);

            $pdf->Cell(40, 7, '', 1, 0, 'L' ,0);
            $pdf->Cell(70, 7, '     ������ :', 1, 0, 'L' ,0);
            $pdf->SetFont('TimesB','',10);
            $pdf->Cell(50, 7, $generalInfo->get('VOB_SPART_BASEMENT'), 1, 0, 'C' ,0);
            $pdf->SetFont('Times','',10);
            $pdf->MultiCell(0, 7, '�3', 1, 'C', 0);
        }
    }
/*
 * �������������� ��������������
 */
    function createStaticConstructiveRow($title, $height, $align)
    {
       $pdf = $this->pdf;
       $this->pdf->SetFont('TimesB','',10);
       $this->pdf->MultiCell(0, $height, $title, 1, $align ,0);
       $this->pdf->SetFont('Times','',10);

    }

    function createDynamicConstructiveRow($result)
    {
        foreach ($result as $record) {
            $this->pdf->Cell(110, 7, '    '.$record->get('GUIDE_NAME'), 'RL', 0, 'L' ,0);
            $this->pdf->MultiCell(0, 7, $record->get('VALUE'), 'RL', 'C' ,0);
        }
    }

    function createConstructivePage($passportId)
    {
        $pdf = $this->pdf;
        $this->createTitle("�������������� �������������� ������");

        $this->createStaticConstructiveRow('�. ���������������� �����', 10, 'C');
            $this->createStaticConstructiveRow(' 1. ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getBasePpart($passportId));
            $this->createStaticConstructiveRow(' 2. �������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getPillarPpart($passportId));
            $this->createStaticConstructiveRow(' 3. ����������� �����', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getCraneGirderPpart($passportId));
            $this->createStaticConstructiveRow(' 4. �����', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getWallPpart($passportId));
            $this->createStaticConstructiveRow(' 5. �����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getPeregorodkiPpart($passportId));
            $this->createStaticConstructiveRow(' 6. ������� ����������� ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getNesKonstruktPerekrPpart($passportId));
            $this->createStaticConstructiveRow(' 7. ������� ������� ������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getNesKonstruktKrovliPpart($passportId));
            $this->createStaticConstructiveRow(' 8. ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getYteplitPpart($passportId));
            $this->createStaticConstructiveRow(' 9. ������ (��������������� ����)', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getKrovlyaPpart($passportId));

        $this->createStaticConstructiveRow('B. ��������-������� �����', 10, 'C');
            $this->createStaticConstructiveRow(' 1. ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getBaseSBpart($passportId));
            $this->createStaticConstructiveRow(' 2. ������� ������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getNesKarkasSBpart($passportId));
            $this->createStaticConstructiveRow(' 3. �����', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getWallSBpart($passportId));
            $this->createStaticConstructiveRow(' 4. �����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getPeregorodkiSBpart($passportId));
            $this->createStaticConstructiveRow(' 5. ������������ ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getMejStorePerekrSBpart($passportId));
            $this->createStaticConstructiveRow(' 6. ��������� ����������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getCherdakPerekrSBpart($passportId));
            $this->createStaticConstructiveRow(' 7. ��������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getLestnizaSBpart($passportId));
            $this->createStaticConstructiveRow(' 8. ������� ������� ������', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getNesElemKrovliSBpart($passportId));
            $this->createStaticConstructiveRow(' 9. ������ (��������������� ����)', 7, 'L');
            $this->createDynamicConstructiveRow($this->passportService->getKrovlyaSBpart($passportId));
    }
/*
 * ������� ���������
 */
    function createStaticAreaRow($text, $params, $align)
    {
       $this->pdf->Cell(70, 7, $text, 1,0, $align ,0);
       $this->pdf->Cell(30, 7, $params[0], 1,0, 'C' ,0);
       $this->pdf->Cell(40, 7, $params[1], 1, 0, 'C' ,0);
       $this->pdf->MultiCell(0, 7, $params[2], 1, 'C' ,0);
    }

    function createDynamicAreaRow($result)
    {
        foreach ($result as $record) {
            $this->pdf->Cell(70, 7, $record->get('GUIDE_NAME'), 1,0, 'L' ,0);
            $this->pdf->Cell(30, 7, $record->get('VALUE_ALL'), 1,0, 'C' ,0);
            $this->pdf->Cell(40, 7, $record->get('VALUE_ONE_STORIES'), 1, 0, 'C' ,0);
            $this->pdf->MultiCell(0, 7, $record->get('VALUE_MANY_STORIES'), 1, 'C' ,0);
        }
    }

    function createAreaPage($passportId)
    {
       $this->createTitle('�������  ���������');
       $area = $this->passportService->getAreaOfRooms($passportId);
       $this->pdf->SetFont('TimesB','',10);
       $this->pdf->Cell(70, 10, '������������ ���������', 'RLT',0, 'C' ,0);
       $this->pdf->Cell(30, 10, '�����', 'RLT',0, 'C' ,0);
       $this->pdf->MultiCell(0, 10, '� ��� ����� :', 1, 'C' ,0);
       $this->pdf->Cell(70, 7, '', 'LB',0, 'C' ,0);
       $this->pdf->Cell(30, 7, '', 'LB',0, 'C' ,0);
       $this->pdf->Cell(40, 7, '����������� �����', 1, 0, 'C' ,0);
       $this->pdf->MultiCell(0, 7, '������������ �����', 1, 'C' ,0);
       $this->pdf->SetFont('Times','',10);

       $this->createStaticAreaRow(' ����������������',  array($area->get('SR_PPART_ALL'), $area->get('SR_PPART_ONE_STORIED'), $area->get('SR_PPART_MANY_STORIED')), 'L');
       $this->pdf->MultiCell(0, 7, ' � ��� ����� :', 1, 'L' ,0);
       $this->createStaticAreaRow('�� ����������',      array($area->get('SR_PPART_ENTRESOL_ALL'), $area->get('SR_PPART_ENTRESOL_ONE_STORIED'), $area->get('SR_PPART_ENTRESOL_MANY_STORIED')), 'C');
       $this->createStaticAreaRow('� ��������',         array($area->get('SR_PPART_BASEMENT_ALL'), $area->get('SR_PPART_BASEMENT_ONE_STORIED'), $area->get('SR_PPART_BASEMENT_MANY_STORIED')), 'C');
       $this->createStaticAreaRow(' ������',            array($area->get('SR_DEPOT_ALL'), $area->get('SR_DEPOT_ONE_STORIED'), $area->get('SR_DEPOT_MANY_STORIED')), 'L');
       $this->pdf->MultiCell(0, 7, ' � ��� ����� :', 1, 'L' ,0);
       $this->createStaticAreaRow('�� ����������',      array($area->get('SR_DEPOT_ENTRESOL_ALL'), $area->get('SR_DEPOT_ENTRESOL_ONE_STORIED'), $area->get('SR_DEPOT_ENTRESOL_MANY_STORIED')), 'C');
       $this->createStaticAreaRow('� ��������',         array($area->get('SR_DEPOT_BASEMENT_ALL'), $area->get('SR_DEPOT_BASEMENT_ONE_STORIED'), $area->get('SR_DEPOT_BASEMENT_MANY_STORIED')), 'C');
       $this->createStaticAreaRow(' ���������',         array($area->get('SR_SPART_ALL'), $area->get('SR_SPART_ONE_STORIED'), $area->get('SR_SPART_MANY_STORIED')), 'L');
       $this->pdf->MultiCell(0, 7, ' � ��� ����� :', 1, 'L' ,0);
       $this->createStaticAreaRow('�� ����������',      array($area->get('SR_SPART_ENTRESOL_ALL'), $area->get('SR_SPART_ENTRESOL_ONE_STORIED'), $area->get('SR_SPART_ENTRESOL_MANY_STORIED')), 'C');
       $this->createStaticAreaRow('� ��������',         array($area->get('SR_SPART_BASEMENT_ALL'), $area->get('SR_SPART_BASEMENT_ONE_STORIED'), $area->get('SR_SPART_BASEMENT_MANY_STORIED')), 'C');
       $this->createDynamicAreaRow($this->passportService->getAreaOfOtherRooms($passportId));
    }
/*
 * ������� �����
 */
    function createDinamicAreaFloorRow($result)
    {
        foreach ($result as $record) {
            $this->pdf->Cell(70, 7, $record->get('GUIDE_NAME'), 1,0, 'C' ,0);
            $this->pdf->Cell(40, 7, $record->get('VALUE_ONE_STORIES'), 1,0, 'C' ,0);
            $this->pdf->Cell(40, 7, $record->get('VALUE_MANY_STORIES'), 1,0, 'C' ,0);
            $this->pdf->MultiCell(0, 7, $record->get('VALUE_ALL'), 1, 'C' ,0);
        }
    }

    function createAreaFloorPage($passportId)
    {
        $this->createTitle('�������  �����');
        $result = $this->passportService->getAreaOfFloor($passportId);
        $this->pdf->SetFont('TimesB','',10);
        $this->pdf->Cell(70, 7, '������������ ����', 1,0, 'C' ,0);
        $this->pdf->Cell(40, 7, '����������� �����', 1,0, 'C' ,0);
        $this->pdf->Cell(40, 7, '������������ �����', 1,0, 'C' ,0);
        $this->pdf->MultiCell(0, 7, '�����', 1, 'C' ,0);
        $this->pdf->SetFont('Times','',10);
        $this->createDinamicAreaFloorRow($result);
    }

/*
 * ������� �������� ������������ ����������� �����������
 */
    function createStaticOutsideSurfacesRow3($text, $value)
    {
        $res = $this->getRowHeight(array($value),array(30));
        $h = $res['h'];

    	$this->pdf->Cell(140, $h, $text, 1,0, 'L' ,0);

    	if ($res['values'][0] != $value) {
    		$this->writeXY($res['values'][0]);
    		$value = "";
    	}

        $this->pdf->Cell(30, $h, $value, 1,0, 'C' ,0);

        $this->pdf->MultiCell(0, $h, '�2', 1, 'C' ,0);
    }

    function createStaticOutsideSurfacesRow5($text, $value1, $value2)
    {
		$res = $this->getRowHeight(array($value1, $value2),array(40, 30));
        $h = $res['h'];

        $this->pdf->Cell(90, $h, $text, 1,0, 'L' ,0);
    	if ($res['values'][0] != $value1) {
    		$this->writeXY($res['values'][0]);
    		$value1 = "";
    	}

        $this->pdf->Cell(40, $h, $value1, 1,0, 'C' ,0);
        $this->pdf->Cell(10, $h, '��.', 1,0, 'C' ,0);
    	if ($res['values'][1] != $value2) {
    		$this->writeXY($res['values'][1]);
    		$value2 = "";
    	}
        $this->pdf->Cell(30, $h, $value2, 1,0, 'C' ,0);
        $this->pdf->MultiCell(0, $h, '�2', 1, 'C' ,0);
    }

    function createDinamicOutsideSurfacesRow($result)
    {
        foreach ($result as $record) {
            $this->pdf->Cell(90, 7, '', 1, 0, 'L' ,0);
            $this->pdf->Cell(80, 7, $record->get('GLASS_SIZE'), 1,0, 'C' ,0);
            $this->pdf->MultiCell(0, 7, '�2', 1, 'C' ,0);
        }
    }

    function createOutsideSurfacesPage($passportId)
    {
        $this->createTitle('������� �������� ������������ ����������� �����������');
        $this->pdf->SetFont('Times','',10);
        $oS = $this->passportService->getRazmeriNarujPoverhn($passportId);

        $this->createStaticOutsideSurfacesRow3(' 1. ������� ���� �� ������� �������',       $oS->get('S_OS_WALL_MINUS_APERTURE'));
        $this->createStaticOutsideSurfacesRow3(' 2. ������� �������� ������ �������',       $oS->get('S_OS_END_WALL_OF_LANTERN'));
        $this->createStaticOutsideSurfacesRow3(' 3. ������� �������� ������������, �����',  $oS->get('S_OS_WALL_SENESTRATION_ALL'));
        $this->pdf->MultiCell(0, 7, '    � ��� ����� � ��������� ������ :', 1, 'L' ,0);
        $this->createDinamicOutsideSurfacesRow($this->passportService->getRazmeriStekol1($passportId));
        $this->createStaticOutsideSurfacesRow3(' 4. ������� ��������� ����������, �����',   $oS->get('S_OS_LANTERN_GLAZING_ALL'));
        $this->pdf->MultiCell(0, 7, '    � ��� ����� � ��������� ������ :', 1, 'L' ,0);
        $this->createDinamicOutsideSurfacesRow($this->passportService->getRazmeriStekol2($passportId));
        $this->createStaticOutsideSurfacesRow3(' 5. ����� ������� ���������� ��������� � ���������',   $oS->get('S_OS_GLAZING'));
        $this->createStaticOutsideSurfacesRow5(' 6. ���-�� � ������� ���������', $oS->get('Q_OS_AUTOGATE'), $oS->get('S_OS_AUTOGATE'));
        $this->createStaticOutsideSurfacesRow5(' 7. ���-�� � ������� �/� �����', $oS->get('Q_OS_RAILROADGATE'), $oS->get('S_OS_RAILROADGATE'));
        $this->createStaticOutsideSurfacesRow5(' 8. ���-�� � ������� �������� ������', $oS->get('Q_OS_EXTERNAL_DOOR'), $oS->get('S_OS_EXTERNAL_DOOR'));
        $this->createStaticOutsideSurfacesRow3(' 9. ������� ������, �����',   $oS->get('S_OS_ROOF_ALL'));
        $this->pdf->MultiCell(0, 7, '    � ��� ����� :', 1, 'L' ,0);
        $this->pdf->Cell(140, 7, '������������ ������', 1,0, 'C' ,0);
        $this->pdf->MultiCell(0, 7, '�������', 1, 'C' ,0);
        $result = $this->passportService->getRoofingValue($passportId);
        foreach ($result as $record) {
            $this->pdf->Cell(140, 7, $record->get('GUIDE_NAME'), 1, 0, 'L' ,0);
            $this->pdf->MultiCell(0, 7, $record->get('ROOFING_VALUE'), 1, 'C' ,0);
        }
    }

/*
* ������� ���������� ������������ ����������� � ������� ����������� ����������� �����
*/
    function createStaticInsideSurfacesRow($text1, $value, $text2)
    {
    	$res = $this->getRowHeight(array($value),array(30));
        $h = $res['h'];
    	$this->pdf->Cell(140, $h, $text1, 1,0, 'L' ,0);
    	if ($res['values'][0] != $value) {
    		$this->writeXY($res['values'][0]);
    		$value = "";
    	}
    	$this->pdf->Cell(30, $h, $value, 1,0, 'C' ,0);
        $this->pdf->MultiCell(0, $h, '�2', 1, 'C' ,0);
        $this->pdf->Cell(50, 7, '   � ��� ����� :', 1,0, 'L' ,0);
        $this->pdf->Cell(90, 7, $text2, 1,0, 'C' ,0);
        $this->pdf->MultiCell(0, 7, '�������', 1, 'C' ,0);
    }

    function createDynamicInsideSurfacesRow($result, $value)
    {
        foreach ($result as $record) {
            $this->pdf->Cell(50, 7, '', 1,0, 'L' ,0);
            $this->pdf->Cell(90, 7, $record->get('GUIDE_NAME'), 1,0, 'C' ,0);
            $this->pdf->Cell(30, 7, $record->get($value), 1,0, 'C' ,0);
            $this->pdf->MultiCell(0, 7, '�2', 1, 'C' ,0);
        }
        $this->pdf->MultiCell(0, 5, '', 1, 'C' ,0);
    }

    function createInsideSurfacesPage($passportId)
    {
        $this->createTitle('������� ���������� ������������ ����������� � ������� ����������� ����������� �����');
        $this->pdf->SetFont('Times','',10);
        $iS = $this->passportService->getRazmeriVnytrPoverhn($passportId);
        $this->createStaticInsideSurfacesRow('1. ����������� ������� ����������, �����', $iS->get('S_IS_FLOOR_ALL'), '����������� ����������');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsCeiling($passportId), 'CEILING_VALUE');
        $this->createStaticInsideSurfacesRow('2. ������� ����, �����', $iS->get('S_IS_WALL_ALL'), '����������� �����');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsWall($passportId), 'WALL_VALUE');
        $this->createStaticInsideSurfacesRow('3. ������� �����������, �����', $iS->get('S_IS_SEPTA_ALL'), '����������� �����������');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsSepta($passportId), 'SEPTA_VALUE');
        $this->createStaticInsideSurfacesRow('4. ����������� ����������� ������, �����', $iS->get('S_IS_PILLAR_ALL'), '����������� �������');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsPillar($passportId), 'PILLAR_VALUE');
        $this->createStaticInsideSurfacesRow('5. ����������� ����������� ����������� �����, �����', $iS->get('S_IS_CRANE_GIRDER_ALL'), '����������� ����������� �����');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsCraneGirder($passportId), 'CRANE_GIRDER_VALUE');
        $this->createStaticInsideSurfacesRow('6. ������ ����������� �����������, �����', $iS->get('S_IS_OTHER_ALL'), '�����������');
        $this->createDynamicInsideSurfacesRow($this->passportService->getIsOther($passportId), 'OTHER_VALUE');
    }

    /*
     * ��������� ������ � ����������� ��������� �� �������� ����������� ������ � ����������
     */
    function createProjectDataAndNotePage($passportId)
    {
        $this->createTitle('��������� ������ � ����������� ��������� �� �������� ����������� ������');

        $note = $this->passportService->getNote($passportId);
        $this->pdf->SetFont('TimesB','',10);
        $this->pdf->MultiCell(0, 7, '�� ����� ��� ����� ���������� ����������� �����(����������)', 1, 'C' ,0);
        $this->pdf->SetFont('Times','',10);
        $this->pdf->MultiCell(0, 7, ' '.$note->get('LOAD_GIRDER'), 1, 'L' ,0);

        $this->pdf->SetFont('TimesB','',10);
        $this->pdf->MultiCell(0, 7, '�� ����������� �����(����������)', 1, 'C' ,0);
        $this->pdf->SetFont('Times','',10);
        $this->pdf->MultiCell(0, 7, ' '.$note->get('LOAD_CRANE_GIRDER'), 1, 'L' ,0);

        $this->pdf->SetFont('TimesB','',10);
        $this->pdf->MultiCell(0, 7, '�� ���� ����������� �����(����������)', 1, 'C' ,0);
        $this->pdf->SetFont('Times','',10);
        $this->pdf->MultiCell(0, 7, ' '.$note->get('LOAD_FLOOR'), 1, 'L' ,0);
        $this->pdf->SetFont('TimesB','',10);

        $this->pdf->MultiCell(0, 7, '�� ���� ������������� �����(����������)', 1, 'C' ,0);
        $this->pdf->SetFont('Times','',10);
        $this->pdf->MultiCell(0, 7, ' '.$note->get('LOAD_STORE_CEILING'), 1, 'L' ,0);

        $this->createTitle('����������');
        $this->pdf->SetFont('Times','',10);
        $this->pdf->MultiCell(0, 7, ' '.$note->get('NOTE'), 1, 'L' ,0);
    }

    /*
     * ��������� �����
     */
    function saveFloorPlanImage($floorPlanName, $content)
    {
         $fpw = @fopen(WGS3_PRINTCACHE_DIR.$floorPlanName,"w+");
         $nBuffer = 8192;
         for ($i=0; $i<strlen($content); $i+=$nBuffer)
         @fwrite($fpw,substr($content,$i,($i+$nBuffer)),$nBuffer);
         @fclose ($fpw);
		 $convertScript = '"C:\Program Files\GraphicsMagick-1.3.13-Q8\gm.exe" convert d:/project/wgs3/application/www/printcache/' . $floorPlanName . ' d:/project/wgs3/application/www/printcache/' . $floorPlanName . '.png' ;
		 $error = exec($convertScript);
		 //@unlink(WGS3_PRINTCACHE_DIR.$floorPlanName);
		 return WGS3_PRINTCACHE_DIR.$floorPlanName.'.png';
    }

    function addImage($pdf,$imageName)
    {
		$pdf -> AddPage();
        $pdf -> Image($imageName, 0, 0, 0);
		@unlink($imageName);
		
		//old version:
		/* 
		$image_param = getimagesize($imageName);
        pdf_begin_page($pdf, $image_param[0], $image_param[1]);
        $image = pdf_open_image_file($pdf,'tiff',$imageName, null, null);
        pdf_place_image($pdf,$image,0,0,1.0);
        pdf_end_page($pdf);
        @unlink(WGS3_PRINTCACHE_DIR.$floorPlanName);
		
		*/
    }

    function getFloorPlan($passportId)
    {
        if (count($this->passportService->getFloorPlan($passportId)) > 0) {
            return true;
        } else
        return false;
    }
function doGetFloorPlan(){	
	/* 
	*	@author ������� �.
	*	@date 20.03.2012
	*
	*	1. �������� ��������� �����
	*	2. ���� ����� ����, ��:
	*		2.1 ��������� ������ �� hdd
	*		2.2 ��������� ������ ����������� � PNG � ������� ��������
	*		2.3	���������� ������ � ������ PDF ��� max ������ PNG
	*		2.4 ��������� PDF �� PNG � ������� PNG
	*		2.5 ���������� PDF ������� � ������� ��� � hdd
	*
	*/
	$passportId = $this->request->getPost('passportId');
	$result = $this->wgsTier->getProjectObjectService()->getPassportService()->getFloorPlan($passportId);
	if (count($result)) {
		$seq = rand();

		$floorplanImageNames = array();
		$i = 0;
		$maxHeight = 0;
		$maxWidth = 0;
		foreach ($result as $record) {
            $floorplanImageNames[$i] = $this->saveFloorPlanImage($record->get('FLOOR_PLAN_NAME') . "-" . $seq,$record->get('PICTURE'));
			$imageinfo = getimagesize($floorplanImageNames[$i]);
				if ($imageinfo[0] > $maxWidth) $maxWidth = $imageinfo[0];
				if ($imageinfo[1] > $maxHeight) $maxHeight = $imageinfo[1];
			$i++;			
        }
		
		$pdf = new PDF('P','pt', array($maxWidth, $maxHeight));
		$pdf -> Open();
		$pdf ->AddFont('Times','','times.php');
        $pdf ->AddFont('TimesB','','timesbd.php');
        $pdf ->AliasNbPages();
		
		foreach ($floorplanImageNames as $fpName) {
			$this->addImage($pdf, $fpName);
		}
       
		$pdf->output(WGS3_PRINTCACHE_DIR."floorplan-".$seq.".pdf");
		echo $seq;
		//@unlink(WGS3_PRINTCACHE_DIR."floorplan-$seq.pdf");
	}        
        
	
		/*$passportId = $this->request->getPost('passportId');
        if ($passportId) {
            $seq = rand(); //���������� ��������� ��������� ����� �����
            	
			
			$floorplan = array();
            $result = $this->wgsTier->getProjectObjectService()->getPassportService()->getFloorPlan($passportId);
            if (count($result)) {
                $pdf = pdf_new();
                pdf_open_file($pdf, WGS3_PRINTCACHE_DIR."floorplan-$seq.pdf");
                foreach ($result as $record) {
                   $this->addImage($pdf, $this->saveFloorPlanImage($record->get('FLOOR_PLAN_NAME'),$record->get('PICTURE')));
                }
                pdf_close($pdf);
                echo $seq;
            }
			//echo $seq;
			
        }
		
        //@unlink(WGS3_PRINTCACHE_DIR."map-$seq.pdf");
        */
    }
    
	/*
	* 	���� � ���, ��� �� ���������� pdf ������ � ��� ������ ���� ��������� �����-�� ���������,
	*	�� ���� getFloorPlanIsUpToDate ���������� 0, ���� ��� ��. � � ���� ������ �� ������ ���������� passportId,
	*	�� �������� ���������� pdf-��������
	*/
/*function doGetFloorPlan()
    {
        $passportId = $this->request->getPost('passportId');
        if ($passportId) {
			$seq = rand();
			$isUpToDate= $this->wgsTier->getProjectObjectService()->getPassportService()->getFloorPlanIsUpToDate($passportId);
            if (count($isUpToDate)){
				foreach ($isUpToDate as $i=>$record){
					$outdatedCount = $record->get('cfp');
				}
			}
			if ($outdatedCount > 0){
			$result = $this->wgsTier->getProjectObjectService()->getPassportService()->getFloorPlan($passportId);
            if (count($result)) {
				try{
					$outputPdf = new Imagick(); //������� ������ ��� ������
					$page = new Imagick(); //������� ������ Imagick �� ����������� �����������.
					
					foreach ($result as $i=>$record) {
                		$page -> readImageBlob($record->get('PICTURE'));
						//$images -> readImage('c:\3300.tiff');
						//$outputPdf -> addImage($page);
						$page -> writeImage('c:\page'.$i.'-'.$seq.'.ps');
						$page -> clear();					
					}
                //pdf_close($pdf);
					//$outputPdf -> writeImages(WGS3_PRINTCACHE_DIR.'floorplan-'./*$otdatedCount$passportId.'.pdf', true);
					//$outputPdf -> clear();
					$upd = $this->wgsTier->getProjectObjectService()->getPassportService()->updateFloorPlanCacheDate($passportId);
					echo $passportId;
					
					//echo $passportId;
					
				}catch(Exception $e){
						//echo $e->getMessage();
				}
					
			}
			} else echo $passportId;
        }
		
    }*/

    function doGetPdf()
    {
        $params = $this->getRequestParams();
        $name = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."$name.pdf");
        $len = strlen($buf);
        $this->response->header("Content-type: application/pdf");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: inline; filename=$name.pdf");
        $this->response->write($buf);
        //if($name{0}!='f')
		@unlink(WGS3_PRINTCACHE_DIR."$name.pdf");
    }

}

?>