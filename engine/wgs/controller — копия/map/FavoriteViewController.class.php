<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/controller/MapController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class FavoriteViewController extends wgsFrontController
{
    
	function doGetCount(){
		$wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
        if($wgsuser_id){
			return $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->GetCount($wgsuser_id);         
		}else return false;
	}
	// ��������� store ��� ������ �������
    //---------------------------------------------------------------------------------------------------------
    function doGetView($ViewArray)
    {
        $count = 1; 
        while ($count < 10)
        {
            if (isset($ViewArray[$count]) && $ViewArray[$count]) 
            {
                $url = $ViewArray[$count]['url'];
                $centerX = $ViewArray[$count]['centerX'];
                $centerY = $ViewArray[$count]['centerY'];
                $scale = $ViewArray[$count]['scale'];
                $name = $ViewArray[$count]['name'];
                $layerview = $ViewArray[$count]['layerview'];
            }
            else 
            {
                $url =  '/images/favorite_'.$count.'.png';
                $centerX = '';
                $centerY = '';
                $scale = '';
                $name = lmb_win1251_to_utf8('��� '.$count);  
				$layerview = $ViewArray[$count]['layerview'];
            }
            $documents[] = array (
                'name' => $name,
                'number' => $count, 
                'url'=> $url,
                'centerX' => $centerX,
                'centerY' => $centerY,
                'scale' => $scale,
                'width' => 257,
                'height' => 200,
            	'layerview' => $layerview
            );
            $count++;
        }
        $store = array('view' => $documents);
        echo json_encode($store);
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ����������� �������������� �������������� ��������� ����� ��� user
    //---------------------------------------------------------------------------------------------------------
    function doGetGeometryProperty()
    {   
        if ($this->wgsTier->getAuthorizedUser())
            $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
        
        if ($this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout())
            $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
        
        if ($wgsuser_id && $defaultWebLayout)    
            $viewList = $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->getGeometryPropretyByUserId($wgsuser_id, $defaultWebLayout);
        
        foreach($viewList as $view)
        {
            $center = explode(',', $view->get('CENTER'));
            $centerX = $center[0];
            $centerY = $center[1];

            if ($this->request->getPost('MAPNAME')) $mapname = $this->request->getPost('MAPNAME');
            if ($this->request->getPost('SESSION')) $session = $this->request->getPost('SESSION');
            
            $ViewArray[$view->get('NUM')]['centerX'] = $centerX;
            $ViewArray[$view->get('NUM')]['centerY'] = $centerY;
            $ViewArray[$view->get('NUM')]['scale'] = $view->get('SCALE');
            $ViewArray[$view->get('NUM')]['name'] = lmb_win1251_to_utf8($view->get('NAME'));
            $ViewArray[$view->get('NUM')]['layerview'] = $view->get('LAYER_VIEW');
            
            $file = WGS3_FAVORITEVIEW_DIR.$view->get('CACHE_NAME');

            if (!file_exists($file)) {
                $ViewArray[$view->get('NUM')]['url'] = $this->doGetPngFromBlob($view->get('NUM'), $wgsuser_id, $defaultWebLayout, $view->get('CACHE_NAME'));
            } else {
                $ViewArray[$view->get('NUM')]['url'] = "/favoriteview_cache/".$view->get('CACHE_NAME');     
            }
        }
        $this->doGetView($ViewArray);
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ��������� � ��������� png �� ��������� ����� �� �������
    //---------------------------------------------------------------------------------------------------------
    function doGetPng($mapname, $session, $centerX, $centerY, $scale, $width, $height, $number)
    {
        //$this->delTemporaryFiles(WGS3_FAVORITEVIEW_DIR);
        
        $this->server = $_SERVER;
        $seq = rand();
        $DPI = 96;
        $widthSketch = 257;
        $heightSketch = 200;
        $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
        // �������� ��������
        if ($width > $height) 
            $orgScale = $scale * ($width / $widthSketch);
        else
            $orgScale = $scale * ($height / $heightSketch);
        
        // ����������� png    
        $query   = "MAPNAME=$mapname&OPERATION=GETVISIBLEMAPEXTENT&SEQ=0.256{$seq}265&SESSION=$session&SETDISPLAYDPI=$DPI&SETDISPLAYHEIGHT=$heightSketch&SETDISPLAYWIDTH=$widthSketch&SETVIEWCENTERX=$centerX&SETVIEWCENTERY=$centerY&SETVIEWSCALE=$orgScale&VERSION=1.0.0";        
        $header  = '';
        $header .= "POST /mapguide/mapguide/wgsagent.fcgi HTTP/1.1\r\n";
        $header .= "Host: ". $this->server['SERVER_NAME'] .":".$this->server['SERVER_PORT']."\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-length: " . strlen($query) . "\r\n";
        $header .= "Connection: keep-alive\r\n\r\n";
        $socket = fsockopen($this->server['SERVER_NAME'],$this->server['SERVER_PORT']);
        if (!$socket)
        {
            echo '������ �������� ������<BR>';
            return;
        }
        fputs($socket, $header.$query);
        while (!feof($socket)) {
            fgets($socket, 4096);
        }
        fclose($socket);
        
        // ��������� png � ���
        $fpw = @fopen(WGS3_FAVORITEVIEW_DIR."favorite-$seq.png","w+");
        if ($fpr = @fopen("http://".$this->server['SERVER_NAME'].":".$this->server['SERVER_PORT']."/mapguide/mapagent/wgsagent.fcgi?OPERATION=GETDYNAMICMAPOVERLAYIMAGE&FORMAT=PNG&VERSION=1.0.0&SESSION=$session&MAPNAME=$mapname&SEQ=0.256{$seq}256", "rb")) {
            while (!@feof ($fpr)) {
                $buffer = @fread($fpr, 4096);  
                @fwrite($fpw, $buffer); 
            }
            @fclose ($fpr); 
        }
        @fclose ($fpw);
        return "/favoriteview_cache/favorite-$seq.png";
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ��������� �� ���� � ��������� png �� ��������� ����� �� �������
    //---------------------------------------------------------------------------------------------------------
    function doGetPngFromBlob($num, $wgsuser_id, $defaultWebLayout, $cache_name)
    {
        
        $content = $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->getBlob($num, $wgsuser_id, $defaultWebLayout);
        // ��������� png � ���
        $fpw = @fopen(WGS3_FAVORITEVIEW_DIR.$cache_name,"w+");
          $nBuffer = 8192;
          for ($i=0; $i<strlen($content[0]['PICTURE']); $i+=$nBuffer)
            @fwrite($fpw,substr($content[0]['PICTURE'],$i,($i+$nBuffer)),$nBuffer);   
        @fclose ($fpw);
        
        return "/favoriteview_cache/$cache_name";
    }
    //---------------------------------------------------------------------------------------------------------
    
	function doGetCurrentViewLayers(){
		$mapname = $this->request->getPost('MAPNAME');        
		$layer_view = implode(",",$this->getLayerVisibility($mapname)); 
        return $layer_view;
	}
    // ������� ��������� ������ ����
    //---------------------------------------------------------------------------------------------------------
    function doInsertView()
    {
        $number = $this->request->getPost('number');
        $scale = $this->request->getPost('scale');
        $centerX = $this->request->getPost('centerX');
        $centerY = $this->request->getPost('centerY');
        $center = $centerX.','.$centerY;
        $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
        $name = '��� '.$number.'';
        
        $mapname = $this->request->getPost('MAPNAME');
        $session = $this->request->getPost('SESSION');
        $width = $this->request->getPost('WIDTH');
        $height = $this->request->getPost('HEIGHT');
        $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();

        $url = $this->doGetPng($mapname, $session, $centerX, $centerY, $scale, $width, $height, $number);
        $cache = explode('/', $url);
        $cache_name = $cache[2];
        $picture = WGS3_FAVORITEVIEW_DIR.$cache_name;
        $layer_view = implode(",",$this->getLayerVisibility($mapname)); 
         
        $old_cache_name = $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->insertView($number, $wgsuser_id, $center, $scale, $name, $defaultWebLayout, $picture, $cache_name, $layer_view); 
        if ($old_cache_name && file_exists(WGS3_FAVORITEVIEW_DIR.$old_cache_name))
            @unlink (WGS3_FAVORITEVIEW_DIR.$old_cache_name);
            
        $this->response->write($url);
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ������� �������� ����
    //---------------------------------------------------------------------------------------------------------
    function doRemoveView($number)
    {
        if ($this->request->getPost('number')) $number = $this->request->getPost('number');
        $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
        $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
        $cache_name = $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->removeView($number, $wgsuser_id, $defaultWebLayout);
        if ($cache_name && file_exists(WGS3_FAVORITEVIEW_DIR.$cache_name)) 
            @unlink (WGS3_FAVORITEVIEW_DIR.$cache_name);
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ������� �������� �����
    //---------------------------------------------------------------------------------------------------------
    function doClearView()
    {
        if ($this->request->getPost('param'))
        {
            $params = (json_decode($this->request->getPost('param'), true));
            foreach($params as $param)
            {
                $this->doRemoveView($param['number']);
            }
        } 
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ������� ���������� ����� ����
    //---------------------------------------------------------------------------------------------------------
    function doUpdateName()
    {
        if ($this->request->getPost('param'))
        {
            $request = (json_decode($this->request->getPost('param'), true));
            $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();    
            foreach($request as $view)
            {
                $number = $view['number'];
                $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
                $name = lmb_utf8_to_win1251($view['name']);
                
                $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->updateName($number, $wgsuser_id, $name, $defaultWebLayout);
            }   
        }       
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ������� ���������� �����
    //---------------------------------------------------------------------------------------------------------
    function doUpdateViewList()
    {
        if ($this->request->getPost('param'))
        {
            $params = (json_decode($this->request->getPost('param'), true));
            
            foreach($params as $param)
            {
                $mapname = $this->request->getPost('MAPNAME');
                $session = $this->request->getPost('SESSION');
                $width = $this->request->getPost('WIDTH');
                $height = $this->request->getPost('HEIGHT');
                $centerX = $param['centerX'];
                $centerY = $param['centerY'];
                $center = $centerX.','.$centerY;
                $scale = $param['scale'];
                $number = $param['number']; 
                
                $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
                $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
                
                $url = $this->doGetPng($mapname, $session, $centerX, $centerY, $scale, $width, $height, $number);
                $cache = explode('/', $url);
                $cache_name = $cache[2];
                $picture = WGS3_FAVORITEVIEW_DIR.$cache_name; 
                 
                $old_cache_name = $this->wgsTier->getProjectTier()->getModuleService()->getFavoriteViewService()->insertView($number, $wgsuser_id, $center, $scale, $name, $defaultWebLayout, $picture, $cache_name); 
                if ($old_cache_name && file_exists(WGS3_FAVORITEVIEW_DIR.$old_cache_name)) 
                    @unlink (WGS3_FAVORITEVIEW_DIR.$old_cache_name);
                
                $ViewArray[$number]['url'] = $url;
            }
            echo json_encode($ViewArray);   
        }       
    }
    //---------------------------------------------------------------------------------------------------------
    
    //------------------------------------------������� ��������� ����� � �������------------------------------
    function delTemporaryFiles ($directory)
    {
        $dir = @opendir ($directory);
        while (( $file = @readdir ($dir)))
        { 
        if( is_file ($directory.$file))
        {
          $acc_time = filemtime ($directory."/".$file);
          $time =  time();
          if (($time - $acc_time) > 3600) // ���
          {
               @unlink ($directory.$file);
          }     
        }
        }
        @closedir ($dir);
    }   
    //---------------------------------------------------------------------------------------------------------
    
    function getLayerVisibility($mapName)
    {
        if ($mapName) {
        	//$layersArray = array();
        	//$layersGroupArray = array();
        	$visibleArray = array();
        	
			$this->mgTier = lmbToolkit :: instance() -> getMgTier();
			$siteConnection = $this->mgTier->getSiteConnection();

			$map = new MgMap($siteConnection);

			$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
			
			$map->Open($resourceService, $mapName);
			
			$layers = $map->GetLayers();
			$layersCount = $layers->GetCount();
			
			for ($i = 0; $i < $layersCount; $i++) {
				$layer = $layers->GetItem($i);
				if ($layer->isVisible()) {
					$layerDefinition = $layer->GetLayerDefinition();
					$visibleArray[] = sprintf("%u",crc32($layerDefinition->GetPath().$layerDefinition->GetName().$layer->GetName()));
				}
			}
        	
			$layerGroups = $map->GetLayerGroups();
			$layerGroupsCount = $layerGroups->GetCount();
			
			for ($i = 0; $i < $layerGroupsCount; $i++) {
				$layerGroup = $layerGroups->GetItem($i);
				if ($layerGroup->isVisible()) {
					$visibleArray[] = sprintf("%u",crc32($layerGroup->GetName()));
				}
			}
			
			//echo json_encode(array('l' => $layersArray, 'lg' => $layersGroupArray));
			
			return $visibleArray;
			/*
			return array(
				'l' => $layersArray,
				'lg' => $layersGroupArray
			);
			*/
			//print_r($layersArray);
			//print_r($layersGroupArray);
        }
    }
}

?>