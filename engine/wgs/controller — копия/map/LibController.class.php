<?php

lmb_require('wgs/controller/wgsFrontController.class.php');

class LibController extends wgsFrontController
{
    function doGetSelectedFeatures()
    {
        $mapName = $this->request->getPost('mapname');
        $selectionXML = $this->request->getPost('selection');

        $isKingOra = strstr($selectionXML, 'KingOra') ? true : false;
        $isShape = $this->request->getPost('isshape');
        if(!$isShape)
			$isShape = strstr($selectionXML,'shp') ? true: false;
		
        
        /////////////////////////////////////////
        if ($this->request->getPost('version') == '4') {

            //todo ������ ��� ����, �.�. ���������� ����������� �������� � ������ ����������
            $sessionID = $this->request->getPost('session');
            session_start();

            $user = new MgUserInformation($sessionID);
            $user->SetClientIp(GetClientIp());
            $user->SetClientAgent(GetClientAgent());

            $siteConnection = new MgSiteConnection();
            $siteConnection->Open($user);

            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        }
        //////////////////////////////////////////////
        if ($mapName && $selectionXML) {
            $this->mgTier = lmbToolkit :: instance()->getMgTier();
            if (!$this->request->getPost('version') || $this->request->getPost('version') != '4') {
                $siteConnection = $this->mgTier->getSiteConnection();
                $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
            }
            $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);

            $map = new MgMap($siteConnection);
            $map->Open($mapName);
            $selection = new MgSelection($map, $selectionXML);

            $layers = $selection->GetLayers();
            $layersCount = $layers->GetCount();

            $selectedLayers = array();

            for ($i = 0; $i < $layers->GetCount(); $i++) {
                $selectedLayer = array();
                $layer = $layers->GetItem($i);
                $featureClassName = $layer->GetFeatureClassName();
                $selectedLayer['featureClass'] = $featureClassName;
                $layerFeatureId = $layer->GetFeatureSourceId();
                $selectionString = $selection->GenerateFilter($layer, $featureClassName);
                $queryOptions = new MgFeatureQueryOptions();
                $queryOptions->SetFilter($selectionString);
                $featureReader = $featureService->SelectFeatures(new MgResourceIdentifier($layerFeatureId), $featureClassName, $queryOptions);

                while ($featureReader->ReadNext()) {
                    if (($featureReader->GetClassDefinition()->GetProperties()->Contains('FEATID'))) {
                        $selectedLayer['features'][] = array(
                            'FeatId' => $featureReader->GetInt64('FEATID')
                        );
                    } else {
                        //ar_dump($featureReader->GetPropertyType('feature_id')); //Is it Oracle or PostGIS
                        if ($isShape) {
							try {
								$id = $featureReader->GetInt64('OBJECTID');
							} catch (Exception $e) {
								$id = rand();
							}
							$label = $featureReader->IsNull('Text')? lmb_win1251_to_utf8('��� ������������') : $featureReader->GetString('Text');
							
							$selectedLayer['features'][] = array(
								'id' => $id,
								'label' => $label
							);
						} else {
					
                        
							$selectedLayer['features'][] = $isKingOra ? array(
								'entityId' => $featureReader->GetInt64('entityid'),
								'featureId' => $featureReader->GetInt64('FEATURE_ID'),
								'label' => $featureReader->IsNull('LABEL') ? '' : $featureReader->GetString('LABEL')
							) : array(
								'entityId' => $featureReader->GetInt64('feature_id'),
								'featureId' => $featureReader->GetInt64('feature_id'),
								'label' => $featureReader->IsNull('label') ? '' : $featureReader->GetString('label')
							);
						}

                    }
                }
                $selectedLayers[] = $selectedLayer;
            }
            $this->response->write(json_encode($selectedLayers));
        }
    }

    function queryParentFeatures($objects, $features)
    {
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getFeaturesByOrphanObjects($objects);
        // var_dump( count($result));
        if (count($result) > 0) {
            foreach ($result as $value) {
                array_push($features, $value->get('feature_id'));
            }
        }
        
        return $features;
    }

    function doSelectFeatures()
    {
        $mapName = $this->request->getPost('mapname');
        $wgsVersion = $this->request->getPost('wgsVersion');
        $features = $this->request->getPost('features');
        if($wgsVersion >= 4 && features != null) {
            $features = json_decode($features);    
        }
        $objects = $this->request->getPost('objects');
        if($wgsVersion >= 4 && $objects != null) {
            $objects = json_decode($objects);
            $features = array();
        }
        $sessionID = $this->request->getPost('session');
        session_start();

        $user = new MgUserInformation($sessionID);
        $user->SetClientIp(GetClientIp());
        $user->SetClientAgent(GetClientAgent());

        $siteConnection = new MgSiteConnection();
        $siteConnection->Open($user);

        //������������� ������� ��� ������������� ��������
        if ($objects && count($objects) > 0) {
            if (!$features || count($features) == 0) {
                $features = array();
            }
            $features = $this->queryParentFeatures($objects, $features);
        }

        // var_dump($objects);
        // var_dump($features);
        if ($mapName && $features) {
            //���� �������� ��� ��������� ����
            $mgLayerList = $this->wgsTier->getProjectObjectService()->getMgLayerService()->getMgLayerListByEntityIds($features);
            if (count($mgLayerList) > 0) {

                $mgLayerArray = array();
                foreach ($mgLayerList as $mgLayer) {
                    $mgLayerArray[$mgLayer->get('resource_id')][] = $mgLayer->get('feature_id');
                }
                
                // var_dump($mgLayerArray);

                if (count($mgLayerArray) > 0) {
                    $this->mgTier = lmbToolkit :: instance()->getMgTier();
                    // $siteConnection = $this->mgTier->getSiteConnection();

                    $map = new MgMap($siteConnection);

                    $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
                    $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);

                    $map->Open($mapName);
                    $layers = $map->GetLayers();

                    $selection = new MgSelection($map);
                    $entityArray = array();
                    //������ ������ ��� �������� �������, � ������ ���� ������������ ���� entityid
                    foreach ($mgLayerArray as $resourceId => $entities) {
                        for ($i = 0; $i < $layers->GetCount(); $i++) {
                            $layer = $layers->GetItem($i);
                            $layerResourceId = $layer->GetLayerDefinition()->GetPath() . '/' . $layer->GetLayerDefinition()->GetName();
                            if (strstr($resourceId, $layerResourceId)) {
                                $entityArray[$layer->GetLayerDefinition()->ToString()] = $entities;
                            }
                        }
                    }
                    
                    // var_dump($entityArray);
                    //���� � ������������� ����� ���� ������� ����� �������
                    if (count($entityArray) > 0) {
                        for ($i = 0; $i < $layers->GetCount(); $i++) {
                            $layer = $layers->GetItem($i);
                            if (isset($entityArray[$layer->GetLayerDefinition()->ToString()])) {
                                $featureClassName = $layer->GetFeatureClassName();
                                $layerFeatureId = $layer->GetFeatureSourceId();
                                $featureFilter = 'feature_id = ' . implode(' or feature_id = ', $entityArray[$layer->GetLayerDefinition()->ToString()]);
                                $queryOptions = new MgFeatureQueryOptions();
                                $queryOptions->SetFilter($featureFilter);
                                $featureClassReader = $featureService->SelectFeatures(new MgResourceIdentifier($layerFeatureId), $featureClassName, $queryOptions);
                                if ($featureClassReader->ReadNext()) {
                                    if ($layer->GetGroup()) {
                                        if (!$layer->GetGroup()->isVisible()) {
                                            $layer->GetGroup()->SetVisible(true);
                                        }
                                    }
                                    if (!$layer->isVisible()) {
                                        $layer->SetVisible(true);
                                    }
                                }
                                $featureReader = $layer->SelectFeatures($queryOptions);
                                $selection->AddFeatures($layer, $featureReader, 0);
                                $layer->ForceRefresh();
                            }
                        }

                        $map->Save($resourceService);
                        //$selection->Save($resourceService, $mapName);

                        $xml = $selection->ToXml();

                        $objectCount = DOMDocument::loadXML($xml)->getElementsByTagName('ID')->length;
                        if ($objectCount) {
                            echo $xml;
                        } else {
                            throw new wgsException(wgsException::SELECT_OBJECT_IS_IMPOSSIBLE);
                        }
                    } else {
                        throw new wgsException(wgsException::SELECT_OBJECT_IS_IMPOSSIBLE);
                    }
                } else {
                    throw new wgsException(wgsException::SELECT_OBJECT_IS_IMPOSSIBLE);
                }
            } else {
                throw new wgsException(wgsException::SELECT_OBJECT_IS_IMPOSSIBLE);
            }
        }
    }

    function doSetLayerVisibility()
    {
        $mapName = $this->request->getPost('mapname');
        $layerView = $this->request->getPost('layerview');
        if ($mapName) {
            $this->mgTier = lmbToolkit :: instance()->getMgTier();
            $siteConnection = $this->mgTier->getSiteConnection();

            $map = new MgMap($siteConnection);

            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

            $map->Open($resourceService, $mapName);

            $layers = $map->GetLayers();
            $layersCount = $layers->GetCount();

            for ($i = 0; $i < $layersCount; $i++) {
                $layer = $layers->GetItem($i);
                if ($layer->GetDisplayInLegend()) {
                    $layerDefinition = $layer->GetLayerDefinition();
                    $layerCRC = sprintf("%u", crc32($layerDefinition->GetPath() . $layerDefinition->GetName() . $layer->GetName()));
                    $layer->SetVisible(in_array($layerCRC, $layerView));
                    $layer->ForceRefresh();
                }
            }

            $layerGroups = $map->GetLayerGroups();
            $layerGroupsCount = $layerGroups->GetCount();

            for ($i = 0; $i < $layerGroupsCount; $i++) {
                $layerGroup = $layerGroups->GetItem($i);
                if ($layerGroup->GetDisplayInLegend()) {
                    $layerGroupCRC = sprintf("%u", crc32($layerGroup->GetName()));
                    $layerGroup->SetVisible(in_array($layerGroupCRC, $layerView));
                }
            }

            $map->Save($resourceService);
        }
    }

    function doGetFeaturesByPolygon()
    {
        $mapName = $this->request->getPost('mapname');
        $coordsOfPolygon = $this->request->getPost('coords');
        if ($mapName && $coordsOfPolygon) {
            //$mgLayerList = $this->wgsTier->getProjectObjectService()->getMgLayerService()->getMgLayerListByEntityIds($features);
            $this->mgTier = lmbToolkit :: instance()->getMgTier();
            $siteConnection = $this->mgTier->getSiteConnection();
            // ��� ��������� ����� ����� mapguide
            $map = new MgMap($siteConnection);
            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
            $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
            //��� ��������� ����� ����� oracle
            $user = $this->wgsTier->getAuthorizedUser();
            $acl = $this->wgsTier->getACL();
            $this->mgTier = lmbToolkit :: instance()->getMgTier();

            $siteConnection = $this->mgTier->getSiteConnection();
            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

            $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout();
            $featureLayerList = $this->wgsTier->getProjectObjectService()->getFeatureLayerList();

            $map->Open($resourceService, $mapName);
            $layers = $map->GetLayers();

            $featureLayerIds = array();

            if ($mgMapLayerList = $defaultWebLayout->getMgMapLayers()) {
                foreach ($mgMapLayerList as $mgMapLayer) {
                    foreach ($featureLayerList as $featureLayer) {
                        if ($featureLayer->getId() == $mgMapLayer->getFeaturelayerId()) {
                            if ($acl->isAllowed($user, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS)) {

                                $mgMapLayerIdentify = $mgMapLayer->getMgResourceId();

                                for ($i = 0; $i < $layers->GetCount(); $i++) {
                                    $layer = $layers->GetItem($i);
                                    if ($layer->GetGroup()) {
                                        if ($layer->GetGroup()->isVisible()) {
                                            if ($layer->isVisible()) {
                                                if ($layer->GetSelectable()) {
                                                    $layerResourceId = $layer->GetLayerDefinition()->GetPath() . '/' . $layer->GetLayerDefinition()->GetName();
                                                    if ('Library://' . $layerResourceId . '.LayerDefinition' == $mgMapLayerIdentify) {
                                                        $featureLayerIds[] = $featureLayer->getId();
                                                    }
                                                }
                                            }
                                        }

                                    } else {
                                        if ($layer->isVisible()) {
                                            if ($layer->GetSelectable()) {
                                                $layerResourceId = $layer->GetLayerDefinition()->GetPath() . '/' . $layer->GetLayerDefinition()->GetName();
                                                if ('Library://' . $layerResourceId . '.LayerDefinition' == $mgMapLayerIdentify) {
                                                    $featureLayerIds[] = $featureLayer->getId();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByPolygon($coordsOfPolygon, $featureLayerIds);
            foreach ($objectList as $object) {
                $objectsArray[] = $object->get('feature_id');

                /*
                             $objectsArray[] = array(
                                'objectId'  => $object->get('object_id'),
                                'featureId' => $object->get('feature_id')
                            );
                 *
                 */
            }


            $this->response->write(json_encode(array_values($objectsArray)));
        }
    }
}

?>
