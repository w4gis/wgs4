<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
//lmb_require('wgs/model/wgsProjectRole.class.php');
//lmb_require('wgs/model/wgsProjectObject.class.php');
//lmb_require('wgs/model/wgsPermission.class.php');
//lmb_require('wgs/model/wgsPrivilege.class.php');
//lmb_require('wgs/model/wgsMgWebLayout.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class UnbindedObjController extends wgsFrontController
{    
    function doGetLayerTypeFeatureTypes() {
        $featureTypes = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListUnbindCardsFeatureType();
        $featureTypesArray = array();
        if ($featureTypes) {
            foreach($featureTypes as $featureType) {
                $featureTypesArray[] = array(
                    'featureTypeId' => $featureType['object_type_id'],
                    'featureTypeName' => lmb_win1251_to_utf8($featureType['name'])
                );
            }
        }
        $this->response->write(json_encode(array_values($featureTypesArray)));
    }
    
    function doGetLayerTypeUnbindDates() {
        $featureTypeId = $this->request->getPost('featureTypeId');
        if ($featureTypeId) {
            $unbindDates = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListUnbindCardsDateByType($featureTypeId);
            $unbindDatesArray = array();
            if ($unbindDates) {
                foreach($unbindDates as $unbindDate) {
                    $unbindDatesArray[] = array(
                        'unbindedDate' => $unbindDate['unbind_date']
                    );
                }
            }
            $this->response->write(json_encode(array_values($unbindDatesArray)));
        }
    }
    
    function doGetLayerTypeObjects() {
        $featureTypeId = $this->request->getPost('featureTypeId');
        $unbindedDate = $this->request->getPost('unbindedDate');
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 30;
        if ($featureTypeId && $unbindedDate) {
            $objects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByTypeUnbindDate($featureTypeId, $unbindedDate, $start, $limit, $totalCount);
            if (count($objects)) {
                $result = array();
                $result['success'] = true;
                $result['totalCount'] = $totalCount;
                foreach($objects as $value) {
                    $result['rows'][] = array(
                        'objectId' => $value->get("object_id"),
                        'name' => $value->get("name")? lmb_win1251_to_utf8($value->get("name")): lmb_win1251_to_utf8('<span style="color:#bbb">��� ������������</span>'),
                        'hide' => false
                    );
                }
            } else {
                $result['success'] = false;
                $result['totalCount'] = 0;
                $result['rows'] = array();
            }
            $this->response->write(json_encode($result));
        }
    }
    
    function doGetObjectCardObjects() {
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 30;

        $objects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListAllUnbindedObject($start, $limit, $totalCount);
        if (count($objects)) {
            $result = array();
            $result['success'] = true;
            $result['totalCount'] = $totalCount;
            foreach($objects as $value) {
                $result['rows'][] = array(
                    'objectId' => $value->get("object_id"),
                    'objectName' => $value->get("object_name")? lmb_win1251_to_utf8($value->get("object_name")): lmb_win1251_to_utf8('<span style="color:#bbb">��� ������������</span>'),
                    'objectTypeName' => lmb_win1251_to_utf8($value->get("object_type_name")),
                    'objectTypeId' => lmb_win1251_to_utf8($value->get("object_type_id")),
                    'unbindedDate' => $value->get("unbind_custom_date"),
                    'hide' => false
                );
            }
        } else {
            $result['success'] = false;
            $result['totalCount'] = 0;
            $result['rows'] = array();
        }
        $this->response->write(json_encode($result));
    }
    
    function doGetCardCardObjects() {
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 30;

        $objects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListAllOrphanObject($start, $limit, $totalCount);
        if (count($objects)) {
            $result = array();
            $result['success'] = true;
            $result['totalCount'] = $totalCount;
            foreach($objects as $value) {
                $result['rows'][] = array(
                    'objectId' => $value->get("object_id"),
                    'objectName' => $value->get("object_name")? lmb_win1251_to_utf8($value->get("object_name")): lmb_win1251_to_utf8('<span style="color:#bbb">��� ������������</span>'),
                    'objectTypeName' => lmb_win1251_to_utf8($value->get("object_type_name")),
                    'objectTypeId' => lmb_win1251_to_utf8($value->get("object_type_id")),
                    'hide' => false
                );
            }
        } else {
            $result['success'] = false;
            $result['totalCount'] = 0;
            $result['rows'] = array();
        }
        $this->response->write(json_encode($result));
    }
    
    function doRemoveAll() {
        $featureTypeId = $this->request->getPost('featureTypeId');
        $unbindedDate = $this->request->getPost('unbindedDate');
        if ($featureTypeId && $unbindedDate) {
            $this->wgsTier->getProjectObjectService()->getAttObjectService()->removeObjectsByTypeUnbindDate($featureTypeId, $unbindedDate);
        }
    }
}

?>