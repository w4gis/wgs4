<?php
//  ini_set('display_errors',1);
//  error_reporting(E_ALL);
lmb_require('wgs/controller/ObjectAdminController.class.php');
lmb_require('wgs/controller/wgsSOAPClient/wgsSOAPClientController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
require_once('wgs/lib/fpdf/textbox.php');
function compareSearchRes($a, $b) {
  // var_dump ($a['cardId'] - $b['cardId']);
  return $a['cardId'] - $b['cardId'] > 0 ? 1 : ($a['cardId'] - $b['cardId'] < 0 ? -1 : 0);
}

class CardController extends ObjectAdminController
{
    public $soapClient;

    function __construct() {
        parent::__construct();
    }

    function doDisplay()
    {
    }

    function doGetChildCardsByRelationIdAndName(){
        $id = $this -> request -> get('relationId');
        $name = decode(mb_substr($this -> request -> get('name'), 0, -1));
        if(mb_strlen($name) < 2) {
            $this -> response -> write(array());
            return;
        }
        if($id == null) {
            die('no id');
        }

        if(!$this -> soapClient) {
            try {
                $this -> soapClient = new wgsSOAPClientController();
            } catch (Exception $e) {
                ;
            }
        }
        // $objTypeRels = $this->doGetObjTypeRelationByRelationId($id);


        $objectIds = array();
        // foreach ($objTypeRels as $key => $objTypeRelation) {

        $cards = $this -> wgsTier -> getProjectObjectService() -> getAttObjectService() -> getObjectsByRelationIdAndName($id, $name);
        foreach ($cards as $key => $card) {
            if($card -> get('object_id') != null) {
                $objectId = $card -> get('object_id');
                if($objectId == null) {
                    continue;
                }
                $soapId = null;
                $additionalString = '';

                $advancedProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);
                if ($advancedProperties) {
                    foreach($advancedProperties as $advancedProperty) {
                        if( $advancedProperty -> get('property_id') != null ) {
                            if( encode($advancedProperty->get('property_name')) == 'soapId' ) {
                                $soapId = $advancedProperty->get('property_value');
                            }

                        }
                    }
                }

                if($soapId != null && $this -> soapClient != null) {
                    // $additionalString = "(" . $this -> soapClient -> getStudentProperty($soapId, 'sex'). ", ";
                    $additionalString = $this -> soapClient -> getStudentProperty($soapId, 'group');
                }

                $objectIds[] = array(
                    'id' => $card -> get('object_id'),
                    'cardId' => $card -> get('object_id'),
                    'name' => encode($card -> get('name')) . ($additionalString ? (', гр.' . $additionalString ) : ""),
                    'relation_id' => $card -> get('object_relation_id')
                );
            }
        }

        // }
        // die();
        uasort($objectIds, 'custom_sort_relations');

        $this -> response -> write(json_encode($objectIds));
    }

    function custom_sort_relations($a,$b) {
        return strcmp($a['name'], $b['name']);
    }

    // Get all cards by ObjectTypeId and FeatureLayerId
    function doGetCardsByTypeAndLayerId($layerId, $cardType, $rangeStart = 0, $rangeLimit = 1, $sortby = '', $filter, $onlyCount=false, $isContingent = false){
        // if($isContingent) {
        $foundRecords = array();

        if(!$this -> soapClient) {
            $this -> soapClient = new wgsSOAPClientController();
        }

        $filters = array_map('trim', explode(',', $filter));
        // var_dump($filters);
        foreach($filters as $i => $singleFilter) {
          if($i != 0 && $singleFilter == '') {
            continue;
          }
          // var_dump($singleFilter);
          unset($objectIds);
          unset($used);
          unset($cards);

          $cards = array();
          $objectIds = array();
          $used = array();
          // echo "stage 0 :: ";
          // var_dump(date_format(date_create('now'), 'H:i:s'));
          $soapIds = $this -> soapClient -> getStudentIdsByProperty(encode($singleFilter), 0, 10e6);
          // echo "stage 1 :: ";
          // var_dump(date_format(date_create('now'), 'H:i:s'));

          // $i = -1;
          if($soapIds['data'] && count($soapIds['data']) > 0) {

            // var_dump($rangeStart, $rangeLimit);
            // for($i = $rangeStart; $i < $rangeStart + $rangeLimit; $i++) {
              // var_dump($cardX = $this -> doGetCardsByLayersAndTypes(array(0=>$layerId), array(0=>$cardType), 0, 1, '7',  $soapId,  true));
              // $count += intval($cardX = $this -> doGetCardsByLayersAndTypes(array(0=>$layerId), array(0=>$cardType), 0, 1, '7',  $soapId,  true));
              // if(!$soapIds['data'][$i]) continue;
              // echo implode(',', $soapIds['data']);
              $cards = $this -> doGetCardsByLayersAndTypes(array(0=>$layerId), array(0=>$cardType), 0, 10e6, '7',  implode(",", $soapIds['data']),  false, true);
              // $cards[] = $cardX[0];
            // }
          } else {
            $cards = $this->doGetCardsByLayersAndTypes(array(0=>$layerId), array(0=>$cardType), 0, 10e6, $sortby,  $singleFilter,  false);
            // $count = count($cards);
          }

          // var_dump($cards);
          // echo "stage 2 :: ";
          // var_dump(date_format(date_create('now'), 'H:i:s'));

          foreach ($cards as $key => $card) {
            if(in_array($card['id'], $used)) {
              continue;
            }
            $used[] = $card['id'];
            $objectIds[] = array(
                'cardId' => $card['id'],
                'objectTypeId' => $card['objectTypeId']
            );
          }
          // var_dump($objectIds);

          // foreach ($cards2 as $key => $card) {
          //   if(in_array($card['id'], $used)) {
          //     continue;
          //   }
          //   $used[] = $card['id'];
          //   $objectIds[] = array(
          //       'cardId' => $card['id'],
          //       'objectTypeId' => $card['objectTypeId']
          //   );
          // }

          $foundRecords[] = $objectIds;
        }
        // die();
        // var_dump(count($foundRecords));
        // echo "copmlete :: ";
        // var_dump(date_format(date_create('now'), 'H:i:s'));

        $result = $foundRecords[0];
        foreach($foundRecords as $i => $searchRes) {
          if($i == 0) {
            continue;
          }

          $result = array_uintersect($result, $searchRes, 'compareSearchRes');
        }
        $count = count($result);
        // var_dump(array_slice($result, $rangeStart, $rangeLimit));

        // echo "done :: ";
        // var_dump(date_format(date_create('now'), 'H:i:s'));

        // var_dump($objectIds);
        // die();
        return array('total' => $count, 'result' => array_slice($result, $rangeStart, $rangeLimit));
    }


    //// ОДИНОЧНОЕ ЗАПОЛНЕНИЕ КОНЕЦ //////
    function doGetCardsByFeatureId($featureIds = null, $isObjectIds = false, $WgsVersion = 4) {
            $isRequest = false;

        //check if it is request
            if($featureIds == null) {
                $isRequest = true;
                $isObjectIds = false;
            }

            if($isRequest) {
                $WgsVersion = $this->request->getPost('WgsVersion');
            //check if it is ObjectIds (direct to cards), not the FeatureIds
                $isObjectIds = $this->request->getPost('isDirectIds');
                $featureIds=json_decode($this->request->getPost('featureIds'));
            }


        // use new or old controller
            if($WgsVersion >= 4) {
            //one or multiple features
                if(count($featureIds, COUNT_RECURSIVE) == 1 ) {
                    $arrayCard = $this->doGetCardDescriptionByFeatureId($featureIds, $isObjectIds);
                } elseif (count($featureIds, COUNT_RECURSIVE) > 1) {
                    $arrayCard = $this->doGetMultipleCardsByFeatureId($featureIds);
                }

                if($isRequest) {
                    $this->response->write(json_encode(array_values($arrayCard)));
                } else {
                    return $arrayCard;
                }
            } else {
                    $this->doGetCardsByFeatureIdOld();
                }
    }

    function doGetCardDescriptionByFeatureId($featureIds, $isObjectIds) {
            $cardsArray = array();

        //if arguments have featureIds - get object ids.
            if($isObjectIds) {
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($featureIds, 1);
                foreach ($objectTypesList as $objectType) {
                    if($objectType->get('object_type_id')) {
                        $cardsArray[] = array(
                           'objectTypeId' => $objectType->get('object_type_id'),
                           'objectId'  => $objectType->get('object_id'),
                           'cardId'  => $objectType->get('object_id')
                        );
                    }
                }
            } else {
                $cardsArray = $this -> doGetCardsArray($featureIds);
            }


        // get base props, prop values, relations & domains
            $valueBaseArray = $this -> doGetValueBasePropertiesOne($cardsArray);
            $valuePropertiesArray = $this -> doGetValuePropertiesOne($cardsArray);
            $relationList = $this -> doGetListRelByObjectOne($cardsArray);

            $resultCardsArray = array();
            $resultValueBaseArray = array();
            $resultRelationList = array();

        //filter avay relation cards
            foreach($cardsArray as $key => $card) {
                if(!$card['cardRelationName']) {
                    $resultCardsArray[] = $card;
                    $resultValueBaseArray[] = $valueBaseArray[$key];
                    $resultRelationList[] = $relationList[$key];
                }
            }

            $arrayCard = array(
                0 => $resultCardsArray,
                1 => $resultValueBaseArray,
                2 => $valuePropertiesArray,
                3 => $resultRelationList,
            );

            return $arrayCard;
    }

    function doGetMultipleCardsByFeatureId($featureIds) {
        //Получаем список карточек(doGetCards($featureIds))
            $objectsArray = array();
            $objectsArray = $this -> doGetCards($featureIds);


        //Получаем названия карточек(doGetObjectTypes($featureIds))
            // $layersWithTypes = array();
            // $layersWithTypes = $this -> doGetObjectTypes($featureIds);

        //Получаем группы и имена полей(doGetPropertiesByObjectType($objectTypeId))
            $propertyList = array();
            $objectTypeIds = array();

            foreach ($objectsArray as $value) {
                $found = false;
                foreach($objectTypeIds as $i => $oid) {
                    if($value['objectTypeId'] == $oid['id']) {
                        $found = true;
                        $objectTypeIds[$i]['count']++;
                        break;
                    }
                }

                if(!$found) {
                    $objectTypeIds[] = array('id' => $value['objectTypeId'], 'name'=>$value['objectTypeName'], 'count' => 1);
                }
            }

            for($i = 0; $i < count($objectTypeIds) - 1; $i++) {
                for($j = 0; $j < count($objectTypeIds) - $i - 1; $j++) {
                    if($objectTypeIds[$j]['id'] > $objectTypeIds[$j + 1]['id']) {
                        $tmp = $objectTypeIds[$j];
                        $objectTypeIds[$j] = $objectTypeIds[$j + 1];
                        $objectTypeIds[$j + 1] = $tmp;
                    }
                }
            }


            //var_dump($objectTypeId);
            for($i = 0; $i < count($objectTypeIds); $i++) {
                $propertyList[$i] = $this -> doGetPropertiesByObjectType($objectTypeIds[$i]['id']);
            }

            //Получаем значения основных характеристик и остальных групп характеристик (doGetBaseValueProperties ($objectIds)),doGetValueProperties($objectIds)

            $values = array();
            $valuePropertyList = array();

            for ($j = 0; $j < count($objectTypeIds); $j++) {
                $value = $objectTypeIds[$j];


                $objectIds = array();

                for($i = 0; $i<(count($objectsArray)); $i++) {
                    if($value['id'] == $objectsArray[$i]['objectTypeId']) {
                        $objectIds[$i] = $objectsArray[$i]['objectId'];
                    }
                }


                $values[$j] = $this -> doGetBaseValueProperties($objectIds);
                $values[$j]['objectTypeId'] = $objectTypeIds[$j]['id'];
                $values[$j]['objectTypeName'] = $objectTypeIds[$j]['name'];
                $values[$j]['cardCount'] = $objectTypeIds[$j]['count'];
                $valuePropertyList[$j] = $this -> doGetValuePropertiesMultiple($objectIds);

                // unset($objectIds);
                // $j++;
            }



        //Вывод результата множественного заполнения
            $arrayCards = array(
                0 => $values,
                1 => $propertyList,
                2 => $valuePropertyList
            );
            return $arrayCards;
            //$this->response->write(json_encode(array_values($arrayCards)));
    }


    /////////////////////////Функции множественного заполнения
    function doGetCards($featureIds)
    {
        //$objectIds = $this->request->getPost('objectIds');
        $objectsArray = array();
        if ($this->request->getPost('featureIds')) {
            //$featureIds = json_decode($this->request->getPost('featureIds'));
            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
                $objectsArray = array();
                foreach ($objectList as $object) {
                    if($object->get('featurelayer_id')) {
                        $objectsArray[] = array (
                            'featureLayerId'    => $object->get('featurelayer_id'),
                            'objectId'  => $object->get('object_id'),
                            'objectTypeId'  => $object->get('object_type_id'),
                            'objectTypeName'  => encode($object->get('object_type_name'))
                        );
                    }
                }
        } else {

            if ($this->request->getPost('objectIds')) {
                $objectIds = $this->request->getPost('objectIds');
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds, 1);
                foreach ($objectTypesList as $objectType) {
                     if($objectType->get('object_type_id')) {
                         $objectsArray[] = array(
                            'objectTypeId' => $objectType->get('object_type_id'),
                            'objectId'  => $objectType->get('object_id'),
                            'objectTypeName'  => encode($object->get('object_type_name'))
                         );
                     }
                 }
            }
        }
        //$this->response->write(json_encode(array_values($objectsArray)));
    //   var_dump($objectsArray);
       return $objectsArray;

    }

     function doGetObjectTypes($featureIds)
    {
        if($this->request->getPost('featureIds')) {
        // if($featureIds) {
            // $featureIds =json_decode($this->request->getPost('featureIds'));
            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
            $objectsArray = array();
            foreach ($objectList as $object) {
                if($object->get('featurelayer_id')) {
                    $objectsArray[] = array (
                        'featureLayerId'    => $object->get('featurelayer_id'),
                        'featureLayerName'  => encode($object->get('featurelayer_name')),
                        'objectTypeId'  => $object->get('object_type_id'),
                        'objectTypeName'  => encode($object->get('object_type_name')),
                        'objectId'  => $object->get('object_id')
                    );
                }
            }
            $layers = array();
            $uniqLayers = array();
            for ($i = 0; $i < count($objectsArray); $i++) {
                $layers[] = $objectsArray[$i]['featureLayerId'];
            }
            $uniqLayers = array_unique($layers);

            $layersWithTypes = array();

            foreach ($uniqLayers as $uniqLayer) {
                $featuresCount = 0;
                foreach ($objectsArray as $object) {
                    if($object['featureLayerId'] == $uniqLayer) {
                        $layerLabel = $object['featureLayerName'];
                        $featuresCount++;
                    }
                }
                $layersWithTypes[] = array(
                    'name'              => $layerLabel.' ('.'карточек'.' : '.$featuresCount.' )',
                    'label'             => $layerLabel.' ('.'карточек'.' : '.$featuresCount.' )',
                    'featureLayerId'    => -1,
                    'objectTypeId'      => -1
                );
                $featureTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($uniqLayer);
                foreach ($featureTypesList as $featureType) {
                    if($featureType->get('is_current')) {
                        $layersWithTypes[] = array(
                                'name'              => '<div style="padding-left:15px; font-size:8pt;">'.encode($featureType->get('Name')).' (основное)'.'</div>',
                                'label'             => encode($featureType->get('Name')).' (основное)',
                                'featureLayerId'    => $uniqLayer,
                                'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                    } else {
                        $layersWithTypes[] = array(
                                'name'              => '<div style="padding-left:15px; font-size:8pt;">'.encode($featureType->get('Name')).'</div>',
                                'label'             => encode($featureType->get('Name')),
                                'featureLayerId'    => $uniqLayer,
                                'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                    }
            // var_dump($layersWithTypes);
            //     die();
                }
            }
        } else {
            $layersWithTypes = array();
            if($this->request->getPost('objectIds')) {
                $objectIds = $this->request->getPost('objectIds');
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds,0);
                foreach ($objectTypesList as $objectType) {
                     $layersWithTypes[] = array(
                        'label'=> encode($objectType->get('type_name')),
                        'name' => encode($objectType->get('type_name')),
                        'objectTypeId' => $objectType->get('object_type_id')
                     );
                 }
            }
        }

        return $layersWithTypes;
        // $this->response->write(json_encode(array_values($layersWithTypes)));
    }


    function doGetPropertiesByObjectType($objectTypeId)
    {
        //$objectTypeId = $this->request->getPost('objectTypeId');
        if ($objectTypeId) {
            $properties = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($objectTypeId, null);
            $propertyList = array();

            foreach($properties as $property)
            {
                if($property->get('Property_Id')) {
                    $tmp = array(
                                'id'            => $property->get('Property_Id'),
                                'name'          => encode($property->get('Name')),
                                'valueTypeId'   => $property->get('Value_properties_type_id'),
                                'groupId'       => $property->get('Properties_group_id'),
                                'groupName'     => encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($property->get('Properties_group_id')))
                    );
                    if($tmp['valueTypeId'] == 5){
                        $tmp['domain'] = $this->doGetValueDomainListOne($tmp['id']);
                    }
                    $propertyList[] = $tmp;
                }
            }
            //$this->response->write(json_encode(array_values($propertyList)));
            return $propertyList;
        }
    }


    function doGetBaseValueProperties ($objectIds)
    {
        //$objectIds = $this->request->getPost('objectIds');
        $names = array();
        $shortNames = array();
        $notes = array();
        foreach ($objectIds as $objectId) {
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){
                $names[] = encode($baseProperty->get('name'));
                $shortNames[] = encode($baseProperty->get('short_name'));
                $notes[] = encode($baseProperty->get('note'));
            }
        }
        $name = ((count(array_unique($names))==1))? $names[0]: '<...>';
        //var_dump($names);
        $shortName = ((count(array_unique($shortNames))==1))? $shortNames[0]: '<...>';
        $note = ((count(array_unique($notes))==1))?$notes[0]: '<...>';


        $values = array(
                'name' => $name,
                'shortName' => $shortName,
                'note' => $note
        );
        return $values;
        // $this->response->write(json_encode(array_values($values)));
    }


    function doGetValuePropertiesMultiple($objectIds)
    {
	//var_dump('test');
        // $objectIds = $this->request->getPost('objectIds');
        $propertyList = array();
        if ($objectIds != null) {

            $properties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValuePropertiesByObjects($objectIds);
            foreach ($properties as $property) {
                $propertyList[] = array (
                    'propertyId' => $property->get('property_id'),
                    'propertyValue' => encode($property->get('property_value'))
                );
            }

            // $this->response->write(json_encode(array_values($propertyList)));
            $valuePropertyList = $propertyList;
            //var_dump($valuePropertyList);
            return $valuePropertyList;
        }
    }


    function doSetMultipleCards()
    {
        //$objectIds = $this->request->getPost('objectIds');
        //$propertyIds = $this->request->getPost('propIds');
        //$propertyValues = $this->request->getPost('propValues');
        //$name = lmb_utf8_to_win1251($this->request->getPost('name'));
        //$shortName = lmb_utf8_to_win1251($this->request->getPost('shortName'));
        //$note = lmb_utf8_to_win1251($this->request->getPost('note'));

        $featureIds = json_decode($this->request->getPost('featureIds'));
        $objectTypeId = $this->request->getPost('id');

        $post = $this->request->getPost();
        $properties = array();
        foreach ($post as $propertyId => $propertyValue) {
            if($propertyId != 'featureIds' && $propertyId != 'id') {
                $properties[ strstr($propertyId,"property") ? explode("-", $propertyId)[1] : $propertyId ] = $propertyValue;
            }
        }

        // var_dump($properties);
        // die();
        //Получаем список карточек(doGetCards($featureIds))
            //$objectsArray = array();
        $objectsArray = $this -> doGetCards($featureIds);
        foreach ($objectsArray as $value) {
            if($value['objectTypeId'] == $objectTypeId) {
                $this->doSetPropertiesByObjectId($value['objectId'], $properties);
            }
        }
    }


    function doSetPropertiesByObjectId($objectId,$properties)
    {
        foreach ($properties as $propertyId => $propertyValue) {
            if($propertyValue == '<...>') {
                continue;
            }
            if ($propertyId == 'name') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetName($objectId,decode($propertyValue));
            }
            if ($propertyId == 'shortName') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetShortName($objectId,decode($propertyValue));
            }
            if ($propertyId == 'note') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetNote($objectId,decode($propertyValue));
            } else {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($objectId,$propertyId,decode($propertyValue));
            }
        }
    }
    ////////////////////////Конец множественного заполнения////////////////////////////////////////////////////////////////////////////////////////


//// ОДИНОЧНОЕ ЗАПОЛНЕНИЕ //////
    function doGetCardsArray($featureIds)
    {

            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($featureIds[0]);
            $user = $this->wgsTier->getAuthorizedUser();
            $User = $user->getId();
            $Host = $_SERVER['REMOTE_ADDR'];

            $i = 1;
            foreach ($cardList as $value) {
                if($value->get('Name') != ''){
                    $cardName = encode($value->get('Name')).' - '.encode($value->get('object_type_name'));
                    $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','card','view_card');
                    if($liId){
                       $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_view',encode($cardName));
                       //$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'featurelayer',lmb_utf8_to_win1251($on));
                    }
               } else {
                    $cardName = 'Без наименования - '.encode($value->get('object_type_name'));
               }


                $objectId = $value->get('Object_id');
                if($objectId != null) {
                    $cardsArray[] = array(//<li style="list-style:square;">
                        'cardName' => $cardName,
                        'cardId' => $objectId,
                        'objectTypeId'  => $value->get('object_type_id'),
                        'cardIsCurrent' => $value->get('is_current'),
                        'cardTypeName'  => encode($value->get('object_type_name')),
                        'cardRelationName' => '',
                        'cardLabel' => $cardName
                    );
                }


                $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
                foreach ($childObjects as $childObject) {
                    if($childObject->get('object_id') != null) {

                        if($childObject->get('name') != '') {
                            $cardName = encode($childObject->get('name')).' - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                        }
                        else {
                            $cardName = 'Без наименования - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                        }

                        $cardsArray[] = array(
                            'cardId'            => $childObject->get('object_id'),
                            'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                            'objectTypeId'      => $childObject->get('object_type_id'),
                            'cardIsCurrent'     => 0,
                            'cardTypeName'      => encode($childObject->get('type_name')),
                            'cardRelationName'  => encode($childObject->get('relation_name')),
                            'cardLabel'         => $cardName,
                            'isChild'           => true
                        );
                    }
                }
            }
        return $cardsArray;
    }

    function doGetValueBasePropertiesOne($cardsArray)
    {
        $valueArray = array();
        foreach ($cardsArray as $value) {
            $objectId = $value['cardId'];
            $objectTypeId = $value['objectTypeId'];

            // if($value['isChild']) continue;

            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){

                if($baseProperty->get('object_id') != null) {
                    $valueArray[] = array(
                        'id'            => $baseProperty->get('object_id'),
                        'objectTypeId'  => $objectTypeId,
                        'name'          => encode($baseProperty->get('name')),
                        'shortName'     => encode($baseProperty->get('short_name')),
                        'typeName'      => encode($baseProperty->get('type_name')),
                        'createDate'    => $baseProperty->get('create_date'),
                        'edit_user'     => encode($baseProperty->get('edit_user')),
                        'note'          => encode($baseProperty->get('note'))
                    );
                }
            }

        }
            // var_dump($valueArray);
            // die();

       return $valueArray;
    }

    function getSoapIds($cardArray) {
        $resultArray = array();

        foreach($cardArray as $value){

            $objectId = $value['cardId'];

            if($objectId == null) {
                 continue;
            }

           $advancedProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);
           if ($advancedProperties) {
               foreach($advancedProperties as $advancedProperty) {
                   if( $advancedProperty -> get('property_id') != null ) {
                       if( encode($advancedProperty->get('property_name')) == 'soapId' ) {
                           $resultArray[] = array('soapId' => $advancedProperty->get('property_value'), 'objectId' => $objectId);
                       }
                   }
               }
           }
        }

        return $resultArray;
    }

    function doGetValuePropertiesOne($cardsArray){
        $soapKeys = array();
        //  $soapKeys = array(
        //     array(
        //         'name' => 'sex',
        //         'label' => 'Пол'
        //     ),

        //     array(
        //         'name' => 'group',
        //         'label' => 'Учебная группа'
        //     ),

        //     array(
        //         'name' => 'faculty',
        //         'label' => 'Факультет'
        //     ),

        //     array(
        //         'name' => 'army',
        //         'label' => 'Военная обязанность'
        //     ),

        //     // array(
        //     //     'name' => 'citizenship',
        //     //     'label' => 'Гражданство'
        //     // ),

        //     array(
        //         'name' => 'firstName',
        //         'label' => 'Имя'
        //     ),

        //     array(
        //         'name' => 'lastName',
        //         'label' => 'Фамилия'
        //     ),

        //     array(
        //         'name' => 'middleName',
        //         'label' => 'Отчество'
        //     ),

        //     array(
        //         'name' => 'birth',
        //         'label' => 'Дата рождения'
        //     )
        //  );

         $propertyList = array();


         foreach($cardsArray as $value){
             $objectId = $value['cardId'];
             if($objectId == null || $value['isChild']) {
                  continue;
             }

		    $advancedProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);
			if ($advancedProperties) {

				$propertiesArray = array();
				$soapId = null;
				foreach($advancedProperties as $advancedProperty) {
                    if( $advancedProperty -> get('property_id') != null ) {
                        if( encode($advancedProperty->get('property_name')) == 'soapId' ) {
                            $soapId = $advancedProperty->get('property_value');
                        }

                    }
				}

        $cont_allowed = false;
        if (($user = $this->wgsTier->getAuthorizedUser()) && ($acl = $this->wgsTier->getACL())) {
	        if ($sysPrivileges = $this->wgsTier->getSysPrivileges()) {
	            if (is_array($sysPrivileges)) {
	                foreach ($sysPrivileges as $sysPrivilege) {
	                    if ($sysPrivilege-> getName() == "CONTINGENT" && $acl->isAllowed($user, $sysPrivilege, wgsPrivilege::SYSPRIVILEGE_ACCESS)) {
	                        $cont_allowed = true;
	                    }
	                }
	            }
	        }
        }

				foreach($advancedProperties as $advancedProperty) {
					if( $advancedProperty -> get('property_id') != null ) {
						$propertyType = $advancedProperty->get('value_properties_type_id');
						if( $propertyType == 8/* soap */) {
              if(!$cont_allowed) {
                $value = '';

              } else {
						    $valueField = null;
						    $value = '';

						        if($advancedProperty->get('value_descriptor') != null) {
                                $valueField = $advancedProperty->get('value_descriptor');
                            } else {
    						    for($i = 0; $i < count($soapKeys); $i++) {

    						        if($soapKeys[$i]['label'] == encode($advancedProperty->get('property_name'))) {
    						            $valueField = $soapKeys[$i]['name'];
    						            break;
    						        }
    						    }
                            }

						    if($valueField != null) {
						        if($this -> soapClient == null) {
						            try {
                                        $this -> soapClient = new wgsSOAPClientController();
                                    } catch(Exception $e) {
                                    //   var_dump($e);
                                        throw $e;
                                    }
						        }
						        $value = $this -> soapClient -> getStudentProperty($soapId, $valueField);
						    }

              }
						} else {
						    $value = encode($advancedProperty->get('property_value'));
						}
						$temporaryArray = array(
							'id'            => $advancedProperty->get('property_id'),
							'name'          => encode($advancedProperty->get('property_name')),
							'valueTypeId'   => $advancedProperty->get('value_properties_type_id'),
							'groupId'       => $advancedProperty->get('property_group_id'),
							'groupName'     => encode($advancedProperty->get('property_group_name')),
							'value'         => $value,
							'valueDescriptor' => $advancedProperty->get('value_descriptor')
						);


                        if($advancedProperty->get('value_properties_type_id') == 5){
                            $temporaryArray['domain']= $this->doGetValueDomainListOne($advancedProperty->get('property_id'));
                        }
                        $propertiesArray[] = $temporaryArray;
					}
				}
			}
			$propertyList[] = $propertiesArray;
		}
        return $propertyList;
    }

    /*function doGetListRelByObject()
    {
        $objectId = $this->request->getPost('objectId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelByObject($objectId);
        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['relationId'] = $object->get('relation_id');
            $objectList[$count]['name'] = encode($object->get('name'));
            $objectList[$count]['relationTypeId'] = $object->get('relation_type_id');
            $objectList[$count]['countTypeRelation'] = $object->get('count_type_relation');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }*/

    function doGetListRelByObjectOne($cardsArray)
    {
        $relationCardList = array();
        foreach ($cardsArray as $key => $value) {
            $objectId = $value['cardId'];
            if($objectId == null || $value['objectTypeId'] == null ) {
                continue;
            }
            // $relations = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelByObject($objectId);
            $relationList = array();
            $relationTypes = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByParent($value['objectTypeId']);
            foreach($relationTypes as $relation) {
                if($relation->get('relation_id') != null) {

                    if(!$relationList[$relation->get('relation_id')]) {
                        $relationList[$relation->get('relation_id')] = array(
                            'relationId'        => $relation->get('relation_id'),
                            'name'              => encode($relation->get('relation_name')),
                            'relationTypeId'    => $relation->get('relation_type_id'),
                            'objTypes' => array(array(
                                                        'objTypeRelationId' => $relation->get('objtype_relation_id'),
                                                        'id'      => $relation->get('child_object_type_id'),
                                                        'name'    => encode($relation->get('child_name'))
                                                        )),
                            // 'countTypeRelation' => $relation->get('count_type_relation'),
                            'linkedCards'       => $this -> doGetListChildObjectByOneRelation($relation->get('relation_id'), $objectId)
                        );
                    } else {
                        $relationList[$relation->get('relation_id')]['objTypes'][] = array(
                                                        'objTypeRelationId' => $relation->get('objtype_relation_id'),
                                                        'id'      => $relation->get('child_object_type_id'),
                                                        'name'    => encode($relation->get('child_name'))
                                                        );
                    }

                }
            }
            $relationCardList[$key] = $relationList;
        }

         // var_dump($relationCardList);
         // die();
        return $relationCardList;
    }

    function doGetListChildObjectByOneRelation($relation, $objectId)
    {

        $cardsByRelation = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectByRelation($relation, $objectId);
        $cardsList = array();
        if($cardsByRelation) {
            // $cardChildArray=$cardsByRelation;
            foreach ($cardsByRelation as $cardByRelation) {
                if($cardByRelation->get('object_id') != null) {

                    $cardsList[] = array(
                        'id'                => $cardByRelation->get('object_id'),
                        'name'              => encode($cardByRelation->get('name')),
                        'shortName'         => encode($cardByRelation->get('short_name')),
                        'typeid'            => $cardByRelation->get('object_type_id'),
                        'typeName'          => encode($cardByRelation->get('object_type_name')),
                        // 'invNumber'         => $cardByRelation->get('INVENTARY_NUMBER'),
                        'createDate'        => $cardByRelation->get('create_date'),
                        'note'              => encode($cardByRelation->get('note')),
                        'author'            => encode($cardByRelation->get('author')),
                        'label'             => encode($cardByRelation->get('label')),
                        'ObjTypeRelationId' => $cardByRelation->get('objtype_relation_id'),
                        'objRelationId'     => $cardByRelation->get('object_relation_id')
                    );
                }
            }


        }

        return $cardsList;
    }

    function doGetValueDomainListOne($propId)
    {        $valueDomain=array();
                    $valueDomainArray=array();
                    $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueDomain($propId);
                        foreach($result as $object) {
                            $valueDomain[] = array(
                                'Value_Domain_ID'=>$object->get('Value_Domain_ID'),
                                'Value_Domain'=>encode($object->get('Value_Domain')),
                            );
                        }
        return $valueDomain;
    }

    function removeObjectNew($objectId, $removeMode) {
        $this->wgsTier->getProjectObjectService()->getAttObjectService()->Remove($objectId, $removeMode);
    }

    function doRemoveObjectNew()
    {
        $objectId = $this->request->getPost('objectId');
        $removeMode = $this->request->getPost('removeMode');
        $this->wgsTier->getProjectObjectService()->getAttObjectService()->Remove($objectId, $removeMode);
        //$this->response->write($resp);
    }

    function doCreateChildCard() {
        $objTypeRelationId = $this->request->getPost('objTypeRelationId');
        $parentObjectId = $this->request->getPost('parentId');
        $relationId = $this->request->getPost('relationId');

        $objTypeRels = $this->doGetObjTypeRelationByRelationId($relationId);
        $objectTypeId = null;
        foreach ($objTypeRels as $key => $objRel) {
            if($objRel['objTypeRelationId'] == $objTypeRelationId)
                $objectTypeId = $objRel['childObjectTypeId'];
        }
        if($objectTypeId != null) {
            $relId = $this->doCreateChildObject($objTypeRelationId, $parentObjectId, $objectTypeId);
        }
        $this->response->write($relId);
    }

    ////////    ФУНКЦИИ WGS3, НЕ ТРОГАТЬ!

    function doGetCardsByFeatureIdOld() {
            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($this->request->getPost('featureId'));
            $user = $this->wgsTier->getAuthorizedUser();
            $User = $user->getId();
            $Host = $_SERVER['REMOTE_ADDR'];
            $i = 1;
            foreach ($cardList as $value) {
                if($value->get('Name') != '') {

                   $cardName = encode($value->get('Name')).' - '.encode($value->get('object_type_name'));
                   $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','card','view_card');
                   if($liId){
                        $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_view',lmb_utf8_to_win1251($cardName));
                        //$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'featurelayer',lmb_utf8_to_win1251($on));
                   }
             } else {
                  $cardName = 'Без наименования - '.encode($value->get('object_type_name'));
             }

             $objectId = $value->get('Object_id');
             $cardsArray[] = array(//<li style="list-style:square;">
                 'cardName' => $cardName,
                 'cardId' => $objectId,
                 'cardIsCurrent' => $value->get('is_current'),
                 'cardTypeName'  => encode($value->get('object_type_name')),
                 'cardRelationName' => '',
                 'cardLabel' => $cardName
             );

            //var_dump($cardsArray);
            //die();
             $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
             foreach ($childObjects as $childObject) {
                 if($childObject->get('name') != '') {
                      $cardName = encode($childObject->get('name')).' - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                 } else {
                      $cardName = 'Без наименования - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                 }

                 $cardsArray[] = array(
                     'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                     'cardId'            => $childObject->get('object_id'),
                     'cardIsCurrent'     => 0,
                     'cardTypeName'      => encode($childObject->get('type_name')),
                     'cardRelationName'  => encode($childObject->get('relation_name')),
                     'cardLabel'         => $cardName
                 );
              }
          }

          $this->response->write(json_encode(array_values($cardsArray)));
    }


    function doGetValueBaseProperties($cardsArray)
    {
        $objectId = $this->request->getPost('objectId');
        $valueArray = array();
        //foreach ($cardsArray as $value) {
            //$objectId=$value['objectId'];

            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){

                if($baseProperty->get('object_id')) {
                    $valueArray[] = array(
                        'id'            => $baseProperty->get('object_id'),
                        'name'          => encode($baseProperty->get('name')),
                        'shortName'     => encode($baseProperty->get('short_name')),
                        'typeName'      => encode($baseProperty->get('type_name')),
                        'createDate'    => $baseProperty->get('create_date'),
                        'edit_user'     => encode($baseProperty->get('edit_user')),
                        'note'          => encode($baseProperty->get('note'))
                    );
                }
            }

        //}
         $this->response->write(json_encode(array_values($valueArray)));
       //return $valueArray;
    }

    function doGetCardsByObjectId()
    {
        $objectId = $this->request->getPost('objectId');
        $childObjects = array();
        if(isset($objectId)) {
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){
                if($baseProperty->get('name') != '')
                    $cardName = encode($baseProperty->get('name')).' - '.encode($baseProperty->get('type_name'));
                else
                    $cardName = 'Без наименования - '.encode($baseProperty->get('type_name'));

                $valueArray[] = array(
                    'cardName'          => $cardName,
                    'cardId'            => $baseProperty->get('object_id'),
                    'cardIsCurrent'     => 0,
                    'cardTypeName'      => encode($baseProperty->get('name')),
                    'cardRelationName'  => '',
                    'cardLabel'         => $cardName
                    );
            }
            $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
            foreach ($childObjects as $childObject) {
                if($childObject->get('name') != '')
                    $cardName = encode($childObject->get('name')).' - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                else
                    $cardName = 'Без наименования - '.encode($childObject->get('type_name')).' ('.encode($childObject->get('relation_name')).')';
                $valueArray[] = array(
                    'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                    'cardId'            => $childObject->get('object_id'),
                    'cardIsCurrent'     => 0,
                    'cardTypeName'      => encode($childObject->get('type_name')),
                    'cardRelationName'  => encode($childObject->get('relation_name')),
                    'cardLabel'         => $cardName
                    );
            }
            $this->response->write(json_encode(array_values($valueArray)));
        }
    }

    function doGetEntityIdByObject()
    {
        $objectId = $this->request->getPost('id');
        if (isset($objectId)) {
            $entityId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getEntityIdByObject($objectId);
            echo $entityId;
        }
    }

    function doGetEntityIdByFeature()
    {

        $featureId = $this->request->getPost('id');
        if (isset($featureId)) {
            $entityId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getEntityIdByFeature($featureId);
            echo $entityId;
        }
    }


	/**/
    function doPrint()
    {
        $objectId = json_decode($this->request->get('objectId'));
        if($objectId) {
            $seq = rand();
            $pdf = new PDF();
            $pdf->Open();
            $pdf->SetMargins(15, 10, 15);
            $pdf->AddFont('Times','','times.php');
            $pdf->AddFont('TimesB','','timesbd.php');
            $this->previewCard($objectId, $pdf);
            $fileName = WGS3_PRINTCACHE_DIR . "card-$seq.pdf";

            $pdf -> output($fileName);
            ob_clean(); //clean buffers before writing file to output

            $buf = @file_get_contents($fileName);
            $len = strlen($buf);
            $this->response->header("Content-type: application/pdf");
            $this->response->header("Content-Length: $len");
            $this->response->header("Content-Disposition: inline; filename=card-$seq.pdf");
            $this->response->write($buf);
        }
    }

    function previewCard($objectId, $pdf)
    {
        $pdf->addPage();
        $this->addBaseProperties($objectId, $pdf);
        $this->addAdvancedProperties($objectId, $pdf);
        $this->addChildCards($objectId, $pdf);
    }

    function doCreateFont()
    {
        lmb_require(FPDF_FONTPATH. "makefont/makefont.php");
        MakeFont('D:/fonts/verdana.ttf','D:/fonts/verdana.afm','cp1251');
        MakeFont('D:/fonts/verdanab.ttf','D:/fonts/verdanab.afm','cp1251');
        MakeFont('D:/fonts/verdanai.ttf','D:/fonts/verdanai.afm','cp1251');
        MakeFont('D:/fonts/verdanaz.ttf','D:/fonts/verdanaz.afm','cp1251');
    }

    function addHeader($pdf, $title)
    {
        $pdf-> SetFont('TimesB','',12);
        //$pdf->Cell(0, 15, $title, 1, 1, 'C');
        $pdf->MultiCell(0, 10, $title, 0, 'C' ,0);
    }

    function addBaseProperties($objectId, $pdf)
    {
        $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
           foreach ($baseProperties as $baseProperty){
            $valueArray[] = array(
                'id'            => $baseProperty->get('object_id'),
                'name'          => decode($baseProperty->get('name')),
                'shortName'     => decode($baseProperty->get('short_name')),
                'typeName'      => decode($baseProperty->get('type_name')),
                'createDate'    => $baseProperty->get('create_date'),
                'note'          => decode($baseProperty->get('note'))
                );
        }

        if(isset($valueArray[0]['name'])) {
            $title = $valueArray[0]['name'].' - '.$valueArray[0]['typeName'];
        } else {
            $title = decode(' Атрибутивное описание: ').$valueArray[0]['typeName'];
        }
        $this->addHeader($pdf, $title);

        $pdf->SetFillColor(230);
        $pdf->SetFont('TimesB','',11);
        $pdf->Cell(0, 7, decode(' Основные характеристики'), 1, 1, 'L', 1);

        $pdf->SetFillColor(250);
        $pdf->SetFont('Times','',10);

        $this->createRow($pdf,decode(' Наименование'), decode(' ').$valueArray[0]['name']);
        $this->createRow($pdf,decode(' Краткое наименование'),decode(' ').$valueArray[0]['shortName']);
        $this->createRow($pdf,decode(' Атрибутиваное описание'),decode(' ').$valueArray[0]['createDate']);
        $this->createRow($pdf,decode(' Примечание'),decode(' ').$valueArray[0]['note']);

        $pdf->Cell(0, 5, '', 0, 1, 'L', 0);
    }

    function addAdvancedProperties($objectId, $pdf)
    {



        $advancedProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);
        if ($advancedProperties) {
            $propertiesArray = array();
            $groupArray = array();
            $groupArrayUniq = array();

            $soapId = null; //get contingent ID
            foreach($advancedProperties as $advancedProperty) {
                if( $advancedProperty -> get('property_id') != null ) {
                    if( encode($advancedProperty->get('property_name')) == 'soapId' ) {
                        $soapId = $advancedProperty->get('property_value');
                    }

                }
            }

            foreach($advancedProperties as $advancedProperty) {

                $propertyType = $advancedProperty->get('value_properties_type_id');
                if( $propertyType == 8/* soap */) {
                    $valueField = null;
                    $value = '';
                    if($advancedProperty->get('value_descriptor') != null) {
                        $valueField = $advancedProperty->get('value_descriptor');
                    } else {
                        for($i = 0; $i < count($soapKeys); $i++) {

                            if($soapKeys[$i]['label'] == encode($advancedProperty->get('property_name'))) {
                                $valueField = $soapKeys[$i]['name'];
                                break;
                            }
                        }
                    }

                    if($valueField != null) {
                        if($this -> soapClient == null) {
                            try {
                                $this -> soapClient = new wgsSOAPClientController();
                            } catch(Exception $e) {
                            //   var_dump($e);
                                throw $e;
                            }
                        }
                        $value = $this -> soapClient -> getStudentProperty($soapId, $valueField);
                    }


                } else {
                    $value = encode($advancedProperty->get('property_value'));
                }

                $propertiesArray[] = array(
                    'id'            => $advancedProperty->get('property_id'),
                    'name'          => decode($advancedProperty->get('property_name')),
                    'valueTypeId'   => $advancedProperty->get('value_properties_type_id'),
                    'groupId'       => $advancedProperty->get('property_group_id'),
                    'groupName'     => decode($advancedProperty->get('property_group_name')),
                    'value'         => decode($value)
                    );
                $groupArray[] = decode($advancedProperty->get('property_group_name'));
            }

            if(count($propertiesArray) > 0) {
                $groupArrayUniq = array_unique($groupArray);
                $valueDomainArray = array();
                foreach ($groupArrayUniq as $group) {
                    $pdf->SetFillColor(230);
                    $pdf->SetFont('TimesB','',11);
                    $pdf->MultiCell(0, 7,decode(' Группа характеристик : ').decode($group), 1, 'L', 1);
                    $pdf->SetFillColor(250);
                    $pdf->SetFont('Times','',10);
                    foreach ($propertiesArray as $property) {
                        $name = decode(' ').$property['name'];
                        if ($property['valueTypeId'] == 5) {
                            $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueDomain($property['id']);
                            foreach($result as $object) {
                                $valueDomainArray[$object->get('Value_Domain_ID')] = $object->get('Value_Domain');
                            }
                            $value = $valueDomainArray[$property['value']];
                        } else {
                            $value = $property['value'];
                        }
                        $value = ' '.$value;
                        if($property['groupName']== $group) {
                         $this->createRow($pdf, $name, $value);
                     }
                 }
             }
         }
         $pdf->Cell(0, 5, '', 0, 1, 'L', 0);
     }
    }

 function addChildCards($objectId, $pdf)
{
    $relations = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelByObject($objectId);

    if ($relations) {
        $relationList = array();
        foreach($relations as $relation)
        {
            $relationList[] = array(
                'relationId'        => $relation->get('relation_id'),
                'name'              => decode($relation->get('name')),
                'relationTypeId'    => $relation->get('relation_type_id'),
                'countTypeRelation' => $relation->get('count_type_relation')
                );
        }
        foreach ($relationList as $relation) {
            $pdf->SetFillColor(230);
            $pdf->SetFont('TimesB','',11);
            //$pdf->MultiCell(0, 7, ' Г‘ГўГїГ§Гј : '.$relation['name'], 1, 'L', 1);
            $pdf->MultiCell(0, 7,$relation['name'], 1, 'L', 1);
            $cardsByRelation = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectByRelation($relation['relationId'], $objectId);
            if($cardsByRelation) {
                $cardsList = array();
                $pdf->SetFillColor(250);
                $pdf->SetFont('Times','',10);
                foreach ($cardsByRelation as $cardByRelation) {
                    $cardsList[] = array(
                        'id'                => $cardByRelation->get('OBJECT_ID'),
                        'name'              => decode($cardByRelation->get('NAME')),
                        'shortName'         => decode($cardByRelation->get('SHORT_NAME')),
                        'typeid'            => $cardByRelation->get('OBJECT_TYPE_ID'),
                        'typeName'          => decode($cardByRelation->get('object_type_name')),
                        'invNumber'         => $cardByRelation->get('INVENTARY_NUMBER'),
                        'createDate'        => $cardByRelation->get('CREATE_DATE'),
                        'note'              => decode($cardByRelation->get('NOTE')),
                        'author'            => decode($cardByRelation->get('AUTHOR')),
                        'label'             => decode($cardByRelation->get('LABEL')),
                        'ObjTypeRelationId' => $cardByRelation->get('ObjType_relation_id'),
                        'objRelationId'     => $cardByRelation->get('Object_relation_id')
                        );
                }

            foreach ($cardsList as $card) {
            $name = ' '.$card['name'];
            $value = ' '.$card['typeName'];

            $this->createRow($pdf, $name, $value);
            }
            }
            $pdf->Cell(0, 5, '', 0, 1, 'L', 0);
        }
    }
}

function createRow($pdf, $name, $value)
{
  $height = 5;
  $width = 180;
  $nameCellWidth = 70;
  $valueCellWidth = $width-$nameCellWidth;
  $marginLeft = 15;

  $y = $pdf->getY();

  $nameWidth = round($pdf->GetStringWidth($name));
  $valueWidth = round($pdf->GetStringWidth($value));
  $kn = ($nameWidth >= $nameCellWidth) ? floor(($nameCellWidth + $nameWidth) / $nameCellWidth) : 1;
  $kv = ($valueWidth >= $valueCellWidth) ? floor(($valueCellWidth + $valueWidth) / $valueCellWidth) : 1;

  $k = max($kn, $kv);

  $pdf->drawTextBox($name, $nameCellWidth, $height*$k, 'L', 'M');

  if ($pdf->getY() < $y) {
     $y = 10;
     $pdf->Rect($marginLeft, 10, $nameCellWidth, 10);
 }

 $pdf->setXY($nameCellWidth+$marginLeft, $y);

 if ($kn > $kv) {
     $pdf->MultiCell($valueCellWidth, $height*$k, $value, 1, 'J', 0);
 } else {
     $pdf->MultiCell($valueCellWidth, $height, $value, 1, 'J', 0);
 }
}

    /*

    function createRow($p, $rows, $colls, $data)
    {
        $x0 = 45;
        $y0 = 760;
        $width = 510;
        $height = 680;
        $heightCell = 20;
        $x = $x0;
        for ($j = 0; $j < $rows; $j++) {
            if ($j == 0) {
                $y = $y0;
            }
            else {
                $y = $y0 - $heightCell*$j;
            }

            for ($i = 0; $i < $colls; $i++) {
                if ($i == 0) {
                    $p->rect($x0, $y, $width/$colls, $heightCell);
                    $p->moveto($x0+10, $y-10);
                    // $p->show_xy($data[$i][$j], $x0+10, $y-10);
                }
                else {
                    $x = $x+$width/$colls;
                    $p->rect($x, $y, $width/$colls, $heightCell);
                    //$p->show_xy($data[$i][$j], $x0+10, $y-10);
                }
            }
            $x = $x0;
        }
        $p->stroke();
    }

    */

    function doGetPdf()
    {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."card-$seq.pdf");
        $len = strlen($buf);
        $this->response->header("Content-type: application/pdf");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: inline; filename=card-$seq.pdf");
        $this->response->write($buf);
        //@unlink(WGS3_PRINTCACHE_DIR."card-$seq.pdf");
    }

    function doTest()
    {
        $mass = array();
      for($i = 0; $i < 20; $i++) {
         $mass[] = array(
            "name" => rand(1, 100),
            "visits" => rand(1, rand(200, 10000))
            );
     }
     $this->response->write(json_encode(array_values($mass)));
    }




}
?>
