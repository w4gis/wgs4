<?php
// ini_set('display_errors',1);
// error_reporting(E_ALL);
    lmb_require('wgs/controller/objectadmin/CardController.class.php');
    lmb_require('limb/i18n/utf8.inc.php');
    lmb_require('wgs/lib/fpdf/textbox.php');

    class CardTableController extends CardController {

        function doGetCardTableHeader() {

            $layerId = $this->request->get('layerId');
            $cardType = $this->request->get('cardType');
            $cardsId = $this->doGetCardsByTypeAndLayerId(-1, $cardType, 0, 1, '', '', false)['result'];
            // var_dump($cardsId);
            $cardColumnsSet1 = array(
                array(
                    'label'     => 'Основные характеристики',
                    'children'  =>  array(
                        array(
                            'field' => 'name',
                            'label' => "Наименование",
                            'valueTypeId' => 1
                        ),
                        array(
                            'field' => 'action',
                            'label' => "",
                            'valueTypeId' => 102
                        )
                    ))
            );

            $cardColumnsSet2 = $this->getBasePropertiesHeader();
            $cardColumnsSet2 = array_merge($cardColumnsSet2, $this->getPropertiesHeader($cardsId));
            if($this->getRelationsHeader($cardsId) != false) {
                $cardColumnsSet2[] = $this->getRelationsHeader($cardsId);
            }

            $cardColumns = array(
              0 => array(
                  0 => $cardColumnsSet1
                  ),
              1 => array(
                  0 => $cardColumnsSet2
            ));


            $this->response->write(json_encode($cardColumns));
        }

        function getBasePropertiesHeader() {
          $basePropertiesHeader = array();

          $basePropertiesHeader[] = array(
              'label'     => 'Дополнительная информация',
              'children'  =>  array(
                  array(
                      'field' => 'id',
                      'label' => "Object ID",
                      'canEdit' => false,
                      'hidden'  => true,
                      'valueTypeId' => 0),

                  array(
                      'field' => 'shortName',
                      'label' => "Краткое наименование",
                      'canEdit'   => false,
                      'valueTypeId' => 1),

                  array(
                      'field' => 'note',
                      'label' => 'Примечание',
                      'valueTypeId' => 1),

                  array(
                      'field' => 'typeName',
                      'label' => 'Атрибутивное описание',
                      //'hidden'  => true,
                      'valueTypeId' => 1),

                  array(
                      'field' => 'createDate',
                      'label' => 'Дата изменения',
                      'canEdit'   => false,
                      'valueTypeId' => 0),

                  array(
                      'field' => 'edit_user',
                      'label' => 'Пользователь',
                      'canEdit'   => false,
                      'valueTypeId' => 0),
              )
          );
          return $basePropertiesHeader;
        }

        function getPropertiesHeader($cardsId){
          $propertiesArray = $this -> doGetValuePropertiesOne($cardsId);
          $propertiesHeader = array();
          $groups = array();

          foreach ($propertiesArray[0] as $property) {
              if($property['id'] == null) continue;

              if($groups[$property['groupId']] == null) {
                  $groups[$property['groupId']] = array(
                      'label'     =>  $property['groupName'],
                      'children'  =>  array()
                  );
              }

              $propArr = array(
                  'label' => $property['name'],
                  'field' => 'property-'.$property['id'],
                  'valueTypeId' => +$property['valueTypeId']
              );

              if($property['valueTypeId']==5) {
                  $propArr['domain'] =$property['domain'];
              }
              if($property['valueTypeId']==8) {
                  $propArr['sortable'] = false;
              }

              $groups[$property['groupId']]['children'][] = $propArr;
          }

          foreach ($groups as $group) {
              $propertiesHeader[] = $group;
          }

          return $propertiesHeader;
        }

        function getRelationsHeader($cardsId){
          $relationList = $this -> doGetListRelByObjectOne($cardsId);
        //   var_dump($cardsId);
        //   var_dump($relationList);
        //   $hasRelations = false;
        //   foreach($relationList as $relation) {
        //       if($relation['relation_id'] !== null) {
        //           $hasRelations = true;
        //           break;
        //       }
        //   }

        //   if($hasRelations == true) {
                $relationsHeader = array(
                    'label' => 'Вложенные карточки',
                    'children'  =>  array()
                );

                foreach($relationList[0] as $relation) {
                    $relationsHeader['children'][] = array(
                        'label' =>  $relation['name'],
                        'field' =>  'relation-'.$relation['relationId'],
                        'valueTypeId'   =>  101 /* nested cards fake type id*/
                        );
                }

                return $relationsHeader;
        //   }

          return false;
        }

        function doCardTableValues() {

            $method = $_SERVER['REQUEST_METHOD'];

            if ($method == 'PUT') {
                $this->doSetCardTableValues(substr($this -> request -> getUriPath(), strrpos($this -> request -> getUriPath(), '/') + 1));
            } elseif ($method == 'GET') {
                $this->doGetCardTableValues();
            }
        }

        function doGetCardTableValuesCount(){
            $layerId = $this->request->get('layerId');
            $cardType = $this->request->get('typeId');

            $totalNum = $this -> doGetCardsByTypeAndLayerId($layerId, $cardType, 0, 1, '', '', true, false);
            $updated = file_get_contents(WGS3_ROOT_DIR . '/application/projects/' . getProjectName() . '/updated');
            if($updated == '') {
                $updated = 'Никогда';
            }
            $this -> response -> write(json_encode(array(
                'total' => $totalNum,
                'updated' => $updated
            )));
        }

        function getCardTableValues($range) {

          foreach ($_GET as $key=>$parameter) {
              if (strstr($key,"sort(")) {
                  $sortBy = substr($key, 6, strpos($key,')')-6);
                  if($sortBy=='shortName'){
                      $sortBy = 'short_name';
                  } elseif(strstr($sortBy,"property-")){
                      $sortBy = explode('-', $sortBy)[1];
                  } elseif($sortBy != 'name'){
                      $sortBy = '';
                  }
              }
          }

          $sortBy = '';

          // var_dump($sortBy);
          $layerId = $this->request->get('layerId');
          $cardType = $this->request->get('typeId');
          $filter = ($this->request->get('filter') != null) ? decode($this->request->get('filter')) : '';
          $searchInContingent = $this->request->get('contingent') == 1 ? true : false;

          if($layerId && $cardType) {
              if (strpos($layerId, '=')) {
                  $layerId = explode("=", $layerId)[1];
              }
              if (strpos($cardType, '=')) {
                  $cardType = explode("=", $cardType)[1];
              }
              // echo "***1***";
              $searchRes = $this -> doGetCardsByTypeAndLayerId($layerId, $cardType, $range[0], $range[1] - $range[0], $sortBy, $filter, false, $searchInContingent);
              // var_dump($sarchRes['result']);
              $totalNum = $searchRes['total'];
              // echo "***2***";
              $cardIds = $searchRes['result'];
              // var_dump($cardIds);
          } else {
              $cardIds = array(
                  0 => array(
                      'cardId' => substr($this -> request -> getUriPath(), strrpos($this -> request -> getUriPath(), '/') + 1),
                      'objectTypeId' => $cardType
                      )
                  );
              $totalNum = 1;
          }

          $cardsArray = $this->doGetValueBasePropertiesOne($cardIds);
          $propertiesArray = $this -> doGetValuePropertiesOne($cardIds);
          $relationList = $this -> doGetListRelByObjectOne($cardIds);

          $cardColumns = $this->doGetCardValuesColumns($cardsArray, $propertiesArray, $relationList);

          return array('columns' => $cardColumns, 'count' =>count($cardIds), 'total' => $totalNum) ;
        }

        function doGetCardTableValues(){

          $headers = apache_request_headers();
          foreach ($headers as $header => $value) {
              if(strtolower($header) == 'range') {
                  $value = substr($value, 6);
                  $range = substr($value, 6, strlen($range) - 6);
                  $range = explode('-', $value);
              }
          }

          $cardInfo = $this -> getCardTableValues( $range);
          header("Content-Range:items " . $range[0] . "-" . ( $range[0] + $cardInfo['count']) . "/" . $cardInfo['total']);
          $this->response->write(json_encode(array_values($cardInfo['columns'])));
        }

        function doExcel () {
          // $headers = apache_request_headers();
          // foreach ($headers as $header => $value) {
          //     if(strtolower($header) == 'range') {
          //         $value = substr($value, 6);
          //         $range = substr($value, 6, strlen($range) - 6);
          //         $range = explode('-', $value);
          //     }
          // }
          set_time_limit(60 * 60 * 1000);

          $cardType = $this->request->get('typeId');
          $cardColumnsSet2 = $this->getBasePropertiesHeader();
          $cardsId = $this->doGetCardsByTypeAndLayerId(-1, $cardType, 0, 1, '', '', false)['result'];
          $cardColumnsSet2 = array_merge($cardColumnsSet2, $this->getPropertiesHeader($cardsId));

          $cardInfo = $this -> getCardTableValues(array(0, 1500));
          // var_dump($cardInfo);
          // die();
          ob_clean();
          // die();
          // var_dump($cardsId, $this->getPropertiesHeader($cardsId));
          // die();
          /**
           * PHPExcel
           *
           * Copyright (c) 2006 - 2015 PHPExcel
           *
           * This library is free software; you can redistribute it and/or
           * modify it under the terms of the GNU Lesser General Public
           * License as published by the Free Software Foundation; either
           * version 2.1 of the License, or (at your option) any later version.
           *
           * This library is distributed in the hope that it will be useful,
           * but WITHOUT ANY WARRANTY; without even the implied warranty of
           * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           * Lesser General Public License for more details.
           *
           * You should have received a copy of the GNU Lesser General Public
           * License along with this library; if not, write to the Free Software
           * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
           *
           * @category   PHPExcel
           * @package    PHPExcel
           * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
           * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
           * @version    ##VERSION##, ##DATE##
           */
          /** Error reporting */
          // error_reporting(E_ALL);
          // ini_set('display_errors', TRUE);
          // ini_set('display_startup_errors', TRUE);
          // // date_default_timezone_set('Europe/London');
          if (PHP_SAPI == 'cli')
          	die('This example should only be run from a Web Browser');
          /** Include PHPExcel */
          require_once '/wgs/lib/PHPExcel/Classes/PHPExcel.php';
          // Create new PHPExcel object
          $objPHPExcel = new PHPExcel();
          // Set document properties
          $objPHPExcel->getProperties()->setCreator("WGS4.TUSUR export")
          							 ->setLastModifiedBy("WGS4.TUSUR export")
          							 ->setTitle("Экспорт данных ГИС ТУСУР")
          							 ->setSubject("")
          							 ->setDescription("")
          							 ->setKeywords("")
          							 ->setCategory("");
          // Add some data
          $i = 1;
          $l = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
          $j = 0;
          $fields = array();
          // var_dump($cardColumnsSet2);
          // die();
          foreach($cardColumnsSet2 as $cardFieldGroup) {
            foreach($cardFieldGroup['children'] as $cardField) {
              $j++;
              $fields[$cardField['field']] = $j;
              // echo $l[$j] . $i. " :: ". $cardField['field'];

              if(!in_array(strtolower($cardField['field']), array('shortname', 'objectid', 'createdate', 'note', 'typename', 'edit_user'))) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($l[$j])->setAutoSize(true);
              }

              $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l[$j] . $i, $cardField['label']);
            }
          }

// die();
          //var_dump($cardInfo);
          //die();
          foreach($cardInfo['columns'] as $row) {
            $i++;
            // $j=0;
            foreach($row as $key => $value) {
              // $j++;
              if($l[$fields[$key]]) {
                $excelValue = $value;
                $dateTimeValue = DateTime::createFromFormat('Y-m-d G:i:s', $value);
                if(!$dateTimeValue) {
                  $notime = true;
                  $dateTimeValue = DateTime::createFromFormat('Y-m-d', $value);
                }
                if ($dateTimeValue !== FALSE) {
                  // it's a date
                  $excelValue = PHPExcel_Shared_Date::PHPToExcel($dateTimeValue);
                  $objPHPExcel->getActiveSheet()->getStyle($l[$fields[$key]] . $i)->getNumberFormat()->setFormatCode($notime ? PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15 : PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME);
                }
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l[$fields[$key]] . $i, $excelValue);

              }
              // echo $l[$fields[$key]] . $i . "; ";
              // echo $i . ":" . $value . "; ";
            }
          }
          // die();
          // Miscellaneous glyphs, UTF-8
          // Rename worksheet
          $objPHPExcel->getActiveSheet()->setTitle('Результаты поиска');


          // Set active sheet index to the first sheet, so Excel opens this as the first sheet
          $objPHPExcel->setActiveSheetIndex(0);
          // Redirect output to a client’s web browser (Excel2007)
          $fname = $this->request->get('filter') ? $this->request->get('filter') : 'search';
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment;filename="' . $fname . '.xlsx"');
          header('Cache-Control: max-age=0');
          // If you're serving to IE 9, then the following may be needed
          header('Cache-Control: max-age=1');
          // If you're serving to IE over SSL, then the following may be needed
          header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
          header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
          header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
          header ('Pragma: public'); // HTTP/1.0
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
          $objWriter->save('php://output');
          exit;
        }


        function doGetCardValuesColumns($cardsArray, $propertiesArray, $relationList) {
            $cardColumns = array();
            foreach ($cardsArray as $key => $card) {
                if($card != null){

                    $cardColumns[] = array(
                        'id'        => $card["id"],
                        'action'        => array('id' => $card["id"], 'type' => $card['objectTypeId']),
                        'name'          => $card["name"],
                        'shortName'     => $card['shortName'],
                        'typeName'      => $card['typeName'],
                        'createDate'    => $card['createDate'],
                        'edit_user'     => $card['edit_user'],
                        'note'          => $card['note']
                    );

                    foreach ($propertiesArray[$key] as $key2 => $property) {
                        $cardColumns[$key]['property-'.$property['id']] = $property['value'];
                    }

                    foreach ($relationList[$key] as $key2 => $relation) {
                        $rels = json_encode(array_values($relation['linkedCards']));
                        $cardColumns[$key]['relation-'.$relation['relationId']] = $rels;
                    }
                }
            }
            return $cardColumns;

        }

        /*
        *   @objectId - ID of card which is gonna be edited
        */
        function doSetCardTableValues($objectId) {
            $putdata=array();
            $putdata= json_decode(file_get_contents("php://input"));

            $user = $this->wgsTier->getAuthorizedUser();

            $ObjectEditUser = decode('источник : ГИС;  пользователь : '.$user->getName().'; хост : '.$_SERVER['REMOTE_ADDR']);

            $card = (array)$putdata;
            $oldBaseProperties = $this->doGetValueBasePropertiesOne(array(0=>array('cardId'=>$objectId)));

            if($card['name']) {
                $objectName = $card['name'];
            } else {
                $objectName = $oldBaseProperties[0]['name'];
            }

            if($card['shortName']) {
                $objectShortName = $card['shortName'];
            } else {
                $objectShortName = $oldBaseProperties[0]['shortName'];
            }
            if($card['note']) {
                $objectNote = $card['note'];
            } else {
                $objectNote = $oldBaseProperties[0]['note'];
            }

            $this->wgsTier->getProjectObjectService()->getAttObjectService()->Set($objectId,decode($objectName),decode($objectShortName),decode($objectNote),$ObjectEditUser);

            foreach($card as $key => $propertyValue)
            {
                if (strstr($key,"property"))
                {
                    $mas = explode("-", $key);
                    $propertyId = $mas[1];
                    $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($objectId,$propertyId,decode($propertyValue));
                }
            }
        }
    }
?>
