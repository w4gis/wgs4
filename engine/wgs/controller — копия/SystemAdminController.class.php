<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsPermission.class.php');
lmb_require('wgs/model/wgsSysObject.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class SystemAdminController extends wgsFrontController
{
	private $modules = array();
	//  ��������� ������ ��������� ������� ������� 
    //----------------------------------------------------------------------------------------------------------
    function doGetProjectList()
    {
    	$SysProjectList = $this->wgsTier->getSysObjectService()->getSysProjectService()->getProjectList();
    	if (count($SysProjectList) > 0) {
            $SysProjectArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ ��������'), 'expanded' => true);
    	} else {
            $SysProjectArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ ��������'), 'expanded' => false, 'leaf' => 'false');    
        }
    	 
    	foreach($SysProjectList as $SysProject)
        {
        	$SysProjectId = $SysProject->getId();
        	$SysModuleList = $SysProject->getSysModuleList();
        	
        	if ($SysProject->getLocked() == 1) $projectLocked = "color: #888888; font-style: italic;";
            else $projectLocked = '';
        	
        	
        	if ($SysModuleList) { $projectleaf = ''; $expandedProject = true; }
        	else { $projectleaf = 'true'; $expandedProject = false; }
        	
        		$SysProjectArray['children'][$SysProjectId]['id'] = $SysProject->getId();
		        $SysProjectArray['children'][$SysProjectId]['text'] = lmb_win1251_to_utf8('<span style="'.$projectLocked.'">'.$SysProject->getName().'</span>');
		        $SysProjectArray['children'][$SysProjectId]['leaf'] = $projectleaf;
		        $SysProjectArray['children'][$SysProjectId]['expanded'] = $expandedProject;
		        $SysProjectArray['children'][$SysProjectId]['type'] = wgsSysObject::PROJECT;
		        $SysProjectArray['children'][$SysProjectId]['name'] = lmb_win1251_to_utf8($SysProject->getName());
		        
		        for ($i = 0; $i < count($SysModuleList); $i++)
	        	{
	        	    if ($SysModuleList[$i]['SYSOBJECT_LOCKED'] == 1) $moduleLocked = "color: #888888; font-style: italic;";
                    else $moduleLocked = '';
                    
	        		$SysModuleChildList = $this->GetProjectTree($SysModuleList[$i]['SYSMODULE_ID'], $SysProjectId);
		            if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysModuleList[$i]['SYSMODULE_ID'],$SysProjectId))
		            $SysProjectArray['children'][$SysProjectId]['children'][] = array(
		                'id'  => $SysModuleList[$i]['SYSOBJECT_ID']."_".md5(rand()),
		                'text' => lmb_win1251_to_utf8('<span style="'.$moduleLocked.'">'.$SysModuleList[$i]['LABEL'].'</span>'),
		                'expanded' => true,
		                'type' => wgsSysObject::MODULE,
		                'children' => $SysModuleChildList
		            );
		            else 
		            $SysProjectArray['children'][$SysProjectId]['children'][] = array(
		            	'id'  => $SysModuleList[$i]['SYSOBJECT_ID'],
		                'text' => lmb_win1251_to_utf8('<span style="'.$moduleLocked.'">'.$SysModuleList[$i]['LABEL'].'</span>'),
		                'leaf' => 'true',
		                'type' => wgsSysObject::MODULE
		            );
	        	}
        }
        $SysProjectArray['children'] = array_merge($SysProjectArray['children']);
        $jsonSysProjectArray = array();        
        $jsonSysProjectArray[]=$SysProjectArray;
        $this->response->write(json_encode($jsonSysProjectArray));
        
    }
    function GetProjectTree($SysModuleId, $projectId)
    {
    		$count = 0;
	    	$SysModuleChildArray = array();
	    	$SysModuleChildList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysModuleId,$projectId);
	    	if ($SysModuleChildList) 
	    	{
	    		foreach($SysModuleChildList as $SysModuleChild)
	        	{
                    if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysModuleChild->getId(), $projectId)) 
                    { $moduleleaf = ''; $expandedModule = true; }
                    else { $moduleleaf = 'true'; $expandedModule = false; }
                    
                    if ($SysModuleChild->getLocked() == 1) $moduleLocked = "color: #888888; font-style: italic;";
                    else $moduleLocked = '';
                    
	        	    $SysModuleChildArray[$count]['id'] = $SysModuleChild->getId()."_".md5(rand());
		    		$SysModuleChildArray[$count]['text'] = lmb_win1251_to_utf8('<span style="'.$moduleLocked.'">'.$SysModuleChild->getLabel().'</span>');
		    		$SysModuleChildArray[$count]['leaf'] = $moduleleaf;
		    		$SysModuleChildArray[$count]['expanded'] = $expandedModule;
		    		$SysModuleChildArray[$count]['type'] = wgsSysObject::MODULE;
		    		$SysModuleChildArray[$count]['sysid'] = $SysModuleChild->getId();
                    
		    		if ($count+1 == count($SysModuleChildList))
		    		{
		    			for ($i = 0; $i < count($SysModuleChildArray); $i++)
		    				if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysModuleChildArray[$i]['sysid'], $projectId))
	    					$SysModuleChildArray[$i]['children'] = $this->GetProjectTree($SysModuleChildArray[$i]['sysid'], $projectId);
		    		}
		    		$count++;
	        	}
	    		return $SysModuleChildArray;
	    	}
    }
    //----------------------------------------------------------------------------------------------------------
	
    // ��������� ������ ��������� ��������, ������ ����� ������ � ���� 
    function doGetProjectListPath()
    {
        $SysProjectArray = array();

        $SysProjectList = $this->wgsTier->getSysObjectService()->getSysProjectService()->getProjectList();
    	// ���� ������������ getModuleListByProjectid, �� � ����������� ��������� ���������� �������� ������ ����������� ������
        $SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getModuleListByProjectid($_POST['node']);

    	if ($SysProjectList)
        foreach($SysProjectList as $SysProject)
        {
            $id = $SysProject->getId();
            if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getModuleListByProjectid($id)) $projectleaf = '';
            else $projectleaf = 'true';
            $SysProjectArray['root'][] = array(
                'id' => $id,
                'text' => lmb_win1251_to_utf8($SysProject->getName()),
                'leaf' => $projectleaf
            ); 
        }
        
        if ($SysModuleList)
        foreach($SysModuleList as $SysModule)
        {
            if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModule->getId())) 
            $moduleleaf = '';
            else $moduleleaf = 'true';
            $id = $SysModule->getId()."_".$_POST['node']."_".md5(rand());
            $SysProjectArray[$_POST['node']][] = array(
                'id' => $id,
                'text' => lmb_win1251_to_utf8($SysModule->getLabel()),
                'leaf' => $moduleleaf
            ); 
        }

        $SysModuleIdList = explode("_", $_POST['node']);
        
        if (count($SysModuleIdList) == 3) 
        {        
            $SysModuleId = $SysModuleIdList[0];
            $SysProjectId = $SysModuleIdList[1];
            
            $SysChildModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysModuleId, $SysProjectId);
            if ($SysChildModuleList)
            foreach($SysChildModuleList as $SysChildModule)
            {
                $id = $SysChildModule->getId()."_".$SysProjectId."_".md5(rand());
                if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByProjectId($SysChildModule->getId(), $SysProjectId)) 
                $moduleleaf = '';
                else $moduleleaf = 'true';
                $SysProjectArray[$_POST['node']][] = array(
                    'id' => $id,
                    'text' => lmb_win1251_to_utf8($SysChildModule->getLabel()),
                    'leaf' => $moduleleaf
                ); 
            }
        }
        
        if (isset($_POST['node']))
            $this->response->write(json_encode(array_merge($SysProjectArray[$_POST['node']])));
       
    }
    
    //  ��������� ������ ��������� ������� ������� 
    //----------------------------------------------------------------------------------------------------------
    function doGetModuleList()
    {
    	$SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getParentModuleList();
    	
    	if (count($SysModuleList) > 0) {
    	   $moduleArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ �������'), 'expanded' => true);
    	} else {
    	    $moduleArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ �������'), 'expanded' => true, 'leaf' => 'false');
    	}
		    	
		foreach($SysModuleList as $SysModule)
        {
        	$SysModuleId = $SysModule->getId();
        	$SysModuleChildList = $this->GetModuleTree($SysModuleId);
        	if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleId)) 
            $moduleleaf = '';
            else $moduleleaf = 'true';
            
            if ($SysModule->getLocked() == 1) $moduleLocked = "color: #888888; font-style: italic;";
            else $moduleLocked = '';
            
        	$moduleArray['children'][$SysModuleId]['id'] = $SysModule->getId();
	        $moduleArray['children'][$SysModuleId]['text'] = lmb_win1251_to_utf8('<span style="'.$moduleLocked.'">'.$SysModule->getLabel().'</span>');
            $moduleArray['children'][$SysModuleId]['leaf'] = $moduleleaf;
            if ($moduleleaf == 'true') $moduleArray['children'][$SysModuleId]['expanded'] = false;
            else $moduleArray['children'][$SysModuleId]['expanded'] = true;
	        $moduleArray['children'][$SysModuleId]['name'] = $SysModule->getName();
	        
	        if ($SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleId))
	        $moduleArray['children'][$SysModuleId]['children'] = $SysModuleChildList;
        }
        
        $moduleArray['children'] = array_merge($moduleArray['children']);
        $jsonModuleArray = array();
        $jsonModuleArray[]=$moduleArray;
        $this->response->write(json_encode($jsonModuleArray));
    }
	function GetModuleTree($SysModuleId)
    {
    		$count = 0;
	    	$SysModuleChildArray = array();
	    	$SysModuleChildList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleId);
	    	if ($SysModuleChildList) 
	    	{
	    		foreach($SysModuleChildList as $SysModuleChild)
	        	{
		    		$SysModuleChildArray[$count]['id'] = $SysModuleChild->getId();
		    		if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleChild->getId())) 
                    $moduleleaf = '';
                    else $moduleleaf = 'true';
                    
                    if ($SysModuleChild->getLocked() == 1) $moduleLocked = "color: #888888; font-style: italic;";
                    else $moduleLocked = '';
                    
	                $SysModuleChildArray[$count]['text'] = lmb_win1251_to_utf8('<span style="'.$moduleLocked.'">'.$SysModuleChild->getLabel().'</span>');
                    $SysModuleChildArray[$count]['leaf'] = $moduleleaf;
                    if ($moduleleaf == 'true') $SysModuleChildArray[$count]['expanded'] = false;
                    else $SysModuleChildArray[$count]['expanded'] = true;
	                $SysModuleChildArray[$count]['name'] = $SysModuleChild->getName();
		    		
		    		if ($count+1 == count($SysModuleChildList))
		    		{
		    			for ($i = 0; $i < count($SysModuleChildArray); $i++)
		    				if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleChildArray[$i]['id']))
	    					$SysModuleChildArray[$i]['children'] = $this->GetModuleTree($SysModuleChildArray[$i]['id']);
		    		}
		    		$count++;
	        	}
	        	return $SysModuleChildArray;
	    	}
	}
    
	// �������� ������ � ����� ������� � ��������
	//----------------------------------------------------------------------------------------------------------
    function doGetMemberships()
    {
    	$SysAssignModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getmodulelistbyprojectid($this->request->getPost('id'));
    	$SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->GetModuleList();
    
        $count = 0;
        foreach($SysModuleList as $SysModule)
        { 
            $children = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModule->getId());
            $level = $SysModule->getLevel()*15;   
            if ($children) $img = "/images/root.gif";
            else $img="/images/leaf.gif";
            
            if ($SysModule->getLocked() == 1) $moduleLocked = "color: #888888; font-style: italic;";
            else $moduleLocked = '';
            
            $Memberships[$count]['name'] = lmb_win1251_to_utf8('<div style="padding-left: '.$level.'px; '.$moduleLocked.'"><img src="'.$img.'">     '.$SysModule->getLabel().'</div>'); 
            $Memberships[$count]['id'] = $SysModule->getId(); 
            $Memberships[$count]['parentId'] = $SysModule->getParentId();
            $Memberships[$count]['label'] = lmb_win1251_to_utf8($SysModule->getLabel()); 
            $Memberships[$count]['urlstring'] = $SysModule->getUrlString(); 
            $Memberships[$count]['description'] = lmb_win1251_to_utf8($SysModule->getDescription()); 
            $Memberships[$count]['priority'] = $SysModule->getPriority(); 
            
            $id = $SysModule->getId();
            $MembershipsCount = 0;
            foreach($SysAssignModuleList as $SysAssignModule)
            if ($id == $SysAssignModule->getId()) 
            $MembershipsCount++;

            if ($MembershipsCount != 0) $membershipsAllow = 1;
            else $membershipsAllow = '';
            
            $Memberships[$count]['memberships'][] = array(
                    'membershipsId' => "membership-".$SysModule->getId(),
                    'membershipsName' => lmb_win1251_to_utf8('��������'),
                    'membershipsAllow' => $membershipsAllow
                );
            $Memberships[$count]['objectid'] = $SysModule->getObjectId();
            // ������� ����
            if ($this->request->getPost('id'))
            {
                $permissions = $this->wgsTier->getSysPermissionService()->getPermissionsByRoleId($this->request->getPost('id'), $SysModule->getObjectId());
        		$permissionList = array();
        		$privilegeNameList = array();
        		foreach($permissions as $permission)
        		{
        		    $Memberships[$count]['privileges'][] = array(
                        'privilegesId' => "privilege-".$permission->getPrivilege()->getId(),
                        'privilegesName' => lmb_win1251_to_utf8($permission->getPrivilege()->getDescription()),
                        'privilegesAllow' => $permission->getPermission()
                    );
        		}
            }	
            
            $count++;
        }
        
        $count = 0;
        $MembershipsSort = array();
        for ($i = 0; $i < count($Memberships); $i++)
        {
            if ($Memberships[$i]['parentId'] == '' && in_array($Memberships[$i], $MembershipsSort) == false)
                $MembershipsResult[] = $this->GetModuleListTreeSort($Memberships[$i], $Memberships);
        }  
        $this->response->write(json_encode($this->modules));
    }    
    
    // �������� ������ ������������� � ���� ������ �������
    //-------------------------------------------------------------------------------------------------------
    function doGetModuleListTree()
    {
    	$SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->GetModuleList();
        $count = 0;
        foreach($SysModuleList as $SysModule)
        {
            $children = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModule->getId());
            $level = $SysModule->getLevel()*15;   
            if ($children) $img = "/images/root.gif";
            else $img="/images/leaf.gif";
            if ($SysModule->getLocked() == 1) $moduleLocked = "color: #888888; font-style: italic;";
            else $moduleLocked = '';
            
            $Memberships[$count]['nameHTML'] = lmb_win1251_to_utf8('<div style="padding-left: '.$level.'px; '.$moduleLocked.'"><img src="'.$img.'">     '.$SysModule->getLabel().'</div>'); 
            $Memberships[$count]['name'] = lmb_win1251_to_utf8($SysModule->getName()); 
            $Memberships[$count]['id'] = $SysModule->getId(); 
            $Memberships[$count]['parentId'] = $SysModule->getParentId();
            $Memberships[$count]['label'] = lmb_win1251_to_utf8($SysModule->getLabel()); 
            $Memberships[$count]['urlstring'] = $SysModule->getUrlString(); 
            $Memberships[$count]['description'] = lmb_win1251_to_utf8($SysModule->getDescription()); 
            $Memberships[$count]['priority'] = $SysModule->getPriority(); 
            $Memberships[$count]['container'] = $SysModule->isContainer(); 
            $Memberships[$count]['inmenu'] = $SysModule->getInMenu(); 
            $Memberships[$count]['locked'] = $SysModule->getLocked(); 
            $count++;
        }
        $count = 0;
        $MembershipsSort = array();
        for ($i = 0; $i < count($Memberships); $i++)
        {
            if ($Memberships[$i]['parentId'] == '' && in_array($Memberships[$i], $MembershipsSort) == false)
                $MembershipsResult[] = $this->GetModuleListTreeSort($Memberships[$i], $Memberships);
        }  
        $this->response->write(json_encode($this->modules));
    }
    function GetModuleListTreeSort($MembershipsSort, $Memberships)
    {
        if (in_array($MembershipsSort, $this->modules) == false)
        $this->modules[]=$MembershipsSort;
        for ($j = 0; $j < count($Memberships); $j++)
        {
            if ($Memberships[$j]['parentId'] == $MembershipsSort['id'])
                $this->GetModuleListTreeSort($Memberships[$j], $Memberships);
        } 
    }
    //-------------------------------------------------------------------------------------------------------
	
    // ��������� ������ ��������� �������, ������ ����� ������ � ���� 
    //---------------------------------------------------------------------------------------------------------
    function doGetModuleListPath()
    {
        $SysModuleArray = array();
        $SysModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getParentModuleList();
        if ($SysModuleList)
        foreach($SysModuleList as $SysModule)
        {
            if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModule->getId())) 
            $moduleleaf = '';
            else $moduleleaf = 'true';
            $id = $SysModule->getId()."_".md5(rand());
            $SysModuleArray['root'][] = array(
                'id' => $id,
                'text' => lmb_win1251_to_utf8($SysModule->getLabel()),
                'leaf' => $moduleleaf,
                'expanded' => true,
                'name' => $SysModule->getName()
            ); 
        }
        
        $SysModuleIdList = explode("_", $_POST['node']);
        $SysModuleId = $SysModuleIdList[0];
        
        $SysChildModuleList = $this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysModuleId);
        if ($SysChildModuleList)
        foreach($SysChildModuleList as $SysChildModule)
        {
            $id = $SysChildModule->getId()."_".md5(rand());
            if ($this->wgsTier->getSysObjectService()->getSysModuleService()->getChildModuleListByModuleId($SysChildModule->getId())) 
            $moduleleaf = '';
            else $moduleleaf = 'true';
            $SysModuleArray[$_POST['node']][] = array(
                'id' => $id,
                'text' => lmb_win1251_to_utf8($SysChildModule->getLabel()),
                'leaf' => $moduleleaf,
            	'expanded' => true,
            	'name' => $SysModule->getName()
            ); 
        }
        
        if (isset($_POST['node']))
            $this->response->write(json_encode(array_merge($SysModuleArray[$_POST['node']])));
    }
    //---------------------------------------------------------------------------------------------------------
    
    // �������� ������ ����������
    //---------------------------------------------------------------------------------------------------------
    function doGetStorePrivilegeList()
    {
        $SysPrivilegeList = $this->wgsTier->getSysObjectService()->getSysPrivilegeService()->getSysPrivilegeList();
    	$count = 0;
    	foreach($SysPrivilegeList as $SysPrivilege)
        {
        	$SysPrivilegeId = $SysPrivilege->getId();
        	
        	if ($SysPrivilege->getLocked() == 1) $privilegeLocked = "color: #888888; font-style: italic;";
            else $privilegeLocked = '';
        	
       		$SysPrivilegeArray[$count]['id'] = $SysPrivilege->getId();
	        $SysPrivilegeArray[$count]['name'] = lmb_win1251_to_utf8($SysPrivilege->getName());
	        $SysPrivilegeArray[$count]['nameHtml'] = lmb_win1251_to_utf8('<div style="'.$privilegeLocked.'">'.$SysPrivilege->getName().'</div>');
	        $SysPrivilegeArray[$count]['description'] = lmb_win1251_to_utf8($SysPrivilege->getDescription());
	        $SysPrivilegeArray[$count]['objectid'] = $SysPrivilege->getObjectId();
	        $SysPrivilegeArray[$count]['locked'] = $SysPrivilege->getLocked();
            
	        // ������� ����
            if ($this->request->getPost('id'))
            {
                $permissions = $this->wgsTier->getSysPermissionService()->getPermissionsByRoleId($this->request->getPost('id'), $SysPrivilege->getObjectId());
        		$permissionList = array();
        		$privilegeNameList = array();
        		foreach($permissions as $permission)
        		{
        		    $SysPrivilegeArray[$count]['privileges'][] = array(
                        'privilegesId' => "privilege-".$permission->getPrivilege()->getId(),
                        'privilegesName' => lmb_win1251_to_utf8($permission->getPrivilege()->getDescription()),
                        'privilegesAllow' => $permission->getPermission()
                    );
        		}
            }
	        
            $count++;
        }
        $this->response->write(json_encode($SysPrivilegeArray));
    }
    //---------------------------------------------------------------------------------------------------------
    
    // �������� ������ ��������
    //---------------------------------------------------------------------------------------------------------
    function doGetStoreProjectList()
    {
        $SysProjectList = $this->wgsTier->getSysObjectService()->getSysProjectService()->getProjectList();
    	$count = 0;
        
    	foreach($SysProjectList as $SysProject)
        {
        	$SysProjectId = $SysProject->getId();
        	
        	if ($SysProject->getLocked() == 1) $projectLocked = "color: #888888; font-style: italic;";
            else $projectLocked = '';
       		
            $SysProjectArray[$count]['id'] = $SysProject->getId();
	        $SysProjectArray[$count]['name'] = lmb_win1251_to_utf8($SysProject->getName());
	        $SysProjectArray[$count]['nameHtml'] = lmb_win1251_to_utf8('<span style="'.$projectLocked.'">'.$SysProject->getName().'</span>');
	        $SysProjectArray[$count]['domain'] = $SysProject->getDomain();
	        $SysProjectArray[$count]['description'] = lmb_win1251_to_utf8($SysProject->getDescription());
	        $SysProjectArray[$count]['database'] = $SysProject->getDatabase();
	        $SysProjectArray[$count]['title'] = lmb_win1251_to_utf8($SysProject->getTitle());
	        $SysProjectArray[$count]['schemewdb'] = $SysProject->getSchemeWdb();
	        $SysProjectArray[$count]['schemesdb'] = $SysProject->getSchemeSdb();
	        $SysProjectArray[$count]['schemeadb'] = $SysProject->getSchemeAdb();
	        $SysProjectArray[$count]['schemeuser'] = $SysProject->getSchemeUser();
	        $SysProjectArray[$count]['locked'] = $SysProject->getLocked();
	        $count++;
        }
        $this->response->write(json_encode($SysProjectArray));
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ���������� ��������� ������� ��������
    //--------------------------------------------------------------------------------------------------------
    function doSetMemberships()
    {
        if ($this->request->getPost('memberships') && $this->request->getPost('projectid'))
        {
            $request = (json_decode($this->request->getPost('memberships'), true));
            $projectId = $this->request->getPost('projectid');
            $grantes = array();
            $revokes = array();
            foreach($request as $membership)
            {
                foreach($membership as $key => $value)
                    if (strstr($key, "membership-"))
                    {
                        $moduleId = $membership['id'];
                        $wgsSysProject = new wgsSysProject();
                        $wgsSysProject->setId($projectId);
                        $wgsSysModule = new wgsSysModule();
                        $wgsSysModule->setId($moduleId);
                
                        if ($value)
                            $this->wgsTier->getSysObjectService()->getSysModuleService()->grantSysModuleMemberships($wgsSysModule,$wgsSysProject);
                        else
                            $this->wgsTier->getSysObjectService()->getSysModuleService()->revokeSysModuleMemberships($wgsSysModule,$wgsSysProject);
                    }   
            }
            $this->response->write("succses");
        }
    }
    //---------------------------------------------------------------------------------------------------------
    
    // ������� ������� ���� ����� �� �������
    //---------------------------------------------------------------------------------------------------------
    function doSetSysObjectPermissions()
	{
	    if ($this->request->getPost('privileges') && $this->request->getPost('roleid'))
        {
            $request = (json_decode($this->request->getPost('privileges'), true));
            $roleId = $this->request->getPost('roleid');

            $sysPermissions = array();    
            foreach($request as $privilege)
            {
                foreach($privilege as $key => $value)
                    if (strstr($key, "privilege-"))
                    {     
                        $objectId = $privilege['objectid'];
                        $privilegeMassId = explode('-', $key);
                        $privilegeId = $privilegeMassId[1];
                        $sysRole = new wgsSysRole();
        				$sysRole->setId($roleId);
        				$sysObject = new wgsProjectObject();
        				$sysObject->setObjectId($objectId);
        				$sysPermission = new wgsPermission();
        				$sysPrivilege = new wgsSysPrivilege();
        				$sysPrivilege->setId($privilegeId);
        				$sysPermission->setPrivilege($sysPrivilege);
        				$sysPermission->setRole($sysRole);
        				$sysPermission->setObject($sysObject);
        				$sysPermission->setPermission($value);
        				$sysPermissions[] = $sysPermission;
                    }   
            }
            if ($sysPermissions)
                $this->wgsTier->getSysPermissionService()->setPermissions($sysPermissions);
                       
            $this->response->write("succses");
        }
	}
    //---------------------------------------------------------------------------------------------------------
	
	//������� �������� ������ ������
	//---------------------------------------------------------------------------------------------------------
	function doCreateSysModule()
	{
	    $name = $this->request->getPost('name');
	    $label = lmb_utf8_to_win1251($this->request->getPost('label'));
	    $urlstring = $this->request->getPost('urlstring');
	    $description = lmb_utf8_to_win1251($this->request->getPost('description'));
	    $priority = $this->request->getPost('priority');
	    $parentId = $this->request->getPost('parentId');
        $parentIdCheck = $this->request->getPost('parentModule');
        $locked = $this->request->getPost('locked');
        
	    $container = $this->request->getPost('container');
	    if ($container == 'on') $container = 1;
	    else $container = 0;
	    $inmenu = $this->request->getPost('inmenu');
	    if ($inmenu == 'on') $inmenu = 1;
	    else $inmenu = 0;
	    
	    $wgsSysModule = new wgsSysModule();
        $wgsSysModule->setName($name);
        $wgsSysModule->setLabel($label);
        $wgsSysModule->setUrlString($urlstring);
        $wgsSysModule->setDescription($description);
        $wgsSysModule->setContainer($container);
        $wgsSysModule->setPriority($priority);
        if ($parentIdCheck == 'on' && $parentId != 'null' && $parentId) $wgsSysModule->setParentId($parentId);
        $wgsSysModule->setInMenu($inmenu);
        if ($locked == 'on') $wgsSysModule->setLocked(1);
        if ($locked == 'off') $wgsSysModule->setLocked(0);
        
        $this->wgsTier->getSysObjectService()->getSysModuleService()->createSysModuleService($wgsSysModule);
	}
	//---------------------------------------------------------------------------------------------------------
	
	//������� �������������� ������� ������
	//---------------------------------------------------------------------------------------------------------
	function doEditSysModule()
	{
	    $id = $this->request->getPost('id');
	    $name = $this->request->getPost('name');
	    $label = lmb_utf8_to_win1251($this->request->getPost('label'));
	    $urlstring = $this->request->getPost('urlstring');
	    $description = lmb_utf8_to_win1251($this->request->getPost('description'));
	    $priority = $this->request->getPost('priority');
	    $parentId = $this->request->getPost('parentId');
        $parentIdCheck = $this->request->getPost('parentModule');
        $locked = $this->request->getPost('locked');
        
	    $container = $this->request->getPost('container');
	    if ($container == 'on') $container = 1;
	    else $container = 0;
	    $inmenu = $this->request->getPost('inmenu');
	    if ($inmenu == 'on') $inmenu = 1;
	    else $inmenu = 0;
	    
	    $wgsSysModule = new wgsSysModule();
	    $wgsSysModule->setId($id);
        $wgsSysModule->setName($name);
        $wgsSysModule->setLabel($label);
        $wgsSysModule->setUrlString($urlstring);
        $wgsSysModule->setDescription($description);
        $wgsSysModule->setContainer($container);
        $wgsSysModule->setPriority($priority);
        if ($parentIdCheck == 'on' && $parentId != 'null' && $parentId) $wgsSysModule->setParentId($parentId);
        $wgsSysModule->setInMenu($inmenu);
        if ($locked == 'on') $wgsSysModule->setLocked(1);
        else $wgsSysModule->setLocked(0);
        
        $this->wgsTier->getSysObjectService()->getSysModuleService()->editSysModuleService($wgsSysModule);
	}
    //---------------------------------------------------------------------------------------------------------
    
	// ������� �������� ������
	//---------------------------------------------------------------------------------------------------------
	function doRemoveSysModule()
	{
	   $id = $this->request->getPost('id');
       $wgsSysModule = new wgsSysModule();
       $wgsSysModule->setId($id);
       $this->wgsTier->getSysObjectService()->getSysModuleService()->removeSysModuleService($wgsSysModule);
	}
    //---------------------------------------------------------------------------------------------------------
    
	//������� �������� ����� ����������
	//---------------------------------------------------------------------------------------------------------
	function doCreateSysPrivilege()
	{
	    $name = lmb_utf8_to_win1251($this->request->getPost('name'));
	    $description = lmb_utf8_to_win1251($this->request->getPost('description'));
        $locked = $this->request->getPost('locked'); 
         
	    $wgsSysPrivilege = new wgsSysPrivilege();
        $wgsSysPrivilege->setName($name);
        $wgsSysPrivilege->setDescription($description);
        
        if ($locked == 'on') $wgsSysPrivilege->setLocked(1);
        else $wgsSysPrivilege->setLocked(0);
        
        $this->wgsTier->getSysObjectService()->getSysPrivilegeService()->createSysPrivilegeService($wgsSysPrivilege);
	}
	//---------------------------------------------------------------------------------------------------------
	
	//������� �������������� ������� ����������
	//---------------------------------------------------------------------------------------------------------
	function doEditSysPrivilege()
	{
	    $id = $this->request->getPost('id');
	    $name = lmb_utf8_to_win1251($this->request->getPost('name'));
	    $description = lmb_utf8_to_win1251($this->request->getPost('description'));
        $locked = $this->request->getPost('locked'); 
        
	    $wgsSysPrivilege = new wgsSysPrivilege();
        $wgsSysPrivilege->setId($id);
        $wgsSysPrivilege->setName($name);
        $wgsSysPrivilege->setDescription($description);
        
        if ($locked == 'on') $wgsSysPrivilege->setLocked(1);
        else $wgsSysPrivilege->setLocked(0);
        
        $this->wgsTier->getSysObjectService()->getSysPrivilegeService()->editSysPrivilegeService($wgsSysPrivilege);
	}
    //---------------------------------------------------------------------------------------------------------
    
	// ������� �������� ����������
	//---------------------------------------------------------------------------------------------------------
	function doRemoveSysPrivilege()
	{
	   $params = explode('_', $this->request->getPost('id'));
	   foreach($params as $id)
       {   if ($id) {
               $wgsSysPrivilege = new wgsSysPrivilege();
               $wgsSysPrivilege->setId($id);
               $this->wgsTier->getSysObjectService()->getSysPrivilegeService()->removeSysPrivilegeService($wgsSysPrivilege);
           }    
       }    
	}
    //---------------------------------------------------------------------------------------------------------
	
	//������� �������� ������ �������
    //---------------------------------------------------------------------------------------------------------
    function doCreateSysProject()
    {
        $name = $this->request->getPost('name');
        $domain = $this->request->getPost('domain');
        $description = lmb_utf8_to_win1251($this->request->getPost('description'));
        $database = $this->request->getPost('database');
        $title = lmb_utf8_to_win1251($this->request->getPost('title'));
        $locked = $this->request->getPost('locked');
     
        $install_schemes = file_get_contents(WGS3_SETTINGS_DIR.'install_schemes.sql');
        
        $install_schemes = str_ireplace( 'PROJECT_WDB', $name.'_WDB', $install_schemes); 
        //$install_schemes = str_ireplace( 'PROJWEB',  $name.'web', $install_schemes); 
        $install_schemes = str_ireplace( 'PROJECT_SDB', $name.'_SDB', $install_schemes); 
        //$install_schemes = str_ireplace( 'PROJSPA',  $name.'spa', $install_schemes);
        $install_schemes = str_ireplace( 'PROJECT_ADB', $name.'_ADB', $install_schemes); 
        //$install_schemes = str_ireplace( 'PROJATT',  $name.'att', $install_schemes);
        $install_schemes = str_ireplace( 'PROJECT_USER', $name.'_USER', $install_schemes); 
        //$install_schemes = str_ireplace( 'PROJUSER',  $name.'user', $install_schemes);
        $install_schemes_array = explode(';', $install_schemes);
        
        
        $install_logics = file_get_contents(WGS3_SETTINGS_DIR.'install_logics.sql');
        $install_logics = str_ireplace( 'PROJECT_WDB', $name.'_WDB', $install_logics); 
        $install_logics = str_ireplace( 'PROJECT_SDB', $name.'_SDB', $install_logics); 
        $install_logics = str_ireplace( 'PROJECT_ADB', $name.'_ADB', $install_logics); 
        $install_logics = str_ireplace( 'PROJECT_USER', $name.'_USER', $install_logics); 
        $install_logics_array = explode('/', $install_logics);
        
        for ($i = 0; $i < count($install_logics_array)-1; $i++)
            $install_logics_result[$i] = $install_logics_array[$i];
        //print_r($install_logics_result);
        $wgsSysProject = new wgsSysProject();
        $wgsSysProject->setName($name);
        $wgsSysProject->setDomain($domain);
        $wgsSysProject->setDescription($description);
        $wgsSysProject->setDatabase($database);
        $wgsSysProject->setTitle($title);
        
        if ($locked == 'on') $wgsSysProject->setLocked(1);
        else $wgsSysProject->setLocked(0);
        
        $this->wgsTier->getSysObjectService()->getSysProjectService()->createSysProjectService($wgsSysProject, $install_schemes_array, $install_logics_result);
    }
    //---------------------------------------------------------------------------------------------------------
    
    //������� �������������� ������� �������
    //---------------------------------------------------------------------------------------------------------
    function doEditSysProject()
    {
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $domain = $this->request->getPost('domain');
        $description = lmb_utf8_to_win1251($this->request->getPost('description'));
        $database = $this->request->getPost('database');
        $title = lmb_utf8_to_win1251($this->request->getPost('title'));
        $locked = $this->request->getPost('locked');
        
        $wgsSysProject = new wgsSysProject();
        $wgsSysProject->setId($id);
        $wgsSysProject->setName($name);
        $wgsSysProject->setDomain($domain);
        $wgsSysProject->setDescription($description);
        $wgsSysProject->setDatabase($database);
        $wgsSysProject->setTitle($title);

        if ($locked == 'on') $wgsSysProject->setLocked(1);
        else $wgsSysProject->setLocked(0);

        $this->wgsTier->getSysObjectService()->getSysProjectService()->editSysProjectService($wgsSysProject);
    }
    //---------------------------------------------------------------------------------------------------------
    
	// ������� �������� �������
	//---------------------------------------------------------------------------------------------------------
	function doRemoveSysProject()
	{
	   $id = $this->request->getPost('id'); 
	   $wgsSysProject = new wgsSysProject();
       $wgsSysProject->setId($id);
       $this->wgsTier->getSysObjectService()->getSysProjectService()->removeSysProjectService($wgsSysProject);
	}
    //---------------------------------------------------------------------------------------------------------
	
	// ������� ������ ����� � ���� ������
	//---------------------------------------------------------------------------------------------------------------------
    function doGetRoleList()
    {
    	$SysRoleList = $this->wgsTier->getSysRoleService()->getHighestSysRoles();
    	if (count($SysRoleList) > 0) {
    	   $roleArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ �����'), 'expanded' => true);    
    	} else {
    	    $roleArray = array('id' => 'root','text' => lmb_win1251_to_utf8('������ �����'), 'expanded' => true, 'leaf' => 'false');
    	}
    	
		    	
		foreach($SysRoleList as $SysRole)
        {
        	$SysRoleId = $SysRole->getId();
        	$SysRoleChildList = $this->GetRoleTree($SysRoleId);
        	if ($this->wgsTier->getSysRoleService()->getChildrenRolesById($SysRoleId)) 
            $roleleaf = '';
            else $roleleaf = 'true';
            
        	$roleArray['children'][$SysRoleId]['id'] = $SysRoleId;
	        $roleArray['children'][$SysRoleId]['text'] = lmb_win1251_to_utf8($SysRole->getName());
            $roleArray['children'][$SysRoleId]['leaf'] = $roleleaf;
            if ($roleleaf == 'true') $roleArray['children'][$SysRoleId]['expanded'] = false;
            else $roleArray['children'][$SysRoleId]['expanded'] = true;
	        $roleArray['children'][$SysRoleId]['name'] = lmb_win1251_to_utf8($SysRole->getName());
	        
	        if ($SysRoleList = $this->wgsTier->getSysRoleService()->getChildrenRolesById($SysRoleId))
	        $roleArray['children'][$SysRoleId]['children'] = $SysRoleChildList;
        }
        
        $roleArray['children'] = array_merge($roleArray['children']);
        $jsonRoleArray = array();
        $jsonRoleArray[]=$roleArray;
        $this->response->write(json_encode($jsonRoleArray));
    }
	function GetRoleTree($SysRoleId)
    {
    		$count = 0;
	    	$SysRoleChildArray = array();
	    	$SysRoleChildList = $this->wgsTier->getSysRoleService()->getChildrenRolesById($SysRoleId);
	    	if ($SysRoleChildList) 
	    	{
	    		foreach($SysRoleChildList as $SysRoleChild)
	        	{
		    		$SysRoleChildArray[$count]['id'] = $SysRoleChild->getId();
		    		if ($this->wgsTier->getSysRoleService()->getChildrenRolesById($SysRoleChild->getId())) 
                    $roleleaf = '';
                    else $roleleaf = 'true';
                    
	                $SysRoleChildArray[$count]['text'] = lmb_win1251_to_utf8($SysRoleChild->getName());
                    $SysRoleChildArray[$count]['leaf'] = $roleleaf;
                    if ($roleleaf == 'true') $SysRoleChildArray[$count]['expanded'] = false;
                    else $SysRoleChildArray[$count]['expanded'] = true;
	                $SysRoleChildArray[$count]['name'] = lmb_win1251_to_utf8($SysRoleChild->getName());
		    		
		    		if ($count+1 == count($SysRoleChildList))
		    		{
		    			for ($i = 0; $i < count($SysRoleChildArray); $i++)
		    				if ($this->wgsTier->getSysRoleService()->getChildrenRolesById($SysRoleChildArray[$i]['id']))
	    					$SysRoleChildArray[$i]['children'] = $this->GetRoleTree($SysRoleChildArray[$i]['id']);
		    		}
		    		$count++;
	        	}
	        	return $SysRoleChildArray;
	    	}
	}
    //---------------------------------------------------------------------------------------------------------------------
	
	function doDisplay()
    {
        $topmenu = $this->createTopMenu();
        //var_dump($topmenu);
        //echo '���';
        //die();
        $this->setTemplate('main.html');
        //ini_set('display_errors',1);
        //echo ":: WGS3 :: ����������������� ������� ::";
        //die();
        
        $this->passToView("title", ":: WGS3 :: ����������������� ������� ::");
        $this->passToView("body", $topmenu);
        
        $this->startFrontEndApplication(array(
        '/js/wgs/systemadmin/systemobjectadmin.js',
        '/js/wgs/systemadmin/systemroleadmin.js',
        '/js/wgs/systemadmin/systemuseradmin.js',
        '/js/wgs/systemadmin.js'), 'Systemadmin');
		//echo '�����������������';
		//echo '�����������������';
    }

}
 
?>
