<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');

class ObjectAdminController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->wgsTier->commitSession();
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-Функция, вызывающая модуль-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function doDisplay()
    {
        $topmenu = $this->createTopMenu();
        $this->setTemplate('main.html');
        $this->passToView("title", ":: WGS3 :: Управление данными ::");
        $this->passToView("body", $topmenu);
        $this->startFrontEndApplication(array(
            '/js/wgs/map/commands.js',
            '/js/wgs/map/viewer.js',
            '/js/wgs/map/viewerapi.js',
            '/js/wgs/map/viewerlib.js',
            '/js/wgs/map/raphaeldrawing.js',
            '/js/wgs/map/card.js',
            '/js/wgs/map/search.js',
            '/js/wgs/map/bind.js',
            '/js/wgs/map/search.js',
            '/js/wgs/map/floorplan.js',
            '/js/wgs/map/featuredoc.js',
            '/js/wgs/objectadmin.js',
            '/js/wgs/map/cardNew.js'), 'Objectadmin');
    }

    function getFeatureLayerByGroupName($layerName) {
        $layers = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetLayersByGroupName($layerName);
        $layerDesc = array();
        foreach($layers as $layer) {
            if($layer -> get('featurelayer_id') != null) {
                $layerDesc[] = array(
                    'layer_id' => $layer -> get('featurelayer_id')
                );
            }
        }

        return $layerDesc;
    }

    function getObjectsByName($layerName, $objectName) {
        $objects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectsByFeatureName($layerName, $objectName);
        $objectDesc = array();

        foreach($objects as $object) {
            if($object -> get('object_id') != null) {
                $objectDesc[] = array(
                    'objectId' => $object -> get('object_id'),
                    'name' => $object -> get('name'),
                    'objectTypeId' => $object -> get('object_type_id'),
                    'relationId' => $object -> get('objtype_relation_id')
                );
            }
        }

        return $objectDesc;
    }

    function unbindAllStudents() {
		$this->wgsTier->getProjectObjectService()->getAttObjectService()->unbindStudents();
	}
	function updateProps() {
		$this->wgsTier->getProjectObjectService()->getAttObjectService()->updateProps();
	}

    // function doGetObjectsByName() {
    //     $name = decode($this->request->get('name'));
    //     $layername = 25;

    //     // var_dump($this -> getObjectsByName('25', '249'));
    // }

    // function doGetFeatureLayerByName() {
    //     $layerName = decode($this->request->get('layer'));
    //     $layerDesc = $this -> getFeatureLayerbyName($layerName);
    //     // $this -> response -> write($layerDesc);
    //     // var_dump($layerDesc);
    // }

    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    //*/*/*/*/*/*/*/*/*/СОХРАНЕНИЕ И ЗАГРУЗКА АТРИБУТИВНОГО ОПИСАНИЯ В XML/*/*/*/*/*/*/*/
    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    function doSaveAttrTypeToXML()
    {
        $objectTypesId = $this->request->getPost('typesId');
        if (count($objectTypesId)>0)
        foreach($objectTypesId as $objectTypeId)
        {
            //$objectTypeId = $this->request->getPost('objectTypeId');
            $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->Get($objectTypeId,$objectTypeName,$objectTypeShortName,$objectTypeNote,$objectTypeAddDate,$objectTypeLabel);
            $properties = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($objectTypeId,null);

            $dom = new DOMDocument('1.0', 'UTF-8');
            $root = $dom->appendChild(new DOMElement('root'));
            $node = $dom->createElement('ObjectType');
            $typeNode = $root->appendChild($node);
            $typeNode->setAttribute('id', $objectTypeId);
            $typeNode->setAttribute('name', encode($objectTypeName));
            $typeNode->setAttribute('shortname', encode($objectTypeShortName));
            $typeNode->setAttribute('note', encode($objectTypeNote));
            $typeNode->setAttribute('adddate', $objectTypeAddDate);
            $typeNode->setAttribute('objectlabel', encode($objectTypeLabel));
            //$typeNode = $root->createElement('para');
            $count = 0;
            foreach($properties as $property)
            {
                $node = $dom->createElement('Property');
                $propertyNode = $typeNode->appendChild($node);
                $propertyNode->setAttribute('id', $property->get('Property_Id'));
                $propertyNode->setAttribute('name', encode($property->get('Name')));
                $propertyNode->setAttribute('autoCADname', encode($property->get('CAD_NAME')));
                $propertyNode->setAttribute('acadview', $property->get('ACAD_VIEW'));
                $propertyNode->setAttribute('groupid', $property->get('Properties_group_id'));
                $propertyNode->setAttribute('groupname', encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($property->get('Properties_group_id'))));
                $propertyNode->setAttribute('valuetypeid', $property->get('Value_properties_type_id'));
                $propertyNode->setAttribute('valuetypename',  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($property->get('Value_properties_type_id'))));
                $propertyNode->setAttribute('placdate', $property->get('placement_date'));
                $propertyNode->setAttribute('grouppriority', $property->get('Group_Priority'));
                $propertyNode->setAttribute('priority', $property->get('Priority'));
            }

            $codeXML = base64_encode($dom->saveXML());
            $textXML = base64_decode($codeXML);
            /*echo $codeXML;
            echo $textXML;*/
            $dom->save(WGS3_DOCCACHE_DIR.$objectTypeName.".xml");
            $fpw = @fopen(WGS3_DOCCACHE_DIR.$objectTypeName.".attr","w+");
            $nBuffer = 8192;
              for ($i=0; $i<strlen($codeXML); $i+=$nBuffer)
                @fwrite($fpw,substr($codeXML,$i,($i+$nBuffer)),$nBuffer);
            @fclose ($fpw);

            $this->response->write(encode($objectTypeName).".attr");
        }
    }

    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/РАБОТА С АТРИБУТИВНЫМИ ОБЪЕКТАМИ/*/*/*/*/*/*/*/*/*/*/
    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-Функции для работы с характеристиками объекта -*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function doGetValueProperties()
    {
        $objectId = $this->request->getPost('objectId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);



        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {//id name shortName typeId typeName invNumber createDate note author label
            $objectList[$count]['id'] = $object->get('property_id');
            //echo '1 ';
            $objectList[$count]['name'] = encode($object->get('property_name'));
            //echo '1 ';
            $objectList[$count]['vulueTypeId'] = $object->get('value_properties_type_id');
            //echo '1 ';
            $objectList[$count]['groupId'] = $object->get('property_group_id');
            //echo '1 ';
            $objectList[$count]['groupName'] = encode($object->get('property_group_name'));
            //echo '1 ';
            $objectList[$count]['value'] = encode($object->get('property_value'));//;html_entity_decode(htmlentities(lmb_win1251_to_utf8($object->get('property_value')), ENT_NOQUOTES | ENT_IGNORE, 'CP1251'), ENT_NOQUOTES | ENT_IGNORE, 'UTF-8');
            //echo '1 ';
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
        //var_dump($objectList);
    }

    function doSetCards(){
        $user = $this->wgsTier->getAuthorizedUser();

        $ObjectEditUser = 'источник : ПО интеллектуального анализа данных;  пользователь : '.$user->getName().'; хост : '.$_SERVER['REMOTE_ADDR'];
        $data = $this->request->getPost('data');
        $data = CJSON::decode($data);
        for($i=0;$i<count($data);$i++){
            $objectId = $data[$i]['id'];
            $post = str_replace(',','.',$data[$i]);
            $this->doSetCard($objectId, $ObjectEditUser, $post);
        }

    }
    function doSetCard($objectId = null ,$ObjectEditUser = null, $post = null)
    {
        // ini_set('display_errors', 1);
        // error_reporting(E_ALL);
        $user = $this->wgsTier->getAuthorizedUser();

        $objectRelationId = $this->request->getPost('objectRelationId');
        if(!$objectId){
            $objectId = $this->request->getPost('id');
            $ObjectEditUser = decode('источник : ГИС;  пользователь : '.$user->getName().'; хост : '.$_SERVER['REMOTE_ADDR']);
        }


        $objectName = decode($this->request->getPost('name'));
        $objectShortName = decode($this->request->getPost('shortName'));
        $objectNote = decode($this->request->getPost('note'));

        $User = $user->getId();
        $Host = $_SERVER['REMOTE_ADDR'];
        $on = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypeNameByObject($objectId);
        $n = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getName($objectId);


        /*
         * if($n!='' || $objectName!='')
            $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','card','edit_card');
        if($liId){
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_edit',decode($objectName));
            if(decode($n)!=$objectName)
                $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_before_edit',decode($n));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_short_name_edit',decode($objectShortName));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_type',decode($on));
        }*/

        // var_dump($ObjectEditUser);
        // var_dump($objectName);
        // var_dump($objectShortName);
        // var_dump($objectNote);
        // var_dump($ObjectEditUser);


        $this->wgsTier->getProjectObjectService()->getAttObjectService()->Set($objectId,$objectName,$objectShortName,$objectNote,$ObjectEditUser);

        // die();

        if(!$post)
            $post = $this->request->getPost();
        foreach($post as $key => $propertyValue)
        {
            if (strstr($key,"property"))
            {
                $mas = explode("-", $key);
                $propertyId = $mas[1];
                // var_dump($propertyValue);
                // die();
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($objectId,$propertyId,decode($propertyValue));
            }
        }
        foreach($post as $key => $propertyValue)
        {
            if (strstr($key,"propRel"))
            {
                $mas = explode("-", $key);
                $propertyId = $mas[1];
                $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->SetValuePropertyRelation($objectRelationId,$propertyId,decode($propertyValue));
            }
        }

    }

    function setCardPropertiesFromRequest($objectId, $post = array()) {
        foreach($post as $key => $propertyValue)
        {

            if (strstr($key,"property"))
            {
                $mas = explode("-", $key);
                $propertyId = $mas[1];
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($objectId,$propertyId,decode($propertyValue));
            }
        }
        foreach($post as $key => $propertyValue)
        {
            if (strstr($key,"propRel"))
            {
                $mas = explode("-", $key);
                $propertyId = $mas[1];
                $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->SetValuePropertyRelation($objectRelationId,$propertyId,decode($propertyValue));
            }
        }

    }

    function addCard($objectTypeId, $objectName, $objectShortName, $objectNote, $ObjectEditUser, $postLikeData) {
        $objectId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->Add($objectTypeId);

        if($objectId == null) {
            return false;
        }

        $this -> wgsTier -> getProjectObjectService() -> getAttObjectService() -> Set($objectId, $objectName, $objectShortName, $objectNote, $ObjectEditUser);
        $this -> setCardPropertiesFromRequest($objectId, $postLikeData);
        return $objectId;

    }

    function doAddCard() {
         $objectTypeId = $this->request->get('objectType');
         if($objectTypeId == null) {
             return;
         }

         $objectName = decode($this->request->get('name'));
         $objectShortName = decode($this->request->get('shortName'));
         $objectNote = decode($this->request->get('note'));
         $ObjectEditUser = $this->wgsTier->getAuthorizedUser()->getName();

         return $this -> addCard($objectTypeId, $objectName, $objectShortName, $objectNote, $ObjectEditUser, $this -> request -> getRequest() );
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-Функции для работы со списком объектов-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function doGetListObjectByTypesPage()
    {
        $typesIdMas = $this->request->getPost('typesId');
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 33;
        if (count($typesIdMas)>0)
        {
            $typesId = implode(",", $typesIdMas);

            $r = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetListObjectByTypes($typesId, $start, $limit, $totalCount);

            if (count($r)) {
                $result = array();
                $result['success'] = true;
                $result['totalCount'] = $totalCount;
                foreach($r as $object) {
                    $result['rows'][] = array(
                    'id' => $object->get('OBJECT_ID'),
                    'name' => encode($object->get('NAME')),
                    'shortName' => encode($object->get('SHORT_NAME')),
                    'typeId' => $object->get('OBJECT_TYPE_ID'),
                    'typeName' => encode($object->get('object_type_name')),
                    'invNumber' => encode($object->get('INVENTARY_NUMBER')),
                    'createDate' => $object->get('CREATE_DATE'),
                    'note' => encode($object->get('NOTE')),
                    'edit_user' => encode($object->get('EDIT_USER')),
                    'label' => encode( $object->get('LABEL') )
                    );
                }
            } else {
                $result['success'] = false;
                $result['totalCount'] = 0;
            }
            //return $result;
            $this->response->write(json_encode($result));
        }
    }

    function doGetListObjectByTypes()
    {
        $typesIdMas = $this->request->getPost('typesId');
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 30;
        if (count($typesIdMas)>0)
        {
            $typesId = implode(",", $typesIdMas);
            $result = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetListObjectByTypes($typesId, $start, $limit, $totalCount);
            $objectList = array();
            $count = 0;
            foreach($result as $object)
            {//id name shortName typeId typeName invNumber createDate note author label
                $objectList[$count]['id'] = $object->get('OBJECT_ID');
                $objectList[$count]['name'] = encode($object->get('NAME'));
                $objectList[$count]['shortName'] = encode($object->get('SHORT_NAME'));
                $objectList[$count]['typeId'] = $object->get('OBJECT_TYPE_ID');
                $objectList[$count]['typeName'] = encode($object->get('object_type_name'));
                $objectList[$count]['invNumber'] = encode($object->get('INVENTARY_NUMBER'));
                $objectList[$count]['createDate'] = $object->get('CREATE_DATE');
                $objectList[$count]['note'] = encode($object->get('NOTE'));
                $objectList[$count]['edit_user'] = encode($object->get('EDIT_USER'));
                $objectList[$count]['label'] = $object->get('LABEL');
                $count = $count + 1;
            }
            $this->response->write(json_encode($objectList));
        }
    }

    function doAddObject()
    {
        $typeId = $this->request->getPost('typeId');
        $objectId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->Add($typeId);
        print(json_encode(array('objectId'=>$objectId)));
    }

    function doCreateAndBindObjectToFeature()
    {
        $objectTypeId  = $this->request->getPost('objectTypeId');
        $featureId = $this->request->getPost('featureId');

        $objectId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->Add($objectTypeId);
        // var_dump($objectId);
        // die();

        if ($objectId && $featureId) {
            $this->wgsTier->getProjectObjectService()->getAttObjectService()->addRelationByFeature(null, $objectId, $featureId, 0);
        }
    }

    function doRemoveObject()
    {
        $objectsId = $this->request->getPost('objectsId');
        $removeMode = $this->request->getPost('removeMode');
        if (count($objectsId)>0) {
            foreach($objectsId as $objectId) {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->Remove($objectId,$removeMode);
            }
        }
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-Функции для работы со связями объектов атрибутики -*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function doGetListRelByObject()
    {
        $objectId = $this->request->getPost('objectId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelByObject($objectId);
        // $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByParent($value['objectTypeId']);

        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['relationId'] = $object->get('relation_id');
            $objectList[$count]['name'] = encode($object->get('name'));
            $objectList[$count]['relationTypeId'] = $object->get('relation_type_id');
            $objectList[$count]['countTypeRelation'] = $object->get('count_type_relation');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doGetListChildObjectByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        $parentObjectId = $this->request->getPost('objectId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectByRelation($relationId, $parentObjectId);
        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('OBJECT_ID');
            $objectList[$count]['name'] = encode($object->get('NAME'));
            $objectList[$count]['shortName'] = encode($object->get('SHORT_NAME'));
            $objectList[$count]['typeId'] = $object->get('OBJECT_TYPE_ID');
            $objectList[$count]['typeName'] = encode($object->get('object_type_name'));
            $objectList[$count]['invNumber'] = encode($object->get('INVENTARY_NUMBER'));
            $objectList[$count]['createDate'] = $object->get('CREATE_DATE');
            $objectList[$count]['note'] = encode($object->get('NOTE'));
            $objectList[$count]['edit_user'] = encode($object->get('EDIT_USER'));
            $objectList[$count]['label'] = $object->get('LABEL');
            $objectList[$count]['ObjTypeRelationId'] = $object->get('ObjType_relation_id');
            $objectList[$count]['objRelationId'] = $object->get('Object_relation_id');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doGetListChildObjectTypeByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        //$objectId = $this->request->getPost('objectId');
        //if ($objectId) {
        //    $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectTypeByRelEx($relationId, $objectId);
        //} else {
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectTypeByRelation($relationId);
        //}
        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('OBJECT_TYPE_ID');
            $objectList[$count]['name'] = encode($object->get('object_type_name'));
            $objectList[$count]['objTypeRelationId'] = encode($object->get('ObjType_relation_id'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doCreateChildObject($objTypeRelationId = null, $parentObjectId = null, $objectTypeId = null)
    {
        $isReq = false;
        if($objTypeRelationId == null) {
            $isReq = true;
            $objTypeRelationId = $this->request->getPost('objTypeRelationId');
            $parentObjectId = $this->request->getPost('parentObjectId');
            $parentFeaturesIds = $this->request->getPost('parentFeaturesIds');
            $objectTypeId = $this->request->getPost('objectTypeId');
        }
        $childObjectId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->Add($objectTypeId);
        if ($parentObjectId)
            $objectRelationId = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId);
        else
        if ($parentFeaturesIds)
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjectRelationEx($parentFeaturesIds,$childObjectId,$objTypeRelationId);
        $result = array('childObjectId'=>$childObjectId, 'objectRelationId'=>$objectRelationId);
        if($isReq){
            print(json_encode($result));
        }
        else return $result;
    }


    function bindChildObject($objTypeRelationId, $parentObjectId, $childObjectId) {
        return $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId);
    }

    function doBindChildObject()
    {
        $objTypeRelationId = $this->request->getPost('objTypeRelationId');
        $parentObjectId = $this->request->getPost('parentObjectId');
        $childObjectId = $this->request->getPost('childObjectId');
        $parentFeatureIds = $this->request->getPost('parentFeatureIds');
        if ($parentObjectId)
            $objectRelationId = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId);
        else
        if ($parentFeatureIds)
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjectRelationEx($parentFeatureIds,$childObjectId,$objTypeRelationId);
    }

    function doGetValuePropertiesByObjRel()
    {
        $objectRelationId = $this->request->getPost('objectRelationId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetValuePropertiesByObjRel($objectRelationId);
        $objectList = array();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('property_id');
            $objectList[$count]['name'] = encode($object->get('property_name'));
            $objectList[$count]['vulueTypeId'] = $object->get('value_properties_type_id');
            $objectList[$count]['groupId'] = $object->get('property_group_id');
            $objectList[$count]['groupName'] = encode($object->get('property_group_name'));
            $objectList[$count]['value'] = encode($object->get('property_value'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doRemoveObjectRelation()
    {
        $wgsVersion = $this->request->getPost('WgsVersion');

        $relationsId = $wgsVersion >= 4 ? json_decode($this->request->getPost('relationsId')) : $this->request->getPost('relationsId');
        $removeMode = $this->request->getPost('removeMode');
        $childObjectIds = $this->request->getPost('childObjectIds');
        $parentFeatureIds = $this->request->getPost('parentFeatureIds');
        $objectTypeRelationIds = $this->request->getPost('objectTypeRelationIds');
        if (count($relationsId)>0) {
            foreach($relationsId as $relationId)
            {
                $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->RemoveObjectRelation($relationId,$removeMode);
            }
        } else
        if (count($childObjectIds) > 0 && count($parentFeatureIds) > 0 && count($objectTypeRelationIds) > 0) {
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->RemoveObjectRelationEx($childObjectIds,$parentFeatureIds,$objectTypeRelationIds,$removeMode);
        }
    }
    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
    //*/*/*/*/*/*/РАБОТА С АТРИБУТИВНЫМИ ОПИСАНИЯМИ СЛОЯМИ И ХАРАКТЕРИСТИКАМИ /*/*/*/*/*/
    //*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-Функции для работы со связями между атрибутикой -*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Функция получения связей с другими атрибутивными типами
    function doGetObjTypeRelation()
    {
        $childObjTypeId = $this->request->getPost('childObjTypeId');
        $parentObjTypeId = $this->request->getPost('parentObjTypeId');
        if ($parentObjTypeId != null)
        {
            $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByParent($parentObjTypeId);
        }
        if ($childObjTypeId != null)
        {
            $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByChild($childObjTypeId);
        }
        /*
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['objTypeRelationId'] = $object->get('ObjType_relation_id');
            $objectList[$count]['parentObjectTypeId'] = $object->get('Parent_object_type_id');
            $objectList[$count]['childObjectTypeId'] = $object->get('Child_object_type_id');
            $objectList[$count]['relationId'] = $object->get('Relation_id');
            $objectList[$count]['parentName'] = lmb_win1251_to_utf8($object->get('Parent_Name'));
            $objectList[$count]['childName'] = lmb_win1251_to_utf8($object->get('Child_Name'));
            $objectList[$count]['relationName'] = lmb_win1251_to_utf8($object->get('Relation_Name'));
            $objectList[$count]['relationTypeName'] = lmb_win1251_to_utf8($object->get('Relation_Type_Name'));
            $objectList[$count]['relationType'] = $object->get('relation_type_id');
            $count = $count + 1;
        }
        */
        foreach($result as $object)
        {
            $relationId = $object->get('Relation_id');
            if($relationId) {
                $objectList[$relationId]['objTypeRelationId'] = $object->get('ObjType_relation_id');
                $objectList[$relationId]['parentObjectTypeId'] = $object->get('Parent_object_type_id');
                $objectList[$relationId]['childObjectTypeId'] = $object->get('Child_object_type_id');
                $objectList[$relationId]['relationId'] = $object->get('Relation_id');
                $objectList[$relationId]['parentName'] = encode($object->get('Parent_Name'));
                $objectList[$relationId]['childName'] = ($objectList[$relationId]['childName']?$objectList[$relationId]['childName'].", ".encode($object->get('Child_Name')): encode($object->get('Child_Name')));
                $objectList[$relationId]['relationName'] = encode($object->get('Relation_Name'));
                $objectList[$relationId]['relationTypeName'] = encode($object->get('Relation_Type_Name'));
                $objectList[$relationId]['relationType'] = $object->get('relation_type_id');
            }
        }
        $this->response->write(json_encode(array_merge($objectList)));
    }
    //Функция получения списка связей по ID связи
    function doGetObjTypeRelationByRelation($relationId)
    {
        $relationId = $this->request->getPost('relationId');
        $this->response->write(json_encode($this->doGetObjTypeRelationByRelationId($relationId)));
    }

    //Функция получения списка связей по ID связи
    function doGetObjTypeRelationByRelationId($relationId)
    {
        $objectList = array();
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelation($relationId);
        foreach($result as $object)
        {
            if($object->get('ObjType_relation_id')) {
                $objectList[] = array(
                    'objTypeRelationId' => $object->get('ObjType_relation_id'),
                    'parentObjectTypeId' => $object->get('Parent_object_type_id'),
                    'childObjectTypeId' => $object->get('Child_object_type_id')
                    );
            }
        }
        return $objectList;
    }

    //Функция получения списка характеристик для связи
    //GetListPropertyByRelation($relationId)
    function doGetListPropertyByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListPropertyByRelation($relationId);
        $count = 0;
        $objectList = array();
        foreach($result as $object)
        {
            if($object->get('property_id')) {
                $objectList[$count]['id'] = $object->get('property_id');
                $objectList[$count]['priority'] = $object->get('priority');
                $objectList[$count]['name'] = encode($object->get('Name'));
                $objectList[$count]['groupid'] = $object->get('Properties_group_id');
                $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($object->get('Properties_group_id')));
                $objectList[$count]['valuetypeid'] = $object->get('Value_properties_type_id');
                $objectList[$count]['valuetypename'] =  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($object->get('Value_properties_type_id')));
                $objectList[$count]['placdate'] = $object->get('placement_date');
                $count = $count + 1;
            }
        }
        $this->response->write(json_encode($objectList));
    }
    //Функция получения списка характеристик не для связи
    //GetListPropertyNotByRelation($relationId)
    function doGetListPropertyNotByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListPropertyNotByRelation($relationId);
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('property_id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $objectList[$count]['groupid'] = $object->get('Properties_group_id');
            $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($object->get('Properties_group_id')));
            $objectList[$count]['valuetypeid'] = $object->get('Value_properties_type_id');
            $objectList[$count]['valuetypename'] =  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($object->get('Value_properties_type_id')));
            $objectList[$count]['placdate'] = $object->get('placement_date');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }
    //Привязка характеристик
    function doAddPropetyByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        $propertiesId = $this->request->getPost('propertiesId');
        if (count($propertiesId)>0)
        foreach($propertiesId as $propertyId)
        {
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddPropetyByRelation($relationId,$propertyId);
        }
    }
    //Отвязка характеристик
    function doRemovePropetyByRelation()
    {
        $relationId = $this->request->getPost('relationId');
        $propertiesId = $this->request->getPost('propertiesId');
        if (count($propertiesId)>0)
        foreach($propertiesId as $propertyId)
        {
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->RemovePropetyByRelation($relationId,$propertyId);
        }
    }

    //Функция удаления связи между атрибутивными описаниями
    function doRemoveObjTypeRelation()
    { //$ParentObjectTypeId,$ChildObjectTypeId,$RelationId
        $objTypeRelationId = $this->request->getPost('objTypeRelationId');
        foreach($objTypeRelationId as $id)
        {
            $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->RemoveObjTypeRelation($id,0);
        }
    }

//Функция удаления-добавления связи между атрибутивными описаниями по данным о ней
    function doAddRemoveObjTypeRelationByData()
    { //$ParentObjectTypeId,$ChildObjectTypeId,$RelationId
        $parentObjectTypeId = $this->request->getPost('parentObjectTypeId');
        $addChildObjectTypeId = $this->request->getPost('addChildObjectTypeId');
        $removeChildObjectTypeId = $this->request->getPost('removeChildObjectTypeId');
        $relationId = $this->request->getPost('relationId');
        if (count($removeChildObjectTypeId)>0)
            foreach($removeChildObjectTypeId as $childId)
            {
                $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->RemoveObjTypeRelationByData($parentObjectTypeId,$childId,$relationId);
            }
        if (count($addChildObjectTypeId)>0)
            foreach($addChildObjectTypeId as $childId)
            {
                $objTypeRelationId = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddObjTypeRelation($parentObjectTypeId,$childId,$relationId);
            }
    }
//Функция создания связи
    function doAddRelation()
    { //$ParentObjectTypeId,$ChildObjectTypeId,$RelationId
        $name = decode($this->request->getPost('relationName'));
        $relationTypeId = $this->request->getPost('relationType');
        $relationId = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->AddRelation($name,$relationTypeId);
        print($relationId);
    }
//Функция редактирования связи
    function doSetRelation()
    { //$ParentObjectTypeId,$ChildObjectTypeId,$RelationId
        $name = decode($this->request->getPost('relationName'));
        $relationId = $this->request->getPost('relationId');
        $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->SetRelation($relationId,$name);
    }

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-Функции для работы со связями между слоем и описанием-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

function doGetListUnBindObjectDateByType()
{
    $layerid = $this->request->getPost('layerid');
    $typerid = $this->request->getPost('typeid');
    $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListUnBindObjectDateByType($layerid, $typerid);
    $count = 0;
    foreach($result as $object)
    {
        $objectList[$count]['unBindDate'] = encode($object->get('unbind_date').' - ('.$object->get('count_unbind_date')).' объектов)';
        $count = $count + 1;
    }
    $this->response->write(json_encode($objectList));
}
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-Функции для работы с атрибутивными описаниями-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Функция получения списка атрибутивных описаний
    function doGetAttrTypeList() {
        $relationId = $this->request->getPost('relationId');
        $objectList = array();
        if ($relationId == null)
        {
            $layerid = $this->request->getPost('layerId');
            if ($layerid == null)
            {
                $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer(null);
            }
            if ($layerid != null)
            {
                $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($layerid);
            }
            $count = 0;
            //ot.Object_Type_id, ot.Name, ot.Short_Name, ot.Add_date, ot.Note
            foreach($result as $object)
            {


                if($object->get('Object_Type_id')) {
                    $objectList[] = array();
                    $objectList[$count]['id'] = $object->get('Object_Type_id');
                    $objectList[$count]['name'] = encode($object->get('Name'));
                    if ($object->get('is_current')==1)
                    {
                        $layerCurrent = "font-weight: bold;font-style: italic;";
                        $objectList[$count]['nameTag'] = encode('<span style="'.$layerCurrent.'">'.$object->get('Name').'</span>');
                    }
                    else
                    {
                        //$objectList[$count]['nameTag'] = lmb_win1251_to_utf8($object->get('Name'));
                        $layerCurrent = "font-weight: normal;font-style: normal;";
                        $objectList[$count]['nameTag'] = encode('<span style="'.$layerCurrent.'">'.$object->get('Name').'</span>');
                    }
                    $objectList[$count]['shortname'] = encode($object->get('Short_Name'));
                    $objectList[$count]['adddate'] = $object->get('Add_date');
                    $objectList[$count]['note'] = encode($object->get('Note'));
                    $objectList[$count]['label'] = encode($object->get('Label'));
                    $count = $count + 1;
                }
            }
        }
        else
        {
            $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectTypeByRelation($relationId);
            $objectList = array();
            $count = 0;
            foreach($result as $object)
            {
                $objectList[$count]['id'] = $object->get('OBJECT_TYPE_ID');
                $objectList[$count]['name'] = encode($object->get('object_type_name'));
                $count = $count + 1;
            }
        }
        $this->response->write(json_encode($objectList));
    }
    //Функция получения списка атрибутивных описаний НЕ привязанных к определенному слою
    function doGetAttrTypeListNotByLayer()
    {
        $layerid = $this->request->getPost('layerId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeNotByLayer($layerid);
        $count = 0;
        //ot.Object_Type_id, ot.Name, ot.Short_Name, ot.Add_date, ot.Note
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('Object_Type_id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $objectList[$count]['shortname'] = encode($object->get('Short_Name'));
            $objectList[$count]['adddate'] = $object->get('Add_date');
            $objectList[$count]['note'] = encode($object->get('Note'));
            $objectList[$count]['label'] = $object->get('Label');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doEditAttrType()
    {
        $id = $this->request->getPost('id');
        $name = decode($this->request->getPost('name'));
        $shortname = decode($this->request->getPost('shortname'));
        $note = decode($this->request->getPost('note'));
        $adddate = $this->request->getPost('adddate');

        $objectLabel = $this->request->getPost('objectLabel');
        $propnamelabel = $this->request->getPost('propnamelabel');
        if ($objectLabel == -4)
        {
            $objectLabel = $propnamelabel;
        }


        $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->Set($id,$name,$shortname,$note,$adddate,$objectLabel);
    }
    function doAddAttrType()
    {
        $name = decode($this->request->getPost('name'));
        $shortname = decode($this->request->getPost('shortname'));
        $note = decode($this->request->getPost('note'));
        $adddate = $this->request->getPost('adddate');
        $objectLabel = $this->request->getPost('objectLabel');
        $propnamelabel = $this->request->getPost('propnamelabel');
        if ($objectLabel == -4)
        {
            $objectLabel = $propnamelabel;
        }
        $objectid = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->Add($name,$shortname,$note,$adddate,$objectLabel);
        $User = $this->wgsTier->getAuthorizedUser()->getId();
        $Host = $_SERVER['REMOTE_ADDR'];

        $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','objectadmin','create_object');
        if($liId){
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'object_name_add',decode($name));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'object_shortname_add',decode($shortname));
        }

        print($objectid);
    }
    function doRemoveAttrType()
    {
        $id = $this->request->getPost('id');
        $User = $this->wgsTier->getAuthorizedUser()->getId();
        $Host = $_SERVER['REMOTE_ADDR'];
        // $liId = $this->wgsTier->getProjectTier()->getAuditService()->removeObjectType($id,$User,$Host);

        $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->Remove($id, 1);
    }


    function doAddPropertyByObjectType()
    {
        $aCadView = 0;
        $typeid = decode($this->request->getPost('typeid'));
        $propertiesId = $this->request->getPost('propertiesId');
        if ($this->request->getPost('aCadView') == 'true')
        {
            $aCadView = 1;
        }
        if (count($propertiesId)>0)
        foreach($propertiesId as $propertyId)
        {
            $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->AddPropertyByObjectType($typeid,$propertyId,$aCadView);
        }
    }
    function doRemovePropertyByObjectType()
    {

        $propertyid = decode($this->request->getPost('propertyid'));

        $typeid = decode($this->request->getPost('typeid'));
        $aCadViews = $this->request->getPost('aCadView');
        $propertiesId = $this->request->getPost('propertiesId');
        if (count($propertiesId)>0)
        for ($i=0;$i<count($propertiesId);$i++)
        {
            $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->RemovePropertyByObjectType($typeid,$propertiesId[$i],$aCadViews[$i]);
        }
    }
    function doSetPropertyByObjectType()
    {
        $typeid = decode($this->request->getPost('typeid'));
        $propertyid = decode($this->request->getPost('propertyid'));
        $aCadView = decode($this->request->getPost('aCadView'));
        $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->SetPropertyByObjectType($typeid,$propertyid,$aCadView);
    }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-Функции для работы с группами слоев-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Функция получения списка групп слоев
    function doGetLayerGroupList()
    {
        $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupListEx();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('FeatureLayerGroup_Id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }
    //Функция добавления группы слоев
    function doAddLayerGroup()
    {
        $name = $this->request->getPost('name');
        $objectid = $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Add(decode($name));
        print($objectid);
    }
    //Функция удаления группы слоев
    function doRemoveLayerGroup()
    {
        $objectid = $this->request->getPost('objectid');
        $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Remove($objectid);
    }
    //Функция редактирования группы слоев
    function doEditLayerGroup()
    {
        $objectid = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Set($objectid,decode($name));
    }



    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-Функции для работы со слоями-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function getFeatureLayerList($typeId, $groupid) {
        if (($typeId == null)&&($groupid == null))
        {
            $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType(null);
        }
        if (($typeId != null)&&($groupid == null))
        {
            $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType($typeId);
        }
        if (($typeId == null)&&($groupid != null))
        {
            $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByGroup($groupid);
        }
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('FeatureLayer_Id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $objectList[$count]['groupid'] = $object->get('FeatureLayerGroup_Id');
            $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Get($object->get('FeatureLayerGroup_Id')));
            $count = $count + 1;
        }
        return $objectList;
    }

    function doGetFeatureLayerList()
    {
        $typeId = $this->request->getPost('typeId');
        $groupid = $this->request->getPost('groupId');
        $this->response->write(json_encode(getFeatureLayerList  ));
    }
    //Функция добавления слоя
    function doAddFeatureLayer()
    {

        $name = $this->request->getPost('name');
        $cs = $this->request->getPost('cs');
        $groupId = $this->request->getPost('groupid');
        $objectid = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->Add(decode($name),$groupId,$cs, null);
        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->addTrigger($objectid);
           if ($objectid && $name) {

            $User = $this->wgsTier->getAuthorizedUser()->getId();
            $Host = $_SERVER['REMOTE_ADDR'];

            $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','objectadmin','create_layer');
            if($liId)
                $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'featurelayer_add',decode($name));

            $this->updateMgLayers($name);
            $this->updateFeatureSources();
            return $objectid;
        }
    }
    //восстановление связи, когда уже создан MapGuide-слой, ссылающийся на несуществующий слой в Oracle
    function updateMgLayers($featureName)
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $mgMapLayers = $this->wgsTier->getProjectObjectService()->getMgMapLayerList();
        if ($mgMapLayers) {
            foreach ($mgMapLayers as $mgMapLayer) {
                $mgResourceId = new MgResourceIdentifier($mgMapLayer->getMgResourceId());
                $byteContentReader = $resourceService->GetResourceContent($mgResourceId);
                $mgResourceContent = $byteContentReader->ToString();
                $mgResourceContentDOM = DOMDocument::loadXML($mgResourceContent);
                $fName = $mgResourceContentDOM->getElementsByTagName('FeatureName')->item(0)->nodeValue;
                if (strcasecmp("KingOra:$featureName", $fName) == 0) {
                    $this->wgsTier->getProjectObjectService()->getMgLayerService()->setMgLayer($mgMapLayer->getMgResourceId(),$featureName);
                } else
                if ($mgResourceContentDOM->getElementsByTagName('Filter')->length) {
                    $fFilter = $mgResourceContentDOM->getElementsByTagName('Filter')->item(0)->nodeValue;
                    $fFilter = str_replace('&quot;', '"', $fFilter);
                    $fFilter = str_replace('&apos;', "'", $fFilter);
                    if (eregi(".*(ADMPLAYER|\"ADMPLAYER\")[= ]*'(.*)'.*", $fFilter, $regs)) {
                        if (strcasecmp($regs[2], $featureName) == 0) {

                            $this->wgsTier->getProjectObjectService()->getMgLayerService()->setMgLayer($mgMapLayer->getMgResourceId(),$featureName);
                        }
                    }
                }
            }
        }
    }

    //Функция редактирования слоя
    function doEditFeatureLayer()
    {//Set($id,$name,$viewName,$groupid)
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $groupId = $this->request->getPost('groupid');
        $objectid = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->Set($id,decode($name),null,$groupId);

        //подключаемся к репозиторию MapGuide и переименовываем слой
        if ($id && $name) {
            $this->mgTier = lmbToolkit :: instance() -> getMgTier();
            $siteConnection = $this->mgTier->getSiteConnection();
            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
            $mgMapLayers = $this->wgsTier->getProjectObjectService()->getMgMapLayerList();
            if ($mgMapLayers) {
                foreach ($mgMapLayers as $mgMapLayer) {
                    if ($mgMapLayer->getFeatureLayerId() == $id) {
                        $mgResourceId = new MgResourceIdentifier($mgMapLayer->getMgResourceId());
                        $byteContentReader = $resourceService->GetResourceContent($mgResourceId);
                        $byteHeaderReader = $resourceService->GetResourceHeader($mgResourceId);
                        $mgResourceContent = $byteContentReader->ToString();
                        $mgResourceContentDOM = DOMDocument::loadXML($mgResourceContent);
                        $isKingOra = strstr($mgResourceContent,'KingOra')? true: false;
                        if ($isKingOra) {
                            $mgResourceContentDOM->getElementsByTagName('FeatureName')->item(0)->nodeValue = "KingOra:$name";
                        } else {
                            if ($mgResourceContentDOM->getElementsByTagName('Filter')->length) {
                                $mgResourceContentDOM->getElementsByTagName('Filter')->item(0)->nodeValue = eregi_replace("(.*(ADMPLAYER|\"ADMPLAYER\")[= ]*').*('.*)",'\\1'.$name.'\\3',$mgResourceContentDOM->getElementsByTagName('Filter')->item(0)->nodeValue);
                            }
                        }
                        $featureSourceResourceId = $mgResourceContentDOM->getElementsByTagName('ResourceId')->item(0)->nodeValue;
                        $mgResourceContentXML = $mgResourceContentDOM->saveXML();
                        $byteSource = new MgByteSource($mgResourceContentXML, strlen($mgResourceContentXML));
                        $byteSource->setMimeType("text/xml");
                        $byteXMLReader = $byteSource->GetReader();
                        $resourceService->SetResource($mgResourceId, $byteXMLReader, $byteHeaderReader);
                        $this->updateFeatureSource($resourceService, $featureSourceResourceId);
                    }
                }
            }
        }
    }

    function updateFeatureSources()
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $mgMapLayers = $this->wgsTier->getProjectObjectService()->getMgMapLayerList();
        if ($mgMapLayers) {
            foreach ($mgMapLayers as $mgMapLayer) {
                $mgResourceId = new MgResourceIdentifier($mgMapLayer->getMgResourceId());
                $byteContentReader = $resourceService->GetResourceContent($mgResourceId);
                $mgResourceContent = $byteContentReader->ToString();
                $mgResourceContentDOM = DOMDocument::loadXML($mgResourceContent);
                $featureSourceResourceId = $mgResourceContentDOM->getElementsByTagName('ResourceId')->item(0)->nodeValue;
                $this->updateFeatureSource($resourceService, $featureSourceResourceId);
            }
        }
    }

    ///////
    function updateFeatureSource(&$resourceService, $featureSourceResourceId)
    {
        $mgResourceId = new MgResourceIdentifier($featureSourceResourceId);
        $byteContentReader = $resourceService->GetResourceContent($mgResourceId);
        $byteHeaderReader = $resourceService->GetResourceHeader($mgResourceId);
        $resourceService->SetResource($mgResourceId, $byteContentReader, $byteHeaderReader);
    }

    //Функция удаления слоя
    function doRemoveFeatureLayer()
    {
        $objectid = $this->request->getPost('id');
        $User = $this->wgsTier->getAuthorizedUser()->getId();
        $Host = $_SERVER['REMOTE_ADDR'];
        $liId = $this->wgsTier->getProjectTier()->getAuditService()->removeFeatureLayer($objectid,$User,$Host);

        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->Remove($objectid,1);
        $this->updateFeatureSources();
    }

    function doRemoveRelationAtt()
    {
        $layerid = $this->request->getPost('layerid');
        $typerid = $this->request->getPost('typeid');
        $User = $this->wgsTier->getAuthorizedUser()->getId();
        $Host = $_SERVER['REMOTE_ADDR'];
        $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetName($typerid,$name);
        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetName($layerid,$lname);
        $isRemoveObject = ($this->request->getPost('removeObject') == 'true') ? 'Да' : 'Нет';

        $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','objectadmin','unbind_object');
        if($liId){
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'object_name_bind',decode($name));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'layer_name_bind',decode($lname));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'is_remove_object',decode($isRemoveOld));

        }

        if ($this->request->getPost('removeObject') == 'true')
        {
            $removemode = 0;
        }
        if ($this->request->getPost('removeObject') == 'false')
        {
            $removemode = 1;
        }
        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->RemoveRelationAtt($layerid,$typerid,$removemode);
        $this->updateFeatureSources();
    }
    function doAddRelationAtt()
    {
        $layerid = $this->request->getPost('layerid');
        $typerid = $this->request->getPost('typeid');
        $unBindDate = $this->request->getPost('unBindDate');
        $User = $this->wgsTier->getAuthorizedUser()->getId();
        $Host = $_SERVER['REMOTE_ADDR'];
        $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetName($typerid,$name);
        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetName($layerid,$lname);
        $isCurrent = ($this->request->getPost('iscurrent') == 'true') ? 'Да' : 'Нет';
        $isDeleteOld = ($this->request->getPost('removeOldObject') == 'true') ? 'Да' : 'Нет';
        $isBindOld = ($this->request->getPost('bindOldObject') == 'true') ? 'Да' : 'Нет';

        $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','objectadmin','bind_object');
        if($liId){
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'object_name_bind',decode($name));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'layer_name_bind',decode($lname));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'is_bind_old',decode($isBindOld));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'is_delete_old',decode($isDeleteOld));
            $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'is_current_type',decode($isCurrent));

        }

        if ($unBindDate) {
             $_unBindDate = explode(' - ', $unBindDate);
             $unBindDate = $_unBindDate[0];
        }

        $iscurrent = 0;
        if ($this->request->getPost('iscurrent') == 'true')
            $iscurrent = 1;
        if (($this->request->getPost('bindOldObject') == 'false') && ($this->request->getPost('removeOldObject') == 'false'))
        {
            $RelationMode = 2;
        } else
        if (($this->request->getPost('bindOldObject') == 'true') && ($this->request->getPost('removeOldObject') == 'false'))
        {
            $RelationMode = 3;
        } else
        if (($this->request->getPost('bindOldObject') == 'false') && ($this->request->getPost('removeOldObject') == 'true'))
        {
            $RelationMode = 0;
        } else
        if (($this->request->getPost('bindOldObject') == 'true') && ($this->request->getPost('removeOldObject') == 'true'))
        {
            $RelationMode = 4;
        }
        $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->AddRelationAtt($layerid,$typerid,$unBindDate,$iscurrent,$RelationMode);
        $this->updateFeatureSources();
    }
    function doSetRelationAtt()
    {
        $iscurrent = 0;
        if ($this->request->getPost('iscurrent') == 'true')
        $iscurrent = 1;
        $layerid = $this->request->getPost('layerid');
        $typerid = $this->request->getPost('typeid');
        $objectid = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->SetRelationAtt($layerid,$typerid,$iscurrent);
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-Функции для работы с группами характеристик-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Функция получения списка групп характеристик
    function doGetPropGroupList()
    {
        $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->getListPropertyGroup();
        $count = 0;
        foreach($result as $object)
        {
            if($object->get('Property_Group_Id')) {
                $objectList[$count]['id'] = $object->get('Property_Group_Id');
                $objectList[$count]['name'] = encode($object->get('Name'));
                $count = $count + 1;
            }
        }
        $this->response->write(json_encode($objectList));
    }
    //Функция добавления группы характеристик
    function doAddPropGroup()
    {
        $name = $this->request->getPost('name');
        $objectid = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->AddGroupProperty(decode($name));
        print($objectid);
    }
    //Функция редактирования группы характеристик
    function doEditPropGroup()
    {
        $objectid = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        //print_r($this->request->getPost());
        $this->wgsTier->getProjectObjectService()->getAttPropertyService()->SetGroupProperty($objectid,decode($name));
    }
    //Функция удаления группы характеристик
    function doRemovePropGroup()
    {
        $objectid = $this->request->getPost('id');
        $this->wgsTier->getProjectObjectService()->getAttPropertyService()->RemoveGroupProperty($objectid,1);
    }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-Функции для работы с характеристиками-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Функция получения списка характеристик
    function doGetPropList()
    {
        //Set($propId, $propName, $valueType, $propGroupId, $placDate)
        $typeId = $this->request->getPost('typeId');
        $groupId = $this->request->getPost('groupId');
        $aCadView = $this->request->getPost('aCadView');
        /*if (($typeId == null)&&($groupId == null))
         {
         $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType(null);
         }*/
        if (($typeId != null)&&($groupId == null))
        {
            $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($typeId,$aCadView);
            $count = 0;
            foreach($result as $object)
            {
                if($object->get('Property_Id') != null) {

                    $objectList[$count]['id'] = $object->get('Property_Id');
                    $objectList[$count]['name'] = encode($object->get('Name'));
                    $objectList[$count]['autoCADname'] = encode($object->get('CAD_NAME'));
                    $objectList[$count]['acadview'] = $object->get('ACAD_VIEW');
                    if ($object->get('ACAD_VIEW')==1)
                    {
                        $layerCurrent = "font-weight: bold;font-style: italic;";
                        $objectList[$count]['tagname'] = '<span style="'.$layerCurrent.'">'. encode($object->get('Name')).'</span>';
                    }
                    else
                    {
                        $objectList[$count]['tagname'] = encode($object->get('Name'));
                    }
                    $objectList[$count]['groupid'] = $object->get('Properties_group_id');
                    $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($object->get('Properties_group_id')));
                    $objectList[$count]['valuetypeid'] = $object->get('Value_properties_type_id');
                    $objectList[$count]['valuetypename'] =  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($object->get('Value_properties_type_id')));
                    $objectList[$count]['placdate'] = $object->get('placement_date');
                    $objectList[$count]['grouppriority'] = $object->get('Group_Priority');
                    $objectList[$count]['priority'] = $object->get('Priority');
                    $count = $count + 1;
                }
            }
        }
        if ($typeId == null)
        {
            $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->getListPropertyOfPropertyGroup($groupId);
            $count = 0;
            foreach($result as $object)
            {
                $objectList[$count]['id'] = $object->get('Property_Id');
                $objectList[$count]['name'] = encode($object->get('Name'));
                $objectList[$count]['autoCADname'] = encode($object->get('CAD_NAME'));
                $objectList[$count]['tagname'] = encode($object->get('Name'));
                $objectList[$count]['groupid'] = $object->get('Properties_group_id');
                $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($object->get('Properties_group_id')));
                $objectList[$count]['valuetypeid'] = $object->get('Value_properties_type_id');
                $objectList[$count]['valuetypename'] =  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($object->get('Value_properties_type_id')));
                $objectList[$count]['placdate'] = $object->get('placement_date');
                $count = $count + 1;
            }
        }
        // var_dump($objectList);
        $this->response->write(json_encode($objectList));
    }
    //Функция для получения характеристик не привязанных определенному атрибутивному типу
    function doGetPropListNotByType()
    {
        $typeId = $this->request->getPost('typeId');
        $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListPropertyNotByObjectType($typeId);
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('Property_Id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $objectList[$count]['autoCADname'] = encode($object->get('CAD_NAME'));
            $objectList[$count]['groupid'] = $object->get('Properties_group_id');
            $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($object->get('Properties_group_id')));
            $objectList[$count]['valuetypeid'] = $object->get('Value_properties_type_id');
            $objectList[$count]['valuetypename'] =  encode($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetValueType($object->get('Value_properties_type_id')));
            $objectList[$count]['placdate'] = $object->get('placement_date');
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }
    //Функция добавления характеристики
    function doAddProp()
    {
        $name = $this->request->getPost('name');
        $autoCADname = $this->request->getPost('autoCADname');
        $valueType = $this->request->getPost('valuetypeid');
        $propGroupId = $this->request->getPost('groupid');
        $placDate = $this->request->getPost('placdate');
        $objectid = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->Add(decode($name), decode($autoCADname), $valueType, $propGroupId, $placDate);
        print($objectid);
        //----
    }
    //Функция добавления характеристики
    function doEditProp()
    {
        //print($this->request->getPost('autoCADname'));
        $objectid = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $autoCADname = $this->request->getPost('autoCADname');
        $valueType = $this->request->getPost('valuetypeid');
        $propGroupId = $this->request->getPost('groupid');
        $placDate = $this->request->getPost('placdate');
        //echo $name."   ".html_entity_decode("&#8470;", ENT_NOQUOTES | ENT_IGNORE, 'UTF-8').str_replace(html_entity_decode("&#8470;", ENT_NOQUOTES | ENT_IGNORE, 'UTF-8'), 'No', $name);
        $objectid = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->Set($objectid,decode(str_replace(html_entity_decode("&#8470;", ENT_NOQUOTES | ENT_IGNORE, 'UTF-8'), '&#8470;', $name)), decode($autoCADname), $valueType, $propGroupId, $placDate);
        //----
    }
    //Функция удаления характеристики
    function doRemoveProp()
    {
        $objectid = $this->request->getPost('id');
        $this->wgsTier->getProjectObjectService()->getAttPropertyService()->RemoveProperty($objectid,0);
    }

    //Функция для получения списка типов значений
    function doGetValueTypeList()
    {
        $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueType();
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('Value_Property_Type_Id');
            $objectList[$count]['name'] = encode($object->get('Name'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    //Функция для получения списка значений доменной характеристики
    function doGetValueDomainList()
    {
        $propid = $this->request->getPost('propId');
        $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueDomain($propid);
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('Value_Domain_ID');
            $objectList[$count]['name'] = encode($object->get('Value_Domain'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }
    function doAddValueDomain()
    {
        $propid = $this->request->getPost('propId');
        $valueDomain = $this->request->getPost('valueDomain');
        $objectid = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->AddValueDomain($propid,decode($valueDomain));
        print($objectid);
    }
    function doSetValueDomain()
    {
        $valueDomainId = $this->request->getPost('valueDomainId');
        $valueDomain = $this->request->getPost('valueDomain');
        $this->wgsTier->getProjectObjectService()->getAttPropertyService()->SetValueDomain($valueDomainId,decode($valueDomain));
    }
    function doRemoveValueDomain()
    {
        $valueDomainId = $this->request->getPost('valueDomainId');
        $this->wgsTier->getProjectObjectService()->getAttPropertyService()->RemoveValueDomain($valueDomainId);
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-Функция получения дерева (Loader)-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    function doGetAttrTypes() {
        $filterLayer = null;
        $featureId = $this->request->get('featureId');
        $isSecondGenFunction = $this->request->get('noempty') == 1 ? true : false;
        if($featureId) {
            $filterLayer = $this->request->get('filterLayer');
            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($featureId);
        }

        // var_dump($filterLayer.$featureId);
        // var_dump($cardList);
        // die();

        $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer(null);
        $objectList = array();
        foreach($result as $object)
        {
            $objectTypeId = $object->get('Object_Type_id');
            //var_dump($objectTypeId);
            //die();
            $objectTypeName = $object->get('Name');
            if($objectTypeId) {
                //var_dump(encode($objectTypeName));
                $layers = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType($objectTypeId, $isSecondGenFunction);

                $tmp = array(
                    'id' => $objectTypeId,
                    'name' => encode($objectTypeName),
                    'type' => 'attr-type',
                    'layers' => array()
                );
                //var_dump($tmp);

                $passesFeatureFilter = true;
                $passesFilter = false;

                if($cardList != null) {
                    foreach ($cardList as $cardKey => $card) {
                        //var_dump($card->get('object_type_id').$tmp['id']);
                        if($card->get('object_type_id') == $tmp['id']){
                            $passesFeatureFilter = false;
                            //var_dump($tmp['name'].' passes');
                        }
                    }
                }

                if($passesFeatureFilter && $layers != null) {
                    if($filterLayer == null) {
                        $passesFilter = true;
                    }

                    foreach($layers as $layer) {
                        if($layer -> get('featurelayer_id')) {
                            $tmp['layers'][] = array(
                                'id' =>  $layer -> get('featurelayer_id'),
                                'name' =>  encode($layer -> get('name')),
                                'groupId'   =>  $layer -> get('featurelayergroup_id'),
                                'groupName'  =>  encode($layer -> get('group_name')),
                                'type' => 'layer'
                            );
                            //var_dump($filterLayer.$layer -> get('featurelayer_id'));
                                // var_dump($filterLayer);
                                // var_dump($layer -> get('featurelayer_id'));
                            if ($layer -> get('featurelayer_id') == $filterLayer) {
                                $passesFilter = true;
                            }
                        }
                    }

                }

                // var_dump($tmp['name']);
                // var_dump($passesFilter);
                // var_dump($passesFeatureFilter);

                if($passesFeatureFilter && $passesFilter) {
                    $objectList[] = $tmp;
                }
            }
        }
        $this ->response -> write(json_encode($objectList));
    }

    function doGetLayers() {
        $layerList = array();
        $layers = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByGroup(null);
        foreach($layers as $layer) {
            $layerId = $layer -> get('featurelayer_id');
            if($layerId != null){
                $layerList[] = array(
                    'id' =>  $layer -> get('featurelayer_id'),
                    'type' => 'layer',
                    'name' =>  encode($layer -> get('name')),
                    'layerGroup' =>  $layer -> get('featurelayergroup_id'),
                    'layerGroupName' =>  encode($layer -> get('group_name')),
                    'objectTypes' => array()
                );

                $objectTypes = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($layerId);

                foreach($objectTypes as $objectType) {
                    $objectTypeId = $objectType->get('Object_Type_id');
                    $objectTypeName = $objectType->get('Name');

                    if($objectTypeId) {
                        $objectDescription = array(
                            'id' => $objectTypeId,
                            'name' => encode($objectTypeName),
                            'type' => 'attr-type',
                        );

                        $layerList[count($layerList) -1]['objectTypes'][] = $objectDescription;
                    }
                }
            }
        }
        $this -> response -> write(json_encode($layerList));
    }

    function doGetCardsByLayersAndTypes($layerIds, $typeIds, $rangeStart=0, $rangeLimit=1, $sortby='', $filter = '', $onlyCount=false, $arrayed = false) {
        //$layerIds = array(1);
        //$typeIds = array(1);

        // var_dump($layerIds);
        // var_dump($typeIds);
        // var_dump($arrayed);
        // die('!');
        if($onlyCount == true) {
            return $this->wgsTier->getProjectObjectService()->getAttObjectService()-> getListObjectByFeaturesAndTypeCount($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter, $arrayed);
        }
        $cards = $this->wgsTier->getProjectObjectService()->getAttObjectService()-> getListObjectByFeaturesAndType($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter, $arrayed);
        // var_dump($cards);
        // die('!!');

        $cardsDescription = array();

        foreach($cards as $card) {
            if($card -> get('object_id')) {
                $cardsDescription[] = array(
                    'id' => $card -> get('object_id'),
                    'name' => encode($card -> get('name')),
                    'objectTypeId' => $card -> get('object_type_id')
                );
            }
        }

        //$this -> response -> write(json_encode($cardsDescription));
        return $cardsDescription;
    }


    function doGetObjectTree()
    {
        //$objectList = array();
        if ($_POST['Type'] == 'root')    //Если это рутовский узел
        {
            if ($_POST['node'] == 'AttrType-root')    //Если рутовский узел атрибутивных описаний
            {
                $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer(null);
                foreach($result as $object)
                {
                    $objectTypeId = $object->get('Object_Type_id');
                    $objectTypeName = $object->get('Name');
                    if($objectTypeId) {
                        $objectList['AttrType-root'][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($objectTypeName),
                            'Type' => 'AttrType',
                            'OwnerId' => 'null',
                            'OwnerType' => 'root',
                            'MyId' => $objectTypeId,
                            'iconCls' => 'icon-atttype'
                        //'leaf' => true
                        );
                    }
                }
            }
            if ($_POST['node'] == 'PropGroup-root')        //Если рутовский узел групп характеристик
            {
                $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->getListPropertyGroup();
                foreach($result as $object)
                {
                    $propertyGroupId = $object->get('Property_Group_Id');
                    $propertyGroupName = $object->get('Name');
                    if($propertyGroupId) {
                        $objectList['PropGroup-root'][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($propertyGroupName),
                            'Type' => 'PropGroup',
                            'OwnerId' => 'null',
                            'OwnerType' => 'root',
                            'MyId' => $propertyGroupId,
                            'iconCls' => 'icon-property-group'
                        //'leaf' => true
                        );
                    }

                }
            }
            if ($_POST['node'] == 'LayerGroup-root')    //Если рутовский узел групп слоев
            {
                $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupListEx();
                foreach($result as $object)
                {
                    $layerGroupId = $object->get('FeatureLayerGroup_Id');
                    $layerGroupName = $object->get('Name');
                    if($layerGroupId) {
                        $objectList['LayerGroup-root'][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($layerGroupName),
                            'Type' => 'LayerGroup',
                            'OwnerId' => 'null',
                            'OwnerType' => 'root',
                            'MyId' => $layerGroupId,
                            'iconCls' => 'icon-layer-group'
                        //'leaf' => true
                        );
                    }
                }
            }
        }
        else    //Если узел не является рутовым
        {
            if ($_POST['Type'] == 'PropGroup')        //Если раскрыт узел "Группа характеристик" (получение списка характеристик для группы)
            {
                $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->getListPropertyOfPropertyGroup($_POST['MyId']);
                foreach($result as $object)
                {
                    $propertyId = $object->get('Property_Id');
                    $propertyName = $object->get('Name');
                    if($propertyId) {
                        $objectList[$_POST['node']][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($propertyName),
                            'Type' => 'Prop',
                            'OwnerId' => $_POST['MyId'],
                            'OwnerType' => $_POST['Type'],
                            'MyId' => $propertyId,
                            'leaf' => true
                        );
                    }

                }
            }
            if ($_POST['Type'] == 'LayerGroup')        //Если раскрыт узел "Группа слоев" (получение списка слоев для группы)
            {
                $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByGroup($_POST['MyId']);
                foreach($result as $object)
                {
                    $LayerId = $object->get('FeatureLayer_Id');
                    $LayerName = $object->get('Name');
                    if($LayerId) {
                        $objectList[$_POST['node']][] = array(
                        'id' =>    md5(rand()),
                        'text' => encode($LayerName),
                        'Type' => 'Layer',
                        'OwnerId' => $_POST['MyId'],
                        'OwnerType' => $_POST['Type'],
                        'MyId' => $LayerId,
                        'iconCls' => 'icon-layer'
                        );
                    }
                }
             }
            if ($_POST['Type'] == 'AttrType-root')    //Если узел с подписью "Атрибутивные описания"
             {    if ($_POST['OwnerType']='Layer')    //Если он дочерний узлу "Слой"
                 {
                     $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($_POST['OwnerId']);
                 }
                 foreach($result as $object)
                {
                    $objectTypeId = $object->get('Object_Type_id');
                    $objectTypeName = $object->get('Name');
                    $objectTypeCurrent =  $object->get('is_current');

                    if ($objectTypeCurrent == 1)
                    {
                        //$currentStyle = "font-weight: bold;";
                        //$objectTypeNameTag = '<span style="'.$currentStyle.'">'.$objectTypeName.'</span>';
                        $objectTypeNameTag = $objectTypeName.' (основное)';
                    }
                    else
                    {
                        $objectTypeNameTag = $objectTypeName;
                    }

                    if($objectTypeId) {
                        $objectList[$_POST['node']][] = array(
                                'id' =>    md5(rand()),
                                'text' => encode($objectTypeNameTag),
                                'Type' => 'AttrType',
                                'OwnerId' => $_POST['OwnerId'],
                                'OwnerType' => $_POST['OwnerType'],
                                'MyId' => $objectTypeId,
                                'iconCls' => 'icon-atttype',
                                'leaf' => true
                        );
                    }

                }
            }
            if ($_POST['Type'] == 'ChildAttrType-root')    //Если узел с подписью Дочерние описания"
            {    if ($_POST['OwnerType']='AttrType')    //Если он дочерний узлу "Атрибутивное описание"
                {
                    $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByParent($_POST['OwnerId']);
                }
                foreach($result as $object)
                {
                    $objectTypeId = $object->get('Child_object_type_id');
                    $objectTypeName = $object->get('Child_Name');
                    $relationName = $object->get('Relation_Name');
                    if($objectTypeId) {
                        $objectList[$_POST['node']][] = array(
                                'id' =>    md5(rand()),
                                'text' => encode("$objectTypeName ($relationName)"),
                                'Type' => 'AttrType',
                                'OwnerId' => $_POST['OwnerId'],
                                'OwnerType' => $_POST['OwnerType'],
                                'MyId' => $objectTypeId,
                                'iconCls' => 'icon-atttype',
                                'leaf' => true
                        );
                    }

                }

            }
            if ($_POST['Type'] == 'ParentAttrType-root') {    //Если узел с подписью Родительские описания"
                if ($_POST['OwnerType']='AttrType')    //Если он дочерний узлу "Атрибутивное описание"
                {
                    $result = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListObjTypeRelationByChild($_POST['OwnerId']);
                }
                foreach($result as $object)
                {
                    $objectTypeId = $object->get('parent_object_type_id');
                    $objectTypeName = $object->get('parent_name');
                    $relationName = $object->get('relation_name');
                    if($objectTypeId) {
                        $objectList[$_POST['node']][] = array(
                                'id' =>    md5(rand()),
                                'text' => encode("$objectTypeName ($relationName)"),
                                'Type' => 'AttrType',
                                'OwnerId' => $_POST['OwnerId'],
                                'OwnerType' => $_POST['OwnerType'],
                                'MyId' => $objectTypeId,
                                'iconCls' => 'icon-atttype',
                                'leaf' => true
                        );
                    }
                }
            }
            if ($_POST['Type'] == 'Prop-root')        //Если узел с подписью "Характеристики"
            {    if ($_POST['OwnerType']='AttrType')    //Если он дочерний узлу "Атрибутивное описание"
                {
                    $result = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($_POST['OwnerId'],null);
                }
                foreach($result as $object)
                {
                    $propertyId = $object->get('Property_Id');
                    $propertyName = $object->get('Name');
                    if($propertyId) {
                        $objectList[$_POST['node']][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($propertyName),
                            'Type' => 'Prop',
                            'OwnerId' => $_POST['OwnerId'],
                            'OwnerType' => $_POST['OwnerType'],
                            'MyId' => $propertyId,
                            'leaf' => true
                        );
                    }
                }
            }
            if ($_POST['Type'] == 'Layer-root')        //Если узел с подписью "Слои"
            {    if ($_POST['OwnerType']='AttrType')    //Если он дочерний узлу "Атрибутивное описание"
                {
                    $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType($_POST['OwnerId'],null);
                }
                foreach($result as $object)
                {
                    $LayerId = $object->get('FeatureLayer_Id');
                    $LayerName = $object->get('Name');
                    if($LayerId) {
                        $objectList[$_POST['node']][] = array(
                            'id' =>    md5(rand()),
                            'text' => encode($LayerName),
                            'Type' => 'Layer',
                            'OwnerId' => $_POST['OwnerId'],
                            'OwnerType' => $_POST['OwnerType'],
                            'MyId' => $LayerId,
                            'iconCls' => 'icon-layer',
                            'leaf' => true
                        );
                    }
                }
            }
            if ($_POST['Type'] == 'AttrType')
            {
                $objectList[$_POST['node']][] = array(
                'id' => md5(rand()),
                'text' => 'Дочерние описания (связь)',
                'Type' => 'ChildAttrType-root',
                'OwnerId' => $_POST['MyId'],
                'OwnerType' => $_POST['Type'],
                'MyId' => 'null'
                );
                $objectList[$_POST['node']][] = array(
                'id' => md5(rand()),
                'text' => 'Родительские описания (связь)',
                'Type' => 'ParentAttrType-root',
                'OwnerId' => $_POST['MyId'],
                'OwnerType' => $_POST['Type'],
                'MyId' => 'null'
                );
                $objectList[$_POST['node']][] = array(
                'id' => md5(rand()),
                'text' => 'Слои',
                'Type' => 'Layer-root',
                'OwnerId' => $_POST['MyId'],
                'OwnerType' => $_POST['Type'],
                'MyId' => 'null'
                );
                $objectList[$_POST['node']][] = array(
                'id' => md5(rand()),
                'text' => 'Характеристики',
                'Type' => 'Prop-root',
                'OwnerId' => $_POST['MyId'],
                'OwnerType' => $_POST['Type'],
                'MyId' => 'null'
                );
            }
            if ($_POST['Type'] == 'Layer')
            {
                $objectList[$_POST['node']][] = array(
                'id' => md5(rand()),
                'text' => 'Атрибутивные описания',
                'Type' => 'AttrType-root',
                'OwnerId' => $_POST['MyId'],
                'OwnerType' => $_POST['Type'],
                'MyId' => 'null'
                );
            }

        }


        /*foreach($result as $object)
         {
         $objectId = $object->get('OBJECT_ID');
         $objectList[$_POST][] = array(
         'id' => $objectId."_".md5(rand()),
         'text' => $objectId,
         'leaf' => true//lmb_win1251_to_utf8($featureLayerGroup->getName())
         );
         }*/
        //var_dump($objectList);

        if ($objectList)
        {
            $this->response->write(json_encode(array_merge($objectList[$_POST['node']])));
        }
        else
        {
            $this->response->write(json_encode(array()));
        }

    }

    //******Привязка слоев к атрибутивному описанию
    //------Получение всего списка слоев и техкоторые уже привязаны списка
  function doGetFeatureLayerListForAttrType()
    {
        $typeId = $this->request->getPost('parentObjTypeId');
        if ($typeId == -1) {
            $result = array();
            $result = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType(null);
            $count = 0;
            foreach($result as $object)
            {
                $objectList[$count]['id'] = $object->get('FeatureLayer_Id');
                $objectList[$count]['name'] = encode($object->get('Name'));
                $objectList[$count]['groupid'] = $object->get('FeatureLayerGroup_Id');
                $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Get($object->get('FeatureLayerGroup_Id')));
                $count = $count + 1;
            }
        } else {
            $objectList = array();
            $featureLayersWithType = array();
            $featureLayers = array();
            $featureLayers = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType(null);
            $featureLayersWithType = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetListFeatureLayerByType($typeId);
            $count = 0;
            $idsWithType = array();
            foreach ($featureLayersWithType as $featureLayerWithType) {
                $idsWithType[] = $featureLayerWithType->get('FeatureLayer_Id');
            }
            foreach ($featureLayers as $featureLayer) {
                $id = $featureLayer->get('FeatureLayer_Id');
                if(in_array($id, $idsWithType)) {
                    $objectList[$count]['checked'] = 1;
                } else {
                    $objectList[$count]['checked'] = 0;
                }
                $objectList[$count]['id'] = $id;
                $objectList[$count]['name'] = encode($featureLayer->get('Name'));
                $objectList[$count]['groupid'] = $featureLayer->get('FeatureLayerGroup_Id');
                $objectList[$count]['groupname'] = encode($this->wgsTier->getProjectObjectService()->getFeatureLayerGroupService()->Get($featureLayer->get('FeatureLayerGroup_Id')));
                $count = $count + 1;
            }
        }
        $this->response->write(json_encode($objectList));
    }
}

?>
