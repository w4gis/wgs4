<?php

class ScholarshipOrderInfoDTO
{

    /**
     * @var string $reasorId
     */
    protected $reasorId = null;

    /**
     * @var string $reasorName
     */
    protected $reasorName = null;

    /**
     * @var string $Id
     */
    protected $Id = null;

    /**
     * @var OrderTypeDTO $OrderType
     */
    protected $OrderType = null;

    /**
     * @var float $Summa
     */
    protected $Summa = null;

    /**
     * @var date $BeginDate
     */
    protected $BeginDate = null;

    /**
     * @var date $EndDate
     */
    protected $EndDate = null;

    /**
     * @var PrivelegeDTO $Privelege
     */
    protected $Privelege = null;

    /**
     * @var SessionTypeDTO $SessionType
     */
    protected $SessionType = null;

    /**
     * @var SessionEndTypeDTO $SessionEndType
     */
    protected $SessionEndType = null;

    /**
     * @var string $ScholarOrderNumber
     */
    protected $ScholarOrderNumber = null;

    /**
     * @var date $ScholarOrderDate
     */
    protected $ScholarOrderDate = null;

    /**
     * @var string $Fund
     */
    protected $Fund = null;

    /**
     * @var OrderHeaderDTO $OrderHeader
     */
    protected $OrderHeader = null;

    /**
     * @var string $StudyId
     */
    protected $StudyId = null;

    /**
     * @param float $Summa
     */
    public function __construct($Summa)
    {
      $this->Summa = $Summa;
    }

    /**
     * @return string
     */
    public function getReasorId()
    {
      return $this->reasorId;
    }

    /**
     * @param string $reasorId
     * @return ScholarshipOrderInfoDTO
     */
    public function setReasorId($reasorId)
    {
      $this->reasorId = $reasorId;
      return $this;
    }

    /**
     * @return string
     */
    public function getReasorName()
    {
      return $this->reasorName;
    }

    /**
     * @param string $reasorName
     * @return ScholarshipOrderInfoDTO
     */
    public function setReasorName($reasorName)
    {
      $this->reasorName = $reasorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return ScholarshipOrderInfoDTO
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return OrderTypeDTO
     */
    public function getOrderType()
    {
      return $this->OrderType;
    }

    /**
     * @param OrderTypeDTO $OrderType
     * @return ScholarshipOrderInfoDTO
     */
    public function setOrderType($OrderType)
    {
      $this->OrderType = $OrderType;
      return $this;
    }

    /**
     * @return float
     */
    public function getSumma()
    {
      return $this->Summa;
    }

    /**
     * @param float $Summa
     * @return ScholarshipOrderInfoDTO
     */
    public function setSumma($Summa)
    {
      $this->Summa = $Summa;
      return $this;
    }

    /**
     * @return date
     */
    public function getBeginDate()
    {
      return $this->BeginDate;
    }

    /**
     * @param date $BeginDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setBeginDate($BeginDate)
    {
      $this->BeginDate = $BeginDate;
      return $this;
    }

    /**
     * @return date
     */
    public function getEndDate()
    {
      return $this->EndDate;
    }

    /**
     * @param date $EndDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setEndDate($EndDate)
    {
      $this->EndDate = $EndDate;
      return $this;
    }

    /**
     * @return PrivelegeDTO
     */
    public function getPrivelege()
    {
      return $this->Privelege;
    }

    /**
     * @param PrivelegeDTO $Privelege
     * @return ScholarshipOrderInfoDTO
     */
    public function setPrivelege($Privelege)
    {
      $this->Privelege = $Privelege;
      return $this;
    }

    /**
     * @return SessionTypeDTO
     */
    public function getSessionType()
    {
      return $this->SessionType;
    }

    /**
     * @param SessionTypeDTO $SessionType
     * @return ScholarshipOrderInfoDTO
     */
    public function setSessionType($SessionType)
    {
      $this->SessionType = $SessionType;
      return $this;
    }

    /**
     * @return SessionEndTypeDTO
     */
    public function getSessionEndType()
    {
      return $this->SessionEndType;
    }

    /**
     * @param SessionEndTypeDTO $SessionEndType
     * @return ScholarshipOrderInfoDTO
     */
    public function setSessionEndType($SessionEndType)
    {
      $this->SessionEndType = $SessionEndType;
      return $this;
    }

    /**
     * @return string
     */
    public function getScholarOrderNumber()
    {
      return $this->ScholarOrderNumber;
    }

    /**
     * @param string $ScholarOrderNumber
     * @return ScholarshipOrderInfoDTO
     */
    public function setScholarOrderNumber($ScholarOrderNumber)
    {
      $this->ScholarOrderNumber = $ScholarOrderNumber;
      return $this;
    }

    /**
     * @return date
     */
    public function getScholarOrderDate()
    {
      return $this->ScholarOrderDate;
    }

    /**
     * @param date $ScholarOrderDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setScholarOrderDate($ScholarOrderDate)
    {
      $this->ScholarOrderDate = $ScholarOrderDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getFund()
    {
      return $this->Fund;
    }

    /**
     * @param string $Fund
     * @return ScholarshipOrderInfoDTO
     */
    public function setFund($Fund)
    {
      $this->Fund = $Fund;
      return $this;
    }

    /**
     * @return OrderHeaderDTO
     */
    public function getOrderHeader()
    {
      return $this->OrderHeader;
    }

    /**
     * @param OrderHeaderDTO $OrderHeader
     * @return ScholarshipOrderInfoDTO
     */
    public function setOrderHeader($OrderHeader)
    {
      $this->OrderHeader = $OrderHeader;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param string $StudyId
     * @return ScholarshipOrderInfoDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

}
