<?php

class SpecialityDTOArray
{

    /**
     * @var SpecialityDTO[] $item
     */
    protected $item = null;

    /**
     * @param SpecialityDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return SpecialityDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param SpecialityDTO[] $item
     * @return SpecialityDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
