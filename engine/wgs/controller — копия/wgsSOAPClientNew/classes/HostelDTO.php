<?php

class HostelDTO
{

    /**
     * @var string $HostelId
     */
    protected $HostelId = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getHostelId()
    {
      return $this->HostelId;
    }

    /**
     * @param string $HostelId
     * @return HostelDTO
     */
    public function setHostelId($HostelId)
    {
      $this->HostelId = $HostelId;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return HostelDTO
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
