<?php

class HostelStudentDTO
{

    /**
     * @var string $StudentHosteId
     */
    protected $StudentHosteId = null;

    /**
     * @var HostelDTO $Hostel
     */
    protected $Hostel = null;

    /**
     * @var int $RoomNumber
     */
    protected $RoomNumber = null;

    /**
     * @var date $StartDate
     */
    protected $StartDate = null;

    /**
     * @var date $EndDate
     */
    protected $EndDate = null;

    /**
     * @var string $StudyId
     */
    protected $StudyId = null;

    /**
     * @param int $RoomNumber
     */
    public function __construct($RoomNumber)
    {
      $this->RoomNumber = $RoomNumber;
    }

    /**
     * @return string
     */
    public function getStudentHosteId()
    {
      return $this->StudentHosteId;
    }

    /**
     * @param string $StudentHosteId
     * @return HostelStudentDTO
     */
    public function setStudentHosteId($StudentHosteId)
    {
      $this->StudentHosteId = $StudentHosteId;
      return $this;
    }

    /**
     * @return HostelDTO
     */
    public function getHostel()
    {
      return $this->Hostel;
    }

    /**
     * @param HostelDTO $Hostel
     * @return HostelStudentDTO
     */
    public function setHostel($Hostel)
    {
      $this->Hostel = $Hostel;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoomNumber()
    {
      return $this->RoomNumber;
    }

    /**
     * @param int $RoomNumber
     * @return HostelStudentDTO
     */
    public function setRoomNumber($RoomNumber)
    {
      $this->RoomNumber = $RoomNumber;
      return $this;
    }

    /**
     * @return date
     */
    public function getStartDate()
    {
      return $this->StartDate;
    }

    /**
     * @param date $StartDate
     * @return HostelStudentDTO
     */
    public function setStartDate($StartDate)
    {
      $this->StartDate = $StartDate;
      return $this;
    }

    /**
     * @return date
     */
    public function getEndDate()
    {
      return $this->EndDate;
    }

    /**
     * @param date $EndDate
     * @return HostelStudentDTO
     */
    public function setEndDate($EndDate)
    {
      $this->EndDate = $EndDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param string $StudyId
     * @return HostelStudentDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

}
