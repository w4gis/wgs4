<?php

class DocIdentDTO
{

    /**
     * @var string $study_id
     */
    protected $study_id = null;

    /**
     * @var string $identify_id
     */
    protected $identify_id = null;

    /**
     * @var string $student_id
     */
    protected $student_id = null;

    /**
     * @var string $doc_ser
     */
    protected $doc_ser = null;

    /**
     * @var string $doc_num
     */
    protected $doc_num = null;

    /**
     * @var date $doc_from
     */
    protected $doc_from = null;

    /**
     * @var date $doc_to
     */
    protected $doc_to = null;

    /**
     * @var string $doc_who
     */
    protected $doc_who = null;

    /**
     * @var string $doc_birthplace
     */
    protected $doc_birthplace = null;

    /**
     * @var date $first_order_date
     */
    protected $first_order_date = null;

    /**
     * @var string $first_order_num
     */
    protected $first_order_num = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getStudy_id()
    {
      return $this->study_id;
    }

    /**
     * @param string $study_id
     * @return DocIdentDTO
     */
    public function setStudy_id($study_id)
    {
      $this->study_id = $study_id;
      return $this;
    }

    /**
     * @return string
     */
    public function getIdentify_id()
    {
      return $this->identify_id;
    }

    /**
     * @param string $identify_id
     * @return DocIdentDTO
     */
    public function setIdentify_id($identify_id)
    {
      $this->identify_id = $identify_id;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudent_id()
    {
      return $this->student_id;
    }

    /**
     * @param string $student_id
     * @return DocIdentDTO
     */
    public function setStudent_id($student_id)
    {
      $this->student_id = $student_id;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_ser()
    {
      return $this->doc_ser;
    }

    /**
     * @param string $doc_ser
     * @return DocIdentDTO
     */
    public function setDoc_ser($doc_ser)
    {
      $this->doc_ser = $doc_ser;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_num()
    {
      return $this->doc_num;
    }

    /**
     * @param string $doc_num
     * @return DocIdentDTO
     */
    public function setDoc_num($doc_num)
    {
      $this->doc_num = $doc_num;
      return $this;
    }

    /**
     * @return date
     */
    public function getDoc_from()
    {
      return $this->doc_from;
    }

    /**
     * @param date $doc_from
     * @return DocIdentDTO
     */
    public function setDoc_from($doc_from)
    {
      $this->doc_from = $doc_from;
      return $this;
    }

    /**
     * @return date
     */
    public function getDoc_to()
    {
      return $this->doc_to;
    }

    /**
     * @param date $doc_to
     * @return DocIdentDTO
     */
    public function setDoc_to($doc_to)
    {
      $this->doc_to = $doc_to;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_who()
    {
      return $this->doc_who;
    }

    /**
     * @param string $doc_who
     * @return DocIdentDTO
     */
    public function setDoc_who($doc_who)
    {
      $this->doc_who = $doc_who;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_birthplace()
    {
      return $this->doc_birthplace;
    }

    /**
     * @param string $doc_birthplace
     * @return DocIdentDTO
     */
    public function setDoc_birthplace($doc_birthplace)
    {
      $this->doc_birthplace = $doc_birthplace;
      return $this;
    }

    /**
     * @return date
     */
    public function getFirst_order_date()
    {
      return $this->first_order_date;
    }

    /**
     * @param date $first_order_date
     * @return DocIdentDTO
     */
    public function setFirst_order_date($first_order_date)
    {
      $this->first_order_date = $first_order_date;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirst_order_num()
    {
      return $this->first_order_num;
    }

    /**
     * @param string $first_order_num
     * @return DocIdentDTO
     */
    public function setFirst_order_num($first_order_num)
    {
      $this->first_order_num = $first_order_num;
      return $this;
    }

}
