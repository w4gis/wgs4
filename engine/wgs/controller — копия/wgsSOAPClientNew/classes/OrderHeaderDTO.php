<?php

class OrderHeaderDTO
{

    /**
     * @var string $Id
     */
    protected $Id = null;

    /**
     * @var string $Number
     */
    protected $Number = null;

    /**
     * @var date $OrderDate
     */
    protected $OrderDate = null;

    /**
     * @var date $CreateDate
     */
    protected $CreateDate = null;

    /**
     * @var date $HandleDate
     */
    protected $HandleDate = null;

    /**
     * @var SubDivisionDTO $SubDivision
     */
    protected $SubDivision = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return OrderHeaderDTO
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
      return $this->Number;
    }

    /**
     * @param string $Number
     * @return OrderHeaderDTO
     */
    public function setNumber($Number)
    {
      $this->Number = $Number;
      return $this;
    }

    /**
     * @return date
     */
    public function getOrderDate()
    {
      return $this->OrderDate;
    }

    /**
     * @param date $OrderDate
     * @return OrderHeaderDTO
     */
    public function setOrderDate($OrderDate)
    {
      $this->OrderDate = $OrderDate;
      return $this;
    }

    /**
     * @return date
     */
    public function getCreateDate()
    {
      return $this->CreateDate;
    }

    /**
     * @param date $CreateDate
     * @return OrderHeaderDTO
     */
    public function setCreateDate($CreateDate)
    {
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return date
     */
    public function getHandleDate()
    {
      return $this->HandleDate;
    }

    /**
     * @param date $HandleDate
     * @return OrderHeaderDTO
     */
    public function setHandleDate($HandleDate)
    {
      $this->HandleDate = $HandleDate;
      return $this;
    }

    /**
     * @return SubDivisionDTO
     */
    public function getSubDivision()
    {
      return $this->SubDivision;
    }

    /**
     * @param SubDivisionDTO $SubDivision
     * @return OrderHeaderDTO
     */
    public function setSubDivision($SubDivision)
    {
      $this->SubDivision = $SubDivision;
      return $this;
    }

}
