<?php

class DocIdentDTOArray
{

    /**
     * @var DocIdentDTO[] $item
     */
    protected $item = null;

    /**
     * @param DocIdentDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return DocIdentDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param DocIdentDTO[] $item
     * @return DocIdentDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
