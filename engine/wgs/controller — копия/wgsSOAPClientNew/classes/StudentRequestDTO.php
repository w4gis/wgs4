<?php

class StudentRequestDTO
{

    /**
     * @var string $PersonId
     */
    protected $PersonId = null;

    /**
     * @var string $StudyId
     */
    protected $StudyId = null;

    /**
     * @var string $GroupId
     */
    protected $GroupId = null;

    /**
     * @var string $CitizenshipId
     */
    protected $CitizenshipId = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $MiddleName
     */
    protected $MiddleName = null;

    /**
     * @var string $ZachNumber
     */
    protected $ZachNumber = null;

    /**
     * @var string $GroupName
     */
    protected $GroupName = null;

    /**
     * @var string $EduId
     */
    protected $EduId = null;

    /**
     * @var string $EduFormId
     */
    protected $EduFormId = null;

    /**
     * @var string $SubFacultyId
     */
    protected $SubFacultyId = null;

    /**
     * @var string $FacultyId
     */
    protected $FacultyId = null;

    /**
     * @var string $SpecialityId
     */
    protected $SpecialityId = null;

    /**
     * @var string $SpecializationId
     */
    protected $SpecializationId = null;

    /**
     * @var int $SpecialityTypeId
     */
    protected $SpecialityTypeId = null;

    /**
     * @var int $StudentStateId
     */
    protected $StudentStateId = null;

    /**
     * @var int $CourseGroup
     */
    protected $CourseGroup = null;

    /**
     * @param int $SpecialityTypeId
     * @param int $StudentStateId
     * @param int $CourseGroup
     */
    public function __construct($SpecialityTypeId, $StudentStateId, $CourseGroup)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      $this->StudentStateId = $StudentStateId;
      $this->CourseGroup = $CourseGroup;
    }

    /**
     * @return string
     */
    public function getPersonId()
    {
      return $this->PersonId;
    }

    /**
     * @param string $PersonId
     * @return StudentRequestDTO
     */
    public function setPersonId($PersonId)
    {
      $this->PersonId = $PersonId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param string $StudyId
     * @return StudentRequestDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupId()
    {
      return $this->GroupId;
    }

    /**
     * @param string $GroupId
     * @return StudentRequestDTO
     */
    public function setGroupId($GroupId)
    {
      $this->GroupId = $GroupId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCitizenshipId()
    {
      return $this->CitizenshipId;
    }

    /**
     * @param string $CitizenshipId
     * @return StudentRequestDTO
     */
    public function setCitizenshipId($CitizenshipId)
    {
      $this->CitizenshipId = $CitizenshipId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return StudentRequestDTO
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return StudentRequestDTO
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return StudentRequestDTO
     */
    public function setMiddleName($MiddleName)
    {
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getZachNumber()
    {
      return $this->ZachNumber;
    }

    /**
     * @param string $ZachNumber
     * @return StudentRequestDTO
     */
    public function setZachNumber($ZachNumber)
    {
      $this->ZachNumber = $ZachNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
      return $this->GroupName;
    }

    /**
     * @param string $GroupName
     * @return StudentRequestDTO
     */
    public function setGroupName($GroupName)
    {
      $this->GroupName = $GroupName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEduId()
    {
      return $this->EduId;
    }

    /**
     * @param string $EduId
     * @return StudentRequestDTO
     */
    public function setEduId($EduId)
    {
      $this->EduId = $EduId;
      return $this;
    }

    /**
     * @return string
     */
    public function getEduFormId()
    {
      return $this->EduFormId;
    }

    /**
     * @param string $EduFormId
     * @return StudentRequestDTO
     */
    public function setEduFormId($EduFormId)
    {
      $this->EduFormId = $EduFormId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubFacultyId()
    {
      return $this->SubFacultyId;
    }

    /**
     * @param string $SubFacultyId
     * @return StudentRequestDTO
     */
    public function setSubFacultyId($SubFacultyId)
    {
      $this->SubFacultyId = $SubFacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFacultyId()
    {
      return $this->FacultyId;
    }

    /**
     * @param string $FacultyId
     * @return StudentRequestDTO
     */
    public function setFacultyId($FacultyId)
    {
      $this->FacultyId = $FacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityId()
    {
      return $this->SpecialityId;
    }

    /**
     * @param string $SpecialityId
     * @return StudentRequestDTO
     */
    public function setSpecialityId($SpecialityId)
    {
      $this->SpecialityId = $SpecialityId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecializationId()
    {
      return $this->SpecializationId;
    }

    /**
     * @param string $SpecializationId
     * @return StudentRequestDTO
     */
    public function setSpecializationId($SpecializationId)
    {
      $this->SpecializationId = $SpecializationId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpecialityTypeId()
    {
      return $this->SpecialityTypeId;
    }

    /**
     * @param int $SpecialityTypeId
     * @return StudentRequestDTO
     */
    public function setSpecialityTypeId($SpecialityTypeId)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudentStateId()
    {
      return $this->StudentStateId;
    }

    /**
     * @param int $StudentStateId
     * @return StudentRequestDTO
     */
    public function setStudentStateId($StudentStateId)
    {
      $this->StudentStateId = $StudentStateId;
      return $this;
    }

    /**
     * @return int
     */
    public function getCourseGroup()
    {
      return $this->CourseGroup;
    }

    /**
     * @param int $CourseGroup
     * @return StudentRequestDTO
     */
    public function setCourseGroup($CourseGroup)
    {
      $this->CourseGroup = $CourseGroup;
      return $this;
    }

}
