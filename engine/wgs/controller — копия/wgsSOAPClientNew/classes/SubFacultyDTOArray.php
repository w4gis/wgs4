<?php

class SubFacultyDTOArray
{

    /**
     * @var SubFacultyDTO[] $item
     */
    protected $item = null;

    /**
     * @param SubFacultyDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return SubFacultyDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param SubFacultyDTO[] $item
     * @return SubFacultyDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
