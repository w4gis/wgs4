<?php

class HostelDTOArray
{

    /**
     * @var HostelDTO[] $item
     */
    protected $item = null;

    /**
     * @param HostelDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return HostelDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param HostelDTO[] $item
     * @return HostelDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
