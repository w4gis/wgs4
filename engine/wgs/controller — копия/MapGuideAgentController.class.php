<?php
lmb_require('wgs/controller/wgsMapAgentController.class.php');
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('limb/net/src/lmbHttpRequest.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require('wgs/lib/mapagent/wgsMgCommandDispatcher.class.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');


lmb_require('wgs/model/MgMapResource.class.php');

function BooleanToString($boolean){
     if (is_bool($boolean))
         return ($boolean ? "true" : "false");
     else
         return "'ERROR in BooleanToString.'";
}

function GetSiteVersion() {
	$username = MGAUTHOR_USERNAME;
	$password = MGAUTHOR_PASSWORD;
	//var_dump($password)
    $user = new MgUserInformation($username, $password);
        
    
    $serverAdmin = new MgServerAdmin();
    $serverAdmin->Open($user);
    /* use these lines for MGOS < 2.2
    $infoProps = $serverAdmin->GetInformationProperties();
    $versionProp = $infoProps->GetItem(MgServerInformationProperties::ServerVersion);
    $serverVersion = $versionProp->GetValue();*/
    $serverVersion = $serverAdmin->GetSiteVersion();
    return $serverVersion;
}

function OutputGroupInfo($group)
{
    $groupObj = new stdClass();

    $groupObj->Name 		= array($group->GetName());
    $groupObj->LegendLabel 	= array($group->GetLegendLabel());
    $groupObj->ObjectId 	= array($group->GetObjectId());
    $groupObj->DisplayInLegend = array(BooleanToString($group->GetDisplayInLegend()));
    $groupObj->ExpandInLegend = array(BooleanToString($group->GetExpandInLegend()));
    $parent = $group->GetGroup();
    $groupObj->ParentObjectId = array($parent != null ? $parent->GetObjectId() : '');
    $groupObj->Visible = array(BooleanToString($group->GetVisible()));
    $groupObj->ActuallyVisible = array(BooleanToString($group->isVisible()));
    $groupObj->Type = array($group->GetLayerGroupType());
    //$isBaseMapGroup = ($group->GetLayerGroupType() == MgLayerGroupType::BaseMap);
    //$groupObj->IsBaseMapGroup =  array($isBaseMapGroup;

    return $groupObj;
}

function buildScaleRanges($layer, $resourceService, $xmldoc = NULL)
{
	//var_dump($xmldoc);
    $aScaleRanges = array();
    //global $resourceService;
    if ($xmldoc == NULL) {
        $resID = $layer->GetLayerDefinition();
        $layerContent = $resourceService->GetResourceContent($resID);
        $xmldoc = new DOMDocument();
        $xmldoc->loadXML(ByteReaderToString($layerContent));
    }
    $type = 0;
    $scaleRanges = $xmldoc->getElementsByTagName('VectorScaleRange');
    //var_dump($scaleRanges);
    if($scaleRanges->length == 0) {
        $scaleRanges = $xmldoc->getElementsByTagName('GridScaleRange');
        if($scaleRanges->length == 0) {
            $scaleRanges = $xmldoc->getElementsByTagName('DrawingLayerDefinition');
            if($scaleRanges->length == 0) {
                return;
            }
            $type = 2;
        } else {
            $type = 1;
        }
    }
    $typeStyles = array("PointTypeStyle", "LineTypeStyle", "AreaTypeStyle", "CompositeTypeStyle");
    $ruleNames = array("PointRule", "LineRule", "AreaRule", "CompositeRule");
    for($sc = 0; $sc < $scaleRanges->length; $sc++)
    {
        $scaleRangeObj = array();//new stdClass();
        $scaleRangeObj['FeatureStyle'] = array();
        $scaleRange = $scaleRanges->item($sc);
        $minElt = $scaleRange->getElementsByTagName('MinScale');
        $maxElt = $scaleRange->getElementsByTagName('MaxScale');
        $minScale = "0";
        $maxScale = '1000000000000';  // as MDF's VectorScaleRange::MAX_MAP_SCALE
        if($minElt->length > 0)
            $minScale = $minElt->item(0)->nodeValue;
        if($maxElt->length > 0)
            $maxScale = $maxElt->item(0)->nodeValue;

        $scaleRangeObj['MinScale'] = $minScale;
        $scaleRangeObj['MaxScale'] = $maxScale;

		//var_dump($scaleRangeObj);
        if($type != 0) {
            array_push($aScaleRanges, $scaleRangeObj);
            break;
        }


        $styleIndex = 0;
        for($ts=0, $count = count($typeStyles); $ts < $count; $ts++)
        {
            $typeStyle = $scaleRange->getElementsByTagName($typeStyles[$ts]);
            $catIndex = 0;
            for($st = 0; $st < $typeStyle->length; $st++) {

                // We will check if this typestyle is going to be shown in the legend
                $showInLegend = $typeStyle->item($st)->getElementsByTagName("ShowInLegend");
                if($showInLegend->length > 0)
                    if($showInLegend->item(0)->nodeValue == "false")
                        continue;   // This typestyle does not need to be shown in the legend

                $rules = $typeStyle->item($st)->getElementsByTagName($ruleNames[$ts]);
                for($r = 0; $r < $rules->length; $r++) {
                    $rule = $rules->item($r);
                    $label = $rule->getElementsByTagName("LegendLabel");
                    $filter = $rule->getElementsByTagName("Filter");

                    $labelText = $label->length==1? $label->item(0)->nodeValue: null;
                    $filterText = $filter->length==1? $filter->item(0)->nodeValue: null;
                    $styleObj = array();//new stdClass();
                    $styleObj['Type'] = array((string)($ts+1));
                    //$styleObj['CategoryIndex'] = $catIndex++;
                    $styleObj['Rule'] = array(
						'LegendLabel' => null,//$labelText,
						'Filter' => $filterText,
						'Icon' => array("R0lGODdhEAAQAIAAAAQCBPz+/CwAAAAAEAAQAAACFIyPqcvtDaKctNqLs968+/uE4jgWADs=")
					);
		
					
                    
                    array_push($scaleRangeObj['FeatureStyle'], $styleObj);
                }
            }
        }
        array_push($aScaleRanges, $scaleRangeObj);

    }
   // var_dump($aScaleRanges);
        
    return $aScaleRanges;
}


/**
 * ����� ������������ ��� '���������' �������� �� MGStudio � mapagent.exe.
 * � ����������� �� ���� ����� ������� ���������� (SETRESOURCE,DELETERESOURCE,MOVERESOURCE,COPYRESOURCE)
 * ��������� ������ ���������������� ������ � '��������������' ����������
 * ����������� ����������� MupGuide � ���� Oracle
 */
class MapGuideAgentController extends wgsFrontController
{
    private    $server;
    private    $path_to = "/mapguide/mapagent/wgsagent.fcgi";

    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->wgsTier->commitSession();
    }

    private function sendToHostGet()
    {
        $data = array();
        $query = '';
        $sep = '';
        $sdata = '';

        foreach ($this->request->getRequest() as $index => $value)
        {
            $query .= $sep . $index .'=' .urlencode($value);
            $sep = '&';
            $data[$index] = rawurldecode($value);
        }


		$a=$this->wgsTier->getProjectTier()->getAuditService();
	    $isAuditable = $a->getAuditActivity();
		if($isAuditable>1 && $data['OPERATION'] === "GETDYNAMICMAPOVERLAYIMAGE" && $data['MAPNAME'] && $data['SESSION'] && $data['MAPNAME']!='map'){
			//fwrite($f, 'catched GET ('.$data['MAPNAME'].'): '.$data['OPERATION']."\r\n");
			lmb_require('wgs/controller/map/ThemesController.class.php');
			$mc = new ThemesController();
			$layers = $mc->getLayerVisibility($data['MAPNAME']);
			$flayers = $mc->getLayerSummary($data['MAPNAME']);
			$ids = $flayers['fli'];
			if (count($ids)){
				$id = $this->wgsTier->getAuthorizedUser()->getId();
				if($id) {
				    $li = $a->setMapActivity($id, $_SERVER['REMOTE_ADDR'], $data['MAPNAME'], $data['SESSION'], $ids);
                }
			}
			
		
		}

		$sep = '';
        foreach ($data as $index => $value)
        {
            $sdata .= $sep . $index .'=' .urlencode($value);
            $sep = '&';
        }

        if ($this->server['REQUEST_METHOD'] == 'GET') {
            $this->path_to .= '?' . $sdata;
        }

        $header = '';
        $body = '';

        $header .= "".$this->server['REQUEST_METHOD']." ". $this->path_to ." HTTP/1.1\r\n";
        $header .= "Host:". $this->server['SERVER_NAME'] .":".$this->server['SERVER_PORT']."\r\n";
        if (isset($this->server['HTTP_USER_AGENT']))
        $header .= "User-Agent: ".$this->server['HTTP_USER_AGENT']."\r\n";
        $header .= "Authorization: ".$this->server['REDIRECT_REDIRECT_REMOTE_USER']."\r\n";
        $header .= "Content-Type: text/plain; charset=utf-8\r\n";
        $header .= "Content-length: " . strlen($body) . "\r\n";
        $header .= "Connection: close\r\n\r\n";

		$socket = fsockopen($this->server['SERVER_NAME'],$this->server['SERVER_PORT']);
        if (!$socket)
        {
            echo '������ �������� ������<BR>';
            return;
        }
		
        fputs($socket, $header.$body);
        $responce = '';
        while (!feof($socket)) {
			$r = fgets($socket,4096);
			 $responce .= $r;
			

        }
		fclose($socket);

        return array( 'response' => $responce, 'xml' => null);
    }

    private function sendToHostPost()
    {
		file_put_contents("c:\log.mg", "Post\r\n", FILE_APPEND | LOCK_EX);
        $header = '';
        $body = '';
        $boundary = '';
        $xml = '';

        if(isset($this->server['CONTENT_TYPE']))
        {
            $content_type = explode('; boundary=',$this->server['CONTENT_TYPE']);
            if(isset($content_type[1]))
            $boundary = $content_type[1];
        }

        $header .= "".$this->server['REQUEST_METHOD']." ". $this->path_to ." HTTP/1.1\r\n";
        if (isset($this->server['HTTP_USER_AGENT']))
        {
            $header .= "User-Agent: ".$this->server['HTTP_USER_AGENT']."\r\n";
            if (strstr($this->server['HTTP_USER_AGENT'], 'Autodesk MapGuide Studio'))
            $header .= "Authorization: ".$this->server['REDIRECT_REDIRECT_REMOTE_USER']."\r\n";
        }
        elseif(isset($this->server['REDIRECT_REMOTE_USER']))
        {
            $header .= "Authorization: ".$this->server['REDIRECT_REMOTE_USER']."\r\n";
        }
        $header .= "Content-Type: ".$this->server['CONTENT_TYPE']."\r\n";
        $header .= "Host: ". /*$this->server['SERVER_NAME']*/'127.0.0.1' .":".$this->server['SERVER_PORT']."\r\n";

        if(sizeof($_FILES) != 0)
        {
            foreach($this->request->getRequest() as $index => $value)
            {
                if(!is_object($value)) {
                    $body .= "--{$boundary}\r\n";
                    $body .= "Content-Disposition: form-data; name=\"$index\"\r\n\r\n";
                    $body .= urldecode($value) . "\r\n";
                }
            }

            foreach($_FILES as $index => $value)
            {
                $body .= "--{$boundary}\r\n";
                $body .= "Content-Disposition: form-data; name=\"$index\"; filename=\"$index\"\r\n";
                $body .= 'Content-Type: ' . (empty($value['type']) ? 'application/octet-stream' : $value['type']) . "\r\n\r\n";
                if (is_readable($value['tmp_name']))
                {
                    $handle = fopen($value['tmp_name'], 'rb');
                    $xml = fread($handle, filesize($value['tmp_name']));


                    if($this->request->getRequest('OPERATION') != 'APPLYRESOURCEPACKAGE') {
                        $xml = $this->createUniqueCommands($xml);
                    } else {
                        $zip = zip_open($value['tmp_name']);
                        if ($zip) {
                            while ($zip_entry = zip_read($zip)) {
                                if(zip_entry_name($zip_entry) == 'MgResourcePackageManifest.xml') {
                                    if (zip_entry_open($zip, $zip_entry, "r")) {
                                        $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                                        $xmlbuf = simplexml_load_string($buf);
                                        foreach ($xmlbuf->xpath('//Operation') as $operation) {
                                            if ($operation->Name == 'SETRESOURCE') {
                                                foreach($operation->Parameters->Parameter as $parameter) {
                                                    if((substr_count($parameter->Value,'LayerDefinition') > 0) || (substr_count($parameter->Value,'MapDefinition') > 0)) {
                                                        $xml_file_name = $operation->Parameters->Parameter[0]->Value;
                                                        $resource_id = $parameter->Value;
                                                        $zip_archive = new ZipArchive();
                                                        if ($zip_archive->open($value['tmp_name']) === true) {
                                                            $request = array();
                                                            $request['OPERATION'] = 'SETRESOURCE';
                                                            $request['RESOURCEID'] = $resource_id;
                                                            $command = wgsMgCommandDispatcher :: factory($request, $zip_archive->getFromName($xml_file_name));
                                                            if($command != null) {
                                                                $command->run();
                                                            }
                                                            $zip_archive->close();
                                                        } 
                                                    }
                                                }    
                                            }
                                        }
                                        zip_entry_close($zip_entry);
                                    } 
                                }
                            }
                            zip_close($zip);
                        }                        
                    }    

                    $body .= $xml;
                    fclose($handle);
                }
                $body .= "\r\n";
            }
            $body .= "--$boundary--\r\n";
        }
        else
        {
            if($boundary !='')
            {
                foreach($this->request->getRequest() as $index => $value)
                {
                    $body .= "--{$boundary}\r\n";
                    $body .= "Content-Disposition: form-data; name=\"$index\"\r\n\r\n";
                    $body .= urldecode($value) . "\r\n";

                }
                $body .= "--$boundary--\r\n";
            }
            else
            {
                $sep = '';
                foreach($this->request->getRequest() as $index => $value)
                {
                    $body .= $sep . $index .'=' .urlencode($value);
                    $sep = '&';
                }
                //$body .= "\r\n";
            }
        }

        $header .= "Content-Length: " . strlen($body) . "\r\n";

        if(isset($this->server['HTTP_EXPECT']))
        {
            $header .= "Expect: ".$this->server['HTTP_EXPECT']."\r\n";
        }

        if (isset($this->server['HTTP_CONNECTION']))
        $header .= "Connection: ".$this->server['HTTP_CONNECTION']."\r\n";

        $header .= "\r\n";

        $socket = fsockopen(/*$this->server['SERVER_NAME']*/'127.0.0.1', $this->server['SERVER_PORT']);
        if (!$socket)
        {
            echo 'fsockopen failed<BR>';
            return;
        }
        fputs($socket, $header.$body);
        $responce = '';
        while (!feof($socket))
        {
            $responce .= fgets($socket,4096);
        }

        fclose($socket);

        return array( 'response' => $responce, 'xml' => $xml);
    }

    private function parseContent( $r )
    {
        $header = array();
        $body = array();
        $ar = explode("\r\n", $r);
        foreach($ar as $idx => $line) {
            if (trim($line) == '') {
                $header = array_slice( $ar, 0, $idx);
                $body = array_slice( $ar, $idx+1);
                break;
            }
        }
        return array( 'header' => $header, 'body' => $body );
    }

    public function createLog($file_name,$assoc_array,$string)
    {
        $file = fopen($file_name,"a+");
        fwrite($file,"\r\n");
        fwrite($file,date("l dS of F Y h:i:s A")."\r\n");
        fwrite($file,"\r\n");
        if (isset($assoc_array)&&(!isset($string)))
        {
            foreach ($assoc_array as $key => $value)
            {
                fwrite($file,$key .' = '.$value);
                fwrite($file,"\r\n");
            }
        }
        if (isset($string)&&(!isset($assoc_array)))
        {
            fwrite($file,$string);
        }
        fwrite($file,"\r\n");
    }

    private function removeBadFeatureClasses ($xml)
    {
        $isKingOra = 0;
        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xpath = new DOMXPath($doc);
        $query = '//xs:schema/xs:*';
        $entries = $xpath->query($query);
        if($entries->item(0)) {
            if($entries->item(0)->getAttribute('name')) {
                if($entries->item(0)->getAttribute('name') != 'ADMPFEATURELAYERType') {
                    $isKingOra = 1;
                }
            }
            if ($isKingOra) {
                foreach ($entries as $entry){
                    if(ereg ("(.*)-x7e-(.*)-x7e-(.*)", $entry->getAttribute('name'), $regs))
                    $entry->parentNode->removeChild($entry);
                }
            } else {
                foreach ($entries as $entry) {
                    /*
                    if(!eregi("FEATURETABLE", $entry->getAttribute('name'), $regs) && eregi(":", $entry->getAttribute('name'), $regs)) {
                        $entry->parentNode->removeChild($entry);                        
                    }
                    */
                }
            }
        }
        return $doc->saveXML();
    }

    private function createUniqueCommands($xml)
    {
        $doc = new DOMDocument();
        $doc->loadXML($xml);

        $scripts = $doc->getElementsByTagName('Script');

        for ($i = 0; $i < $scripts->length; $i++)
        {
            for($j = 0; $j < $scripts->length; $j++)
            {
                if($j != $i)
                {
                    $message = '������ ������� ��� ���������� ����� !!!';
                    $message = iconv(iconv_get_encoding($message),'UTF-8',$message);
                    if($scripts->item($j)->nodeValue == $scripts->item($i)->nodeValue)
                    $scripts->item($j)->nodeValue = "var ".chr(mt_rand(65,90))." = ".rand(0,9999999999).";alert('$message');";
                }
            }
        }
        return $doc->saveXML();
    }

    public function redirectRequest()
    {
		file_put_contents("c:\log.mg", "Start processing request. \r\n", FILE_APPEND | LOCK_EX);
		
        $this->response->header('Content-type: text/xml; charset=utf-8');
		if($this->server['REQUEST_METHOD'] == 'POST')
			$r = $this->sendToHostPost();
        else {
			$r = $this->sendToHostGet();
        }
        $aResults = $this->parseContent($r['response']);


		if($_REQUEST['operation'] == 'CREATERUNTIMEMAP'){
            //ini_set('display_errors',1);
            
            $request 	= $this->request->getRequest();
            $sessionId 	= $request['session'];
            $mapName 	= $request['targetMapName'];
            $mapDef 	= $request['mapdefinition'];
            
            file_put_contents("c:\log.mg", "Map creation request\r\n", FILE_APPEND | LOCK_EX);
            
            $resp = CJSON::decode($aResults['body'][0]);
            $this->mgTier    = lmbToolkit :: instance() -> getMgTier();
            //lmbToolkit :: instance() -> getMgTier() ->openMgSession($sessionId);
            $mapTitle = 'demo1';
			
            //$map->Open($mapName);
			$siteConnection  = $this->mgTier->getSiteConnection();
			$user        	 = $this->wgsTier->getAuthorizedUser();
			$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
			
			$site = $siteConnection->GetSite();
			$sessionID = $site->CreateSession();
     
			$map = new MgMap();
			$resourceID = new  MgResourceIdentifier($mapDef);  
			
			
			$map->Create($resourceService, $resourceID, $mapName);      
			
			//var_dump($map->GetSessionId());
			$mapAddress = "Session:" . $sessionID . "//" .$mapTitle . "." . MgResourceType::Map;            
			$mapStateId = new MgResourceIdentifier($mapAddress);
			$map->Save($resourceService, $mapStateId);
			//echo "Name of map:               '" . $map->GetName() . "'n";
			//echo "   Session ID of map:      " . $map->GetSessionId() . "n";
			//echo "   Object ID:              " . $map->GetObjectId() . "n";
			
			$layers = $map->GetLayers();
			$lc = $layers->GetCount();
			$acl        = $this->wgsTier->getACL();

			$j = 0; $layerArray = array();
			ini_set('display_errors',1);
			//
			$layerDefinitionIds = new MgStringCollection();
			$featureSourceIds = new MgStringCollection();
			for($i=0;$i<$layers->GetCount();$i++){
				$layer = $layers->GetItem($i);
				$lid = $layer->GetLayerDefinition();
				$layerDefinitionIds->Add($lid->ToString());
				$featureSourceIds->Add($layer->GetFeatureSourceId());
			}
			
			$layerDefinitionContents = $resourceService->GetResourceContents($layerDefinitionIds, null);
			$featureSourceContents   = $resourceService->GetResourceContents($featureSourceIds, null);
			$layerDocs = array();
			$fsDocs = array();
			for($i=0;$i<$layers->GetCount();$i++){
				$ldfContent = $layerDefinitionContents->GetItem($i);
				$ldfdoc = new DOMDocument();
				$ldfdoc->loadXML($ldfContent);
				array_push($layerDocs, $ldfdoc);
				$fsContent = $featureSourceContents->GetItem($i);
				$fsDoc = new DOMDocument();
				$fsDoc->loadXML($fsContent);
				array_push($fsDocs, $fsDoc);
			}
			file_put_contents("c:\log.mg", "this is ".$user -> getId()." ".wgsPrivilege::FEATURELAYER_ACCESS."\r\n", FILE_APPEND | LOCK_EX);       
					
			if ($mgMapLayerList = $this->wgsTier->getProjectObjectService()->getMgMapLayerList())            
				foreach ($mgMapLayerList as $mgMapLayer)                    
				for ($i = 0; $i < $lc; $i++){
					$layer = $layers->GetItem($j);
					
					$layerContent = $layerDocs[$i];
					$fsContent = $fsDocs[$i];
        
					
					$layerDefinition = $layer->GetLayerDefinition()->ToString();
					file_put_contents("c:\log.mg", "Layer ".$mgMapLayer -> getMgResourceId()."\r\n", FILE_APPEND | LOCK_EX);       
								
					if($layerDefinition == $mgMapLayer -> getMgResourceId()){
						$ld = "      layer definition:    " . $layerDefinition  . "";
						$ll = "      legend label:        " . $layer->GetLegendLabel()  . "";
						file_put_contents("c:\log.mg", $ld." ".$ll."\r\n", FILE_APPEND | LOCK_EX);       
				
						if (true && (!$acl->isAllowed($user, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS) &&
							!$acl->isAllowed($user, $mgMapLayer, wgsPrivilege::MGMAPLAYER_ACCESS))){
								file_put_contents("c:\log.mg", "Layer is not allowed. ".$mapTitle."::".$layerDefinition."\r\n", FILE_APPEND | LOCK_EX);       
								$layers -> Remove($layer);
						}else {
							$j++;
							$layerObj = array();
							$layerObj['ActuallyVisible'] = array(BooleanToString($layer->IsVisible()));
							$layerObj['DisplayInLegend'] = array(BooleanToString($layer->GetDisplayInLegend()));
							$layerObj['ExpandInLegend']  = array(BooleanToString($layer->GetExpandInLegend()));
							//echo $layer->GetFeatureSourceId();
							
							$layerObj['FeatureSource'] 	 = array(array(
								'ClassName' => array($layer->GetFeatureClassName()),
								'Geometry'  => array($layer->GetFeatureGeometryName()),
								'ResourceId' => array($layer->GetFeatureSourceId())								
							));
							
							$layerObj['LayerDefinition'] = array($layerDefinition);
							$layerObj['LegendLabel'] = array($layer->GetLegendLabel());
							$layerObj['Name'] = array($layer->GetName());
							$layerObj['ObjectId'] = array($layer->GetObjectId());
							$gr = $layer->GetGroup();
							//echo $gr;
							if($gr)
								$layerObj['ParentId'] = array($gr->GetObjectId());
							//var_dump($gr);
							//$layerObj['ParentId'] = array();//->GetObjectId());
							$oScaleRanges = buildScaleRanges($layer, $resourceService, $layerContent);
        
							$layerObj['ScaleRange'] = $oScaleRanges;
							$layerObj['Selectable'] = array(BooleanToString($layer->GetSelectable()));
							$layerObj['Type'] 		= array((string)$layer->GetLayerType());
							$layerObj['Visible']	= array(BooleanToString($layer->GetVisible()));
							array_push($layerArray, $layerObj);
						}
					
						file_put_contents("c:\log.mg", $layers->GetCount()."\r\n", FILE_APPEND | LOCK_EX);       
						break;
					}
					
				}
			
			$srs = $map->GetMapSRS();
			$epsgCode = "";
			if($srs != ""){
				$csFactory = new MgCoordinateSystemFactory();
				$cs = $csFactory->Create($srs);
				$metersPerUnit = $cs->ConvertCoordinateSystemUnitsToMeters(1.0);
				try {
					$epsgCode = $csFactory->ConvertWktToEpsgCode($srs);

					// Convert EPSG code 3857 to the equivalent code 900913 that is understood by OpenLayers
					if($epsgCode == 3857){
						$epsgCode = 900913;
						// We need to set the srs code to null because OpenLayers doesn't know the srs code.
						$srs = "";
					}
	
				} catch (MgException $e) {
				//just catch the exception and set epsgCode to empty string
				}
			}	
			
			$extents = $map->GetMapExtent();
    
			$oMin = $extents->GetLowerLeftCoordinate();
			$oMax = $extents->GetUpperRightCoordinate();
			$layerGroups = $map->GetLayerGroups();
			$mapObjGroups = array(); 
			
			for($i=0;$i<$layerGroups->GetCount();$i++){
				$group = $layerGroups->GetItem($i);
				array_push($mapObjGroups, OutputGroupInfo($group));
			}
			
			$RuntimeMap = array( 'RuntimeMap' => array(
				'@xmlns:xsi' => array("http://www.w3.org/2001/XMLSchema-instance"),
				'@xsi:noNamespaceSchemaLocation' => array("RuntimeMap-2.6.0.xsd"),
				'BackgroundColor' => array("ffffffff"),
				'CoordinateSystem' => array(
					array(
						"EpsgCode" 		=> array($epsgCode),
						"MentorCode"	=> array("WGS84.PseudoMercator"),
						"MetersPerUnit" => array(1),
						"Wkt"			=> array("PROJCS[\"WGS84.PseudoMercator\",GEOGCS[\"LL84\",DATUM[\"WGS84\",SPHEROID[\"WGS84\",6378137.000,298.25722293]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Popular Visualisation Pseudo Mercator\"],PARAMETER[\"false_easting\",0.000],PARAMETER[\"false_northing\",0.000],PARAMETER[\"central_meridian\",0.00000000000000],UNIT[\"Meter\",1.00000000000000]]")//array($srs)
					),					
				),
				"DisplayDpi" => array("96"),
				"Extents"	=> array(
					array(
						"LowerLeftCoordinate" => array(
							array(
								"X" => array($oMin->GetX()),
								"Y" => array($oMin->GetY())
							)
						),
						"UpperRightCoordinate" => array(
							array(
								"X" => array($oMax->GetX()),
								"Y" => array($oMax->GetY())
							)
						)
					)
				),
				"Group" => $mapObjGroups,
				"IconMimeType" => array("image/gif"),
				"Layer"	=> $layerArray,
				"MapDefinition" =>  array($mapDef),
				"Name" => array($mapName),
				"SessionId" => array($map->GetSessionId()),
				"SiteVersion" => array(GetSiteVersion())
			));
			$map->Save($resourceService, $mapStateId);
			/*foreach($resp as $key=>$val){
				$RuntimeMap[$key] = $val;
			}
			
			//$RuntimeMap["RuntimeMap"]["Group"] = $mapObjGroups;			
			foreach($RuntimeMap["RuntimeMap"]["Group"] as $key=>$val){ 
				$val['ObjectId'] = $mapObjGroups->$key['ObjectId'];
			}
			foreach($RuntimeMap["RuntimeMap"]["Layer"] as $key=>$val){ 
				$val['ObjectId'] = $layerArray[$key]['ObjectId'];
			}
			$RuntimeMap["RuntimeMap"]["Name"] = array($mapName);
            $RuntimeMap["RuntimeMap"]["SessionId"] = array($sessionID);//$this->mgTier->getMgSessionId());
            */
            //var_dump($map->GetSessionId() == $this->mgTier->getMgSessionId());
			//var_dump($map->GetMapSRS())
            //$sel = new MgSelection($map);
			//$sel->Save($resourceService, 'demo');

			
        
			
			
			/*
            $mapResource = new MgMapResource("Session:" . $resp['RuntimeMap']['SessionId'][0] . "//" . 'demo' . "." . MgResourceType::Map, 'demo');
			
			if ($mgMapLayerList = $this->wgsTier->getProjectObjectService()->getMgMapLayerList())
                {
                    foreach ($mgMapLayerList as $mgMapLayer)
                    {
                        if (!$acl->isAllowed($u, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS) &&
                            !$acl->isAllowed($u, $mgMapLayer, wgsPrivilege::MGMAPLAYER_ACCESS)){
								file_put_contents("c:\log.mg", "Layer is not allowed. ".$mapTitle."::".$mgMapLayer->getMgResourceId()." ".$mgMapLayer->getMgResourceId()."\r\n", FILE_APPEND | LOCK_EX);       
								//$mapResource->excludeLayer($mgMapLayer->getName());
								$mapResource->excludeLayer($mgMapLayer->getMgResourceId());
							}                       
                    }
                }
			$mapResource->saveToSession();        	
			*/
			file_put_contents("c:\log.mg", "*****************\r\n", FILE_APPEND | LOCK_EX); 
			$layers = $map->GetLayers();
			for ($i = 0; $i < $layers->GetCount(); $i++){
				$layer = $layers->GetItem($i);
				$layerDefinition = $layer->GetLayerDefinition();
					$ld = "      layer definition:    '" . $layerDefinition->ToString()  . "'n";
					$ll = "      legend label:        '" . $layer->GetLegendLabel()  . "'n";
					file_put_contents("c:\log.mg", $ld." ".$ll."\r\n", FILE_APPEND | LOCK_EX);       
								
			}
			//file_put_contents("c:\log.mg",$mapResource->getContent()."\r\n", FILE_APPEND | LOCK_EX);		
            file_put_contents("c:\log.mg", "response is ".$mapAddress."\r\n", FILE_APPEND | LOCK_EX);		
            
           // var_dump(array_intersect_assoc($resp,$RuntimeMap));
            //$reps["Group"] = $mapObjGroups;
			
            //$resp["RuntimeMap"]["Layer"] = $layerArray;				
			//$reps["SessionId"] = array($this->mgTier->getMgSessionId());
				
            
            echo CJSON::encode($RuntimeMap); return;
        }
        
        //////////////////////
		
        if($_REQUEST['OPERATION'] == 'DESCRIBEFEATURESCHEMA')
        {
            $aResults['body'][0] = $this->removeBadFeatureClasses($aResults['body'][0]);
        }
        
        
        
        
        foreach($aResults['header'] as $header) {
            file_put_contents("c:\log.mg", $header."\r\n", FILE_APPEND | LOCK_EX);		
            $this->response->header($header);
        }
        $this->response->commit();

        $this->response->write(implode("\r\n", $aResults['body']));

        if(stripos($this->request->getRequest('RESOURCEID'), 'New folder')===false)
        {

            $command = wgsMgCommandDispatcher :: factory($this->request->getRequest(), $r['xml']);

            if($command != null)
            {
                $command->run();
            }
        }
		
       
    }

    public function doDisplay()
    {
        $this->server = $_SERVER;
        $this->redirectRequest();
    }

}

?>
