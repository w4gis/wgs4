<?php

class ArrayOfDocIdentDTO
{

    /**
     * @var DocIdentDTO[] $DocIdentDTO
     */
    protected $DocIdentDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DocIdentDTO[]
     */
    public function getDocIdentDTO()
    {
      return $this->DocIdentDTO;
    }

    /**
     * @param DocIdentDTO[] $DocIdentDTO
     * @return ArrayOfDocIdentDTO
     */
    public function setDocIdentDTO(array $DocIdentDTO)
    {
      $this->DocIdentDTO = $DocIdentDTO;
      return $this;
    }

}
