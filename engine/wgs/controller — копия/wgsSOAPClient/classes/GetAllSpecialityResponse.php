<?php

class GetAllSpecialityResponse
{

    /**
     * @var ArrayOfSpecialityDTO $GetAllSpecialityResult
     */
    protected $GetAllSpecialityResult = null;

    /**
     * @param ArrayOfSpecialityDTO $GetAllSpecialityResult
     */
    public function __construct($GetAllSpecialityResult)
    {
      $this->GetAllSpecialityResult = $GetAllSpecialityResult;
    }

    /**
     * @return ArrayOfSpecialityDTO
     */
    public function getGetAllSpecialityResult()
    {
      return $this->GetAllSpecialityResult;
    }

    /**
     * @param ArrayOfSpecialityDTO $GetAllSpecialityResult
     * @return GetAllSpecialityResponse
     */
    public function setGetAllSpecialityResult($GetAllSpecialityResult)
    {
      $this->GetAllSpecialityResult = $GetAllSpecialityResult;
      return $this;
    }

}
