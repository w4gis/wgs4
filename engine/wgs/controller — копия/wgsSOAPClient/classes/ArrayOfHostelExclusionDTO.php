<?php

class ArrayOfHostelExclusionDTO
{

    /**
     * @var HostelExclusionDTO[] $HostelExclusionDTO
     */
    protected $HostelExclusionDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return HostelExclusionDTO[]
     */
    public function getHostelExclusionDTO()
    {
      return $this->HostelExclusionDTO;
    }

    /**
     * @param HostelExclusionDTO[] $HostelExclusionDTO
     * @return ArrayOfHostelExclusionDTO
     */
    public function setHostelExclusionDTO(array $HostelExclusionDTO)
    {
      $this->HostelExclusionDTO = $HostelExclusionDTO;
      return $this;
    }

}
