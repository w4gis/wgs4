<?php

class GetEducationInfoByIdResponse
{

    /**
     * @var ArrayOfEducationDTO $GetEducationInfoByIdResult
     */
    protected $GetEducationInfoByIdResult = null;

    /**
     * @param ArrayOfEducationDTO $GetEducationInfoByIdResult
     */
    public function __construct($GetEducationInfoByIdResult)
    {
      $this->GetEducationInfoByIdResult = $GetEducationInfoByIdResult;
    }

    /**
     * @return ArrayOfEducationDTO
     */
    public function getGetEducationInfoByIdResult()
    {
      return $this->GetEducationInfoByIdResult;
    }

    /**
     * @param ArrayOfEducationDTO $GetEducationInfoByIdResult
     * @return GetEducationInfoByIdResponse
     */
    public function setGetEducationInfoByIdResult($GetEducationInfoByIdResult)
    {
      $this->GetEducationInfoByIdResult = $GetEducationInfoByIdResult;
      return $this;
    }

}
