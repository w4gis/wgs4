<?php

class GetStudentRoomsResponse
{

    /**
     * @var ArrayOfStudentRoomDTO $GetStudentRoomsResult
     */
    protected $GetStudentRoomsResult = null;

    /**
     * @param ArrayOfStudentRoomDTO $GetStudentRoomsResult
     */
    public function __construct($GetStudentRoomsResult)
    {
      $this->GetStudentRoomsResult = $GetStudentRoomsResult;
    }

    /**
     * @return ArrayOfStudentRoomDTO
     */
    public function getGetStudentRoomsResult()
    {
      return $this->GetStudentRoomsResult;
    }

    /**
     * @param ArrayOfStudentRoomDTO $GetStudentRoomsResult
     * @return GetStudentRoomsResponse
     */
    public function setGetStudentRoomsResult($GetStudentRoomsResult)
    {
      $this->GetStudentRoomsResult = $GetStudentRoomsResult;
      return $this;
    }

}
