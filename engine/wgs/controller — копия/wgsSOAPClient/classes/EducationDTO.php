<?php

class EducationDTO
{

    /**
     * @var int $EduId
     */
    protected $EduId = null;

    /**
     * @var EduFormDTO $EduForm
     */
    protected $EduForm = null;

    /**
     * @var SubFacultyDTO $SubFaculty
     */
    protected $SubFaculty = null;

    /**
     * @var FacultyDTO $Faculty
     */
    protected $Faculty = null;

    /**
     * @var SpecializationDTO $Specialization
     */
    protected $Specialization = null;

    /**
     * @var SpecialityDTO $Speciality
     */
    protected $Speciality = null;

    /**
     * @var SpecTypeDTO $SpecType
     */
    protected $SpecType = null;

    /**
     * @var boolean $IsActive
     */
    protected $IsActive = null;

    /**
     * @param int $EduId
     * @param EduFormDTO $EduForm
     * @param SubFacultyDTO $SubFaculty
     * @param FacultyDTO $Faculty
     * @param SpecializationDTO $Specialization
     * @param SpecialityDTO $Speciality
     * @param SpecTypeDTO $SpecType
     * @param boolean $IsActive
     */
    public function __construct($EduId, $EduForm, $SubFaculty, $Faculty, $Specialization, $Speciality, $SpecType, $IsActive)
    {
      $this->EduId = $EduId;
      $this->EduForm = $EduForm;
      $this->SubFaculty = $SubFaculty;
      $this->Faculty = $Faculty;
      $this->Specialization = $Specialization;
      $this->Speciality = $Speciality;
      $this->SpecType = $SpecType;
      $this->IsActive = $IsActive;
    }

    /**
     * @return int
     */
    public function getEduId()
    {
      return $this->EduId;
    }

    /**
     * @param int $EduId
     * @return EducationDTO
     */
    public function setEduId($EduId)
    {
      $this->EduId = $EduId;
      return $this;
    }

    /**
     * @return EduFormDTO
     */
    public function getEduForm()
    {
      return $this->EduForm;
    }

    /**
     * @param EduFormDTO $EduForm
     * @return EducationDTO
     */
    public function setEduForm($EduForm)
    {
      $this->EduForm = $EduForm;
      return $this;
    }

    /**
     * @return SubFacultyDTO
     */
    public function getSubFaculty()
    {
      return $this->SubFaculty;
    }

    /**
     * @param SubFacultyDTO $SubFaculty
     * @return EducationDTO
     */
    public function setSubFaculty($SubFaculty)
    {
      $this->SubFaculty = $SubFaculty;
      return $this;
    }

    /**
     * @return FacultyDTO
     */
    public function getFaculty()
    {
      return $this->Faculty;
    }

    /**
     * @param FacultyDTO $Faculty
     * @return EducationDTO
     */
    public function setFaculty($Faculty)
    {
      $this->Faculty = $Faculty;
      return $this;
    }

    /**
     * @return SpecializationDTO
     */
    public function getSpecialization()
    {
      return $this->Specialization;
    }

    /**
     * @param SpecializationDTO $Specialization
     * @return EducationDTO
     */
    public function setSpecialization($Specialization)
    {
      $this->Specialization = $Specialization;
      return $this;
    }

    /**
     * @return SpecialityDTO
     */
    public function getSpeciality()
    {
      return $this->Speciality;
    }

    /**
     * @param SpecialityDTO $Speciality
     * @return EducationDTO
     */
    public function setSpeciality($Speciality)
    {
      $this->Speciality = $Speciality;
      return $this;
    }

    /**
     * @return SpecTypeDTO
     */
    public function getSpecType()
    {
      return $this->SpecType;
    }

    /**
     * @param SpecTypeDTO $SpecType
     * @return EducationDTO
     */
    public function setSpecType($SpecType)
    {
      $this->SpecType = $SpecType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
      return $this->IsActive;
    }

    /**
     * @param boolean $IsActive
     * @return EducationDTO
     */
    public function setIsActive($IsActive)
    {
      $this->IsActive = $IsActive;
      return $this;
    }

}
