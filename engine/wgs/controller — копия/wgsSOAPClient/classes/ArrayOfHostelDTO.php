<?php

class ArrayOfHostelDTO
{

    /**
     * @var HostelDTO[] $HostelDTO
     */
    protected $HostelDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return HostelDTO[]
     */
    public function getHostelDTO()
    {
      return $this->HostelDTO;
    }

    /**
     * @param HostelDTO[] $HostelDTO
     * @return ArrayOfHostelDTO
     */
    public function setHostelDTO(array $HostelDTO)
    {
      $this->HostelDTO = $HostelDTO;
      return $this;
    }

}
