<?php

class StudentInfoForEduDTO
{

    /**
     * @var ArrayOfStudentAddressDTO $studentAddress
     */
    protected $studentAddress = null;

    /**
     * @var StudentDTO $student
     */
    protected $student = null;

    /**
     * @param ArrayOfStudentAddressDTO $studentAddress
     * @param StudentDTO $student
     */
    public function __construct($studentAddress, $student)
    {
      $this->studentAddress = $studentAddress;
      $this->student = $student;
    }

    /**
     * @return ArrayOfStudentAddressDTO
     */
    public function getStudentAddress()
    {
      return $this->studentAddress;
    }

    /**
     * @param ArrayOfStudentAddressDTO $studentAddress
     * @return StudentInfoForEduDTO
     */
    public function setStudentAddress($studentAddress)
    {
      $this->studentAddress = $studentAddress;
      return $this;
    }

    /**
     * @return StudentDTO
     */
    public function getStudent()
    {
      return $this->student;
    }

    /**
     * @param StudentDTO $student
     * @return StudentInfoForEduDTO
     */
    public function setStudent($student)
    {
      $this->student = $student;
      return $this;
    }

}
