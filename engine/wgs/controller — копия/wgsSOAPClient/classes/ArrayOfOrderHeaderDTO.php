<?php

class ArrayOfOrderHeaderDTO
{

    /**
     * @var OrderHeaderDTO[] $OrderHeaderDTO
     */
    protected $OrderHeaderDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OrderHeaderDTO[]
     */
    public function getOrderHeaderDTO()
    {
      return $this->OrderHeaderDTO;
    }

    /**
     * @param OrderHeaderDTO[] $OrderHeaderDTO
     * @return ArrayOfOrderHeaderDTO
     */
    public function setOrderHeaderDTO(array $OrderHeaderDTO)
    {
      $this->OrderHeaderDTO = $OrderHeaderDTO;
      return $this;
    }

}
