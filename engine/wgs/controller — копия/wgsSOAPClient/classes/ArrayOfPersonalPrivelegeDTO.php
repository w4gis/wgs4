<?php

class ArrayOfPersonalPrivelegeDTO
{

    /**
     * @var PersonalPrivelegeDTO[] $PersonalPrivelegeDTO
     */
    protected $PersonalPrivelegeDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PersonalPrivelegeDTO[]
     */
    public function getPersonalPrivelegeDTO()
    {
      return $this->PersonalPrivelegeDTO;
    }

    /**
     * @param PersonalPrivelegeDTO[] $PersonalPrivelegeDTO
     * @return ArrayOfPersonalPrivelegeDTO
     */
    public function setPersonalPrivelegeDTO(array $PersonalPrivelegeDTO)
    {
      $this->PersonalPrivelegeDTO = $PersonalPrivelegeDTO;
      return $this;
    }

}
