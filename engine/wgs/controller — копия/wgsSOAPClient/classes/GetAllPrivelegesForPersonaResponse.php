<?php

class GetAllPrivelegesForPersonaResponse
{

    /**
     * @var ArrayOfPersonalPrivelegeDTO $GetAllPrivelegesForPersonaResult
     */
    protected $GetAllPrivelegesForPersonaResult = null;

    /**
     * @param ArrayOfPersonalPrivelegeDTO $GetAllPrivelegesForPersonaResult
     */
    public function __construct($GetAllPrivelegesForPersonaResult)
    {
      $this->GetAllPrivelegesForPersonaResult = $GetAllPrivelegesForPersonaResult;
    }

    /**
     * @return ArrayOfPersonalPrivelegeDTO
     */
    public function getGetAllPrivelegesForPersonaResult()
    {
      return $this->GetAllPrivelegesForPersonaResult;
    }

    /**
     * @param ArrayOfPersonalPrivelegeDTO $GetAllPrivelegesForPersonaResult
     * @return GetAllPrivelegesForPersonaResponse
     */
    public function setGetAllPrivelegesForPersonaResult($GetAllPrivelegesForPersonaResult)
    {
      $this->GetAllPrivelegesForPersonaResult = $GetAllPrivelegesForPersonaResult;
      return $this;
    }

}
