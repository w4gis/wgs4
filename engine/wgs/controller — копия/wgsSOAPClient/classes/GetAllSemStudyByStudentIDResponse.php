<?php

class GetAllSemStudyByStudentIDResponse
{

    /**
     * @var ArrayOfSemestrStudyDTO $GetAllSemStudyByStudentIDResult
     */
    protected $GetAllSemStudyByStudentIDResult = null;

    /**
     * @param ArrayOfSemestrStudyDTO $GetAllSemStudyByStudentIDResult
     */
    public function __construct($GetAllSemStudyByStudentIDResult)
    {
      $this->GetAllSemStudyByStudentIDResult = $GetAllSemStudyByStudentIDResult;
    }

    /**
     * @return ArrayOfSemestrStudyDTO
     */
    public function getGetAllSemStudyByStudentIDResult()
    {
      return $this->GetAllSemStudyByStudentIDResult;
    }

    /**
     * @param ArrayOfSemestrStudyDTO $GetAllSemStudyByStudentIDResult
     * @return GetAllSemStudyByStudentIDResponse
     */
    public function setGetAllSemStudyByStudentIDResult($GetAllSemStudyByStudentIDResult)
    {
      $this->GetAllSemStudyByStudentIDResult = $GetAllSemStudyByStudentIDResult;
      return $this;
    }

}
