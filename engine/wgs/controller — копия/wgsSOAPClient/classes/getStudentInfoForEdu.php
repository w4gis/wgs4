<?php

class getStudentInfoForEdu
{

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var string $fistName
     */
    protected $fistName = null;

    /**
     * @var string $midleName
     */
    protected $midleName = null;

    /**
     * @param string $lastName
     * @param string $fistName
     * @param string $midleName
     */
    public function __construct($lastName, $fistName, $midleName)
    {
      $this->lastName = $lastName;
      $this->fistName = $fistName;
      $this->midleName = $midleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return getStudentInfoForEdu
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFistName()
    {
      return $this->fistName;
    }

    /**
     * @param string $fistName
     * @return getStudentInfoForEdu
     */
    public function setFistName($fistName)
    {
      $this->fistName = $fistName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMidleName()
    {
      return $this->midleName;
    }

    /**
     * @param string $midleName
     * @return getStudentInfoForEdu
     */
    public function setMidleName($midleName)
    {
      $this->midleName = $midleName;
      return $this;
    }

}
