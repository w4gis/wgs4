<?php

class UpdateGrant
{

    /**
     * @var GrantDTO $grant
     */
    protected $grant = null;

    /**
     * @param GrantDTO $grant
     */
    public function __construct($grant)
    {
      $this->grant = $grant;
    }

    /**
     * @return GrantDTO
     */
    public function getGrant()
    {
      return $this->grant;
    }

    /**
     * @param GrantDTO $grant
     * @return UpdateGrant
     */
    public function setGrant($grant)
    {
      $this->grant = $grant;
      return $this;
    }

}
