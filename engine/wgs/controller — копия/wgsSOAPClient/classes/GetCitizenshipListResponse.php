<?php

class GetCitizenshipListResponse
{

    /**
     * @var ArrayOfCitizenshipDTO $GetCitizenshipListResult
     */
    protected $GetCitizenshipListResult = null;

    /**
     * @param ArrayOfCitizenshipDTO $GetCitizenshipListResult
     */
    public function __construct($GetCitizenshipListResult)
    {
      $this->GetCitizenshipListResult = $GetCitizenshipListResult;
    }

    /**
     * @return ArrayOfCitizenshipDTO
     */
    public function getGetCitizenshipListResult()
    {
      return $this->GetCitizenshipListResult;
    }

    /**
     * @param ArrayOfCitizenshipDTO $GetCitizenshipListResult
     * @return GetCitizenshipListResponse
     */
    public function setGetCitizenshipListResult($GetCitizenshipListResult)
    {
      $this->GetCitizenshipListResult = $GetCitizenshipListResult;
      return $this;
    }

}
