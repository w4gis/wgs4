<?php

class ArrayOfScholarshipOrderInfoDTO
{

    /**
     * @var ScholarshipOrderInfoDTO[] $ScholarshipOrderInfoDTO
     */
    protected $ScholarshipOrderInfoDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ScholarshipOrderInfoDTO[]
     */
    public function getScholarshipOrderInfoDTO()
    {
      return $this->ScholarshipOrderInfoDTO;
    }

    /**
     * @param ScholarshipOrderInfoDTO[] $ScholarshipOrderInfoDTO
     * @return ArrayOfScholarshipOrderInfoDTO
     */
    public function setScholarshipOrderInfoDTO(array $ScholarshipOrderInfoDTO)
    {
      $this->ScholarshipOrderInfoDTO = $ScholarshipOrderInfoDTO;
      return $this;
    }

}
