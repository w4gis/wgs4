<?php

class GetBeginSessionStudentsResponse
{

    /**
     * @var ArrayOfLong $GetBeginSessionStudentsResult
     */
    protected $GetBeginSessionStudentsResult = null;

    /**
     * @param ArrayOfLong $GetBeginSessionStudentsResult
     */
    public function __construct($GetBeginSessionStudentsResult)
    {
      $this->GetBeginSessionStudentsResult = $GetBeginSessionStudentsResult;
    }

    /**
     * @return ArrayOfLong
     */
    public function getGetBeginSessionStudentsResult()
    {
      return $this->GetBeginSessionStudentsResult;
    }

    /**
     * @param ArrayOfLong $GetBeginSessionStudentsResult
     * @return GetBeginSessionStudentsResponse
     */
    public function setGetBeginSessionStudentsResult($GetBeginSessionStudentsResult)
    {
      $this->GetBeginSessionStudentsResult = $GetBeginSessionStudentsResult;
      return $this;
    }

}
