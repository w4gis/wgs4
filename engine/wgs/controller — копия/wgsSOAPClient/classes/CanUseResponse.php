<?php

class CanUseResponse
{

    /**
     * @var boolean $CanUseResult
     */
    protected $CanUseResult = null;

    /**
     * @param boolean $CanUseResult
     */
    public function __construct($CanUseResult)
    {
      $this->CanUseResult = $CanUseResult;
    }

    /**
     * @return boolean
     */
    public function getCanUseResult()
    {
      return $this->CanUseResult;
    }

    /**
     * @param boolean $CanUseResult
     * @return CanUseResponse
     */
    public function setCanUseResult($CanUseResult)
    {
      $this->CanUseResult = $CanUseResult;
      return $this;
    }

}
