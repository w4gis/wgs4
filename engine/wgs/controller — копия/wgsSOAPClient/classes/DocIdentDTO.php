<?php

class DocIdentDTO
{

    /**
     * @var int $study_id
     */
    protected $study_id = null;

    /**
     * @var int $identify_id
     */
    protected $identify_id = null;

    /**
     * @var int $student_id
     */
    protected $student_id = null;

    /**
     * @var string $doc_ser
     */
    protected $doc_ser = null;

    /**
     * @var string $doc_num
     */
    protected $doc_num = null;

    /**
     * @var \DateTime $doc_from
     */
    protected $doc_from = null;

    /**
     * @var \DateTime $doc_to
     */
    protected $doc_to = null;

    /**
     * @var string $doc_who
     */
    protected $doc_who = null;

    /**
     * @var string $doc_birthplace
     */
    protected $doc_birthplace = null;

    /**
     * @var \DateTime $first_order_date
     */
    protected $first_order_date = null;

    /**
     * @var string $first_order_num
     */
    protected $first_order_num = null;

    /**
     * @param int $study_id
     * @param int $identify_id
     * @param int $student_id
     * @param string $doc_ser
     * @param string $doc_num
     * @param \DateTime $doc_from
     * @param \DateTime $doc_to
     * @param string $doc_who
     * @param string $doc_birthplace
     * @param \DateTime $first_order_date
     * @param string $first_order_num
     */
    public function __construct($study_id, $identify_id, $student_id, $doc_ser, $doc_num, \DateTime $doc_from, \DateTime $doc_to, $doc_who, $doc_birthplace, \DateTime $first_order_date, $first_order_num)
    {
      $this->study_id = $study_id;
      $this->identify_id = $identify_id;
      $this->student_id = $student_id;
      $this->doc_ser = $doc_ser;
      $this->doc_num = $doc_num;
      $this->doc_from = $doc_from->format(\DateTime::ATOM);
      $this->doc_to = $doc_to->format(\DateTime::ATOM);
      $this->doc_who = $doc_who;
      $this->doc_birthplace = $doc_birthplace;
      $this->first_order_date = $first_order_date->format(\DateTime::ATOM);
      $this->first_order_num = $first_order_num;
    }

    /**
     * @return int
     */
    public function getStudy_id()
    {
      return $this->study_id;
    }

    /**
     * @param int $study_id
     * @return DocIdentDTO
     */
    public function setStudy_id($study_id)
    {
      $this->study_id = $study_id;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdentify_id()
    {
      return $this->identify_id;
    }

    /**
     * @param int $identify_id
     * @return DocIdentDTO
     */
    public function setIdentify_id($identify_id)
    {
      $this->identify_id = $identify_id;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudent_id()
    {
      return $this->student_id;
    }

    /**
     * @param int $student_id
     * @return DocIdentDTO
     */
    public function setStudent_id($student_id)
    {
      $this->student_id = $student_id;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_ser()
    {
      return $this->doc_ser;
    }

    /**
     * @param string $doc_ser
     * @return DocIdentDTO
     */
    public function setDoc_ser($doc_ser)
    {
      $this->doc_ser = $doc_ser;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_num()
    {
      return $this->doc_num;
    }

    /**
     * @param string $doc_num
     * @return DocIdentDTO
     */
    public function setDoc_num($doc_num)
    {
      $this->doc_num = $doc_num;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDoc_from()
    {
      if ($this->doc_from == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->doc_from);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $doc_from
     * @return DocIdentDTO
     */
    public function setDoc_from(\DateTime $doc_from)
    {
      $this->doc_from = $doc_from->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDoc_to()
    {
      if ($this->doc_to == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->doc_to);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $doc_to
     * @return DocIdentDTO
     */
    public function setDoc_to(\DateTime $doc_to)
    {
      $this->doc_to = $doc_to->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_who()
    {
      return $this->doc_who;
    }

    /**
     * @param string $doc_who
     * @return DocIdentDTO
     */
    public function setDoc_who($doc_who)
    {
      $this->doc_who = $doc_who;
      return $this;
    }

    /**
     * @return string
     */
    public function getDoc_birthplace()
    {
      return $this->doc_birthplace;
    }

    /**
     * @param string $doc_birthplace
     * @return DocIdentDTO
     */
    public function setDoc_birthplace($doc_birthplace)
    {
      $this->doc_birthplace = $doc_birthplace;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFirst_order_date()
    {
      if ($this->first_order_date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->first_order_date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $first_order_date
     * @return DocIdentDTO
     */
    public function setFirst_order_date(\DateTime $first_order_date)
    {
      $this->first_order_date = $first_order_date->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getFirst_order_num()
    {
      return $this->first_order_num;
    }

    /**
     * @param string $first_order_num
     * @return DocIdentDTO
     */
    public function setFirst_order_num($first_order_num)
    {
      $this->first_order_num = $first_order_num;
      return $this;
    }

}
