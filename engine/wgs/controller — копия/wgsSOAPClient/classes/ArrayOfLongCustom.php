<?php

class ArrayOfLongCustom
{

    /**
     * @var long[] $long
     */
    protected $long = null;

    /**
     * @param long[] $long
     */
    public function __construct(array $long)
    {
      $this->long = $long;
    }

    /**
     * @return long[]
     */
    public function getLong()
    {
      return $this->long;
    }

    /**
     * @param long[] $long
     * @return ArrayOfLong
     */
    public function setLong(array $long)
    {
      $this->long = $long;
      return $this;
    }

}
