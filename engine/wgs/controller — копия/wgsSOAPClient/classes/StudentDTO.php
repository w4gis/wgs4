<?php

class StudentDTO
{

    /**
     * @var int $PersonId
     */
    protected $PersonId = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @var int $CitizenshipId
     */
    protected $CitizenshipId = null;

    /**
     * @var EducationDTO $Education
     */
    protected $Education = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $MiddleName
     */
    protected $MiddleName = null;

    /**
     * @var \DateTime $BirthDate
     */
    protected $BirthDate = null;

    /**
     * @var string $Sex
     */
    protected $Sex = null;

    /**
     * @var int $Married
     */
    protected $Married = null;

    /**
     * @var string $ZachNumber
     */
    protected $ZachNumber = null;

    /**
     * @var int $EduCount
     */
    protected $EduCount = null;

    /**
     * @var boolean $Gpo
     */
    protected $Gpo = null;

    /**
     * @var boolean $Problem
     */
    protected $Problem = null;

    /**
     * @var boolean $Privelege
     */
    protected $Privelege = null;

    /**
     * @var int $sessionExt
     */
    protected $sessionExt = null;

    /**
     * @var \DateTime $endSession
     */
    protected $endSession = null;

    /**
     * @var int $indGrafic
     */
    protected $indGrafic = null;

    /**
     * @var \DateTime $beginGrafic
     */
    protected $beginGrafic = null;

    /**
     * @var \DateTime $endGrafic
     */
    protected $endGrafic = null;

    /**
     * @var GroupDTO $Group
     */
    protected $Group = null;

    /**
     * @var string $Financing
     */
    protected $Financing = null;

    /**
     * @var StudentStateDTO $StudentState
     */
    protected $StudentState = null;

    /**
     * @var FilialDTO $Filial
     */
    protected $Filial = null;

    /**
     * @var CitizenshipDTO $Citizenship
     */
    protected $Citizenship = null;

    /**
     * @param int $PersonId
     * @param int $StudyId
     * @param int $CitizenshipId
     * @param EducationDTO $Education
     * @param string $LastName
     * @param string $FirstName
     * @param string $MiddleName
     * @param \DateTime $BirthDate
     * @param string $Sex
     * @param int $Married
     * @param string $ZachNumber
     * @param int $EduCount
     * @param boolean $Gpo
     * @param boolean $Problem
     * @param boolean $Privelege
     * @param int $sessionExt
     * @param \DateTime $endSession
     * @param int $indGrafic
     * @param \DateTime $beginGrafic
     * @param \DateTime $endGrafic
     * @param GroupDTO $Group
     * @param string $Financing
     * @param StudentStateDTO $StudentState
     * @param FilialDTO $Filial
     * @param CitizenshipDTO $Citizenship
     */
    public function __construct($PersonId, $StudyId, $CitizenshipId, $Education, $LastName, $FirstName, $MiddleName, \DateTime $BirthDate, $Sex, $Married, $ZachNumber, $EduCount, $Gpo, $Problem, $Privelege, $sessionExt, \DateTime $endSession, $indGrafic, \DateTime $beginGrafic, \DateTime $endGrafic, $Group, $Financing, $StudentState, $Filial, $Citizenship)
    {
      $this->PersonId = $PersonId;
      $this->StudyId = $StudyId;
      $this->CitizenshipId = $CitizenshipId;
      $this->Education = $Education;
      $this->LastName = $LastName;
      $this->FirstName = $FirstName;
      $this->MiddleName = $MiddleName;
      $this->BirthDate = $BirthDate->format(\DateTime::ATOM);
      $this->Sex = $Sex;
      $this->Married = $Married;
      $this->ZachNumber = $ZachNumber;
      $this->EduCount = $EduCount;
      $this->Gpo = $Gpo;
      $this->Problem = $Problem;
      $this->Privelege = $Privelege;
      $this->sessionExt = $sessionExt;
      $this->endSession = $endSession->format(\DateTime::ATOM);
      $this->indGrafic = $indGrafic;
      $this->beginGrafic = $beginGrafic->format(\DateTime::ATOM);
      $this->endGrafic = $endGrafic->format(\DateTime::ATOM);
      $this->Group = $Group;
      $this->Financing = $Financing;
      $this->StudentState = $StudentState;
      $this->Filial = $Filial;
      $this->Citizenship = $Citizenship;
    }

    /**
     * @return int
     */
    public function getPersonId()
    {
      return $this->PersonId;
    }

    /**
     * @param int $PersonId
     * @return StudentDTO
     */
    public function setPersonId($PersonId)
    {
      $this->PersonId = $PersonId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return StudentDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return int
     */
    public function getCitizenshipId()
    {
      return $this->CitizenshipId;
    }

    /**
     * @param int $CitizenshipId
     * @return StudentDTO
     */
    public function setCitizenshipId($CitizenshipId)
    {
      $this->CitizenshipId = $CitizenshipId;
      return $this;
    }

    /**
     * @return EducationDTO
     */
    public function getEducation()
    {
      return $this->Education;
    }

    /**
     * @param EducationDTO $Education
     * @return StudentDTO
     */
    public function setEducation($Education)
    {
      $this->Education = $Education;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return StudentDTO
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return StudentDTO
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return StudentDTO
     */
    public function setMiddleName($MiddleName)
    {
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
      if ($this->BirthDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->BirthDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $BirthDate
     * @return StudentDTO
     */
    public function setBirthDate(\DateTime $BirthDate)
    {
      $this->BirthDate = $BirthDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getSex()
    {
      return $this->Sex;
    }

    /**
     * @param string $Sex
     * @return StudentDTO
     */
    public function setSex($Sex)
    {
      $this->Sex = $Sex;
      return $this;
    }

    /**
     * @return int
     */
    public function getMarried()
    {
      return $this->Married;
    }

    /**
     * @param int $Married
     * @return StudentDTO
     */
    public function setMarried($Married)
    {
      $this->Married = $Married;
      return $this;
    }

    /**
     * @return string
     */
    public function getZachNumber()
    {
      return $this->ZachNumber;
    }

    /**
     * @param string $ZachNumber
     * @return StudentDTO
     */
    public function setZachNumber($ZachNumber)
    {
      $this->ZachNumber = $ZachNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getEduCount()
    {
      return $this->EduCount;
    }

    /**
     * @param int $EduCount
     * @return StudentDTO
     */
    public function setEduCount($EduCount)
    {
      $this->EduCount = $EduCount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGpo()
    {
      return $this->Gpo;
    }

    /**
     * @param boolean $Gpo
     * @return StudentDTO
     */
    public function setGpo($Gpo)
    {
      $this->Gpo = $Gpo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getProblem()
    {
      return $this->Problem;
    }

    /**
     * @param boolean $Problem
     * @return StudentDTO
     */
    public function setProblem($Problem)
    {
      $this->Problem = $Problem;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPrivelege()
    {
      return $this->Privelege;
    }

    /**
     * @param boolean $Privelege
     * @return StudentDTO
     */
    public function setPrivelege($Privelege)
    {
      $this->Privelege = $Privelege;
      return $this;
    }

    /**
     * @return int
     */
    public function getSessionExt()
    {
      return $this->sessionExt;
    }

    /**
     * @param int $sessionExt
     * @return StudentDTO
     */
    public function setSessionExt($sessionExt)
    {
      $this->sessionExt = $sessionExt;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndSession()
    {
      if ($this->endSession == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->endSession);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $endSession
     * @return StudentDTO
     */
    public function setEndSession(\DateTime $endSession)
    {
      $this->endSession = $endSession->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getIndGrafic()
    {
      return $this->indGrafic;
    }

    /**
     * @param int $indGrafic
     * @return StudentDTO
     */
    public function setIndGrafic($indGrafic)
    {
      $this->indGrafic = $indGrafic;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginGrafic()
    {
      if ($this->beginGrafic == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->beginGrafic);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $beginGrafic
     * @return StudentDTO
     */
    public function setBeginGrafic(\DateTime $beginGrafic)
    {
      $this->beginGrafic = $beginGrafic->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndGrafic()
    {
      if ($this->endGrafic == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->endGrafic);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $endGrafic
     * @return StudentDTO
     */
    public function setEndGrafic(\DateTime $endGrafic)
    {
      $this->endGrafic = $endGrafic->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return GroupDTO
     */
    public function getGroup()
    {
      return $this->Group;
    }

    /**
     * @param GroupDTO $Group
     * @return StudentDTO
     */
    public function setGroup($Group)
    {
      $this->Group = $Group;
      return $this;
    }

    /**
     * @return string
     */
    public function getFinancing()
    {
      return $this->Financing;
    }

    /**
     * @param string $Financing
     * @return StudentDTO
     */
    public function setFinancing($Financing)
    {
      $this->Financing = $Financing;
      return $this;
    }

    /**
     * @return StudentStateDTO
     */
    public function getStudentState()
    {
      return $this->StudentState;
    }

    /**
     * @param StudentStateDTO $StudentState
     * @return StudentDTO
     */
    public function setStudentState($StudentState)
    {
      $this->StudentState = $StudentState;
      return $this;
    }

    /**
     * @return FilialDTO
     */
    public function getFilial()
    {
      return $this->Filial;
    }

    /**
     * @param FilialDTO $Filial
     * @return StudentDTO
     */
    public function setFilial($Filial)
    {
      $this->Filial = $Filial;
      return $this;
    }

    /**
     * @return CitizenshipDTO
     */
    public function getCitizenship()
    {
      return $this->Citizenship;
    }

    /**
     * @param CitizenshipDTO $Citizenship
     * @return StudentDTO
     */
    public function setCitizenship($Citizenship)
    {
      $this->Citizenship = $Citizenship;
      return $this;
    }

}
