<?php

class ArrayOfStudentDTO
{

    /**
     * @var StudentDTO[] $StudentDTO
     */
    protected $StudentDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StudentDTO[]
     */
    public function getStudentDTO()
    {
      return $this->StudentDTO;
    }

    /**
     * @param StudentDTO[] $StudentDTO
     * @return ArrayOfStudentDTO
     */
    public function setStudentDTO(array $StudentDTO)
    {
      $this->StudentDTO = $StudentDTO;
      return $this;
    }

}
