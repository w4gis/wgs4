<?php

class SubDivisionDTO
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @param int $Id
     * @param string $Name
     */
    public function __construct($Id, $Name)
    {
      $this->Id = $Id;
      $this->Name = $Name;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return SubDivisionDTO
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return SubDivisionDTO
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
