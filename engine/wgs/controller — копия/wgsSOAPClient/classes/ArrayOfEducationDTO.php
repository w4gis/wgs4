<?php

class ArrayOfEducationDTO
{

    /**
     * @var EducationDTO[] $EducationDTO
     */
    protected $EducationDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EducationDTO[]
     */
    public function getEducationDTO()
    {
      return $this->EducationDTO;
    }

    /**
     * @param EducationDTO[] $EducationDTO
     * @return ArrayOfEducationDTO
     */
    public function setEducationDTO(array $EducationDTO)
    {
      $this->EducationDTO = $EducationDTO;
      return $this;
    }

}
