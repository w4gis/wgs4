<?php

class GetFilialsResponse
{

    /**
     * @var ArrayOfFilialDTO $GetFilialsResult
     */
    protected $GetFilialsResult = null;

    /**
     * @param ArrayOfFilialDTO $GetFilialsResult
     */
    public function __construct($GetFilialsResult)
    {
      $this->GetFilialsResult = $GetFilialsResult;
    }

    /**
     * @return ArrayOfFilialDTO
     */
    public function getGetFilialsResult()
    {
      return $this->GetFilialsResult;
    }

    /**
     * @param ArrayOfFilialDTO $GetFilialsResult
     * @return GetFilialsResponse
     */
    public function setGetFilialsResult($GetFilialsResult)
    {
      $this->GetFilialsResult = $GetFilialsResult;
      return $this;
    }

}
