<?php

class getStudentInfoForEduResponse
{

    /**
     * @var ArrayOfStudentInfoForEduDTO $getStudentInfoForEduResult
     */
    protected $getStudentInfoForEduResult = null;

    /**
     * @param ArrayOfStudentInfoForEduDTO $getStudentInfoForEduResult
     */
    public function __construct($getStudentInfoForEduResult)
    {
      $this->getStudentInfoForEduResult = $getStudentInfoForEduResult;
    }

    /**
     * @return ArrayOfStudentInfoForEduDTO
     */
    public function getGetStudentInfoForEduResult()
    {
      return $this->getStudentInfoForEduResult;
    }

    /**
     * @param ArrayOfStudentInfoForEduDTO $getStudentInfoForEduResult
     * @return getStudentInfoForEduResponse
     */
    public function setGetStudentInfoForEduResult($getStudentInfoForEduResult)
    {
      $this->getStudentInfoForEduResult = $getStudentInfoForEduResult;
      return $this;
    }

}
