<?php

class GetAllOrderHeaderGreaterDateCreateResponse
{

    /**
     * @var ArrayOfOrderHeaderDTO $GetAllOrderHeaderGreaterDateCreateResult
     */
    protected $GetAllOrderHeaderGreaterDateCreateResult = null;

    /**
     * @param ArrayOfOrderHeaderDTO $GetAllOrderHeaderGreaterDateCreateResult
     */
    public function __construct($GetAllOrderHeaderGreaterDateCreateResult)
    {
      $this->GetAllOrderHeaderGreaterDateCreateResult = $GetAllOrderHeaderGreaterDateCreateResult;
    }

    /**
     * @return ArrayOfOrderHeaderDTO
     */
    public function getGetAllOrderHeaderGreaterDateCreateResult()
    {
      return $this->GetAllOrderHeaderGreaterDateCreateResult;
    }

    /**
     * @param ArrayOfOrderHeaderDTO $GetAllOrderHeaderGreaterDateCreateResult
     * @return GetAllOrderHeaderGreaterDateCreateResponse
     */
    public function setGetAllOrderHeaderGreaterDateCreateResult($GetAllOrderHeaderGreaterDateCreateResult)
    {
      $this->GetAllOrderHeaderGreaterDateCreateResult = $GetAllOrderHeaderGreaterDateCreateResult;
      return $this;
    }

}
