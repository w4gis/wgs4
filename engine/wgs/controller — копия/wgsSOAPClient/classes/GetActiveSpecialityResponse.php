<?php

class GetActiveSpecialityResponse
{

    /**
     * @var ArrayOfSpecialityDTO $GetActiveSpecialityResult
     */
    protected $GetActiveSpecialityResult = null;

    /**
     * @param ArrayOfSpecialityDTO $GetActiveSpecialityResult
     */
    public function __construct($GetActiveSpecialityResult)
    {
      $this->GetActiveSpecialityResult = $GetActiveSpecialityResult;
    }

    /**
     * @return ArrayOfSpecialityDTO
     */
    public function getGetActiveSpecialityResult()
    {
      return $this->GetActiveSpecialityResult;
    }

    /**
     * @param ArrayOfSpecialityDTO $GetActiveSpecialityResult
     * @return GetActiveSpecialityResponse
     */
    public function setGetActiveSpecialityResult($GetActiveSpecialityResult)
    {
      $this->GetActiveSpecialityResult = $GetActiveSpecialityResult;
      return $this;
    }

}
