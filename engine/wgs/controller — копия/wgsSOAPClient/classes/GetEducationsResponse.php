<?php

class GetEducationsResponse
{

    /**
     * @var ArrayOfEducationDTO $GetEducationsResult
     */
    protected $GetEducationsResult = null;

    /**
     * @param ArrayOfEducationDTO $GetEducationsResult
     */
    public function __construct($GetEducationsResult)
    {
      $this->GetEducationsResult = $GetEducationsResult;
    }

    /**
     * @return ArrayOfEducationDTO
     */
    public function getGetEducationsResult()
    {
      return $this->GetEducationsResult;
    }

    /**
     * @param ArrayOfEducationDTO $GetEducationsResult
     * @return GetEducationsResponse
     */
    public function setGetEducationsResult($GetEducationsResult)
    {
      $this->GetEducationsResult = $GetEducationsResult;
      return $this;
    }

}
