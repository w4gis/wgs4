<?php

class FacultyDTO
{

    /**
     * @var int $FacultyId
     */
    protected $FacultyId = null;

    /**
     * @var string $FacultyName
     */
    protected $FacultyName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    /**
     * @param int $FacultyId
     * @param string $FacultyName
     * @param string $ShortName
     */
    public function __construct($FacultyId, $FacultyName, $ShortName)
    {
      $this->FacultyId = $FacultyId;
      $this->FacultyName = $FacultyName;
      $this->ShortName = $ShortName;
    }

    /**
     * @return int
     */
    public function getFacultyId()
    {
      return $this->FacultyId;
    }

    /**
     * @param int $FacultyId
     * @return FacultyDTO
     */
    public function setFacultyId($FacultyId)
    {
      $this->FacultyId = $FacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFacultyName()
    {
      return $this->FacultyName;
    }

    /**
     * @param string $FacultyName
     * @return FacultyDTO
     */
    public function setFacultyName($FacultyName)
    {
      $this->FacultyName = $FacultyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return FacultyDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
