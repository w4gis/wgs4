<?php

lmb_require('limb/filter_chain/src/lmbInterceptingFilter.interface.php');
lmb_require('limb/dbal/src/exception/lmbDbException.class.php');
lmb_require('wgs/exception/wgsException.class.php');
lmb_require('wgs/model/wgsPrivilege.class.php');

class wgsSysAccessFilter implements lmbInterceptingFilter
{
    private $response;
    private $request;
    private $wgsTier;

    function run($filter_chain)
    {
        $toolkit        = lmbToolkit :: instance();
        $this->response = $toolkit->getResponse();
        $this->request  = $toolkit->getRequest();
        $this->wgsTier  = $toolkit->getWgsTier();
		//var_dump($this->wgsTier->getProjectTier()->isAuthorized());die();
        
		try
        {
        	if ($this->ajaxMode())
                error_reporting(0);

            if (!$this->isBuiltInModule($this->request))
            {
            	$this->wgsTier->open();
            	$this->checkPermissions();
            	
            }
            $filter_chain->next();
        }
        catch(lmbDbException $exception)
        {
            $code = $exception->getParam('code');
            $message = $exception->getMessage();
            $exception = new wgsException($code, nl2br($message));
            if ($this->ajaxMode())
                $exception->response($this->response);
        }
        catch(wgsException $exception)
        {
            if ($this->ajaxMode())
                $exception->response($this->response);
            else
            switch ($exception->getCode())
            {
                case wgsException::AUTHORIZATION_REQUIRED:
                {
					//var_dump('AUTHORIZATION REQUIRED'); die();
                    $this->response->setCookie('return', $this->request->getUri()->getPath());
                    if ($this->request->get('public') == 1) {
                        $this->response->setCookie('public', true);
                    }
                    $this->response->redirect('/info');
                    $this->response->commit();
                } break;
                default:
                    $exception->handle();
                break;
            }
        }
        catch(MgException $exception)
        {
            if ($this->ajaxMode())
            {
                //$exception->response($this->response);
                /*
                $this->response->write(json_encode(array('exception' => wgsException::MAPGUIDE_EXCEPTION,
                                                         'message' => 'MapGuide: '.$exception->GetMessage())));
				*/
                $this->response->write(json_encode(array('exception' => wgsException::MAPGUIDE_EXCEPTION,
                                                         'message' => 'MapGuide: '.$exception->GetDetails() /*.'<br/>'. $exception->GetStackTrace()*/)));
                $this->response->commit();                
            }
        }
    }

    private function ajaxMode()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
    }

    private function checkPermissions()
    {
        $level = count($this->request->getUri()->getPathElements());
        //var_dump($this->wgsTier->getSysTier()->isAuthorized());
        if ($this->wgsTier->getSysTier()->isAuthorized()) {
            for($i = $level-1; $i > 0; $i--) {
                if ($module = $this->wgsTier->getSysObjectService()->getSysModuleService()->getModuleByUrl($this->request->getUri()->getPathToLevel($i)))
                    break;
            }
        }
        else
        if ($this->wgsTier->getProjectTier()->isAuthorized()) {
			//echo 'AUTHORIZED';
            for($i = $level-1; $i > 0; $i--){
				//echo 
                if ($module = $this->wgsTier->getSysObjectService()->getSysModuleService()->getProjectModuleByUrl($this->request->getUri()->getPathToLevel($i), getProjectName()))
                    break;
					
			}
        }
        if (!$module)
            throw new wgsException(wgsException::MODULE_NOT_FOUND);
        elseif ($module->getLocked())
            throw new wgsException(wgsException::MODULE_IS_LOCKED);

        $user = $this->wgsTier->getAuthorizedUser();
        $acl  = $this->wgsTier->getACL();
        if (!$acl->isAllowed($user->getRoleId(), $module, wgsPrivilege::SYSMODULE_ACCESS)) {
            throw new wgsException(wgsException::INSUFFICIENT_PRIVILEGES);
        }
    }

    private function isBuiltInModule(& $request)
    {
        $requestedModule = $request->getUri()->getPathElements();
        $possibleModules = array(
            '/',
            '/info',
            '/exit',
        	'/component',
            '/authorization/login',
            '/authorization/logout',
            '/authorization/status',
        	'/message',
            '/mapguideagent'
        );
        foreach($possibleModules as $possibleModule)
        {
            $possibleModuleLevels = explode("/", $possibleModule);
            if ( count( array_diff_assoc($possibleModuleLevels, $requestedModule) ) == 0 )
                return true;
        }
        return false;
    }
}
?>
