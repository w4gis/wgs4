<?php
/**********************************************************************************
* Copyright 2004 BIT, Ltd. http://limb-project.com, mailto: support@limb-project.com
*
* Released under the LGPL license (http://www.gnu.org/copyleft/lesser.html)
***********************************************************************************
*
* $Id: MainPageController.class.php 4443 2006-11-07 14:50:29Z pachanga $
*
***********************************************************************************/

lmb_require('wgs/controller/wgsFrontController.class.php');

class IndexController extends wgsFrontController
{
    function doExit()
    {
        $this->response->redirect('/authorization/logout');
    }

	function doDisplay()
    {
        $this->response->redirect('/map');
    }

    function doInfo()
    {

        $this->setTemplate('login.html');
        $this->passToView("ip", $_SERVER['REMOTE_ADDR']);

        //$this->passToView("title", ":: WGS3 ::");
        //$this->startFrontEndApplication(array('/js/wgs/info.js'), 'Info');
    }

    function doInfoDesktop()
    {
        $this->setTemplate('main.html');
        $this->passToView("title", ":: WGS3 ::");

        $this->startFrontEndApplication(array('/js/wgs/info.js'), 'Info');
    }

    /*function doInformation()
    {
        $topmenu = $this->createTopMenu();

        $this->setTemplate('information.html');
        $info = $this->view->render();

        $this->setTemplate('main.html');
        $this->passToView("title", ":: WGS3 Project Admin ::");
        $this->passToView("body", $topmenu.$info);
    } */

    function doInformation()
    {
        $topmenu = $this->createTopMenu();

        $this->setTemplate('information.html');
        $info = $this->view->render();

        $this->setTemplate('main.html');
        $this->passToView("title", ":: WGS3 :: ���������� ::");
        $this->passToView("body", $topmenu.$info);

        if ($this->wgsTier->isProjectUser()) {
            $this->startFrontEndApplication(array(
                '/js/wgs/information.js'
                ), 'Information');
        }
    }
}

?>
