<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('wgs/model/wgsProjectObject.class.php');
lmb_require('wgs/model/wgsPermission.class.php');
lmb_require('wgs/model/wgsPrivilege.class.php');
lmb_require('wgs/model/wgsMgWebLayout.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class ObjectController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::PROJECTOBJECT_ADMIN);
    }

    function doGetRoleTree()
    {
        $roleArray = array();
        $roleList = $this->wgsTier->getProjectRoleService()->getList();
        foreach($roleList as $role)
        {
            $id = $role->getId();
            $roleArray['root'][$id] = array(
                'id' => $id,
                'text' => lmb_win1251_to_utf8($role->getDescription().' ('.$role->getName().')'),
                'leaf' => true
            );
        }
        if (isset($_POST['node']) && $roleArray[$_POST['node']])
            $this->response->write(json_encode(array_merge($roleArray[$_POST['node']])));
    }

    function doGetObjectTree()
    {
        $featureArray = array();
        // bottom layers
        $bottomLayerList = $this->wgsTier->getProjectObjectService()->getBottomLayerList();
        foreach($bottomLayerList as $bottomLayer)
        {
            $id = $bottomLayer->getObjectId();
            $featureArray['bl-root'][$id] = array(
                'id' => "bl_$id",
                'text' => $bottomLayer->getName().' ('.$bottomLayer->getMgMapName().')',
                'leaf' => true,
                'iconCls' => 'icon-layer'
            );
        }
        // feature layers        
        $featureLayerList = $this->wgsTier->getProjectObjectService()->getFeatureLayerList();
        $featureLayerGroupList = $this->wgsTier->getProjectObjectService()->getFeatureLayerGroupList();
        foreach($featureLayerGroupList as $featureLayerGroup)
        {
            $id = $featureLayerGroup->getId();
            $featureArray['fl-root'][$id] = array(
                'id' => "flg_$id",
                'text' => lmb_win1251_to_utf8($featureLayerGroup->getName()),
                'iconCls' => 'icon-layer-group'
            );
        }
        foreach($featureLayerList as $featureLayer)
        {
            $id = $featureLayer->getObjectId();
            $featureArray[$featureLayer->getFeatureLayerGroupId()][$id] = array(
                'id' => "fl_$id",
                'text' => lmb_win1251_to_utf8($featureLayer->getName()),
                'leaf' => true,
                'iconCls' => 'icon-layer'
            );
        }
        // view and command list
        $webLayoutList = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getWebLayoutList();
        foreach ($webLayoutList as $webLayout)
        {
            $id = $webLayout->getObjectId();
            $featureArray['cmd-root'][] = array(
                'id' => "cmd_$id",
                'text' => lmb_win1251_to_utf8($webLayout->getName()),
                'iconCls' => 'icon-weblayout'
            );
            if ($mgBuiltInCommands = $webLayout->getBuiltInCommands())
            {
                $featureArray[$id][] = array(
                    'id' => 'builtInCommands'.$id,
                    'text' => lmb_win1251_to_utf8('����������� �������')
                );
                foreach($mgBuiltInCommands as $mgBuiltInCommand)
                    $featureArray['builtInCommands'.$id][] = array(
                        'id' => $mgBuiltInCommand->getObjectId(),
                        'text' => lmb_win1251_to_utf8($mgBuiltInCommand->getDescription()),
                        'leaf' => true
                    );
            }
            if ($mgCustomCommands = $webLayout->getCustomCommands())
            {
                $featureArray[$id][] = array(
                    'id' => 'customCommands'.$id,
                    'text' => lmb_win1251_to_utf8('��������� �������')
                );
                foreach($mgCustomCommands as $mgCustomCommand)
                    $featureArray['customCommands'.$id][] = array(
                        'id' => $mgCustomCommand->getObjectId(),
                        'text' => lmb_win1251_to_utf8($mgCustomCommand->getDescription()),
                        'leaf' => true
                    );
            }
            if ($mgPanes = $webLayout->getPanes())
            {
                $featureArray[$id][] = array(
                    'id' => 'panes'.$id,
                    'text' => lmb_win1251_to_utf8('������')
                );
                foreach($mgPanes as $mgPane)
                    $featureArray['panes'.$id][] = array(
                        'id' => $mgPane->getObjectId(),
                        'text' => lmb_win1251_to_utf8($mgPane->getDescription()),
                        'leaf' => true
                    );
            }
        }
        //TODO: ��������������
        if (isset($_POST['node'])) {
            if (strstr($_POST['node'], '_')) {
                $q = explode("_", $_POST['node']);
                $_POST['node'] = $q[1];
            }
            if (isset($featureArray[$_POST['node']])) {
                $this->response->write(json_encode(array_merge($featureArray[$_POST['node']])));
            }
        }
    }

    function doGetObjectPermissions()
    {
        $id = $this->request->getRequest('id');
        if ($id) {
            if (strstr($id, '_')) {
                $q = explode("_", $id);
                $id = $q[1];
            }
            $permissions = $this->wgsTier->getProjectPermissionService()->getPermissionsByObjectId($id);
            $permissionArray = array();
            foreach($permissions as $permission)
            {
                $roleId = $permission->getRole()->getId();
                $permissionArray[$roleId]['roleId'] = $roleId;
                $permissionArray[$roleId]['roleName'] = lmb_win1251_to_utf8($permission->getRole()->getName());
                $permissionArray[$roleId]['roleDescription'] = lmb_win1251_to_utf8($permission->getRole()->getDescription());
                $permissionArray[$roleId]['parentRoles'] = lmb_win1251_to_utf8(implode(", ",$permission->getRole()->getParentsNames()));
                $permissionArray[$roleId]['childRoles'] = lmb_win1251_to_utf8(implode(", ",$permission->getRole()->getChildrenNames()));
                $permissionArray[$roleId]['privileges'][] = array(
                    'privilegeId' => "privilege-".$permission->getPrivilege()->getId(),
                    'privilegeName' => lmb_win1251_to_utf8($permission->getPrivilege()->getDescription()),
                    'permission' => $permission->getPermission()
                );
            }
            $this->response->write(json_encode(array_merge($permissionArray)));
        }
    }

    function doSetRolePermissions()
    {
        if ($this->request->getPost('permissions') && $this->request->getPost('roleid'))
        {
            $request = (json_decode($this->request->getPost('permissions'), true));
            $roleId = $this->request->getPost('roleid');
            $permissions = array();
            foreach($request as $permission)
            {
                $temp = explode("-", $permission['id']);
                $objectId = $temp[0];
                $privilegeId = $temp[1];
                $wgsProjectRole = new wgsProjectRole();
                $wgsProjectRole->setId($roleId);
                $wgsProjectObject = new wgsProjectObject();
                $wgsProjectObject->setObjectId($objectId);
                $wgsPermission = new wgsPermission();
                $wgsPrivilege = new wgsPrivilege();
                $wgsPrivilege->setId($privilegeId);
                $wgsPermission->setPrivilege($wgsPrivilege);
                $wgsPermission->setRole($wgsProjectRole);
                $wgsPermission->setObject($wgsProjectObject);
                $wgsPermission->setPermission($permission['permission']);
                $permissions[] = $wgsPermission;
            }
            if ($permissions)
                $this->wgsTier->getProjectPermissionService()->setPermissions($permissions);
        }
    }

    function doGetRolePermissions()
    {
        if ($this->request->getRequest('id'))
        {
            $permissions = $this->wgsTier->getProjectPermissionService()->getPermissionsByRoleId($this->request->getRequest('id'));
            $permissionArray = array();
            $permissionArray[0]['id'] = wgsProjectObject :: FEATURELAYER;
            $permissionArray[0]['text'] = lmb_win1251_to_utf8('���� � �������');
            $permissionArray[1]['id'] = wgsProjectObject ::MGMAPLAYER;
            $permissionArray[1]['text'] = lmb_win1251_to_utf8('��������');
            $permissionArray[2]['id'] = wgsProjectObject :: MGWEBLAYOUT;
            $permissionArray[2]['text'] = lmb_win1251_to_utf8('�������');

            foreach($permissions as $permission)
            {
                $object = $permission->getObject();
                switch ($object->getType())
                {
                    case wgsProjectObject :: FEATURELAYER:
                    {
                        $groupId = $object->getFeatureLayerGroupId();
                        $permissionArray[0]['c'][$groupId]['id'] = $groupId.'-'.wgsProjectObject :: FEATURELAYER;
                        $permissionArray[0]['c'][$groupId]['text'] = lmb_win1251_to_utf8($object->getFeatureLayerGroupName());
                        $permissionArray[0]['c'][$groupId]['c'][$object->getObjectId()]['id'] = $object->getObjectId().'-'.wgsPrivilege :: FEATURELAYER_ACCESS;
                        $permissionArray[0]['c'][$groupId]['c'][$object->getObjectId()]['text'] = lmb_win1251_to_utf8($object->getName());
                        $permissionArray[0]['c'][$groupId]['c'][$object->getObjectId()]['permission'] = $permission->getPermission();
                    } break;
                    case wgsProjectObject :: MGMAPLAYER:
                    {
                        if ($object->isBottomLayer())
                        {
                            $permissionArray[1]['c'][$object->getObjectId()]['id'] = $object->getObjectId().'-'.wgsPrivilege :: MGMAPLAYER_ACCESS;
                            $permissionArray[1]['c'][$object->getObjectId()]['text'] = $object->getName().' ('.$object->getMgMapName().')';
                            $permissionArray[1]['c'][$object->getObjectId()]['permission'] = $permission->getPermission();
                        }
                    } break;
                    case wgsProjectObject :: MGBUILTINCOMMAND:
                    {
                        $groupId = $object->getMgWebLayoutId();
                        $permissionArray[2]['c'][$groupId]['id'] = wgsProjectObject :: MGBUILTINCOMMAND;
                        $permissionArray[2]['c'][$groupId]['text'] = lmb_win1251_to_utf8($object->getMgWebLayoutName());
                        $permissionArray[2]['c'][$groupId]['c'][0]['id'] = $groupId.'-'.wgsProjectObject :: MGBUILTINCOMMAND;
                        $permissionArray[2]['c'][$groupId]['c'][0]['text'] = lmb_win1251_to_utf8('����������� �������');
                        $permissionArray[2]['c'][$groupId]['c'][0]['c'][$object->getObjectId()]['id'] = $object->getObjectId().'-'.wgsPrivilege :: MGBUILTINCOMMAND_ACCESS;
                        $permissionArray[2]['c'][$groupId]['c'][0]['c'][$object->getObjectId()]['text'] = lmb_win1251_to_utf8($object->getDescription());
                        $permissionArray[2]['c'][$groupId]['c'][0]['c'][$object->getObjectId()]['permission'] = $permission->getPermission();
                    } break;
                    case wgsProjectObject :: MGCUSTOMCOMMAND:
                    {
                        $groupId = $object->getMgWebLayoutId();
                        $permissionArray[2]['id'] = wgsProjectObject :: MGCUSTOMCOMMAND;
                        $permissionArray[2]['text'] = lmb_win1251_to_utf8('�������');
                        $permissionArray[2]['c'][$groupId]['id'] = $groupId.'-'.wgsProjectObject :: MGCUSTOMCOMMAND;
                        $permissionArray[2]['c'][$groupId]['text'] = lmb_win1251_to_utf8($object->getMgWebLayoutName());
                        $permissionArray[2]['c'][$groupId]['c'][1]['id'] = $groupId.'-'.$object->getObjectId().'-'.wgsProjectObject :: MGCUSTOMCOMMAND;
                        $permissionArray[2]['c'][$groupId]['c'][1]['text'] = lmb_win1251_to_utf8('��������� �������');
                        $permissionArray[2]['c'][$groupId]['c'][1]['c'][$object->getObjectId()]['id'] = $object->getObjectId().'-'.wgsPrivilege :: MGCUSTOMCOMMAND_ACCESS;
                        $permissionArray[2]['c'][$groupId]['c'][1]['c'][$object->getObjectId()]['text'] = lmb_win1251_to_utf8($object->getDescription());
                        $permissionArray[2]['c'][$groupId]['c'][1]['c'][$object->getObjectId()]['permission'] = $permission->getPermission();
                    } break;
                    case wgsProjectObject :: MGPANE:
                    {
                        $groupId = $object->getMgWebLayoutId();
                        $permissionArray[2]['id'] = wgsProjectObject :: MGPANE;
                        $permissionArray[2]['text'] = lmb_win1251_to_utf8('�������');
                        $permissionArray[2]['c'][$groupId]['id'] = $groupId.'-'.wgsProjectObject :: MGPANE;
                        $permissionArray[2]['c'][$groupId]['text'] = lmb_win1251_to_utf8($object->getMgWebLayoutName());
                        $permissionArray[2]['c'][$groupId]['c'][2]['id'] = $groupId.'-'.$object->getObjectId().'-'.wgsProjectObject :: MGPANE;
                        $permissionArray[2]['c'][$groupId]['c'][2]['text'] = lmb_win1251_to_utf8('������');
                        $permissionArray[2]['c'][$groupId]['c'][2]['c'][$object->getObjectId()]['id'] = $object->getObjectId().'-'.wgsPrivilege :: MGPANE_ACCESS;
                        $permissionArray[2]['c'][$groupId]['c'][2]['c'][$object->getObjectId()]['text'] = lmb_win1251_to_utf8($object->getDescription());
                        $permissionArray[2]['c'][$groupId]['c'][2]['c'][$object->getObjectId()]['permission'] = $permission->getPermission();
                    } break;
                }
            }

            function getPermissions($a, &$p = array(), $l = 0)
            {
                foreach($a as $v)
                {
                    array_push($p, array(
                        'id' => $v['id'],
                        'text' => '<div style="padding-left: '.($l*30).'px;"><img src="'.((isset($v['permission']))? '/images/leaf.gif': '/images/root.gif').'">&nbsp;'.$v['text'].'</div>',
                        'permission' => (isset($v['permission']))? $v['permission']: null
                    ));
                    if (isset($v['c']) && count($v['c']))
                        getPermissions($v['c'], $p, $l+1);
                }
                return $p;
            }

            $permissions = getPermissions($permissionArray);
            
            //var_dump( json_encode( getPermissions($permissionArray) ));
            $this->response->write(json_encode($permissions));
        }
    }

    function doSetObjectPermissions()
    {
        if ($this->request->getPost('permissions') && $this->request->getPost('objectid'))
        {
            $request = (json_decode($this->request->getPost('permissions'), true));
            $objectId = $this->request->getPost('objectid');
            if (strstr($objectId, "_") !== FALSE) {
                $objectId = explode("_", $objectId);
                $objectId = $objectId[1];
            }
            $permissions = array();
            foreach($request as $permission)
            {
                foreach($permission as $key => $value)
                    if (strstr($key, "privilege-"))
                    {
                        $roleId = $permission['roleId'];
                        $privilege = explode("-", $key);
                        $privilegeId = $privilege[1];
                        $wgsProjectRole = new wgsProjectRole();
                        $wgsProjectRole->setId($roleId);
                        $wgsProjectObject = new wgsProjectObject();
                        $wgsProjectObject->setObjectId($objectId);
                        $wgsPermission = new wgsPermission();
                        $wgsPrivilege = new wgsPrivilege();
                        $wgsPrivilege->setId($privilegeId);
                        $wgsPermission->setPrivilege($wgsPrivilege);
                        $wgsPermission->setRole($wgsProjectRole);
                        $wgsPermission->setObject($wgsProjectObject);
                        $wgsPermission->setPermission($value);
                        $permissions[] = $wgsPermission;
                    }
            }
            if ($permissions)
                $this->wgsTier->getProjectPermissionService()->setPermissions($permissions);
        }
    }

    function doSetDefaultLayout()
    {
        if ($this->request->getPost('id'))
        {
            $webLayout = new wgsMgWebLayout();
            $webLayout->setId($this->request->getPost('id'));
            $webLayout->setByDefault(1);
            $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->setDefaultWebLayout($webLayout);
        }
    }

    function doGetDefaultLayout()
    {
        if($webLayoutList = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getWebLayoutList())
        {
            $layoutArray = array();
            foreach($webLayoutList as $webLayout)
                $layoutArray[] = array(
                    'id' => $webLayout->getId(),
                    'bydefault' => $webLayout->getByDefault(),
                    'name' => lmb_win1251_to_utf8($webLayout->getName().' - '.$webLayout->getMgResourceId()),
                );
            $this->response->write(json_encode($layoutArray));
        }
    }

    function doSetOverviewLayout()
    {
        if ($this->request->getPost('id'))
        {
            $webLayout = new wgsMgWebLayout();
            $webLayout->setId($this->request->getPost('id'));
            $webLayout->setOverview(1);
            $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->setOverviewWebLayout($webLayout);
        }
    }

    function doGetOverviewLayout()
    {
        if($webLayoutList = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getWebLayoutList())
        {
            $layoutArray = array();
            foreach($webLayoutList as $webLayout)
                $layoutArray[] = array(
                    'id' => $webLayout->getId(),
                    'overview' => $webLayout->getOverview(),
                    'name' => lmb_win1251_to_utf8($webLayout->getName().' - '.$webLayout->getMgResourceId()),
                );
            $this->response->write(json_encode($layoutArray));
        }
    }
}

?>
