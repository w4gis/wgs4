<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');
class RoleController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::PROJECTROLE_ADMIN);        
    }
    
	function doSetArea(){
		$m = $this->request->getPost('m');
		$r = $this->request->getPost('r');
		$l = $this->request->getPost('l');
		$d = $this->request->getPost('d');
		$sdo = $this->request->getPost('s');
		if($m && $r)
			return $this->wgsTier->getProjectRoleService()->setArea($m,$r,$l,$d,$sdo);
		else return '-1';
	}
	function doGetAreaM(){
		$s = $this->request->getPost('s');
		if($s)
			$results = $this->wgsTier->getProjectRoleService()->getArea($m,$r);
			foreach($results as $result) {
				$records[] = array(
					'it' => $result->get('it'),
					'area' => $result->get('area'),
					);
			}
			return CJSON::encode(array(
				'success' => true,
				'record' => $records
			));
	}
	function doGetArea(){
		$m = $this->request->getPost('m');
		$r = $this->request->getPost('r');		
		if($m && $r){
			$records = array();
			$results = $this->wgsTier->getProjectRoleService()->getArea($m,$r);
			foreach($results as $result) {
				$records[] = array(
					'ftf' => $result->get('limit_ftf'),
					'display' => $result->get('limit_display'),
					'sdo' => $result->get('limit_sdo')
				);
			}
			return CJSON::encode(array(
				'success' => true,
				'record' => $records
			));
    	}	
		else return '-1';
	}
	
    function doGetAvialableRoles()
    {

        $roleList = array();
        if($this->request->getPost('id')==0)
        {
            $roles = $this->wgsTier->getProjectRoleService()->getList();
        }
        else
        {
            $roles = $this->wgsTier->getProjectRoleService()->getAcceptableChildrenRolesById($this->request->getPost('id'));
        }

        foreach ($roles as $role)
        {
            if(!(is_array($role->getParentWgsRoleIds())))
            {
                $id = $role->getId();
                $roleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
                );
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($roleList[$_POST['node']])));

    }

    function doGetChildrenRoles()
    {
        $roles = $this->wgsTier->getProjectRoleService()->getChildrenRolesById($this->request->getPost('id'));
        foreach ($roles as $role)
        {
            if(!(is_array($role->getParentWgsRoleIds())))
            {
                $id = $role->getId();
                $roleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
                );
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($roleList[$_POST['node']])));
    }

    function doGetAvialableUsers()
    {
        $users = $this->wgsTier->getProjectUserService()->getList();
        $userList = array();
        if($this->request->getPost('id') == 0)
        {
            foreach ($users as $user)
            {
                $id = $user->getId();
                $userList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($user->getName()),
                'leaf'=>true
                );
            }
        }
        else
        {
            $userMembers =  $this->wgsTier->getProjectUserService()->getUserMembersByRoleId($this->request->getPost('id'));
            foreach($users as $user)
            {
                if(count($userMembers) > 0)
                {
                    $k = 0;
                    foreach($userMembers as $usermember)
                    {
                        if($user->getId() != $usermember->getId())
                        {
                            $k++;
                        }
                        if ($k == count($userMembers))
                        {
                            //$availableUsers[$user->getId()]['availableSysUserId'] = $user->getId();
                            //$availableUsers[$user->getId()]['availableSysUserName'] = $user->getName();
                            $id = $user->getId();
                            $userList['root'][] = array(
                            'id'=>$id,
                            'text'=>lmb_win1251_to_utf8($user->getName()),
                            'leaf'=>true
                            );
                        }
                    }
                }
                else
                {
                    $id = $user->getId();
                    $userList['root'][] = array(
                    'id'=>$id,
                    'text'=>lmb_win1251_to_utf8($user->getName()),
                    'leaf'=>true
                    );
                }
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($userList[$_POST['node']])));

    }

    function doGetMemberUsers()
    {
        $userMembers =  $this->wgsTier->getProjectUserService()->getUserMembersByRoleId($this->request->getPost('id'));
        $userList = array();
        foreach ($userMembers as $user)
        {
            $id = $user->getId();
            $userList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($user->getName()),
                'leaf'=>true
            );
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($userList[$_POST['node']])));
    }

    function doGetEmptyTree()
    {

    }

    function doGetRoleList()
    {
        $roles = $this->wgsTier->getProjectRoleService()->getList();
        $roleList = array();
        foreach ($roles as $role)
        {
            /* if($role->getLocked() == 0)
             {*/
            $roleList[$role->getId()]['roleId'] = $role->getId();
            //$roleList[$role->getId()]['roleName'] = '<i><font color="#858585">'.$role->getName().'</font></i>';
            $roleList[$role->getId()]['roleName'] = lmb_win1251_to_utf8($role->getName());
            $roleList[$role->getId()]['description'] = lmb_win1251_to_utf8($role->getDescription());

            $childrenRoles = $this->wgsTier->getProjectRoleService()->getChildrenRolesById($role->getId());

            $sChildrenRoles = '';
            if(count($childrenRoles) > 0)
            {
                $k = 0;
                foreach ($childrenRoles as $childrenRole)
                {
                    if($k == count($childrenRoles)-1)
                    $sChildrenRoles.=lmb_win1251_to_utf8($childrenRole->getName());
                    else
                    $sChildrenRoles.=lmb_win1251_to_utf8($childrenRole->getName()).', ';
                    $k++;
                }
            }

            $roleList[$role->getId()]['childrenRoles'] = $sChildrenRoles;

            if(is_array($role->getWgsUserMembers()))
            {
                $roleList[$role->getId()]['roleUserMembers'] = lmb_win1251_to_utf8(implode(', ',$role->getWgsUserMembers()));
            }
            // }
            /*else
             {
             $roleList[$role->getId()]['roleId'] = $role->getId();
             //$roleList[$role->getId()]['roleName'] = '<i><font color="#858585">'.$role->getName().'</font></i>';
             $roleList[$role->getId()]['roleName'] = '<i><font color="#858585">'.$role->getName().'</font></i>';
             $roleList[$role->getId()]['description'] = '<i><font color="#858585">'.$role->getDescription().'</font></i>';

             $childrenRoles = $this->wgsTier->getProjectRoleService()->getChildrenRolesById($role->getId());

             $sChildrenRoles = '';
             if(count($childrenRoles) > 0)
             {
             $k = 0;
             foreach ($childrenRoles as $childrenRole)
             {
             if($k == count($childrenRoles)-1)
             $sChildrenRoles.=$childrenRole->getName();
             else
             $sChildrenRoles.=$childrenRole->getName().', ';
             $k++;
             }
             }

             $roleList[$role->getId()]['childrenRoles'] = '<i><font color="#858585">'.$sChildrenRoles.'</font></i>';

             if(is_array($role->getWgsUserMembers()))
             {
             $roleList[$role->getId()]['roleUserMembers'] = '<i><font color="#858585">'.implode(', ',$role->getWgsUserMembers()).'</font></i>';
             }
             }*/
        }


        $this->response->write(json_encode(array_merge($roleList)));

    }

    function doCreateRole()
    {
        $wgsProjectRole = new wgsProjectRole();
        $wgsProjectRole->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $wgsProjectRole->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));
        $wgsProjectRole->setLocked($this->request->getPost('locked'));
        //���������� id ����
        $wgsRoleId = $this->wgsTier->getProjectRoleService()->addRole($wgsProjectRole);
        $wgsProjectRole->setId($wgsRoleId);

        if(count($this->request->getPost('memberUserIds')) > 0)
        {
            $userMembers = $this->request->getPost('memberUserIds');

            foreach ($userMembers as $userMember)
            {
                if($userMember)
                {
                    $wgsProjectUser = new wgsProjectUser;
                    $wgsProjectUser->setId($userMember);
                    $this->wgsTier->getProjectUserService()->grantWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }
        if(count($this->request->getPost('childrenRoleIds')) > 0)
        {
            $roleChildren = $this->request->getPost('childrenRoleIds');
            foreach ($roleChildren as $roleChild)
            {
                if($roleChild)
                {
                    $wgsProjectRoleChild = new wgsProjectRole;
                    $wgsProjectRoleChild->setId($roleChild);
                    $this->wgsTier->getProjectRoleService()->grantWgsRoleMemberships($wgsProjectRoleChild, $wgsProjectRole);
                }
            }
        }
    }

    function doEditRole()
    {
        $wgsProjectRole = new wgsProjectRole();
        $wgsProjectRole->setId($this->request->getPost('id'));
        $wgsProjectRole->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $wgsProjectRole->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));
        $wgsProjectRole->setLocked($this->request->getPost('locked'));

        //try{
        $this->wgsTier->getProjectRoleService()->setRole($wgsProjectRole);
        /*}catch(lmbDbException $exception)
         {
         $exception->getCode()=='20003';
         }*/
        $wgsRoleId = $wgsProjectRole->getId();

        if(count($this->request->getPost('memberNotUserIds')) > 0)
        {
            $userNotMembers = $this->request->getPost('memberNotUserIds');

            foreach ($userNotMembers as $userNotMember)
            {
                if($userNotMember)
                {
                    $wgsProjectUser = new wgsProjectUser;
                    $wgsProjectUser->setId($userNotMember);
                    $this->wgsTier->getProjectUserService()->revokeWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }
        if(count($this->request->getPost('childrenNotRoleIds')) > 0)
        {
            $roleNotChildren = $this->request->getPost('childrenNotRoleIds');
            foreach ($roleNotChildren as $roleNotChild)
            {
                if($roleNotChild)
                {
                    $wgsProjectRoleChild = new wgsProjectRole();
                    $wgsProjectRoleChild->setId($roleNotChild);
                    $this->wgsTier->getProjectRoleService()->revokeWgsRoleMemberships($wgsProjectRoleChild, $wgsProjectRole);
                }
            }
        }

        if(count($this->request->getPost('memberUserIds')) > 0)
        {
            $userMembers = $this->request->getPost('memberUserIds');

            foreach ($userMembers as $userMember)
            {
                if($userMember)
                {
                    $wgsProjectUser = new wgsProjectUser;
                    $wgsProjectUser->setId($userMember);
                    $this->wgsTier->getProjectUserService()->grantWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }
        if(count($this->request->getPost('childrenRoleIds')) > 0)
        {
            $roleChildren = $this->request->getPost('childrenRoleIds');
            foreach ($roleChildren as $roleChild)
            {
                if($roleChild)
                {
                    $wgsProjectRoleChild = new wgsProjectRole();
                    $wgsProjectRoleChild->setId($roleChild);
                    $this->wgsTier->getProjectRoleService()->grantWgsRoleMemberships($wgsProjectRoleChild, $wgsProjectRole);
                }
            }
        }
    }

    function doRemoveRole ()
    {
        if(count($this->request->getPost('rolesForRemoveIds')) > 0)
        {
            $wgsRoleIds = $this->request->getPost('rolesForRemoveIds');

            foreach ($wgsRoleIds as $wgsRoleId)
            {
                $wgsProjectRole = new wgsProjectRole();
                $wgsProjectRole->setId($wgsRoleId);
                $this->wgsTier->getProjectRoleService()->removeRole($wgsProjectRole);
            }
        }
    }

    function doGetRoleProperties ()
    {
        $editRole = $this->wgsTier->getProjectRoleService()->getRoleById($this->request->getPost('id'));
        $roleList[$editRole->getId()]['roleId'] = $editRole->getId();
        $roleList[$editRole->getId()]['roleName'] = lmb_win1251_to_utf8($editRole->getName());
        $roleList[$editRole->getId()]['roleDescription'] = lmb_win1251_to_utf8($editRole->getDescription());
        $roleList[$editRole->getId()]['roleLocked'] = $editRole->getLocked();
        $this->response->write(json_encode(array_merge($roleList)));
    }
}
?>