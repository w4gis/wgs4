<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require(MAPVIEWER_DIR."/common.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require(MAPVIEWER_DIR."/layerdefinitionfactory.php");
lmb_require('limb/i18n/utf8.inc.php');

class MobileController extends wgsFrontController
{
    protected $mgTier;

    private function updateAclRevision()
    {
        $this->wgsTier->sessionSet('savedWebLayoutsAclRevision', $this->wgsTier->getAclRevision());
    }

    function doGetArea(){
        $input = $this->request->getRequest('area');
        return str_replace(',','.',$this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetArea($input));
    }
    private function isAclRevisionUpdated()
    {
        if ($this->wgsTier->getAclRevision() != $this->wgsTier->sessionGet('savedWebLayoutsAclRevision'))
            return true;
        return false;
    }


    function doDisplay()
    {

        if ($this->wgsTier->isProjectUser()) {
            $this->setTemplate('wgs45.html');
            $this->startFrontEndApplication(array(), 'Mobile', array(), "false");

        }
    }

    function doDebug()
    {
		//ini_set('display_errors',0);
		//error_reporting(E_NONE);
		//var_dump($this->wgsTier);
        if ($this->wgsTier->isProjectUser()) {
            $this->setTemplate('wgs4_mobile_debug.html');
            $this->startFrontEndApplication(array(), 'Mobile', array(), "false");
        }
    }

    function doTusur() {
        if ($this->wgsTier->isProjectUser()) {
            $this->setTemplate('wgs45_debug.html');
            $this->startFrontEndApplication(array(), 'Mobile', array(), "false");
        }
    }
}

?>
