<?php

class xsd_stringArray
{

    /**
     * @var string[] $item
     */
    protected $item = null;

    /**
     * @param string[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return string[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param string[] $item
     * @return xsd_stringArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
