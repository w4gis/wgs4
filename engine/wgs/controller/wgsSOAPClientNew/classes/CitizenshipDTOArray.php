<?php

class CitizenshipDTOArray
{

    /**
     * @var CitizenshipDTO[] $item
     */
    protected $item = null;

    /**
     * @param CitizenshipDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return CitizenshipDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param CitizenshipDTO[] $item
     * @return CitizenshipDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
