<?php

class SpecialityDTO
{

    /**
     * @var string $SpecialityId
     */
    protected $SpecialityId = null;

    /**
     * @var string $SpecialityCode
     */
    protected $SpecialityCode = null;

    /**
     * @var string $SpecialityName
     */
    protected $SpecialityName = null;

    /**
     * @var string $SpecialityTypeName
     */
    protected $SpecialityTypeName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSpecialityId()
    {
      return $this->SpecialityId;
    }

    /**
     * @param string $SpecialityId
     * @return SpecialityDTO
     */
    public function setSpecialityId($SpecialityId)
    {
      $this->SpecialityId = $SpecialityId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityCode()
    {
      return $this->SpecialityCode;
    }

    /**
     * @param string $SpecialityCode
     * @return SpecialityDTO
     */
    public function setSpecialityCode($SpecialityCode)
    {
      $this->SpecialityCode = $SpecialityCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityName()
    {
      return $this->SpecialityName;
    }

    /**
     * @param string $SpecialityName
     * @return SpecialityDTO
     */
    public function setSpecialityName($SpecialityName)
    {
      $this->SpecialityName = $SpecialityName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityTypeName()
    {
      return $this->SpecialityTypeName;
    }

    /**
     * @param string $SpecialityTypeName
     * @return SpecialityDTO
     */
    public function setSpecialityTypeName($SpecialityTypeName)
    {
      $this->SpecialityTypeName = $SpecialityTypeName;
      return $this;
    }

}
