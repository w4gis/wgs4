<?php

class StudentDTO
{

    /**
     * @var string $PersonId
     */
    protected $PersonId = null;

    /**
     * @var string $StudyId
     */
    protected $StudyId = null;

    /**
     * @var EducationDTO $Education
     */
    protected $Education = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $MiddleName
     */
    protected $MiddleName = null;

    /**
     * @var date $BirthDate
     */
    protected $BirthDate = null;

    /**
     * @var string $Sex
     */
    protected $Sex = null;

    /**
     * @var int $Married
     */
    protected $Married = null;

    /**
     * @var string $ZachNumber
     */
    protected $ZachNumber = null;

    /**
     * @var int $EduCount
     */
    protected $EduCount = null;

    /**
     * @var boolean $Gpo
     */
    protected $Gpo = null;

    /**
     * @var boolean $Problem
     */
    protected $Problem = null;

    /**
     * @var int $sessionExt
     */
    protected $sessionExt = null;

    /**
     * @var date $endSession
     */
    protected $endSession = null;

    /**
     * @var int $indGrafic
     */
    protected $indGrafic = null;

    /**
     * @var date $beginGrafic
     */
    protected $beginGrafic = null;

    /**
     * @var date $endGrafic
     */
    protected $endGrafic = null;

    /**
     * @var GroupDTO $Group
     */
    protected $Group = null;

    /**
     * @var string $Financing
     */
    protected $Financing = null;

    /**
     * @var StudentStateDTO $StudentState
     */
    protected $StudentState = null;

    /**
     * @var FilialDTO $Filial
     */
    protected $Filial = null;

    /**
     * @var date $activateDate
     */
    protected $activateDate = null;

    /**
     * @param int $Married
     * @param int $EduCount
     * @param boolean $Gpo
     * @param boolean $Problem
     * @param int $sessionExt
     * @param int $indGrafic
     */
    public function __construct($Married, $EduCount, $Gpo, $Problem, $sessionExt, $indGrafic)
    {
      $this->Married = $Married;
      $this->EduCount = $EduCount;
      $this->Gpo = $Gpo;
      $this->Problem = $Problem;
      $this->sessionExt = $sessionExt;
      $this->indGrafic = $indGrafic;
    }

    /**
     * @return string
     */
    public function getPersonId()
    {
      return $this->PersonId;
    }

    /**
     * @param string $PersonId
     * @return StudentDTO
     */
    public function setPersonId($PersonId)
    {
      $this->PersonId = $PersonId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param string $StudyId
     * @return StudentDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return EducationDTO
     */
    public function getEducation()
    {
      return $this->Education;
    }

    /**
     * @param EducationDTO $Education
     * @return StudentDTO
     */
    public function setEducation($Education)
    {
      $this->Education = $Education;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return StudentDTO
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return StudentDTO
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return StudentDTO
     */
    public function setMiddleName($MiddleName)
    {
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return date
     */
    public function getBirthDate()
    {
      return $this->BirthDate;
    }

    /**
     * @param date $BirthDate
     * @return StudentDTO
     */
    public function setBirthDate($BirthDate)
    {
      $this->BirthDate = $BirthDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSex()
    {
      return $this->Sex;
    }

    /**
     * @param string $Sex
     * @return StudentDTO
     */
    public function setSex($Sex)
    {
      $this->Sex = $Sex;
      return $this;
    }

    /**
     * @return int
     */
    public function getMarried()
    {
      return $this->Married;
    }

    /**
     * @param int $Married
     * @return StudentDTO
     */
    public function setMarried($Married)
    {
      $this->Married = $Married;
      return $this;
    }

    /**
     * @return string
     */
    public function getZachNumber()
    {
      return $this->ZachNumber;
    }

    /**
     * @param string $ZachNumber
     * @return StudentDTO
     */
    public function setZachNumber($ZachNumber)
    {
      $this->ZachNumber = $ZachNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getEduCount()
    {
      return $this->EduCount;
    }

    /**
     * @param int $EduCount
     * @return StudentDTO
     */
    public function setEduCount($EduCount)
    {
      $this->EduCount = $EduCount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGpo()
    {
      return $this->Gpo;
    }

    /**
     * @param boolean $Gpo
     * @return StudentDTO
     */
    public function setGpo($Gpo)
    {
      $this->Gpo = $Gpo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getProblem()
    {
      return $this->Problem;
    }

    /**
     * @param boolean $Problem
     * @return StudentDTO
     */
    public function setProblem($Problem)
    {
      $this->Problem = $Problem;
      return $this;
    }

    /**
     * @return int
     */
    public function getSessionExt()
    {
      return $this->sessionExt;
    }

    /**
     * @param int $sessionExt
     * @return StudentDTO
     */
    public function setSessionExt($sessionExt)
    {
      $this->sessionExt = $sessionExt;
      return $this;
    }

    /**
     * @return date
     */
    public function getEndSession()
    {
      return $this->endSession;
    }

    /**
     * @param date $endSession
     * @return StudentDTO
     */
    public function setEndSession($endSession)
    {
      $this->endSession = $endSession;
      return $this;
    }

    /**
     * @return int
     */
    public function getIndGrafic()
    {
      return $this->indGrafic;
    }

    /**
     * @param int $indGrafic
     * @return StudentDTO
     */
    public function setIndGrafic($indGrafic)
    {
      $this->indGrafic = $indGrafic;
      return $this;
    }

    /**
     * @return date
     */
    public function getBeginGrafic()
    {
      return $this->beginGrafic;
    }

    /**
     * @param date $beginGrafic
     * @return StudentDTO
     */
    public function setBeginGrafic($beginGrafic)
    {
      $this->beginGrafic = $beginGrafic;
      return $this;
    }

    /**
     * @return date
     */
    public function getEndGrafic()
    {
      return $this->endGrafic;
    }

    /**
     * @param date $endGrafic
     * @return StudentDTO
     */
    public function setEndGrafic($endGrafic)
    {
      $this->endGrafic = $endGrafic;
      return $this;
    }

    /**
     * @return GroupDTO
     */
    public function getGroup()
    {
      return $this->Group;
    }

    /**
     * @param GroupDTO $Group
     * @return StudentDTO
     */
    public function setGroup($Group)
    {
      $this->Group = $Group;
      return $this;
    }

    /**
     * @return string
     */
    public function getFinancing()
    {
      return $this->Financing;
    }

    /**
     * @param string $Financing
     * @return StudentDTO
     */
    public function setFinancing($Financing)
    {
      $this->Financing = $Financing;
      return $this;
    }

    /**
     * @return StudentStateDTO
     */
    public function getStudentState()
    {
      return $this->StudentState;
    }

    /**
     * @param StudentStateDTO $StudentState
     * @return StudentDTO
     */
    public function setStudentState($StudentState)
    {
      $this->StudentState = $StudentState;
      return $this;
    }

    /**
     * @return FilialDTO
     */
    public function getFilial()
    {
      return $this->Filial;
    }

    /**
     * @param FilialDTO $Filial
     * @return StudentDTO
     */
    public function setFilial($Filial)
    {
      $this->Filial = $Filial;
      return $this;
    }

    /**
     * @return date
     */
    public function getActivateDate()
    {
      return $this->activateDate;
    }

    /**
     * @param date $activateDate
     * @return StudentDTO
     */
    public function setActivateDate($activateDate)
    {
      $this->activateDate = $activateDate;
      return $this;
    }

}
