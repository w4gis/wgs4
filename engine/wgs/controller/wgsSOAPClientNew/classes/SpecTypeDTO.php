<?php

class SpecTypeDTO
{

    /**
     * @var string $SpecialityTypeId
     */
    protected $SpecialityTypeId = null;

    /**
     * @var string $SpecialityType
     */
    protected $SpecialityType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSpecialityTypeId()
    {
      return $this->SpecialityTypeId;
    }

    /**
     * @param string $SpecialityTypeId
     * @return SpecTypeDTO
     */
    public function setSpecialityTypeId($SpecialityTypeId)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityType()
    {
      return $this->SpecialityType;
    }

    /**
     * @param string $SpecialityType
     * @return SpecTypeDTO
     */
    public function setSpecialityType($SpecialityType)
    {
      $this->SpecialityType = $SpecialityType;
      return $this;
    }

}
