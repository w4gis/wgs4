<?php

class GroupDTO
{

    /**
     * @var string $GroupId
     */
    protected $GroupId = null;

    /**
     * @var string $GroupName
     */
    protected $GroupName = null;

    /**
     * @var int $YearForming
     */
    protected $YearForming = null;

    /**
     * @var int $Course
     */
    protected $Course = null;

    /**
     * @var int $Semestre
     */
    protected $Semestre = null;

    /**
     * @var float $YearsCount
     */
    protected $YearsCount = null;

    /**
     * @var EducationDTO $Education
     */
    protected $Education = null;

    /**
     * @param int $YearForming
     * @param int $Course
     * @param int $Semestre
     * @param float $YearsCount
     */
    public function __construct($YearForming, $Course, $Semestre, $YearsCount)
    {
      $this->YearForming = $YearForming;
      $this->Course = $Course;
      $this->Semestre = $Semestre;
      $this->YearsCount = $YearsCount;
    }

    /**
     * @return string
     */
    public function getGroupId()
    {
      return $this->GroupId;
    }

    /**
     * @param string $GroupId
     * @return GroupDTO
     */
    public function setGroupId($GroupId)
    {
      $this->GroupId = $GroupId;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
      return $this->GroupName;
    }

    /**
     * @param string $GroupName
     * @return GroupDTO
     */
    public function setGroupName($GroupName)
    {
      $this->GroupName = $GroupName;
      return $this;
    }

    /**
     * @return int
     */
    public function getYearForming()
    {
      return $this->YearForming;
    }

    /**
     * @param int $YearForming
     * @return GroupDTO
     */
    public function setYearForming($YearForming)
    {
      $this->YearForming = $YearForming;
      return $this;
    }

    /**
     * @return int
     */
    public function getCourse()
    {
      return $this->Course;
    }

    /**
     * @param int $Course
     * @return GroupDTO
     */
    public function setCourse($Course)
    {
      $this->Course = $Course;
      return $this;
    }

    /**
     * @return int
     */
    public function getSemestre()
    {
      return $this->Semestre;
    }

    /**
     * @param int $Semestre
     * @return GroupDTO
     */
    public function setSemestre($Semestre)
    {
      $this->Semestre = $Semestre;
      return $this;
    }

    /**
     * @return float
     */
    public function getYearsCount()
    {
      return $this->YearsCount;
    }

    /**
     * @param float $YearsCount
     * @return GroupDTO
     */
    public function setYearsCount($YearsCount)
    {
      $this->YearsCount = $YearsCount;
      return $this;
    }

    /**
     * @return EducationDTO
     */
    public function getEducation()
    {
      return $this->Education;
    }

    /**
     * @param EducationDTO $Education
     * @return GroupDTO
     */
    public function setEducation($Education)
    {
      $this->Education = $Education;
      return $this;
    }

}
