<?php

class ScholarshipOrderInfoDTOArray
{

    /**
     * @var ScholarshipOrderInfoDTO[] $item
     */
    protected $item = null;

    /**
     * @param ScholarshipOrderInfoDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return ScholarshipOrderInfoDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param ScholarshipOrderInfoDTO[] $item
     * @return ScholarshipOrderInfoDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
