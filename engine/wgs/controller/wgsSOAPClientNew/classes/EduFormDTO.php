<?php

class EduFormDTO
{

    /**
     * @var string $EduFormId
     */
    protected $EduFormId = null;

    /**
     * @var string $EduFormName
     */
    protected $EduFormName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getEduFormId()
    {
      return $this->EduFormId;
    }

    /**
     * @param string $EduFormId
     * @return EduFormDTO
     */
    public function setEduFormId($EduFormId)
    {
      $this->EduFormId = $EduFormId;
      return $this;
    }

    /**
     * @return string
     */
    public function getEduFormName()
    {
      return $this->EduFormName;
    }

    /**
     * @param string $EduFormName
     * @return EduFormDTO
     */
    public function setEduFormName($EduFormName)
    {
      $this->EduFormName = $EduFormName;
      return $this;
    }

}
