<?php

class EduFormDTOArray
{

    /**
     * @var EduFormDTO[] $item
     */
    protected $item = null;

    /**
     * @param EduFormDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return EduFormDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param EduFormDTO[] $item
     * @return EduFormDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
