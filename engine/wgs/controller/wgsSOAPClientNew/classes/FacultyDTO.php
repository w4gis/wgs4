<?php

class FacultyDTO
{

    /**
     * @var string $FacultyId
     */
    protected $FacultyId = null;

    /**
     * @var string $FacultyName
     */
    protected $FacultyName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getFacultyId()
    {
      return $this->FacultyId;
    }

    /**
     * @param string $FacultyId
     * @return FacultyDTO
     */
    public function setFacultyId($FacultyId)
    {
      $this->FacultyId = $FacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFacultyName()
    {
      return $this->FacultyName;
    }

    /**
     * @param string $FacultyName
     * @return FacultyDTO
     */
    public function setFacultyName($FacultyName)
    {
      $this->FacultyName = $FacultyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return FacultyDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
