<?php

class LocmanInfoService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'EduFormDTO' => '\\EduFormDTO',
  'SubFacultyDTO' => '\\SubFacultyDTO',
  'FacultyDTO' => '\\FacultyDTO',
  'SpecializationDTO' => '\\SpecializationDTO',
  'SpecialityDTO' => '\\SpecialityDTO',
  'SpecTypeDTO' => '\\SpecTypeDTO',
  'EducationDTO' => '\\EducationDTO',
  'GroupDTO' => '\\GroupDTO',
  'StudentStateDTO' => '\\StudentStateDTO',
  'FilialDTO' => '\\FilialDTO',
  'StudentDTO' => '\\StudentDTO',
  'StudentRequestDTO' => '\\StudentRequestDTO',
  'StudentDTOArray' => '\\StudentDTOArray',
  'GroupDTOArray' => '\\GroupDTOArray',
  'FacultyDTOArray' => '\\FacultyDTOArray',
  'SubFacultyDTOArray' => '\\SubFacultyDTOArray',
  'EduFormDTOArray' => '\\EduFormDTOArray',
  'SpecialityDTOArray' => '\\SpecialityDTOArray',
  'HostelDTO' => '\\HostelDTO',
  'HostelDTOArray' => '\\HostelDTOArray',
  'HostelStudentDTO' => '\\HostelStudentDTO',
  'HostelStudentDTOArray' => '\\HostelStudentDTOArray',
  'OrderTypeDTO' => '\\OrderTypeDTO',
  'OrderTypeDTOArray' => '\\OrderTypeDTOArray',
  'PrivelegeDTO' => '\\PrivelegeDTO',
  'PrivelegeDTOArray' => '\\PrivelegeDTOArray',
  'SessionTypeDTO' => '\\SessionTypeDTO',
  'SessionEndTypeDTO' => '\\SessionEndTypeDTO',
  'SubDivisionDTO' => '\\SubDivisionDTO',
  'OrderHeaderDTO' => '\\OrderHeaderDTO',
  'ScholarshipOrderInfoDTO' => '\\ScholarshipOrderInfoDTO',
  'ScholarshipOrderInfoDTOArray' => '\\ScholarshipOrderInfoDTOArray',
  'OrderHeaderDTOArray' => '\\OrderHeaderDTOArray',
  'AddressLevelDTO' => '\\AddressLevelDTO',
  'AddressPrefixDTO' => '\\AddressPrefixDTO',
  'AddressDTO' => '\\AddressDTO',
  'AddressTypeDTO' => '\\AddressTypeDTO',
  'StudentAddressDTO' => '\\StudentAddressDTO',
  'StudentAddressDTOArray' => '\\StudentAddressDTOArray',
  'xsd_stringArray' => '\\xsd_stringArray',
  'DocIdentDTO' => '\\DocIdentDTO',
  'DocIdentDTOArray' => '\\DocIdentDTOArray',
  'StudentStateDTOArray' => '\\StudentStateDTOArray',
  'CitizenshipDTO' => '\\CitizenshipDTO',
  'CitizenshipDTOArray' => '\\CitizenshipDTOArray',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = 'http://ls.2i.tusur.ru/Productive/services/lws.nsf/LocmanInfo?wsdl')
    {

  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
        'features' => 1,
        'login'  => "CUser",
        'password' => "010cod"
      ), $options);

      set_error_handler(array(&$this, 'my_error_handler'));
      set_exception_handler(array(&$this, 'my_exception_handler'));

      try {
        parent::__construct($wsdl, $options);
      }

      catch (Exception $fault) {
        throw new Exception();
        //exit();
      }

      restore_error_handler();
      restore_exception_handler();
    }

    function my_exception_handler($e) {
        throw new Exception();
        //exit();
    }

    function my_error_handler($no,$str,$file,$line) {
        $e = new ErrorException($str,$no,0,$file,$line);
        $this->my_exception_handler($e);
    }

    /**
     * @param string $logon
     * @param string $password
     * @return void
     */
    public function LogOn($logon, $password)
    {
      return $this->__soapCall('LogOn', array($logon, $password));
    }

    /**
     * @return boolean
     */
    public function isLogin()
    {
      return $this->__soapCall('isLogin', array());
    }

    /**
     * @param string $MetodName
     * @return boolean
     */
    public function CanUse($MetodName)
    {
      return $this->__soapCall('CanUse', array($MetodName));
    }

    /**
     * @param string $ID
     * @return StudentDTO
     */
    public function GetStudentById($ID)
    {
      return $this->__soapCall('GetStudentById', array($ID));
    }

    /**
     * @param StudentRequestDTO $studentCriteria
     * @return StudentDTOArray
     */
    public function GetStudentsByCriteria(StudentRequestDTO $studentCriteria)
    {
      return $this->__soapCall('GetStudentsByCriteria', array($studentCriteria));
    }

    /**
     * @return GroupDTOArray
     */
    public function GetAllGroups()
    {
      return $this->__soapCall('GetAllGroups', array());
    }

    /**
     * @param string $username
     * @return GroupDTOArray
     */
    public function GetAllGroupsByUser($username)
    {
      return $this->__soapCall('GetAllGroupsByUser', array($username));
    }

    /**
     * @return string
     */
    public function GetAllActiveGroupsTest()
    {
      return $this->__soapCall('GetAllActiveGroupsTest', array());
    }

    /**
     * @return GroupDTOArray
     */
    public function GetAllActiveGroups()
    {
      return $this->__soapCall('GetAllActiveGroups', array());
    }

    /**
     * @return FacultyDTOArray
     */
    public function GetActiveFacultys()
    {
      return $this->__soapCall('GetActiveFacultys', array());
    }

    /**
     * @return SubFacultyDTOArray
     */
    public function GetActiveSubFacultys()
    {
      return $this->__soapCall('GetActiveSubFacultys', array());
    }

    /**
     * @return EduFormDTOArray
     */
    public function GetEduForms()
    {
      return $this->__soapCall('GetEduForms', array());
    }

    /**
     * @return SpecialityDTOArray
     */
    public function GetAllSpeciality()
    {
      return $this->__soapCall('GetAllSpeciality', array());
    }

    /**
     * @return SpecialityDTOArray
     */
    public function GetActiveSpeciality()
    {
      return $this->__soapCall('GetActiveSpeciality', array());
    }

    /**
     * @return HostelDTOArray
     */
    public function GetHostels()
    {
      return $this->__soapCall('GetHostels', array());
    }

    /**
     * @param string $studentID
     * @return HostelStudentDTOArray
     */
    public function GetStudentHostels($studentID)
    {
      return $this->__soapCall('GetStudentHostels', array($studentID));
    }

    /**
     * @return OrderTypeDTOArray
     */
    public function GetOrderTypes()
    {
      return $this->__soapCall('GetOrderTypes', array());
    }

    /**
     * @return PrivelegeDTOArray
     */
    public function GetAllPriveleges()
    {
      return $this->__soapCall('GetAllPriveleges', array());
    }

    /**
     * @param string $privelegePersonalId
     * @return PrivelegeDTOArray
     */
    public function GetAllPrivelegesForPersona($privelegePersonalId)
    {
      return $this->__soapCall('GetAllPrivelegesForPersona', array($privelegePersonalId));
    }

    /**
     * @param string $student_id
     * @return ScholarshipOrderInfoDTOArray
     */
    public function GetScholarshipOrderInfoByCriteria($student_id)
    {
      return $this->__soapCall('GetScholarshipOrderInfoByCriteria', array($student_id));
    }

    /**
     * @param string $numb
     * @param string $date
     * @return ScholarshipOrderInfoDTOArray
     */
    public function GetScholarshipOrderInfoByNumbAndDate($numb, $date)
    {
      return $this->__soapCall('GetScholarshipOrderInfoByNumbAndDate', array($numb, $date));
    }

    /**
     * @param string $date
     * @return ScholarshipOrderInfoDTOArray
     */
    public function GetScholarshipOrderInfoByCreateDate($date)
    {
      return $this->__soapCall('GetScholarshipOrderInfoByCreateDate', array($date));
    }

    /**
     * @param string $orderHeaderId
     * @return ScholarshipOrderInfoDTOArray
     */
    public function GetScholarshipOrderByOrderHeaderId($orderHeaderId)
    {
      return $this->__soapCall('GetScholarshipOrderByOrderHeaderId', array($orderHeaderId));
    }

    /**
     * @param string $date
     * @param string $internalNumb
     * @return OrderHeaderDTOArray
     */
    public function GetAllOrderHeaderGreaterDateCreate($date, $internalNumb)
    {
      return $this->__soapCall('GetAllOrderHeaderGreaterDateCreate', array($date, $internalNumb));
    }

    /**
     * @param string $studentId
     * @return StudentAddressDTOArray
     */
    public function getAddressByStudentId($studentId)
    {
      return $this->__soapCall('getAddressByStudentId', array($studentId));
    }

    /**
     * @param string $orderHeaderId
     * @param string $order_date
     * @param string $numb
     * @return boolean
     */
    public function FinishScholarshipOrderByOrderHeaderId($orderHeaderId, $order_date, $numb)
    {
      return $this->__soapCall('FinishScholarshipOrderByOrderHeaderId', array($orderHeaderId, $order_date, $numb));
    }

    /**
     * @param string $groupID
     * @return xsd_stringArray
     */
    public function GetStudentIDsByGroupID($groupID)
    {
      return $this->__soapCall('GetStudentIDsByGroupID', array($groupID));
    }

    /**
     * @param string $studentID
     * @return DocIdentDTOArray
     */
    public function getDocIdentOsnByStudyId($studentID)
    {
      return $this->__soapCall('getDocIdentOsnByStudyId', array($studentID));
    }

    /**
     * @return StudentStateDTOArray
     */
    public function GetAllStudentStates()
    {
      return $this->__soapCall('GetAllStudentStates', array());
    }

    /**
     * @return CitizenshipDTOArray
     */
    public function GetCitizenshipList()
    {
      return $this->__soapCall('GetCitizenshipList', array());
    }

}
