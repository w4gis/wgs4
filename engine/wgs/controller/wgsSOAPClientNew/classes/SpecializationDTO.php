<?php

class SpecializationDTO
{

    /**
     * @var string $SpecializationId
     */
    protected $SpecializationId = null;

    /**
     * @var string $SpecializationName
     */
    protected $SpecializationName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSpecializationId()
    {
      return $this->SpecializationId;
    }

    /**
     * @param string $SpecializationId
     * @return SpecializationDTO
     */
    public function setSpecializationId($SpecializationId)
    {
      $this->SpecializationId = $SpecializationId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecializationName()
    {
      return $this->SpecializationName;
    }

    /**
     * @param string $SpecializationName
     * @return SpecializationDTO
     */
    public function setSpecializationName($SpecializationName)
    {
      $this->SpecializationName = $SpecializationName;
      return $this;
    }

}
