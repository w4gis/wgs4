<?php

class SubFacultyDTO
{

    /**
     * @var string $SubFacultyId
     */
    protected $SubFacultyId = null;

    /**
     * @var string $SubFacultyName
     */
    protected $SubFacultyName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSubFacultyId()
    {
      return $this->SubFacultyId;
    }

    /**
     * @param string $SubFacultyId
     * @return SubFacultyDTO
     */
    public function setSubFacultyId($SubFacultyId)
    {
      $this->SubFacultyId = $SubFacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubFacultyName()
    {
      return $this->SubFacultyName;
    }

    /**
     * @param string $SubFacultyName
     * @return SubFacultyDTO
     */
    public function setSubFacultyName($SubFacultyName)
    {
      $this->SubFacultyName = $SubFacultyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return SubFacultyDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
