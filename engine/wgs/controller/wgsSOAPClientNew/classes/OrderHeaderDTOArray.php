<?php

class OrderHeaderDTOArray
{

    /**
     * @var OrderHeaderDTO[] $item
     */
    protected $item = null;

    /**
     * @param OrderHeaderDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return OrderHeaderDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param OrderHeaderDTO[] $item
     * @return OrderHeaderDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
