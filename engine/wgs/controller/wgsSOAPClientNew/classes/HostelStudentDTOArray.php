<?php

class HostelStudentDTOArray
{

    /**
     * @var HostelStudentDTO[] $item
     */
    protected $item = null;

    /**
     * @param HostelStudentDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return HostelStudentDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param HostelStudentDTO[] $item
     * @return HostelStudentDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
