<?php

class StudentAddressDTOArray
{

    /**
     * @var StudentAddressDTO[] $item
     */
    protected $item = null;

    /**
     * @param StudentAddressDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return StudentAddressDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param StudentAddressDTO[] $item
     * @return StudentAddressDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
