﻿<?php
/**
 * /ConfigContingentArrays.class.php
 */

class ConfigContingentArrays {

	private $facultyDTOArray = array("FacultyId"		 => "getFacultyId",
							   	     "FacultyName"  	 => "getFacultyName",
							   	     "FacultyShortName"  => "getShortName"
							   	     );

	private $subFacultyDTOArray = array("SubFacultyId"	  => "getSubFacultyId",
							   	    	"SubFacultyName"  => "getSubFacultyName",
							   	    	"ShortName" 	  => "getShortName"
							   	   		);

	private $groupDTOArray = array("GroupId" 	  => "getGroupId",
								   "GroupName"    => "getGroupName",
								   "YearForming"  => "getYearForming",
								   "Course" 	  => "getCourse",
								   "Semestre" 	  => "getSemestre",
								   "Education"    => "getEducation" 	//EducationDTO
								   );

	private $eduFormsDTOArray = array("EduFormId"	 => "getEduFormId",
							   	  	  "EduFormName"  => "getEduFormName"
							   	 	  );

	private $specialityDTOArray = array("SpecialityId" 	 	 => "getSpecialityId",
							   	 		"SpecialityCode" 	 => "getSpecialityCode",
							   	 		"SpecialityName" 	 => "getSpecialityName",
							   	 		"SpecialityTypeName" => "getSpecialityTypeName"
							   	 		);

	private $studentStateDTOArray = array("Id" 	 => "getId",
							   	 	  	  "Name" => "getName"
							   		  	  );

	private $studentDTOArray = array("PersonId" 	 => "getPersonId",
							   		 "StudyId"  	 => "getStudyId",
							   		 "CitizenshipId" => "getCitizenshipId",
							   	 	 "Education"  	 => "getEducation", 	//EducationDTO
							   		 "LastName"  	 => "getLastName",
							   		 "FirstName"  	 => "getFirstName",
							   		 "MiddleName"  	 => "getMiddleName",
							   		 "BirthDate"  	 => "getBirthDate",
							   		 "Sex"  		 => "getSex",
							   		 "Married"  	 => "getMarried",
							   		 "ZachNumber"  	 => "getZachNumber",
							   		 "EduCount"  	 => "getEduCount",
							   		 "Gpo"  		 => "getGpo",
							   		 "Problem"  	 => "getProblem",
							   		 "Privelege"  	 => "getPrivelege",
							   		 "sessionExt"  	 => "getSessionExt",
							   		 "endSession"  	 => "getEndSession",
							   		 "indGrafic"  	 => "getIndGrafic",
							   		 "beginGrafic"   => "getBeginGrafic",
							   		 "endGrafic"  	 => "getEndGrafic",
							   		 "Group"  		 => "getGroup",			//GroupDTO
							   		 "Financing"  	 => "getFinancing",
							   		 "StudentState"  => "getStudentState", 	//StudentStateDTO
							   		 "Filial"  		 => "getFilial",		//FilialDTO 
							   		 "Citizenship"	 => "getCitizenship"	//CitizenshipDTO
							   		 );

	private $citizenshipDTOArray = array("CitizenshipId"	=> "getCitizenshipId",
							   			 "Name"  			=> "getName",
							   			 "Code"				=> "getCode"
							   			);

	private $hostelDTOArray = array("HostelId" 	=> "getHostelId",
							   		"Name"  	=> "getName"
							   		);

	private $educationDTOArray = array("EduId" 			=> "getEduId",
									   "EduForm" 		=> "getEduForm", 		//EduFormDTO
									   "SubFaculty" 	=> "getSubFaculty", 	//SubFacultyDTO
									   "Faculty" 		=> "getFaculty", 		//FacultyDTO
									   "Specialization" => "getSpecialization", //SpecialityDTO
									   "Speciality" 	=> "getSpeciality", 	//SpecialityDTO
									   "SpecType" 		=> "getSpecType", 		//SpecTypeDTO
									   "IsActive" 		=> "getIsActive"
									   );

	private $filialDTOArray = array("FilialId"	  => "getFilialId",
							   	    "FilialName"  => "getFilialName",
							   	    "ShortName"   => "getShortName"
							   	   	);

	private $grantDTOArray = array("id" 		=> "getId",
								   "studentId" 	=> "getStudentId",
								   "typeId" 	=> "getTypeId", 
								   "dateBegin" 	=> "getDateBegin", 	
								   "dateEnd" 	=> "getDateEnd", 
								   "dateNow" 	=> "getDateNow", 
								   "protNumb" 	=> "getProtNumb"
								   );

	private $studentRoomDTOArray = array("StudentId" 	   => "getStudentId",
								   		 "StudentName" 	   => "getStudentName",
								   		 "HostelId" 	   => "getHostelId", 
								   		 "RoomNumberPart1" => "getRoomNumberPart1", 	
								   		 "RoomNumberPart2" => "getRoomNumberPart2", 
								   		 "RoomNumberPart3" => "getRoomNumberPart3", 
								   		 "FacultyId" 	   => "getFacultyId"
								   		 );

	private $subDivisionDTOArray = array("Id"   => "getId",
								    	 "Name" => "getName"
								    	 );

	private $addressDTOArray = array("id" 			 => "getId",
								   	 "value" 		 => "getValue", 
								   	 "shortName" 	 => "getShortName", 	
								   	 "AddressLevel"  => "getAddressLevel",  //AddressLevelDTO
								   	 "AddressPrefix" => "getAddressPrefix", //AddressPrefixDTO
								   	 "AddressParent" => "getAddressParent"  //AddressDTO
								   	 );

	private $addressLevelDTOArray = array("id"   => "getId",
								    	  "name" => "getName"
								    	  );

	private $addressPrefixDTOArray = array("id"   => "getId",
								      	   "prefix" => "getPrefix"
								     	   );

	private $studentAddressDTOArray = array("id" 			=> "getId",
								   	 		"postString" 	=> "getPostString", 
								   	 		"addressIndex" => "getAddressIndex", 	
								   	 		"defaultItem"  => "getDefaultItem",  
								   	 		"Address" 		=> "getAddress", 	 //AddressDTO
								   	 		"AddressType"  => "getAddressType"   //AddressTypeDTO
								   	 		);

	private $addressTypeDTOArray = array("id"   => "getId",
								    	 "name" => "getName"
								    	 );

	private $hostelStudentDTOArray = array("StudentHosteId" => "getStudentHosteId",
								   	 	   "Hostel" 		=> "getHostel", 		//HostelDTO
								   	 	   "RoomNumber" 	=> "getRoomNumber", 	
								   	 	   "StartDate"  	=> "getStartDate",  
								   	 	   "EndDate" 		=> "getEndDate", 	 
								   	 	   "StudyId"  		=> "getStudyId"  
								   		   );

	private $infoTypeDTOArray = array("Id"   => "getId",
								      "Name" => "getName"
								      );

	private $nextCourseDTOArray = array("id"   		=> "getId",
								      	"studentId" => "getStudentId",
								      	"flagDebt"  => "getFlagDebt"
								      	);

	private $orderDTOArray = array("StudentOrderId" => "getStudentOrderId",
								   "StudyId" 		=> "getStudyId", 		
								   "InfoTypeDTO" 	=> "getInfoType", 		//InfoTypeDTO 
								   "OrderDTO"  		=> "getOrder", 			//OrderDTO
								   "OrderTypeDTO" 	=> "getOrderType", 		//OrderTypeDTO
								   "OrderHeaderDTO" => "getOrderHeader"		//OrderHeaderDTO
								   );

	private $orderTypeDTOArray = array("Id"   => "getId",
								       "Name" => "getName"
								       );

	private $orderHeaderDTOArray = array("Id" 			=> "getId",
								    	 "Number" 		=> "getNumber", 		
								    	 "OrderDate" 	=> "getOrderDate",
								    	 "CreateDate"  	=> "getCreateDate",
								    	 "HandleDate" 	=> "getHandleDate",
								     	 "SubDivision"	=> "getSubDivision"		//SubDivisionDTO
								    	 );

	private $personalPrivelegeDTOArray = array("Privelege" => "getPrivelege",	//PrivelegeDTO
								   	 	  	   "PersonaId" => "getPersonaId", 		
								   	 	  	   "BeginDate" => "getBeginDate", 	 
								   	 	  	   "EndDate"   => "getEndDate"  
								   		 	   );

	private $privelegeDTOArray = array("Id"   => "getId",
								       "Name" => "getName"
								       );

	private $scholarshipOrderInfoDTOArray = array("reasorId" 	 		=> "getReasorId",
										   		  "reasorName"  	 	=> "getReasorName",
										   		  "Id" 					=> "getId",
										   	 	  "OrderType"  	 		=> "getOrderType", 			//OrderTypeDTO
										   		  "Summa"  	 			=> "getSumma",
										   		  "BeginDate"  	 		=> "getBeginDate",
										   		  "EndDate"  	 		=> "getEndDate",
										   		  "Privelege"  	 		=> "getPrivelege", 			//PrivelegeDTO
										   		  "SessionType"  	 	=> "getSessionType", 		//SessionTypeDTO
										   		  "SessionEndType"  	=> "getSessionEndType", 	//SessionEndTypeDTO
										   		  "ScholarOrderNumber"	=> "getScholarOrderNumber",
										   		  "ScholarOrderDate"	=> "getScholarOrderDate",
										   		  "Fund"  	 			=> "getFund",
										   		  "OrderHeader"  		=> "getOrderHeader", 		//OrderHeaderDTO
										   		  "StudyId"  	 		=> "getStudyId"
										   		  );

	private $sessionTypeDTOArray = array("Id"   => "getId",
								       	 "Name" => "getName"
								         );

	private $sessionEndTypeDTOArray = array("Id"   => "getId",
								       	 	"Name" => "getName"
								         	);

	private $studentInfoForEduDTOArray = array("studentAddress"	=> "getStudentAddress", 	//ArrayOfStudentAddressDTO
								       	 	   "student" 		=> "getStudent"				//StudentDTO
								         	   );

	private $specializationDTOArray = array("SpecializationId"   => "getId",
								       	 	"SpecializationName" => "getName"
								         	);

	private $hostelOrderInfoDTOArray = array("prot" 	 		=> "getProt",
										   	 "id"  	 			=> "getId",
										   	 "studyId" 			=> "getStudyId",
										   	 "studyBaseName"  	=> "getStudyBaseName", 			
										   	 "studyFullName"  	=> "getStudyFullName",
										   	 "studyName"  	 	=> "getStudyName",
										   	 "studyOldName"  	=> "getStudyOldName",
										   	 "studyGroopId"  	=> "getStudyGroopId", 			
										   	 "studyGroopName"	=> "getStudyGroopName", 		
										   	 "financing"  		=> "getFinancing", 	
										   	 "hostelId"			=> "getHostelId",
										   	 "hostelName"		=> "getHostelName",
										   	 "beginDate"  	 	=> "getBeginDate",
										   	 "endDate"  	 	=> "getEndDate"
										   	 );

	private $studentRequestDTOArray = array("PersonId" 	 	=> "getPersonId",
							   		 	 "StudyId"  	 	=> "getStudyId",
							   		 	 "GroupId" 		 	=> "getGroupId",
							   	 	 	 "CitizenshipId" 	=> "getCitizenshipId", 	
							   		 	 "LastName"  	 	=> "getLastName",
							   		 	 "FirstName"  	 	=> "getFirstName",
							   		 	 "MiddleName"  	 	=> "getMiddleName",
							   		 	 "ZachNumber"  	 	=> "getZachNumber",
							   		 	 "GroupName"  	 	=> "getGroupName",
							   			 "EduId"  	 	 	=> "getEduId",
							   			 "EduFormId"  	 	=> "getEduFormId",
							   			 "SubFacultyId"  	=> "getSubFacultyId",
							   			 "FacultyId"  		=> "getFacultyId",
							   			 "SpecialityId" 	=> "getSpecialityId",
							   			 "SpecializationId"	=> "getSpecializationId",
							   			 "SpecialityTypeId" => "getSpecialityTypeId",
							   			 "StudentStateId"  	=> "getStudentStateId", 	
							   			 "CourseGroup"  	=> "getCourseGroup"	
							   			 );

	private $specTypeDTOArray = array("SpecialityTypeId" => "getSpecialityTypeId",
								  	  "SpecialityType" 	 => "getSpecialityType"
								 	  );

	private $docIdentDTOArray = array("study_id" 			=> "getStudy_id",
								  	  "identify_id" 		=> "getIdentify_id",
									  "student_id" 			=> "getStudent_id",
									  "doc_ser" 			=> "getDoc_ser",
								  	  "doc_num" 			=> "getDoc_num",
									  "doc_from" 			=> "getDoc_from",
									  "doc_to" 				=> "getDoc_to",
								  	  "doc_who" 			=> "getDoc_who",
									  "doc_birthplace" 		=> "getDoc_birthplace",
									  "first_order_date"	=> "getFirst_order_date",
									  "first_order_num" 	=> "getFirst_order_num"
								 	  );								  
									
	public function getFacultyDTOArray() {
		return $this->facultyDTOArray;
	}

	public function getSubFacultyDTOArray() {
		return $this->subFacultyDTOArray;
	}

	public function getGroupDTOArray() {
		return $this->groupDTOArray;
	}

	public function getEduFormsDTOArray() {
		return $this->eduFormsDTOArray;
	}
	
	public function getSpecialityDTOArray() {
		return $this->specialityDTOArray;
	}

	public function getStudentStateDTOArray() {
		return $this->studentStateDTOArray;
	}

	public function getStudentDTOArray() {
		return $this->studentDTOArray;
	}

	public function getCitizenshipDTOArray() {
		return $this->citizenshipDTOArray;
	}

	public function getHostelDTOArray() {
		return $this->hostelDTOArray;
	}

	public function getEducationDTOArray() {
		return $this->educationDTOArray;
	}

	public function getFilialDTOArray() {
		return $this->filialDTOArray;
	}

	public function getGrantDTOArray() {
		return $this->grantDTOArray;
	}

	public function getStudentRoomDTOArray() {
		return $this->studentRoomDTOArray;
	}

	public function getSubDivisionDTOArray() {
		return $this->subDivisionDTOArray;
	}

	public function getAddressDTOArray() {
		return $this->addressDTOArray;
	}

	public function getAddressLevelDTOArray() {
		return $this->addressLevelDTOArray;
	}

	public function getAddressPrefixDTOArray() {
		return $this->addressPrefixDTOArray;
	}

	public function getStudentAddressDTOArray() {
		return $this->studentAddressDTOArray;
	}

	public function getAddressTypeDTOArray() {
		return $this->addressTypeDTOArray;
	}

	public function getHostelStudentDTOArray() {
		return $this->hostelStudentDTOArray;
	}

	public function getInfoTypeDTOArray() {
		return $this->infoTypeDTOArray;
	}

	public function getNextCourseDTOArray() {
		return $this->nextCourseDTOArray;
	}

	public function getOrderDTOArray() {
		return $this->orderDTOArray;
	}

	public function getOrderTypeDTOArray() {
		return $this->orderTypeDTOArray;
	}

	public function getOrderHeaderDTOArray() {
		return $this->orderHeaderDTOArray;
	}

	public function getPersonalPrivelegeDTOArray() {
		return $this->personalPrivelegeDTOArray;
	}

	public function getPrivelegeDTOArray() {
		return $this->privelegeDTOArray;
	}

	public function getScholarshipOrderInfoDTOArray() {
		return $this->scholarshipOrderInfoDTOArray;
	}

	public function getSessionTypeDTOArray() {
		return $this->sessionTypeDTOArray;
	}

	public function getSessionEndTypeDTOArray() {
		return $this->sessionEndTypeDTOArray;
	}

	public function getStudentInfoForEduDTOArray() {
		return $this->studentInfoForEduDTOArray;
	}

	public function getSpecializationDTOArray() {
		return $this->specializationDTOArray;
	}

	public function getHostelOrderInfoDTOArray() {
		return $this->hostelOrderInfoDTOArray;
	}

	public function getstudentRequestDTOArray() {
		return $this->studentRequestDTOArray;
	}

	public function getSpecTypeDTOArray() {
		return $this->specTypeDTOArray;
	}
	
	public function getDocIdentDTOArray() {
		return $this->docIdentDTOArray;
	}
}

?>