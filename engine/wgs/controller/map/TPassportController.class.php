<?php
lmb_require('limb/web_app/src/controller/lmbController.class.php');
lmb_require('wgs/lib/wgsTPassport.class.php');

class TPassportController extends lmbController {
	/*
			
			//******************
			
			//************
			//��������� �����
			
			$planes_list = $obj_passport->get_floorplan_names($passport_id);
			
			if ($planes_list)
				$tpl_passport->floor_planes->PARSE($planes_list);
			else
				$tpl_passport->floor_planes_not_exists->PARSE();
			
			//************
			
			
			$obj_passport->disconnect();

			//$temp = "map_pasport_".$_GET["type"].".i.html";
		}
		else
		{
			$tpl_passport = new Page2("map_pasport_null.i.html");
		}
		$tpl_passport->print_all();
	}
	 * 
	 * */
	
	
	
	function doDisplay() {
 
		$macro = new lmbMacroTemplate('tpassport.phtml');
	 
    	//$objectId = 222534;
    	$objectId = 222383;                      
	    $tpassport = new wgsTPassport();                      
		$tpassport->connect();
	    
		//**********
		//1 ��������
		//���������
		//*********
		$title_list_array = $tpassport->get_title_list($objectId);
		$passport_id = $title_list_array['PASSPORT_ID'];
				
		foreach ($title_list_array as $k => $v) {
			$macro->set("{$k}", $v);
		}
		
		//**********
		//2 ��������
		//����� ��������
		//**************
		$general_info = $tpassport->get_general_info($passport_id);
		$macro->set("general_info", $general_info);
		
		
		//**********
		//3 ��������
		//���������������� �����
		//**********************
		$macro->set("base", $tpassport->get_base_ppart($passport_id));
		$macro->set("pillar", $tpassport->get_pillar_ppart($passport_id));
		$macro->set("crane", $tpassport->get_crane_girder_ppart($passport_id));
		$macro->set("wall", $tpassport->get_wall_ppart($passport_id));
		$macro->set("peregorodki", $tpassport->get_peregorodki_ppart($passport_id));
		$macro->set("nes_konstr_perekr", $tpassport->get_nes_konstrukt_perekr_ppart($passport_id));
		$macro->set("nes_konstr_krovli", $tpassport->get_nes_konstrukt_krovli_ppart($passport_id));
		$macro->set("yteplit", $tpassport->get_yteplit_ppart($passport_id));
		$macro->set("krovlya", $tpassport->get_krovlya_ppart($passport_id));

		//*****************
		//��������_������� �����
		//**********************
		$macro->set("base_sb", $tpassport->get_base_sbpart($passport_id));
		$macro->set("nes_karkas", $tpassport->get_nes_karkas_sbpart($passport_id));
		$macro->set("wall_sb", $tpassport->get_wall_sbpart($passport_id));
		$macro->set("peregorodki_sb", $tpassport->get_peregorodki_sbpart($passport_id));
		$macro->set("mej_store", $tpassport->get_mej_store_perekr_sbpart($passport_id));
		$macro->set("cherdak_perekr", $tpassport->get_cherdak_perekr_sbpart($passport_id));
		$macro->set("lestniza", $tpassport->get_lestniza_sbpart($passport_id));
		$macro->set("nes_elem_krovli", $tpassport->get_nes_elem_krovli_sbpart($passport_id));
		$macro->set("krovlya_sb", $tpassport->get_krovlya_sbpart($passport_id));

		//**********
		//4 ��������
		//������� ���������
		//*****************
		$area_room = $tpassport->get_area_of_rooms($passport_id);
		
		foreach ($area_room as $k => $v) {
			$macro->set("{$k}", $v);
		}
		
		//������ ���������
		$macro->set("area_other_room", $tpassport->get_area_of_other_rooms($passport_id));
		
		//**********
		//5 ��������
		//������� �����
		//*************
		$macro->set("area_of_floor", $tpassport->get_area_of_floor($passport_id));

		//**********
		//6 ��������
		//������� �������� ������������
		//*****************************
		$size_of_nar_pov = $tpassport->get_razmeri_naryj_poverhn($passport_id);

		foreach ($size_of_nar_pov as $k => $v) {
			$macro->set("{$k}", $v);
		}
		
		//�����������
		$macro->set("size_of_glass_1", $tpassport->get_razmeri_stekol_1($passport_id));
		$macro->set("size_of_glass_2", $tpassport->get_razmeri_stekol_2($passport_id));
		$macro->set("roof", $tpassport->get_roofing_value($passport_id));
		
		//**********
		//7 ��������
		//������� ���������� ������������
		//*******************************
		$size_of_vn_pov = $tpassport->get_razmeri_vnytr_poverh($passport_id);
		
		foreach ($size_of_vn_pov as $k => $v) {
			$macro->set("{$k}", $v);
		}
		
		//C����������
		$macro->set("is_ceiling", $tpassport->get_is_ceiling($passport_id));
		$macro->set("is_wall", $tpassport->get_is_wall($passport_id));
		$macro->set("is_septa", $tpassport->get_is_septa($passport_id));
		$macro->set("is_pillar", $tpassport->get_is_pillar($passport_id));
		$macro->set("is_crane_girder", $tpassport->get_is_crane_girder($passport_id));
		$macro->set("is_other", $tpassport->get_is_other($passport_id));
		
		//************
		//8,9 ��������
		//�������� � ����������
		//*********************
		$note = $tpassport->get_note($passport_id);

		foreach ($note as $k => $v) {
			$macro->set("{$k}", $v);
		}
		
		//************
		//��������� �����
		//***************
			
		$planes_list = $tpassport->get_floorplan_names($passport_id);

		if (count($planes_list)) {
			$macro->set("floor_planes", $planes_list);
			$macro->set("floor_planes_not_exists", array());
		}
		else {
			$macro->set("floor_planes", array());
			$macro->set("floor_planes_not_exists", array(''));
		}
		
		echo $macro->render();
		
		$tpassport->disconnect();
	}
	
	function doGetFloorplan() {

		$fId =  $this->request->getRequest('plan_id');
		$tpassport = new wgsTPassport();
		$tpassport->connect();
		global $TMP_ROOT;
		$result = $tpassport->get_floorplan_picture($fId);
	
		$tpassport->disconnect();
		
  		if ($result)
  		{
  			$name = md5(rand());
  			$sTempImageFileName = WGS3_DOCCACHE_DIR.$name.".tiff";
  			file_put_contents($sTempImageFileName, $result);
  			
  			$image_param = getimagesize($sTempImageFileName);
  			$pdf = pdf_new();

  			pdf_open_file($pdf, null);
			pdf_begin_page($pdf, $image_param[0], $image_param[1]);
			$image = pdf_open_image_file($pdf,'tiff',$sTempImageFileName, null, null);
			
			pdf_place_image($pdf,$image,0,0,1.0);
			pdf_end_page($pdf);
			pdf_close($pdf);
			
			$data = pdf_get_buffer($pdf);

			header("Content-type: application/pdf");
			header("Content-disposition: inline; filename=" . basename($sTempImageFileName));
			header("Content-length: " . strlen($data));
  
			echo $data;
			unlink($sTempImageFileName);
		}
	}
}