<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
require_once('Spreadsheet/Excel/Writer.php');

class CardsJournalController extends wgsFrontController
{

	const REPAIR = 249;
	const CRASH  = 248;

	function printExcel($header, $rows, $title) {

        $seq = rand();
        $fileName = "report-$seq.xls";
        $xls =& new Spreadsheet_Excel_Writer(WGS3_PRINTCACHE_DIR.$fileName);

            // �������� �����
        $cart =& $xls->addWorksheet('report');

        $titleText = $propertyName;
            // �������� ������� ��������������
        $titleFormat =& $xls->addFormat();
            // ����������� ������ - Helvetica �������� � OpenOffice calc ����...
        $titleFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
        $titleFormat->setBold();
            // ����������� ������� ������
        $titleFormat->setSize('11');
            // ����������� ����� ������
        $titleFormat->setColor('navy');
            // ����������� ������ ������� ��������� � "thick"
        $titleFormat->setBottom(2);
            // ����������� ����� ������� ���������
        $titleFormat->setBottomColor('navy');
            // ����������� ������������ � ����������� ��������
        $titleFormat->setAlign('merge');
        $titleFormat->setVAlign('vcenter');
            // ���������� ��������� � ���x��� ����� ������ ����� ,
            // ��������� ��� ������ ��������� � ����� ������ ��������������

            // ����������� ���������� ��������������
            $colHeadingFormat =& $xls->addFormat();
            //$colHeadingFormat->setBold();
            $colHeadingFormat->setFontFamily('Helvetica');
            //$colHeadingFormat->setBold();
            $colHeadingFormat->setSize('10');
            $colHeadingFormat->setAlign('center');
            $colHeadingFormat->setVAlign('vcenter');




        $i=0;
        foreach ($header as $k => $h) {
        	$cart->write(0, $i, '', $titleFormat);
        	$cart->write(1, $i++, $h, $titleFormat);
        }

        $cart->write(0, 0, $title, $titleFormat);
            // ������ ������
        $cart->setRow(0,27);
            // ����������� ������ ������� ��� ������ 4 �������
        $cart->setColumn(0,count($header)-1,22);

        for ($k = 1; $k <= count($rows); $k++) {
			$i=0;
	        foreach ($header as $k1 => $h) {
	        	$cart->write($k+1, $i++, $rows[$k-1][$k1], $colHeadingFormat);
	        	$cart->setRow($k+1, 20);
	        }
        }

        $xls->send($fileName);
        $xls->close();

        echo $seq;
	}

    function doGetExcelResult() {

        $cards = $this->request->getPost('cards');
        $featureType = $this->request->getPost('featureType');

        $attService = $this->wgsTier->getProjectObjectService()->getAttObjectService();

        switch ($featureType) {
        	case self::REPAIR:
        		//������������ - name
        		//����(�������)
        		//����������� (��� ������������)
        		//����������. - note

        		$rows = array();
        		for ($i=0; $i < count($cards); $i++) {
        			$baseProperties = $attService->getValueBasePropertiesByObject($cards[$i]);
        			foreach ($baseProperties as $baseProperty){
        				$rows[$i]['name'] = $baseProperty->get('name');//������������
        				$rows[$i]['note'] = $baseProperty->get('note');//����������
        			}
        			$advancedProperties = $attService->GetValuePropertiesByObject($cards[$i]);
        			foreach ($advancedProperties as $advancedProperty){
        				if ($advancedProperty->get('PROPERTY_ID') == 234) {//����(�������)
        					$rows[$i]['rapair_date'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 239) {//����������� (��� ������������)
        					$rows[$i]['executor'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 247) {//�������
        					$rows[$i]['kvartal'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 125) {//����� ����
        					$rows[$i]['nomerdoma'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 124) {//�����
        					$rows[$i]['ulitsa'] = $advancedProperty->get('property_value');
        				}
        			}

        			$rows[$i]['addr'] = $rows[$i]['kvartal'].' '.$rows[$i]['ulitsa'].' '.$rows[$i]['nomerdoma'];
        		}

        		$header = array(
        			'name' => '������������',
        			'addr' => '�����',
        			'rapair_date' => '���� �������',
        			'executor' => '�����������',
        			'note' => '����������',
        		);

        		$this->printExcel($header, $rows, '������ ��������');

        	break;
        	case self::CRASH:
        		//������������
        		//����������
        		//����(�����������)
        		//�������(�����������)
        		//���(�����������)
        		//�����������
        		//����(����������)

        		$rows = array();
        		for ($i=0; $i < count($cards); $i++) {
        			$baseProperties = $attService->getValueBasePropertiesByObject($cards[$i]);
        			foreach ($baseProperties as $baseProperty){
        				$rows[$i]['name'] = $baseProperty->get('name');//������������
        				$rows[$i]['note'] = $baseProperty->get('note');//����������
        			}
        			$advancedProperties = $attService->GetValuePropertiesByObject($cards[$i]);
        			foreach ($advancedProperties as $advancedProperty){
        				if ($advancedProperty->get('PROPERTY_ID') == 224) {//����(�����������)
        					$rows[$i]['crash_date'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 227) {//�������(�����������)
        					$rows[$i]['cause'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 225) {//���(�����������)
        					$rows[$i]['type'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 311) {//�����������
        					$rows[$i]['executor'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 312) {//����(����������)
        					$rows[$i]['removal_date'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 247) {//�������
        					$rows[$i]['kvartal'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 125) {//����� ����
        					$rows[$i]['nomerdoma'] = $advancedProperty->get('property_value');
        				}
        				else
        				if ($advancedProperty->get('PROPERTY_ID') == 124) {//�����
        					$rows[$i]['ulitsa'] = $advancedProperty->get('property_value');
        				}
        			}
					$rows[$i]['addr'] = $rows[$i]['kvartal'].' '.$rows[$i]['ulitsa'].' '.$rows[$i]['nomerdoma'];
        		}

        		$header = array(
        			'name' => '������������',
        			'addr' => '�����',
        			'crash_date' => '���� �����������',
        			'cause' => '�������',
        			'type' => '���',
        			'executor' => '�����������',
        			'removal_date' => '���� ����������',
        			'note' => '����������',
        		);

        		$this->printExcel($header, $rows, '������ ������');

        	break;
        }
    }

    function doGetExcelFile() {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."report-$seq.xls");
        $len = strlen($buf);
        $this->response->header("Content-type: application/vnd.ms-excel");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: attachment; filename=report-$seq.xls");
        $this->response->header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        $this->response->header("Pragma: public");
        $this->response->write($buf);
        //@unlink(WGS3_PRINTCACHE_DIR."report-$seq.xls");
    }

	function doGetExcel() {
    	$id = $this->request->getRequest('id');
     	$this->response->write("<html><head>" .
        '<script language="javascript">' .
        'window.location.href = "/map/cardsjournal/GetExcelFile/'.$id.'";'.
        '</script>'.
        "</head><body></body></html>");
    }
}