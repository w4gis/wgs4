<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');

class GeometryController extends wgsFrontController
{
	
	function getCoordinateTransform($coordSysFactory, $reverse = false){
		
		$coordSysWkt = 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]]';
        $coordSysIni = $coordSysFactory->Create($coordSysWkt);
					
        $wgs84 = 'GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]]';
		$coordSysWgs84 = $coordSysFactory->Create($wgs84);
		echo $reverse;
		
		if(!$reverse == 1)
			$coordTransformWgs = $coordSysFactory->GetTransform($coordSysIni, $coordSysWgs84);
		else
			$coordTransformWgs = $coordSysFactory->GetTransform($coordSysWgs84, $coordSysIni);
		// create a coordinate
		
       
		return $coordTransformWgs;
	}
	
	function doTransformCoords(){
		$coordX = $this->request->getPost('x');
		$coordY = $this->request->getPost('y');			
       
		
		$coordSysFactory = new MgCoordinateSystemFactory();
		$geometryFactory = new MgGeometryFactory();
		$coordTransformWgs = $this->getCoordinateTransform($coordSysFactory);
			
		$coordinate = $geometryFactory->CreateCoordinateXY($coordX, $coordY); 
		$convertedCoordinateWgs =  $coordTransformWgs->Transform($coordinate);
		
		return CJSON::encode( array ( 'x' => $convertedCoordinateWgs -> GetX(), 'y' => $convertedCoordinateWgs -> GetY()  ));
			
	}
	
	function doGetFeatureTypeByLayer() {
		$mapName = $this->request->getPost('mapname');
        $sessionId = $this->request->getPost('session');
        $layerId = $this->request->getPost('layerId');
        if(!$mapName || !$layerId || !$sessionId )
			return;			
		
		
		$this->mgTier = lmbToolkit :: instance() -> getMgTier();
		if( $sessionId)$this->mgTier->openMgSession($sessionId);
        
        $siteConnection = $this->mgTier->getSiteConnection();
            
		$featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
		$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        
        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $layers = $map->GetLayers();  
       // echo $layers->GetCount();// SELECT ST_GeometryType(geom) FROM table WHERE column=value
		
        for ($i = 0; $i < $layers->GetCount(); $i++){
			try {
				if(strrpos( $layers->GetItem($i)->GetFeatureClassName(),  "_".$layerId .'_')){
					$res = $featureService->ExecuteSqlQuery(
						new MgResourceIdentifier($layers->GetItem($i)->GetFeatureSourceId()), 
						'SELECT ST_GeometryType(geom) as gt FROM '.getProjectName().'_sdb.layer_'.$layerId .'_g limit 1');
					$res -> ReadNext();
					$result = array ( 'error' => 0, 'gt' => ( 
						strtoupper($res -> GetString ( 'GT' ) ) == "ST_POLYGON" ? 3 : 
							(strtoupper($res -> GetString ( 'GT' ) ) == "ST_POINT" ? 1 : 2)
						)
					);
					return CJSON :: encode ($result);
				} 
			} catch (Exception $e){
				$result = array ( 'error' => '1', 'message' => 'unknown error');
				return CJSON :: encode ($result);
			}		
		}
		$result = array ( 'error' => '2', 'message' => 'wrong parameters');
		return CJSON :: encode ($result);
	}
		
	//TODO: 4326??
	function doInsertPoint(){
		$mapName = $this->request->getPost('mapname');
		$espg = 4326;
        $sessionId = $this->request->getPost('session');
        $layerId = $this->request->getPost('layerId');
        $featureType = $this->request->getPost('feature_type');
        if(!$mapName || !$layerId || !$sessionId ||  !$featureType )
			return;			
		//
		$coordSysFactory = new MgCoordinateSystemFactory();
		$geometryFactory = new MgGeometryFactory();
		$reverse = $this->request->getPost('reverse');
		$coordTransformWgs = $this->getCoordinateTransform($coordSysFactory);
			
		switch ($featureType) {
			
			/*
			 * POINT
			 */
			case 1: {
				$coordX = $this->request->getPost('x');
				$coordY = $this->request->getPost('y');			
				$coordinate = $geometryFactory->CreateCoordinateXY($coordX, $coordY);       
				$convertedCoordinateWgs =  $reverse == 1 ? $coordinate : $coordTransformWgs->Transform($coordinate);
				$statement = 'ST_MakePoint('. $convertedCoordinateWgs->GetX(). ','. $convertedCoordinateWgs->GetY() .')';				
				break;
			};
			
			/*
			 * LINE
			 */
			case 2: {
				$convertedCoords = [];
				$coords = $data = CJSON::decode($this->request->getPost('coords'));
				$coordString = '';
				for($i=0; $i < count($coords); $i++) {				
					$currentConverterdCoord = $reverse == 1 ? $geometryFactory->CreateCoordinateXY($coords[$i]['x'], $coords[$i]['y']) : $coordTransformWgs->Transform( $geometryFactory->CreateCoordinateXY($coords[$i]['x'], $coords[$i]['y']) );
					$coordString .=  'ST_MakePoint('. $currentConverterdCoord -> GetX(). ','. $currentConverterdCoord -> GetY() .'),';				
				}	
				$statement = 'ST_MakeLine(' . substr($coordString, 0, -1 ) . ')';
				
				break;
			};
			
			/*
			 * Polygon
			 */ 
			case 3: {
				$convertedCoords = [];
				$coords = $data = CJSON::decode($this->request->getPost('coords'));
				$coordString = 'ST_GeomFromText(\'LINESTRING(';
				for($i=0; $i < count($coords); $i++) {				
					$currentConverterdCoord = $coordTransformWgs->Transform( $geometryFactory->CreateCoordinateXY($coords[$i]['x'], $coords[$i]['y']) );
					//$currentConverterdCoord = $geometryFactory->CreateCoordinateXY($coords[$i]['x'], $coords[$i]['y']) ;//$coordTransformWgs->Transform( $geometryFactory->CreateCoordinateXY($coords[$i]['x'], $coords[$i]['y']) );
					$coordString .=  $currentConverterdCoord -> GetX(). ' '. $currentConverterdCoord -> GetY() .',';				
				}
				$firstCoord = $coordTransformWgs->Transform( $geometryFactory->CreateCoordinateXY($coords[0]['x'], $coords[0]['y']) );
				//$firstCoord = $geometryFactory->CreateCoordinateXY($coords[0]['x'], $coords[0]['y']);//$coordTransformWgs->Transform( $geometryFactory->CreateCoordinateXY($coords[0]['x'], $coords[0]['y']) );
				$coordString .=  $firstCoord -> GetX(). ' '. $firstCoord -> GetY() .',';	
				
				$statement = 'ST_MakePolygon(' . substr($coordString, 0, -1 ) . ')\'))';
				//echo $statement;
				//die();
				break;
			}
			
			
		}
		
		$this->mgTier = lmbToolkit :: instance() -> getMgTier();
		if( $sessionId)$this->mgTier->openMgSession($sessionId);
        
        $siteConnection = $this->mgTier->getSiteConnection();
            
		$featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
		$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        
        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $layers = $map->GetLayers();  
        //echo $layers->GetCount();
        for ($i = 0; $i < $layers->GetCount(); $i++){
			if(strrpos( $layers->GetItem($i)->GetFeatureClassName(),  "_".$layerId .'_')){
				//echo 'insert into '.getProjectName().'_sdb.layer_'.$layerId .'_g (geom) values(ST_SetSRID('. $statement .', '.$espg.'))';
				//die();
				$featureService->ExecuteSqlQuery(
					new MgResourceIdentifier($layers->GetItem($i)->GetFeatureSourceId()), 
					'insert into '.getProjectName().'_sdb.layer_'.$layerId .'_g (geom) values(ST_SetSRID('. $statement .', '.$espg.'))');
				
				return "{'message':'".$layers->GetItem($i)->GetFeatureClassName()."'}";
			}
		} 
		return;		
	}
	
	
	function doRemoveFeature(){
		$mapName = $this->request->getPost('mapname');
        $sessionId = $this->request->getPost('session');
        $layerId = $this->request->getPost('layerId');
        $featureId = $this->request->getPost('feature_id');
        if(!$mapName || !$layerId || !$sessionId ||  !$featureId )
			return;			
		
		$this->mgTier = lmbToolkit :: instance() -> getMgTier();
		if( $sessionId)$this->mgTier->openMgSession($sessionId);
        
        $siteConnection = $this->mgTier->getSiteConnection();
            
		$featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
		$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        
        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $layers = $map->GetLayers();  
        //echo $layers->GetCount();
        for ($i = 0; $i < $layers->GetCount(); $i++){
			if(strrpos( $layers->GetItem($i)->GetFeatureClassName(),  "_".$layerId .'_')){
				echo 'delete from tusur_production.layer_'.$layerId .'_g where feature_id = ' . $featureId;
				$featureService->ExecuteSqlQuery(
					new MgResourceIdentifier($layers->GetItem($i)->GetFeatureSourceId()), 
					'delete from tusur_production.layer_'.$layerId .'_g where feature_id = ' . $featureId);
				$featureService->ExecuteSqlQuery(
					new MgResourceIdentifier($layers->GetItem($i)->GetFeatureSourceId()), 
					'delete from tusur_production.featuretable where feature_id = ' . $featureId);
				
				
				return "{'message':'".$layers->GetItem($i)->GetFeatureClassName()."'}";
			}
		} 
		return;		
	}
	
	
    function doGetSelectedFeatures()
    {
        $agfReaderWriter = new MgAgfReaderWriter();

        $mapName = $this->request->getPost('mapname');
        $version = $this->request->getPost('version');
        $selectionXML = $this->request->getPost('selection');
        $sessionId = $this->request->getPost('session');
       //ini_set('display_errors',1);
       
		$isShape = $this->request->getPost('isshape');
        if(!$isShape)
			$isShape = strstr($selectionXML,'shp') ? true: false;
		
        if ($mapName && $selectionXML) 
            {
            $this->mgTier = lmbToolkit :: instance() -> getMgTier();
			if( $sessionId)$this->mgTier->openMgSession($sessionId);
        
            $siteConnection = $this->mgTier->getSiteConnection();
            $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
            $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);

            $map = new MgMap();
            $map->Open($resourceService, $mapName);
            $selection = new MgSelection($map, $selectionXML);

            $layers = $selection->GetLayers();
            $layersCount = $layers->GetCount();
             
            $selectedLayers = array();
            $a = array();
            for ($i = 0; $i < $layers->GetCount(); $i++)
            {
				
                $selectedLayer = array();
                $layer = $layers->GetItem($i);
                $layerLegend = $layer->GetLegendLabel();
                $featureClassName = $layer->GetFeatureClassName();
                $layerFeatureId = $layer->GetFeatureSourceId();
                $selectionString = $selection->GenerateFilter($layer, $featureClassName);
                $queryOptions = new MgFeatureQueryOptions();
                $queryOptions->SetFilter($selectionString);
                $layerFeatureResource = new MgResourceIdentifier($layerFeatureId);
                $featureReader = $featureService->SelectFeatures($layerFeatureResource, $featureClassName, $queryOptions);
                $defaultGeometryProperty = $featureReader -> GetClassDefinition() -> GetDefaultGeometryPropertyName();
                
                $spatialContextReader = $featureService->GetSpatialContexts($layerFeatureResource, true);
                
                $coordSysWkt=null;
				while ($spatialContextReader->ReadNext()) {
					$name = $spatialContextReader->GetName();
					if ($name == NULL) {
						$name = "null";
					}
					$coordSysWkt = $spatialContextReader->GetCoordinateSystemWkt();
					if ($coordSysWkt == NULL) {
						$coordSysWkt = "null";
					}
					//echo  $coordSysWkt;
					//echo  $name;
					//if(strtoupper($name) === "DEFAULT")
						break;
				}
               
				// echo  $coordSysWkt;
    //             die();
				if(!$coordSysWkt || $coordSysWkt == 'null') {
				    // 	$coordSysWkt = 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]]';
				    $coordSysWkt = 'PROJCS["UTM84-4N",GEOGCS["LL84",DATUM["WGS84",SPHEROID["WGS84",6378137.000,298.25722293]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["false_easting",500000.000],PARAMETER["false_northing",0.000],PARAMETER["central_meridian",-159.00000000000000],PARAMETER["scale_factor",0.9996],PARAMETER["latitude_of_origin",0.000],UNIT["Meter",1.00000000000000]]';
				}
				
                while ($featureReader->ReadNext()) 
                {
					//var_dump( );
                    if ($isShape) {
                        try {
							$id = $featureReader->GetInt64('OBJECTID');
						} catch (Exception $e) {
							$id = rand();
						}
                        $label = $featureReader->IsNull('Text') ? 'Без наименование' : $featureReader->GetString('Text');
                        
						$byteReader = $featureReader->GetGeometry($defaultGeometryProperty);
                    } else {
						$byteReader = $featureReader->GetGeometry($defaultGeometryProperty);
						try{
							$id = $featureReader->GetInt64('feature_id');													
						} catch (Exception $e) {
							try {
								$id = $featureReader->GetInt64('OBJECTID');
							} catch (Exception $e) {
								$id = rand();
							}
						}
						
						try {
							$label = $featureReader->IsNull('label') ? 'Без наименование' : $featureReader->GetString('label');
							if(!$featureReader->IsNull('name'))
								$label = $featureReader->GetString('name');
						}catch (Exception $e) {
							$label = 'Без наименование';
						}	
						
					}
					
					$wgs84 = 'GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]]';
					$pseudoMercator = 'PROJCS["WGS84.PseudoMercator",GEOGCS["LL84",DATUM["WGS84",SPHEROID["WGS84",6378137.000,298.25722293]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Popular Visualisation Pseudo Mercator"],PARAMETER["false_easting",0.000],PARAMETER["false_northing",0.000],PARAMETER["central_meridian",0.00000000000000],UNIT["Meter",1.00000000000000]]';
					$pulkovo = 'GEOGCS["Pulkovo42.LL",DATUM["PULKOVO",SPHEROID["KRASOV",6378245.000,298.30000000],TOWGS84[28.0000,-130.0000,-95.0000,0.000000,0.000000,0.000000,0.00000000]],PRIMEM["Greenwich",0],UNIT["Degree",0.01745329251994]]';
					$pz90 = 'GEOGCS["ParametropZemp1990a.LL",DATUM["ParametropZemp1990a",SPHEROID["PZ-90",6378136.000,298.25784285],TOWGS84[0.0000,0.0000,1.5000,0.000000,0.000000,-0.076000,0.00000000]],PRIMEM["Greenwich",0],UNIT["Degree",0.01745329251994]]';
					$utm = 'PROJCS["UTM84-45N",GEOGCS["LL84",DATUM["WGS84",SPHEROID["WGS84",6378137.000,298.25722293]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["false_easting",500000.000],PARAMETER["false_northing",0.000],PARAMETER["central_meridian",87.00000000000000],PARAMETER["scale_factor",0.9996],PARAMETER["latitude_of_origin",0.000],UNIT["Meter",1.00000000000000]]';
                    
                    $geometry = $agfReaderWriter->Read($byteReader);
                    $dimension = $geometry->GetDimension();
                    $coordinateIterator = $geometry->GetCoordinates();
                    $geometryFactory = new MgGeometryFactory();
					$coordSysFactory = new MgCoordinateSystemFactory();
					
					$coordSysIni = $coordSysFactory->Create($coordSysWkt);
					
					$coordSysProjPM = $coordSysFactory->Create($pseudoMercator);
                    $coordTransformPM = $coordSysFactory->GetTransform($coordSysIni,$coordSysProjPM);                        
					
					$coordSysProjUTM = $coordSysFactory->Create($utm);
                    $coordTransformUTM = $coordSysFactory->GetTransform($coordSysIni,$coordSysProjUTM);                        
					
                    $coordSysProjPu = $coordSysFactory->Create($pulkovo);
                    $coordTransformPu = $coordSysFactory->GetTransform($coordSysIni,$coordSysProjPu);                        
					
                    $coordSysProjPz = $coordSysFactory->Create($pz90);
                    $coordTransformPz = $coordSysFactory->GetTransform($coordSysIni,$coordSysProjPz);                        
					
					
					$coordSysProjWgs = $coordSysFactory->Create($wgs84);
                    $coordTransformWgs = $coordSysFactory->GetTransform($coordSysIni,$coordSysProjWgs);                        
					
                    $j = 0;
                    $coords = array('wgs'=>array(), 'pm'=>array(),'pz'=>array(),'pu'=>array());
                    $coordsStr = '<br>';
                    while($coordinateIterator->MoveNext())
                    {
                       // echo $j;
                        $coordinate = $coordinateIterator->GetCurrent();
                        
						$convertedCoordinatePM = $coordTransformPM->Transform($coordinate);
						$convertedCoordinatePu = $coordTransformPu->Transform($coordinate);
						$convertedCoordinatePz = $coordTransformPz->Transform($coordinate);
						$convertedCoordinateWgs = $coordTransformWgs->Transform($coordinate);
                        
                        //WGS84
                        $x = $convertedCoordinateWgs->GetX();
                        $y = $convertedCoordinateWgs->GetY();
                        
                        if( (($x != $coords['wgs'][0]['x']) || ($y != $coords['wgs'][0]['y'])) && (($coords['wgs'][$j-1]['x']!=$x)||($coords['wgs'][$j-1]['y']!=$y)))
                        {
                            $coords['wgs'][$j]['x'] = $x;
                            $coords['wgs'][$j]['y'] = $y;
                            //$coordsStrPm = '<b>'.$coordsStr.$j.' : ( '.(round($x*100)/100).' , '.(round($y*100)/100).' )<br></b>';
                        }
                        
                        //PSEUDO MERCATOR
                        $x = $convertedCoordinatePM->GetX();
                        $y = $convertedCoordinatePM->GetY();
                        //echo (($coords['pm'][$j-1]['x']!=$x) ? "1":'0').$x." ".$coords['pm'][$j-1]['x']."\r\n";
                        if( (($x != $coords['pm'][0]['x']) || ($y != $coords['pm'][0]['y'])) && (($coords['pm'][$j-1]['x']!=$x)||($coords['pm'][$j-1]['y']!=$y)))
                        {
                            $coords['pm'][$j]['x'] = $x;
                            $coords['pm'][$j]['y'] = $y;
                            $coordsStr = ''.$coordsStr.$j.' : ( '.(round($x*100)/100).' , '.(round($y*100)/100).' )<br/>';
                        }
                        
                        //PULKOVO-42
                        $x = $convertedCoordinatePu->GetX();
                        $y = $convertedCoordinatePu->GetY();
                        if( (($x != $coords['pu'][0]['x'])||($y != $coords['pu'][0]['y'])) && (($coords['pu'][$j-1]['x']!=$x) || ($coords['pu'][$j-1]['y']!=$y)))
                        {
                            $coords['pu'][$j]['x'] = $x;
                            $coords['pu'][$j]['y'] = $y;
                            //$coordsStrPu = '<b>'.$coordsStr.$j.' : ( '.(round($x*100)/100).' , '.(round($y*100)/100).' )<br></b>';
                        }
                        
                        //PARAMETRY ZEMLY
                        $x = $convertedCoordinatePz->GetX();
                        $y = $convertedCoordinatePz->GetY();
                        if((($x != $coords['pz']['x']) || ($y != $coords['pz'][0]['y'])) && (($coords['pz'][$j-1]['x']!=$x) || ($coords['pz'][$j-1]['y']!=$y)))
                        {
                            $coords['pz'][$j]['x'] = $x;
                            $coords['pz'][$j]['y'] = $y;
                           // $coordsStrPz = '<b>'.$coordsStr.$j.' : ( '.(round($x*100)/100).' , '.(round($y*100)/100).' )<br></b>';
                        }
                        $j++;
                    }
                                                
                    switch ($dimension) {
                        case 0:
                            $a[$id]['coords'] = $coordStr;
                            $a[$id]['id'] = $id;
                            $a[$id]['featureId'] = $featureId;
                            $a[$id]['label'] = $label;
                            $a[$id]['type'] = 'Point';
                            $a[$id]['layer'] = $layerLegend;
                            if (!$version || $version < 4) {
								$a[$id]['X'] = round($coords['pm'][0]['x']*100)/100;
								$a[$id]['Y'] = round($coords['pm'][0]['y']*100)/100;
							}
                            //$a[$id]['X'] = round($coords[0]['x']*100)/100;
                            //$a[$id]['Y'] = round($coords[0]['y']*100)/100;
                            $a[$id]['coords'] = ($version && $version == 4) ? $coords : $a[$id]['coords'] = $coordsStr;
                            
                            break;
                        case 1:
                           // $length = $geometry->GetLength();
                           	$geometry2 = $geometry; //->Transform($coordTransformUTM);

							// $area = $geometry->GetArea()'); 
                            // $length = $coordSysProjPM->ConvertCoordinateSystemUnitsToMeters($geometry2->GetLength());
                            $length = $geometry2->GetLength();
                            
                            //echo $geometry->GetLength()." ".$length;
                            $a[$id]['length'] = round($length*100)/100;
                            $a[$id]['featureId'] = $featureId;
                            $a[$id]['label'] = $label;
                            $a[$id]['coords'] = ($version && $version == 4) ? $coords : $a[$id]['coords'] = $coordsStr;
                            $a[$id]['id'] = $id;
                            $a[$id]['type'] = 'Line';
                            $a[$id]['layer'] = $layerLegend;
                            break;
                        case 2:
                            $geometry2 = $geometry; //->Transform($coordTransformUTM);

							$area = strstr($coordSysWkt,'PROJECTION') ? $geometry->GetArea() : $geometry2->GetArea();
                            $length = strstr($coordSysWkt,'PROJECTION') ? $geometry->GetLength() : $coordSysProjPM->ConvertCoordinateSystemUnitsToMeters($geometry2->GetLength());
                            //$length = 0;;
                            //$area = 0;
                            //$area = $geometry->GetArea();//$coordSysIni->ConvertCoordinateSystemUnitsToMeters();
                            //$length = $geometry->GetLength();
                            //$length = $coordSysIni->ConvertCoordinateSystemUnitsToMeters($geometry->GetLength());                            
                            $a[$id]['id'] = $id;
                            $a[$id]['featureId'] = $featureId;
                            $a[$id][($version && $version == 4) ? 'length' : 'perimeter'] = round($length*100)/100;
                            $a[$id]['area'] =  $area;
                            $a[$id]['coords'] = ($version && $version == 4) ? $coords : $a[$id]['coords'] = $coordsStr;
                            $a[$id]['label'] = $label;
                            $a[$id]['type'] = 'Polygon';
                            $a[$id]['layer'] = $layerLegend;
                            break;
                    }
                }
                 
            }
            $this->response->write(json_encode(($version && $version == 4) ? $a : array_merge($a)));
        }
    }
    
    function doTest()
    {
        print_r($this->request->getPost());
    }
    
}
?>
