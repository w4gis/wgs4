<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
include 'wgs/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';
include 'wgs/lib/PHPExcel/Classes/PHPExcel.php';

class MultipleFillingController extends wgsFrontController
{

function doGetExcelValues(){
/////////////////////////////////////////////////
            $json_values = $this->doGetValues();
			$_values = json_decode($json_values);
			$values = $_values -> record;
			//var_dump($values);
			$names = $this->request->getPost('names');
			$seq = rand();
            $fileName = "report-$seq.xls";
            /*$xls =& new Spreadsheet_Excel_Writer(WGS3_PRINTCACHE_DIR.$fileName);
			$xls->setVersion(8); // ����������� ����!!! ����� �� ����� �������� setInputEncoding
			# ���������� ����� � �����, ����������� ������� ��� ���������� �����x
			// �������� �����
            $cart =& $xls->addWorksheet('report');
			//$cart->setInputEncoding('UTF-8'); 
            
            $titleText = '������� ����������';//lmb_win1251_to_utf8('������� ����������');
            // �������� ������� ��������������
            $titleFormat =& $xls->addFormat();
            // ����������� ������ - Helvetica �������� � OpenOffice calc ����...
            $titleFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
            $titleFormat->setBold();
            // ����������� ������� ������
            $titleFormat->setSize('13');
            // ����������� ����� ������
            $titleFormat->setColor('navy');
            // ����������� ������ ������� ��������� � "thick"
            $titleFormat->setBottom(2);
            // ����������� ����� ������� ���������
            $titleFormat->setBottomColor('navy');
            // ����������� ������������ � ����������� ��������
            $titleFormat->setAlign('merge');
            $titleFormat->setVAlign('vcenter');
            // ���������� ��������� � ���x��� ����� ������ ����� ,
            // ��������� ��� ������ ��������� � ����� ������ ��������������

            $cart->write(0,0, $titleText, $titleFormat);
            $i=1;
			foreach($values[0] as $key=> $val)			
				if(strpos($key,'pi')!== false){
					$cart->write(0,$i,'',$titleFormat);
					$i++;
				}
            
            // ������ ������
            $cart->setRow(0,30);
            // ����������� ������ ������� ��� ������ 4 �������
            $cart->setColumn(0,$i-1,20);
			$i=0;
			foreach($names as $key=> $val)	{		
				$cart->write(1,$i,lmb_utf8_to_win1251($val),$titleFormat);
				$i++;
			}
			// ������ ������
            $cart->setRow(1,30);
            // ����������� ������ ������� ��� ������ 4 �������
            $cart->setColumn(0,$i-1,20);
			
            // ����������� ���������� ��������������
            $colHeadingFormat =& $xls->addFormat();
            $colHeadingFormat->setBold();
            $colHeadingFormat->setFontFamily('Helvetica');
            $colHeadingFormat->setBold();
            $colHeadingFormat->setSize('10');
            $colHeadingFormat->setAlign('center');

            // ������ � ������� ��������� ��� ��������
            //$colNames = array('Item','Price($)','Quantity','Total');

            // ���������� ���� ��������� ������ �������
            // ��������� ������ ������ ��� ����� ��������� ����
            //$cart->writeRow(2,0,$colNames,$colHeadingFormat);

            // ������ ����� ��� �������������
            // 1-�� �������� - ������� ������������� �����������
            // 2-�� �������� - ������� ��������������� �����������
            // (0 = ��� ��������������� �����������)
            // 3-�� �������� - ������� ������� ������ ����� ������������� �����������
            // 4-�� �������� - ����� ������� ������� ����� ��������������� �����������
            $freeze = array(2,0,2,0);

            // ���������� ��� ������!
            $cart->freezePanes($freeze);

            // ������ ������

            // ����������� ��� ��� ������������ �������� ������ ������
            $currentRow = 2;

            // �������� ����� ������, �������� �� � ����
            //foreach ( $items as $item ) {
               // Write each item to the sheet
            //$cart->writeRow($currentRow,0,$item);
            //$currentRow++;
            //}

          
            $level2ValueFormat = & $xls->addFormat();
            $level2ValueFormat->setAlign('merge');
            $level2ValueFormat->setFontFamily('Helvetica');
            // ����������� ������� ������
            $level2ValueFormat->setBold();
            // ����������� ������� ������
            $level2ValueFormat->setSize('11');
            $level2ValueFormat->setVAlign('vcenter');
            // ����������� ����� ������
            $level2ValueFormat->setTop(1);
            $level2ValueFormat->setBottom(1);
            $level2ValueFormat->setColor('black');


            foreach ($values as $k=>$v) {
				if(1==1){
					$i = 0;
					foreach($v as $key=> $val){
						if(strpos($key,'pv')!== false || strpos($key,'objectId')!==false){
							$cart->setRow($currentRow, 20);
							$cart->write($currentRow, $i, ($val ? lmb_utf8_to_win1251($val): " "), $level2ValueFormat);
							$i++;
						}
					}
					$currentRow++;
				}
            }                        
                   
            $xls->send($fileName);
            $xls->close();
*/
$objPHPExcel = new PHPExcel;
// set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');// set default font size
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
// create the writer
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");
$objSheet = $objPHPExcel->getActiveSheet();
// rename the sheet
$objSheet->setTitle(lmb_win1251_to_utf8('������� ����������'));
$alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P');
$i=0;
$objSheet->getCell('A1')->setValue(lmb_win1251_to_utf8('������� ����������'));
				
	foreach($names as $key=> $val)	{		
					$objSheet->getCell($alpha[$i].'2')->setValue($val);
					$i++;
					}
				
    $currentRow=0;     
	foreach ($values as $k=>$v) {
				if(1==1){
					$i = 0;
					foreach($v as $key=> $val){
						if(strpos($key,'pv')!== false || strpos($key,'objectId')!==false){
						//	$objSheet->getCell('A1')->setValue('ETEE');
//31	('A1', 'ETEE');
							$objSheet->getCell($alpha[$i] . ($currentRow+3))->setValue($val);
    
							$i++;
						}
					}
					$currentRow++;
				}
            } 
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(FONT_DIR.'/iad.xls');

            echo $seq;
}
function doGetValues()
    {
        $objectIds = $this->request->getPost('objectIds');
        $propIds = $this->request->getPost('propIds');
		 $starts = $this->request->getPost('start')?$this->request->getPost('start'):0;
		 $limits = $this->request->getPost('limit')?$this->request->getPost('limit'):0;
		$total = count($objectIds);			
		$const = 500;
        $propertyList = array();		
        if ($objectIds && $propIds) {
			$c = count($objectIds)/$const;
			$oi = array();
			$pl = array();
			//if($starts+$limits>$total)
				//$limits = 1;			
			if($limits>0)
				$c = 1;
			for($j = 0; $j<$c; $j++){
				$oi = array_slice($objectIds, $starts ? $starts : ($j)*$const, $limits ? $limits : ($j+1)*$const);		
				$properties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValues($oi, $propIds);
				$total = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetCountListObjectByExample($oi[0]);
				
				foreach ($properties as $property) {
					$propertyList[] = array (
						'objectId' => $property->get('object_id')?$property->get('object_id'):$property->get('vco'),
						'pv' => lmb_win1251_to_utf8($property->get('pv')),//?$property->get('pv'):'',
						'pi' => lmb_win1251_to_utf8($property->get('pi'))//?$property->get('pi'):'',
					);
					$i=1;
					while($i<count($propIds)){
						$propertyList[count($propertyList)-1]['pv'.($i+1)] = lmb_win1251_to_utf8($property->get('pv'.($i+1)));//;/?$property->get('pv'.($i+1)):'';
						$propertyList[count($propertyList)-1]['pi'.($i+1)] = lmb_win1251_to_utf8($property->get('pi'.($i+1)));//?$property->get('pi'.($i+1)):'';
						$i++;
					}
				}
			}
			$a['record'] = array_values($propertyList);
			$a['total']=$total;
            return json_encode($a);
        }
    }
    function doGetCards()
    {
        //$objectIds = $this->request->getPost('objectIds');
        $objectsArray = array();
        if ($this->request->getPost('featureIds')) {
            $featureIds = $this->request->getPost('featureIds');
            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
                $objectsArray = array();
                foreach ($objectList as $object) {
                    $objectsArray[] = array (
                            'featureLayerId'    => $object->get('featurelayer_id'),
                            'objectId'  => $object->get('object_id'),
                            'objectTypeId'  => $object->get('object_type_id')
                    );
                }
        } else {

            if ($this->request->getPost('objectIds')) {
                $objectIds = $this->request->getPost('objectIds');
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds, 1);
                foreach ($objectTypesList as $objectType) {
                     $objectsArray[] = array(
                        'objectTypeId' => $objectType->get('object_type_id'),
                        'objectId'  => $objectType->get('object_id')
                     );
                 }
            }
        }
        $this->response->write(json_encode(array_values($objectsArray)));
    }

    function doGetObjectTypes()
    {
        if($this->request->getPost('featureIds')) {
            $featureIds = $this->request->getPost('featureIds');
            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
            $objectsArray = array();
            foreach ($objectList as $object) {
                $objectsArray[] = array (
                    'featureLayerId'    => $object->get('featurelayer_id'),
                    'featureLayerName'  => lmb_win1251_to_utf8($object->get('featurelayer_name')),
                    'objectTypeId'  => $object->get('object_type_id'),
                    'objectTypeName'  => lmb_win1251_to_utf8($object->get('object_type_name')),
                    'objectId'  => $object->get('object_id')
                );
            }
            $layers = array();
            $uniqLayers = array();
            for ($i = 0; $i < count($objectsArray); $i++) {
                $layers[] = $objectsArray[$i]['featureLayerId'];
            }
            $uniqLayers = array_unique($layers);

            $layersWithTypes = array();

            foreach ($uniqLayers as $uniqLayer) {
                $featuresCount = 0;
                foreach ($objectsArray as $object) {
                    if($object['featureLayerId'] == $uniqLayer) {
                        $layerLabel = $object['featureLayerName'];
                        $featuresCount++;
                    }
                }
                $layersWithTypes[] = array(
                    'name'              => $layerLabel.' ('.lmb_win1251_to_utf8('��������').' : '.$featuresCount.' )',
                    'label'             => $layerLabel.' ('.lmb_win1251_to_utf8('��������').' : '.$featuresCount.' )',
                    'featureLayerId'    => -1,
                    'objectTypeId'      => -1
                );
                $featureTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($uniqLayer);
                foreach ($featureTypesList as $featureType) {
                    if($featureType->get('is_current')) {
                        $layersWithTypes[] = array(
                                'name'              => '<div style="padding-left:15px; font-size:8pt;">'.lmb_win1251_to_utf8($featureType->get('Name').' (��������)').'</div>',
                                'label'             => lmb_win1251_to_utf8($featureType->get('Name').' (��������)'),
                                'featureLayerId'    => $uniqLayer,
                                'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                    } else {
                        $layersWithTypes[] = array(
                                'name'              => '<div style="padding-left:15px; font-size:8pt;">'.lmb_win1251_to_utf8($featureType->get('Name')).'</div>',
                                'label'             => lmb_win1251_to_utf8($featureType->get('Name')),
                                'featureLayerId'    => $uniqLayer,
                                'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                    }
                }
            }
        } else {
            $layersWithTypes = array();
            if($this->request->getPost('objectIds')) {
                $objectIds = $this->request->getPost('objectIds');
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds,0);
                foreach ($objectTypesList as $objectType) {
                     $layersWithTypes[] = array(
                        'label'=> lmb_win1251_to_utf8($objectType->get('type_name')),
                        'name' => lmb_win1251_to_utf8($objectType->get('type_name')),
                        'objectTypeId' => $objectType->get('object_type_id')
                     );
                 }
            }
        }
        $this->response->write(json_encode(array_values($layersWithTypes)));
    }

    function doGetPropertiesByObjectType()
    {
        $objectTypeId = $this->request->getPost('objectTypeId');
        if ($objectTypeId) {
            $properties = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($objectTypeId, null);
            $propertyList = array();

            foreach($properties as $property)
            {
                $propertyList[] = array(
                                'id'            => $property->get('Property_Id'),
                                'name'          => lmb_win1251_to_utf8($property->get('Name')),
                                'valueTypeId'   => $property->get('Value_properties_type_id'),
                                'groupId'       => $property->get('Properties_group_id'),
                                'groupName'     => lmb_win1251_to_utf8($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($property->get('Properties_group_id')))
                );
            }
            $this->response->write(json_encode(array_values($propertyList)));
        }
    }

    function doGetBaseValueProperties () {
        $objectIds = $this->request->getPost('objectIds');
        $names = array();
        $shortNames = array();
        $notes = array();
        foreach ($objectIds as $objectId) {
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){
                $names[] = lmb_win1251_to_utf8($baseProperty->get('name'));
                $shortNames[] = lmb_win1251_to_utf8($baseProperty->get('short_name'));
                $notes[] = lmb_win1251_to_utf8($baseProperty->get('note'));
            }
        }
        $name = ((count(array_unique($names))==1))? $names[0]: '<...>';
        $shortName = ((count(array_unique($shortNames))==1))? $shortNames[0]: '<...>';
        $note = ((count(array_unique($notes))==1))?$notes[0]: '<...>';


        $values[0] = array(
                'name' => $name,
                'shortName' => $shortName,
                'note' => $note
        );
        $this->response->write(json_encode(array_values($values)));
    }

    function doGetValueProperties()
    {
        $objectIds = $this->request->getPost('objectIds');
        $propertyList = array();
        if ($objectIds) {
            $properties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValuePropertiesByObjects($objectIds);
            foreach ($properties as $property) {
                $propertyList[] = array (
                    'propertyId' => $property->get('property_id'),
                    'propertyValue' => lmb_win1251_to_utf8($property->get('property_value'))
                );
            }
            $this->response->write(json_encode(array_values($propertyList)));
        }
    }

    function doSetValueProperties()
    {
        $objectIds = $this->request->getPost('objectIds');
        $propertyIds = $this->request->getPost('propIds');
        $propertyValues = $this->request->getPost('propValues');
        $name = lmb_utf8_to_win1251($this->request->getPost('name'));
        $shortName = lmb_utf8_to_win1251($this->request->getPost('shortName'));
        $note = lmb_utf8_to_win1251($this->request->getPost('note'));

        for ($i = 0; $i < count($objectIds); $i++ ) {
            if ($name != '&^&') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetName($objectIds[$i],$name);
            }
            if ($shortName != '&^&') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetShortName($objectIds[$i],$shortName);
            }
            if ($note != '&^&') {
                $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetNote($objectIds[$i],$note);
            }
            if (isset($propertyIds)) {
                for ($j = 0; $j < count($propertyIds); $j++ ) {
                    $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($objectIds[$i],$propertyIds[$j],lmb_utf8_to_win1251($propertyValues[$j]));
                }
            }
        }
    }

    //������� ��� ��������� ������ �������� �������� ��������������
    function doGetValueDomainList()
    {
        $propid = $this->request->getPost('propId');
        $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueDomain($propid);
        $count = 0;
        foreach($result as $object)
        {
            $objectList[$count]['id'] = $object->get('Value_Domain_ID');
            $objectList[$count]['name'] = lmb_win1251_to_utf8($object->get('Value_Domain'));
            $count = $count + 1;
        }
        $this->response->write(json_encode($objectList));
    }

    function doDisplay()
    {

    }

    function doGetCardsByFeatureId()
    {
        $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($this->request->getPost('featureId'));
        $i = 1;
        foreach ($cardList as $value) {
            if($value->get('Name') != '')
            $cardName = lmb_win1251_to_utf8($value->get('Name')).' - '.lmb_win1251_to_utf8($value->get('object_type_name'));
            else
            $cardName = lmb_win1251_to_utf8('��� ������������ - ').lmb_win1251_to_utf8($value->get('object_type_name'));
            $objectId = $value->get('Object_id');
            $cardsArray[] = array(//<li style="list-style:square;">
                                    'cardName' => $cardName,
                                    'cardId' => $objectId,
                                    'cardIsCurrent' => $value->get('is_current'),
                                    'cardTypeName'  => lmb_win1251_to_utf8($value->get('object_type_name')),
                                    'cardRelationName' => '',
                                    'cardLabel' => $cardName
            );
            $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
            foreach ($childObjects as $childObject) {
                if($childObject->get('name') != '')
                $cardName = lmb_win1251_to_utf8($childObject->get('name')).' - '.lmb_win1251_to_utf8($childObject->get('type_name').' (�����: ').lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                else
                $cardName = lmb_win1251_to_utf8('��� ������������ - ').lmb_win1251_to_utf8($childObject->get('type_name').' (�����: ').lmb_win1251_to_utf8($childObject->get('relation_name')).')';

                $cardsArray[] = array(
                                    'cardName'          => '<div style="padding-left:15px; font-size:8pt;">'.$cardName.'</div>',
                                    'cardId'            => $childObject->get('object_id'),
                                    'cardIsCurrent'     => 0,
                                    'cardTypeName'      => lmb_win1251_to_utf8($childObject->get('type_name')),
                                    'cardRelationName'  => lmb_win1251_to_utf8($childObject->get('relation_name')),
                                    'cardLabel'         => $cardName
                );
            }
        }
        $this->response->write(json_encode(array_values($cardsArray)));
    }

    function doGetValueBaseProperties()
    {
        $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($this->request->getPost('objectId'));
        foreach ($baseProperties as $baseProperty){
            $valueArray[] = array(
                                    'id'            => $baseProperty->get('object_id'),
                                    'name'          => lmb_win1251_to_utf8($baseProperty->get('name')),
                                    'shortName'     => lmb_win1251_to_utf8($baseProperty->get('short_name')),
                                    'typeName'      => lmb_win1251_to_utf8($baseProperty->get('type_name')),
                                    'createDate'    => $baseProperty->get('create_date'),
                                    'note'          => lmb_win1251_to_utf8($baseProperty->get('note'))
            );
        }
        $this->response->write(json_encode(array_values($valueArray)));
    }

    function doGetCardsByObjectId()
    {
        $objectId = $this->request->getPost('objectId');
        $childObjects = array();
        if(isset($objectId)) {
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){
                if($baseProperty->get('name') != '')
                $cardName = lmb_win1251_to_utf8($baseProperty->get('name')).' - '.lmb_win1251_to_utf8($baseProperty->get('type_name'));
                else
                $cardName = lmb_win1251_to_utf8('��� ������������ - ').lmb_win1251_to_utf8($baseProperty->get('type_name'));

                $valueArray[] = array(
                                        'cardName'          => $cardName,
                                        'cardId'            => $baseProperty->get('object_id'),
                                        'cardIsCurrent'     => 0,
                                        'cardTypeName'      => lmb_win1251_to_utf8($baseProperty->get('name')),
                                        'cardRelationName'  => '',
                                        'cardLabel'         => $cardName
                );
            }
            $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
            foreach ($childObjects as $childObject) {
                if($childObject->get('name') != '')
                $cardName = lmb_win1251_to_utf8($childObject->get('name')).' - '.lmb_win1251_to_utf8($childObject->get('type_name').' (�����: ').lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                else
                $cardName = lmb_win1251_to_utf8('��� ������������ - ').lmb_win1251_to_utf8($childObject->get('type_name').' (�����: ').lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                $valueArray[] = array(
                                    'cardName'          => '<div style="padding-left:15px; font-size:8pt;">'.$cardName.'</div>',
                                    'cardId'            => $childObject->get('object_id'),
                                    'cardIsCurrent'     => 0,
                                    'cardTypeName'      => lmb_win1251_to_utf8($childObject->get('type_name')),
                                    'cardRelationName'  => lmb_win1251_to_utf8($childObject->get('relation_name')),
                                    'cardLabel'         => $cardName
                );
            }
            $this->response->write(json_encode(array_values($valueArray)));
        }
    }
}

?>