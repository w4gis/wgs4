<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require(MAPVIEWER_DIR."/common.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require(MAPVIEWER_DIR."/layerdefinitionfactory.php");

class RedlineController extends wgsFrontController
{
    function doesLayerExist($layerName, $map)
    {
        $layerCollection = $map->GetLayers();
        return( $layerCollection->Contains($layerName) ? true : false ) ;
    }

    function getMapSrs($map)
    {
        $srs = $map->GetMapSRS();
        if($srs != "")
        return $srs;
        return "LOCALCS[\"Non-Earth (Meter)\",LOCAL_DATUM[\"Local Datum\",0],UNIT[\"Meter\", 1],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";
    }

    function makeLine($name, $key, $x0, $y0 , $x1, $y1)
    {
        $propertyCollection = new MgPropertyCollection();
        $nameProperty = new MgStringProperty("NAME", $name);//MgStringProperty
        $propertyCollection->Add($nameProperty);
       /* $keyProperty = new MgInt32Property("OBJECTID", $key);
        $propertyCollection->Add($keyProperty);
        $totalProp = new MgDoubleProperty("TOTAL", $key);
        $propertyCollection->Add($totalProp);*/
        $wktReaderWriter = new MgWktReaderWriter();
        $agfReaderWriter = new MgAgfReaderWriter();
        $geometry = $wktReaderWriter->Read("LINESTRING XY ($x0 $y0, $x1 $y1)");
        $geometryByteReader = $agfReaderWriter->Write($geometry);
        $geometryProperty = new MgGeometryProperty("SHPGEOM", $geometryByteReader);
        $propertyCollection->Add($geometryProperty);
        return $propertyCollection;
    }
  
    
    function addLine($key,$x0, $y0, $x1, $y1) 
    {
        echo $key, $x0, $y0, $x1, $y1;
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
        $sessionId = $this->mgTier->getMgSessionId();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $map = new MgMap();
        $map->Open($resourceService, $mapName);

        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);
        
        // Add the line to the feature source 
        $batchPropertyCollection = new MgBatchPropertyCollection(); 
        $propertyCollection = $this->makeLine("Line A", $key,$x0, $y0, $x1, $y1); 
        $batchPropertyCollection->Add($propertyCollection); 
                    
        // Add the batch property collection to the feature source 
        $cmd = new MgInsertFeatures($layerName, $batchPropertyCollection);  
        $featureCommandCollection = new MgFeatureCommandCollection(); 
        $featureCommandCollection->Add($cmd); 
              
        // Execute the "add" commands 
        $featureService->UpdateFeatures($resourceIdentifier, $featureCommandCollection, false);      
    }
    
    function makePoint($name, $x, $y)
    {
        $propertyCollection = new MgPropertyCollection();
        $nameProperty = new MgStringProperty("NAME", $name);
        $propertyCollection->Add($nameProperty);

        $wktReaderWriter = new MgWktReaderWriter();
        $agfReaderWriter = new MgAgfReaderWriter();

        $geometry = $wktReaderWriter->Read("POINT XY ($x $y)");
        $geometryByteReader = $agfReaderWriter->Write($geometry);
        $geometryProperty = new MgGeometryProperty("SHPGEOM", $geometryByteReader);
        $propertyCollection->Add($geometryProperty);

        return $propertyCollection;
    }

    function doesResourceExist($resourceIdentifier, $resourceService)
    {
        try
        {
            $resourceService->GetResourceContent($resourceIdentifier);
        }
        catch (MgResourceNotFoundException $e)
        {
            return false;
        }
        return true;
    }

    function addLayerDefinitionToMap($layerDefinition, $layerName, $layerLegendLabel, $sessionId, $resourceService, &$map)
    {
        $byteSource = new MgByteSource($layerDefinition, strlen($layerDefinition));
        $byteSource->SetMimeType(MgMimeType::Xml);
        $resourceID = new MgResourceIdentifier("Session:$sessionId//$layerName.LayerDefinition");
        $resourceService->SetResource($resourceID, $byteSource->GetReader(), null);
        $newLayer = $this->addLayerResourceToMap($resourceID, $resourceService, $layerName, $layerLegendLabel, $map);
        return $newLayer;
    }
    //not used
    function addLayerToGroup($layer, $layerGroupName, $layerGroupLegendLabel, &$map)
    {
        $layerGroupCollection = $map->GetLayerGroups();
        if ($layerGroupCollection->Contains($layerGroupName))
        {
            $layerGroup = $layerGroupCollection->GetItem($layerGroupName);
        }
        else
        {
            $layerGroup = new MgLayerGroup($layerGroupName);
            $layerGroup->SetVisible(true);
            $layerGroup->SetDisplayInLegend(true);
            $layerGroup->SetLegendLabel($layerGroupLegendLabel);
            $layerGroupCollection->Add($layerGroup);
        }
        $layer->SetGroup($layerGroup);
    }
    //not used
    function addLayerResourceToMap($layerResourceID, $resourceService, $layerName, $layerLegendLabel, &$map)
    {
        $newLayer = new MgLayer($layerResourceID, $resourceService);
        $newLayer->SetName($layerName);
        $newLayer->SetVisible(true);
        $newLayer->SetLegendLabel($layerLegendLabel);
        $newLayer->SetDisplayInLegend(true);
        $layerCollection = $map->GetLayers();
        if (! $layerCollection->Contains($layerName) )
        {
            $layerCollection->Insert(0, $newLayer);
        }
        return $newLayer;
    }

    function createLayerDefinition($resourceId, $featureClass, $geometry, $featureClassRange)
    {
        $layerDef = file_get_contents(VIEWERFILES_DIR."/layerdefinition.templ");
        $layerDef = sprintf($layerDef, $resourceId, $featureClass, $geometry, $featureClassRange);
        return $layerDef;
    }

    function createLineRule($legendLabel, $filter, $color)
    {
        $lineRule = file_get_contents(VIEWERFILES_DIR."/linerule.templ");
        $lineRule = sprintf($lineRule, $legendLabel, $filter, $color);
        return $lineRule;
    }

    function createLineTypeStyle($lineRules)
    {
        $lineStyle = file_get_contents(VIEWERFILES_DIR."/linetypestyle.templ");
        $lineStyle = sprintf($lineStyle, $lineRules);
        return $lineStyle;
    }

    function createAreaRule($legendLabel, $filterText, $foreGroundColor)
    {
        $areaRule = file_get_contents(VIEWERFILES_DIR."/arearule.templ");
        $areaRule = sprintf($areaRule, $legendLabel, $filterText, $foreGroundColor);
        return $areaRule;
    }

    function createAreaTypeStyle($areaRules)
    {
        $style = file_get_contents(VIEWERFILES_DIR."/areatypestyle.templ");
        $style = sprintf($style, $areaRules);
        return $style;
    }

    function createScaleRange($minScale, $maxScale, $typeStyle)
    {
        $scaleRange = file_get_contents(VIEWERFILES_DIR."/scalerange.templ");
        $scaleRange = sprintf($scaleRange, $minScale, $maxScale, $typeStyle);
        return $scaleRange;
    }

    function createMarkSymbol($resourceId, $symbolName, $width, $height, $color)
    {
        $markSymbol = file_get_contents(VIEWERFILES_DIR."/marksymbol.templ");
        $markSymbol = sprintf($markSymbol, $width, $height, $resourceId, $symbolName, $color);
        return $markSymbol;
    }

    function createTextSymbol($text, $fontHeight, $foregroundColor)
    {
        $textSymbol = file_get_contents(VIEWERFILES_DIR."/textsymbol.templ");
        $textSymbol = sprintf($textSymbol, $fontHeight, $fontHeight, $text, $foregroundColor);
        return $textSymbol;
    }

    function createPointRule($legendLabel, $filter, $label, $pointSym)
    {
        $pointRule = file_get_contents(VIEWERFILES_DIR."/pointrule.templ");
        $pointRule = sprintf($pointRule, $legendLabel, $filter, $label, $pointSym);
        return $pointRule;
    }

    function createPointTypeStyle($pointRule)
    {
        $pointTypeStyle = file_get_contents(VIEWERFILES_DIR."/pointtypestyle.templ");
        $pointTypeStyle = sprintf($pointTypeStyle, $pointRule);
        return $pointTypeStyle;
    }

    function clearDataSource($featureSrvc, $dataSourceId, $featureName)
    {
        $deleteCmd = new MgDeleteFeatures($featureName, "KEY >= 0");
        $commands = new MgFeatureCommandCollection();
        $commands->Add($deleteCmd);
        $featureSrvc->UpdateFeatures($dataSourceId, $commands, false);
    }

    function createFeatureSource($layerName, $map, $featureService, $resourceIdentifier)
    {
        $classDefinition = new MgClassDefinition();
        $classDefinition->SetName($layerName);
        $geometryPropertyName="SHPGEOM";
        $classDefinition->SetDefaultGeometryPropertyName( $geometryPropertyName);

        $identityProperty = new MgDataPropertyDefinition("KEY");
        $identityProperty->SetDataType(MgPropertyType::Int32);
        $identityProperty->SetAutoGeneration(true);
        $identityProperty->SetReadOnly(true);
        $classDefinition->GetIdentityProperties()->Add($identityProperty);
        $classDefinition->GetProperties()->Add($identityProperty);

        /*$nameProperty = new MgDataPropertyDefinition("NAME");
        $nameProperty->SetDataType(MgPropertyType::String);
        $classDefinition->GetProperties()->Add($nameProperty);

        $objectIdProperty = new MgDataPropertyDefinition("OBJECTID");
        $objectIdProperty->SetDataType(MgPropertyType::Int32);
        $classDefinition->GetProperties()->Add($objectIdProperty);

        $prop = new MgDataPropertyDefinition("TOTAL");
        $prop->SetDataType(MgPropertyType::Double);
        $classDefinition->GetProperties()->Add($prop);*/

        $geometryProperty = new MgGeometricPropertyDefinition($geometryPropertyName);
        $geometryProperty->SetGeometryTypes(MgFeatureGeometricType::Surface);
        $classDefinition->GetProperties()->Add($geometryProperty);

        $featureSchema = new MgFeatureSchema("SHP_Schema","$layerName schema");
        $featureSchema->GetClasses()->Add($classDefinition);

        $wkt = $map->GetMapSRS();
        $sdfParams = new MgCreateSdfParams("spatial context", $wkt, $featureSchema);
        $featureService->CreateFeatureSource($resourceIdentifier, $sdfParams);
    }

    
    
    function createLayer($mapName, $layerName, $layerLegendLabel, $layerType)
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
        $sessionId = $this->mgTier->getMgSessionId();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $map = new MgMap();
        $map->Open($resourceService, $mapName);

        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);

        $featureSourceExists = $this->doesResourceExist($resourceIdentifier, $resourceService);

        if (! $featureSourceExists)
        {
            $this->createFeatureSource($layerName, $map, $featureService, $resourceIdentifier);
        }

        $layerExists = $this->doesLayerExist($layerName, $map);
        if (! $layerExists )
        {
            $featureName = $layerName;
            $geometry = 'SHPGEOM';
            $this->clearDataSource($featureService, $resourceIdentifier, $layerName);

            switch ($layerType) {

                case 'point':
                    $resourceId = 'Library:///symbols/test.SymbolLibrary';
                    $symbolName = 'test';
                    $width = '24';  // unit = points
                    $height = '24'; // unit = points
                    $color = 'FFFF0000';
                    $markSymbol = $this->createMarkSymbol($resourceId, $symbolName, $width, $height, $color);

                    $text = "KEY";
                    $fontHeight="12";
                    $foregroundColor = 'FF000000';
                    $textSymbol = $this->createTextSymbol($text, $fontHeight, $foregroundColor);

                    $legendLabel = 'trees';
                    $filter = '';
                    $pointRule = $this->createPointRule($legendLabel,$filter, $textSymbol, $markSymbol);

                    $pointTypeStyle = $this->createPointTypeStyle($pointRule);

                    $minScale = '0';
                    $maxScale = '1000000000000';
                    $scaleRange = $this->createScaleRange($minScale, $maxScale, $pointTypeStyle);
                    break;
                case 'line':
                    $ruleLegendLabel = 'Line Rule';
                    $filter = '';
                    $color = 'FFFF0000';
                    $factory = new LayerDefinitionFactory();
                    $lineRule = $this->createLineRule($ruleLegendLabel, $filter, $color);
                    $lineTypeStyle = $this->createLineTypeStyle($lineRule);

                    $minScale = '0';
                    $maxScale = '1000000000000';
                    $scaleRange = $this->createScaleRange($minScale, $maxScale, $lineTypeStyle);
                    
                    break;
                case 'polygon':
                    $areaRule1 = $this->createAreaRule('1 - 2000',     'SQFT &gt;= on 1 AND SQFT &lt; 2000', 'FFFFFF00');
                    $areaRule2 = $this->createAreaRule('2000 - 100000',  'SQFT &gt;= 2000 AND SQFT &lt; 100000',  'FFFFBF20');
                    $areaRule3 = $this->createAreaRule('100000 - 100000000000', 'SQFT &gt;= 100000 AND SQFT &lt; 100000000000', 'FFFF8040');

                    $areaTypeStyle = $this->createAreaTypeStyle($areaRule1 . $areaRule2 . $areaRule3);

                    $minScale = '0';
                    $maxScale = '1000000000000';
                    $scaleRange = $this->createScaleRange($minScale, $maxScale, $areaTypeStyle);

                    break;
            }
            $layerDefinition = $this->createLayerDefinition($featureSourceName, $layerName, $geometry, $scaleRange);
            $newLayer = $this->addLayerDefinitionToMap($layerDefinition, $layerName, $layerLegendLabel, $sessionId, $resourceService, $map);
        }

        $newLayer = $this->addLayerDefinitionToMap($layerDefinition, $layerName, $layerName, $sessionId, $resourceService, $map);
        //$this->addLayerToGroup($newLayer, "Analysis", "Analysis", $map);
        $layerCollection = $map->GetLayers();
        if ($layerCollection->Contains($layerName))
        {
            $layer =$layerCollection->GetItem($layerName);
            $layer->SetVisible(true);
        }
        
        /*$groupCollection = $map->GetLayerGroups();
        if ($groupCollection->Contains($groupName))
        {
          $analysisGroup =$groupCollection->GetItem($groupName);
          $analysisGroup->SetVisible(true);
        }*/
        
        $sessionIdName = "Session:$sessionId//$mapName.Map";
        $sessionResourceID = new MgResourceIdentifier($sessionIdName);
        $sessionResourceID->Validate();
        $map->Save($resourceService, $sessionResourceID);
    }

    function removeLayer($mapName, $layerName)
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
        $sessionId = $this->mgTier->getMgSessionId();
        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);
        $this->clearDataSource($featureService, $resourceIdentifier, $layerName);

        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $sessionIdName = "Session:$sessionId//$mapName.Map";
        $sessionResourceId = new MgResourceIdentifier($sessionIdName);
        $layerCollection = $map->GetLayers();
        if ($layerCollection->Contains($layerName))
        {
            $layer =$layerCollection->GetItem($layerName);
            $layerCollection->Remove($layer);
        }
        $map->Save($resourceService, $sessionResourceId);
    }
    
    
    

    function doDisplay()
    {
        $post = $this->request->getPost();
        if($post['line'] == 'true') {
            $this->createLayer($post['mapname'],$post['lineLayerName'], $post['lineLayerLegend'], 'line');
            
        } else
        $this->removeLayer($post['mapname'],$post['lineLayerName']);

        if($post['polygon'] == 'true')
        {
            $this->createLayer($post['mapname'],$post['polygonLayerName'], $post['polygonLayerLegend'], 'polygon');
            $this->addLine(12345,10048,4975,11418,4907);
        }
        else
        $this->removeLayer($post['mapname'],$post['polygonLayerName']);

        if($post['point'] == 'true')
        {
            $this->createLayer($post['mapname'],$post['pointLayerName'], $post['pointLayerLegend'], 'point');
        }
        else
        $this->removeLayer($post['mapname'],$post['pointLayerName']);
    }
}

?>