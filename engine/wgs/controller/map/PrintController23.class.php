<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
require_once('wgs/lib/tcpdf5/tcpdf.php');
require_once('FirePHPCore/fb.php');

class PrintController extends wgsFrontController
{
	private $headerWidth = 110;
	private $dpi = 108;
	private $pdfPictureWidth;
	private $wdth = 494;
	//private $hgth = 95*494/794;
	
	function getLayers($mapName)
	{
		if ($mapName) {
        	//$layersArray = array();
        	//$layersGroupArray = array();
        	$visibleArray = array();
        	
			$this->mgTier = lmbToolkit :: instance() -> getMgTier();
			$siteConnection = $this->mgTier->getSiteConnection();

			$map = new MgMap($siteConnection);

			$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
			
			$map->Open($resourceService, $mapName);
			
			$layers = $map->GetLayers();
			$layersCount = $layers->GetCount();
			
			for ($i = 0, $j = 0; $i < $layersCount; $i++) {
				$layer = $layers->GetItem($i);
				$a = false;
				if ($layer->isVisible()) {
					$definition = $layer->GetLayerDefinition();
        			$parentGroup = $layer->GetGroup();
        			if ($parentGroup == null) {
						$a = array(
							'name' =>  $layer->GetName(),
							'legend' =>  $layer->GetLegendLabel(),
							'definition' => $definition->GetPath().'/'.$definition->GetName().'.LayerDefinition'
						);
        			} else
        			if ($parentGroup->isVisible()) {
						$a = array(
							'name' =>  $layer->GetName(),
							'legend' =>  $layer->GetLegendLabel(),
							'definition' => $definition->GetPath().'/'.$definition->GetName().'.LayerDefinition'
						);
        			}
        			if ($a) {
        				$visibleArray[$j++] = $a;
        				//////////////////////////////////////////////////////////////////////////////
        				
        				$layerData = $resourceService->GetResourceContent($definition)->ToString();
        				
        				$xmldoc = DOMDocument::loadXML($layerData);
        				
        			    $type = 0;
					    $scaleRanges = $xmldoc->getElementsByTagName('VectorScaleRange');
					    if($scaleRanges->length == 0) {
					        $scaleRanges = $xmldoc->getElementsByTagName('GridScaleRange');
					        if($scaleRanges->length == 0) {
					            $scaleRanges = $xmldoc->getElementsByTagName('DrawingLayerDefinition');
					            if($scaleRanges->length == 0)
					                return;
					            $type = 2;
					        }
					        else
					            $type = 1;
					    }
        				
					    $typeStyles = array("PointTypeStyle", "LineTypeStyle", "AreaTypeStyle", "CompositeTypeStyle");
					    $ruleNames = array("PointRule", "LineRule", "AreaRule", "CompositeRule");
					    
					    for($sc = 0; $sc < $scaleRanges->length; $sc++)
					    {
					    	$scaleRange = $scaleRanges->item($sc);
					    	
					        if($type != 0)
					            break;
					
					        $styleIndex = 0;
					        for($ts=0, $count = count($typeStyles); $ts < $count; $ts++)
					        {
					            $typeStyle = $scaleRange->getElementsByTagName($typeStyles[$ts]);
					            $catIndex = 0;
					            for($st = 0; $st < $typeStyle->length; $st++)
					            {
	            	                // We will check if this typestyle is going to be shown in the legend
					                $showInLegend = $typeStyle->item($st)->getElementsByTagName("ShowInLegend");
					                if($showInLegend->length > 0)
					                    if($showInLegend->item(0)->nodeValue == "false")
					                        continue;   // This typestyle does not need to be shown in the legend
					
					                $rules = $typeStyle->item(0)->getElementsByTagName($ruleNames[$ts]);
					                for($r = 0; $r < $rules->length; $r++)
					                {
					                    $rule = $rules->item($r);
					                    $label = $rule->getElementsByTagName("LegendLabel");
					                    //$filter = $rule->getElementsByTagName("Filter");
					
					                    $labelText = $label->length==1? $label->item(0)->nodeValue: "";
					                    //$filterText = $filter->length==1? $filter->item(0)->nodeValue: "";
					                    
					                    //TODO: etc...
					                    $visibleArray[$j-1]['items'][$ts+1] = $labelText;
					                }
					            }					        	
					        }
					    	
					    	
					    }					    
					    
        			}
				}
			}
        	/*
			$layerGroups = $map->GetLayerGroups();
			$layerGroupsCount = $layerGroups->GetCount();
			
			for ($i = 0; $i < $layerGroupsCount; $i++) {
				$layerGroup = $layerGroups->GetItem($i);
				if ($layerGroup->isVisible()) {
					$visibleArray[] = sprintf("%u",crc32($layerGroup->GetName()));
				}
			}
			*/
			//echo json_encode(array('l' => $layersArray, 'lg' => $layersGroupArray));
			
			return $visibleArray;
        }
	}
	
	function putLegend(&$pdf, $legendWidth, $cm, $DPI, $sessionId, $orgScale, $mapName)
	{
		$this->mgTier = lmbToolkit :: instance() -> getMgTier();
			
		$siteConnection = $this->mgTier->getSiteConnection();
		$map = new MgMap($siteConnection);
		$resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
		$map->Open($resourceService, $mapName);$layers = $map->GetLayers();       
        
		$layerGroupCollection = $map->GetLayerGroups();	    
		$name = null;		
		foreach($_SESSION['themeNames'] as $tn){
			if ($layerGroupCollection->Contains($tn['name'])){
				$name = $tn['title'];							
				break;
			}
		}
		//
		$layers = $this->getLayers($mapName);
		//$firephp = FirePHP::getInstance(true);
		//file_put_contents("d:/12313213.txt", print_r($layers, true));
		//$firephp -> log('HELLO');( $pdfPictureWidth > $wdth+40 ? ($pdfPictureWidth -$wdth*72/$DPI)/2+5 : 40*72/$DPI+5), 20, ( $pdfPictureWidth  > $wdth+40 ? $wdth*72/$DPI : $pdfPictureWidth  - 40*72/$DPI), ( $pdfPictureWidth > $wdth+40 ? $hght*72/$DPI : ($pdfPictureWidth  - 10)/$wdth*$hght)
		$pdf->SetFont('arial','',9);
		$l = ( ($this->pdfPictureWidth > $this->wdth+40) ? (($this->pdfPictureWidth -$this->wdth)/2+5) : (40*72/$this->dpi+5));
		$pdf->SetXY($l, ($this->headerWidth-25) *72/$this->dpi);//$this -> headerWidth * 72/$this -> dpi -100);//5.35 * $cm );
		//$pdf->setFont()
		$pdf->MultiCell($this->pdfPictureWidth - 40, 20, $name, 0, 'L', false, 1, '', '', true, 0, false, false, 0, 'T', false);
			$pdf->SetXY(0.35 * $cm, $this -> headerWidth * 72/$this -> dpi +1);//5.35 * $cm );
		$pdf->SetFont('arial','',7);
		
			
		foreach ($layers as $layer) {
		
			$layerDefinition = urlencode($layer['definition']);
			
			if (count($layer['items']) < 2) {
			
				$file = "/mapguide/mapagent/mapagent.fcgi".
				"?OPERATION=GETLEGENDIMAGE".
				"&SESSION=$sessionId".
				"&VERSION=1.0.0".
				"&SCALE=$orgScale".
				"&LAYERDEFINITION=Session%3A$sessionId%2F%2F$layerDefinition".
				"&THEMECATEGORY=-1".
				"&TYPE=-1".
				"&CLIENTAGENT=Ajax%20Viewer";
			
				$image = file_get_contents("http://"."127.0.0.1".":".$this->server['SERVER_PORT'].$file);
				
				$fmd5 = md5($sessionId.$layer['definition']);
				
				file_put_contents(WGS3_PRINTCACHE_DIR."$fmd5.png", $image);
		        $pdf->SetY($pdf->GetY()+2);
		        $pdf->SetX(0.35 * $cm);
		        $pdf->Image(WGS3_PRINTCACHE_DIR."$fmd5.png", '', '', 16, 16, '', '', '', true, $DPI);
		        $pdf->SetY($pdf->GetY()+1);
		        $pdf->SetX(0.35 * $cm + 17);
		        $pdf->MultiCell((($legendWidth-27)*72/$DPI), 16, $layer['legend'], 0, 'L', false, 1, '', '', true, 0, false, false, 0, 'T', false);
		        
		        $pdf->Line($pdf->GetX() + 0.35 * $cm, $pdf->GetY()+1, $pdf->GetX()+(($legendWidth+0.35 * $cm)*72/$DPI), $pdf->GetY()+1);
		        
		        $pdf->SetX(0.35 * $cm);
				@unlink(WGS3_PRINTCACHE_DIR."$fmd5.png");
			} else {
				$pdf->SetY($pdf->GetY()+3);
				$pdf->SetX(0.26 * $cm);
				$pdf->MultiCell((($legendWidth+5)*72/$DPI), 16, $layer['legend'], 0, 'L', false, 1, '', '', true, 0, false, false, 0, 'T', false);
				$pdf->SetX(0.35 * $cm);
				foreach ($layer['items'] as $ts => $legend) {
					$file = "/mapguide/mapagent/mapagent.fcgi".
					"?OPERATION=GETLEGENDIMAGE".
					"&SESSION=$sessionId".
					"&VERSION=1.0.0".
					"&SCALE=$orgScale".
					"&LAYERDEFINITION=Session%3A$sessionId%2F%2F$layerDefinition".
					"&THEMECATEGORY=0".
					"&TYPE=$ts".
					"&CLIENTAGENT=Ajax%20Viewer";				
					$image = file_get_contents("http://"."127.0.0.1".":".$this->server['SERVER_PORT'].$file);
					
					$fmd5 = md5($sessionId.$layer['definition'].$ts);
					
					file_put_contents(WGS3_PRINTCACHE_DIR."$fmd5.png", $image);
		
			        $pdf->Image(WGS3_PRINTCACHE_DIR."$fmd5.png", '', '', 16, 16, '', '', '', true, $DPI);
			        $pdf->SetX(0.35 * $cm + 17);
			        $pdf->MultiCell((($legendWidth-27)*72/$DPI), 16, $legend, 0, 'L', false, 1, '', '', true, 0, false, false, 0, 'T', false);
			        $pdf->SetX(0.35 * $cm);
			        @unlink(WGS3_PRINTCACHE_DIR."$fmd5.png");
				}
				$pdf->SetX(0.35 * $cm);
				$pdf->Line($pdf->GetX(), $pdf->GetY()+1, $pdf->GetX()+(($legendWidth-0.15*$cm)*72/$DPI), $pdf->GetY()+1);
			}
	        
		}
			

		/*
        $pdf->SetXY(0.35 * $cm, 0.35 * $cm);
        //$pdf->Cell(24, 16, "M", 1, 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Image("d:/test1.png", '', '', 16, 16, '', '', '', true, $DPI);
        $pdf->SetX(0.35 * $cm + 17);
        $pdf->MultiCell((($legendWidth-27)*72/$DPI), 16, iconv("windows-1251", "UTF-8", "    "), 0, 'L', false, 1, '', '', true, 0, false, true, 0, 'T', false);
        $pdf->SetX(0.35 * $cm);
*/
        //$pdf->Cell(24, 16, "M", 1, 0, 'R', false, '', 0, false, 'T', 'T');
        /*
        $pdf->Image("d:/test1.png", '', '', 16, 16, '', '', '', true, $DPI);
        $pdf->SetX(0.35 * $cm + 17);
        $pdf->MultiCell((($legendWidth-27)*72/$DPI), 16, iconv("windows-1251", "UTF-8", "  2"), 0, 'L', false, 1, '', '', true, 0, false, true, 0, 'T', false);
		*/
	}
		
    function doDisplay()
    {
        $this->server = $_SERVER;
        $printSizes = array(
            '5' => array(0.148, 0.210),
            '4' => array(0.210, 0.297),
            '3' => array(0.297, 0.420),
            '2' => array(0.420, 0.594),
            '1' => array(0.594, 0.840),
            //'0' => array(1.188, 1.680)
            //'0' => array(0.841, 1.189)
        	'0' => array(0.840, 1.188)
        );
        $DPI = 108;
        $metersPerPixel = 0.0254 / $DPI;
        $centerX = $this->request->getPost('centerX');
        $centerY = $this->request->getPost('centerY');
        $orgScale = $this->request->getPost('orgScale');
        $orientation = $this->request->getPost('orientation');
        $printSize = $this->request->getPost('printSize');
		$pageWidth = $this->request->getPost('width');
        $legendWidth = $this->request->getPost('legendWidth')*72/$DPI;
		
        $mapName = $this->request->getPost('mapName');
        $sessionId = $this->request->getPost('sessionId');
        $bottom = $this->request->getPost('bottom');
        $act = $this->request->getPost('act');
        //$legendWidth = $legendWidth? 150: 0;
        $seq = rand();
        $headerL = 110;
		$bottomL = 140;
		$rightL = 60;
        if (!$centerX || !$centerY || !$orgScale || !$orientation || !$mapName || !$sessionId) {
            return;
        }
        //changes
        if ($orientation == '1') {
            $width = round($printSizes[$printSize][1] / $metersPerPixel) - round($legendWidth) - $rightL;
            $height = round($printSizes[$printSize][0] / $metersPerPixel) - $headerL - $bottomL;
            $pdfWidth = round($printSizes[$printSize][1] / (0.0254 / 72));// - round($legendWidth);
            $pdfHeight = round($printSizes[$printSize][0] / (0.0254 / 72));
			$pdfPictureHeight = round($printSizes[$printSize][0] / (0.0254 / 72) - ($headerL + $bottomL) *72/$DPI);
			$pdfPictureWidth = round($printSizes[$printSize][1] / (0.0254 / 72) - ($rightL) *72/$DPI);
        } else {
            $width = round($printSizes[$printSize][0] / $metersPerPixel) - round($legendWidth) - $rightL;
            $height = round($printSizes[$printSize][1] / $metersPerPixel) - $headerL- $bottomL;
            $pdfWidth = round($printSizes[$printSize][0] / (0.0254 / 72));// - round($legendWidth);
            //$pdfHeight = round($printSizes[$printSize][1] / (0.0254 / 72) - $header *$DPI/72);
			$pdfHeight = round($printSizes[$printSize][1] / (0.0254 / 72));
			$pdfPictureHeight = round($printSizes[$printSize][1] / (0.0254 / 72) - ($headerL+$bottomL) *72/$DPI);
			$pdfPictureWidth = round($printSizes[$printSize][0] / (0.0254 / 72) - ($rightL) *72/$DPI);
        }
		
		$this -> pdfPictureWidth = $pdfPictureWidth;
        $query   = "MAPNAME=$mapName&OPERATION=GETVISIBLEMAPEXTENT&SEQ=0.256{$seq}265&SESSION=$sessionId&SETDISPLAYDPI=$DPI&SETDISPLAYHEIGHT=$height&SETDISPLAYWIDTH=$width&SETVIEWCENTERX=$centerX&SETVIEWCENTERY=$centerY&SETVIEWSCALE=$orgScale&VERSION=1.0.0";        
        $header  = '';
        $header .= "POST /mapguide/mapagent/mapagent.fcgi HTTP/1.1\r\n";
        $header .= "Host: ". "127.0.0.1" .":".$this->server['SERVER_PORT']."\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-length: " . strlen($query) . "\r\n";
        $header .= "Connection: keep-alive\r\n\r\n";
        $socket = fsockopen("127.0.0.1",$this->server['SERVER_PORT']);
        if (!$socket)
        {
            echo '?????? ???????? ??????<BR>';
            return;
        }
        fputs($socket, $header.$query);
        while (!feof($socket)) {
            fgets($socket, 4096);
        }
        fclose($socket);
        $fpw = @fopen(WGS3_PRINTCACHE_DIR."map-$seq.png","w+");
        if ($fpr = @fopen("http://"."127.0.0.1".":".$this->server['SERVER_PORT']."/mapguide/mapagent/mapagent.fcgi?OPERATION=GETDYNAMICMAPOVERLAYIMAGE&FORMAT=PNG&VERSION=1.0.0&SESSION=$sessionId&MAPNAME=$mapName&SEQ=0.256{$seq}256", "rb")) {
            while (!@feof ($fpr)) {
                $buffer = @fread($fpr, 4096);  
                @fwrite($fpw, $buffer); 
            }
            @fclose ($fpr); 
        }
        @fclose ($fpw);
        ///////////////////////LEGEND//////////////////////////////
        $cm = 0.01 / (0.0254 / $DPI);
        $legendHeight = $height - 0.9 * $cm;
        /*
        $fpw = @fopen(WGS3_PRINTCACHE_DIR."legend-$seq.png","w+");
        if ($fpr = @fopen("http://"."127.0.0.1".":".$this->server['SERVER_PORT']."/mapguide/mapagent/mapagent.fcgi?OPERATION=GETMAPLEGENDIMAGE&VERSION=1.0.0&SESSION=$sessionId&MAPNAME=$mapName&WIDTH=$legendWidth&HEIGHT=$legendHeight&FORMAT=PNG", "rb")) {
            while (!@feof ($fpr)) {
                $buffer = @fread($fpr, 4096);  
                @fwrite($fpw, $buffer); 
            }
            @fclose ($fpr);
        }
        @fclose ($fpw);
		*/
        ///////////////////////SCALER//////////////////////////////
        $scalerDPI = 96;
        $metersPerPixel = 0.0254 / $scalerDPI;
        $cm = 0.01 / $metersPerPixel;
        $scalerWidth = 6 * $cm;
        $scalerHeight = 1.2 * $cm;
        $background = array(
            0, 0,
            6 * $cm, 0,
            6 * $cm , 1.2 * $cm,
            0, 1.2 * $cm
        );
        $scaleBackground = array(
            0.5 * $cm, 0.75 * $cm,
            5.5 * $cm + 1, 0.75 * $cm,
            5.5 * $cm + 1, 0.5 * $cm,
            0.5 * $cm, 0.5 * $cm
        );
        // create image
        $imageBorder = imagecreatetruecolor($pdfPictureWidth+2, $pdfPictureHeight+2);
        
		$image = imagecreatetruecolor($scalerWidth, $scalerHeight);
        // some colors      
        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);
        // create backgrounds
        $border = array(
            0, 0,
            $pdfPictureWidth+2, 0,
            $pdfPictureWidth+2, $pdfPictureHeight+2,
            0, $pdfPictureHeight+2
        );
		$borderInner = array(
            1, 1,
            $pdfPictureWidth, 1,
            $pdfPictureWidth, $pdfPictureHeight,
            1, $pdfPictureHeight
        );
		imagefilledpolygon($imageBorder, $border, 4, $black);
        imagefilledpolygon($imageBorder, $borderInner, 4, $white);
        
		imagepng($imageBorder, WGS3_PRINTCACHE_DIR."background-$seq.png", 9);
        
		imagefilledpolygon($image, $background, 4, $white);
        imagefilledpolygon($image, $scaleBackground, 4, $black);
        // create scaler
        $s1 = array(
            0.5 * $cm + 1, 0.75 * $cm - 1,
            1.5 * $cm, 0.75 * $cm - 1,
            1.5 * $cm, 0.5 * $cm + 1,
            0.5 * $cm + 1, 0.5 * $cm + 1
        );
        imagefilledpolygon($image, $s1, 4, $white);
        $s2 = array(
            2.5 * $cm + 1, 0.75 * $cm - 1,
            3.5 * $cm, 0.75 * $cm - 1,
            3.5 * $cm, 0.5 * $cm + 1,
            2.5 * $cm + 1, 0.5 * $cm + 1
        );
        imagefilledpolygon($image, $s2, 4, $white);
        $s3 = array(
            4.5 * $cm + 1, 0.75 * $cm - 1,
            5.5 * $cm, 0.75 * $cm - 1,
            5.5 * $cm, 0.5 * $cm + 1,
            4.5 * $cm + 1, 0.5 * $cm + 1
        );
        imagefilledpolygon($image, $s3, 4, $white);
        // create labels
        if (100000 <= $orgScale) {
            $step = round($orgScale / 100000, 1);
            $metric = 'KM';
        } else {
            $step = round($orgScale / 100, 1);
            $metric = 'M';
        }
        $fontWidth = imagefontwidth(1);
        $fontHeight = imagefontheight(1);
        imagestring($image, 1, 0.5 * $cm-$fontWidth/2, 0.85 * $cm, "0", $black);
        imagestring($image, 1, 1.5 * $cm-($fontWidth*strlen((string)$step))/2, 0.85 * $cm, $step, $black);
        imagestring($image, 1, 2.5 * $cm-($fontWidth*strlen((string)(2*$step)))/2, 0.85 * $cm, (2*$step), $black);
        imagestring($image, 1, 3.5 * $cm-($fontWidth*strlen((string)(3*$step)))/2, 0.85 * $cm, (3*$step), $black);
        imagestring($image, 1, 4.5 * $cm-($fontWidth*strlen((string)(4*$step)))/2, 0.85 * $cm, (4*$step), $black);
        imagestring($image, 1, 5.5 * $cm-($fontWidth*strlen((5*$step).$metric))/2, 0.85 * $cm, (5*$step).$metric, $black);
        // create scale
        imagestring($image, 1, 3.0 * $cm-($fontWidth*strlen("1 : $orgScale"))/2, 0.15 * $cm, "1 : $orgScale", $black);
        // flush image
        imagepng($image, WGS3_PRINTCACHE_DIR."scaler-$seq.png", 9);
        imagedestroy($image);
		$month = array(1=>'������',2=>'�������',3=>'�����',4=>'������',5=>'���',6=>'����',7=>'����',8=>'�������',9=>'��������',10=>'�������',11=>'������',12=>'�������');
		$imageDate = imagecreatetruecolor($scalerWidth, $scalerHeight);
         $background = array(
            0, 0,
            6 * $cm, 0,
            6 * $cm , 1.2 * $cm,
            0, 1.2 * $cm
        );
        
		$imageDate = imagecreatetruecolor(190, $scalerHeight);
        // create backgrounds
        imagefilledpolygon($imageDate, $background, 4, $white);
		imagettftext($imageDate, 8, 0, 0.5 * $cm, 1 * $cm, $black, FONT_DIR . 'arial.ttf', date("d").' '. ($month[date("m")] == null ? '������' : $month[date("m")]). ' ' .date("Y").' �.  '.date("H :i :s"));
        
		imagepng($imageDate, WGS3_PRINTCACHE_DIR."date-$seq.png", 9);
        
		//$this-> doTest();
        ///////////////////////PDFlib//////////////////////////////
        /*$p = new PDFlib();
        if ($p->begin_document("", "") == 0) {
            die("Error: " . $p->get_errmsg());
        }
        if ($orientation == '1') {
            $p->begin_page_ext(0, 0, "width=a$printSize.height height=a$printSize.width");
        } else {
            $p->begin_page_ext(0, 0, "width=a$printSize.width height=a$printSize.height");
        }

        if ($legendWidth) {
            $img = $p->load_image('PNG', WGS3_PRINTCACHE_DIR."legend-$seq.png", "");
            $p->fit_image($img, 0.35 * $cm, 0.35 * $cm, "dpi=$DPI");
        }
        
        $img = $p->load_image('PNG', WGS3_PRINTCACHE_DIR."map-$seq.png", "");
        $p->fit_image($img, ($legendWidth*72/$DPI)+20, 0, "dpi=$DPI");

        $img = $p->load_image('PNG', WGS3_PRINTCACHE_DIR."scaler-$seq.png", "");
        $p->fit_image($img, $pdfWidth - ($scalerWidth*72/$scalerDPI) - 0.35 * $cm, 0.35 * $cm, "dpi=$scalerDPI");
        
        $p->end_page_ext("");
        $p->end_document("");
        $buf = $p->get_buffer();
        file_put_contents(WGS3_PRINTCACHE_DIR."map-$seq.pdf",$buf);
        $p->delete();*/
        //////////////////////TCPDF////////////////////////////////
        $PDF_PAGE_ORIENTATION = ($orientation == '1')? "L": "P";
        $PDF_UNIT = "pt";
        $PDF_PAGE_FORMAT = "A$printSize";
        $pdf = new TCPDF($PDF_PAGE_ORIENTATION, $PDF_UNIT, $PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(0, 0, 0); 
        $pdf->SetHeaderMargin(0); 
        $pdf->SetFooterMargin(0); 
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->setPrintHeader(false); 
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        
        if ($legendWidth) {
            //$pdf->Image(WGS3_PRINTCACHE_DIR."legend-$seq.png", 0.35 * $cm, 0.35 * $cm, ($legendWidth*72/$DPI), ($legendHeight*72/$DPI), '', '', '', true, $DPI);
            $this->putLegend($pdf, $legendWidth, $cm, $DPI, $sessionId, $orgScale, $mapName);
        }
        $pdf->Image(WGS3_PRINTCACHE_DIR."background-$seq.png", ($legendWidth*72/$DPI)+19, $headerL*72/$DPI-1, $pdfPictureWidth-($legendWidth*72/$DPI)+2, $pdfPictureHeight+2, '', '', '', true, $DPI);
		
		$pdf->Image(WGS3_PRINTCACHE_DIR."map-$seq.png", ($legendWidth*72/$DPI)+20, $headerL*72/$DPI, $pdfPictureWidth-($legendWidth*72/$DPI), $pdfPictureHeight, '', '', '', true, $DPI);
		
        if ($bottom) {
        	/* for SVK project only */
        	//TODO: ?????, ??? PrintBottomController, ??? ??? ??
        	$lw = ($legendWidth*72/$DPI);
        	//$bpw = 1142;
        	//$bph = 224;
        	//$bpw = 1222;
        	//$bph = 149;
        	$bpw = 936;
        	$bph = 116;

        	$tpw = 455;
        	$tph = 19;
        	
        	$bw = ($bpw*72/$DPI);
        	$bh = ($bph*72/$DPI);

        	$tw = ($tpw*72/$DPI);
        	$th = ($tph*72/$DPI);
        	
        	$bx = 13;
        	$by = $pdfHeight - $bh;
        	
        	$pdf->Image(PROJECT_PATH."/bottom.png", $bx, $by-10, $bw, $bh, '', '', '', true, $DPI);
        	$pdf->Image(PROJECT_PATH."/top.png", ($pdfWidth-$tw)/2, 10, $tw, $th, '', '', '', true, $DPI);
        	$pdf->Image(WGS3_PRINTCACHE_DIR."scaler-$seq.png", $pdfWidth - ($scalerWidth*72/$scalerDPI) - 0.35 * $cm, ($scalerHeight*72/$scalerDPI) - (0.35 * $cm), ($scalerWidth*72/$scalerDPI), ($scalerHeight*72/$scalerDPI), '', '', '', true, $scalerDPI);
        }
        else
        if ($act) {
        	/* for SVK project only */
        	//TODO: ?????, ??? PrintBottomController, ??? ??? ??
        	$lw = ($legendWidth*72/$DPI);
        	//$bpw = 1142;
        	//$bph = 224;
        	//$bpw = 1222;
        	//$bph = 149;
        	$bpw = 750;
        	$bph = 152;

        	$tpw = 791;
        	$tph = 21;
        	
        	$bw = ($bpw*72/$DPI);
        	$bh = ($bph*72/$DPI);

        	$tw = ($tpw*72/$DPI);
        	$th = ($tph*72/$DPI);
        	
        	$bx = 13;
        	$by = $pdfHeight - $bh;
        	
        	$pdf->Image(PROJECT_PATH."/bottomact1.png", ($pdfWidth-$bw)-$bx, $by-10, $bw, $bh, '', '', '', true, $DPI);
        	$pdf->Image(PROJECT_PATH."/topact.png", ($pdfWidth-$tw)/2, 5, $tw, $th, '', '', '', true, $DPI);
        	$pdf->Image(WGS3_PRINTCACHE_DIR."scaler-$seq.png", $pdfWidth - ($scalerWidth*72/$scalerDPI) - 0.35 * $cm, ($scalerHeight*72/$scalerDPI) - (0.35 * $cm), ($scalerWidth*72/$scalerDPI), ($scalerHeight*72/$scalerDPI), '', '', '', true, $scalerDPI);        	        
        }
        if (!$bottom && !$act) {
		//imagettftext($im, 12, 0, 12, 15, $black, $font, $text);		
        	$pdf->Image(WGS3_PRINTCACHE_DIR."scaler-$seq.png", ($legendWidth*72/$DPI)+20,/*$pdfWidth - ($scalerWidth*72/$scalerDPI) - 0.35 * $cm,*/ $pdfHeight - ($scalerHeight*72/$scalerDPI) - (0.35 * $cm), ($scalerWidth*72/$scalerDPI), ($scalerHeight*72/$scalerDPI), '', '', '', true, $scalerDPI);
			$pdf->Image(WGS3_PRINTCACHE_DIR."date-$seq.png", $pdfWidth - (190*72/$scalerDPI) -20,/*$pdfWidth - ($scalerWidth*72/$scalerDPI) - 0.35 * $cm,*/ $pdfHeight - ($scalerHeight*72/$scalerDPI) - (0.35 * $cm), ($scalerWidth*72/$scalerDPI), ($scalerHeight*72/$scalerDPI), '', '', '', true, $scalerDPI);
        }
		$wdth = 496;
		$hght = $wdth / 744 * 95;
		$pdf->Image(WGS3_PRINTCACHE_DIR."header.png", ( $pdfPictureWidth > $wdth+40 ? ($pdfPictureWidth -$wdth*72/$DPI)/2+5 : 40*72/$DPI+5), 10, ( $pdfPictureWidth  > $wdth+40 ? $wdth*72/$DPI : $pdfPictureWidth  - 40*72/$DPI), ( $pdfPictureWidth > $wdth+40 ? $hght*72/$DPI : ($pdfPictureWidth  - 10)/$wdth*$hght), '', '', '', true, $DPI);
		$pdf->Image(WGS3_PRINTCACHE_DIR."narrow.png", $pdfWidth -100, $pdfHeight - 85, 30, 40, '', '', '', true, $DPI);

        ///////////TABLE/////////////
        /*
        $header = array('Country', 'Capital', 'Area (sq km)', "????�???\n???? \n???);
        $pdf->SetFont('dejavusans', '', 12); 
        //$pdf->SetFillColor(255, 0, 0); 
        $pdf->SetTextColor(255, 0, 0); 
        $pdf->SetDrawColor(128, 0, 0); 
        $pdf->SetLineWidth(0.3); 
        //$pdf->SetFont('', 'B'); 
        
        $w = array(40, 35, 40, 145); 
        for($i = 0; $i < count($header); $i++) 
        $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1); 
        $pdf->Ln();
        */ 
        /////////////////////////////
        
        $buf = $pdf->Output("map-$seq.pdf", 'S');
        file_put_contents(WGS3_PRINTCACHE_DIR."map-$seq.pdf",$buf);
        ///////////////////////////////////////////////////////////
        //@unlink(WGS3_PRINTCACHE_DIR."legend-$seq.png");
        @unlink(WGS3_PRINTCACHE_DIR."map-$seq.png");
        @unlink(WGS3_PRINTCACHE_DIR."scaler-$seq.png");
        @unlink(WGS3_PRINTCACHE_DIR."date-$seq.png");
		@unlink(WGS3_PRINTCACHE_DIR."background-$seq.png");
        
        
		echo $seq;
    }
    
    function doGetPdf()
    {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."map-$seq.pdf");
        $len = strlen($buf);
        $this->response->header("Content-type: application/pdf");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: inline; filename=map-$seq.pdf");
        $this->response->write($buf);
        @unlink(WGS3_PRINTCACHE_DIR."map-$seq.pdf");
    }
}

?>