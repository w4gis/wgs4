<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/controller/MapController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class GoogleIntegrationController extends wgsFrontController
{
    private $kml;
    
    function EmitGeometry($geometry, $extrude, $ht)
    {
        if ($geometry != null)
        {
            if ($geometry->GetGeometryType() == MgGeometryType::Polygon)
            {
                $this->EmitPolygon($geometry, $extrude, $ht);
            }
            if ($geometry->GetGeometryType() == MgGeometryType::MultiPolygon)
            {
                $this->EmitMultiPolygon($geometry, $extrude, $ht);
            }
            if ($geometry->GetGeometryType() == MgGeometryType::LineString)
            {
                $this->EmitLineString($geometry, /*$ht*/rand()*0.01);
            }
            if ($geometry->GetGeometryType() == MgGeometryType::Point)
            {
                $this->EmitPoint($geometry, $ht);
            }
            
        }
    }
    
    function StartEmitConsolidatedGeometry()
    {
        $this->kml .= '<GeometryCollection>';
    }

    function WriteConsolidatedGeometryRing($geometry, $extrude, $ht)
    {
        //EmitPolygon($geometry, $extrude, $ht);
        $this->EmitGeometry($geometry, $extrude, $ht);
    }   
    
    function EndEmitConsolidatedGeometry()
    {
        $this->kml .= '</GeometryCollection>';
    }
    
    function EmitPolygon($geometry, $extrude, $ht)
    {
        if ($geometry != null)
        {
            $this->kml .= '<Polygon>';
    
            $this->kml .= '<extrude>'.$extrude.'</extrude>';
            $this->kml .= '<altitudeMode>relativeToGround</altitudeMode>';
            $this->kml .= '<outerBoundaryIs>';
    
            $this->EmitLinearRing($geometry, $geometry->GetExteriorRing(), $ht);
    
            $this->kml .= '</outerBoundaryIs>';
            $this->kml .= '</Polygon>';
        }
    }

    function EmitMultiPolygon($geometry, $extrude, $ht)
    {
        if ($geometry != null)
        {
            $this->kml .= '<GeometryCollection>';
    
            for ($i = 0; $i < $geometry->GetCount(); $i++)
            {
                $this->EmitPolygon($geometry->GetPolygon($i), $extrude, $ht);
            }
            $this->kml .= '</GeometryCollection>';
        }
    }
        
    function EmitLinearRing($geometry, $linearRing, $ht)
    {
        if ($linearRing != null)
        {
            $this->kml .= '<LinearRing>';
            $this->kml .= '<coordinates>';
        
            $coordIterCenter = $geometry->GetCentroid();
            $coordCenter = $coordIterCenter->GetCoordinate();
            
            /*$realCoord = $this->Vector($coordCenter->GetX(), $coordCenter->GetY());
            $la = $realCoord[0];
            $lo = $realCoord[1];*/
            // �������
            $la[0] = 84.74088847971734;
            $la[1] = 0.000016269888785;
            $la[2] = -0.000000829474707;
            
            $lo[0] = 56.5392231795189;
            $lo[1] = 0.000000457746382;
            $lo[2] = 0.000008977633709;
            
            $coordIter = $linearRing->GetCoordinates();
            
            while ($coordIter->MoveNext())
            {   
                $coord = $coordIter->GetCurrent();
                if ($coord != null)
                {
                    $lat = $la[0] + $la[1]*$coord->GetX() + $la[2]*$coord->GetY();
                    $lon = $lo[0] + $lo[1]*$coord->GetX() + $lo[2]*$coord->GetY();
                    
                    $this->kml .= $lat . ',' . $lon . ',' .$ht . ' ';
                    //$this->kml .= $coord->GetX() . ',' . $coord->GetY() . ',' .$ht . ' ';
                }
            }
            $this->kml .= '</coordinates>';
            $this->kml .= '</LinearRing>';
        }       
    }   
    
    function EmitLineString($lineString, $ht)
    {
        if ($lineString != null)
        {
            $this->kml .= '<MultiGeometry>';
			$this->kml .= '<LineString>';
            $this->kml .= '<coordinates>';
            
            /*$coordIterCenter = $lineString->GetCentroid();
            $coordCenter = $coordIterCenter->GetCoordinate();
            
            $realCoord = Vector($coordCenter->GetX(), $coordCenter->GetY());
            $la = $realCoord[0];
            $lo = $realCoord[1];*/
            /*
            $la[0] = 87.0656943920925;
            $la[1] = 0.000011333223974;
            $la[2] = -0.000010760208652;
            
            $lo[0] = 53.75621554941188;
            $lo[1] = 0.000006152312206;
            $lo[2] = 0.000006869051736;
            */
            // �������
            
            $la[0] = 84.74088847971734;
            $la[1] = 0.000016269888785;
            $la[2] = -0.000000829474707;
            
            $lo[0] = 56.5392231795189;
            $lo[1] = 0.000000457746382;
            $lo[2] = 0.000008977633709;
            
            $coordIter = $lineString->GetCoordinates();
            
            while ($coordIter->MoveNext())
            {   
                $coord = $coordIter->GetCurrent();
                if ($coord != null)
                {
                    $lat = $la[0] + $la[1]*$coord->GetX() + $la[2]*$coord->GetY();
                    $lon = $lo[0] + $lo[1]*$coord->GetX() + $lo[2]*$coord->GetY();
                    
                    $this->kml .= $lat . ',' . $lon . ',' .$coord->GetZ() . ' ';
                }
            }
            $this->kml .= '</coordinates>';
			$this->kml .= '<altitudeMode>relativeToGround</altitudeMode>';
            
            $this->kml .= '</LineString>';
            $this->kml .= '</MultiGeometry>';
        }       
    }  

    function EmitPoint($point, $ht)
    {
        if ($point != null)
        {
            $this->kml .= '<Point>';
            $this->kml .= '<coordinates>';
            //print_r($point);exit;
            /*$coordIterCenter = $lineString->GetCentroid();
            $coordCenter = $coordIterCenter->GetCoordinate();
            
            $realCoord = Vector($coordCenter->GetX(), $coordCenter->GetY());
            $la = $realCoord[0];
            $lo = $realCoord[1];*/
            /*
            $la[0] = 87.0656943920925;
            $la[1] = 0.000011333223974;
            $la[2] = -0.000010760208652;
            
            $lo[0] = 53.75621554941188;
            $lo[1] = 0.000006152312206;
            $lo[2] = 0.000006869051736;
            */
            // �������
            
            $la[0] = 84.74088847971734;
            $la[1] = 0.000016269888785;
            $la[2] = -0.000000829474707;
            
            $lo[0] = 56.5392231795189;
            $lo[1] = 0.000000457746382;
            $lo[2] = 0.000008977633709;
            
            $coordIter = $point->GetCoordinates();
            
            while ($coordIter->MoveNext())
            {   
                $coord = $coordIter->GetCurrent();
                if ($coord != null)
                {
                    $lat = $la[0] + $la[1]*$coord->GetX() + $la[2]*$coord->GetY();
                    $lon = $lo[0] + $lo[1]*$coord->GetX() + $lo[2]*$coord->GetY();
                    
                    $this->kml .= $lat . ',' . $lon . ',' .$ht . ' ';
                }
            }
            $this->kml .= '</coordinates>';
            $this->kml .= '</Point>';
        }       
    }   
    
    function Vector($x, $y)
    {
        $lat = array();
        $lon = array();
        
        // ��� �������� A1 
        if ( ($x >= -1515.8830 && $x < 484.1170) && ($y >= 1226.1415 && $y < 3226.1415) ) {
            $lat[0] = 87.09560124657992;
            $lat[1] = -0.000015978961139;
            $lat[2] = 0.000007257613703;
            
            $lon[0] = 53.75688936329334;
            $lon[1] = 0.000005915552947;
            $lon[2] = 0.000006448538828;
        }
        
        // ��� �������� A2 
        if ( ($x >= -1515.8830 && $x < 484.1170) && ($y >= -773.8585 && $y < 1226.1415) ) {
            $lat[0] = 87.06712541726495;
            $lat[1] = 0.000011911459505;
            $lat[2] = -0.00001089870932;
            
            $lon[0] = 53.75644843590999;
            $lon[1] = 0.000006084589732;
            $lon[2] = 0.000006496309482;
        }
        
        // ��� �������� A3 
        if ( ($x >= -1515.8830 && $x < 484.1170) && ($y >= -2773.8585 && $y < -773.8585) ) {
            $lat[0] = 87.06712541726495;
            $lat[1] = 0.000011911459505;
            $lat[2] = -0.00001089870932;
            
            $lon[0] = 53.75644843590999;
            $lon[1] = 0.000006084589732;
            $lon[2] = 0.000006496309482;
        }
        
        // ��� �������� B1 
        if ( ($x >= 484.1170 && $x < 2484.1170) && ($y >= 1226.1415 && $y < 3226.1415) ) {
            $lat[0] = 87.06675286579497;
            $lat[1] = 0.000010894756192;
            $lat[2] = -0.000008188685074;
            
            $lon[0] = 53.75672530963635;
            $lon[1] = 0.000006243502611;
            $lon[2] = 0.000005061143266;
        }
        
        // ��� �������� B21 
        if ( ($x >= 484.1170 && $x < 1484.1170) && ($y >= 226.1415 && $y < 1226.1415) ) {
            $lat[0] = 87.09560124657992;
            $lat[1] = -0.000015978961139;
            $lat[2] = 0.000007257613703;
            
            $lon[0] = 53.75688936329334;
            $lon[1] = 0.000005915552947;
            $lon[2] = 0.000006448538828;
        }
        
        // ��� �������� B22 
        if ( ($x >= 1484.1170 && $x < 2484.1170) && ($y >= 226.1415 && $y < 1226.1415) ) {
            $lat[0] = 87.06675286579497;
            $lat[1] = 0.000010894756192;
            $lat[2] = -0.000008188685074;
            
            $lon[0] = 53.75672530963635;
            $lon[1] = 0.000006243502611;
            $lon[2] = 0.000005061143266;
        }
        
        // ��� �������� B23 
        if ( ($x >= 484.1170 && $x < 1484.1170) && ($y >= -773.8585 && $y < 226.1415) ) {
            $lat[0] = 87.06728523487095;
            $lat[1] = 0.000010877831047;
            $lat[2] = -0.000010381126547;
            
            $lon[0] = 53.756609248590394;
            $lon[1] = 0.00000608365019;
            $lon[2] = 0.000006608102793;
        }
        
        // ��� �������� B24 
        if ( ($x >= 1484.1170 && $x < 2484.1170) && ($y >= -773.8585 && $y < 226.1415) ) {
            $lat[0] = 87.06718386153669;
            $lat[1] = 0.000011000296495;
            $lat[2] = -0.00001024751686;
            
            $lon[0] = 53.75625254905704;
            $lon[1] = 0.000006257924931;
            $lon[2] = 0.000006285121297;
        }
        
        // ��� �������� B3 
        if ( ($x >= 484.1170 && $x < 2484.1170) && ($y >= -2773.8585 && $y < -773.8585) ) {
            $lat[0] = 87.08949166253797;
            $lat[1] = 0.000013534214396;
            $lat[2] = -0.000003768414103;
            
            $lon[0] = 53.75644837618688;
            $lon[1] = 0.000006172401857;
            $lon[2] = 0.000006501594076;
        }
        
        // ��� �������� C1 
        if ( ($x >= 2484.1170 && $x < 4484.1170) && ($y >= 1226.1415 && $y < 3226.1415) ) {
            $lat[0] = 87.05625794605294;
            $lat[1] = 0.000014418635724;
            $lat[2] = -0.000013788130289;
            
            $lon[0] = 53.75548685657668;
            $lon[1] = 0.000006443868231;
            $lon[2] = 0.000006412792337;
        }
        
        // ��� �������� C21 
        if ( ($x >= 2484.1170 && $x < 3484.1170) && ($y >= 226.1415 && $y < 1226.1415) ) {
            $lat[0] = 87.07125891289343;
            $lat[1] = 0.000009403208796;
            $lat[2] = -0.000008913467192;
            
            $lon[0] = 53.75632704168129;
            $lon[1] = 0.000006231125671;
            $lon[2] = 0.000006476235616;
        }
        
        // ��� �������� C22 
        if ( ($x >= 3484.1170 && $x < 4484.1170) && ($y >= 226.1415 && $y < 1226.1415) ) {
            $lat[0] = 87.05625794605294;
            $lat[1] = 0.000014418635724;
            $lat[2] = -0.000013788130289;
            
            $lon[0] = 53.75548685657668;
            $lon[1] = 0.000006443868231;
            $lon[2] = 0.000006412792337;
        }
        
        // ��� �������� C23 
        if ( ($x >= 2484.1170 && $x < 3484.1170) && ($y >= -773.8585 && $y < 226.1415) ) {
            $lat[0] = 87.06644898069726;
            $lat[1] = 0.000011281604911;
            $lat[2] = -0.000010624408557;
            
            $lon[0] = 53.75670314447882;
            $lon[1] = 0.000006081905465;
            $lon[2] = 0.000006494875175;
        }
        
        // ��� �������� C24 
        if ( ($x >= 3484.1170 && $x < 4484.1170) && ($y >= -773.8585 && $y < 226.1415) ) {
            $lat[0] = 87.06261574881546;
            $lat[1] = 0.000012224751091;
            $lat[2] = -0.000011290104387;
            
            $lon[0] = 53.75672466147262;
            $lon[1] = 0.000006105184977;
            $lon[2] = 0.000006605595259;
        }
        
        // ��� �������� C3 
        if ( ($x >= 2484.1170 && $x < 4484.1170) && ($y >= -2773.8585 && $y < -773.8585) ) {
            $lat[0] = 87.06700258424212;
            $lat[1] = 0.000011069944784;
            $lat[2] = -0.000010340172595;
            
            $lon[0] = 53.75668164783656;
            $lon[1] = 0.000006119259455;
            $lon[2] = 0.000006577512678;
        }
        
        // ��� �������� D1 
        if ( ($x >= 4484.1170 && $x < 6484.1170) && ($y >= 1226.1415 && $y < 3226.1415) ) {
            $lat[0] = 87.0656943920925;
            $lat[1] = 0.000011333223974;
            $lat[2] = -0.000010760208652;
            
            $lon[0] = 53.75621554941188;
            $lon[1] = 0.000006152312206;
            $lon[2] = 0.000006869051736;
        }
        
        // ��� �������� D2 
        if ( ($x >= 4484.1170 && $x < 6484.1170) && ($y >= -773.8585 && $y < 1226.1415) ) {
            $lat[0] = 87.0656943920925;
            $lat[1] = 0.000011333223974;
            $lat[2] = -0.000010760208652;
            
            $lon[0] = 53.75621554941188;
            $lon[1] = 0.000006152312206;
            $lon[2] = 0.000006869051736;
        }
        
        // ��� �������� D3 
        if ( ($x >= 4484.1170 && $x < 6484.1170) && ($y >= -2773.8585 && $y < -773.8585) ) {
            $lat[0] = 87.06700258424212;
            $lat[1] = 0.000011069944784;
            $lat[2] = -0.000010340172595;
            
            $lon[0] = 53.75668164783656;
            $lon[1] = 0.000006119259455;
            $lon[2] = 0.000006577512678;
        }
        
        // ��� �������� E1 
        if ( ($x >= 6484.1170 && $x < 8484.1170) && ($y >= 1226.1415 && $y < 3226.1415) ) {
            $lat[0] = 87.0656943920925;
            $lat[1] = 0.000011333223974;
            $lat[2] = -0.000010760208652;
            
            $lon[0] = 53.75621554941188;
            $lon[1] = 0.000006152312206;
            $lon[2] = 0.000006869051736;
        }
        
        // ��� �������� E2 
        if ( ($x >= 6484.1170 && $x < 8484.1170) && ($y >= -773.8585 && $y < 1226.1415) ) {
            $lat[0] = 87.0656943920925;
            $lat[1] = 0.000011333223974;
            $lat[2] = -0.000010760208652;
            
            $lon[0] = 53.75621554941188;
            $lon[1] = 0.000006152312206;
            $lon[2] = 0.000006869051736;
        }
        
		$coord[0] = $lat;
        $coord[1] = $lon;
        return $coord;
    }

    function BasicKml($kmlnamelist, /*$basic_kml_name,*/ $domain)
    {
        $basic_kml =  '<?xml version="1.0" encoding="UTF-8"?>';
        $basic_kml .= '<kml xmlns="http://earth.google.com/kml/2.0">';
        $basic_kml .= '<Folder>';
        //$basic_kml .= '<name>MapGuide Network Links</name>';
        $basic_kml .= '<visibility>1</visibility>';
        $basic_kml .= '<open>0</open>';   
        foreach ($kmlnamelist as $kmlname)
        {
            $basic_kml .= '<NetworkLink>';
            //$basic_kml .= '<name>City of Wheaton - Municipal Boundary</name>';
            $basic_kml .= '<visibility>1</visibility>';
            $basic_kml .= '<open>1</open>';
            $basic_kml .= '<refreshVisibility>0</refreshVisibility>';
            $basic_kml .= '<flyToView>1</flyToView>';
            $basic_kml .= '<Url>';
            $basic_kml .= '<href>'.$domain.'/kmlcache/'.$kmlname.'</href>';
            $basic_kml .= '</Url>';
            $basic_kml .= '</NetworkLink>';      
        }    
        $basic_kml .= '</Folder>';
        $basic_kml .= '</kml>';
        
        $basic_kml_name = 'basic_'.md5(rand()).'.kml';
        $result = $this->SaveKml($basic_kml, $basic_kml_name);
        
        $this->delTemporaryFiles(WGS3_KMLCACHE_DIR);
        $this->response->write($result); 
    }
    
    function doGetFeatureReader()
    {
		$layerFeatureId = $this->request->getPost('layerFeatureId');        
		$featureClassName = $this->request->getPost('featureClassName');
		//$basic_kml_name = $this->request->getPost('kmlName');
		$domain = $this->request->getPost('domain');
		$layerColor = 'ff'.strrev($this->request->getPost('layerColor'));
        
		$this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);

        $kml_name = array();
        
        $layerFeatureResource = new MgResourceIdentifier($layerFeatureId); 
        $featureReader = $featureService->SelectFeatures($layerFeatureResource, $featureClassName, null);
       
        $kml_name[] = $this->GetKml($featureReader, $layerColor);
             
        if ($kml_name) $this->BasicKml($kml_name, /*$basic_kml_name,*/ $domain);
    }
    
    function GetKml($featureReader, $layerColor)
    {   
        $geometryReaderWriter = new MgAgfReaderWriter();
        
        $this->kml = '<?xml version="1.0" encoding="UTF-8"?>';
        $this->kml .= '<kml xmlns="http://earth.google.com/kml/2.0">';
        $this->kml .= '<Document>';
        $this->kml .= '<Style id="muniStyle">';
        $this->kml .= '<PolyStyle>';
        $this->kml .= '<outline>1</outline>';
        $this->kml .= '<fill>1</fill>';
        $this->kml .= '<color>70000000</color>';
        $this->kml .= '</PolyStyle>';
        $this->kml .= '<LineStyle>';
        //$this->kml .= '<color>ff0000ff</color>';
		$this->kml .= '<width>'.'2'.'</width>';
        $this->kml .= '<color>'.$layerColor.'</color>';
        $this->kml .= '</LineStyle>';
        $this->kml .= '</Style>';
        $this->kml .= '<Folder>';
        //$this->kml .= '<description>wgs3</description>';
        //$this->kml .= '<name>wgs3</name>';
        $this->kml .= '<visibility>1</visibility>';
        $this->kml .= '<open>0</open>';
        $this->kml .= '<Placemark>';
        //$this->kml .= '<name>wgs3</name>';
        //$this->kml .= '<description>wgs3</description>';
        $this->kml .= '<styleUrl>#muniStyle</styleUrl>';

        $this->StartEmitConsolidatedGeometry();
        try {
            while ($featureReader->ReadNext())
            {
                if (!$featureReader->IsNull('GEOMETRY')) {
                    $byteReader = $featureReader->GetGeometry('GEOMETRY');
					//var_dump($byteReader);
                    $ht = rand();//$featureReader->GetDouble('HEIGHT');
                    $geometry = $geometryReaderWriter->Read($byteReader);
                    $this->WriteConsolidatedGeometryRing($geometry,1,($ht*0.001));
                }
            }
        } catch(MgException $e) {}
        
        $featureReader->Close();
        $this->EndEmitConsolidatedGeometry();
        
        $this->kml .= '</Placemark>';
        $this->kml .= '</Folder>';
        $this->kml .= '</Document>';
        $this->kml .= '</kml>';
        
        $kmlname = md5(rand()).'.kml';
        $this->SaveKml($this->kml, $kmlname);
        
        return $kmlname;
    }

    function SaveKml($content, $kmlname)
    {
        $fpw = @fopen(WGS3_KMLCACHE_DIR.$kmlname,"w+");
          $nBuffer = 8192;
          for ($i=0; $i<strlen($content); $i+=$nBuffer)
            @fwrite($fpw,substr($content,$i,($i+$nBuffer)),$nBuffer);   
        @fclose ($fpw);
        return $kmlname;
    }
    
    function doGetLayerList()
    {
        $user         = $this->wgsTier->getAuthorizedUser();
        $acl          = $this->wgsTier->getACL();
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        
		$siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

        $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout();
        $featureLayerList = $this->wgsTier->getProjectObjectService()->getFeatureLayerList();
        
        $LayerArray = array();

        if ($mgMapLayerList = $defaultWebLayout->getMgMapLayers())
        {
            foreach ($mgMapLayerList as $mgMapLayer)
            {
                if ($featureLayerList)
                    foreach ($featureLayerList as $featureLayer)
                    {
                        if ($featureLayer->getId() == $mgMapLayer->getFeaturelayerId()) {
                            if ($acl->isAllowed($user, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS)) {
    
    							$mgMapLayerIdentify = $mgMapLayer->getMgResourceId();
                                $resourceIdentifier = new MgResourceIdentifier($mgMapLayerIdentify);
                                $MgLayer = new MgLayer ($resourceIdentifier, $resourceService);
        
                                $layerFeatureId = $MgLayer->GetFeatureSourceId();
                                $featureClassName = $MgLayer->GetFeatureClassName();
    
                                $groupId = $featureLayer->getFeatureLayerGroupId();
                                $LayerArray['root'][$groupId]['id'] = $groupId.'_'.md5(rand());
                                $LayerArray['root'][$groupId]['text'] = lmb_win1251_to_utf8($featureLayer->getFeatureLayerGroupName());
                                $LayerArray['root'][$groupId]['layer'] = false;
                                $LayerArray['root'][$groupId]['leaf'] = '';
                                $LayerArray['root'][$groupId]['iconCls'] = 'icon-layer-group';
                                $LayerArray['root'][$groupId]['children'][] = array(
                                    'id'  => $featureLayer->getObjectId(),
                                    'text' => lmb_win1251_to_utf8($featureLayer->getName()),
    								'layer'=> true ,
                                    'expanded' => false,
                                    'leaf'=>'true',
                                    'iconCls' => 'icon-layer',
    								'layerFeatureId' => $layerFeatureId,
    								'featureClassName' => $featureClassName,
    								'kmlObject' => false,
    								'layerColor' => 'FF6600'
                                );
                            }
                        }
                    }
                else {//print $mgMapLayer->getName();
                    //if ($acl->isAllowed($user, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS)) {
                        $mgMapLayerIdentify = $mgMapLayer->getMgResourceId();
                        $resourceIdentifier = new MgResourceIdentifier($mgMapLayerIdentify);
                        $MgLayer = new MgLayer ($resourceIdentifier, $resourceService);
        
                        $layerFeatureId = $MgLayer->GetFeatureSourceId();
                        $featureClassName = $MgLayer->GetFeatureClassName();
        
                        $groupId = 9878745;//$featureLayer->getFeatureLayerGroupId();
                        $LayerArray['root'][$groupId]['id'] = $groupId.'_'.md5(rand());
                        $LayerArray['root'][$groupId]['text'] = lmb_win1251_to_utf8('shp ����');//lmb_win1251_to_utf8($featureLayer->getFeatureLayerGroupName());
                        $LayerArray['root'][$groupId]['layer'] = false;
                        $LayerArray['root'][$groupId]['leaf'] = '';
                        $LayerArray['root'][$groupId]['iconCls'] = 'icon-layer-group';
                        $LayerArray['root'][$groupId]['children'][] = array(
                            'id'  => $mgMapLayer->getObjectId(),
                            'text' => $mgMapLayer->getName(),
                            'layer'=> true ,
                            'expanded' => false,
                            'leaf'=>'true',
                            'iconCls' => 'icon-layer',
                            'layerFeatureId' => $layerFeatureId,
                            'featureClassName' => $featureClassName,
                            'kmlObject' => false,
                            'layerColor' => 'FF6600'
                        );
                    }//}
            }
        }
		
        if (count($LayerArray) == 0) { 
            $LayerArray['root'][] = array(
                'id' => 'empty',
                'text' => lmb_win1251_to_utf8('��� �� ������ ���������� ����'), 
                'expanded' => true, 
                'leaf' => 'true',
                'iconCls' => 'icon-layer-group'
            );  
        } 
        
        $this->response->write(json_encode(array_merge($LayerArray['root'])));
    }
      
    //------------------------------------------������� ��������� ����� � �������------------------------------
    function delTemporaryFiles ($directory)
    {
        $dir = @opendir ($directory);
        while (( $file = @readdir ($dir)))
        { 
        if( is_file ($directory.$file))
        {
          $acc_time = filemtime ($directory."/".$file);
          $time =  time();
          if (($time - $acc_time) > 1296000) // 1296000 = 15 ����
          {
               @unlink ($directory.$file);
          }     
        }
        }
        @closedir ($dir);
    }   
    //---------------------------------------------------------------------------------------------------------
}

?>