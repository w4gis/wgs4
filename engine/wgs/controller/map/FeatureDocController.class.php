﻿<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
require_once('wgs/lib/fpdf/fpdf.class.php');
require_once('wgs/lib/PHPExcel/Classes/PHPExcel/IOFactory.php');

class PDF extends FPDF
{
//Page header

//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Times','',8);
        //Page number
        $this->Cell(0,10,'Страница '.$this->PageNo().'/{nb}',0,0,'R');
    }
}

class FeatureDocController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::FEATUREDOC_BIND);
    }
	function doImportFile()
    {
        $uploaddir = WGS3_DOCCACHE_DIR;
        $uploadfile = DOCCACHE_FULLDIR. basename($_FILES['file']['name']);

        $response = array();
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
			/** Load $inputFileName to a PHPExcel Object  **/
			$objPHPExcel = PHPExcel_IOFactory::load($uploadfile);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			//$r = var_dump($sheetData);
			$response['success'] = true;
			//$response['r'] = $uploadfile;
			for ($i=0;$i<count($sheetData[1]);$i++)
			
			$response['data'] = $sheetData;
		
        }
        else $response['success'] = false;
        echo json_encode($response);
    }
    function doUploadFile()
    {
        $uploaddir = WGS3_DOCCACHE_DIR;
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);

        $response = array();
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {

            //$entityId = $this->request->getPost('entityId');
            $entityIdList = json_decode($this->request->getPost('entityId'), true);

            $doc_name = encode(basename($_FILES['file']['name']));
            $type = explode ('.', $doc_name);
			$doc_type = mb_strtolower($type[count($type)-1]);

			$doc_cache_name = md5(rand()*1000000000);
            $doc_cache_type = $this->GetModifyDocument($uploadfile, $uploaddir, $doc_cache_name, $doc_type);
            $document = $uploaddir.$doc_cache_name.'.'.$doc_cache_type;

            if (!file_exists($document)) rename($uploadfile, $document);

            if ($this->request->getPost('doc_id'))
			{
			    if ($this->doUpdateDoc($this->request->getPost('doc_id'), $doc_type, $doc_name, $document, $doc_cache_name.'.'.$doc_cache_type))
			    $response['success'] = true;
			    else $response['success'] = false;
			}
			else {
			    foreach($entityIdList as $entityId)
                if ($this->doAddDoc($entityId, $doc_name, $doc_type, $document, $doc_cache_name.'.'.$doc_cache_type))
                $response['success'] = true;
                else $response['success'] = false;
			}
        }
        else $response['success'] = false;
        echo json_encode($response);
    }

    function doUploadFileSwf()
    {
        if ( (!empty($_REQUEST["PHPSESSID"]) && !empty($_REQUEST["PHPSESSIDX"]))
                && $_REQUEST["PHPSESSID"] != $_REQUEST["PHPSESSIDX"] ) {

                $_REQUEST["PHPSESSID"] = $_REQUEST["PHPSESSIDX"];
                unset($_REQUEST["PHPSESSIDX"]);
                $_COOKIE["PHPSESSID"] = $_REQUEST["PHPSESSID"];
        }
        session_start();

        $uploaddir = WGS3_DOCCACHE_DIR;
        $uploadfile = $uploaddir . basename($_FILES['Filedata']['tmp_name']);

        if (move_uploaded_file($_FILES['Filedata']['tmp_name'], $uploadfile)) {
            //$entityId = $this->request->getPost('ENTITYID');
            $entityIdList = json_decode($this->request->getPost('ENTITYID'), true);
            $doc_name = lmb_utf8_to_win1251(basename($_FILES['Filedata']['name']));
            $type = explode ('.', $doc_name);
            $doc_type = mb_strtolower($type[count($type)-1]);

            $doc_cache_name = md5(rand()*1000000000);
            $doc_cache_type = $this->GetModifyDocument($uploadfile, $uploaddir, $doc_cache_name, $doc_type);
            $document = $uploaddir.$doc_cache_name.'.'.$doc_cache_type;

            if (!file_exists($document)) rename($uploadfile, $document);

            if ($this->request->getPost('DOC_ID') && $this->request->getPost('DOC_ID') != 'null')
                $this->doUpdateDoc($this->request->getPost('DOC_ID'), $doc_type, $doc_name, $document, $doc_cache_name.'.'.$doc_cache_type);
            else {
                foreach($entityIdList as $entityId)
                    $this->doAddDoc($entityId, $doc_name, $doc_type, $document, $doc_cache_name.'.'.$doc_cache_type);
            }
        }
    }

    function GetModifyDocument($uploadfile, $uploaddir, $doc_cache_name, $doc_type)
    {
        $doc_cache_type = $doc_type;

        if ( $doc_type == 'jpe'  || $doc_type == 'jpg' || $doc_type == 'jpeg' || $doc_type == 'png'  || $doc_type == 'gif' )
        {
            $this->img_resize($uploadfile, $uploaddir.$doc_cache_name.'.'.$doc_type, 2816, 2112);
            $this->img_resize($uploadfile, $uploaddir.'sketch_'.$doc_cache_name.'.'.$doc_type, 120, 90);
            @unlink ($uploadfile);
        }

        if ($doc_type == 'tif' || $doc_type == 'tiff')
        {
            $doc_cache_type = $this->GetPdf($uploaddir, $doc_cache_name, $doc_type, $uploadfile);
            //@unlink ($uploadfile);
        }

        return $doc_cache_type;
    }

    // Получает количество документов
    function doGetCountDoc()
    {
        echo $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocCount($this->request->getPost('entityId'));
    }
    // Получает список документов
    //---------------------------------------------------------------------------------------------------------
    function doGetStoreDocList()
    {
		$entityIdList = json_decode($this->request->getPost('entityId'), true);
        $count = 0;
        
		foreach($entityIdList as $entityId)
        {
			//echo $entityId;
            $DocList = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocDescriptionList($entityId);
            foreach($DocList as $Doc)
            {
				$object_name = lmb_win1251_to_utf8($Doc->get('OBJECT_NAME'));
           		if (!$object_name) {
           			$object_name = encode('Без наименования')." - {$entityId}";
           		}
            	$actions = $this->getDocActions($Doc);
				//var_dump( $Doc);
            	$DocArray[$count]['id'] = $Doc->get('doc_id');
    	        $DocArray[$count]['name'] = lmb_win1251_to_utf8($Doc->get('doc_name'));
    	        $DocArray[$count]['description'] = encode($Doc->get('description'));
    	        $DocArray[$count]['entityid'] = $entityId;
    	        $DocArray[$count]['objectName'] = $object_name;
    	        $DocArray[$count]['iconClsView'] = $actions["iconClsView"];
    	        $DocArray[$count]['qtipView'] = "qTip";//$actions["qtipView"];
    	        $DocArray[$count]['urlDocument'] = $actions["urlDocument"];
    	        $DocArray[$count]['empty'] = '';
    	        $DocArray[$count]['storeType'] = $Doc->get('store_type');
    	        $count++;
            }
        }
		//var_dump($DocArray);
        $this->response->write(json_encode($DocArray));
    }
    //---------------------------------------------------------------------------------------------------------

    // Формирует массив действий для различных типов документов (просмотр/сохранение)
    //---------------------------------------------------------------------------------------------------------
    private function getDocActions($doc)
    {
    	$actions = array();

    	$doc_type = $doc->get('DOC_TYPE');
    	if ($doc_type == 'jpe'  || $doc_type == 'jpg' || $doc_type == 'jpeg' || $doc_type == 'png'  || $doc_type == 'gif') {
    		$actions['iconClsView'] = 'icon-view-image';
    		$actions['qtipView'] = lmb_win1251_to_utf8('Открыть изображение');
    	} else
    	if ($doc_type == 'tif'  || $doc_type == 'tiff' || $doc_type == 'pdf') {
    		$actions['iconClsView'] = 'icon-view-pdf';
    		$actions['qtipView'] = lmb_win1251_to_utf8('Открыть документ');
    	} else
    	if ($doc_type == 'dwf') {
    		$actions['iconClsView'] = 'icon-view-dwf';
    		$actions['qtipView'] = lmb_win1251_to_utf8('Открыть документ');
    	} else
    	if ($doc_type == 'mpg' || $doc_type == 'avi' || $doc_type == 'wmv') {
    		$actions['iconClsView'] = 'icon-view-video';
    		$actions['qtipView'] = lmb_win1251_to_utf8('Открыть видео');
    	} else {
    		$actions['iconClsView'] = 'icon-view-forbidden';
    		$actions['qtipView'] = lmb_win1251_to_utf8('Просмотр недоступен');
    	}
    	$actions['urlDocument'] = $this->getUrlZipDocument($doc);
    	return $actions;
    }
    //---------------------------------------------------------------------------------------------------------

    // Проверяет есть ли уже в doccache документы
    //---------------------------------------------------------------------------------------------------------
    function doGetDocExist()
    {
		
        $DocList = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocDescriptionList($this->request->getPost('entityId'));
        $count = 0;
        
        foreach($DocList as $Doc){
			//echo DOCCACHE_FULLDIR.$Doc->get('DOC_CACHE_NAME');
			if (file_exists(DOCCACHE_FULLDIR.$Doc->get('DOC_CACHE_NAME'))) $count++;
		}
        echo $count;
    }
    //---------------------------------------------------------------------------------------------------------

    // Получает список документов
    //---------------------------------------------------------------------------------------------------------
    function doupdateParams()
    {
        if ($this->request->getPost('param'))
        {
            $request = (json_decode($this->request->getPost('param'), true));

            foreach($request as $doc)
            {
                $id = $doc['id'];
                $name = lmb_utf8_to_win1251($doc['name']);
                $description = lmb_utf8_to_win1251($doc['description']);
                $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->setDocParam($id, $name, $description);
            }
        }
    }
    //---------------------------------------------------------------------------------------------------------

    // Функция добавления документов
	//---------------------------------------------------------------------------------------------------------
	function doAddDoc($entityId, $doc_name, $doc_type, $document, $doc_cache_name)
	{
	    $description = false;
	    $doc_id = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name);
        if ($doc_id) return $doc_id;
        else return false;
	}
    //---------------------------------------------------------------------------------------------------------

	// Функция редактирования поля blob документов
	//---------------------------------------------------------------------------------------------------------
	function doUpdateDoc($doc_id, $doc_type, $doc_name, $document, $doc_cache_name)
	{
	    $doc_cache_name = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->updateDoc($doc_id, $doc_type, $document, $doc_cache_name);
	    if ($doc_cache_name && file_exists(WGS3_DOCCACHE_DIR.$doc_cache_name))
        {
            @unlink (WGS3_DOCCACHE_DIR.$doc_cache_name);
            @unlink (WGS3_DOCCACHE_DIR.'sketch_'.$doc_cache_name);
            @unlink (WGS3_DOCCACHE_DIR.$doc_cache_name.'.zip');
        }
	    if ($doc_cache_name) return true;
        else return false;
	}
    //---------------------------------------------------------------------------------------------------------

    // Функция удаления документов
	//---------------------------------------------------------------------------------------------------------
	function doRemoveDoc()
	{
       $params = explode('_', $this->request->getPost('id'));
	   foreach($params as $id)
           if ($id && $id != 'undefined') {
                $doc_cache_name = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->removeDoc($id);
                if ($doc_cache_name && file_exists(WGS3_DOCCACHE_DIR.$doc_cache_name))
                {
                    @unlink (WGS3_DOCCACHE_DIR.$doc_cache_name);
                    @unlink (WGS3_DOCCACHE_DIR.'sketch_'.$doc_cache_name);
                    @unlink (WGS3_DOCCACHE_DIR.$doc_cache_name.'.zip');
                }
           }
	}
    //---------------------------------------------------------------------------------------------------------

	function doGetDocByEntityId()
	{
        $DocList = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocDescriptionList($this->request->getPost('entityId'));
        if ($DocList) $this->doGetDocContent($DocList);
	}

    function doGetDocByDocId()
    {
        $Doc = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocDescription($this->request->getPost('docId'));
        if ($Doc) $this->doGetDocContent($Doc);
    }

	// Получает содержимое документов
	//---------------------------------------------------------------------------------------------------------
	function doGetDocContent($DocList)
	{
	    //$this->delTemporaryFiles(WGS3_DOCCACHE_DIR);
	    $count = 0;
        $documents = array();

        foreach($DocList as $Doc)
        {
            $doc_id = $Doc->get('DOC_ID');
            $doc_name = lmb_win1251_to_utf8($Doc->get('DOC_NAME'));
            $doc_cache_name = $Doc->get('DOC_CACHE_NAME');
            $doc_type = $Doc->get('DOC_TYPE');
            $file = WGS3_DOCCACHE_DIR.$doc_cache_name;

            if (!file_exists($file)) {

            	if ($Doc->get('STORE_TYPE') == '0') {
                	$content = $this->wgsTier->getProjectTier()->getModuleService()->getFeatureDocService()->getDocument($doc_id);
            	} else {
            		$content = array();
            		$content[0]['DOCUMENT'] = file_get_contents(FEATUREDOC_DIR.'/'.$Doc->get('DOC_CACHE_NAME'));
            	}
                $this->SaveFileFromContent($doc_cache_name, $doc_type, $content[0]['DOCUMENT']);
            }

            $url_document = '/doccache/'.$doc_cache_name;

            if ($doc_type == 'jpe'  || $doc_type == 'jpg' || $doc_type == 'jpeg' || $doc_type == 'png'  || $doc_type == 'gif')
            {
                if (!file_exists(WGS3_DOCCACHE_DIR.'sketch_'.$doc_cache_name))
                $this->img_resize(WGS3_DOCCACHE_DIR.$doc_cache_name, WGS3_DOCCACHE_DIR.'sketch_'.$doc_cache_name, 120, 90);

                $url_sketch = '/doccache/sketch_'.$doc_cache_name;
                $object = '<img id="custom" src="'.$url_document.'" width="100%" height="100%" style="position:absolute;left:0;top:0;"/>';
            } else

            if ($doc_type == 'tif'  || $doc_type == 'tiff' || $doc_type == 'pdf')
            {
                if ($doc_type != 'pdf') {
					$url_sketch = '/images/pdf.png';
					$doc_cache_name_without_type = explode('.', $doc_cache_name);
					$url_document = '/doccache/'.$doc_cache_name_without_type[0].'.pdf';
					if (!file_exists(WGS3_DOCCACHE_DIR.$doc_cache_name_without_type[0].'.pdf')) {
						$this->GetModifyDocument(WGS3_DOCCACHE_DIR.$doc_cache_name, WGS3_DOCCACHE_DIR, $doc_cache_name_without_type[0], $doc_type);
                	}
				}
                else $url_sketch = '/images/pdf.png';
                $doc_type = 'pdf';

                $object = ' <object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" STANDBY="Zagruzka"
                            CODEBASE="/downloads/AdbeRdr90ru.exe"
                            id="custom" width="100%" height="100%">
                            <param name="src" value="'.$url_document.'"></param>
                            <EMBED src="'.$url_document.'" width="100%" height="100%" href="'.$url_document.'"></EMBED>
                            </object>';
            } else

            if ($doc_type == 'dwf')
            {
                $url_sketch = '/images/dwf.png';
                $object = ' <object classid="clsid:A662DA7E-CCB7-4743-B71A-D817F6D575DF" standby="Loading Elvis..."
                            CODEBASE="/downloads/SetupDesignReview.exe"
                            id="custom" width="100%" height="100%">
                            <param name="src" value="'.$url_document.'"></param>
                            </object>';
            } else

            if ($doc_type == 'mpg' || $doc_type == 'avi' || $doc_type == 'wmv')
            {
                if ($doc_type == 'avi') $url_sketch = '/images/avi.png';
                if ($doc_type == 'wmv') $url_sketch = '/images/wmv.png';
                if ($doc_type == 'mpg') $url_sketch = '/images/mpg.png';
                $object = ' <object classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"
                            id="custom" width="100%" height="100%">
                            <param name="url" value="'.$url_document.'"></param>
                            <param name="autoStart" value="true"></param>
                            <EMBED src="'.$url_document.'" width="100%" height="100%" href="'.$url_document.'"></EMBED>
                            </object>';
            } else {
            	/*$zip = new ZipArchive;
            	$zipName = WGS3_DOCCACHE_DIR.lmb_utf8_to_win1251($doc_name);
            	$res = $zip->open("{$zipName}.zip", ZipArchive::CREATE);
            	if (true === $res) {
            		$zip->addFile($file, $this->translit(lmb_utf8_to_win1251($doc_name)));
    				$zip->close();
    				$url_document = "/doccache/{$doc_name}.zip";
            	}*/
            	$url_document = $this->getUrlZipDocument($Doc);
             	$object = null;
            	$url_sketch = '/images/noname.png';
            }


            $documents[] = array (
                'name' => $doc_name,
                'object' => $object,
                'url_sketch'=>$url_sketch,
                'doctype' => $doc_type,
            	'urlDocument' => $url_document,
            	'url_zip' => $this->getUrlZipDocument($Doc)
            );

            $count++;
        }

        $store = array('documents' => $documents);
        echo json_encode($store);
	}
    //---------------------------------------------------------------------------------------------------------

	// Получаем ссылку на архив с документом
    //---------------------------------------------------------------------------------------------------------
	private function getUrlZipDocument($doc)
	{
		$url_document = '';
		$doc_name = $doc->get('DOC_NAME');
        $doc_cache_name = $doc->get('DOC_CACHE_NAME');
        $file = file_exists(WGS3_DOCCACHE_DIR.$doc_name) ? WGS3_DOCCACHE_DIR.$doc_name : WGS3_DOCCACHE_DIR.$doc_cache_name ;

        $zip = new ZipArchive;
        $dcn = $this->translit(lmb_utf8_to_win1251($doc_cache_name));
		$zipName = WGS3_DOCCACHE_DIR.$dcn;

		$res = $zip->open("{$zipName}.zip", ZipArchive::CREATE);
		if (true === $res) {
			$zip->addFile($file, $this->translit(lmb_utf8_to_win1251($doc_name)));
			$zip->close();
			$url_document = "/doccache/{$dcn}.zip";
		}
		//var_dump($res);
		@unlink(WGS3_DOCCACHE_DIR.$doc_name);
		@unlink(WGS3_DOCCACHE_DIR.$doc_name.".pdf");
        return $url_document;
	}
	//---------------------------------------------------------------------------------------------------------

	// Сохраняем документ на диске
    //---------------------------------------------------------------------------------------------------------
	function SaveFileFromContent($doc_cache_name, $doc_type, $content)
	{
	    $fpw = @fopen(WGS3_DOCCACHE_DIR.$doc_cache_name,"w+");
          $nBuffer = 8192;
          for ($i=0; $i<strlen($content); $i+=$nBuffer)
            @fwrite($fpw,substr($content,$i,($i+$nBuffer)),$nBuffer);
        @fclose ($fpw);

        // создаем эскиз для картинки
        if ($doc_type == 'jpe'  || $doc_type == 'jpg' || $doc_type == 'jpeg' || $doc_type == 'png'  || $doc_type == 'gif')
        $this->img_resize(WGS3_DOCCACHE_DIR.$doc_cache_name, WGS3_DOCCACHE_DIR.'sketch_'.$doc_cache_name, 120, 90);
	}

	//---------------------------------------------------------------------------------------------------------

	// Изменяем размер картинки
    //---------------------------------------------------------------------------------------------------------
    function img_resize($src, $dest, $width, $height, $rgb=0xF4F4F4, $quality=90)
    {
      if (!file_exists($src)) return false;

      $size = getimagesize($src);

      if ($size === false) return false;

      // Определяем исходный формат по MIME-информации, предоставленной
      // функцией getimagesize, и выбираем соответствующую формату
      // imagecreatefrom-функцию.
      $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
      $icfunc = "imagecreatefrom" . $format;
      if (!function_exists($icfunc)) return false;

      $x_ratio = $width / $size[0];
      $y_ratio = $height / $size[1];

      $ratio       = min($x_ratio, $y_ratio);
      $use_x_ratio = ($x_ratio == $ratio);

      $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
      $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
      $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
      $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

      $isrc = $icfunc($src);
      $idest = imagecreatetruecolor($width, $height);

      imagefill($idest, 0, 0, $rgb);
      if ($width == 120)
      imagecopyresampled ($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);
      else imagecopyresized ($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

      imagejpeg($idest, $dest, $quality);

      imagedestroy($isrc);
      imagedestroy($idest);
    }
    //---------------------------------------------------------------------------------------------------------

    //----------------------------создаем pdf документ для открытия файлов типа *.tif(tiff)--------------------
    //---------------------------------------------------------------------------------------------------------
    function addImage($pdf,$imageName)
    {
		$pdf -> AddPage();
        $convertScript = GM_EXE_PATH.' convert "'.$path.$imageName.'" "'.$path.$imageName.'.png"' ;
		$pdf -> Image($path.$imageName.'.png');
		@unlink(WGS3_DOCCACHE_DIR.$imageName.'.png');		
	}
	
	function saveFloorPlanImage($imageName, $dcn)
    {
		while(strpos($imageName, '\\') || strpos($imageName, '/')){
			$pos = strpos($imageName, '\\');
			if(!$pos)
				$pos = strpos($imageName, '/');
			$imageName = substr($imageName, $pos+1); 
		}
		$path = DOCCACHE_FULLDIR;
		$convertScript = GM_EXE_PATH . ' convert "'.$path.$imageName.'" "'.$path.$dcn.'.pdf"' ;
		exec($convertScript, $ar , $r);
		//@unlink($path.$imageName);
		return $path.$dcn.'.pdf';
    }
	
	function GetPdf($dir, $doc_cache_name, $doc_type, $imageName)
    {
        $floorplanImageNames = array();
		$i = 0;
		$maxHeight = 0;
		$maxWidth = 0;
		$floorplanImageNames[$i] = $this->saveFloorPlanImage($imageName, $doc_cache_name);
		$imageinfo = getimagesize($floorplanImageNames[$i]);
			if ($imageinfo[0] > $maxWidth) $maxWidth = $imageinfo[0];
			if ($imageinfo[1] > $maxHeight) $maxHeight = $imageinfo[1];
		$i++;			
        		
		return "pdf";
		
		
		/*$fpw = @fopen(WGS3_PRINTCACHE_DIR.$floorPlanName,"w+");
         $nBuffer = 8192;
         for ($i=0; $i<strlen($content); $i+=$nBuffer)
         @fwrite($fpw,substr($content,$i,($i+$nBuffer)),$nBuffer);
         @fclose ($fpw);
		 $convertScript = '"C:\Program Files\GraphicsMagick-1.3.13-Q8\gm.exe" convert d:/project/wgs3/application/www/printcache/' . $floorPlanName . ' d:/project/wgs3/application/www/printcache/' . $floorPlanName . '.png' ;
		 $error = exec($convertScript);
		 @unlink(WGS3_PRINTCACHE_DIR.$floorPlanName);
		 return WGS3_PRINTCACHE_DIR.$floorPlanName.'.png';
		*/
		
		/*$image_param = getimagesize($sTempImageFileName);
        $pdf = pdf_new();
        pdf_open_file($pdf, null);
        pdf_begin_page($pdf, $image_param[0], $image_param[1]);
        $image = pdf_open_image_file($pdf,'tiff',$sTempImageFileName, null, null);
        pdf_place_image($pdf,$image,0,0,1.0);
        pdf_end_page($pdf);
        pdf_close($pdf);
        $data = pdf_get_buffer($pdf);

        $fpw = @fopen($dir.$doc_cache_name.".pdf","w+");
        $nBuffer = 8192;
        for ($i=0; $i<strlen($data); $i+=$nBuffer)
            @fwrite($fpw,substr($data,$i,($i+$nBuffer)),$nBuffer);
        @fclose ($fpw);
        return "pdf";*/
    }
    //---------------------------------------------------------------------------------------------------------

    //------------------------------------------удаляем временные файлы с сервера------------------------------
    function delTemporaryFiles ($directory)
    {
        $dir = @opendir ($directory);
        while (( $file = @readdir ($dir)))
        {
        if( is_file ($directory.$file))
        {
          $acc_time = filemtime ($directory."/".$file);
          $time =  time();
          if (($time - $acc_time) > 7889232) // 7889232 = 3 месяца
          {
               @unlink ($directory.$file);
          }
        }
        }
        @closedir ($dir);
    }
    //---------------------------------------------------------------------------------------------------------

    //------------------------------------------транслитерация кириллицы (для раюоты с zip)--------------------
	function translit($str)
	{
	    $tr = array(
	        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I","Й"=>"Y",
	        "К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T","У"=>"U",
	        "Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH","Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
	        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"," "=>"_"
	    );
	    return strtr($str,$tr);
	}

	function doLoadPhotos()
	{
		set_time_limit(0);
		$tnhk_oci = OCILogon("tnhk_adb", "projatt", "GIS");
		$tsa_oci = OCILogon("tnh_sdb_atom", "tnhspa", "GIS");
		if ($tnhk_oci && $tsa_oci) {
/*
			$query = "select p.pict_id, p.picture, p.object_id, p.pict_name, p.pict_type,
				go.table_name, go.entity_id, go.geometry_hash,
				f.feature_id, f.geometryhash
				from photo p, gp_object go, tnhk_sdb.featuretable f
				where
				p.object_id = go.object_id
				and go.entity_id = f.entityid";
*/

			$query = "select p.pict_id, p.picture, p.object_id, p.pict_name, p.pict_type,        
				f.feature_id, f.geometryhash
				from photo p, indefinite b, tnhk_sdb.featuretable f
				where
				p.object_id = b.object_id
				and 
        b.geometry_hash = f.geometryhash
        and
        f.feature_id not in (select f.feature_id from tnhk_adb.featuredoc f)";


			$statement = oci_parse($tsa_oci, $query);
			oci_execute ($statement);
            $cacheId = 10635;

			while ($row = oci_fetch_array($statement, OCI_ASSOC+OCI_RETURN_LOBS)) {
				$cacheId = $cacheId+1;
				$cacheName = $this->translit($cacheId."_".$row['PICT_NAME']);
				file_put_contents(FEATUREDOC_DIR.$cacheName, $row['PICTURE']);
				$query = "insert into featuredoc(doc_id, feature_id, document, doc_name, doc_type,geometry_hash,description,doc_cache_name,store_type)
								values (featuredoc_id_seq.nextval, {$row['FEATURE_ID']}, null,'{$row['PICT_NAME']}','{$row['PICT_TYPE']}', '{$row['GEOMETRYHASH']}', 'Экспорт из WGS2','{$cacheName}', 1)";
				$stmt = oci_parse($tnhk_oci, $query);
			 	oci_execute($stmt, OCI_DEFAULT);
			 	oci_execute(oci_parse($tnhk_oci, "commit"), OCI_DEFAULT);
			 	oci_free_statement($stmt);
			 	echo "File '{$row['PICT_NAME']}' loaded <br>";
			}

			echo "All files loaded";

			oci_free_statement($statement);
			OCILogoff($tsa_oci);

			OCILogoff($tnhk_oci);

		} else {
			$err = OCIError();
			echo "Oracle Connect Error " . $err['text'];
		}

	}
}

?>
