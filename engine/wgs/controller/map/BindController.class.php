<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class BindController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::CARD_BIND);        
    }
    
    function doDisplay() {}
    
    function doGetFeatureTypes()
    {
        if ($entityId = $this->request->getPost('entityId')) {
            $featureTypes = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getFeatureTypesByFeature($entityId);

            $result = array();
            
            foreach ($featureTypes as $featureType) {
                
                $result[] = array(
                    'featureTypeName' => lmb_win1251_to_utf8($featureType->get('name')),
                    'featureTypeId' => $featureType->get('object_type_id'),
                    'isCurrent' => $featureType->get('is_current')
                );
            }
            
            $this->response->write(json_encode($result));
        }
    }
    
    function doGetBinded()
    {
        if ($featureId = $this->request->getPost('featureId')) {
            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($featureId);
            $result = array();
            foreach ($cardList as $value) {
                $cardName = $value->get('Name')? lmb_win1251_to_utf8($value->get('Name')): lmb_win1251_to_utf8('��� ������������');
                $isCurrent = $value->get('is_current');
                $cardTypeName = lmb_win1251_to_utf8($value->get('object_type_name'));
                $result[] = array(
                    'cardName' => $isCurrent? "<font color=navy>$cardName</font>": $cardName,
                    'cardId' => $value->get('Object_id'),
                    'cardIsCurrent' => $isCurrent,
                    'cardTypeName' => $isCurrent? "<font color=navy>$cardTypeName</font>": $cardTypeName,
                    'ADBSDBRelationId' => $value->get('ADBSDBRelation_Id')
                );
            }
            $this->response->write(json_encode(array_values($result)));
        }
    }
    
    function doGetInlineBinded()
    {
        $featureIds = $this->request->getPost('featureIds');
        $relationId = $this->request->getPost('objTypeRelationId');
        if (count($featureIds)) {            
            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectByFeatures($relationId, $featureIds);
            $result = array();
            foreach ($cardList as $value) {
                $cardName = $value->get('Name')? lmb_win1251_to_utf8($value->get('Name')): lmb_win1251_to_utf8('��� ������������');
                $result[] = array(
                    'cardName' => $isCurrent? "<font color=navy>$cardName</font>": $cardName,
                    'cardId' => $value->get('object_id'),
                    'objTypeRelationId' => $value->get('objtype_relation_id')
                );
            }
            $this->response->write(json_encode(array_values($result)));
        }
    }
    
    function doCreateCard()
    { 
        if ($featureTypeId = $this->request->getPost('featureTypeId')) {
            $objectId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->add($featureTypeId);
            $this->response->write(json_encode(array('objectId' => $objectId)));
        }
    }
    
    function doBindCard()
    {
        $objectId  = $this->request->getPost('objectId');
        $featureId = $this->request->getPost('featureId');
        $isCurrent = ($this->request->getPost('isCurrent'))? 1: 0;
        if ($objectId && $featureId) {
            $this->wgsTier->getProjectObjectService()->getAttObjectService()->addRelationByFeature(null, $objectId, $featureId, $isCurrent);
        }
    }

    function doBindFeature()
    {
        $objectId  = $this->request->getPost('objectId');
        $featureId = $this->request->getPost('featureId');
        if ($objectId && $featureId) {
            $this->wgsTier->getProjectObjectService()->getAttObjectService()->addRelationByFeature(null, $objectId, $featureId, 1);
        }
    }
    
    function doUnBindCard()
    {
        if ($ADBSDBRelations  = $this->request->getPost('ADBSDBRelations')) {
            if (count($ADBSDBRelations) > 0) {
                foreach($ADBSDBRelations as $ADBSDBRelation) {
                    $this->wgsTier->getProjectObjectService()->getAttObjectService()->removeRelationByFeature($ADBSDBRelation, 3);
                }
            }
        }
    }
    
    function doRemoveCard()
    {
        if ($cardIds  = $this->request->getPost('cardIds')) {
            if (count($cardIds) > 0) {
                foreach($cardIds as $cardId) {
                    $this->wgsTier->getProjectObjectService()->getAttObjectService()->Remove($cardId, 0);                    
                }
            }
        }
    }
    
    function doGetFeatures()
    {
        $featureIds = $this->request->getPost('featureIds');
        if (count($featureIds)) {
            $featureList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeatures($featureIds);
            $result = array();
            foreach ($featureList as $value) {
                $result[] = array(
                    'featureLabel' => $value->get('label')? lmb_win1251_to_utf8($value->get('label')): lmb_win1251_to_utf8('��� ������������'),    
                    'featureLayerName' => lmb_win1251_to_utf8($value->get('name')),
                    'cardId' => $value->get('object_id'),
                    'featureId' => $value->get('feature_id'),
                    'entityId' => $value->get('entityid')
                );
            }
            $this->response->write(json_encode(array_values($result)));
        }
    }
    
    function doGetRelations()
    {
        if ($featureId = $this->request->getPost('featureId')) {
            $relationList = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelationByFeature($featureId);
            $result = array();
            foreach ($relationList as $value) {
                $isCurrent = $value->get('is_current');
                $relationName = lmb_win1251_to_utf8($value->get('parent_object_type_name').' - '.$value->get('child_object_type_name').': '.$value->get('relation_name'));
                $result[] = array(
                    'objTypeRelationId' => $value->get('objtype_relation_id'),
                    'relationId' => $value->get('relation_id'),    
                    'relationName' => $relationName,
                    'relationType' => $value->get('relation_type_id'),
                    'featureTypeId' => $value->get('child_object_type_id'),
                    'featureTypeName' => lmb_win1251_to_utf8($value->get('object_type_name'))
                );
            }
            $this->response->write(json_encode(array_values($result)));
        }
    }
}

?>