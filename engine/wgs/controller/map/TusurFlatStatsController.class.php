<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');

class TusurFlatStatsController extends wgsFrontController {

    function doGetStats() {
      $featureLayerId = $this->request->getPost('data');
      if (!$featureLayerId) {
        $this->response->write(json_encode(null));
        return;
      }
      $service = $this->wgsTier->getTusurFlatStatsService();
      $currentPlaces = $service -> getCurrentPlacesByFeatureLayerId($featureLayerId);

      $service = $this->wgsTier->getTusurFlatStatsService();
      $freePlaces = $service -> getFreePlacesByFeatureLayerId($featureLayerId);

      $service = $this->wgsTier->getTusurFlatStatsService();
      $maxByDocsPlaces = $service -> getMaxByDocsPlacesByFeatureLayerId($featureLayerId);

      $service = $this->wgsTier->getTusurFlatStatsService();
      $maxPlaces = $service -> getMaxPlacesByFeatureLayerId($featureLayerId);

      $service = $this->wgsTier->getTusurFlatStatsService();
      $sexOfStudents = $service -> getSummarySexOfStudentsByFeatureLayerId($featureLayerId);

      foreach($sexOfStudents as $result) {
      //var_dump($result);
      $students[] = array(
         'label' => encode($result->get('value_string')),
         'value' => $result->get('sum')
         );
      }



      $stats = array('currentPlaces' => $currentPlaces,
                      'freePlaces' => $freePlaces,
                      'maxByDocsPlaces' => $maxByDocsPlaces,
                      'maxPlaces' => $maxPlaces,
                      'sexOfStudents' => $students);

      //var_dump($stats);

      $this->response->write(json_encode($stats));
    }


    function doGetCurrentPlacesByFeatureLayerId() {
     	$featureLayerId = $this->request->getPost('data');
      if (!$featureLayerId) {
        $this->response->write(json_encode(null));
        return;
      }
      $service = $this->wgsTier->getTusurFlatStatsService();
      $currentPlaces = $service -> getCurrentPlacesByFeatureLayerId($featureLayerId);
      $this->response->write(json_encode($currentPlaces));
    }

}

?>
