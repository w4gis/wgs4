<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require(MAPVIEWER_DIR."/common.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require(MAPVIEWER_DIR."/layerdefinitionfactory.php");

class MeasureController extends wgsFrontController
{
    function doesLayerExist($layerName, $map)
    {
        $layerCollection = $map->GetLayers();
        return( $layerCollection->Contains($layerName) ? true : false ) ;
    }
    
    function getMapSrs($map)
    {
        $srs = $map->GetMapSRS();
        if($srs != "")
        return $srs;
        return "LOCALCS[\"Non-Earth (Meter)\",LOCAL_DATUM[\"Local Datum\",0],UNIT[\"Meter\", 1],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";
    }
	
	
    
    function makeLine($name, $key, $x0, $y0 , $x1, $y1)
    {
        $propertyCollection = new MgPropertyCollection();
        $nameProperty = new MgStringProperty("NAME", $name);//MgStringProperty
        $propertyCollection->Add($nameProperty);
        $keyProperty = new MgInt32Property("OBJECTID", $key);
        $propertyCollection->Add($keyProperty);
        $totalProp = new MgDoubleProperty("TOTAL", $key);
        $propertyCollection->Add($totalProp);
        $wktReaderWriter = new MgWktReaderWriter();
        $agfReaderWriter = new MgAgfReaderWriter();
        $geometry = $wktReaderWriter->Read("LINESTRING XY ($x0 $y0, $x1 $y1)");
        $geometryByteReader = $agfReaderWriter->Write($geometry);
        $geometryProperty = new MgGeometryProperty("SHPGEOM", $geometryByteReader);
        $propertyCollection->Add($geometryProperty);
        return $propertyCollection;
    }
    
    function doesResourceExist($resourceIdentifier, $resourceService)
    {
        try 
        {
            $resourceService->GetResourceContent($resourceIdentifier);
        }
        catch (MgResourceNotFoundException $e)
        {
            return false;
        }
        return true;
    }
    
    function addLayerDefinitionToMap($layerDefinition, $layerName, $layerLegendLabel, $sessionId, $resourceService, &$map)
    {
        $byteSource = new MgByteSource($layerDefinition, strlen($layerDefinition));
        $byteSource->SetMimeType(MgMimeType::Xml);
        $resourceID = new MgResourceIdentifier("Session:$sessionId//$layerName.LayerDefinition");
        $resourceService->SetResource($resourceID, $byteSource->GetReader(), null);
        $newLayer = $this->addLayerResourceToMap($resourceID, $resourceService, $layerName, $layerLegendLabel, $map);
        return $newLayer;
    }
    //not used
    function addLayerToGroup($layer, $layerGroupName, $layerGroupLegendLabel, &$map)
    {
        $layerGroupCollection = $map->GetLayerGroups();
        if ($layerGroupCollection->Contains($layerGroupName))
        {
            $layerGroup = $layerGroupCollection->GetItem($layerGroupName);
        }
        else
        {
            $layerGroup = new MgLayerGroup($layerGroupName);
            $layerGroup->SetVisible(true);
            $layerGroup->SetDisplayInLegend(true);
            $layerGroup->SetLegendLabel($layerGroupLegendLabel);
            $layerGroupCollection->Add($layerGroup);
        }
        $layer->SetGroup($layerGroup);
    }
    //not used
    function addLayerResourceToMap($layerResourceID, $resourceService, $layerName, $layerLegendLabel, &$map)
    {
        $newLayer = new MgLayer($layerResourceID, $resourceService);
        $newLayer->SetName($layerName);
        $newLayer->SetVisible(true);
        $newLayer->SetLegendLabel($layerLegendLabel);
        $newLayer->SetDisplayInLegend(true);
        $layerCollection = $map->GetLayers();
        if (! $layerCollection->Contains($layerName) )
        {
            $layerCollection->Insert(0, $newLayer);
        }
        return $newLayer;
    }

    function createLayerDefinition($resourceId, $featureClass, $geometry, $featureClassRange)
    {
        $layerDef = file_get_contents(VIEWERFILES_DIR."/layerdefinition.templ");
        $layerDef = sprintf($layerDef, $resourceId, $featureClass, $geometry, $featureClassRange);
        return $layerDef;
    }

    function createLineRule($legendLabel, $filter, $color)
    {
        $lineRule = file_get_contents(VIEWERFILES_DIR."/linerule.templ");
        $lineRule = sprintf($lineRule, $legendLabel, $filter, $color);
        return $lineRule;
    }

    function createLineTypeStyle($lineRules)
    {
        $lineStyle = file_get_contents(VIEWERFILES_DIR."/linetypestyle.templ");
        $lineStyle = sprintf($lineStyle, $lineRules);
        return $lineStyle;
    }

    function createScaleRange($minScale, $maxScale, $typeStyle)
    {
        $scaleRange = file_get_contents(VIEWERFILES_DIR."/scalerange.templ");
        $scaleRange = sprintf($scaleRange, $minScale, $maxScale, $typeStyle);
        return $scaleRange;
    }
        
    function clearDataSource($featureSrvc, $dataSourceId, $featureName)
    {
        $deleteCmd = new MgDeleteFeatures($featureName, "KEY >= 0");
        $commands = new MgFeatureCommandCollection();
        $commands->Add($deleteCmd);
        $featureSrvc->UpdateFeatures($dataSourceId, $commands, false);
    }

    function doAddMeashureSegment()
    {
        $post = $this->request->getPost();
        $x0 = $post['x1'];
        $y0 = $post['y1'];
        $x1 = $post['x2'];
        $y1 = $post['y2'];
        $sessionId = $post['session'];
        $mapName = $post['mapname'];
        $layerLegendLabel = $post['layerlegend'];
        $segId  = $post['segment'];
        $layerName = "MeasureLayer";

        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);

        $sessionIdName = "Session:$sessionId//$mapName.Map";
        $sessionResourceId = new MgResourceIdentifier($sessionIdName);
        
        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $srs = $this->getMapSrs($map);
        $srsFactory = new MgCoordinateSystemFactory();
        $srsMap = $srsFactory->Create($srs);
        $srsType = $srsMap->GetType();
        if($srsType == MgCoordinateSystemType::Geographic)
            $distance = $srsMap->MeasureGreatCircleDistance($x0, $y0, $x1, $y1);
        else
            $distance = $srsMap->MeasureEuclideanDistance($x0, $y0, $x1, $y1);
        $distance = $srsMap->ConvertCoordinateSystemUnitsToMeters($distance);
        if(!$us)
            $distance *= 0.001;             //get kilometers
        else
            $distance *= 0.000621371192;    //get miles
        $total += $distance;

        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);
        $featureSourceExists = $this->doesResourceExist($resourceIdentifier, $resourceService);

        if (! $featureSourceExists)
        {
            $classDefinition = new MgClassDefinition();
            $classDefinition->SetName($layerName);
            $geometryPropertyName="SHPGEOM";
            $classDefinition->SetDefaultGeometryPropertyName( $geometryPropertyName);
            
            $identityProperty = new MgDataPropertyDefinition("KEY");
            $identityProperty->SetDataType(MgPropertyType::Int32);
            $identityProperty->SetAutoGeneration(true);
            $identityProperty->SetReadOnly(true);
            $classDefinition->GetIdentityProperties()->Add($identityProperty);
            $classDefinition->GetProperties()->Add($identityProperty);
            
            $nameProperty = new MgDataPropertyDefinition("NAME");
            $nameProperty->SetDataType(MgPropertyType::String);
            $classDefinition->GetProperties()->Add($nameProperty);

            $objectIdProperty = new MgDataPropertyDefinition("OBJECTID");
            $objectIdProperty->SetDataType(MgPropertyType::Int32);
            $classDefinition->GetProperties()->Add($objectIdProperty);

            $prop = new MgDataPropertyDefinition("TOTAL");
            $prop->SetDataType(MgPropertyType::Double);
            $classDefinition->GetProperties()->Add($prop);

            $geometryProperty = new MgGeometricPropertyDefinition($geometryPropertyName);
            $geometryProperty->SetGeometryTypes(MgFeatureGeometricType::Surface);
            $classDefinition->GetProperties()->Add($geometryProperty);

            $featureSchema = new MgFeatureSchema("SHP_Schema","Line schema");
            $featureSchema->GetClasses()->Add($classDefinition);

            $wkt = $map->GetMapSRS();
            $sdfParams = new MgCreateSdfParams("spatial context", $wkt, $featureSchema);
            $featureService->CreateFeatureSource($resourceIdentifier, $sdfParams);
        }

        $layerExists = $this->doesLayerExist($layerName, $map);
        if (! $layerExists )
        {
            $featureName = $layerName;
            $geometry = 'SHPGEOM';

            $this->clearDataSource($featureService, $resourceIdentifier, $layerName);

            $ruleLegendLabel = 'Measure Rule';
            $filter = '';
            $color = 'FFFF0000';
            $factory = new LayerDefinitionFactory();
            $lineRule = $this->createLineRule($ruleLegendLabel, $filter, $color);

            $lineTypeStyle = $this->createLineTypeStyle($lineRule);
            $minScale = '0';
            $maxScale = '1000000000000';
            $lineScaleRange = $this->createScaleRange($minScale, $maxScale, $lineTypeStyle);
            $layerDefinition = $this->createLayerDefinition($featureSourceName, $layerName, $geometry, $lineScaleRange);
            $newLayer = $this->addLayerDefinitionToMap($layerDefinition, $layerName, $layerLegendLabel, $sessionId, $resourceService, $map);

        }
        $objectId = rand();
        $propertyCollection = $this->makeLine('Segment '.$segId, $objectId , $x0, $y0, $x1, $y1);
         
        $cmd = new MgInsertFeatures($layerName, $propertyCollection);//$batchPropertyCollection
        $featureCommandCollection = new MgFeatureCommandCollection();
        $featureCommandCollection->Add($cmd);

        $featureService->UpdateFeatures($resourceIdentifier, $featureCommandCollection, false);
        $layerCollection = $map->GetLayers();
        if ($layerCollection->Contains($layerName))
        {
            $layer =$layerCollection->GetItem($layerName);
            $layer->SetSelectable(true);
            $layer->SetVisible(true);
            $layer->ForceRefresh();
        }
        $sessionResourceId->Validate();
        $map->Save($resourceService, $sessionResourceId);

        $queryOptions = new MgFeatureQueryOptions();
        $layer = $layerCollection->GetItem($layerName);
        $layerClassName = $layer->GetFeatureClassName();
        $layerFeatureId = $layer->GetFeatureSourceId();
        $layerFeatureResource = new MgResourceIdentifier($layerFeatureId);
        $queryOptions->SetFilter('OBJECTID = '.$objectId);
        $featureReader = $featureService->SelectFeatures($layerFeatureResource,$layerClassName, $queryOptions);
        $selection = new MgSelection($map);
        $selection->AddFeatures($layer, $featureReader, 0);
        $responseArray = array();
        
        if(round($total*1000) > 1)
        $total = round($total*1000);
        else        
        $total = round($total*1000, 2);
        $responseArray[0]['segmentName'] = '';
        $responseArray[0]['segmentLength'] = $total;
        $responseArray[0]['segmentSelectionXml'] = $selection->ToXml();
        $this->response->write(json_encode(array_merge($responseArray)));
    }
    
    function doClearMeasure()
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
        $sessionId = $this->mgTier->getMgSessionId();
        $layerName = "MeasureLayer";
        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);
        $this->clearDataSource($featureService, $resourceIdentifier, $layerName);
    }
    
    function doEndMeasure()
    {
        $mapName = $this->request->getPost('mapname');
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        $featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
        $sessionId = $this->mgTier->getMgSessionId();
        $layerName = "MeasureLayer";
        $featureSourceName = "Session:$sessionId//$layerName.FeatureSource";
        $resourceIdentifier = new MgResourceIdentifier($featureSourceName);
        if($this->doesResourceExist($resourceIdentifier, $resourceService))
        $this->clearDataSource($featureService, $resourceIdentifier, $layerName);
        $map = new MgMap();
        $map->Open($resourceService, $mapName);
        $sessionIdName = "Session:$sessionId//$mapName.Map";
        $sessionResourceId = new MgResourceIdentifier($sessionIdName);
        $layerCollection = $map->GetLayers();
        if ($layerCollection->Contains($layerName))
        {
            $layer =$layerCollection->GetItem($layerName);
            $layerCollection->Remove($layer);    
        }
        $map->Save($resourceService, $sessionResourceId);
    }
    
    function doDisplay()
    {
         
    }
}

?>