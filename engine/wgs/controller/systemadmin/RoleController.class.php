<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
//lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class RoleController extends wgsFrontController
{
    function doDisplay ()
    {
        $this->setComponentTemplate('/modules/systemadmin/testextjs1.html');
    }

    function doGetAvialableRoles()
    {

        $roleList = array();
        if($this->request->getPost('id')==0)
        {
            $roles = $this->wgsTier->getSysRoleService()->getShortList();
        }
        else
        {
            $roles = $this->wgsTier->getSysRoleService()->getAcceptableChildrenRolesById($this->request->getPost('id'));
        }

        foreach ($roles as $role)
        {
            if(!(is_array($role->getParentSysRoleIds())))
            {
                $id = $role->getId();
                $roleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
                );
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($roleList[$_POST['node']])));

    }

    function doGetChildrenRoles()
    {
        $roles = $this->wgsTier->getSysRoleService()->getChildrenRolesById($this->request->getPost('id'));
        foreach ($roles as $role)
        {
            if(!(is_array($role->getParentSysRoleIds())))
            {
                $id = $role->getId();
                $roleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
                );
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($roleList[$_POST['node']])));
    }

    function doGetAvialableUsers()
    {
        $users = $this->wgsTier->getSysUserService()->getList();
        $userList = array();
        if($this->request->getPost('id') == 0)
        {
            foreach ($users as $user)
            {
                $id = $user->getId();
                $userList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($user->getName()),
                'leaf'=>true
                );
            }
        }
        else
        {
            $userMembers =  $this->wgsTier->getSysUserService()->getUserMembersByRoleId($this->request->getPost('id'));

            foreach($users as $user)
            {
                if(count($userMembers) > 0)
                {
                    $k = 0;
                    foreach($userMembers as $usermember)
                    {
                        if($user->getId() != $usermember->getId())
                        {
                            $k++;
                        }
                        if ($k == count($userMembers))
                        {
                            $id = $user->getId();
                            $userList['root'][] = array(
                            'id'=>$id,
                            'text'=>$user->getName(),
                            'leaf'=>true
                            );
                        }
                    }
                }
                else
                {
                    $id = $user->getId();
                    $userList['root'][] = array(
                    'id'=>$id,
                    'text'=>$user->getName(),
                    'leaf'=>true
                    );
                }
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($userList[$_POST['node']])));

    }

    function doGetMemberUsers()
    {
        $userMembers =  $this->wgsTier->getSysUserService()->getUserMembersByRoleId($this->request->getPost('id'));
        $userList = array();
        foreach ($userMembers as $user)
        {
            $id = $user->getId();
            $userList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($user->getName()),
                'leaf'=>true
            );
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($userList[$_POST['node']])));
    }

    function doGetEmptyTree()
    {

    }

    function doGetRoleList()
    {
        $roles = $this->wgsTier->getSysRoleService()->getShortList();
      
        $roleList = array();
        foreach ($roles as $role)
        {
            $roleList[$role->getId()]['roleId'] = $role->getId();
            $roleList[$role->getId()]['roleName'] = lmb_win1251_to_utf8($role->getName());
            $roleList[$role->getId()]['description'] = lmb_win1251_to_utf8($role->getDescription());
            $childrenRoles = $this->wgsTier->getSysRoleService()->getChildrenRolesById($role->getId());

            $sChildrenRoles = '';
            if(count($childrenRoles) > 0)
            {
                $k = 0;
                foreach ($childrenRoles as $childrenRole)
                {
                    if($k == count($childrenRoles)-1)
                    $sChildrenRoles.=lmb_win1251_to_utf8($childrenRole->getName());
                    else
                    $sChildrenRoles.=lmb_win1251_to_utf8($childrenRole->getName()).', ';
                    $k++;
                }
            }

            $roleList[$role->getId()]['childrenRoles'] = $sChildrenRoles;

            if(is_array($role->getSysUserMembers()))
            {
                $roleList[$role->getId()]['roleUserMembers'] = lmb_win1251_to_utf8(implode(', ',$role->getSysUserMembers()));
            }
        }
        $this->response->write(json_encode(array_merge($roleList)));

    }

    function doCreateRole()
    {
        $wgsSysRole = new wgsSysRole();
        $wgsSysRole->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $wgsSysRole->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));
        //$wgsSysRole->setLocked($this->request->getPost('locked'));

        //���������� id ����
        $wgsSysRoleId = $this->wgsTier->getSysRoleService()->addRole($wgsSysRole);
        $wgsSysRole->setId($wgsSysRoleId);

        if(count($this->request->getPost('memberUserIds')) > 0)
        {
            $userMembers = $this->request->getPost('memberUserIds');

            foreach ($userMembers as $userMember)
            {
                if($userMember)
                {
                    $wgsSysUser = new wgsSysUser();
                    $wgsSysUser->setId($userMember);
                    $this->wgsTier->getSysUserService()->grantSysRoleMemberships($wgsSysUser, $wgsSysRole);
                }
            }
        }

        if(count($this->request->getPost('childrenRoleIds')) > 0)
        {
            $roleChildren = $this->request->getPost('childrenRoleIds');
            foreach ($roleChildren as $roleChild)
            {
                if($roleChild)
                {
                    $wgsSysRoleChild = new wgsProjectRole();
                    $wgsSysRoleChild->setId($roleChild);
                    $this->wgsTier->getSysRoleService()->grantSysRoleMemberships($wgsSysRoleChild, $wgsSysRole);
                }
            }
        }
    }

    function doEditRole()
    {
        $wgsSysRole = new wgsSysRole();
        $wgsSysRole->setId($this->request->getPost('id'));
        $wgsSysRole->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        //$wgsSysRole->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));
        $wgsSysRole->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));

        //���������� id ����
        //try{
        $this->wgsTier->getSysRoleService()->setRole($wgsSysRole);
        /*}catch(lmbDbException $exception)
         {
         $exception->getCode()=='20003';
         }*/
        //$wgsRoleId = $wgsProjectRole->getId();

        if(count($this->request->getPost('memberNotUserIds')) > 0)
        {
            $userNotMembers = $this->request->getPost('memberNotUserIds');

            foreach ($userNotMembers as $userNotMember)
            {
                if($userNotMember)
                {
                    $wgsSysUser = new wgsSysUser();
                    $wgsSysUser->setId($userNotMember);
                    $this->wgsTier->getSysUserService()->revokeSysRoleMemberships($wgsSysUser, $wgsSysRole);
                }
            }
        }
        if(count($this->request->getPost('childrenNotRoleIds')) > 0)
        {
            $roleNotChildren = $this->request->getPost('childrenNotRoleIds');
            foreach ($roleNotChildren as $roleNotChild)
            {
                if($roleNotChild)
                {
                    $wgsSysRoleChild = new wgsSysRole();
                    $wgsSysRoleChild->setId($roleNotChild);
                    $this->wgsTier->getSysRoleService()->revokeSysRoleMemberships($wgsSysRoleChild);
                }
            }
        }

        if(count($this->request->getPost('memberUserIds')) > 0)
        {
            $userMembers = $this->request->getPost('memberUserIds');

            foreach ($userMembers as $userMember)
            {
                if($userMember)
                {
                    $wgsSysUser = new wgsSysUser();
                    $wgsSysUser->setId($userMember);
                    $this->wgsTier->getSysUserService()->grantSysRoleMemberships($wgsSysUser, $wgsSysRole);
                }
            }
        }
        if(count($this->request->getPost('childrenRoleIds')) > 0)
        {
            $roleChildren = $this->request->getPost('childrenRoleIds');
            foreach ($roleChildren as $roleChild)
            {
                if($roleChild)
                {
                    $wgsSysRoleChild = new wgsSysRole();
                    $wgsSysRoleChild->setId($roleChild);
                    $this->wgsTier->getSysRoleService()->grantSysRoleMemberships($wgsSysRoleChild, $wgsSysRole);
                }
            }
        }

    }

    function doRemoveRole ()
    {
        if(count($this->request->getPost('rolesForRemoveIds')) > 0)
        {
            $wgsRoleIds = $this->request->getPost('rolesForRemoveIds');

            foreach ($wgsRoleIds as $wgsRoleId)
            {
                $wgsSysRole = new wgsSysRole();
                $wgsSysRole->setId($wgsRoleId);
                $this->wgsTier->getSysRoleService()->removeRole($wgsSysRole);
            }
        }
    }

    function doGetRoleProperties ()
    {
        $editRole = $this->wgsTier->getSysRoleService()->getRoleById($this->request->getPost('id'));
        $roleList[$editRole->getId()]['roleId'] = $editRole->getId();
        $roleList[$editRole->getId()]['roleName'] = lmb_win1251_to_utf8($editRole->getName());
        $roleList[$editRole->getId()]['roleDescription'] = lmb_win1251_to_utf8($editRole->getDescription());
        $this->response->write(json_encode(array_merge($roleList)));
    }

}
