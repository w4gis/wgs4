<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class RoleRelationController extends wgsFrontController
{
    private $roles;

    function doGetProjectsWithRoles()
    {   
        $sysProjectList = $this->wgsTier->getSysObjectService()->getSysProjectService()->getProjectsWithRoles();
         
        $projectList = array();

        foreach ($sysProjectList as $sysProject)
        {
            $id = $sysProject->getId();
            $projectList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($sysProject->getName()),
                'leaf'=>false
            );
             
            $roles = $sysProject->getSysProjectRoles();

            foreach($roles as $role)
            {
                $idrole = $role->getId();
                $projectList[$id][$idrole] = array(
                'id' => $id.'_'.$idrole,
                'text' => lmb_win1251_to_utf8($role->getName()),
                'leaf' => true
                );
            }

        }

        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($projectList[$_POST['node']])));
    }

    function doGetSystemRoles()
    {
        $post = explode('_',$this->request->getPost('id'));

        $sysProject = new wgsSysProject();
        $sysProject->setId($post[0]);

        $wgsRole = new wgsProjectRole();
        $wgsRole->setId($post[1]);

        $assignSysRolesIds = array();
        $assignSysRoles = $this->wgsTier->getSysRoleService()->getSysRoleMemberships($sysProject,$wgsRole);

        foreach($assignSysRoles as $assignSysRole)
        $assignSysRolesIds[] = $assignSysRole->getId();

        $highestSysRoles = $this->wgsTier->getSysRoleService()->getHighestSysRoles();
        $sysRoleList = array();

        foreach ($highestSysRoles as $highestSysRole)
        {
            if($this->wgsTier->getSysRoleService()->getChildrenRolesById($highestSysRole->getId()))
            {
                $image = "<img src='/images/root.gif'>";
            }
            else
            {
                $image = "<img src='/images/leaf.gif'>";
            }
            $padding = 0;
            $this->roles[$highestSysRole->getId()]['roleId'] = $highestSysRole->getId();
            if($highestSysRole->getDescription())
            $this->roles[$highestSysRole->getId()]['roleName'] = "<div style='padding-left:".$padding."px;'>".$image.'     '.lmb_win1251_to_utf8($highestSysRole->getName()).' ('.lmb_win1251_to_utf8($highestSysRole->getDescription()).')';
            else
            $this->roles[$highestSysRole->getId()]['roleName'] = "<div style='padding-left:".$padding."px;'>".$image.'     '.lmb_win1251_to_utf8($highestSysRole->getName());
            if(in_array($highestSysRole->getId(), $assignSysRolesIds))
            $this->roles[$highestSysRole->getId()]['roleChecked'] = 1;
            else
            $this->roles[$highestSysRole->getId()]['roleChecked'] = '';
            $sysRoleList[$highestSysRole->getId()] = $this->getChildrenRoles($padding,$highestSysRole,$assignSysRolesIds);

        }
        $this->response->write(json_encode(array_merge($this->roles)));


    }

    function getChildrenRoles($padding,$sysRole, $assignSysRolesIds)
    {
        $childrenRoles = $this->wgsTier->getSysRoleService()->getChildrenRolesById($sysRole->getId());
        if(count($childrenRoles)>0)
        {
            $padding = $padding+15;
            foreach ($childrenRoles as $childrenRole)
            {
                if($this->wgsTier->getSysRoleService()->getChildrenRolesById($childrenRole->getId()))
                {
                    $image = "<img src='/images/root.gif'>";
                }
                else
                {
                    $image = "<img src='/images/leaf.gif'>";
                }

                
                $this->roles[$childrenRole->getId()]['roleId'] = $childrenRole->getId();
                if($childrenRole->getDescription())
                $this->roles[$childrenRole->getId()]['roleName'] = "<div style='padding-left:".$padding."px;'>".$image.'     '.lmb_win1251_to_utf8($childrenRole->getName()).' ('.lmb_win1251_to_utf8($childrenRole->getDescription()).')';
                else
                $this->roles[$childrenRole->getId()]['roleName'] = "<div style='padding-left:".$padding."px;'>".$image.'     '.lmb_win1251_to_utf8($childrenRole->getName());
                if(in_array($childrenRole->getId(), $assignSysRolesIds))
                $this->roles[$childrenRole->getId()]['roleChecked'] = 1;
                else
                $this->roles[$childrenRole->getId()]['roleChecked'] = '';
                if($this->wgsTier->getSysRoleService()->getChildrenRolesById($childrenRole->getId()))
                {
                    $this->getChildrenRoles($padding ,$childrenRole, $assignSysRolesIds);
                }


            }
        }
    }

    function doAssignRoles()
    {
        $wgsProjectRole = new wgsProjectRole();
        $wgsProjectRole->setId($this->request->getPost('wgsRoleId'));
        $sysProject = new wgsSysProject();
        $sysProject->setId($this->request->getPost('sysProjectId'));

        if(count($this->request->getPost('unAssignedRoles')) > 0)
        {
            $unAssignedRoles = $this->request->getPost('unAssignedRoles');

            foreach ($unAssignedRoles as $unAssignedRole)
            {
                if($unAssignedRole)
                {
                    $wgsSysRole = new wgsSysRole();
                    $wgsSysRole->setId($unAssignedRole);
                    $this->wgsTier->getSysRoleService()->revokeSysRoleWgsRole($sysProject, $wgsSysRole, $wgsProjectRole);
                }
            }
        }
        if(count($this->request->getPost('assignedRoles')) > 0)
        {
            $assignedRoles = $this->request->getPost('assignedRoles');

            foreach ($assignedRoles as $assignedRole)
            {
                if($assignedRole)
                {
                    $wgsSysRole = new wgsSysRole();
                    $wgsSysRole->setId($assignedRole);
                    $this->wgsTier->getSysRoleService()->grantSysRoleWgsRole($sysProject, $wgsSysRole, $wgsProjectRole);
                }
            }
        }
    }

}



?>