<?php

class ArrayOfSemestrStudyDTO
{

    /**
     * @var SemestrStudyDTO[] $SemestrStudyDTO
     */
    protected $SemestrStudyDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SemestrStudyDTO[]
     */
    public function getSemestrStudyDTO()
    {
      return $this->SemestrStudyDTO;
    }

    /**
     * @param SemestrStudyDTO[] $SemestrStudyDTO
     * @return ArrayOfSemestrStudyDTO
     */
    public function setSemestrStudyDTO(array $SemestrStudyDTO)
    {
      $this->SemestrStudyDTO = $SemestrStudyDTO;
      return $this;
    }

}
