<?php

class GetSDDate
{

    /**
     * @var int $studentId
     */
    protected $studentId = null;

    /**
     * @param int $studentId
     */
    public function __construct($studentId)
    {
      $this->studentId = $studentId;
    }

    /**
     * @return int
     */
    public function getStudentId()
    {
      return $this->studentId;
    }

    /**
     * @param int $studentId
     * @return GetSDDate
     */
    public function setStudentId($studentId)
    {
      $this->studentId = $studentId;
      return $this;
    }

}
