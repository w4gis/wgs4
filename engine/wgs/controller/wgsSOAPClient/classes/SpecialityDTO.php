<?php

class SpecialityDTO
{

    /**
     * @var int $SpecialityId
     */
    protected $SpecialityId = null;

    /**
     * @var string $SpecialityCode
     */
    protected $SpecialityCode = null;

    /**
     * @var string $SpecialityName
     */
    protected $SpecialityName = null;

    /**
     * @var string $SpecialityTypeName
     */
    protected $SpecialityTypeName = null;

    /**
     * @param int $SpecialityId
     * @param string $SpecialityCode
     * @param string $SpecialityName
     * @param string $SpecialityTypeName
     */
    public function __construct($SpecialityId, $SpecialityCode, $SpecialityName, $SpecialityTypeName)
    {
      $this->SpecialityId = $SpecialityId;
      $this->SpecialityCode = $SpecialityCode;
      $this->SpecialityName = $SpecialityName;
      $this->SpecialityTypeName = $SpecialityTypeName;
    }

    /**
     * @return int
     */
    public function getSpecialityId()
    {
      return $this->SpecialityId;
    }

    /**
     * @param int $SpecialityId
     * @return SpecialityDTO
     */
    public function setSpecialityId($SpecialityId)
    {
      $this->SpecialityId = $SpecialityId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityCode()
    {
      return $this->SpecialityCode;
    }

    /**
     * @param string $SpecialityCode
     * @return SpecialityDTO
     */
    public function setSpecialityCode($SpecialityCode)
    {
      $this->SpecialityCode = $SpecialityCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityName()
    {
      return $this->SpecialityName;
    }

    /**
     * @param string $SpecialityName
     * @return SpecialityDTO
     */
    public function setSpecialityName($SpecialityName)
    {
      $this->SpecialityName = $SpecialityName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityTypeName()
    {
      return $this->SpecialityTypeName;
    }

    /**
     * @param string $SpecialityTypeName
     * @return SpecialityDTO
     */
    public function setSpecialityTypeName($SpecialityTypeName)
    {
      $this->SpecialityTypeName = $SpecialityTypeName;
      return $this;
    }

}
