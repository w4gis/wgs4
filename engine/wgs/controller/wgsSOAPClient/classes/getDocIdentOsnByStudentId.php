<?php

class getDocIdentOsnByStudentId
{

    /**
     * @var long $student_id
     */
    protected $student_id = null;

    /**
     * @param long $student_id
     */
    public function __construct($student_id)
    {
      $this->student_id = $student_id;
    }

    /**
     * @return long
     */
    public function getStudentId()
    {
      return $this->student_id;
    }

    /**
     * @param long $student_id
     * @return getDocIdentOsnByStudentId
     */
    public function setStudentId($student_id)
    {
      $this->student_id = $student_id;
      return $this;
    }

}
