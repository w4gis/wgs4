<?php

class GetScholarshipOrderByOrderHeaderId
{

    /**
     * @var int $orderHeaderId
     */
    protected $orderHeaderId = null;

    /**
     * @param int $orderHeaderId
     */
    public function __construct($orderHeaderId)
    {
      $this->orderHeaderId = $orderHeaderId;
    }

    /**
     * @return int
     */
    public function getOrderHeaderId()
    {
      return $this->orderHeaderId;
    }

    /**
     * @param int $orderHeaderId
     * @return GetScholarshipOrderByOrderHeaderId
     */
    public function setOrderHeaderId($orderHeaderId)
    {
      $this->orderHeaderId = $orderHeaderId;
      return $this;
    }

}
