<?php

class StudentRoomDTO
{

    /**
     * @var string $StudentId
     */
    protected $StudentId = null;

    /**
     * @var string $StudentName
     */
    protected $StudentName = null;

    /**
     * @var int $HostelId
     */
    protected $HostelId = null;

    /**
     * @var string $RoomNumberPart1
     */
    protected $RoomNumberPart1 = null;

    /**
     * @var string $RoomNumberPart2
     */
    protected $RoomNumberPart2 = null;

    /**
     * @var string $RoomNumberPart3
     */
    protected $RoomNumberPart3 = null;

    /**
     * @var int $FacultyId
     */
    protected $FacultyId = null;

    /**
     * @param string $StudentId
     * @param string $StudentName
     * @param int $HostelId
     * @param string $RoomNumberPart1
     * @param string $RoomNumberPart2
     * @param string $RoomNumberPart3
     * @param int $FacultyId
     */
    public function __construct($StudentId, $StudentName, $HostelId, $RoomNumberPart1, $RoomNumberPart2, $RoomNumberPart3, $FacultyId)
    {
      $this->StudentId = $StudentId;
      $this->StudentName = $StudentName;
      $this->HostelId = $HostelId;
      $this->RoomNumberPart1 = $RoomNumberPart1;
      $this->RoomNumberPart2 = $RoomNumberPart2;
      $this->RoomNumberPart3 = $RoomNumberPart3;
      $this->FacultyId = $FacultyId;
    }

    /**
     * @return string
     */
    public function getStudentId()
    {
      return $this->StudentId;
    }

    /**
     * @param string $StudentId
     * @return StudentRoomDTO
     */
    public function setStudentId($StudentId)
    {
      $this->StudentId = $StudentId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudentName()
    {
      return $this->StudentName;
    }

    /**
     * @param string $StudentName
     * @return StudentRoomDTO
     */
    public function setStudentName($StudentName)
    {
      $this->StudentName = $StudentName;
      return $this;
    }

    /**
     * @return int
     */
    public function getHostelId()
    {
      return $this->HostelId;
    }

    /**
     * @param int $HostelId
     * @return StudentRoomDTO
     */
    public function setHostelId($HostelId)
    {
      $this->HostelId = $HostelId;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomNumberPart1()
    {
      return $this->RoomNumberPart1;
    }

    /**
     * @param string $RoomNumberPart1
     * @return StudentRoomDTO
     */
    public function setRoomNumberPart1($RoomNumberPart1)
    {
      $this->RoomNumberPart1 = $RoomNumberPart1;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomNumberPart2()
    {
      return $this->RoomNumberPart2;
    }

    /**
     * @param string $RoomNumberPart2
     * @return StudentRoomDTO
     */
    public function setRoomNumberPart2($RoomNumberPart2)
    {
      $this->RoomNumberPart2 = $RoomNumberPart2;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomNumberPart3()
    {
      return $this->RoomNumberPart3;
    }

    /**
     * @param string $RoomNumberPart3
     * @return StudentRoomDTO
     */
    public function setRoomNumberPart3($RoomNumberPart3)
    {
      $this->RoomNumberPart3 = $RoomNumberPart3;
      return $this;
    }

    /**
     * @return int
     */
    public function getFacultyId()
    {
      return $this->FacultyId;
    }

    /**
     * @param int $FacultyId
     * @return StudentRoomDTO
     */
    public function setFacultyId($FacultyId)
    {
      $this->FacultyId = $FacultyId;
      return $this;
    }

}
