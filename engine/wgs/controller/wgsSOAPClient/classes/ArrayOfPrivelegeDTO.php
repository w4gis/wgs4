<?php

class ArrayOfPrivelegeDTO
{

    /**
     * @var PrivelegeDTO[] $PrivelegeDTO
     */
    protected $PrivelegeDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PrivelegeDTO[]
     */
    public function getPrivelegeDTO()
    {
      return $this->PrivelegeDTO;
    }

    /**
     * @param PrivelegeDTO[] $PrivelegeDTO
     * @return ArrayOfPrivelegeDTO
     */
    public function setPrivelegeDTO(array $PrivelegeDTO)
    {
      $this->PrivelegeDTO = $PrivelegeDTO;
      return $this;
    }

}
