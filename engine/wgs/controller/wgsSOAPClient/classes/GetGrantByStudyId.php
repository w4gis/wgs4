<?php

class GetGrantByStudyId
{

    /**
     * @var int $studyId
     */
    protected $studyId = null;

    /**
     * @param int $studyId
     */
    public function __construct($studyId)
    {
      $this->studyId = $studyId;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->studyId;
    }

    /**
     * @param int $studyId
     * @return GetGrantByStudyId
     */
    public function setStudyId($studyId)
    {
      $this->studyId = $studyId;
      return $this;
    }

}
