<?php

class GetActiveEducationsResponse
{

    /**
     * @var ArrayOfEducationDTO $GetActiveEducationsResult
     */
    protected $GetActiveEducationsResult = null;

    /**
     * @param ArrayOfEducationDTO $GetActiveEducationsResult
     */
    public function __construct($GetActiveEducationsResult)
    {
      $this->GetActiveEducationsResult = $GetActiveEducationsResult;
    }

    /**
     * @return ArrayOfEducationDTO
     */
    public function getGetActiveEducationsResult()
    {
      return $this->GetActiveEducationsResult;
    }

    /**
     * @param ArrayOfEducationDTO $GetActiveEducationsResult
     * @return GetActiveEducationsResponse
     */
    public function setGetActiveEducationsResult($GetActiveEducationsResult)
    {
      $this->GetActiveEducationsResult = $GetActiveEducationsResult;
      return $this;
    }

}
