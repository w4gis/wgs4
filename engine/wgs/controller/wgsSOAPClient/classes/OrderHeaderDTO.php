<?php

class OrderHeaderDTO
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Number
     */
    protected $Number = null;

    /**
     * @var \DateTime $OrderDate
     */
    protected $OrderDate = null;

    /**
     * @var \DateTime $CreateDate
     */
    protected $CreateDate = null;

    /**
     * @var \DateTime $HandleDate
     */
    protected $HandleDate = null;

    /**
     * @var SubDivisionDTO $SubDivision
     */
    protected $SubDivision = null;

    /**
     * @param int $Id
     * @param string $Number
     * @param \DateTime $OrderDate
     * @param \DateTime $CreateDate
     * @param \DateTime $HandleDate
     * @param SubDivisionDTO $SubDivision
     */
    public function __construct($Id, $Number, \DateTime $OrderDate, \DateTime $CreateDate, \DateTime $HandleDate, $SubDivision)
    {
      $this->Id = $Id;
      $this->Number = $Number;
      $this->OrderDate = $OrderDate->format(\DateTime::ATOM);
      $this->CreateDate = $CreateDate->format(\DateTime::ATOM);
      $this->HandleDate = $HandleDate->format(\DateTime::ATOM);
      $this->SubDivision = $SubDivision;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return OrderHeaderDTO
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
      return $this->Number;
    }

    /**
     * @param string $Number
     * @return OrderHeaderDTO
     */
    public function setNumber($Number)
    {
      $this->Number = $Number;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrderDate()
    {
      if ($this->OrderDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->OrderDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $OrderDate
     * @return OrderHeaderDTO
     */
    public function setOrderDate(\DateTime $OrderDate)
    {
      $this->OrderDate = $OrderDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
      if ($this->CreateDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CreateDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CreateDate
     * @return OrderHeaderDTO
     */
    public function setCreateDate(\DateTime $CreateDate)
    {
      $this->CreateDate = $CreateDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHandleDate()
    {
      if ($this->HandleDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HandleDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HandleDate
     * @return OrderHeaderDTO
     */
    public function setHandleDate(\DateTime $HandleDate)
    {
      $this->HandleDate = $HandleDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return SubDivisionDTO
     */
    public function getSubDivision()
    {
      return $this->SubDivision;
    }

    /**
     * @param SubDivisionDTO $SubDivision
     * @return OrderHeaderDTO
     */
    public function setSubDivision($SubDivision)
    {
      $this->SubDivision = $SubDivision;
      return $this;
    }

}
