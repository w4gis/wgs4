<?php

class CitizenshipDTO
{

    /**
     * @var int $CitizenshipId
     */
    protected $CitizenshipId = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @param int $CitizenshipId
     * @param string $Name
     * @param string $Code
     */
    public function __construct($CitizenshipId, $Name, $Code)
    {
      $this->CitizenshipId = $CitizenshipId;
      $this->Name = $Name;
      $this->Code = $Code;
    }

    /**
     * @return int
     */
    public function getCitizenshipId()
    {
      return $this->CitizenshipId;
    }

    /**
     * @param int $CitizenshipId
     * @return CitizenshipDTO
     */
    public function setCitizenshipId($CitizenshipId)
    {
      $this->CitizenshipId = $CitizenshipId;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return CitizenshipDTO
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return CitizenshipDTO
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

}
