<?php

class AddressPrefixDTO
{

    /**
     * @var int $id
     */
    protected $id = null;

    /**
     * @var string $prefix
     */
    protected $prefix = null;

    /**
     * @param int $id
     * @param string $prefix
     */
    public function __construct($id, $prefix)
    {
      $this->id = $id;
      $this->prefix = $prefix;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param int $id
     * @return AddressPrefixDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
      return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return AddressPrefixDTO
     */
    public function setPrefix($prefix)
    {
      $this->prefix = $prefix;
      return $this;
    }

}
