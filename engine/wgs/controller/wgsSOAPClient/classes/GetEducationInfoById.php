<?php

class GetEducationInfoById
{

    /**
     * @var int $educationId
     */
    protected $educationId = null;

    /**
     * @param int $educationId
     */
    public function __construct($educationId)
    {
      $this->educationId = $educationId;
    }

    /**
     * @return int
     */
    public function getEducationId()
    {
      return $this->educationId;
    }

    /**
     * @param int $educationId
     * @return GetEducationInfoById
     */
    public function setEducationId($educationId)
    {
      $this->educationId = $educationId;
      return $this;
    }

}
