<?php

class GetAllSemStudyByStudentID
{

    /**
     * @var int $student_id
     */
    protected $student_id = null;

    /**
     * @param int $student_id
     */
    public function __construct($student_id)
    {
      $this->student_id = $student_id;
    }

    /**
     * @return int
     */
    public function getStudent_id()
    {
      return $this->student_id;
    }

    /**
     * @param int $student_id
     * @return GetAllSemStudyByStudentID
     */
    public function setStudent_id($student_id)
    {
      $this->student_id = $student_id;
      return $this;
    }

}
