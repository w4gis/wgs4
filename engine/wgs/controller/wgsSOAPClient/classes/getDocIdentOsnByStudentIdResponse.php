<?php

class getDocIdentOsnByStudentIdResponse
{

    /**
     * @var ArrayOfDocIdentDTO $getDocIdentOsnByStudentIdResult
     */
    protected $getDocIdentOsnByStudentIdResult = null;

    /**
     * @param ArrayOfDocIdentDTO $getDocIdentOsnByStudentIdResult
     */
    public function __construct($getDocIdentOsnByStudentIdResult)
    {
      $this->getDocIdentOsnByStudentIdResult = $getDocIdentOsnByStudentIdResult;
    }

    /**
     * @return ArrayOfDocIdentDTO
     */
    public function getGetDocIdentOsnByStudentIdResult()
    {
      return $this->getDocIdentOsnByStudentIdResult;
    }

    /**
     * @param ArrayOfDocIdentDTO $getDocIdentOsnByStudentIdResult
     * @return getDocIdentOsnByStudentIdResponse
     */
    public function setGetDocIdentOsnByStudentIdResult($getDocIdentOsnByStudentIdResult)
    {
      $this->getDocIdentOsnByStudentIdResult = $getDocIdentOsnByStudentIdResult;
      return $this;
    }

}
