<?php

class GetActiveSubFacultysResponse
{

    /**
     * @var ArrayOfSubFacultyDTO $GetActiveSubFacultysResult
     */
    protected $GetActiveSubFacultysResult = null;

    /**
     * @param ArrayOfSubFacultyDTO $GetActiveSubFacultysResult
     */
    public function __construct($GetActiveSubFacultysResult)
    {
      $this->GetActiveSubFacultysResult = $GetActiveSubFacultysResult;
    }

    /**
     * @return ArrayOfSubFacultyDTO
     */
    public function getGetActiveSubFacultysResult()
    {
      return $this->GetActiveSubFacultysResult;
    }

    /**
     * @param ArrayOfSubFacultyDTO $GetActiveSubFacultysResult
     * @return GetActiveSubFacultysResponse
     */
    public function setGetActiveSubFacultysResult($GetActiveSubFacultysResult)
    {
      $this->GetActiveSubFacultysResult = $GetActiveSubFacultysResult;
      return $this;
    }

}
