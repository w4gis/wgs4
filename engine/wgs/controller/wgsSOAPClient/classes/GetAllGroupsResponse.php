<?php

class GetAllGroupsResponse
{

    /**
     * @var ArrayOfGroupDTO $GetAllGroupsResult
     */
    protected $GetAllGroupsResult = null;

    /**
     * @param ArrayOfGroupDTO $GetAllGroupsResult
     */
    public function __construct($GetAllGroupsResult)
    {
      $this->GetAllGroupsResult = $GetAllGroupsResult;
    }

    /**
     * @return ArrayOfGroupDTO
     */
    public function getGetAllGroupsResult()
    {
      return $this->GetAllGroupsResult;
    }

    /**
     * @param ArrayOfGroupDTO $GetAllGroupsResult
     * @return GetAllGroupsResponse
     */
    public function setGetAllGroupsResult($GetAllGroupsResult)
    {
      $this->GetAllGroupsResult = $GetAllGroupsResult;
      return $this;
    }

}
