<?php

class GetStudentHostelsWithExclusionsResponse
{

    /**
     * @var ArrayOfHostelStudentDTO $GetStudentHostelsWithExclusionsResult
     */
    protected $GetStudentHostelsWithExclusionsResult = null;

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsWithExclusionsResult
     */
    public function __construct($GetStudentHostelsWithExclusionsResult)
    {
      $this->GetStudentHostelsWithExclusionsResult = $GetStudentHostelsWithExclusionsResult;
    }

    /**
     * @return ArrayOfHostelStudentDTO
     */
    public function getGetStudentHostelsWithExclusionsResult()
    {
      return $this->GetStudentHostelsWithExclusionsResult;
    }

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsWithExclusionsResult
     * @return GetStudentHostelsWithExclusionsResponse
     */
    public function setGetStudentHostelsWithExclusionsResult($GetStudentHostelsWithExclusionsResult)
    {
      $this->GetStudentHostelsWithExclusionsResult = $GetStudentHostelsWithExclusionsResult;
      return $this;
    }

}
