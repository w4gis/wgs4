<?php

class GetStudentsByCriteria2
{

    /**
     * @var StudentRequestDTO $studentCriteria
     */
    protected $studentCriteria = null;

    /**
     * @param StudentRequestDTO $studentCriteria
     */
    public function __construct($studentCriteria)
    {
      $this->studentCriteria = $studentCriteria;
    }

    /**
     * @return StudentRequestDTO
     */
    public function getStudentCriteria()
    {
      return $this->studentCriteria;
    }

    /**
     * @param StudentRequestDTO $studentCriteria
     * @return GetStudentsByCriteria2
     */
    public function setStudentCriteria($studentCriteria)
    {
      $this->studentCriteria = $studentCriteria;
      return $this;
    }

}
