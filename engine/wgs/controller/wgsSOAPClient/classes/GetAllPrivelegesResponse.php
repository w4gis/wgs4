<?php

class GetAllPrivelegesResponse
{

    /**
     * @var ArrayOfPrivelegeDTO $GetAllPrivelegesResult
     */
    protected $GetAllPrivelegesResult = null;

    /**
     * @param ArrayOfPrivelegeDTO $GetAllPrivelegesResult
     */
    public function __construct($GetAllPrivelegesResult)
    {
      $this->GetAllPrivelegesResult = $GetAllPrivelegesResult;
    }

    /**
     * @return ArrayOfPrivelegeDTO
     */
    public function getGetAllPrivelegesResult()
    {
      return $this->GetAllPrivelegesResult;
    }

    /**
     * @param ArrayOfPrivelegeDTO $GetAllPrivelegesResult
     * @return GetAllPrivelegesResponse
     */
    public function setGetAllPrivelegesResult($GetAllPrivelegesResult)
    {
      $this->GetAllPrivelegesResult = $GetAllPrivelegesResult;
      return $this;
    }

}
