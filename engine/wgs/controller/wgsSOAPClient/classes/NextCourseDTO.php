<?php

class NextCourseDTO
{

    /**
     * @var guid $id
     */
    protected $id = null;

    /**
     * @var int $studentId
     */
    protected $studentId = null;

    /**
     * @var int $flagDebt
     */
    protected $flagDebt = null;

    /**
     * @param guid $id
     * @param int $studentId
     * @param int $flagDebt
     */
    public function __construct($id, $studentId, $flagDebt)
    {
      $this->id = $id;
      $this->studentId = $studentId;
      $this->flagDebt = $flagDebt;
    }

    /**
     * @return guid
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param guid $id
     * @return NextCourseDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudentId()
    {
      return $this->studentId;
    }

    /**
     * @param int $studentId
     * @return NextCourseDTO
     */
    public function setStudentId($studentId)
    {
      $this->studentId = $studentId;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlagDebt()
    {
      return $this->flagDebt;
    }

    /**
     * @param int $flagDebt
     * @return NextCourseDTO
     */
    public function setFlagDebt($flagDebt)
    {
      $this->flagDebt = $flagDebt;
      return $this;
    }

}
