<?php

class OrderDTO
{

    /**
     * @var int $StudentOrderId
     */
    protected $StudentOrderId = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @var InfoTypeDTO $InfoType
     */
    protected $InfoType = null;

    /**
     * @var OrderDTO $order
     */
    protected $order = null;

    /**
     * @var OrderTypeDTO $orderType
     */
    protected $orderType = null;

    /**
     * @var OrderHeaderDTO $orderHeader
     */
    protected $orderHeader = null;

    /**
     * @param int $StudentOrderId
     * @param int $StudyId
     * @param InfoTypeDTO $InfoType
     * @param OrderDTO $order
     * @param OrderTypeDTO $orderType
     * @param OrderHeaderDTO $orderHeader
     */
    public function __construct($StudentOrderId, $StudyId, $InfoType, $order, $orderType, $orderHeader)
    {
      $this->StudentOrderId = $StudentOrderId;
      $this->StudyId = $StudyId;
      $this->InfoType = $InfoType;
      $this->order = $order;
      $this->orderType = $orderType;
      $this->orderHeader = $orderHeader;
    }

    /**
     * @return int
     */
    public function getStudentOrderId()
    {
      return $this->StudentOrderId;
    }

    /**
     * @param int $StudentOrderId
     * @return OrderDTO
     */
    public function setStudentOrderId($StudentOrderId)
    {
      $this->StudentOrderId = $StudentOrderId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return OrderDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return InfoTypeDTO
     */
    public function getInfoType()
    {
      return $this->InfoType;
    }

    /**
     * @param InfoTypeDTO $InfoType
     * @return OrderDTO
     */
    public function setInfoType($InfoType)
    {
      $this->InfoType = $InfoType;
      return $this;
    }

    /**
     * @return OrderDTO
     */
    public function getOrder()
    {
      return $this->order;
    }

    /**
     * @param OrderDTO $order
     * @return OrderDTO
     */
    public function setOrder($order)
    {
      $this->order = $order;
      return $this;
    }

    /**
     * @return OrderTypeDTO
     */
    public function getOrderType()
    {
      return $this->orderType;
    }

    /**
     * @param OrderTypeDTO $orderType
     * @return OrderDTO
     */
    public function setOrderType($orderType)
    {
      $this->orderType = $orderType;
      return $this;
    }

    /**
     * @return OrderHeaderDTO
     */
    public function getOrderHeader()
    {
      return $this->orderHeader;
    }

    /**
     * @param OrderHeaderDTO $orderHeader
     * @return OrderDTO
     */
    public function setOrderHeader($orderHeader)
    {
      $this->orderHeader = $orderHeader;
      return $this;
    }

}
