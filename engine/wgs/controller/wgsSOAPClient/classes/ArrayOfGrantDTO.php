<?php

class ArrayOfGrantDTO
{

    /**
     * @var GrantDTO[] $GrantDTO
     */
    protected $GrantDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return GrantDTO[]
     */
    public function getGrantDTO()
    {
      return $this->GrantDTO;
    }

    /**
     * @param GrantDTO[] $GrantDTO
     * @return ArrayOfGrantDTO
     */
    public function setGrantDTO(array $GrantDTO)
    {
      $this->GrantDTO = $GrantDTO;
      return $this;
    }

}
