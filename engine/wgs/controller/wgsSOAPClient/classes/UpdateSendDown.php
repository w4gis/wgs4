<?php

class UpdateSendDown
{

    /**
     * @var SendDownDTO $sdDTO
     */
    protected $sdDTO = null;

    /**
     * @param SendDownDTO $sdDTO
     */
    public function __construct($sdDTO)
    {
      $this->sdDTO = $sdDTO;
    }

    /**
     * @return SendDownDTO
     */
    public function getSdDTO()
    {
      return $this->sdDTO;
    }

    /**
     * @param SendDownDTO $sdDTO
     * @return UpdateSendDown
     */
    public function setSdDTO($sdDTO)
    {
      $this->sdDTO = $sdDTO;
      return $this;
    }

}
