<?php

class CanUse
{

    /**
     * @var string $MetodName
     */
    protected $MetodName = null;

    /**
     * @param string $MetodName
     */
    public function __construct($MetodName)
    {
      $this->MetodName = $MetodName;
    }

    /**
     * @return string
     */
    public function getMetodName()
    {
      return $this->MetodName;
    }

    /**
     * @param string $MetodName
     * @return CanUse
     */
    public function setMetodName($MetodName)
    {
      $this->MetodName = $MetodName;
      return $this;
    }

}
