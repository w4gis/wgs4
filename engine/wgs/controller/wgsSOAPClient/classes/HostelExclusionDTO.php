<?php

class HostelExclusionDTO
{

    /**
     * @var int $StudentOrderId
     */
    protected $StudentOrderId = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @var string $Numb
     */
    protected $Numb = null;

    /**
     * @var \DateTime $orderDate
     */
    protected $orderDate = null;

    /**
     * @var \DateTime $beginDate
     */
    protected $beginDate = null;

    /**
     * @var InfoTypeDTO $InfoType
     */
    protected $InfoType = null;

    /**
     * @var OrderDTO $order
     */
    protected $order = null;

    /**
     * @var OrderTypeDTO $orderType
     */
    protected $orderType = null;

    /**
     * @param int $StudentOrderId
     * @param int $StudyId
     * @param string $Numb
     * @param \DateTime $orderDate
     * @param \DateTime $beginDate
     * @param InfoTypeDTO $InfoType
     * @param OrderDTO $order
     * @param OrderTypeDTO $orderType
     */
    public function __construct($StudentOrderId, $StudyId, $Numb, \DateTime $orderDate, \DateTime $beginDate, $InfoType, $order, $orderType)
    {
      $this->StudentOrderId = $StudentOrderId;
      $this->StudyId = $StudyId;
      $this->Numb = $Numb;
      $this->orderDate = $orderDate->format(\DateTime::ATOM);
      $this->beginDate = $beginDate->format(\DateTime::ATOM);
      $this->InfoType = $InfoType;
      $this->order = $order;
      $this->orderType = $orderType;
    }

    /**
     * @return int
     */
    public function getStudentOrderId()
    {
      return $this->StudentOrderId;
    }

    /**
     * @param int $StudentOrderId
     * @return HostelExclusionDTO
     */
    public function setStudentOrderId($StudentOrderId)
    {
      $this->StudentOrderId = $StudentOrderId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return HostelExclusionDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getNumb()
    {
      return $this->Numb;
    }

    /**
     * @param string $Numb
     * @return HostelExclusionDTO
     */
    public function setNumb($Numb)
    {
      $this->Numb = $Numb;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrderDate()
    {
      if ($this->orderDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->orderDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $orderDate
     * @return HostelExclusionDTO
     */
    public function setOrderDate(\DateTime $orderDate)
    {
      $this->orderDate = $orderDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginDate()
    {
      if ($this->beginDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->beginDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $beginDate
     * @return HostelExclusionDTO
     */
    public function setBeginDate(\DateTime $beginDate)
    {
      $this->beginDate = $beginDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return InfoTypeDTO
     */
    public function getInfoType()
    {
      return $this->InfoType;
    }

    /**
     * @param InfoTypeDTO $InfoType
     * @return HostelExclusionDTO
     */
    public function setInfoType($InfoType)
    {
      $this->InfoType = $InfoType;
      return $this;
    }

    /**
     * @return OrderDTO
     */
    public function getOrder()
    {
      return $this->order;
    }

    /**
     * @param OrderDTO $order
     * @return HostelExclusionDTO
     */
    public function setOrder($order)
    {
      $this->order = $order;
      return $this;
    }

    /**
     * @return OrderTypeDTO
     */
    public function getOrderType()
    {
      return $this->orderType;
    }

    /**
     * @param OrderTypeDTO $orderType
     * @return HostelExclusionDTO
     */
    public function setOrderType($orderType)
    {
      $this->orderType = $orderType;
      return $this;
    }

}
