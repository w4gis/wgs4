<?php

class GetEduFormsResponse
{

    /**
     * @var ArrayOfEduFormDTO $GetEduFormsResult
     */
    protected $GetEduFormsResult = null;

    /**
     * @param ArrayOfEduFormDTO $GetEduFormsResult
     */
    public function __construct($GetEduFormsResult)
    {
      $this->GetEduFormsResult = $GetEduFormsResult;
    }

    /**
     * @return ArrayOfEduFormDTO
     */
    public function getGetEduFormsResult()
    {
      return $this->GetEduFormsResult;
    }

    /**
     * @param ArrayOfEduFormDTO $GetEduFormsResult
     * @return GetEduFormsResponse
     */
    public function setGetEduFormsResult($GetEduFormsResult)
    {
      $this->GetEduFormsResult = $GetEduFormsResult;
      return $this;
    }

}
