<?php

class ArrayOfSpecialityDTO
{

    /**
     * @var SpecialityDTO[] $SpecialityDTO
     */
    protected $SpecialityDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SpecialityDTO[]
     */
    public function getSpecialityDTO()
    {
      return $this->SpecialityDTO;
    }

    /**
     * @param SpecialityDTO[] $SpecialityDTO
     * @return ArrayOfSpecialityDTO
     */
    public function setSpecialityDTO(array $SpecialityDTO)
    {
      $this->SpecialityDTO = $SpecialityDTO;
      return $this;
    }

}
