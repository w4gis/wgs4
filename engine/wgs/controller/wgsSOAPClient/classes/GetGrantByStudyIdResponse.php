<?php

class GetGrantByStudyIdResponse
{

    /**
     * @var ArrayOfGrantDTO $GetGrantByStudyIdResult
     */
    protected $GetGrantByStudyIdResult = null;

    /**
     * @param ArrayOfGrantDTO $GetGrantByStudyIdResult
     */
    public function __construct($GetGrantByStudyIdResult)
    {
      $this->GetGrantByStudyIdResult = $GetGrantByStudyIdResult;
    }

    /**
     * @return ArrayOfGrantDTO
     */
    public function getGetGrantByStudyIdResult()
    {
      return $this->GetGrantByStudyIdResult;
    }

    /**
     * @param ArrayOfGrantDTO $GetGrantByStudyIdResult
     * @return GetGrantByStudyIdResponse
     */
    public function setGetGrantByStudyIdResult($GetGrantByStudyIdResult)
    {
      $this->GetGrantByStudyIdResult = $GetGrantByStudyIdResult;
      return $this;
    }

}
