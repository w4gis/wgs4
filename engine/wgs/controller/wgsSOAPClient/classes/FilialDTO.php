<?php

class FilialDTO
{

    /**
     * @var int $FilialId
     */
    protected $FilialId = null;

    /**
     * @var string $FilialName
     */
    protected $FilialName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    /**
     * @param int $FilialId
     * @param string $FilialName
     * @param string $ShortName
     */
    public function __construct($FilialId, $FilialName, $ShortName)
    {
      $this->FilialId = $FilialId;
      $this->FilialName = $FilialName;
      $this->ShortName = $ShortName;
    }

    /**
     * @return int
     */
    public function getFilialId()
    {
      return $this->FilialId;
    }

    /**
     * @param int $FilialId
     * @return FilialDTO
     */
    public function setFilialId($FilialId)
    {
      $this->FilialId = $FilialId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFilialName()
    {
      return $this->FilialName;
    }

    /**
     * @param string $FilialName
     * @return FilialDTO
     */
    public function setFilialName($FilialName)
    {
      $this->FilialName = $FilialName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return FilialDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
