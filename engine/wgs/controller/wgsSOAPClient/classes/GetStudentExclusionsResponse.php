<?php

class GetStudentExclusionsResponse
{

    /**
     * @var ArrayOfHostelExclusionDTO $GetStudentExclusionsResult
     */
    protected $GetStudentExclusionsResult = null;

    /**
     * @param ArrayOfHostelExclusionDTO $GetStudentExclusionsResult
     */
    public function __construct($GetStudentExclusionsResult)
    {
      $this->GetStudentExclusionsResult = $GetStudentExclusionsResult;
    }

    /**
     * @return ArrayOfHostelExclusionDTO
     */
    public function getGetStudentExclusionsResult()
    {
      return $this->GetStudentExclusionsResult;
    }

    /**
     * @param ArrayOfHostelExclusionDTO $GetStudentExclusionsResult
     * @return GetStudentExclusionsResponse
     */
    public function setGetStudentExclusionsResult($GetStudentExclusionsResult)
    {
      $this->GetStudentExclusionsResult = $GetStudentExclusionsResult;
      return $this;
    }

}
