<?php

class SpecTypeDTO
{

    /**
     * @var int $SpecialityTypeId
     */
    protected $SpecialityTypeId = null;

    /**
     * @var string $SpecialityType
     */
    protected $SpecialityType = null;

    /**
     * @param int $SpecialityTypeId
     * @param string $SpecialityType
     */
    public function __construct($SpecialityTypeId, $SpecialityType)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      $this->SpecialityType = $SpecialityType;
    }

    /**
     * @return int
     */
    public function getSpecialityTypeId()
    {
      return $this->SpecialityTypeId;
    }

    /**
     * @param int $SpecialityTypeId
     * @return SpecTypeDTO
     */
    public function setSpecialityTypeId($SpecialityTypeId)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialityType()
    {
      return $this->SpecialityType;
    }

    /**
     * @param string $SpecialityType
     * @return SpecTypeDTO
     */
    public function setSpecialityType($SpecialityType)
    {
      $this->SpecialityType = $SpecialityType;
      return $this;
    }

}
