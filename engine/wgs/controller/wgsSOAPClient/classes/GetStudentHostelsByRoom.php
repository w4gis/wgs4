<?php

class GetStudentHostelsByRoom
{

    /**
     * @var int $roomNumber
     */
    protected $roomNumber = null;

    /**
     * @var int $hostelId
     */
    protected $hostelId = null;

    /**
     * @param int $roomNumber
     * @param int $hostelId
     */
    public function __construct($roomNumber, $hostelId)
    {
      $this->roomNumber = $roomNumber;
      $this->hostelId = $hostelId;
    }

    /**
     * @return int
     */
    public function getRoomNumber()
    {
      return $this->roomNumber;
    }

    /**
     * @param int $roomNumber
     * @return GetStudentHostelsByRoom
     */
    public function setRoomNumber($roomNumber)
    {
      $this->roomNumber = $roomNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getHostelId()
    {
      return $this->hostelId;
    }

    /**
     * @param int $hostelId
     * @return GetStudentHostelsByRoom
     */
    public function setHostelId($hostelId)
    {
      $this->hostelId = $hostelId;
      return $this;
    }

}
