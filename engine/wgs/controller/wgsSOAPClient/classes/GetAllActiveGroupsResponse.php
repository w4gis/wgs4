<?php

class GetAllActiveGroupsResponse
{

    /**
     * @var ArrayOfGroupDTO $GetAllActiveGroupsResult
     */
    protected $GetAllActiveGroupsResult = null;

    /**
     * @param ArrayOfGroupDTO $GetAllActiveGroupsResult
     */
    public function __construct($GetAllActiveGroupsResult)
    {
      $this->GetAllActiveGroupsResult = $GetAllActiveGroupsResult;
    }

    /**
     * @return ArrayOfGroupDTO
     */
    public function getGetAllActiveGroupsResult()
    {
      return $this->GetAllActiveGroupsResult;
    }

    /**
     * @param ArrayOfGroupDTO $GetAllActiveGroupsResult
     * @return GetAllActiveGroupsResponse
     */
    public function setGetAllActiveGroupsResult($GetAllActiveGroupsResult)
    {
      $this->GetAllActiveGroupsResult = $GetAllActiveGroupsResult;
      return $this;
    }

}
