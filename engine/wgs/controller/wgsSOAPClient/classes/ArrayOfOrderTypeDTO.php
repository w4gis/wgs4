<?php

class ArrayOfOrderTypeDTO
{

    /**
     * @var OrderTypeDTO[] $OrderTypeDTO
     */
    protected $OrderTypeDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OrderTypeDTO[]
     */
    public function getOrderTypeDTO()
    {
      return $this->OrderTypeDTO;
    }

    /**
     * @param OrderTypeDTO[] $OrderTypeDTO
     * @return ArrayOfOrderTypeDTO
     */
    public function setOrderTypeDTO(array $OrderTypeDTO)
    {
      $this->OrderTypeDTO = $OrderTypeDTO;
      return $this;
    }

}
