<?php

class GetScholarshipOrderInfoByCriteria
{

    /**
     * @var int $scholarshipStudyId
     */
    protected $scholarshipStudyId = null;

    /**
     * @param int $scholarshipStudyId
     */
    public function __construct($scholarshipStudyId)
    {
      $this->scholarshipStudyId = $scholarshipStudyId;
    }

    /**
     * @return int
     */
    public function getScholarshipStudyId()
    {
      return $this->scholarshipStudyId;
    }

    /**
     * @param int $scholarshipStudyId
     * @return GetScholarshipOrderInfoByCriteria
     */
    public function setScholarshipStudyId($scholarshipStudyId)
    {
      $this->scholarshipStudyId = $scholarshipStudyId;
      return $this;
    }

}
