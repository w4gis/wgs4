<?php

class ArrayOfStudentAddressDTO
{

    /**
     * @var StudentAddressDTO[] $StudentAddressDTO
     */
    protected $StudentAddressDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StudentAddressDTO[]
     */
    public function getStudentAddressDTO()
    {
      return $this->StudentAddressDTO;
    }

    /**
     * @param StudentAddressDTO[] $StudentAddressDTO
     * @return ArrayOfStudentAddressDTO
     */
    public function setStudentAddressDTO(array $StudentAddressDTO)
    {
      $this->StudentAddressDTO = $StudentAddressDTO;
      return $this;
    }

}
