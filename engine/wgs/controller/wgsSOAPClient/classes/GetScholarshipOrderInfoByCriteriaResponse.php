<?php

class GetScholarshipOrderInfoByCriteriaResponse
{

    /**
     * @var ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCriteriaResult
     */
    protected $GetScholarshipOrderInfoByCriteriaResult = null;

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCriteriaResult
     */
    public function __construct($GetScholarshipOrderInfoByCriteriaResult)
    {
      $this->GetScholarshipOrderInfoByCriteriaResult = $GetScholarshipOrderInfoByCriteriaResult;
    }

    /**
     * @return ArrayOfScholarshipOrderInfoDTO
     */
    public function getGetScholarshipOrderInfoByCriteriaResult()
    {
      return $this->GetScholarshipOrderInfoByCriteriaResult;
    }

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCriteriaResult
     * @return GetScholarshipOrderInfoByCriteriaResponse
     */
    public function setGetScholarshipOrderInfoByCriteriaResult($GetScholarshipOrderInfoByCriteriaResult)
    {
      $this->GetScholarshipOrderInfoByCriteriaResult = $GetScholarshipOrderInfoByCriteriaResult;
      return $this;
    }

}
