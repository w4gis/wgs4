<?php

class GetHostelsResponse
{

    /**
     * @var ArrayOfHostelDTO $GetHostelsResult
     */
    protected $GetHostelsResult = null;

    /**
     * @param ArrayOfHostelDTO $GetHostelsResult
     */
    public function __construct($GetHostelsResult)
    {
      $this->GetHostelsResult = $GetHostelsResult;
    }

    /**
     * @return ArrayOfHostelDTO
     */
    public function getGetHostelsResult()
    {
      return $this->GetHostelsResult;
    }

    /**
     * @param ArrayOfHostelDTO $GetHostelsResult
     * @return GetHostelsResponse
     */
    public function setGetHostelsResult($GetHostelsResult)
    {
      $this->GetHostelsResult = $GetHostelsResult;
      return $this;
    }

}
