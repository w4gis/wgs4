<?php

class SpecializationDTO
{

    /**
     * @var int $SpecializationId
     */
    protected $SpecializationId = null;

    /**
     * @var string $SpecializationName
     */
    protected $SpecializationName = null;

    /**
     * @param int $SpecializationId
     * @param string $SpecializationName
     */
    public function __construct($SpecializationId, $SpecializationName)
    {
      $this->SpecializationId = $SpecializationId;
      $this->SpecializationName = $SpecializationName;
    }

    /**
     * @return int
     */
    public function getSpecializationId()
    {
      return $this->SpecializationId;
    }

    /**
     * @param int $SpecializationId
     * @return SpecializationDTO
     */
    public function setSpecializationId($SpecializationId)
    {
      $this->SpecializationId = $SpecializationId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecializationName()
    {
      return $this->SpecializationName;
    }

    /**
     * @param string $SpecializationName
     * @return SpecializationDTO
     */
    public function setSpecializationName($SpecializationName)
    {
      $this->SpecializationName = $SpecializationName;
      return $this;
    }

}
