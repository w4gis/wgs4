<?php

class SubFacultyDTO
{

    /**
     * @var int $SubFacultyId
     */
    protected $SubFacultyId = null;

    /**
     * @var string $SubFacultyName
     */
    protected $SubFacultyName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    /**
     * @param int $SubFacultyId
     * @param string $SubFacultyName
     * @param string $ShortName
     */
    public function __construct($SubFacultyId, $SubFacultyName, $ShortName)
    {
      $this->SubFacultyId = $SubFacultyId;
      $this->SubFacultyName = $SubFacultyName;
      $this->ShortName = $ShortName;
    }

    /**
     * @return int
     */
    public function getSubFacultyId()
    {
      return $this->SubFacultyId;
    }

    /**
     * @param int $SubFacultyId
     * @return SubFacultyDTO
     */
    public function setSubFacultyId($SubFacultyId)
    {
      $this->SubFacultyId = $SubFacultyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubFacultyName()
    {
      return $this->SubFacultyName;
    }

    /**
     * @param string $SubFacultyName
     * @return SubFacultyDTO
     */
    public function setSubFacultyName($SubFacultyName)
    {
      $this->SubFacultyName = $SubFacultyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return SubFacultyDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
