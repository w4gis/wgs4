<?php

class ArrayOfStudentInfoForEduDTO
{

    /**
     * @var StudentInfoForEduDTO[] $StudentInfoForEduDTO
     */
    protected $StudentInfoForEduDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StudentInfoForEduDTO[]
     */
    public function getStudentInfoForEduDTO()
    {
      return $this->StudentInfoForEduDTO;
    }

    /**
     * @param StudentInfoForEduDTO[] $StudentInfoForEduDTO
     * @return ArrayOfStudentInfoForEduDTO
     */
    public function setStudentInfoForEduDTO(array $StudentInfoForEduDTO)
    {
      $this->StudentInfoForEduDTO = $StudentInfoForEduDTO;
      return $this;
    }

}
