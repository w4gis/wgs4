<?php

class GetScholarshipOrderInfoByNumbAndDateResponse
{

    /**
     * @var ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByNumbAndDateResult
     */
    protected $GetScholarshipOrderInfoByNumbAndDateResult = null;

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByNumbAndDateResult
     */
    public function __construct($GetScholarshipOrderInfoByNumbAndDateResult)
    {
      $this->GetScholarshipOrderInfoByNumbAndDateResult = $GetScholarshipOrderInfoByNumbAndDateResult;
    }

    /**
     * @return ArrayOfScholarshipOrderInfoDTO
     */
    public function getGetScholarshipOrderInfoByNumbAndDateResult()
    {
      return $this->GetScholarshipOrderInfoByNumbAndDateResult;
    }

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByNumbAndDateResult
     * @return GetScholarshipOrderInfoByNumbAndDateResponse
     */
    public function setGetScholarshipOrderInfoByNumbAndDateResult($GetScholarshipOrderInfoByNumbAndDateResult)
    {
      $this->GetScholarshipOrderInfoByNumbAndDateResult = $GetScholarshipOrderInfoByNumbAndDateResult;
      return $this;
    }

}
