<?php

class GetActiveFacultysResponse
{

    /**
     * @var ArrayOfFacultyDTO $GetActiveFacultysResult
     */
    protected $GetActiveFacultysResult = null;

    /**
     * @param ArrayOfFacultyDTO $GetActiveFacultysResult
     */
    public function __construct($GetActiveFacultysResult)
    {
      $this->GetActiveFacultysResult = $GetActiveFacultysResult;
    }

    /**
     * @return ArrayOfFacultyDTO
     */
    public function getGetActiveFacultysResult()
    {
      return $this->GetActiveFacultysResult;
    }

    /**
     * @param ArrayOfFacultyDTO $GetActiveFacultysResult
     * @return GetActiveFacultysResponse
     */
    public function setGetActiveFacultysResult($GetActiveFacultysResult)
    {
      $this->GetActiveFacultysResult = $GetActiveFacultysResult;
      return $this;
    }

}
