<?php

class ArrayOfCitizenshipDTO
{

    /**
     * @var CitizenshipDTO[] $CitizenshipDTO
     */
    protected $CitizenshipDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CitizenshipDTO[]
     */
    public function getCitizenshipDTO()
    {
      return $this->CitizenshipDTO;
    }

    /**
     * @param CitizenshipDTO[] $CitizenshipDTO
     * @return ArrayOfCitizenshipDTO
     */
    public function setCitizenshipDTO(array $CitizenshipDTO)
    {
      $this->CitizenshipDTO = $CitizenshipDTO;
      return $this;
    }

}
