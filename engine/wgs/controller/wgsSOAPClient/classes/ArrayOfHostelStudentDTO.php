<?php

class ArrayOfHostelStudentDTO
{

    /**
     * @var HostelStudentDTO[] $HostelStudentDTO
     */
    protected $HostelStudentDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return HostelStudentDTO[]
     */
    public function getHostelStudentDTO()
    {
      return $this->HostelStudentDTO;
    }

    /**
     * @param HostelStudentDTO[] $HostelStudentDTO
     * @return ArrayOfHostelStudentDTO
     */
    public function setHostelStudentDTO(array $HostelStudentDTO)
    {
      $this->HostelStudentDTO = $HostelStudentDTO;
      return $this;
    }

}
