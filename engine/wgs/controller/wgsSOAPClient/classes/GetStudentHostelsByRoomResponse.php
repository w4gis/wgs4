<?php

class GetStudentHostelsByRoomResponse
{

    /**
     * @var ArrayOfHostelStudentDTO $GetStudentHostelsByRoomResult
     */
    protected $GetStudentHostelsByRoomResult = null;

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsByRoomResult
     */
    public function __construct($GetStudentHostelsByRoomResult)
    {
      $this->GetStudentHostelsByRoomResult = $GetStudentHostelsByRoomResult;
    }

    /**
     * @return ArrayOfHostelStudentDTO
     */
    public function getGetStudentHostelsByRoomResult()
    {
      return $this->GetStudentHostelsByRoomResult;
    }

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsByRoomResult
     * @return GetStudentHostelsByRoomResponse
     */
    public function setGetStudentHostelsByRoomResult($GetStudentHostelsByRoomResult)
    {
      $this->GetStudentHostelsByRoomResult = $GetStudentHostelsByRoomResult;
      return $this;
    }

}
