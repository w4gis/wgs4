<?php

class GetStudentHostelsResponse
{

    /**
     * @var ArrayOfHostelStudentDTO $GetStudentHostelsResult
     */
    protected $GetStudentHostelsResult = null;

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsResult
     */
    public function __construct($GetStudentHostelsResult)
    {
      $this->GetStudentHostelsResult = $GetStudentHostelsResult;
    }

    /**
     * @return ArrayOfHostelStudentDTO
     */
    public function getGetStudentHostelsResult()
    {
      return $this->GetStudentHostelsResult;
    }

    /**
     * @param ArrayOfHostelStudentDTO $GetStudentHostelsResult
     * @return GetStudentHostelsResponse
     */
    public function setGetStudentHostelsResult($GetStudentHostelsResult)
    {
      $this->GetStudentHostelsResult = $GetStudentHostelsResult;
      return $this;
    }

}
