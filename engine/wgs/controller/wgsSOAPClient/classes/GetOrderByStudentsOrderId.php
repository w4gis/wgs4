<?php

class GetOrderByStudentsOrderId
{

    /**
     * @var int $stydyId
     */
    protected $stydyId = null;

    /**
     * @param int $stydyId
     */
    public function __construct($stydyId)
    {
      $this->stydyId = $stydyId;
    }

    /**
     * @return int
     */
    public function getStydyId()
    {
      return $this->stydyId;
    }

    /**
     * @param int $stydyId
     * @return GetOrderByStudentsOrderId
     */
    public function setStydyId($stydyId)
    {
      $this->stydyId = $stydyId;
      return $this;
    }

}
