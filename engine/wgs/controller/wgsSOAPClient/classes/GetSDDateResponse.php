<?php

class GetSDDateResponse
{

    /**
     * @var \DateTime $GetSDDateResult
     */
    protected $GetSDDateResult = null;

    /**
     * @param \DateTime $GetSDDateResult
     */
    public function __construct(\DateTime $GetSDDateResult)
    {
      $this->GetSDDateResult = $GetSDDateResult->format(\DateTime::ATOM);
    }

    /**
     * @return \DateTime
     */
    public function getGetSDDateResult()
    {
      if ($this->GetSDDateResult == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->GetSDDateResult);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $GetSDDateResult
     * @return GetSDDateResponse
     */
    public function setGetSDDateResult(\DateTime $GetSDDateResult)
    {
      $this->GetSDDateResult = $GetSDDateResult->format(\DateTime::ATOM);
      return $this;
    }

}
