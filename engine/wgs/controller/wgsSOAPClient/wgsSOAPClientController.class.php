<?php
/**
 * /wgsSOAPClientController.class.php
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', TRUE);

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/controller/wgsSOAPClient/classes/autoload.php');
lmb_require('wgs/controller/wgsSOAPClient/ConfigContingentArrays.class.php');
lmb_require('wgs/controller/objectadmin/CardController.class.php');

class wgsSOAPClientController extends wgsFrontController
{
	private $client;
	private $config;
	private $contingentStudentArray;
	private $mgTier;
	private $success = false;
	private $IS_ACTIVE_STUDENT = 1;
	private $IS_HOLIDAY_STUDENT = 2;
	private $IS_DISMISS_STUDENT	= 3;
	private $IS_FINISH_STUDENT = 4;
	private $citizenshipDirectory;
	private $citizenshipFileName;

	function __construct() {

		try {
			$this->client = new ContingentInfo();
		} catch (Exception $fault) {
			trigger_error("Couldn't connect to Contingent Server.", E_USER_ERROR);
		}

		$this->config = new ConfigContingentArrays();
		$this->contingentStudentArray = array();
    $this->mgTier = lmbToolkit :: instance() -> getMgTier();
    $this->citizenshipDirectory = __DIR__.'/localfiles/';
		$this->citizenshipFileName = 'AllCitizenships.txt';

    	$this->connectToServer();

		parent::__construct();
	}

	private function connectToServer() {

		try {
			$this->client->LogOn(new LogOn("laesod","test"));
		} catch (Exception $e) {
			trigger_error("Database of Contingent is not accessible", E_USER_ERROR);
            return;
// 			exit();
		}

		try {
			$this->isLogin();
		} catch (Exception $e) {
			trigger_error("Could not login: authorization failed", E_USER_ERROR);
			return;
// 			exit();
		}

	}

	private function searchParams($paramArray, $DTOArray, $valueArray) {

		$paramArrayResponse = array();

		foreach ($paramArray as $paramKey => $paramValue)
			foreach ($DTOArray as $methodKey => $methodValue)
				if ($paramValue["type"]=="class") {
					if((is_object($valueArray->$methodValue())) && (get_class($valueArray->$methodValue()) == $paramValue["class"])) {
						$paramArrayResponse[$paramValue["newName"]] = $this->searchParams( $paramValue["fields"],
																					   $this->config->$paramValue["method"](),
																					   $valueArray->$methodValue()
																					 );
						break;
					}
				} elseif ($paramKey == $methodKey) {
					$paramArrayResponse[$paramValue] = $valueArray->$methodValue();
					break;
				}

		return $paramArrayResponse;
	}

	public function isLogin() {
		$this->client->CanUse(new CanUse("isLogin"));
	}

	public function doGetAllActiveFacultys($paramArray) {

		$facultyDTOArray = $this->config->getFacultyDTOArray();
		$facultysResponse = array();

		try {
           	$facultys = $this->client->GetActiveFacultys(new GetActiveFacultys());
        } catch(Exception $e) {
            trigger_error("Function 'GetActiveFacultys' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($facultys->getGetActiveFacultysResult()->getFacultyDTO() as $faculty) {
			$facultysResponse[] = $this->searchParams($paramArray, $facultyDTOArray, $faculty);
		}

		return $facultysResponse;
	}

	public function doGetAllActiveFacultysJson($paramArray) {
		return json_encode($this->doGetAllActiveFacultys($paramArray));
	}

	public function doGetAllActiveSubFacultys($paramArray)	{

		$subFacultyDTOArray = $this->config->getSubFacultyDTOArray();
		$subFacultysResponse = array();

		try {
			$subFacultys = $this->client->GetActiveSubFacultys(new GetActiveSubFacultys());
        } catch(Exception $e) {
            trigger_error("Function 'GetActiveSubFacultys' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($subFacultys->getGetActiveSubFacultysResult()->getSubFacultyDTO() as $subFaculty) {
			$subFacultysResponse[] = $this->searchParams($paramArray, $subFacultyDTOArray, $subFaculty);
		}

		return $subFacultysResponse;
	}

	public function doGetAllActiveSubFacultysJson($paramArray) {
		return json_encode($this->doGetAllActiveSubFacultys($paramArray));
	}

	public function doGetAllGroups($paramArray) {

		$groupDTOArray = $this->config->getGroupDTOArray();
		$allGroupsResponse = array();

		try {
			$allGroups = $this->client->GetAllGroups(new GetAllGroups());
        } catch(Exception $e) {
            trigger_error("Function 'GetAllGroups' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allGroups->getGetAllGroupsResult()->getGroupDTO() as $group) {
			$allGroupsResponse[] = $this->searchParams($paramArray, $groupDTOArray, $group);
		}

		return $allGroupsResponse;
	}

	public function doGetAllGroupsJson($paramArray) {
		return json_encode($this->doGetAllGroups($paramArray));
	}

	public function doGetAllActiveGroups($paramArray) {

		$groupDTOArray = $this->config->getGroupDTOArray();
		$allActiveGroupsResponse = array();

		try {
			$allActiveGroups = $this->client->GetAllActiveGroups(new GetAllActiveGroups());
        } catch(Exception $e) {
            trigger_error("Function 'GetAllActiveGroups' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allActiveGroups->getGetAllActiveGroupsResult()->getGroupDTO() as $group) {
			$allActiveGroupsResponse[] = $this->searchParams($paramArray, $groupDTOArray, $group);
		}

		return $allActiveGroupsResponse;
	}

	public function doGetAllActiveGroupsJson($paramArray) {
		return json_encode($this->doGetAllActiveGroups($paramArray));
	}

	public function doGetAllEduForms($paramArray) {

		$eduFormsDTOArray = $this->config->getEduFormsDTOArray();
		$eduFormsResponse = array();

		try {
			$eduForms = $this->client->GetEduForms(new GetEduForms());
        } catch(Exception $e) {
            trigger_error("Function 'GetEduForms' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($eduForms->getGetEduFormsResult()->getEduFormDTO() as $eduForm) {
			$eduFormsResponse[] = $this->searchParams($paramArray, $eduFormsDTOArray, $eduForm);
		}

		return $eduFormsResponse;
	}

	public function doGetAllEduFormsJson($paramArray) {
		return json_encode($this->doGetAllEduForms($paramArray));
	}

	public function doGetAllSpeciality($paramArray) {

		$specialityDTOArray = $this->config->getSpecialityDTOArray();
		$allSpecialityResponse = array();

		try {
			$allSpeciality = $this->client->GetAllSpeciality(new GetAllSpeciality());
        } catch(Exception $e) {
            trigger_error("Function 'GetAllSpeciality' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allSpeciality->getGetAllSpecialityResult()->getSpecialityDTO() as $speciality) {
			$allSpecialityResponse[] = $this->searchParams($paramArray, $specialityDTOArray, $speciality);
		}

		return $allSpecialityResponse;
	}

	public function doGetAllSpecialityJson($paramArray) {
		return json_encode($this->doGetAllSpeciality($paramArray));
	}

	public function doGetAllActiveSpeciality($paramArray) {

		$specialityDTOArray = $this->config->getSpecialityDTOArray();
		$allActiveSpecialityResponse = array();

		try {
			$allActiveSpeciality = $this->client->GetActiveSpeciality(new GetActiveSpeciality());
        } catch(Exception $e) {
            trigger_error("Function 'GetActiveSpeciality' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allActiveSpeciality->getGetActiveSpecialityResult()->getSpecialityDTO() as $speciality) {
			$allActiveSpecialityResponse[] = $this->searchParams($paramArray, $specialityDTOArray, $speciality);
		}

		return $allActiveSpecialityResponse;
	}

	public function doGetAllActiveSpecialityJson($paramArray) {
		return json_encode($this->doGetAllActiveSpeciality($paramArray));
	}

	public function doGetAllStudentStates($paramArray)	{

		$studentStateDTOArray = $this->config->getStudentStateDTOArray();
		$allStudentStatesResponse = array();

		try {
			$allStudentStates = $this->client->GetAllStudentStates(new GetAllStudentStates());
        } catch(Exception $e) {
            trigger_error("Function 'GetAllStudentStates' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allStudentStates->getGetAllStudentStatesResult()->getStudentStateDTO() as $studentState) {
			$allStudentStatesResponse[] = $this->searchParams($paramArray, $studentStateDTOArray, $studentState);
		}

		return $allStudentStatesResponse;
	}

	public function doGetAllStudentStatesJson($paramArray) {
		return json_encode($this->doGetAllStudentStates($paramArray));
	}

	public function getActiveStudents($paramArray) {

		$studentDTOArray = $this->config->getStudentDTOArray();
		$studentsResponse = array();

		$criteria = new StudentRequestDTO(0,0,0,0,null,null,null,null,null,0,0,0,0,0,0,0,0,0);

		$criteria->setStudentStateId($this->IS_ACTIVE_STUDENT);

		try {
           $students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentsByCriteria' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($students->getGetStudentsByCriteriaResult()->getStudentDTO() as $student) {
			$studentsResponse[] = $this->searchParams($paramArray, $studentDTOArray, $student);
		}

		return $studentsResponse;
	}

	public function doGetActiveStudentsJson($paramArray) {
		return json_encode($this -> getActiveStudents($paramArray));
	}


	public function GetAllByParams($nameOfParam, $paramArray) {
		switch ($nameOfParam) {
			case "Facultys":
				return $this->doGetAllActiveFacultysJson($paramArray);
				break;
			case "SubFacultys":
				return $this->doGetAllActiveSubFacultysJson($paramArray);
				break;
			case "AllGroups":
				return $this->doGetAllGroupsJson($paramArray);
				break;
			case "AllActiveGroups":
				return $this->doGetAllActiveGroupsJson($paramArray);
				break;
			case "AllEduForms":
				return $this->doGetAllEduFormsJson($paramArray);
				break;
			case "AllSpeciality":
				return $this->doGetAllSpecialityJson($paramArray);
				break;
			case "AllActiveSpeciality":
				return $this->doGetAllActiveSpecialityJson($paramArray);
				break;
			case "AllStudentStates":
				return $this->doGetAllStudentStatesJson($paramArray);
				break;
			case "ActiveStudents":
				return $this->doGetActiveStudentsJson($paramArray);
				break;
			case "Hostels":
				return $this->doGetHostelsJson($paramArray);
				break;
		}
	}

	public function doGetHostels($paramArray) {

		$hostelDTOArray = $this->config->getHostelDTOArray();
		$hostelsResponse = array();

		try {
			$hostels = $this->client->GetHostels(new GetHostels());
        } catch(Exception $e) {
            trigger_error("Function 'GetHostels' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($hostels->getGetHostelsResult()->getHostelDTO() as $hostel) {
			$hostelsResponse[] = $this->searchParams($paramArray, $hostelDTOArray, $hostel);
		}

		return $hostelsResponse;
	}

	public function doGetHostelsJson($paramArray) {
		return json_encode($this -> doGetHostels($paramArray));
	}

	private function getCurrentHostel($studentHostels) {

		if(!is_null($studentHostels->getGetStudentHostelsResult()->getHostelStudentDTO())) {
			$endDate = new DateTime('01/01/1970');
			foreach ($studentHostels->getGetStudentHostelsResult()->getHostelStudentDTO() as $hostel) {
				if (date_format($hostel->getEndDate(),"Y-m-d\TH:i:s") <= date_format($endDate,"Y-m-d\TH:i:s")){
					//echo 'gotcha! ' . date_format($hostel->getEndDate(),"Y-m-d\TH:i:s") . "  VS " . date_format($endDate,"Y-m-d\TH:i:s");
					$currentHostel = $hostel;
				}

				if (date_format($hostel->getEndDate(),"Y-m-d\TH:i:s") >= date("Y-m-d\TH:i:s")) {
					$currentHostel = $hostel;
					//echo 'end date > today //true';
				}
			}
		}

		return $currentHostel;
	}

	public function getStudentById($studentId, $paramArray) {

		$studentRequestArray = $this->config->getstudentDTOArray();

		try {
           $studentArray = $this->client->GetStudentById(new GetStudentById($studentId));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentById' is not available", E_USER_ERROR);
            return null;
        }

        $studentResponse = $this->searchParams($paramArray, $studentRequestArray, $studentArray->getGetStudentByIdResult());

		return $studentResponse;
	}

	public function doGetStudentByIdJson($studentId, $paramArray) {
		return json_encode($this->getStudentById($studentId, $paramArray));
	}

	public function getStudentHostelById($studyId, $paramArray) {

		$hostelStudentDTOArray = $this->config->getHostelStudentDTOArray();
		$hostelStudentResponse = array();

		try {
			$studentHostels = $this->client->GetStudentHostels(new GetStudentHostels($studyId));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentHostels' is not available", E_USER_ERROR);
            return null;
        }

		$currentHostel = $this->getCurrentHostel($studentHostels);

		if(!(is_null($currentHostel))) {
			$hostelStudentResponse = $this->searchParams($paramArray, $hostelStudentDTOArray, $currentHostel);
		}

		return $hostelStudentResponse;
	}

	public function doGetStudentHostelByIdJson($studyId, $paramArray) {
		return json_encode($this -> getStudentHostelById($studyId, $paramArray));
	}

	public function getStudentsIdsByParams($searchArray, $paramArray) {

		$studentDTOArray = $this->config->getStudentDTOArray();
		$studentsResponse = array();

		$criteria = new StudentRequestDTO(0,0,0,0,null,null,null,null,null,0,0,0,0,0,0,0,0,0);

		foreach ($searchArray as $searchKey => $searchValue) {
			$method = 'set'.$searchKey;
			$criteria->$method($searchValue);
		}
		if(!($this->isNull($criteria))) {
			return null;
		}

		try {
			$students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentsByCriteria' is not available", E_USER_ERROR);
            return null;
        }
		foreach ($students->getGetStudentsByCriteriaResult()->getStudentDTO() as $student) {
			$studentsResponse[] = $student->getStudyId();
		}

		/*foreach ($students->getGetStudentsByCriteriaResult()->getStudentDTO() as $student) {
			$studentsResponse[] = $this->searchParams($paramArray, $studentDTOArray, $student);
		}*/

		return $studentsResponse;
	}

	public function getStudentsByParams($searchArray, $paramArray) {

		$studentDTOArray = $this->config->getStudentDTOArray();
		$studentsResponse = array();

		$criteria = new StudentRequestDTO(0,0,0,0,null,null,null,null,null,0,0,0,0,0,0,0,0,0);

		foreach ($searchArray as $searchKey => $searchValue) {
			$method = 'set'.$searchKey;
			$criteria->$method($searchValue);
		}
		if(!($this->isNull($criteria))) {
			return null;
		}

		try {
			$students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentsByCriteria' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($students->getGetStudentsByCriteriaResult()->getStudentDTO() as $student) {
			$studentsResponse[] = $this->searchParams($paramArray, $studentDTOArray, $student);
		}

		return $studentsResponse;
	}

	public function doGetStudentsByParamsJson($searchArray, $paramArray) {
		return json_encode($this->getStudentsByParams($searchArray, $paramArray));
	}

	public function getAddressByStudentId($studyId, $paramArray) {
		$studentAddressDTOArray = $this->config->getStudentAddressDTOArray();
		$studentAddressResponse = array();

		try {
			$studentAddress = $this->client->getAddressByStudentId(new getAddressByStudentId($studyId));
        } catch(Exception $e) {
            trigger_error("Function 'getAddressByStudentId' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($studentAddress->getGetAddressByStudentIdResult()->getStudentAddressDTO() as $address) {
			$studentAddressResponse[] = $this->searchParams($paramArray, $studentAddressDTOArray, $address);
		}

		return $studentAddressResponse;
	}

	public function doGetAddressByStudentIdJson($studyId, $paramArray) {
		return json_encode($this -> getAddressByStudentId($studyId, $paramArray));
	}

	private function objectToArray($object) {

		$resultArray = array();

		$object = (array) $object;

		foreach ($object as $key => $value) {
    		if (is_object($value)) {
    			$value = (array) $value;
    		}
    		if (is_array($value)) {
    			$resultArray[$key] = $this->objectToArray($value);
    		} else {
        		$resultArray[$key] = $value;
    		}
		}

		return $resultArray;
	}

	private function isNull($object) {

		$arr = $this->objectToArray($object);

		foreach ($arr as $value) {
			if($value!==null && $value!==0) {
				$this->success = true;
				return $this->success;
			}
		}

		$this->success = false;

		return $this->success;
	}

	private function cutNInHostelName($hostelNameN) {
		return $hostelName = str_replace("№","",$hostelNameN);
	}

	//Database
    function doGetCount() {
        $this -> response ->write (json_encode(array(
            'total' => $this -> doFillStudentsDataBase(false)
        )));
    }

    function doGetUpdateProgress() {
        // $this -> response ->write (json_encode(array(
        //     'progress' => $this -> mgTier -> getSessionData("progress")
        // )));
        $this -> response ->write(json_encode(array(
            'progress' => $this -> mgTier -> getSessionData("progress"),
            'rprogress' => $this -> mgTier -> getSessionData("rprogress")
        )));
    }

    function doUpdateDb() {
        //return count to Iframe
        $removeInactiveStudents = $this->request->get('remove') == 1 ? true : false;

        $bindNew = $this -> request -> get('bindnew') == 1 ? true : false;
        $bindOld = $this -> request -> get('bindold') == 1 ? true : false;

        $this -> response -> write("<html><body><textarea>" . $this -> doFillStudentsDataBase(true, $removeInactiveStudents, $bindNew, $bindOld) . "</textarea></body></html>");
    }

    //privyazat novykh
    //pereprivyazat starykh
    //setName
	public function doFillStudentsDataBase($getValues = false, $removeInactiveStudents = false, $bindNew = false, $bindOld = false) {
		$PROPERTY_ROOM = 40;
		$PROPERTY_DORM = 39;
		$PROPERTY_SEX = 8;
		$objAdmin = new CardController();

		session_start();
		$this->mgTier->setSessionData("progress", 0);
		$this->mgTier->setSessionData("rprogress", 0);
		session_write_close();


		set_time_limit(3 * 60 * 60 * 60);
		if($bindOld == true) {
			$objAdmin -> unbindAllStudents();
		}

		if($removeInactiveStudents) {
		    session_start();
		    $this->mgTier->setSessionData("rprogress", 0.1);
		    session_write_close();
		    $currentStudentCards = $objAdmin -> doGetCardsByTypeAndLayerId(-1, 3, 0, 1*1000*1000, '', '', false, false);
		    $soapIds = $objAdmin -> getSoapIds($currentStudentCards);
		    $cardNumber = count($soapIds);
		    $j =0;
		    foreach ($soapIds as $studCard) {
		        $j++;
		        session_start();
                $this->mgTier->setSessionData("rprogress", round($j / $cardNumber * 100, 1));
                session_write_close();

		        if(!$this -> isActiveStudent($studCard['soapId'])) {
		            $objAdmin -> removeObjectNew($studCard['objectId'], 0);
		        };
		    }
		}

		if($getValues) {
		    $paramStudentArray = array(
		        "StudyId" 		=> "StudyID",
 				"LastName" 		=> "LastName",
  				"FirstName"		=> "FirstName",
   				"MiddleName" 	=> "MiddleName",
   				"Sex"         => "Sex"
   			);
		} else {
		    $paramStudentArray = array("StudyId" => "StudyID");
		}

		$paramStudentHostelArray = array(
		    "StudentHosteId" => "HostelId",
			"HostelDTO" => array("newName" 	=> "Hostel",
			  						"class" => "HostelDTO",
			  						"method" => "getHostelDTOArray",
								  	"type" 	=> "class",
								  	"fields" => array("HostelId" => "HostelId",
								  						"Name" 	=> "HostelName"
								  						)
			  						),
 			"RoomNumber" => "Room",
  			"StartDate" => "StartDate",
   			"EndDate" 	=> "EndDate",
			"StudyId"	=> "StudentId");

        $activeStudents = $this->getActiveStudents($paramStudentArray);

        // $searchArray = array("FirstName" => 'Артем', 'StudentStateId' => $this -> IS_ACTIVE_STUDENT);
        // $activeStudents = $this->getStudentsByParams($searchArray, $paramStudentArray);

        // // var_dump($activeStudents);
        // die();

        if ($getValues) {
        	if ($activeStudents) {

        		$i = 0;
        		foreach ($activeStudents as $student) {
        			//set_time_limit(5);

        			$studentHostel = $this->getStudentHostelById($student["StudyID"], $paramStudentHostelArray);
        			if(count($studentHostel)) {
						$student["HostelTitle"] = $this->cutNInHostelName($studentHostel["Hostel"]["HostelName"]);

						$student["RoomNumber"] = $studentHostel["Room"];
					}

    				$existedStudentCard = $objAdmin -> doGetCardsByTypeAndLayerId(-1, 3, 0, 1, '7', $student["StudyID"], false, false)['result'];
						// var_dump($existedStudentCard);
    				$name = decode($student["LastName"]) ." ".decode($student["FirstName"])." ".decode($student["MiddleName"]);
    				if(count($existedStudentCard)) {
    				    $childObjectId = $existedStudentCard[0]['cardId'];
    				    $isOld = true;
    				    $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetName($childObjectId, $name);
    				} else {
    					$childObjectId = $objAdmin->addCard(3,
    						$name, "", "", decode('АИС Контингент'), array("property-7" => $student["StudyID"]));
    					$isOld = false;
                    }
// var_dump($childObjectId, $name);
// die();
                    $this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($childObjectId, $PROPERTY_SEX, decode($student["Sex"]));
                    if($student["HostelTitle"] != null) {
						$this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($childObjectId, $PROPERTY_ROOM, decode($student["RoomNumber"]));
						$this->wgsTier->getProjectObjectService()->getAttObjectService()->SetValueProperty($childObjectId, $PROPERTY_DORM, decode($student["HostelTitle"]));
					}

    				$i++;
    				$percent = round($i / count($activeStudents) * 100, 1);

    				session_start();
    				$this -> mgTier -> setSessionData("progress", $percent);
                    session_write_close();

    				if(($isOld && $bindOld) || (!$isOld && $bindNew)) {
        				$featureLayers = $objAdmin->getFeatureLayerByGroupName(decode($student["HostelTitle"]));

        				foreach ($featureLayers as $featureLayer) {
        					if($featureLayer["layer_id"] !=  null && $student["RoomNumber"] != null) {
        						$roomCards = $objAdmin->getObjectsByName($featureLayer["layer_id"], $student["RoomNumber"]);
        						foreach ($roomCards as $roomCard) {
    			    				$objectId = $roomCard['objectId'];
        							$objectTypeId = $roomCard['objectTypeId'];
        							$relationId = $roomCard['relationId'];
        							$objAdmin->bindChildObject($relationId, $objectId, $childObjectId);

    							}
    						}
    				    }
    				}
				}
			}
		}

		return $objAdmin -> updateProps();

		if($getValues) {
		    $updated = file_put_contents("E:\\wgs\\application\\projects\\tusur_production\\application\\projects\\" . getProjectName() . "\\updated", date_format(date_create('now'), 'd.m.y'));
		}

		return count($activeStudents);
	}

	private function addNewStudent($studyId) {
		$paramStudentArray = array( "LastName" 		=> "LastName",
		  							"FirstName" 	=> "FirstName",
		   							"MiddleName" 	=> "MiddleName",
		   							"Education" 	=> array("newName" => "Education",
											  				"class" => "EducationDTO",
														  	"method" => "getEducationDTOArray",
														  	"type" => "class",
														  	"fields" => array(	"EduForm" => array("newName" 	=> "EduForm",
														  										"class" 	=> "EduFormDTO",
														  										"method" 	=> "getEduFormsDTOArray",
																								"type" 		=> "class",
																								"fields" 	=> array("EduFormName"  => "EduFormName"
											   	 	  																)
														  											),
														  						"Faculty" => array("newName" 	=> "Faculty",
														  										"class" 	=> "FacultyDTO",
														  										"method" 	=> "getFacultyDTOArray",
																								"type" 		=> "class",
																								"fields" 	=> array("FacultyName"	 	=> "FacultyName",
											   	  	  																"FacultyShortName"  => "FacultyShortName"
											   	 	  																)
														  											)
														  						)
														  	),
		   							"Sex" 		=> "Sex",
		   							"BirthDate" => "BirthDate",
		   							"Group" 	=> array("newName" 	=> "Group",
											  			"class" 	=> "GroupDTO",
											  			"method" 	=> "getGroupDTOArray",
											  			"type" 		=> "class",
											  			"fields" 	=> array("GroupName" => "GroupName",
											  								"Course" => "Course")
											  			),
		   							"Financing" => "Financing",
		   							"Citizenship" 	=> array("newName" 	=> "Citizenship",
											  			"class" 	=> "CitizenshipDTO",
											  			"method" 	=> "getCitizenshipDTOArray",
											  			"type" 		=> "class",
											  			"fields" 	=> array("CitizenshipId" => "CitizenshipId",
											  								"Name" => "CitizenshipName",
											  								"Code" => "CitizenshipCode"
											  								)
											  			)
		   							);

		$student = $this->getStudentById($studyId, $paramStudentArray);
		$studentPassport = $this->getStudentPassportById($studyId);
		$student['id'] = $studyId;

		if ($studentPassport) {
			$student['passport'] = $studentPassport[0];
		}

		$this->contingentStudentArray[] = $student;

		return $student;
	}

	public function getStudentProperty($studyId, $property) {

		$students = array();

		foreach ($this->contingentStudentArray as $id => $propertyArray) {
			if ($propertyArray['id'] == $studyId) {
				$student = $propertyArray;
                break;
			}
		}

    	if (empty($student)) {
			$student = $this->addNewStudent($studyId);
		}

		switch ($property) {
			case "firstName":
				return $student["FirstName"];
				break;
			case "lastName":
				return $student["LastName"];
				break;
			case "middleName":
				return $student["MiddleName"];
				break;
			case "sex":
				return $student["Sex"];
				break;
			case "birth":
				return date_format($student["BirthDate"],"Y-m-d");
				break;
			case "citizenship":
				return $student["Citizenship"]["CitizenshipName"];
				break;
			case "group":
				return $student["Group"]["GroupName"];
				break;
			case "course":
				return $student["Group"]["Course"];
				break;
			case "faculty":
				return $student["Education"]["Faculty"]["FacultyShortName"];
				break;
			case "facultyLong":
				return $student["Education"]["Faculty"]["FacultyName"];
				break;
			case "education":
				return $student["Education"]["EduForm"]["EduFormName"];
				break;
			case "financing":
				return $student["Financing"];
				break;
			case "doc_ser":
				return $student["passport"]["doc_ser"];
				break;
			case "doc_num":
				return $student["passport"]["doc_num"];
				break;
			case "doc_from":
				return date_format($student["passport"]["doc_from"],"Y-m-d");
				break;
			case "doc_who":
				return $student["passport"]["doc_who"];
				break;
			case "doc_birthplace":
				return $student["passport"]["doc_birthplace"];
				break;

		}
	}

	public function getStudentAddress($studyId) {

		$paramAddressArray = array("id" => "AddressID",
									"postString" => "PostString",
									"addressIndex" => "addressIndex",
									"defaultItem" => "defaultItem",
									"Address" => array("newName" => "Address",
													  	"class" => "AddressDTO",
													  	"method" => "getAddressDTOArray",
													  	"type" => "class",
													  	"fields" => array("id" => "AddressId",
													  						"value" => "AddressValue",
													  						"shortName" => "AddressShortName",
													  						"AddressLevel" => array("newName" => "AddressLevel",
													  											"class" => "AddressLevelDTO",
													  											"method" => "getAddressLevelDTOArray",
																							  	"type" => "class",
																							  	"fields" => array("id"	 => "AddressLevelId",
										   	  	  																	"name"  => "AddressLevelName"
										   	 	  																	)
													  											),
													  						"AddressPrefix" => array("newName" => "AddressPrefix",
													  											"class" => "AddressPrefixDTO",
													  											"method" => "getAddressPrefixDTOArray",
																							  	"type" => "class",
																							  	"fields" => array("id"	 => "AddressPrefix",
										   	  	  																	"prefix"  => "AddressPrefixName"
										   	 	  																	)
													  											),
													  						"AddressParent" => "AddressParent"
													  						)
													  	),
				 					"AddressType" => array("newName" => "AddressType",
													  	"class" => "AddressTypeDTO",
													  	"method" => "getAddressTypeDTOArray",
													  	"type" => "class",
													  	"fields" => array("id" => "AddressTypeId",
													  						"name" => "AddressTypeName"
													  						)
													  	)
				 					);

		$studentAdress = $this->getAddressByStudentId($studyId, $paramAddressArray);

		var_dump($studentAdress);
	}

	private function getFacultyIdByName($facultyName) {
		$paramFacultyArray = array("FacultyId" 			=> "FacultyId",
					 				"FacultyName" 		=> "FacultyName",
					  				"FacultyShortName" 	=> "FacultyShortName");

		$facultys = $this->doGetAllActiveFacultys($paramFacultyArray);

		foreach ($facultys as $faculty) {
			if ($faculty["FacultyName"] == $facultyName || $faculty["FacultyShortName"] == $facultyName) {
				$facultyId = $faculty["FacultyId"];
				break;
			}

		}

		return $facultyId;
	}

	public function getCitizenshipIdByName($citizenshipName) {

		$citizenships = $this->getCitizenshipsFromFile();

		foreach ($citizenships as $citizenship) {
			if ($citizenship->Citizenship->CitizenshipName == $citizenshipName) {
				$citizenshipId = $citizenship->Citizenship->CitizenshipId;
				break;
			}

		}

		return $citizenshipId;
	}

	public function getCitizenshipIdByNameSoap($citizenshipName) {

		$citizenships = $this->client->GetCitizenshipList(new GetCitizenshipList());

		foreach ($citizenships->getGetCitizenshipListResult()->getCitizenshipDTO() as $citizenship) {
			if ($citizenship->getName() == $citizenshipName) {
				$citizenshipId = $citizenship->getCitizenshipId();
				break;
			}

		}

		return $citizenshipId;
	}

	public function getAllStudentIdsProperty($property) {

		$paramStudentArray = array( "StudyId" => "StudyId");

		$searchArray = array(array("LastName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("FirstName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("MiddleName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("GroupName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							// array("CourseGroup" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT)
						);

		try {
			$facultyId = $this->getFacultyIdByName($property);
		} catch (Exception $e) {
			trigger_error("Function 'getFacultyIdByName' is not available", E_USER_ERROR);
			return null;
		}

		$citizenshipId = $this->getCitizenshipIdByNameSoap($property);

		$facultyIdSearch = array("FacultyId" => $facultyId, "StudentStateId" => $this->IS_ACTIVE_STUDENT);
		$citizenshipIdSearch = array("CitizenshipId" => $citizenshipId, "StudentStateId" => $this->IS_ACTIVE_STUDENT);

		if ($facultyId) {
			$studentsArray[] = $this->getStudentsIdsByParams($facultyIdSearch, $paramStudentArray);
		} elseif ($citizenshipId) {
			$studentsArray[] = $this->getStudentsIdsByParams($citizenshipIdSearch, $paramStudentArray);
		} else {
			foreach ($searchArray as $searchArrayValue) {
				$studentsArray[] = $this->getStudentsIdsByParams($searchArrayValue, $paramStudentArray);
			}
		}

		foreach ($studentsArray as $students) {
			if ($students) {
					foreach ($students as $student) {
						$studentsIds[] = $student;
					}
				}
		}

		//Delete unique students
		$uniStudentsIds = array_unique($studentsIds);

		return $uniStudentsIds;
	}

	public function getAllStudentIdsPropertySlow($property) {

		$paramStudentArray = array( "StudyId" => "StudyId");

		$searchArray = array(array("LastName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("FirstName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("MiddleName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							array("GroupName" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT),
							// array("CourseGroup" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT)
						);

		try {
			$facultyId = $this->getFacultyIdByName($property);
		} catch (Exception $e) {
			trigger_error("Function 'getFacultyIdByName' is not available", E_USER_ERROR);
			return null;
		}

		$citizenshipId = $this->getCitizenshipIdByNameSoap($property);

		$facultyIdSearch = array("FacultyId" => $facultyId, "StudentStateId" => $this->IS_ACTIVE_STUDENT);
		$citizenshipIdSearch = array("CitizenshipId" => $property, "StudentStateId" => $this->IS_ACTIVE_STUDENT);

		if ($facultyId) {
			$studentsArray[] = $this->getStudentsByParams($facultyIdSearch, $paramStudentArray);
		} elseif ($citizenshipId) {
			$studentsArray = $this->getStudentsByParams($citizenshipIdSearch, $paramStudentArray);
		} else {
			foreach ($searchArray as $searchArrayValue) {
				$studentsArray[] = $this->getStudentsByParams($searchArrayValue, $paramStudentArray);
			}
		}

		foreach ($studentsArray as $students) {
			if ($students) {
					foreach ($students as $student) {
						$studentsIds[] = $student["StudyId"];
					}
				}
		}

		//Delete unique students
		$uniStudentsIds = array_unique($studentsIds);

		return $uniStudentsIds;
	}

	public function getAllCitizenshipsFromActiveStudents() {

		$paramStudentArray = array( "Citizenship" 	=> array("newName" 	=> "Citizenship",
												  			"class" 	=> "CitizenshipDTO",
												  			"method" 	=> "getCitizenshipDTOArray",
												  			"type" 		=> "class",
												  			"fields" 	=> array("CitizenshipId" => "CitizenshipId",
												  								"Name" => "CitizenshipName",
												  								"Code" => "CitizenshipCode"
												  								)
												  			)
		   							);

		$AllCitizenships = $this->getActiveStudents($paramStudentArray);

		$AllCitizenships = array_reduce($AllCitizenships, function($a, $b) {
		    static $stored = array();

		    $hash = md5(serialize($b));

		    if (!in_array($hash, $stored)) {
		        $stored[] = $hash;
		        $a[] = $b;
		    }

		    return $a;
		}, array());

		return $AllCitizenships;
	}

	public function saveCitizenshipsToFile() {

		$file = $this->citizenshipDirectory.$this->citizenshipFileName;

		$AllCitizenships = $this->getAllCitizenshipsFromActiveStudents();

		file_put_contents($file, json_encode($AllCitizenships), LOCK_EX);

	}

	public function getCitizenshipsFromFile() {

		$citizenshipsArray = array();
		$citizenshipElemnts = 3;
		$file = $this->citizenshipDirectory.$this->citizenshipFileName;

		$citizenshipsArray = json_decode(file_get_contents($file));

		return $citizenshipsArray;

	}

	public function getStudentIdsByProperty($property, $from, $limit) {

		$currentStudentProperty = $this->mgTier->getSessionData("SearchStudentProperty");

		if (($currentStudentProperty) && ($currentStudentProperty == $property)) {
			$studentsId = $this->mgTier->getSessionData("SearchStudentIds");
		} else {
			$studentsId = $this->getAllStudentIdsProperty($property);
			$this->mgTier->setSessionData("SearchStudentProperty", $property);
			$this->mgTier->setSessionData("SearchStudentIds", $studentsId);
		}

		$studentsIdRange = array_slice($studentsId, $from, $limit);

		return array('data' => $studentsIdRange, 'total' => count($studentsId));
	}

	public function isActiveStudent($studyId) {
		$paramStudentArray = array( "StudyId" => "StudyId",);
		$searchArray = array("StudyId" => $studyId, "StudentStateId" => $this->IS_ACTIVE_STUDENT);

		$studentArray = $this->getStudentsIdsByParams($searchArray, $paramStudentArray);

		if ($studentArray) {
			return true;
		} else {
			return false;
		}
	}

	public function getStudentIdsByGroup($name) {
		$paramGroup = array("GroupId" => "GroupId",
							"GroupName" => "GroupName");
		$studentIds = array();

		$date = new DateTime('now');

		$allGroups = $this->doGetAllActiveGroups($paramGroup);

		foreach($allGroups as $group) {
			if($group["GroupName"] == $name) {
				$studentIds = $this->client->GetBeginSessionStudents(new GetBeginSessionStudents($group["GroupId"], $date, $date));

				return $studentIds->getGetBeginSessionStudentsResult()->getLong();
			}
		}
	}

	public function getStudentPassportById($studentId) {

		$studentDocIdentArray = $this->config->getDocIdentDTOArray();

		$paramArray = array("study_id" 			=> "study_id",
							"identify_id" 		=> "identify_id",
							"student_id" 		=> "student_id",
							"doc_ser" 			=> "doc_ser",
							"doc_num" 			=> "doc_num",
							"doc_from" 			=> "doc_from",
							"doc_to" 			=> "doc_to",
							"doc_who" 			=> "doc_who",
							"doc_birthplace" 	=> "doc_birthplace",
							"first_order_date"	=> "first_order_date",
							"first_order_num" 	=> "first_order_num"
							);

		try {
           $studentPassportArray = $this->client->getDocIdentOsnByStudentId(new getDocIdentOsnByStudentId($studentId));
        } catch(Exception $e) {
            trigger_error("Function 'getDocIdentOsnByStudentId' is not available", E_USER_ERROR);
            return null;
        }
		
		if ($studentPassportArray->getGetDocIdentOsnByStudentIdResult()) {
			foreach($studentPassportArray->getGetDocIdentOsnByStudentIdResult()->getDocIdentDTO() as $studentPassport) {
				$studentPassportResponse[] = $this->searchParams($paramArray, $studentDocIdentArray, $studentPassport);
			}
		}

		return $studentPassportResponse;
	}

	//Test function
	public function doTest() {
		$paramArray = array("PersonId" => "PersonID",
						"StudyId" => "StudyID",
						"Education" => array("newName" => "Education",
										  	"class" => "EducationDTO",
										  	"method" => "getEducationDTOArray",
										  	"type" => "class",
										  	"fields" => array("EduId" => "EduId",
										  						"EduForm" => array("newName" => "EducationForm",
										  											"class" => "EduFormDTO",
										  											"method" => "getEduFormsDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("EduFormId"	 => "EducationFormId",
							   	  	  																	"EduFormName"  => "EducationFormName"
							   	 	  																	)
										  											),
										  						"IsActive" => "IsActive"
										  						)
										  	),
	 					"LastName" => "LastName",
	  					"FirstName" => "FirstName",
	   					"MiddleName" => "MiddleName",
	    				"BirthDate" => "BirthDate");
		$searchArray = array("LastName" => "Постолати", "FirstName" => "Максим");

		$paramArray2 = array("id" => "AddressID",
						"postString" => "PostString",
						"addressIndex" => "addressIndex",
						"defaultItem" => "defaultItem",
						"Address" => array("newName" => "Address",
										  	"class" => "AddressDTO",
										  	"method" => "getAddressDTOArray",
										  	"type" => "class",
										  	"fields" => array("id" => "AddressId",
										  						"value" => "AddressValue",
										  						"shortName" => "AddressShortName",
										  						"AddressLevel" => array("newName" => "AddressLevel",
										  											"class" => "AddressLevelDTO",
										  											"method" => "getAddressLevelDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("id"	 => "AddressLevelId",
							   	  	  																	"name"  => "AddressLevelName"
							   	 	  																	)
										  											),
										  						"AddressPrefix" => array("newName" => "AddressPrefix",
										  											"class" => "AddressPrefixDTO",
										  											"method" => "getAddressPrefixDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("id"	 => "AddressPrefix",
							   	  	  																	"prefix"  => "AddressPrefixName"
							   	 	  																	)
										  											),
										  						"AddressParent" => "AddressParent"
										  						)
										  	),
	 					"AddressType" => array("newName" => "AddressType",
										  	"class" => "AddressTypeDTO",
										  	"method" => "getAddressTypeDTOArray",
										  	"type" => "class",
										  	"fields" => array("id" => "AddressTypeId",
										  						"name" => "AddressTypeName"
										  						)
										  	)
	 					);

		$paramStudentHostelArray = array(
			"StudentHosteId" => "HostelId",
			"HostelDTO" => array("newName" => "Hostel",
				"class" => "HostelDTO",
				"method" => "getHostelDTOArray",
		  	"type" => "class",
		  	"fields" => array("HostelId" => "HostelId",
		  						"Name" => "HostelName"
		  						)
				),
		 		"RoomNumber" => "Room",
				"StartDate" => "StartDate",
					"EndDate" => "EndDate",
				"StudyId" => "StudentId");

		/*$paramArray3 = array("StudyId" => "StudyID");

		set_time_limit(3*60 * 60 * 60);

		//$studentsId = $this->getActiveStudents($paramArray3);
		//var_dump($studentsId);
		$criteria = new StudentRequestDTO(0,0,0,0,null,null,null,null,null,0,0,0,0,0,0,0,0,0);

		$criteria->setStudentStateId(1);

		$students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));
		var_dump($students);*/
		/*foreach ($studentsId as $stdId) {
			$studentHostels = $this->client->GetStudentHostels(new GetStudentHostels($stdId["StudyID"]));
			var_dump($stdId["StudyID"]);
			echo "<br>";
			var_dump($studentHostels);
		}*/
		/*$citizenship = $this->getCitizenshipIdByNameSoap("Азербайджан");
		var_dump($citizenship);*/
		//$variables = $this->doGetAddressByStudentId(132337, $paramArray2);
		//$variables = $this->doGetStudentHostelByIdJson(135577, $paramStudentHostelArray);
		// $variables = $this->doGetAddressByStudentId(135752, $paramArray2);
		//var_dump($variables);

		// $studentsIds = $this->getStudentIdsByGroup("422-2");
		// var_dump($studentsIds);

		// $variables = $this->getStudentProperty(135577, "education"); 148197
		// $this->getStudentAddress(135752); 144401, 136740, 143921
		// $variables = $this->getStudentIdsByProperty("622", 0, 100);// 135547
		// foreach($variables["data"] as $key => $val) {
		// 	$variables2 = $this->doGetStudentHostelByIdJson($val, $paramStudentHostelArray);
		// 	var_dump($key);
		// 	var_dump($val);
		// 	var_dump($variables2);
		// }

		//$students = $this->getStudentIdsByProperty($property, $from, $limit)
		//$students = $this->getAllStudentIdsProperty("Казахстан");


		//$facultyId = $this->getFacultyIdByName("ФСУ");

		//genSoap();

		//$student = $this->addNewStudent(140501);
		//var_dump($students);

		var_dump($this->getStudentPassportById(150841));
		//var_dump($this->getStudentPassportById(140501));
		// die('!');

		/*$criteria = new StudentRequestDTO(0,0,0,'Россия',null,null,null,null,null,0,0,0,4,0,0,0,0,0);

		$criteria->setStudentStateId($this->IS_ACTIVE_STUDENT);

		$students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));

		var_dump($students);*/

		/*$studentHostels = $this->client->GetStudentHostels(new GetStudentHostels(132337));

		$currentHostel = $this->getCurrentHostel($studentHostels);
		$studentHostels1 = $this->getStudentHostelById(132337, $studentHostels);

		 print_r($studentHostels1);*/
		//$r = $this -> getStudentHostelById(145117, $paramStudentHostelArray);
		//print_r($r);
		//echo "<br>";

		// echo("<br>");
		// echo("<br>");

		// $studentHostels = $this->client->GetStudentHostels(new GetStudentHostels(136740));

		// var_dump($studentHostels);

		// $variables = $this->doFillStudentsDataBase(false);
		// $variables = $this->isActiveStudent(135577);

		// $variables = $this->getAllCitizenshipsFromActiveStudents();
		//$this->saveCitizenshipsToFile();
		//$variables = $this->getCitizenshipIdByName("Россия");

		// $variables = $this->getStudentsByParams($searchArray, $paramStudentHostelArray);
		// var_dump($variables);
		// $variables = $this->doGetAddressByStudentId(135537, $paramArray2);

		//$paramStudentArray = array("StudyId" => "StudyID", "CitizenshipId" => "CitizenshipId");

		// $activeStudents = $this->getActiveStudents($paramStudentHostelArray);
		// var_dump(json_encode($variables));

		// echo json_encode()
		// $searchArray = array("FirstName" => 'Артем', 'StudentStateId' => $this -> IS_ACTIVE_STUDENT);
		// $activeStudents = $this->getStudentsByParams($searchArray, $paramStudentArray);

		// // var_dump($activeStudents);
		// die();



		// var_dump(json_decode($variables));

		/*try {
			$address = $this->doGetAddressByStudentId(129542, $addressArray);
		}

		catch (Exception $e) {
			echo 'Error :' . $e->getMessage() . '<br />';
	        echo 'Code :' . $e->getCode() . '<br />';
	        echo 'File :' . $e->getFile() . '<br />';
	        echo 'Line :' . $e->getLine() . '<br />';
			exit();
		}



		var_dump(json_decode($address));*/

		//var_dump(json_decode($variables));
	}
}

?>
