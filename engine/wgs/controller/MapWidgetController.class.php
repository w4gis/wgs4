<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require(MAPVIEWER_DIR."/common.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require(MAPVIEWER_DIR."/layerdefinitionfactory.php");
lmb_require('limb/i18n/utf8.inc.php');

class MapWidgetController extends wgsFrontController
{
    protected $mgTier;

    private function updateAclRevision()
    {
        $this->wgsTier->sessionSet('savedWebLayoutsAclRevision', $this->wgsTier->getAclRevision());
    }

    function doGetArea(){
        $input = $this->request->getRequest('area');
        return str_replace(',','.',$this->wgsTier->getProjectObjectService()->getFeatureLayerService()->GetArea($input));
    }
    private function isAclRevisionUpdated()
    {
        if ($this->wgsTier->getAclRevision() != $this->wgsTier->sessionGet('savedWebLayoutsAclRevision'))
            return true;
        return false;
    }

    function prepareWebLayout($webLayoutResource, $webLayoutId, $simpleMode = false)
    {
        $user         = $this->wgsTier->getAuthorizedUser();
        $acl          = $this->wgsTier->getACL();
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();

        if ($simpleMode) {
            $webLayoutResource->setSimpleMode();
        }
        try
        {
            //if ($this->isAclRevisionUpdated() || !$this->mgTier->resourceExists($webLayoutResource->getSessionResource())) {
            $this->updateAclRevision();
            $mapResource = $webLayoutResource->getMapResource();
            //var_dump($mapResource);
            //if (!$this->mgTier->resourceExists($mapResource->getSessionResource()))
            {
                // ��������� ������
                if ($paneList = $this->wgsTier->getProjectObjectService()->getPaneList())
                {
                    foreach ($paneList as $pane)
                    {
                        if ($pane->getMgWebLayoutId() == $webLayoutId &&
                            !$acl->isAllowed($user, $pane, wgsPrivilege::MGPANE_ACCESS))
                            $webLayoutResource->excludePane($pane->getName());
                    }
                }
                // ��������� ����������� �������
                if ($builtInCommandList = $this->wgsTier->getProjectObjectService()->getBuiltInCommandList())
                {
                    foreach ($builtInCommandList as $builtInCommand)
                        if ($builtInCommand->getMgWebLayoutId() == $webLayoutId &&
                            !$acl->isAllowed($user, $builtInCommand, wgsPrivilege::MGBUILTINCOMMAND_ACCESS))
                            $webLayoutResource->excludeCommand($builtInCommand->getName());
                }
                // ��������� ��������� �������
                if ($customCommandList = $this->wgsTier->getProjectObjectService()->getCustomCommandList())
                {
                    foreach ($customCommandList as $customCommand)
                        if ($customCommand->getMgWebLayoutId() == $webLayoutId &&
                            ($simpleMode || !$acl->isAllowed($user, $customCommand, wgsPrivilege::MGCUSTOMCOMMAND_ACCESS)))
                            $webLayoutResource->excludeCommand($customCommand->getName());
                }
                // ��������� ����
                if ($mgMapLayerList = $this->wgsTier->getProjectObjectService()->getMgMapLayerList())
                {
                    foreach ($mgMapLayerList as $mgMapLayer)
                    {
                        if (!$acl->isAllowed($user, $mgMapLayer, wgsPrivilege::FEATURELAYER_ACCESS) &&
                            !$acl->isAllowed($user, $mgMapLayer, wgsPrivilege::MGMAPLAYER_ACCESS))
                            //$mapResource->excludeLayer($mgMapLayer->getName());
                        $mapResource->excludeLayer($mgMapLayer->getMgResourceId());
                    }
                }
                $layerResources = $mapResource->getLayerResources();

                // ���������� �������� ���� � ������
                foreach ($layerResources as $layerResource)
                {
                    $layerResource->removeGarbageLegends();
                    // todo sviga �� ���������� ����� � ����
                    //$layerResource->saveToSession();
                    $mapResource->rebindLayerResource($layerResource);
                }
                // �������� ����� � ������
                $mapResource->saveToSession();
            }
            $webLayoutResource->rebindMapResource($mapResource);
            // �������� ������� � ������
            $webLayoutResource->saveToSession();
            $webLayoutResourceId = $webLayoutResource->getResourceId();
            //}
            // else {
            ////     $webLayoutResourceId = $webLayoutResource->getSessionResource();
            // }
            $sessionId = $this->mgTier->getMgSessionId();

            return array('session' => $sessionId, 'resourceid' => $webLayoutResourceId, 'weblayout' => $webLayoutResource);
        }
        catch(MgException $e)
        {
            echo "ERROR: " . $e->GetMessage() . "\n";
            echo $e->GetDetails() . "\n";
            echo $e->GetStackTrace() . "\n";
            exit;
        }
    }

    function doCreateMapArea($simpleMode = false, $defaultWebLayout = NULL)
    {

        if ($this->wgsTier->isSysUser())
            return '';
        $this->setTemplate('map.html');
        if ($defaultWebLayout == NULL) {
            $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout();
        }
        if ($defaultWebLayout)
        {
            $webLayoutResource = new MgWebLayoutResource($defaultWebLayout->getMgResourceId());
            $defaultMapParameters = $this->prepareWebLayout($webLayoutResource, $defaultWebLayout->getId(), $simpleMode);
            $viewertype = $this->request->getRequest('viewerType');
            $frameSeq = md5(time()+rand());
            $this->passToView("webLayoutExists", true);
            $this->passToView("viewerType", $viewertype);
            $this->passToView("frameSeq", $frameSeq);
            $this->passToView("mapParameters", "SESSION={$defaultMapParameters['session']}&WEBLAYOUT=".urlencode($defaultMapParameters['resourceid'])."&LOCALE=ru");
        }
        return $frameSeq? json_encode(array('mapArea' => $this->view->render(), 'mapFrame' => "WgsMapFrame-{$frameSeq}")): json_encode(array('mapArea' => '', 'mapFrame' => ''));
    }

    function doCreateSimpleMapArea()
    {
        return $this->doCreateMapArea(true);
    }

    function doCreateFloorplan()
    {
        $featureId = $this->request->getRequest('featureId');
        if (defined("PROJECT_FLOORPLAN_PROPERTY_TYPE") && $featureId) {
            $webLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getFloorplanWebLayout($featureId, PROJECT_FLOORPLAN_PROPERTY_TYPE);
        }
        if (!$webLayout) {
            throw new wgsException(wgsException::FLOORPLAN_NOT_FOUND);
        }
        return $this->doCreateMapArea(false, $webLayout);
    }

    function prepareMap($mapResource)
    {
        $this->mgTier    = lmbToolkit :: instance() -> getMgTier();
        $siteConnection  = $this->mgTier->getSiteConnection();
        $sessionId       = $this->mgTier->getMgSessionId();

        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);
        //$resourceID      = new MgResourceIdentifier($webLayoutResource->getSessionResource($webLayoutResource->getMapResourceId()));
        $resourceID      = new MgResourceIdentifier($mapResource->getSessionResource());

        $MapName         = $mapResource->getName();
        $map             = new MgMap();

        $map->Create($resourceService, $resourceID, $MapName);
        //create an empty selection object and store it in the session repository
        $sel = new MgSelection($map);
        $sel->Save($resourceService, $MapName);

        $mapStateId = new MgResourceIdentifier("Session:" . $sessionId . "//" . $MapName . "." . MgResourceType::Map);
        $map->Save($resourceService, $mapStateId);

        return $map;
    }

    function createOverviewArea()
    {
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        if ($overviewWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getOverviewWebLayout())
        {
            $webLayoutResource = new MgWebLayoutResource($overviewWebLayout->getMgResourceId());
            $mapResource = $webLayoutResource->getMapResource();
            if (!$this->mgTier->resourceExists($mapResource->getSessionResource())) {
                $mapResource->saveToSession();
            }
            $map = $this->prepareMap($mapResource);
            $response = array();
            if ($initialView = $webLayoutResource->getInitialView()) {
                $response = $initialView;
            } else {
                $response['CenterX'] = $map->GetViewCenter()->GetCoordinate()->GetX();
                $response['CenterY'] = $map->GetViewCenter()->GetCoordinate()->GetY();
                $response['Scale']   = $map->GetViewScale();
            }
            $response['Width']   = $map->GetDataExtent()->GetWidth();
            $response['Height']  = $map->GetDataExtent()->GetHeight();
            $response['MapName'] = $map->GetName();
            $this->response->write(json_encode($response));
        }
    }

    function createMap()
    {
        $defaultWebLayout = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout();
        $webLayoutResource = new MgWebLayoutResource($defaultWebLayout->getMgResourceId());
        $mapResource = $webLayoutResource->getMapResource()->saveToSession();
        return $this->prepareMap($mapResource);
    }

    function doGetOverviewParameters()
    {
        $this->createOverviewArea();
    }

    function doFix()
    {/*
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $siteConnection = $this->mgTier->getSiteConnection();
        $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);



        $mgMapLayerList = $this->wgsTier->getProjectObjectService()->getMgMapLayerList();

        foreach ($mgMapLayerList as $mgMapLayer) {

            $mgResourceId = new MgResourceIdentifier($mgMapLayer->getMgResourceId());

            $byteContentReader = $resourceService->GetResourceContent($mgResourceId);
            $byteHeaderReader = $resourceService->GetResourceHeader($mgResourceId);

            $mgResourceContent = $byteContentReader->ToString();
            $mgResourceContentDOM = DOMDocument::loadXML($mgResourceContent);
            $mgResourceContentDOM->getElementsByTagName('ResourceId')->item(0)->nodeValue = 'Library://tnhk/datasources/tnhk_king_ora.FeatureSource';
            $mgResourceContentDOM->getElementsByTagName('FeatureName')->item(0)->nodeValue = 'KingOra:FEATURETABLE';

            $mgResourceContentXML = $mgResourceContentDOM->saveXML();

            $byteSource = new MgByteSource($mgResourceContentXML, strlen($mgResourceContentXML));
            $byteSource->setMimeType("text/xml");
            $byteXMLReader = $byteSource->GetReader();
            $resourceService->SetResource($mgResourceId, $byteXMLReader, $byteHeaderReader);

        }*/
    }
	
	
    function doDisplay()
    {
		//var_dump(lmbToolkit::instance()->getSession()->get('userInformation'));die();
        //$topmenu = $this->createTopMenu();
        $this->setTemplate('main_mw.html');
        $this->passToView("title", ":: WGS3 :: �������� ����� ::");
        $this->passToView("body", $topmenu);
        $config = array();

        if ($this->getRequestNamedParams('object')) {
            $config['callback'] = 'zoomToObject';
            $config['objectId'] = $this->getRequestNamedParams('object');
        }
        $viewertype = $this->getRequestNamedParams('viewertype');
        if ($viewertype != 'ajax' && $viewertype != 'dwf') {
            $viewertype = 'ajax';
        }
        $config['viewerType'] = $viewertype;

        if ($this->wgsTier->isProjectUser()) {
            $this->startFrontEndApplication(array(
                '/js/wgs/mapWidget.js',
                '/js/wgs/map/commands.js',
                '/js/wgs/map/card.js',
                '/js/wgs/map/viewer.js',//
                '/js/wgs/map/viewerapi.js',
                '/js/wgs/map/viewerlib.js',
                '/js/wgs/map/geometry.js',
                '/js/wgs/map/redlining.js',
                '/js/wgs/map/measure.js',
                '/js/wgs/map/measureclient.js',
                '/js/wgs/map/raphaeldrawing.js',
                '/js/wgs/map/selectradius.js',//
                '/js/wgs/map/selectpolygon.js',//
                 '/js/wgs/map/favoriteview.js',
                 '/js/wgs/map/buffer.js',
                 '/js/wgs/map/measure.js',
                 '/js/wgs/map/featuredoc.js',
                 '/js/wgs/map/overview.js',
                 '/js/wgs/map/options.js',
            ), 'Map', $config);
        }
    }
}

?>
