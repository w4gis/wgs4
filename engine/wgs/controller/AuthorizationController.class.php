<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/wgsUserInformation.class.php');
lmb_require('wgs/exception/wgsException.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');

class AuthorizationController extends wgsFrontController
{
    function doLogout()
    {
		$this -> wgsTier = lmbToolkit :: instance() -> getWgsTier();
			
        try {			
			$Host = $_SERVER['REMOTE_ADDR'];
			//if($this->wgsTier->getAuthorizedUser())
			//	$liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent( $this->wgsTier->getAuthorizedUser()->getId() ? $this->wgsTier->getAuthorizedUser()->getId():0,$Host,'','authorization','logout');						
            lmbToolkit :: instance() -> getMgTier() -> close();
			//if($liId)
			//	$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'logout_state', lmb_utf8_to_win1251('�������� ����� �� �������'));
      		
        }
        catch(wgsException $exception) {
            if ($exception->getCode() != wgsException::MAPGUIDE_EXCEPTION) {
				if($liId) {
					$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'logout_state', '������ ��� ���������� ������ �� �������. '.$exception->getCode().' :: '.$exception->getMessage());
				}
                throw new wgsException($exception->getCode(), $exception->getMessage());
            }
        }
        lmbToolkit :: instance() -> getWgsTier() -> close();
        $this->response->redirect('/');
    }
    
	function doExternalLogin(){
		if(!$this->request->hasPost())
		return;
		if ($this->wgsTier->isAuthorized()) {
            lmbToolkit :: instance() -> getMgTier() -> close();
            lmbToolkit :: instance() -> getWgsTier() -> close();                
        }
		 $this->wgsTier->open(
            new wgsUserInformation(
                $this->request->getPost('username'),
                $this->request->getPost('password')
            )
        );
	}
    function doLogin()
    {
		if(!$this->request->hasPost())
	    {
			$this->response->redirect('/');
			$this->response->commit();
			return;
	    }
        try
        {
            //$this->validator->addRequiredRule('username');
            $this->validator->addRequiredRule('password');
            $this->validator->validate($this->request);
			
			//$a=$this->wgsTier->getProjectTier()->getAuditService();
			//$isAuditable = $a->getAuditActivity();
			$isAuditable = 0;$liId=false;
			$this -> wgsTier = lmbToolkit :: instance() -> getWgsTier();
			/*����� �����������*/
			if($isAuditable>1){
				$uname = $this->request->getPost('username');
				$Host = $_SERVER['REMOTE_ADDR'];
				
				$liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent(0,$Host,'','authorization','login');
				if($liId)
					$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'login_uname',lmb_utf8_to_win1251($uname));
      		}
			/* */



            if (!$this->validator->isValid()){
                throw new wgsException(wgsException::ALL_FIELDS_REQUIRED);            
				if($liId)
					$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'login_state', lmb_utf8_to_win1251('���� � ������� �� ����� ���� ��������. �� ��������� ��� ������� ������ �/��� ������'));
      		
			}
            
			if ($this->wgsTier->isAuthorized()) {
                lmbToolkit :: instance() -> getMgTier() -> close();
                lmbToolkit :: instance() -> getWgsTier() -> close();                
            }
            
            $ip = $_SERVER['REMOTE_ADDR'];
			$pasHash = $this->request->getPost('password');
            $this->wgsTier->open(
                new wgsUserInformation(
                    $pasHash,
                    $ip
                )
            );
            
            
			if ($this->wgsTier->isAuthorized()){
			    if($liId) {
				// 	$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'login_state', lmb_utf8_to_win1251('�������� ���� � �������.')); 
					
				}
			}
			
        }
        catch(wgsException $exception)
        {
			sleep(1);	    
            $exception->response($this->response);
// 			if($liId)
				// $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'login_state', lmb_utf8_to_win1251('������ ��� ���������� ����� � �������.'));      		
			
        }
        
    }   
	
	function doStatus(){
		//echo $this->wgsTier->isAuthorized() ? '1' : '0';    
		
		$webLayoutId = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
		$prohibitedCommandList = [];
		$simpleMode = false;
		$acl        = $this->wgsTier->getACL();
		$user       = $this->wgsTier->getAuthorizedUser();
		
		if ($customCommandList = $this->wgsTier->getProjectObjectService()->getCustomCommandList()) {
			foreach ($customCommandList as $customCommand) {
				if ($customCommand->getMgWebLayoutId() == $webLayoutId &&
					($simpleMode || !$acl->isAllowed($user, $customCommand, wgsPrivilege::MGCUSTOMCOMMAND_ACCESS)))
						$prohibitedCommandList[ $customCommand->getName() ] = true;
			}
		};
		$prohibitedCommandList ['auth'] = $this->wgsTier->isAuthorized() ? '1' : '0';
		return CJSON::encode( $prohibitedCommandList );
	}
}

?>
