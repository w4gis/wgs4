<?php

lmb_require('wgs/controller/wgsController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class wgsFrontController extends wgsController
{
    public function createTopMenu()
    {
        $user       = $this->wgsTier->getAuthorizedUser();
        $acl        = $this->wgsTier->getACL();
        if ($this->wgsTier->getSysTier()->isAuthorized()) {
            $sysModules = $this->wgsTier->getSysObjectService()->getSysModuleService()->getModuleList();
        } else
        if ($this->wgsTier->getProjectTier()->isAuthorized()) {
            $sysModules = $this->wgsTier->getSysObjectService()->getSysModuleService()->getProjectModuleList(getProjectName());
        }
        $containerModules = array();
        $nestedModules = array();
        foreach($sysModules as $sysModule)
        {
            if ($acl->isAllowed($user, $sysModule, wgsPrivilege::SYSMODULE_ACCESS) && !$sysModule->getLocked())
            {
                if ($sysModule->getInMenu())
                {
                    if (!$sysModule->getParentId())
                    {
						//echo $sysModule->getLabel();
						//var_dump($sysModule->getLabel());
                        $containerModules[$sysModule->getId()] = array(
                            'moduleId' => $sysModule->getId(),
                            'label' => encode($sysModule->getLabel()),
                            'urlString' => ($sysModule->isContainer())? $sysModule->getUrlString(): ''
                        );
                        $nestedModules[$sysModule->getId()]['moduleId'] = $sysModule->getId();
                    }
                    elseif($sysModule->getParentId())
                    { 
                        $nestedModules[$sysModule->getParentId()]['wgsNestedMenuItems'][] = array(
                            'label' => encode($sysModule->getLabel()),
                            'urlString' => $sysModule->getUrlString()
                        );
                    }
                }
            }
        }
        $this->setTemplate('topmenu.html');
        $this->view->findChild('wgsMenu')->registerDataset($containerModules);
        $this->view->findChild('wgsNestedMenu')->registerDataset($nestedModules);
       // echo 'sas'; 
        //var_dump($this->view);
        return $this->view->render();        
    }

    public function startFrontEndApplication($jss, $application, $config = array(), $ExtJs = "true")
    {  
        $js_privileges = "";
        
        if (($user = $this->wgsTier->getAuthorizedUser()) &&
        	($acl = $this->wgsTier->getACL())) {

	        if ($sysPrivileges = $this->wgsTier->getSysPrivileges()) {
	            if (is_array($sysPrivileges)) {
	                foreach ($sysPrivileges as $sysPrivilege) {
	                    if ($acl->isAllowed($user, $sysPrivilege, wgsPrivilege::SYSPRIVILEGE_ACCESS)) {
	                        $js_privileges .= "WGS.PRIVILEGE.".$sysPrivilege->getName()." = true;";
	                    }
	                }
	            }
	        }

    			if ($customCommandList = $this->wgsTier->getProjectObjectService()->getCustomCommandList()) {
    				if (is_array($customCommandList)) {

    					foreach ($customCommandList as $customCommand) {
      					if ($acl->isAllowed($user, $customCommand, wgsPrivilege::MGCUSTOMCOMMAND_ACCESS)) {
    							$js_privileges .= "WGS.MODULES.".$this->convertName($customCommand->getName())." = true;";
    						}
    					}
    				}
    			}
        }
        $js_privileges .= "if(!WGS.FLAGS) WGS.FLAGS={}; WGS.FLAGS.NO_DRIVERS = " . ( WGS4_NO_DRIVERS_MODE == true ? 1 : 0) ."; ";
        $js_scripts = "";
        $config = "{$application}.config = '".json_encode($config)."';";
        foreach($jss as $js)
            $js_scripts .= "<script type=\"text/javascript\" src=\"{$js}\"></script>\n";        
        $js_application =
        "<script type=\"text/javascript\">\n".
        "try {{$config} {$js_privileges} if(".$ExtJs.") Ext.onReady({$application}.init, {$application})}\n".
        "catch(e) {console.log(e);alert('�� ������� ��������� ���������� [{$application}]')}\n".
        "</script>\n";
        $this->passToView('application', $js_scripts.$js_application);
    }
    
    private function convertName($str)
    {
    	$result = preg_match_all("/[A-Z]{1}[a-z]*/", $str, $matches);
   		return $result !== FALSE ? strtoupper(implode("_",$matches[0])): $str;
    }
}

?>
