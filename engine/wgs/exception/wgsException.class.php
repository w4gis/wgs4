<?php

lmb_require('limb/i18n/utf8.inc.php');
lmb_require('limb/view/src/lmbWactView.class.php');

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2008 GISLab, Rybalov Nikita
 */
 
class wgsException extends Exception
{
    // System exceptions
    const AUTHORIZATION_REQUIRED         = 1;
    const INCORRECT_USERNAME_OR_PASSWORD = 2;
    const ALREADY_AUTHORIZED             = 3;
    const ALL_FIELDS_REQUIRED            = 4;
    const INSUFFICIENT_PRIVILEGES        = 5;
    const MODULE_NOT_FOUND               = 6;
    const MODULE_IS_LOCKED               = 7;
    // Map exception
    const SELECT_OBJECT_IS_IMPOSSIBLE    = 100;
    // Database exceptions
    const INCORRECT_DB_USER_OR_PASSWORD  = 1017;
    // MapGuide exceptions
    const MAPGUIDE_EXCEPTION             = 10000;
    const MAPGUIDE_EXCEPTION_WEB_TIER    = 10001;
    // Appliction exceptions
    //const AUTHORIZATION_REQUIRE1       = 20001;
    const UNIQUE_CONSTRAINT_NAME         = 20003;
    const LAYERGROUP_CONTAINS_OBJECTS    = 20004;
    const OBJECT_TYPE_CONTAINS_OBJECTS   = 20029;
    const PROPGROUP_CONTAINS_PROPERTIES  = 20034;
    const FEATURELAYER_NOT_BINDED        = 20050;
    const OBJECTWITHTYPEALREADYBINDED    = 20051;
    const CHILD_OBJECT_ALREADY_BINDED    = 20052;
    const FLOORPLAN_NOT_FOUND            = 20053;
    const ROLE_IS_EXIST                  = 20100;
    const USER_IS_EXIST                  = 20101;
    const INDEX_IS_BUSY                  = 20102;

    const MODULE_MEMBERSHIPS_IS_EXISTS   = 20201;
    const MODULE_CHILDREN_IS_EXISTS      = 20202;
    const MODULE_IS_EXISTS               = 20203;
    const PRIVILEGE_IS_EXISTS            = 20205;
    const PROJECT_IS_EXISTS              = 20206;
    const PROJECT_MEMBERSHIPS_IS_EXISTS  = 20207;
    const SCHEMEWDB_IS_EXISTS            = 20208;
    const SCHEMESDB_IS_EXISTS            = 20209;
    const SCHEMEADB_IS_EXISTS            = 20210;
    const SCHEMEUSER_IS_EXISTS           = 20211;
    

    var $MESSAGES = array(
        self::AUTHORIZATION_REQUIRED => '���������� �����������',
        self::INCORRECT_USERNAME_OR_PASSWORD => '������� ��� ������������ / ������',
        self::ALREADY_AUTHORIZED => '�� ��� ������������',
        self::ALL_FIELDS_REQUIRED => '���������� ��������� ��� ����',
        self::INSUFFICIENT_PRIVILEGES => '������������ ���� ��� ���������� ������ ��������',
        self::MODULE_NOT_FOUND => '������ �� ������',
        self::MODULE_IS_LOCKED => '������ ��������',
        //
        self::SELECT_OBJECT_IS_IMPOSSIBLE => '�� ���� �� ����������� ���� �������� �� ���������� �� �����',
        //
        self::INCORRECT_DB_USER_OR_PASSWORD => '������� ��� ������������ / ������ ���� ������',
        //
        
        self::OBJECTWITHTYPEALREADYBINDED => '�������� ������� ���� ��� ����������� � ���������� �������',
        self::UNIQUE_CONSTRAINT_NAME => '������ � ��������� ������ ��� ����������',
        self::LAYERGROUP_CONTAINS_OBJECTS => '������ ������ �� ����� ���� �������, �.� �������� ����',
        self::OBJECT_TYPE_CONTAINS_OBJECTS => '������ ��� �� ����� ���� ������, �.� �������� �������',
        self::PROPGROUP_CONTAINS_PROPERTIES => '������ ������ ������������� �� ����� ���� �������, �.� �������� ��������������',
        
        self::FEATURELAYER_NOT_BINDED => '��� ������ �������� �� �������� � ���� ���������� �������',
        self::PRIVILEGE_IS_EXISTS => '���������� � ����� ��������� ��� ����������',

        self::MODULE_IS_EXISTS => '������ � ����� url - ������� ��� ����������',
        self::CHILD_OBJECT_ALREADY_BINDED => '������ �������� ��� ���������',
        self::FLOORPLAN_NOT_FOUND => '��������� ����, �� ������� ������������� �� ��������� ���� ������, �� ������',
        
        self::ROLE_IS_EXIST => '���� � ����� ��������� ��� ����������',
        self::USER_IS_EXIST => '������������ � ����� ��������� ��� ����������',
        self::INDEX_IS_BUSY => '�������� ���� �������� ����������',

        self::MODULE_IS_EXISTS => '������ � ����� url - ������� ��� ����������',
        self::MODULE_MEMBERSHIPS_IS_EXISTS => '������ �� ����� ���� ������, ��� ��� �������� �������',
        self::MODULE_CHILDREN_IS_EXISTS => '������ �� ����� ���� ������, ��� ��� ����� �������� ������',
        self::PROJECT_IS_EXISTS => '������ � ����� ��������� ��� ����������',
        self::PROJECT_MEMBERSHIPS_IS_EXISTS => '������ �� ����� ���� ������, ��� ��� ����� ����������� ������',
		self::SCHEMEWDB_IS_EXISTS => '����� WDB ��� ����������',        
		self::SCHEMESDB_IS_EXISTS => '����� SDB ��� ����������',        
		self::SCHEMEADB_IS_EXISTS => '����� ADB ��� ����������',        
		self::SCHEMEUSER_IS_EXISTS => '����� USER ��� ����������',    
		
		self::MAPGUIDE_EXCEPTION_WEB_TIER => 'MapGuide: �� ��������������� web-����. ���������� � �������������� �������.'
    );
    
    function __construct($code = 0, $message = null)
    {
        //if (20000 <= $code && $code <= 20999)
        $this->message = isset($this->MESSAGES[$code])? $this->MESSAGES[$code]: $message;
        parent :: __construct($this->message, $code);
    }
    
    public function response($responseObject)
    {
        $responseObject->write(json_encode(array('exception' => $this->code,
                                                 'message' => lmb_win1251_to_utf8($this->message))));
        $responseObject->commit();
    }
    
    function handle()
    {
        lmbToolkit :: instance() -> setView(new lmbWactView('main.html'));
        $view = lmbToolkit :: instance() -> getView();
        $js_exception = json_encode(array('exception' => $this->code, 'message' => lmb_win1251_to_utf8($this->message)));
        $js_scripts = "<script type=\"text/javascript\" src=\"/js/wgs/exception.js\"></script>\n".
        "<script type=\"text/javascript\">\n".
        "var EXCEPTION = '{$js_exception}';".
        "Ext.onReady(Exception.init, Exception);\n".
        "</script>\n";
        $view->set('application', $js_scripts);
        print $view->render();
    }
}