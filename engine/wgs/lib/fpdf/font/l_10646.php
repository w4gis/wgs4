<?php
$type='TrueType';
$name='LucidaSansUnicode';
$desc=array('Ascent'=>1097,'Descent'=>-440,'CapHeight'=>723,'Flags'=>32,'FontBBox'=>'[-615 -440 1384 1145]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>750);
$up=-100;
$ut=50;
$cw=array(
	chr(0)=>750,chr(1)=>750,chr(2)=>750,chr(3)=>750,chr(4)=>750,chr(5)=>750,chr(6)=>750,chr(7)=>750,chr(8)=>750,chr(9)=>750,chr(10)=>750,chr(11)=>750,chr(12)=>750,chr(13)=>750,chr(14)=>750,chr(15)=>750,chr(16)=>750,chr(17)=>750,chr(18)=>750,chr(19)=>750,chr(20)=>750,chr(21)=>750,
	chr(22)=>750,chr(23)=>750,chr(24)=>750,chr(25)=>750,chr(26)=>750,chr(27)=>750,chr(28)=>750,chr(29)=>750,chr(30)=>750,chr(31)=>750,' '=>316,'!'=>316,'"'=>374,'#'=>632,'$'=>632,'%'=>668,'&'=>697,'\''=>229,'('=>325,')'=>325,'*'=>482,'+'=>795,
	','=>316,'-'=>579,'.'=>316,'/'=>524,'0'=>632,'1'=>632,'2'=>632,'3'=>632,'4'=>632,'5'=>632,'6'=>632,'7'=>632,'8'=>632,'9'=>632,':'=>316,';'=>316,'<'=>795,'='=>795,'>'=>795,'?'=>422,'@'=>858,'A'=>690,
	'B'=>575,'C'=>692,'D'=>749,'E'=>542,'F'=>536,'G'=>723,'H'=>735,'I'=>288,'J'=>311,'K'=>653,'L'=>533,'M'=>861,'N'=>739,'O'=>777,'P'=>553,'Q'=>777,'R'=>632,'S'=>539,'T'=>632,'U'=>693,'V'=>654,'W'=>855,
	'X'=>626,'Y'=>623,'Z'=>604,'['=>325,'\\'=>524,']'=>325,'^'=>632,'_'=>500,'`'=>614,'a'=>552,'b'=>629,'c'=>512,'d'=>629,'e'=>557,'f'=>368,'g'=>624,'h'=>621,'i'=>289,'j'=>304,'k'=>584,'l'=>289,'m'=>934,
	'n'=>621,'o'=>614,'p'=>629,'q'=>629,'r'=>409,'s'=>510,'t'=>374,'u'=>621,'v'=>518,'w'=>771,'x'=>613,'y'=>522,'z'=>573,'{'=>325,'|'=>374,'}'=>325,'~'=>632,chr(127)=>750,chr(128)=>750,chr(129)=>750,chr(130)=>316,chr(131)=>750,
	chr(132)=>374,chr(133)=>1000,chr(134)=>632,chr(135)=>632,chr(136)=>632,chr(137)=>1012,chr(138)=>750,chr(139)=>332,chr(140)=>750,chr(141)=>750,chr(142)=>750,chr(143)=>750,chr(144)=>750,chr(145)=>316,chr(146)=>316,chr(147)=>374,chr(148)=>374,chr(149)=>632,chr(150)=>500,chr(151)=>1000,chr(152)=>750,chr(153)=>807,
	chr(154)=>750,chr(155)=>332,chr(156)=>750,chr(157)=>750,chr(158)=>750,chr(159)=>750,chr(160)=>316,chr(161)=>750,chr(162)=>750,chr(163)=>750,chr(164)=>633,chr(165)=>750,chr(166)=>372,chr(167)=>632,chr(168)=>750,chr(169)=>855,chr(170)=>750,chr(171)=>524,chr(172)=>795,chr(173)=>579,chr(174)=>632,chr(175)=>750,
	chr(176)=>277,chr(177)=>795,chr(178)=>750,chr(179)=>750,chr(180)=>750,chr(181)=>632,chr(182)=>632,chr(183)=>446,chr(184)=>750,chr(185)=>750,chr(186)=>750,chr(187)=>524,chr(188)=>750,chr(189)=>750,chr(190)=>750,chr(191)=>750,chr(192)=>750,chr(193)=>750,chr(194)=>750,chr(195)=>750,chr(196)=>750,chr(197)=>750,
	chr(198)=>750,chr(199)=>750,chr(200)=>750,chr(201)=>750,chr(202)=>750,chr(203)=>750,chr(204)=>750,chr(205)=>750,chr(206)=>750,chr(207)=>750,chr(208)=>750,chr(209)=>750,chr(210)=>750,chr(211)=>750,chr(212)=>750,chr(213)=>750,chr(214)=>750,chr(215)=>750,chr(216)=>750,chr(217)=>750,chr(218)=>750,chr(219)=>750,
	chr(220)=>750,chr(221)=>750,chr(222)=>750,chr(223)=>750,chr(224)=>750,chr(225)=>750,chr(226)=>750,chr(227)=>750,chr(228)=>750,chr(229)=>750,chr(230)=>750,chr(231)=>750,chr(232)=>750,chr(233)=>750,chr(234)=>750,chr(235)=>750,chr(236)=>750,chr(237)=>750,chr(238)=>750,chr(239)=>750,chr(240)=>750,chr(241)=>750,
	chr(242)=>750,chr(243)=>750,chr(244)=>750,chr(245)=>750,chr(246)=>750,chr(247)=>750,chr(248)=>750,chr(249)=>750,chr(250)=>750,chr(251)=>750,chr(252)=>750,chr(253)=>750,chr(254)=>750,chr(255)=>750);
$enc='cp1251';
$diff='128 /afii10051 /afii10052 131 /afii10100 136 /Euro 138 /afii10058 140 /afii10059 /afii10061 /afii10060 /afii10145 /afii10099 152 /.notdef 154 /afii10106 156 /afii10107 /afii10109 /afii10108 /afii10193 161 /afii10062 /afii10110 /afii10057 165 /afii10050 168 /afii10023 170 /afii10053 175 /afii10056 178 /afii10055 /afii10103 /afii10098 184 /afii10071 /afii61352 /afii10101 188 /afii10105 /afii10054 /afii10102 /afii10104 /afii10017 /afii10018 /afii10019 /afii10020 /afii10021 /afii10022 /afii10024 /afii10025 /afii10026 /afii10027 /afii10028 /afii10029 /afii10030 /afii10031 /afii10032 /afii10033 /afii10034 /afii10035 /afii10036 /afii10037 /afii10038 /afii10039 /afii10040 /afii10041 /afii10042 /afii10043 /afii10044 /afii10045 /afii10046 /afii10047 /afii10048 /afii10049 /afii10065 /afii10066 /afii10067 /afii10068 /afii10069 /afii10070 /afii10072 /afii10073 /afii10074 /afii10075 /afii10076 /afii10077 /afii10078 /afii10079 /afii10080 /afii10081 /afii10082 /afii10083 /afii10084 /afii10085 /afii10086 /afii10087 /afii10088 /afii10089 /afii10090 /afii10091 /afii10092 /afii10093 /afii10094 /afii10095 /afii10096 /afii10097';
$file='l_10646.z';
$originalsize=323980;
?>