<?php

interface wgsZendAclAssertInterface
{
    public function assert(wgsZendAcl $acl, wgsZendAclRoleInterface $role = null, wgsZendAclResourceInterface $resource = null,
                           $privilege = null);
} 

?>