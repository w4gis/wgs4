<?php

/**
 * @package    Exported from Zend_Acl
 * @copyright  Copyright (c) 2005-2007 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */ 

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');
lmb_require('wgs/lib/wgsZendAclRoleInterface.interface.php');
lmb_require('wgs/lib/wgsZendAclAssertInterface.interface.php');
lmb_require('wgs/lib/wgsZendAclResource.class.php');
lmb_require('wgs/lib/wgsZendAclRole.class.php');
lmb_require('wgs/lib/wgsZendAclRoleRegistry.class.php');

class wgsZendException extends Exception {}
class wgsZendAclException extends wgsZendException {}
class wgsZendAclRoleRegistryException extends wgsZendAclException {}

class wgsZendAcl
{
    /**
     * Rule type: allow
     */
    const TA = 'TA';

    /**
     * Rule type: deny
     */
    const TD  = 'TD';

    /**
     * Rule operation: add
     */
    const OA = 'OA';

    /**
     * Rule operation: remove
     */
    const OD = 'OD';

    /**
     * Role registry
     *
     * @var wgsZendAclRoleRegistry
     */
    protected $_roleRegistry = null;

    /**
     * Resource tree
     *
     * @var array
     */
    protected $_resources = array();

    /**
     * ACL rules; whitelist (deny everything to all) by default
     *
     * @var array
     */
    protected $_rules = array(
        'allResources' => array(
            'allRoles' => array(
                'allPrivileges' => array(
                    'type'   => self::TD,
                    'assert' => null
                    ),
                'byPrivId' => array()
                ),
            'byRoleId' => array()
            ),
        'byResId' => array()
        );

    /**
     * Adds a Role having an identifier unique to the registry
     *
     * The $parents parameter may be a reference to, or the string identifier for,
     * a Role existing in the registry, or $parents may be passed as an array of
     * these - mixing string identifiers and objects is ok - to indicate the Roles
     * from which the newly added Role will directly inherit.
     *
     * In order to resolve potential ambiguities with conflicting rules inherited
     * from different parents, the most recently added parent takes precedence over
     * parents that were previously added. In other words, the first parent added
     * will have the least priority, and the last parent added will have the
     * highest priority.
     *
     * @param  wgsZendAclRoleInterface|string       $role
     * @param  wgsZendAclRoleInterface|string|array $parents
     * @uses   wgsZendAclRoleRegistry::add()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function addRole($role, $parents = null)
    {
        if ($role instanceof wgsZendAclRoleInterface)
            $this->_getRoleRegistry()->add($role, $parents);
        else
            $this->_getRoleRegistry()->add(new wgsZendAclRole($role), $parents);
        return $this;
    }

    /**
     * Returns the identified Role
     *
     * The $role parameter can either be a Role or Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @uses   wgsZendAclRoleRegistry::get()
     * @return wgsZendAclRoleInterface
     */
    public function getRole($role)
    {
        return $this->_getRoleRegistry()->get($role);
    }

    /**
     * Returns true if and only if the Role exists in the registry
     *
     * The $role parameter can either be a Role or a Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @uses   wgsZendAclRoleRegistry::has()
     * @return boolean
     */
    public function hasRole($role)
    {
        return $this->_getRoleRegistry()->has($role);
    }

    /**
     * Returns true if and only if $role inherits from $inherit
     *
     * Both parameters may be either a Role or a Role identifier. If
     * $onlyParents is true, then $role must inherit directly from
     * $inherit in order to return true. By default, this method looks
     * through the entire inheritance DAG to determine whether $role
     * inherits from $inherit through its ancestor Roles.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @param  wgsZendAclRoleInterface|string $inherit
     * @param  boolean                        $onlyParents
     * @uses   wgsZendAclRoleRegistry::inherits()
     * @return boolean
     */
    public function inheritsRole($role, $inherit, $onlyParents = false)
    {
        return $this->_getRoleRegistry()->inherits($role, $inherit, $onlyParents = false);
    }

    /**
     * Removes the Role from the registry
     *
     * The $role parameter can either be a Role or a Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @uses   wgsZendAclRoleRegistry::remove()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function removeRole($role)
    {
        $this->_getRoleRegistry()->remove($role);

        if ($role instanceof wgsZendAclRoleInterface) {
            $roleId = $role->getRoleId();
        } else {
            $roleId = $role;
        }

        foreach ($this->_rules['allResources']['byRoleId'] as $roleIdCurrent => $rules) {
            if ($roleId === $roleIdCurrent) {
                unset($this->_rules['allResources']['byRoleId'][$roleIdCurrent]);
            }
        }
        foreach ($this->_rules['byResId'] as $resourceIdCurrent => $visitor) {
            foreach ($visitor['byRoleId'] as $roleIdCurrent => $rules) {
                if ($roleId === $roleIdCurrent) {
                    unset($this->_rules['byResId'][$resourceIdCurrent]['byRoleId'][$roleIdCurrent]);
                }
            }
        }

        return $this;
    }

    /**
     * Removes all Roles from the registry
     *
     * @uses   wgsZendAclRoleRegistry::removeAll()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function removeRoleAll()
    {
        $this->_getRoleRegistry()->removeAll();

        foreach ($this->_rules['allResources']['byRoleId'] as $roleIdCurrent => $rules) {
            unset($this->_rules['allResources']['byRoleId'][$roleIdCurrent]);
        }
        foreach ($this->_rules['byResId'] as $resourceIdCurrent => $visitor) {
            foreach ($visitor['byRoleId'] as $roleIdCurrent => $rules) {
                unset($this->_rules['byResId'][$resourceIdCurrent]['byRoleId'][$roleIdCurrent]);
            }
        }

        return $this;
    }

    public function addResource($resource, $parent = null)
    {
        return $this->add(new wgsZendAclResource($resource), $parent);
    }
    
    /**
     * Adds a Resource having an identifier unique to the ACL
     *
     * The $parent parameter may be a reference to, or the string identifier for,
     * the existing Resource from which the newly added Resource will inherit.
     *
     * @param  wgsZendAclResourceInterface        $resource
     * @param  wgsZendAclResourceInterface|string $parent
     * @throws wgsZendAclException
     * @return wgsZendAcl Provides a fluent interface
     */
    public function add(wgsZendAclResourceInterface $resource, $parent = null)
    {
        $resourceId = $resource->getResourceId();

        if ($this->has($resourceId)) {
            throw new wgsZendAclException("Resource id '$resourceId' already exists in the ACL");
        }

        $resourceParent = null;

        if (null !== $parent) {
            try {
                if ($parent instanceof wgsZendAclResourceInterface) {
                    $resourceParentId = $parent->getResourceId();
                } else {
                    $resourceParentId = $parent;
                }
                $resourceParent = $this->get($resourceParentId);
            } catch (wgsZendAclException $e) {
                throw new wgsZendAclException("Parent Resource id '$resourceParentId' does not exist");
            }
            $this->_resources[$resourceParentId]['c'][$resourceId] = $resource;
        }

        $this->_resources[$resourceId] = array(
            'i' => $resource,
            'p'   => $resourceParent,
            'c' => array()
            );

        return $this;
    }

    /**
     * Returns the identified Resource
     *
     * The $resource parameter can either be a Resource or a Resource identifier.
     *
     * @param  wgsZendAclResourceInterface|string $resource
     * @throws wgsZendAclException
     * @return wgsZendAclResourceInterface
     */
    public function get($resource)
    {
        if ($resource instanceof wgsZendAclResourceInterface) {
            $resourceId = $resource->getResourceId();
        } else {
            $resourceId = (string) $resource;
        }

        if (!$this->has($resource)) {
            throw new wgsZendAclException("Resource '$resourceId' not found");
        }

        return $this->_resources[$resourceId]['i'];
    }

    /**
     * Returns true if and only if the Resource exists in the ACL
     *
     * The $resource parameter can either be a Resource or a Resource identifier.
     *
     * @param  wgsZendAclResourceInterface|string $resource
     * @return boolean
     */
    public function has($resource)
    {
        if ($resource instanceof wgsZendAclResourceInterface) {
            $resourceId = $resource->getResourceId();
        } else {
            $resourceId = (string) $resource;
        }

        return isset($this->_resources[$resourceId]);
    }

    /**
     * Returns true if and only if $resource inherits from $inherit
     *
     * Both parameters may be either a Resource or a Resource identifier. If
     * $onlyParent is true, then $resource must inherit directly from
     * $inherit in order to return true. By default, this method looks
     * through the entire inheritance tree to determine whether $resource
     * inherits from $inherit through its ancestor Resources.
     *
     * @param  wgsZendAclResourceInterface|string $resource
     * @param  wgsZendAclResourceInterface|string $inherit
     * @param  boolean                            $onlyParent
     * @throws wgsZendAcl_Resource_Registry_Exception
     * @return boolean
     */
    public function inherits($resource, $inherit, $onlyParent = false)
    {
        try {
            $resourceId     = $this->get($resource)->getResourceId();
            $inheritId = $this->get($inherit)->getResourceId();
        } catch (wgsZendAclException $e) {
            throw $e;
        }

        if (null !== $this->_resources[$resourceId]['p']) {
            $parentId = $this->_resources[$resourceId]['p']->getResourceId();
            if ($inheritId === $parentId) {
                return true;
            } else if ($onlyParent) {
                return false;
            }
        } else {
            return false;
        }

        while (null !== $this->_resources[$parentId]['p']) {
            $parentId = $this->_resources[$parentId]['p']->getResourceId();
            if ($inheritId === $parentId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Removes a Resource and all of its children
     *
     * The $resource parameter can either be a Resource or a Resource identifier.
     *
     * @param  wgsZendAclResourceInterface|string $resource
     * @throws wgsZendAclException
     * @return wgsZendAcl Provides a fluent interface
     */
    public function remove($resource)
    {
        try {
            $resourceId = $this->get($resource)->getResourceId();
        } catch (wgsZendAclException $e) {
            throw $e;
        }

        $resourcesRemoved = array($resourceId);
        if (null !== ($resourceParent = $this->_resources[$resourceId]['p'])) {
            unset($this->_resources[$resourceParent->getResourceId()]['c'][$resourceId]);
        }
        foreach ($this->_resources[$resourceId]['c'] as $childId => $child) {
            $this->remove($childId);
            $resourcesRemoved[] = $childId;
        }

        foreach ($resourcesRemoved as $resourceIdRemoved) {
            foreach ($this->_rules['byResId'] as $resourceIdCurrent => $rules) {
                if ($resourceIdRemoved === $resourceIdCurrent) {
                    unset($this->_rules['byResId'][$resourceIdCurrent]);
                }
            }
        }

        unset($this->_resources[$resourceId]);

        return $this;
    }

    /**
     * Removes all Resources
     *
     * @return wgsZendAcl Provides a fluent interface
     */
    public function removeAll()
    {
        foreach ($this->_resources as $resourceId => $resource) {
            foreach ($this->_rules['byResId'] as $resourceIdCurrent => $rules) {
                if ($resourceId === $resourceIdCurrent) {
                    unset($this->_rules['byResId'][$resourceIdCurrent]);
                }
            }
        }

        $this->_resources = array();

        return $this;
    }

    /**
     * Adds an "allow" rule to the ACL
     *
     * @param  wgsZendAclRoleInterface|string|array     $roles
     * @param  wgsZendAclResourceInterface|string|array $resources
     * @param  string|array                             $privileges
     * @param  wgsZendAclAssertInterface                $assert
     * @uses   wgsZendAcl::setRule()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function allow($roles = null, $resources = null, $privileges = null, wgsZendAclAssertInterface $assert = null)
    {
        return $this->setRule(self::OA, self::TA, $roles, $resources, $privileges, $assert);
    }

    /**
     * Adds a "deny" rule to the ACL
     *
     * @param  wgsZendAclRoleInterface|string|array     $roles
     * @param  wgsZendAclResourceInterface|string|array $resources
     * @param  string|array                             $privileges
     * @param  wgsZendAclAssertInterface                $assert
     * @uses   wgsZendAcl::setRule()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function deny($roles = null, $resources = null, $privileges = null, wgsZendAclAssertInterface $assert = null)
    {
        return $this->setRule(self::OA, self::TD, $roles, $resources, $privileges, $assert);
    }

    /**
     * Removes "allow" permissions from the ACL
     *
     * @param  wgsZendAclRoleInterface|string|array     $roles
     * @param  wgsZendAclResourceInterface|string|array $resources
     * @param  string|array                             $privileges
     * @uses   wgsZendAcl::setRule()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function removeAllow($roles = null, $resources = null, $privileges = null)
    {
        return $this->setRule(self::OD, self::TA, $roles, $resources, $privileges);
    }

    /**
     * Removes "deny" restrictions from the ACL
     *
     * @param  wgsZendAclRoleInterface|string|array     $roles
     * @param  wgsZendAclResourceInterface|string|array $resources
     * @param  string|array                             $privileges
     * @uses   wgsZendAcl::setRule()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function removeDeny($roles = null, $resources = null, $privileges = null)
    {
        return $this->setRule(self::OD, self::TD, $roles, $resources, $privileges);
    }

    /**
     * Performs operations on ACL rules
     *
     * The $operation parameter may be either OA or OD, depending on whether the
     * user wants to add or remove a rule, respectively:
     *
     * OA specifics:
     *
     *      A rule is added that would allow one or more Roles access to [certain $privileges
     *      upon] the specified Resource(s).
     *
     * OD specifics:
     *
     *      The rule is removed only in the context of the given Roles, Resources, and privileges.
     *      Existing rules to which the remove operation does not apply would remain in the
     *      ACL.
     *
     * The $type parameter may be either TA or TD, depending on whether the
     * rule is intended to allow or deny permission, respectively.
     *
     * The $roles and $resources parameters may be references to, or the string identifiers for,
     * existing Resources/Roles, or they may be passed as arrays of these - mixing string identifiers
     * and objects is ok - to indicate the Resources and Roles to which the rule applies. If either
     * $roles or $resources is null, then the rule applies to all Roles or all Resources, respectively.
     * Both may be null in order to work with the default rule of the ACL.
     *
     * The $privileges parameter may be used to further specify that the rule applies only
     * to certain privileges upon the Resource(s) in question. This may be specified to be a single
     * privilege with a string, and multiple privileges may be specified as an array of strings.
     *
     * If $assert is provided, then its assert() method must return true in order for
     * the rule to apply. If $assert is provided with $roles, $resources, and $privileges all
     * equal to null, then a rule having a type of:
     *
     *      TA will imply a type of TD, and
     *
     *      TD will imply a type of TA
     *
     * when the rule's assertion fails. This is because the ACL needs to provide expected
     * behavior when an assertion upon the default ACL rule fails.
     *
     * @param  string                                   $operation
     * @param  string                                   $type
     * @param  wgsZendAclRoleInterface|string|array     $roles
     * @param  wgsZendAclResourceInterface|string|array $resources
     * @param  string|array                             $privileges
     * @param  wgsZendAclAssertInterface                $assert
     * @throws wgsZendAclException
     * @uses   wgsZendAclRoleRegistry::get()
     * @uses   wgsZendAcl::get()
     * @return wgsZendAcl Provides a fluent interface
     */
    public function setRule($operation, $type, $roles = null, $resources = null, $privileges = null,
                            wgsZendAclAssertInterface $assert = null)
    {
        // ensure that the rule type is valid; normalize input to uppercase
        $type = strtoupper($type);
        if (self::TA !== $type && self::TD !== $type) {
            throw new wgsZendAclException("Unsupported rule type; must be either '" . self::TA . "' or '"
                                       . self::TD . "'");
        }

        // ensure that all specified Roles exist; normalize input to array of Role objects or null
        if (!is_array($roles)) {
            $roles = array($roles);
        } else if (0 === count($roles)) {
            $roles = array(null);
        }
        $rolesTemp = $roles;
        $roles = array();
        foreach ($rolesTemp as $role) {
            if (null !== $role) {
                $roles[] = $this->_getRoleRegistry()->get($role);
            } else {
                $roles[] = null;
            }
        }
        unset($rolesTemp);

        // ensure that all specified Resources exist; normalize input to array of Resource objects or null
        if (!is_array($resources)) {
            $resources = array($resources);
        } else if (0 === count($resources)) {
            $resources = array(null);
        }
        $resourcesTemp = $resources;
        $resources = array();
        foreach ($resourcesTemp as $resource) {
            if (null !== $resource) {
                $resources[] = $this->get($resource);
            } else {
                $resources[] = null;
            }
        }
        unset($resourcesTemp);

        // normalize privileges to array
        if (null === $privileges) {
            $privileges = array();
        } else if (!is_array($privileges)) {
            $privileges = array($privileges);
        }

        switch ($operation) {

            // add to the rules
            case self::OA:
                foreach ($resources as $resource) {
                    foreach ($roles as $role) {
                        $rules =& $this->_getRules($resource, $role, true);
                        if (0 === count($privileges)) {
                            $rules['allPrivileges']['type']   = $type;
                            $rules['allPrivileges']['assert'] = $assert;
                            if (!isset($rules['byPrivId'])) {
                                $rules['byPrivId'] = array();
                            }
                        } else {
                            foreach ($privileges as $privilege) {
                                $rules['byPrivId'][$privilege]['type']   = $type;
                                $rules['byPrivId'][$privilege]['assert'] = $assert;
                            }
                        }
                    }
                }
                break;

            // remove from the rules
            case self::OD:
                foreach ($resources as $resource) {
                    foreach ($roles as $role) {
                        $rules =& $this->_getRules($resource, $role);
                        if (null === $rules) {
                            continue;
                        }
                        if (0 === count($privileges)) {
                            if (null === $resource && null === $role) {
                                if ($type === $rules['allPrivileges']['type']) {
                                    $rules = array(
                                        'allPrivileges' => array(
                                            'type'   => self::TD,
                                            'assert' => null
                                            ),
                                        'byPrivId' => array()
                                        );
                                }
                                continue;
                            }
                            if ($type === $rules['allPrivileges']['type']) {
                                unset($rules['allPrivileges']);
                            }
                        } else {
                            foreach ($privileges as $privilege) {
                                if (isset($rules['byPrivId'][$privilege]) &&
                                    $type === $rules['byPrivId'][$privilege]['type']) {
                                    unset($rules['byPrivId'][$privilege]);
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new wgsZendAclException("Unsupported operation; must be either '" . self::OA . "' or '"
                                           . self::OD . "'");
        }

        return $this;
    }

    /**
     * Returns true if and only if the Role has access to the Resource
     *
     * The $role and $resource parameters may be references to, or the string identifiers for,
     * an existing Resource and Role combination.
     *
     * If either $role or $resource is null, then the query applies to all Roles or all Resources,
     * respectively. Both may be null to query whether the ACL has a "blacklist" rule
     * (allow everything to all). By default, wgsZendAcl creates a "whitelist" rule (deny
     * everything to all), and this method would return false unless this default has
     * been overridden (i.e., by executing $acl->allow()).
     *
     * If a $privilege is not provided, then this method returns false if and only if the
     * Role is denied access to at least one privilege upon the Resource. In other words, this
     * method returns true if and only if the Role is allowed all privileges on the Resource.
     *
     * This method checks Role inheritance using a depth-first traversal of the Role registry.
     * The highest priority parent (i.e., the parent most recently added) is checked first,
     * and its respective parents are checked similarly before the lower-priority parents of
     * the Role are checked.
     *
     * @param  wgsZendAclRoleInterface|string     $role
     * @param  wgsZendAclResourceInterface|string $resource
     * @param  string                             $privilege
     * @uses   wgsZendAcl::get()
     * @uses   wgsZendAclRoleRegistry::get()
     * @return boolean
     */
    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        if (null !== $role) {
            $role = $this->_getRoleRegistry()->get($role);
        }

        if (null !== $resource) {
            $resource = $this->get($resource);
        }

        if (null === $privilege) {
            // query on all privileges
            do {
                // depth-first search on $role if it is not 'allRoles' pseudo-parent
                if (null !== $role && null !== ($result = $this->_roleDFSAllPrivileges($role, $resource, $privilege))) {
                    return $result;
                }

                // look for rule on 'allRoles' psuedo-parent
                if (null !== ($rules = $this->_getRules($resource, null))) {
                    foreach ($rules['byPrivId'] as $privilege => $rule) {
                        if (self::TD === ($ruleTypeOnePrivilege = $this->_getRuleType($resource, null, $privilege))) {
                            return false;
                        }
                    }
                    if (null !== ($ruleTypeAllPrivileges = $this->_getRuleType($resource, null, null))) {
                        return self::TA === $ruleTypeAllPrivileges;
                    }
                }

                // try next Resource
                $resource = $this->_resources[$resource->getResourceId()]['p'];

            } while (true); // loop terminates at 'allResources' pseudo-parent
        } else {
            // query on one privilege
//            var_dump($this->_resources[$resource->getResourceId()]);
            do {

                // depth-first search on $role if it is not 'allRoles' pseudo-parent
                if (null !== $role && null !== ($result = $this->_roleDFSOnePrivilege($role, $resource, $privilege))) {
                    return $result;
                }

                // look for rule on 'allRoles' pseudo-parent
                if (null !== ($ruleType = $this->_getRuleType($resource, null, $privilege))) {
                    return self::TA === $ruleType;
                } else if (null !== ($ruleTypeAllPrivileges = $this->_getRuleType($resource, null, null))) {
                    return self::TA === $ruleTypeAllPrivileges;
                }

                // try next Resource
                $resource = $this->_resources[$resource->getResourceId()]['p'];

            } while (true); // loop terminates at 'allResources' pseudo-parent
        }
    }

    /**
     * Returns parent roles
     *
     * @param  wgsZendAclRoleInterface|string     $role
     * @return array
     */
    public function getRoleParents($role)
    {
        return $this->_getRoleRegistry()->getParents($role);
    }
    
    /**
     * Returns the Role registry for this ACL
     *
     * If no Role registry has been created yet, a new default Role registry
     * is created and returned.
     *
     * @return wgsZendAclRoleRegistry
     */
    protected function _getRoleRegistry()
    {
        if (null === $this->_roleRegistry) {
            $this->_roleRegistry = new wgsZendAclRoleRegistry();
        }
        return $this->_roleRegistry;
    }
    
    /**
     * Performs a depth-first search of the Role DAG, starting at $role, in order to find a rule
     * allowing/denying $role access to all privileges upon $resource
     *
     * This method returns true if a rule is found and allows access. If a rule exists and denies access,
     * then this method returns false. If no applicable rule is found, then this method returns null.
     *
     * @param  wgsZendAclRoleInterface     $role
     * @param  wgsZendAclResourceInterface $resource
     * @return boolean|null
     */
    protected function _roleDFSAllPrivileges(wgsZendAclRoleInterface $role, wgsZendAclResourceInterface $resource = null)
    {
        $dfs = array(
            'visited' => array(),
            'stack'   => array()
            );

        if (null !== ($result = $this->_roleDFSVisitAllPrivileges($role, $resource, $dfs))) {
            return $result;
        }

        while (null !== ($role = array_pop($dfs['stack']))) {
            if (!isset($dfs['visited'][$role->getRoleId()])) {
                if (null !== ($result = $this->_roleDFSVisitAllPrivileges($role, $resource, $dfs))) {
                    return $result;
                }
            }
        }

        return null;
    }

    /**
     * Visits an $role in order to look for a rule allowing/denying $role access to all privileges upon $resource
     *
     * This method returns true if a rule is found and allows access. If a rule exists and denies access,
     * then this method returns false. If no applicable rule is found, then this method returns null.
     *
     * This method is used by the internal depth-first search algorithm and may modify the DFS data structure.
     *
     * @param  wgsZendAclRoleInterface     $role
     * @param  wgsZendAclResourceInterface $resource
     * @param  array                  $dfs
     * @return boolean|null
     */
    protected function _roleDFSVisitAllPrivileges(wgsZendAclRoleInterface $role, wgsZendAclResourceInterface $resource = null,
                                                 &$dfs)
    {
        if (null !== ($rules = $this->_getRules($resource, $role))) {
            foreach ($rules['byPrivId'] as $privilege => $rule) {
                if (self::TD === ($ruleTypeOnePrivilege = $this->_getRuleType($resource, $role, $privilege))) {
                    return false;
                }
            }
            if (null !== ($ruleTypeAllPrivileges = $this->_getRuleType($resource, $role, null))) {
                return self::TA === $ruleTypeAllPrivileges;
            }
        }

        $dfs['visited'][$role->getRoleId()] = true;
        foreach ($this->_getRoleRegistry()->getParents($role) as $roleParentId => $roleParent) {
            $dfs['stack'][] = $roleParent;
        }

        return null;
    }

    /**
     * Performs a depth-first search of the Role DAG, starting at $role, in order to find a rule
     * allowing/denying $role access to a $privilege upon $resource
     *
     * This method returns true if a rule is found and allows access. If a rule exists and denies access,
     * then this method returns false. If no applicable rule is found, then this method returns null.
     *
     * @param  wgsZendAclRoleInterface     $role
     * @param  wgsZendAclResourceInterface $resource
     * @param  string                      $privilege
     * @return boolean|null
     */
    protected function _roleDFSOnePrivilege(wgsZendAclRoleInterface $role, wgsZendAclResourceInterface $resource = null, $privilege)
    {
        $dfs = array(
            'visited' => array(),
            'stack'   => array()
            );

        if (null !== ($result = $this->_roleDFSVisitOnePrivilege($role, $resource, $privilege, $dfs))) {
            return $result;
        }

        while (null !== ($role = array_pop($dfs['stack']))) {
            if (!isset($dfs['visited'][$role->getRoleId()])) {
                if (null !== ($result = $this->_roleDFSVisitOnePrivilege($role, $resource, $privilege, $dfs))) {
                    return $result;
                }
            }
        }
        return null;
    }

    /**
     * Visits an $role in order to look for a rule allowing/denying $role access to a $privilege upon $resource
     *
     * This method returns true if a rule is found and allows access. If a rule exists and denies access,
     * then this method returns false. If no applicable rule is found, then this method returns null.
     *
     * This method is used by the internal depth-first search algorithm and may modify the DFS data structure.
     *
     * @param  wgsZendAclRoleInterface     $role
     * @param  wgsZendAclResourceInterface $resource
     * @param  string                      $privilege
     * @param  array                       $dfs
     * @return boolean|null
     */
    protected function _roleDFSVisitOnePrivilege(wgsZendAclRoleInterface $role, wgsZendAclResourceInterface $resource = null,
                                                $privilege, &$dfs)
    {
        if (null !== ($ruleTypeOnePrivilege = $this->_getRuleType($resource, $role, $privilege))) {
            return self::TA === $ruleTypeOnePrivilege;
        } else if (null !== ($ruleTypeAllPrivileges = $this->_getRuleType($resource, $role, null))) {
            return self::TA === $ruleTypeAllPrivileges;
        }

        $dfs['visited'][$role->getRoleId()] = true;
        foreach ($this->_getRoleRegistry()->getParents($role) as $roleParentId => $roleParent) {
            //$dfs['stack'][] = $roleParent;
            array_push($dfs['stack'], $roleParent); //fixed by fisher (18.03.2008)
        }

        return null;
    }

    /**
     * Returns the rule type associated with the specified Resource, Role, and privilege
     * combination.
     *
     * If a rule does not exist or its attached assertion fails, which means that
     * the rule is not applicable, then this method returns null. Otherwise, the
     * rule type applies and is returned as either TA or TD.
     *
     * If $resource or $role is null, then this means that the rule must apply to
     * all Resources or Roles, respectively.
     *
     * If $privilege is null, then the rule must apply to all privileges.
     *
     * If all three parameters are null, then the default ACL rule type is returned,
     * based on whether its assertion method passes.
     *
     * @param  wgsZendAclResourceInterface $resource
     * @param  wgsZendAclRoleInterface     $role
     * @param  string                      $privilege
     * @return string|null
     */
    protected function _getRuleType(wgsZendAclResourceInterface $resource = null, wgsZendAclRoleInterface $role = null,
                                    $privilege = null)
    {
        // get the rules for the $resource and $role
        if (null === ($rules = $this->_getRules($resource, $role))) {
            return null;
        }

        // follow $privilege
        if (null === $privilege) {
            if (isset($rules['allPrivileges'])) {
                $rule = $rules['allPrivileges'];
            } else {
                return null;
            }
        } else if (!isset($rules['byPrivId'][$privilege])) {
            return null;
        } else {
            $rule = $rules['byPrivId'][$privilege];
        }

        // check assertion if necessary
        if (null === $rule['assert'] || $rule['assert']->assert($this, $role, $resource, $privilege)) {
            return $rule['type'];
        } else if (null !== $resource || null !== $role || null !== $privilege) {
            return null;
        } else if (self::TA === $rule['type']) {
            return self::TD;
        } else {
            return self::TA;
        }
    }

    /**
     * Returns the rules associated with a Resource and a Role, or null if no such rules exist
     *
     * If either $resource or $role is null, this means that the rules returned are for all Resources or all Roles,
     * respectively. Both can be null to return the default rule set for all Resources and all Roles.
     *
     * If the $create parameter is true, then a rule set is first created and then returned to the caller.
     *
     * @param  wgsZendAclResourceInterface $resource
     * @param  wgsZendAclRoleInterface     $role
     * @param  boolean                     $create
     * @return array|null
     */
    protected function &_getRules(wgsZendAclResourceInterface $resource = null, wgsZendAclRoleInterface $role = null,
                                  $create = false)
    {
        // create a reference to null
        $null = null;
        $nullRef =& $null;

        // follow $resource
        do {
            if (null === $resource) {
                $visitor =& $this->_rules['allResources'];
                break;
            }
            $resourceId = $resource->getResourceId();
            if (!isset($this->_rules['byResId'][$resourceId])) {
                if (!$create) {
                    return $nullRef;
                }
                $this->_rules['byResId'][$resourceId] = array();
            }
            $visitor =& $this->_rules['byResId'][$resourceId];
        } while (false);


        // follow $role
        if (null === $role) {
            if (!isset($visitor['allRoles'])) {
                if (!$create) {
                    return $nullRef;
                }
                $visitor['allRoles']['byPrivId'] = array();
            }
            return $visitor['allRoles'];
        }
        $roleId = $role->getRoleId();
        if (!isset($visitor['byRoleId'][$roleId])) {
            if (!$create) {
                return $nullRef;
            }
            $visitor['byRoleId'][$roleId]['byPrivId'] = array();
        }
        return $visitor['byRoleId'][$roleId];
    } 
}

?>
