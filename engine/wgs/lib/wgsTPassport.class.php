<?php
define("_PASSPORT_SCHEME", "tnhk_passport");
define("_SDB_SCHEME", "tnhk_sdb");
define("_ADB_SCHEME", "tnhk_adb");

define("SQL_TEST", false);

class wgsTPassport
{
	function connect()
	{
		//$this->conn = OCILogon("tnhk_passport", "tnhkpas", "GISS");
		$this->db_conn = OCILogon("tnhk_passport", "tnhkpas", "GIS");
		if (!$this->db_conn) {
			$err = OCIError();
			echo "Oracle Connect Error " . $err['text'];
		}
	}
	
	function disconnect()
	{
		if (!$this->db_conn) return true;
		if (@OCILogOff($this->db_conn)) {
			//$this->db_last_error=parse(E_SQL_DISCONNECT, array('host' => $this->db_service, 'sql_error' => $this->sql_error()));
			//if (SQL_TEST) $this->print_error();
			return false;
		}
		return true;
	}
	
	function sql_error($p_query_stmt = false)
	{
		if (!$p_query_stmt)
			$error = OCIError();
		else
			$error = $p_query_stmt;
		return $error['message'];
	}
	
	
	function sql_query($p_sql)
	{
		//$sql = $this->parse($p_sql);
		$sql = $p_sql;
		//if (!$this->connect()) return false;
		if (!$query_stmt=@OCIParse($this->db_conn,$sql)) {
			$query_stmt = @OCIParse($this->db_conn);
			//$this->db_last_error=parse(E_SQL_PARSE,array('sql_error'=>$this->sql_error($query_stmt),'sql'=>$sql));
			//if (SQL_TEST) $this->print_error();
			return false;
		}
		if (!@OCIExecute($query_stmt,OCI_DEFAULT)) {
			$query_stmt = @OCIError($query_stmt);
			//$this->db_last_error=parse(E_SQL_EXEC,array('sql_error'=>$this->sql_error($query_stmt),'sql'=>$sql));
			//if (SQL_TEST) $this->print_error();
			return false;
		}
		return $query_stmt;
	}
	
	function sql_assoc_array($p_sql,$p_page=false,$p_rownum=false)
	{
		//if ( $p_page && $p_rownum )
		//	$p_sql = $this->parsePaginalOutput($p_sql, $p_page, $p_rownum);
		$result=Array();
		if (!$stmt=$this->sql_query($p_sql)) return false;
		while (@OCIFetchInto ($stmt, $row, OCI_ASSOC+OCI_RETURN_NULLS)) $result[]=$row;
		@OCIFreeStatement($stmt);
		return $result;
		//return ParseAposFromOracle(ParseQuotesAndTugs($result));
	}
	
	function sql_assoc_row($p_sql)
	{
		if (!$stmt=$this->sql_query($p_sql)) return false;
		@OCIFetchinto($stmt,$result,OCI_ASSOC+OCI_RETURN_NULLS);
		@OCIFreeStatement($stmt);
		return $result;
		//return ParseAposFromOracle(ParseQuotesAndTugs($result));
	}
	
	function sql_value($p_sql)
	{
		if (!$stmt=$this->sql_query($p_sql)) return false;
		@OCIFetchinto($stmt,$result,OCI_NUM+OCI_RETURN_NULLS);
		@OCIFreeStatement($stmt);
		return $result[0];
	}
	
	function sql_lob_value($p_sql)
	{
		if (!$stmt=$this->sql_query($p_sql)) return false;
		@OCIFetchinto($stmt,$result,OCI_RETURN_LOBS);
		@OCIFreeStatement($stmt);
		return $result[0];
	}
	
	
	
	function get_title_list($p_object_id)
	{

		$sql_query = "select pas.date_standard_of_price,pas.passport_id,pas.inventory_number, pas.name_object, pas.create_date,pr.production_name, pou.place_of_use_name,co.compound_object_name,co.compound_object_address, co.compound_object_building,co.compound_object_licence,
							pas.create_date,pas.all_cost,pas.production_part,pas.service_part,pas.superintendent,pas.deputy_chief_engineer
     					  ,pas.year_of_building,pas.number_of_storeys,pas.nos_ppart,pas.nos_ppart_basement,pas.nos_spart,pas.nos_spart_basement
     					  ,pas.area_of_building_all,pas.aob_ppart,pas.aob_ppart_basement,pas.aob_spart,pas.aob_spart_basement
    					   ,pas.volume_of_building_all,pas.vob_ppart,pas.vob_ppart_basement,pas.vob_spart,pas.vob_spart_basement

					  from "._PASSPORT_SCHEME.".bpasport pas, "._PASSPORT_SCHEME.".bp_production pr, "._PASSPORT_SCHEME.".bp_place_of_use pou, "._PASSPORT_SCHEME.".bp_compound_object co
					  where pas.passport_id IN (
						                        select t.passport_ID from "._PASSPORT_SCHEME.".bp_geometry t
                        						where t.geometry_id='$p_object_id'
                        					   )
						 AND pas.production_id=pr.production_id(+)
 						AND pas.place_of_use_id=pou.place_of_use_id(+)
						 AND pas.compound_object_id=co.compound_object_id(+)";

		return $this->sql_assoc_row($sql_query);
	}

	function get_general_info($p_passport_id)
    {
    	$sql_query = "select * from "._PASSPORT_SCHEME.".bp_general_info t where t.passport_id=$p_passport_id";
    	return $this->sql_assoc_array($sql_query);
    }
//**********��������������A�_�����
	
	function get_base_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=1) and  t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
    
	function get_pillar_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=2) and  t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_crane_girder_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=3) and  t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_wall_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where t.guide_id=bp.guide_id and bp.guide_typ=4 and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
    
	function get_peregorodki_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where t.guide_id=bp.guide_id and bp.guide_typ=5 and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_nes_konstrukt_perekr_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where t.guide_id=bp.guide_id and bp.guide_typ=6 and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_nes_konstrukt_krovli_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where t.guide_id=bp.guide_id and bp.guide_typ=7 and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_yteplit_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.* 
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where t.guide_id=bp.guide_id and bp.guide_typ=8 and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_krovlya_ppart($p_passport_id)
	{
		$sql_query="select t.*, bp.* 
					from "._PASSPORT_SCHEME.".bp_value_ppart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=9) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
//************��������_�������_�����
	
	function get_base_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=1) and  t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_nes_karkas_sbpart($p_passport_id)
	{
		$sql_query="select t.*, t.rowid, bp.*, bp.rowid 
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=10) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_wall_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=4) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_peregorodki_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=5) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_mej_store_perekr_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=12) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_cherdak_perekr_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=11) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	
	function get_lestniza_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=13) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_nes_elem_krovli_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.*
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=7) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_krovlya_sbpart($p_passport_id)
	{
		$sql_query="select t.*, bp.* 
					from "._PASSPORT_SCHEME.".bp_value_spart t, "._PASSPORT_SCHEME.".bp_guide bp
					where (t.guide_id=bp.guide_id) and (bp.guide_typ=9) and t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_area_of_rooms($p_passport_id)
	{
		$sql_query="select pas.sr_ppart_all,pas.sr_ppart_entresol_all,pas.sr_ppart_one_storied,pas.sr_ppart_many_storied
      				,pas.sr_ppart_entresol_all,pas.sr_ppart_entresol_one_storied,pas.sr_ppart_entresol_many_storied
      				,pas.sr_ppart_basement_all,pas.sr_ppart_basement_one_storied,pas.sr_ppart_basement_many_storied
      				,pas.sr_depot_all,pas.sr_depot_one_storied,pas.sr_depot_many_storied
      				,pas.sr_depot_entresol_all,pas.sr_depot_entresol_one_storied,pas.sr_depot_entresol_many_storied
      				,pas.sr_depot_basement_all,pas.sr_depot_basement_one_storied,pas.sr_depot_basement_many_storied
      				,pas.sr_spart_all,pas.sr_spart_one_storied,pas.sr_spart_many_storied
      				,pas.sr_spart_entresol_all,pas.sr_spart_entresol_one_storied,pas.sr_spart_entresol_many_storied
      				,pas.sr_spart_basement_all,pas.sr_spart_basement_one_storied,pas.sr_spart_basement_many_storied
						from "._PASSPORT_SCHEME.".bpasport pas
						where pas.passport_id  = '$p_passport_id'";
		return $this->sql_assoc_row($sql_query);
	}
	
	function get_area_of_other_rooms($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name
					from "._PASSPORT_SCHEME.".bp_room_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=14) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_area_of_floor($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name
					from "._PASSPORT_SCHEME.".bp_floor t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=16) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_razmeri_naryj_poverhn($p_passport_id)
	{
		$sql_query="select pas.s_os_wall_minus_aperture,pas.s_os_end_wall_of_lantern,pas.s_os_wall_senestration_all
     				 	,pas.s_os_lantern_glazing_all,pas.s_os_glazing
    				  	,pas.s_os_autogate,pas.q_os_autogate
   					   	,pas.s_os_railroadgate,pas.q_os_railroadgate
   					   	,pas.s_os_external_door,pas.q_os_external_door
      					,pas.s_os_roof_all
					from "._PASSPORT_SCHEME.".bpasport pas
					where pas.passport_id = '$p_passport_id'";
		return $this->sql_assoc_row($sql_query);
	}
	
	function get_razmeri_stekol_1($p_passport_id)
	{
		$sql_query="select t.*
					from "._PASSPORT_SCHEME.".bp_glass t
					where  t.glass_type=1 AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_razmeri_stekol_2($p_passport_id)
	{
		$sql_query="select t.*, t.rowid
					from "._PASSPORT_SCHEME.".bp_glass t
					where t.glass_type=2 AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_roofing_value($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_roofing_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=9) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_razmeri_vnytr_poverh($p_passport_id)
	{
		$sql_query="select pas.s_Is_Floor_All,pas.s_is_wall_all,pas.s_is_septa_all,pas.s_is_pillar_all,pas.s_is_crane_girder_all,pas.s_is_other_all
					from "._PASSPORT_SCHEME.".bpasport pas
					where pas.passport_id = '$p_passport_id'";
		return $this->sql_assoc_row($sql_query);
	}

	function get_is_ceiling($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name
					from "._PASSPORT_SCHEME.".bp_ceiling_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=6) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_is_wall($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_wall_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=4) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_is_septa($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_septa_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=5) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_is_pillar($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_pillar_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=2) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_is_crane_girder($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_crane_girder_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=3) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_is_other($p_passport_id)
	{
		$sql_query="select t.*, g.guide_name, t.rowid
					from "._PASSPORT_SCHEME.".bp_other_value t, "._PASSPORT_SCHEME.".bp_guide g
					Where (g.guide_typ=15) AND (t.guide_id=g.guide_id) AND t.passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}
	
	function get_note($p_passport_id)
	{
		$sql_query="select pas.load_girder,pas.load_crane_girder,pas.load_floor,pas.load_story_ceiling,pas.note
					from "._PASSPORT_SCHEME.".bpasport pas
					where pas.passport_id = '$p_passport_id'";
		return $this->sql_assoc_row($sql_query);
	}

	// �������: ������� �.�, 23 ����
	function get_binded_objects($p_passport_id)
	{
		$sql_query_objects = "select distinct table_name, object_id from "._PASSPORT_SCHEME.".bp_geometry t1, {scheme}gp_object t2
		where t1.geometry_id = t2.object_id
		and t1.passport_id = '$p_passport_id'";

		$tab_list = $this->sql_assoc_array($sql_query_objects);
			
		if (empty($tab_list))
			return false;
			
		$objects_list = array();
		foreach($tab_list as $key => $value)
		{
			$sql_query = "select t.object_name from {scheme}".$value['TABLE_NAME']." t where t.object_id = '".$value['OBJECT_ID']."'";
			$object_name = $this->db->sql_value($sql_query);
			
			$objects_list[$key]['OBJECT_NAME'] = $object_name;
			$objects_list[$key]['OBJECT_ID'] = $value['OBJECT_ID'];
			
		}
		
		return $objects_list;
	}
	
	function bind_objects($p_passport_id,$p_objects)
	{
		$aObjects = explode(",",$p_objects);
		foreach ($aObjects as $key => $value)
		{
			if (!$this->db->sql_value("select * from "._PASSPORT_SCHEME.".bp_geometry where PASSPORT_ID = '$p_passport_id' and GEOMETRY_ID = '$value'"))
				$this->db->sql_insert_ex(""._PASSPORT_SCHEME.".bp_geometry",array('PASSPORT_ID' => $p_passport_id, 'GEOMETRY_ID' => $value));			
		}
	}

	function unbind_objects($p_passport_id,$p_objects)
	{
		$aObjects = explode(",",$p_objects);
		foreach ($aObjects as $key => $value)
		{
			$this->db->sql_delete_ex(""._PASSPORT_SCHEME.".bp_geometry",array('PASSPORT_ID' => $p_passport_id, 'GEOMETRY_ID' => $value));			
		}		
	}
	
	function get_floorplan_names($p_passport_id)
	{
		$sql_query = "select floor_plan_id, floor_plan_name from "._PASSPORT_SCHEME.".bp_floor_plan where passport_id = '$p_passport_id'";
		return $this->sql_assoc_array($sql_query);
	}

	function get_floorplan_name($p_floorplan_id)
	{
		$sql_query = "select floor_plan_name from "._PASSPORT_SCHEME.".bp_floor_plan where floor_plan_id = '$p_floorplan_id'";
		return $this->db->sql_value($sql_query);
	}

	function get_floorplan_picture($p_floorplan_id)
	{
		$sql_query = "select picture from "._PASSPORT_SCHEME.".bp_floor_plan where floor_plan_id = '$p_floorplan_id'";
		return $this->sql_lob_value($sql_query);
	}
}
?>
