<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsZendAclResource implements wgsZendAclResourceInterface
{
    protected $id;

    public function __construct($resourceId)
    {
        $this->id = (string) $resourceId;
    }
    
    public function getResourceId()
    {
        return $this->id;
    }
}

?>