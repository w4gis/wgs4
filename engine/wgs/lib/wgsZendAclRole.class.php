<?php

lmb_require('wgs/lib/wgsZendAclRoleInterface.interface.php');

class wgsZendAclRole implements wgsZendAclRoleInterface
{
    protected $id;

    public function __construct($roleId)
    {
        $this->id = (string) $roleId;
    }

    public function getRoleId()
    {
        return $this->id;
    }
}

?>