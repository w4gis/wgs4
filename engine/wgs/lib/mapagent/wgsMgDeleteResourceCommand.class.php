<?php
lmb_require('wgs/lib/mapagent/wgsMgCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� DELETERESOURCE, �.�. ��������
 * �������� ���� ����� (WebLayout,Map,Layer)� MgStudio � ��.
 *
 */
class wgsMgDeleteResourceCommand extends wgsMgCommand
{
	function __construct($request, $xml)
	{
		parent::__construct($request, $xml);
	}

	public function run()
	{
		if(strpos($this->request['RESOURCEID'], 'Session:')===false)
		{
			$typeofobject = $this->getTypeOfObject($this->request);
			if (isset($typeofobject)) {	
    			switch ($typeofobject)
    			{
    				case 'WebLayout':
    					$this->mgWebLayoutService =  $this->objectService->getMgWebLayoutService();
    					$this->mgWebLayoutService->removeMgWebLayout($this->request['RESOURCEID']);
    					break;
    				case 'MapDefinition':
    					$this->mgMapService =  $this->objectService->getMgMapService();
    					$this->mgMapService->removeMgMap($this->request['RESOURCEID']);
    					break;
    				case 'LayerDefinition':
    					$this->mgLayerService =  $this->objectService->getMgLayerService();
    					$this->mgLayerService->removeMgLayer($this->request['RESOURCEID']);
    					break;
    			}
			} else {
			    $this->mgResourceService =  $this->objectService->getMgResourceService();
                $this->mgResourceService->removeFolder($this->request['RESOURCEID']);
			}
		}
	}
	
	public function getTypeOfObject($request)
	{
		$types = array('WebLayout','MapDefinition','LayerDefinition');
		foreach ($types as $index)
		{
			if(substr_count($request['RESOURCEID'], '.'.$index) != 0)
			return $index;
		}
	}
}
?>