<?php
lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� SETRESOURCE �������� ���� WebLayout.
 *
 */
class wgsMgSetResourceCommandForWebLayout extends wgsMgSetResourceCommand
{
    private $mgWebLayoutService;

    function __construct($request,$xml)
    {
        parent::__construct($request,$xml);
        
        $this->mgWebLayoutService =  $this->objectService->getMgWebLayoutService();
    }

    public function start()
    {

        $aWebLayout = $this->createAssocArrayFromXML(); // ������� ������������� ������ ��������� �� xml

        $names = array();
        $invokescripts = array();

        if(count($aWebLayout['COMMANDS']) == 0)
        {
            $names[0] = 'null';
            $invokescripts[0] = 'null';
        }
        else
        {
            for($i = 0; $i < count($aWebLayout['COMMANDS']); $i++)
            {
                $names[] = (string)$aWebLayout['COMMANDS'][$i]['NAME'];
                $invokescripts[] = (string)$aWebLayout['COMMANDS'][$i]['INVOKESCRIPT'];
            }
        }
     
        $webLayoutId = $this->mgWebLayoutService->addMgWebLayout($this->request['RESOURCEID'],$aWebLayout['TITLE'],$aWebLayout['MAP'],$names,$invokescripts);
   
        if($names[0] != 'null')
        for($j = 0; $j < count($aWebLayout['COMMANDS']); $j++)
        {
            $this->mgWebLayoutService->setMgWebLayout($webLayoutId,$aWebLayout['COMMANDS'][$j]['NAME'],$aWebLayout['COMMANDS'][$j]['LABEL'],$aWebLayout['COMMANDS'][$j]['DESCRIPTION'],$aWebLayout['COMMANDS'][$j]['INVOKESCRIPT'],$aWebLayout['COMMANDS'][$j]['IMAGEURL']);
        }

    }

    private function createAssocArrayFromXML()
    {
        $xml = new SimpleXMLElement($this->xml);

        $weblayout = array('TITLE' => (string)$xml->Title, 'MAP' => (string)$xml->Map->ResourceId, 'COMMANDS' => array());
        $index = 0;
        foreach ($xml->xpath('//Command') as $command)
        {
            if (isset($command->Script))
            {
                $weblayout['COMMANDS'][$index]['NAME'] = (string)$command->Name;
                $weblayout['COMMANDS'][$index]['LABEL'] = (string)$command->Label;
                $weblayout['COMMANDS'][$index]['DESCRIPTION'] = (string)$command->Description;
                $weblayout['COMMANDS'][$index]['INVOKESCRIPT'] = (string)$command->Script.' ';
                $weblayout['COMMANDS'][$index]['IMAGEURL'] = (string)$command->ImageURL;
                
                
                $index++;
            }
        }
        return $weblayout;
    }
}
?>