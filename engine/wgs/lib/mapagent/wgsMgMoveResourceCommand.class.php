<?php
lmb_require('wgs/lib/mapagent/wgsMgCommand.class.php');

/**
 * ����� ������������ ��� ��������� ������� MOVERESOURCE, �.�. ��������������/�����������
 * �������� ���� ����� (WebLayout,Map,Layer)� MgStudio � ��.
 *
 */
class wgsMgMoveResourceCommand extends wgsMgCommand
{
    function __construct($request, $xml)
    {
        parent::__construct($request, $xml);
    }

    public function run()
    {
            $typeofobject = $this->getTypeOfObject($this->request);
           if (isset($typeofobject)) { 
                fwrite($f, 'moving...');
				
				switch ($typeofobject)
                {
                    case 'WebLayout':
                        $this->mgWebLayoutService =  $this->objectService->getMgWebLayoutService();
                        $this->mgWebLayoutService->renameMgWebLayout($this->request['SOURCE'],$this->request['DESTINATION']);
                        break;
                    case 'MapDefinition':
                        $this->mgMapService =  $this->objectService->getMgMapService();
                        $this->mgMapService->renameMgMap($this->request['SOURCE'],$this->request['DESTINATION']);
                        break;
                    case 'LayerDefinition':
                        $this->mgLayerService =  $this->objectService->getMgLayerService();
                        $this->mgLayerService->renameMgLayer($this->request['SOURCE'],$this->request['DESTINATION']);
                        break;
                }
            } else {
                $this->mgResourceService =  $this->objectService->getMgResourceService();
                $this->mgResourceService->renameFolder($this->request['SOURCE'],$this->request['DESTINATION']);
            }     
    }

    public function getTypeOfObject($request)
    {
        $types = array('WebLayout','MapDefinition','LayerDefinition');
        foreach ($types as $index)
        {
            if(substr_count($request['SOURCE'], '.'.$index) != 0)
            return $index;
        }
    }
}
?>