<?php
lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� SETRESOURCE �������� ���� MapDefinition.
 *
 */
class wgsMgSetResourceCommandForMapDefinition extends wgsMgSetResourceCommand
{
	private $mgMapService;

	function __construct($request,$xml)
	{
		parent::__construct($request,$xml);
		//$this->request = $request;
		//$this->xml =$xml;
		$this->mgMapService =  $this->objectService->getMgMapService();
	}

	public function start()
	{

		$aMap = $this->createAssocArrayFromXML(); // ������� ������������� ������ ��������� �� xml

		$resourceIds = array();

		if(count($aMap['MAPLAYERS'])==0)
		{
			$resourceIds[0] = 'null';
		}
		else
		{
			for($i = 0; $i < count($aMap['MAPLAYERS']); $i++)
			{
				$resourceIds[] = $aMap['MAPLAYERS'][$i]['RESOURCEID'];
			}
		}
		$mgMapId = $this->mgMapService->addMgMap($this->request['RESOURCEID'],$resourceIds);

		for($j = 0; $j < count($aMap['MAPLAYERS']); $j++)
		{
			$this->mgMapService->setMgMap($mgMapId,$aMap['MAPLAYERS'][$j]['RESOURCEID'],$aMap['MAPLAYERS'][$j]['LEGENDLABEL'],$aMap['MAPLAYERS'][$j]['NAME']);
		}

	}

	private function createAssocArrayFromXML()
	{
		$xml = simplexml_load_string($this->xml);

		$map = array('NAME' => (string)$xml->Name, 'LEGEND' => (string)$xml->Metadata, 'MAPLAYERGROUPS' => array(),'MAPLAYERS' => array());

		$index = 0;
		foreach ($xml->xpath('//BaseMapLayer') as $basemaplayer)
        {
            $map['MAPLAYERS'][$index]['NAME'] = (string)$basemaplayer->Name;
            $map['MAPLAYERS'][$index]['RESOURCEID'] = (string)$basemaplayer->ResourceId;
            $map['MAPLAYERS'][$index]['LEGENDLABEL'] = (string)$basemaplayer->LegendLabel;
            //$map['MAPLAYERS'][$index]['GROUP'] = $maplayer->Group;
            $index++;
        }
		
		foreach ($xml->xpath('//MapLayer') as $maplayer)
		{
			$map['MAPLAYERS'][$index]['NAME'] = (string)$maplayer->Name;
			$map['MAPLAYERS'][$index]['RESOURCEID'] = (string)$maplayer->ResourceId;
			$map['MAPLAYERS'][$index]['LEGENDLABEL'] = (string)$maplayer->LegendLabel;
			$map['MAPLAYERS'][$index]['GROUP'] = (string)$maplayer->Group;
			$index++;
		}

		$index = 0;
		foreach ($xml->xpath('//MapLayerGroup') as $maplayergroup)
		{
			$map['MAPLAYERGROUPS'][$index]['NAME'] = (string)$maplayergroup->Name;
			$map['MAPLAYERGROUPS'][$index]['LEGENDLABEL'] = (string)$maplayergroup->LegendLabel;
			$map['MAPLAYERGROUPS'][$index]['GROUP'] = (string)$maplayergroup->Group;
			$index++;
		}
		return $map;
	}
}
?>