<?php
lmb_require('wgs/lib/mapagent/wgsMgCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� SETRESOURCE, �.�. ��������/����������
 * �������� � MgStudio � ��. ��������� ���� ����������, �������� ������� �������� �������,
 * ���������� �� ���������� ������ ������� ��� ������������� ���� ��������. 
 *
 */
class wgsMgSetResourceCommand extends wgsMgCommand
{
	
	function __construct($request, $xml)
	{
		parent::__construct($request,$xml);
			
	}

	public function run()
	{
        if(strpos($this->request['RESOURCEID'], 'Session:')===false){
			$xml = simplexml_load_string($this->xml);
			$typeofobject = $this->getTypeOfObject($xml);
			switch ($typeofobject){
				case 'LayerDefinition':
					{
					lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommandForLayerDefinition.class.php');
					$obj = new wgsMgSetResourceCommandForLayerDefinition($this->request,$this->xml);
					$obj->start();
					}
					break;
				case 'MapDefinition':
					{
					lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommandForMapDefinition.class.php');
					$obj = new wgsMgSetResourceCommandForMapDefinition($this->request,$this->xml);
					$obj->start();
					}
					break;
				case 'WebLayout':
					{
					lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommandForWebLayout.class.php');
					$obj = new wgsMgSetResourceCommandForWebLayout($this->request,$this->xml);
					$obj->start();
					}
					break;
				default: 
					break;
			}
		}		
	}

	public  function getTypeOfObject($xml)
	{
		$types = array('WebLayout' => 0,'MapDefinition' => 0,'LayerDefinition' => 0);

		foreach ($types as $index => $value)
		{
			$result = $xml->xpath('/'.$index);
			$schet = 0;
			while(list( , $node) = each($result))
			$schet++;
			if($schet != 0)
			{
				$value = 1;
				return $index;
			}
		}
	}

}
?>