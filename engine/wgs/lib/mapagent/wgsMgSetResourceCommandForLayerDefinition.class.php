<?php
lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� SETRESOURCE �������� ���� MapDefinition.
 *
 */
class wgsMgSetResourceCommandForLayerDefinition extends wgsMgSetResourceCommand
{
	private $mgLayerService;
	function __construct($request,$xml)
	{
		parent::__construct($request,$xml);
        
		$this->mgLayerService =  $this->objectService->getMgLayerService();
    }

	public function start()
	{
	    if($this->mgLayerService->isMgLayerExist($this->request['RESOURCEID']) == 1)
		{
			$this->set();
		}
		else
		{
			$this->add();
		}
    }

	private function add()
	{
		$this->mgLayerService->addMgLayer($this->request['RESOURCEID'],$this->getFeatureLayerName());
       
	}
	
	private function set()
	{
		if($this->request['RESOURCEID'] && $this->getFeatureLayerName())
	    $this->mgLayerService->setMgLayer($this->request['RESOURCEID'],$this->getFeatureLayerName());
	}
	
	private function getFeatureLayerName()
	{
		$xml = simplexml_load_string($this->xml);
		if($xml->VectorLayerDefinition) {
            $feature_layer_name = (string)$xml->VectorLayerDefinition->FeatureName;
            $layer_definition = (string)$xml->VectorLayerDefinition;
		} elseif($xml->GridLayerDefinition) {
            $feature_layer_name = (string)$xml->GridLayerDefinition->FeatureName;
            $layer_definition = (string)$xml->GridLayerDefinition;
		}
		if(isset($feature_layer_name))
            $temp = explode(':',$feature_layer_name);

        return $temp[1];
        /*if($temp[0] == 'KingOra' && $temp[1] != 'FEATURETABLE') {
		    return $temp[1];
		} else {
		    if($layer_definition->Filter)
                if(eregi(".*(ADMPLAYER|\"ADMPLAYER\")[= ]*'([^']*)'.*",$layer_definition->Filter, $regs)) {
                    return $regs[2];        
                }
		}*/
		
	}
}
?>