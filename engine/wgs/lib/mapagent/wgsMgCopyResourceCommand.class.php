<?php
lmb_require('wgs/lib/mapagent/wgsMgCommand.class.php');
/**
 * ����� ������������ ��� ��������� ������� COPYRESOURCE, �.�. �����������
 * �������� ���� ����� (WebLayout,Map,Layer) � MgStudio � ��.
 *
 */
class wgsMgCopyResourceCommand extends wgsMgCommand
{
	function __construct($request, $xml)
	{
		parent::__construct($request, $xml);
	}

	public function run()
	{
		$typeofobject = $this->getTypeOfObject($this->request);
		if (isset($typeofobject) && (strpos($this->request['DESTINATION'], 'Session:')===false)) { 
			switch ($typeofobject)
    		{
    			case 'WebLayout':
    				$this->mgWebLayoutService =  $this->objectService->getMgWebLayoutService();
    				$this->mgWebLayoutService->copyMgWebLayout($this->request['SOURCE'],$this->request['DESTINATION']);
    				break;
    			case 'MapDefinition':
    				$this->mgMapService =  $this->objectService->getMgMapService();
    				$this->mgMapService->copyMgMap($this->request['SOURCE'],$this->request['DESTINATION']);
    				break;
    			case 'LayerDefinition':
    				$this->mgLayerService =  $this->objectService->getMgLayerService();
    				$this->mgLayerService->copyMgLayer($this->request['SOURCE'],$this->request['DESTINATION']);
    				break;
    		}
        } else if(strpos($this->request['DESTINATION'], 'Session:')===false) {
            $this->mgResourceService =  $this->objectService->getMgResourceService();
            $this->mgResourceService->copyFolder($this->request['SOURCE'],$this->request['DESTINATION']);
        }
		 
	}

	public function getTypeOfObject($request)
	{
		$types = array('WebLayout','MapDefinition','LayerDefinition');
		foreach ($types as $index)
		{
			if(substr_count($request['SOURCE'], '.'.$index) != 0)
			return $index;
		}
	}
}
?>