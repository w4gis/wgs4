<?php

/**
 * ����� ������������ ��� �������� �������� �������, '����������������' ���������� ��������� 
 * MapGuide � �������������� �� ������� � ���� Oracle, � ����������� �� ������������ �������� 
 * ��� ��������� � MGStudio (����������, ��������, �����������/�����������, ��������� ���������).  
 * 
 *
 */
class wgsMgCommandDispatcher
{
	public static function factory($request,$xml)
	{
        //$file = fopen("C:/log/log.log","a+");
        //fwrite($file, "request: "."-----------------DISPATCHER: ".$request['OPERATION']."\r\n");
		switch ($request['OPERATION'])
		{
			case 'SETRESOURCE':
				{
					lmb_require('wgs/lib/mapagent/wgsMgSetResourceCommand.class.php');
					return new wgsMgSetResourceCommand($request,$xml);
				} break;
			case 'MOVERESOURCE':
				{
					lmb_require('wgs/lib/mapagent/wgsMgMoveResourceCommand.class.php');
					return new wgsMgMoveResourceCommand($request, $xml);

				} break;
			case 'DELETERESOURCE':
				{
					lmb_require('wgs/lib/mapagent/wgsMgDeleteResourceCommand.class.php');
					return new wgsMgDeleteResourceCommand($request,$xml);
				} break;
			case 'COPYRESOURCE':
				{
					lmb_require('wgs/lib/mapagent/wgsMgCopyResourceCommand.class.php');
					return new wgsMgCopyResourceCommand($request,$xml);
				} break;
			default: return null;
		}
	}
	
}
?>