<?php

require_once('wgs/lib/PHPMailer/PHPMailerAutoload.php');

class wgsErrorHandler
{
	
	var $showErrors = false;
	var $errorStack;

	public function __construct() {
		error_reporting(E_ALL);
		set_error_handler(array(&$this, 'wgsErrorHandler'));
		set_exception_handler(array(&$this, 'wgsExceptionHandler'));
		register_shutdown_function("wgsFatalHandler");
	}

    function wgsExceptionHandler ($e) {

    	if($this->showErrors == true || "WGS_SEND_MAIL" == true) {
    		$separator = '#';
        	$this->errorStack = str_replace($separator,'<br />', $e->getTraceAsString());
    	}

		if($this->showErrors == true) {
    		$this->showErrors($e);
    	}

        if("WGS_SEND_MAIL" == true) {
        	$this->sendEmail($e);
        }       
	}

	function wgsErrorHandler ($no,$str,$file,$line) {
	    $e = new ErrorException($str,$no,0,$file,$line);
	    $this->wgsExceptionHandler($e);
	}

	function wgsFatalHandler() {
	  $errfile = "Unknown file";
	  $errstr  = "Shutdown";
	  $errno   = E_CORE_ERROR;
	  $errline = 0;

	  $error = error_get_last();

	  if( $error !== NULL) {
	    $errno   = $error["type"];
	    $errfile = $error["file"];
	    $errline = $error["line"];
	    $errstr  = $error["message"];

	    $e = new ErrorException($errstr,$errno,0,$errfile,$errline);
	    $this->wgsExceptionHandler($e);
	  }
	}

	private function showErrors($error) {
		
    	echo 'Error :' . $error->getMessage() . '<br />';
	    echo 'Error code :' . $error->getCode() . '<br />';
	    echo 'File :' . $error->getFile() . '<br />';
	    echo 'Line :' . $error->getLine() . '<br />';
	    echo 'Error stack :' . $this->errorStack . '<br />';
	    
	    echo 'Если ошибка повторяется, попробуйте <a href="/authorization/logout">Заново выполнить вход в систему</a>';
	}

	private function sendEmail($error) {

		$errorArray = array(E_ERROR, E_USER_ERROR, E_CORE_ERROR, E_RECOVERABLE_ERROR); 

		if (in_array($error->getCode(), $errorArray)) {

			$dateError = date("Y-m-d H:i:s (T)");

	        $errorMessage  = 'Error date: ' . $dateError . '<br />';
	        $errorMessage .= 'Error :' . $error->getMessage() . '<br />';
		    $errorMessage .= 'Error code :' . $error->getCode() . '<br />';
		    $errorMessage .= 'File :' . $error->getFile() . '<br />';
		    $errorMessage .= 'Line :' . $error->getLine() . '<br />';
		    $errorMessage .= 'Error stack :' . $this->errorStack . '<br />';

		    $mail = new PHPMailer();

			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->CharSet = 'utf-8';
			$mail->IsHTML(true);
			$mail->Host = "smtp.mail.ru:25"; // SMTP server
			$mail->SMTPAuth = true;
			$mail->Port = 465;
			$mail->Username = "supersmokin";
			$mail->Password = "";
			$mail->From = "supersmokin@mail.ru";
			$mail->FromName = "Иван Скляров";
			$mail->SMTPSecure = 'tls'; 
			$mail->AddAddress('superswifire@gmail.com');
			$mail->Subject = 'Error';
			$mail->Body = $errorMessage;

			$mail->Send();
			/*if(!$mail->Send()) {
				echo "Can't send the mail!";
			} else {
				echo 'All is done!';
			}*/

			$mail->ClearAddresses();
			$mail->ClearAttachments();
	        }
	}

	public function showErrorsOn() {
		$this->showErrors = true;
	}

	public function showErrorsOff() {
		$this->showErrors = false;
	}
}

?>