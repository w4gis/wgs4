<?php

class wgsZendAclRoleRegistry
{
    protected $_roles = array();

    /**
     * Adds a Role having an identifier unique to the registry
     *
     * The $parents parameter may be a reference to, or the string identifier for,
     * a Role existing in the registry, or $parents may be passed as an array of
     * these - mixing string identifiers and objects is ok - to indicate the Roles
     * from which the newly added Role will directly inherit.
     *
     * In order to resolve potential ambiguities with conflicting rules inherited
     * from different parents, the most recently added parent takes precedence over
     * parents that were previously added. In other words, the first parent added
     * will have the least priority, and the last parent added will have the
     * highest priority.
     *
     * @param  wgsZendAcl_Role_Interface              $role
     * @param  wgsZendAcl_Role_Interface|string|array $parents
     * @throws wgsZendAclRoleRegistry_Exception
     * @return wgsZendAclRoleRegistry Provides a fluent interface
     */

    public function add(wgsZendAclRoleInterface $role, $parents = null)
    {
        $roleId = $role->getRoleId();

        if ($this->has($roleId)) {
            throw new wgsZendAclRoleRegistryException("Role id '$roleId' already exists in the registry");
        }

        $roleParents = array();
        if (null !== $parents) {
            if (!is_array($parents)) {
                $parents = array($parents);
            }
            foreach ($parents as $parent) {
                try {
                    if ($parent instanceof wgsZendAclRoleInterface) {
                        $roleParentId = $parent->getRoleId();
                    } else {
                        $roleParentId = $parent;
                    }
                    $roleParent = $this->get($roleParentId);
                } catch (wgsZendAclRoleRegistryException $e) {
                    throw new wgsZendAclRoleRegistryException("Parent Role id '$roleParentId' does not exist");
                }

                $roleParents[$roleParentId] = $roleParent;
                $this->_roles[$roleParentId]['c'][$roleId] = $role;
            }
        }

        $this->_roles[$roleId] = array(
            'i' => $role,
            'p'  => $roleParents,
            'c' => array()
            );

        return $this;
    }

    /**
     * Returns the identified Role
     *
     * The $role parameter can either be a Role or a Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @throws wgsZendAclRoleRegistryException
     * @return wgsZendAclRoleInterface
     */
    public function get($role)
    {
        if ($role instanceof wgsZendAclRoleInterface) {
            $roleId = $role->getRoleId();
        } else {
            $roleId = (string) $role;
        }
        if (!$this->has($role)) {
            throw new wgsZendAclRoleRegistryException("Role '$roleId' not found");
        }
        return $this->_roles[$roleId]['i'];
    }

    /**
     * Returns true if and only if the Role exists in the registry
     *
     * The $role parameter can either be a Role or a Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @return boolean
     */
    public function has($role)
    {
        if ($role instanceof wgsZendAclRoleInterface) {
            $roleId = $role->getRoleId();
        } else {
            $roleId = (string) $role;
        }
        return isset($this->_roles[$roleId]);
    }

    /**
     * Returns an array of an existing Role's parents
     *
     * The array keys are the identifiers of the parent Roles, and the values are
     * the parent Role instances. The parent Roles are ordered in this array by
     * ascending priority. The highest priority parent Role, last in the array,
     * corresponds with the parent Role most recently added.
     *
     * If the Role does not have any parents, then an empty array is returned.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @uses   wgsZendAclRoleRegistry::get()
     * @return array
     */
    public function getParents($role)
    {
        $roleId = $this->get($role)->getRoleId();

        return $this->_roles[$roleId]['p'];
    }

    /**
     * Returns true if and only if $role inherits from $inherit
     *
     * Both parameters may be either a Role or a Role identifier. If
     * $onlyParents is true, then $role must inherit directly from
     * $inherit in order to return true. By default, this method looks
     * through the entire inheritance DAG to determine whether $role
     * inherits from $inherit through its ancestor Roles.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @param  wgsZendAclRoleInterface|string $inherit
     * @param  boolean                        $onlyParents
     * @throws wgsZendAclRoleRegistryException
     * @return boolean
     */
    public function inherits($role, $inherit, $onlyParents = false)
    {
        try {
            $roleId     = $this->get($role)->getRoleId();
            $inheritId = $this->get($inherit)->getRoleId();
        } catch (wgsZendAclRoleRegistryException $e) {
            throw $e;
        }

        $inherits = isset($this->_roles[$roleId]['p'][$inheritId]);

        if ($inherits || $onlyParents) {
            return $inherits;
        }

        foreach ($this->_roles[$roleId]['p'] as $parentId => $parent) {
            if ($this->inherits($parentId, $inheritId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Removes the Role from the registry
     *
     * The $role parameter can either be a Role or a Role identifier.
     *
     * @param  wgsZendAclRoleInterface|string $role
     * @throws wgsZendAclRoleRegistryException
     * @return wgsZendAclRoleRegistry Provides a fluent interface
     */
    public function remove($role)
    {
        try {
            $roleId = $this->get($role)->getRoleId();
        } catch (wgsZendAclRoleRegistryException $e) {
            throw $e;
        }

        foreach ($this->_roles[$roleId]['c'] as $childId => $child) {
            unset($this->_roles[$childId]['p'][$roleId]);
        }
        foreach ($this->_roles[$roleId]['p'] as $parentId => $parent) {
            unset($this->_roles[$parentId]['c'][$roleId]);
        }

        unset($this->_roles[$roleId]);

        return $this;
    }

    /**
     * Removes all Roles from the registry
     *
     * @return wgsZendAclRoleRegistry Provides a fluent interface
     */
    public function removeAll()
    {
        $this->_roles = array();

        return $this;
    }

} 

?>