<?php

lmb_require('limb/toolkit/src/lmbAbstractTools.class.php');

class wgsTools extends lmbAbstractTools
{
    protected $wgsTier;
    protected $wgsSysDbConnection;
    protected $wgsProjectDbConnection;
    protected $routes;
    protected $wgsACL;

    function getWgsTier()
    {
        lmb_require('wgs/model/tier/wgsTier.class.php');
        if(!is_object($this->wgsTier))
            $this->wgsTier = wgsTier :: instance();
        return $this->wgsTier;
    }

    function getWgsSysDbConnection()
    {
        lmb_require('limb/dbal/src/lmbDBAL.class.php');
        if(!is_object($this->wgsSysDbConnection))
            $this->wgsSysDbConnection = lmbDBAL :: newConnection(new lmbDbDSN(WGS3_USER_DSN));
        return $this->wgsSysDbConnection;
    }

    function getWgsProjectDbConnection()
    {
        lmb_require('limb/dbal/src/lmbDBAL.class.php');
        if(!is_object($this->wgsProjectDbConnection))
            $this->wgsProjectDbConnection = lmbDBAL :: newConnection(new lmbDbDSN(PROJECT_USER_DSN));
        return $this->wgsProjectDbConnection;
    }
    
    function getWgsRoutes()
    {
	    if(!$this->routes)
	    {
			lmb_require('wgs/request/wgsRoutes.class.php');
			$this->routes = new wgsRoutes();
	    }
	    return $this->routes;
    }

    function getWgsACL()
    {
        lmb_require('wgs/lib/wgsACL.class.php');
        if(!is_object($this->wgsACL))
            $this->wgsACL = new WgsACL();
        return $this->wgsACL;
    }
    
	function createController($controller_name, $requestParams = false, $requestNamedParams = false)
	{
		$class_name = lmb_camel_case($controller_name) . 'Controller';
		if (defined("WGS_CONTROLLERS_INCLUDE_PATH_EXT"))
		    $file = $this->toolkit->findFileByAlias("$class_name.class.php", WGS_CONTROLLERS_INCLUDE_PATH_EXT, 'controller');
		else
		    $file = $this->toolkit->findFileByAlias("$class_name.class.php", WGS_CONTROLLERS_INCLUDE_PATH, 'controller');
		lmb_require($file);
		
		$params = array();
		if ($requestParams)
		    $params['requestParams'] = $requestParams;
		if ($requestNamedParams)
		    $params['requestNamedParams'] = $requestNamedParams;          
		$result = new $class_name($params);
		return $result;
	}
}
?>