<?php

lmb_require('limb/toolkit/src/lmbAbstractTools.class.php');
lmb_require('wgs/model/tier/wgsMgTier.class.php');

class mgTools extends lmbAbstractTools
{
    protected $mgTier;

    function getMgTier()
    {
        if(!is_object($this->mgTier))
            $this->mgTier = wgsMgTier :: instance();
        return $this->mgTier;
    }
}
?>
