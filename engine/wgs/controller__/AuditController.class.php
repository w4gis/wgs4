<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsPrivilege.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/model/tier/wgsTier.class.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');
class AuditController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::PROJECT_ADMIN);
        $this->wgsTier->commitSession();
    }
  function doGetTableAudit()
    {
	$userId = $this->request->getPost('userId') == "" ? null : $this->request->getPost('userId');
	$controller = $this->request->getPost('controller_js') == "" ? null : $this->request->getPost('controller_js');
	$action = $this->request->getPost('action_js') == "" ? null : $this->request->getPost('action_js');
	$dateFrom = $this->request->getPost('dateFrom_js') == "" ? null : $this->request->getPost('dateFrom_js');
	$dateTo = $this->request->getPost('dateTo_js') == "" ? null : $this->request->getPost('dateTo_js');
	$timeFrom = $this->request->getPost('timeFrom_js') == "" ? "00:00" : $this->request->getPost('timeFrom_js');
	$timeTo = $this->request->getPost('timeTo_js') == "" ? "23:59" : $this->request->getPost('timeTo_js');
	$start = $this->request->getPost('start') == "" ? 0 : $this->request->getPost('start');
	$limit = $this->request->getPost('limit') == "" ? 36 : $this->request->getPost('limit');
	
	//$dateFrom.=" $timeFrom";
	//$dateTo.=" $timeTo";
	if($dateTo) $dateTo .= ' '.$timeTo;
	if($dateFrom) $dateFrom .= ' '.$timeFrom;
	
	$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLiEvent($userId, $controller, $action, $dateFrom, $dateTo, $start, $limit);//filter or display
	//lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLayersForAudit();
	//die();
	$total = $db_response['total'];
	$data = $db_response['recordset'];
	$Table = array();
	       foreach ($data as $value) {
            $TableUserName = lmb_win1251_to_utf8($value->get('DESCRIPTION')) ? (lmb_win1251_to_utf8($value->get('DESCRIPTION')).' ('.lmb_win1251_to_utf8($value->get('NAME')).')') : lmb_win1251_to_utf8($value->get('NAME'));
            $TableId = lmb_win1251_to_utf8($value->get('ID_LISTENING'));
			$TableControllerName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_CONTROLLER'));
			$TableParamName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_PARAM'));
			$TableDate = lmb_win1251_to_utf8($value->get('LI_DATE'));
			$TableHost = lmb_win1251_to_utf8($value->get('LI_HOST'));
			//echo lmb_win1251_to_utf8($value->get('LI_DATE'));
			$TableValueParam = lmb_win1251_to_utf8($value->get('VALUE_STRING'));
			$TableActionName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_ACTION'));
			
          

            $Table[] = array(
                                    'Id'          => $TableId,
                                    'User'            => $TableUserName,
                                    'Data'     => $TableDate,
                                    'Host'     => $TableHost,
                                    'Controller'      => $TableControllerName,
                                    'Action'  => $TableActionName,
                                    'Parametr'         => $TableParamName,
									'ZParametr'         => $TableValueParam
                );
			
    }
	
echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table,
			'total' => $total
    	));//write to table
	
	//$this->response->write(json_encode(array_values($Table)));
    
	//echo '1';
    }
/*
function doGetFilterUser(){
	
}
function doGetFilterController(){
	
}
function doGetFilterAction(){
	
}
*/
function doGetLiParams(){//get all actions for filter
$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLiParams();
	if($_GET['all'])
	$Table[] = array(
                                    'ID_PARAM'          => 0,
                                    'NAME_IN_GUI_PARAM'            => lmb_win1251_to_utf8('��� ���������'),
                                    'NAME_PARAM'     => null,
                                    'DESCRIPTION_PARAM'      => null,
									'INCLUDED_PARAM'      => null,
									'ID_ACTION'          => 0,
                                    'NAME_IN_GUI_ACTION'            => lmb_win1251_to_utf8('��� ��������'),
                                    'NAME_ACTION'     => null,
                                    'DESCRIPTION_ACTION'      => null,
									'INCLUDED_ACTION'      => null,
									
                );
	foreach ($db_response as $value) {
            $TableIdParam = lmb_win1251_to_utf8($value->get('ID_PARAM'));
            $TableNameInGuiParam = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_PARAM')) ? lmb_win1251_to_utf8($value->get('NAME_IN_GUI_PARAM')) : $TableNameAction = lmb_win1251_to_utf8($value->get('NAME_PARAM'));
			
			$TableNameParam = lmb_win1251_to_utf8($value->get('NAME_PARAM'));
			$TableDescriptionParam = lmb_win1251_to_utf8($value->get('DESCRIPTION_PARAM'));
            $TableDescriptionIncParam = lmb_win1251_to_utf8($value->get('INCLUDED_PARAM'));
			
			$TableIdAction = lmb_win1251_to_utf8($value->get('ID_ACTION'));
            $TableNameInGuiAction = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_ACTION'));
			$TableNameAction = lmb_win1251_to_utf8($value->get('NAME_ACTION'));
			$TableDescriptionAction = lmb_win1251_to_utf8($value->get('DESCRIPTION_ACTION'));
            $TableDescriptionInc = lmb_win1251_to_utf8($value->get('INCLUDED_ACTION'));
            
            $Table[] = array(
                                    'ID_PARAM'          => $TableIdParam,
                                    'NAME_IN_GUI_PARAM'            => $TableNameInGuiParam,
                                    'NAME_PARAM'     => $TableNameParam,
                                    'DESCRIPTION_PARAM'      => $TableDescriptionParam,
									'INCLUDED_PARAM'      => ($TableDescriptionIncParam > 0) ? true : false,
									 'ID_ACTION'          => $TableIdAction,
                                    'NAME_IN_GUI_ACTION'            => $TableNameInGuiAction,
                                    'NAME_ACTION'     => $TableNameAction,
                                    'DESCRIPTION_ACTION'      => $TableDescriptionAction,
									'INCLUDED_ACTION'      => $TableDescriptionInc,
									
                );
        
    }

	echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));
}

function doGetLiActions(){//get all actions for filter
$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLiActions();
	if($_GET['all'])
	$Table[] = array(
                                    'ID_ACTION'          => 0,
                                    'NAME_IN_GUI_ACTION'            => lmb_win1251_to_utf8('��� ��������'),
                                    'NAME_ACTION'     => null,
                                    'DESCRIPTION_ACTION'      => null,
									'INCLUDED_ACTION'      => null,
									'ID_CONTROLLER'          => 0,
                                    'NAME_IN_GUI_CONTROLLER'            => lmb_win1251_to_utf8('��� ������'),
                                    'NAME_CONTROLLER'     => null,
                                    'DESCRIPTION_CONTROLLER'      => null,
									'INCLUDED_CONTROLLER' => null
                );
	foreach ($db_response as $value) {
            $TableIdAction = lmb_win1251_to_utf8($value->get('ID_ACTION'));
            $TableNameInGuiAction = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_ACTION'));
			$TableNameAction = lmb_win1251_to_utf8($value->get('NAME_ACTION'));
			$TableDescriptionAction = lmb_win1251_to_utf8($value->get('DESCRIPTION_ACTION'));
            $TableDescriptionInc = (lmb_win1251_to_utf8($value->get('INCLUDED_ACTION'))>0);
            $TableIdController = lmb_win1251_to_utf8($value->get('ID_CONTROLLER'));
            $TableNameInGuiController = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_CONTROLLER'));
			$TableNameController = lmb_win1251_to_utf8($value->get('NAME_CONTROLLER'));
			$TableDescriptionController = lmb_win1251_to_utf8($value->get('DESCRIPTION_CONTROLLER'));
			$TableIncludedController = lmb_win1251_to_utf8($value->get('INCLUDED_CONTROLLER'))>0;
			$Table[] = array(
                                    'ID_ACTION'          => $TableIdAction,
                                    'NAME_IN_GUI_ACTION'            => $TableNameInGuiAction,
                                    'NAME_ACTION'     => $TableNameAction,
                                    'DESCRIPTION_ACTION'      => $TableDescriptionAction,
									'INCLUDED_ACTION'      => $TableDescriptionInc,
									'ID_CONTROLLER'          => $TableIdController,
                                    'NAME_IN_GUI_CONTROLLER'            => $TableNameInGuiController,
                                    'NAME_CONTROLLER'     => $TableNameController,
                                    'DESCRIPTION_CONTROLLER'      => $TableDescriptionController,
									'INCLUDED_CONTROLLER' => $TableIncludedController
                );
        
    }

	echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));
}
function doGetLiControllers(){//get all controllers for filter
$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLiControllers();
	if($_GET['all'])
	$Table[] = array(
                                    'ID_CONTROLLER'          => 0,
                                    'NAME_IN_GUI_CONTROLLER'            => lmb_win1251_to_utf8('��� ������'),
                                    'NAME_CONTROLLER'     => null,
                                    'DESCRIPTION_CONTROLLER'      => null,
									'INCLUDED_CONTROLLER' => null
                );
	foreach ($db_response as $value) {
            $TableIdController = lmb_win1251_to_utf8($value->get('ID_CONTROLLER'));
            $TableNameInGuiController = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_CONTROLLER'));
			$TableNameController = lmb_win1251_to_utf8($value->get('NAME_CONTROLLER'));
			$TableDescriptionController = lmb_win1251_to_utf8($value->get('DESCRIPTION_CONTROLLER'));
			$TableIncludedController = lmb_win1251_to_utf8($value->get('INCLUDED_CONTROLLER'))>0;

			
          

            $Table[] = array(
                                    'ID_CONTROLLER'          => $TableIdController,
                                    'NAME_IN_GUI_CONTROLLER'            => $TableNameInGuiController,
                                    'NAME_CONTROLLER'     => $TableNameController,
                                    'DESCRIPTION_CONTROLLER'      => $TableDescriptionController,
									'INCLUDED_CONTROLLER' => $TableIncludedController
                );
        
    }

	echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));
}
    function doDisplay()//create menu,form and etc
    {
        $topmenu = $this->createTopMenu();
        $this->setTemplate('main.html');
        $this->passToView("title", "WGS3 :: �����");
        $this->passToView("body", $topmenu);
        $this->startFrontEndApplication(array('/js/wgs/audit.js'), 'Audit');
    }
	 
	function doUpdateSettings()//get all users for filter
    {//ini_set('display_errors',1);
//error_reporting(E_ALL);

		//$f = fopen('d:\gis\wtf','w');
			$data = CJSON::decode($this->request->getPost('data'));
		foreach ($data['params'] as $key => $value){
			$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->setLiParamActive($value['NAME_PARAM'],(($value['INCLUDED_PARAM']) ? '1' : '0'));
			//echo $value['INCLUDED_PARAM'];
		}
		foreach ($data['actions'] as $key => $value){
			$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->setLiActionActive($value['NAME_ACTION'],(($value['INCLUDED_ACTION']) ? '1' : '0'), $value['ID_CONTROLLER']); 
			//echo $value['NAME_ACTION']. ', '.(($value['INCLUDED_ACTION']) ? '1' : '0'). ', ' . $value['ID_CONTROLLER']. ' '. $db_response.'); '; 
		}
		foreach ($data['controllers'] as $key => $value){
			$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->setLiControllerActive($value['NAME_CONTROLLER'],(($value['INCLUDED_CONTROLLER']) ? '1' : '0')); 
		}
		foreach ($data['layers'] as $key => $value){
			$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->setFLayersActive($value['fl_id'],(($value['fl_is_active']) ? '1' : '0')); 
		}
		echo 'ok';
	}
	 
    function doGetUserList()//get all users for filter
    {
        $users = lmbToolkit::instance()->wgsTier->getProjectUserService()->getList();

        $userList = array();
		$Table[] = array('ID_USER' => null,
                         'NAME_IN_GUI_USER'  => lmb_win1251_to_utf8('��� ������������'),
                         'NAME_USER'     => null,
                         'USER_ROLE' => null);
		  foreach ($users as $user) {
            $Table[] = array(
                'ID_USER'          => lmb_win1251_to_utf8($user->getId()),
                'NAME_IN_GUI_USER'  => (lmb_win1251_to_utf8($user->getDescription()) ? lmb_win1251_to_utf8($user->getDescription()) : lmb_win1251_to_utf8($user->getName())),
                'NAME_USER'     => lmb_win1251_to_utf8($user->getName()),
                'USER_ROLE'      => lmb_win1251_to_utf8($user->getParentRoles()),
            );
        
    }
		
		
		/*
        foreach($users as $user)
        {
            $userList[$user->getId()]['description'] = lmb_win1251_to_utf8($user->getDescription());
            $userList[$user->getId()]['userName'] = lmb_win1251_to_utf8($user->getName());
            $parentRoles = $user->getParentRoles();
            $sParentRoles = '';
            if(count($parentRoles) > 0)
            {
                $k = 0;
                foreach ($parentRoles as $parentRole)
                {
                    if($k == count($parentRoles)-1)
                    $sParentRoles.=lmb_win1251_to_utf8($parentRole->getName());
                    else
                    $sParentRoles.=lmb_win1251_to_utf8($parentRole->getName()).', ';
                    $k++;
                }
            }
            $userList[$user->getId()]['assignedRoles'] = $sParentRoles;
            $userList[$user->getId()]['userId'] = $user->getId();
        }*/
			echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));
        
    }
	function doFilter(){//function for filter
		$a1=$_POST;
		//$a1=CJSON::decode($a1);
		
	$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getLiEvent($_POST['user_js'], $_POST['controller_js'], $_POST['action_js'], $_POST['datebegin_js'], $_POST['dateend_js']);//filter or display
	
	$Table = array();
	       foreach ($db_response as $value) {
            $TableUserName = lmb_win1251_to_utf8($value->get('NAME'));
            $TableId = lmb_win1251_to_utf8($value->get('ID_LISTENING'));
			$TableControllerName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_CONTROLLER'));
			$TableParamName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_PARAM'));
			$TableDate = lmb_win1251_to_utf8($value->get('LI_DATE'));
			//echo lmb_win1251_to_utf8($value->get('LI_DATE'));
			$TableValueParam = lmb_win1251_to_utf8($value->get('VALUE_PARAM'));
			$TableActionName = lmb_win1251_to_utf8($value->get('NAME_IN_GUI_ACTION'));
			
          

            $Table[] = array(
                                    'Id'          => $TableId,
                                    'User'            => $TableUserName,
                                    'Data'     => $TableDate,
                                    'Controller'      => $TableControllerName,
                                    'Action'  => $TableActionName,
                                    'Parametr'         => $TableParamName,
									'ZParametr'         => $TableValueParam
                );
        
    }
	
echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));//write to table
	
	}
	
	function doGetAuditActivity(){
		$db_response = lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getAuditActivity();
		echo ($db_response > 1 ? '2' : '1');
	}
	
	function doSetAuditActivity(){
		$isActive = $this->request->getPost('is_active');
		$reason = $this->request->getPost('reason');
		$id = $this->wgsTier->getAuthorizedUser()->getId();
		if($id)
			return lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->setAuditActivity($isActive !='false' ? 2 : 1, $id, $_SERVER['REMOTE_ADDR'], $reason);		
	}
	
	function doGetFeatureLayersList(){
		$db_response=lmbToolkit::instance()->wgsTier->getProjectTier()->getAuditService()->getActiveFeatureLayersList();//filter or display
	       
		$Table = array();
	       foreach ($db_response as $value) {
			//var_dump($value);
    
		    $TableFlId = $value->get('featurelayer_id');
            $TableFlName = lmb_win1251_to_utf8($value->get('lname'));
			$TableFlIsActive = ($value->get('li_featurelayer_isactive') > 0) ? true : false;
			$TableFlLiId = $value->get('li_featurelayer_id');
			$TableFlGrId = $value->get('featurelayergroup_id');
			$TableFlGrName = lmb_win1251_to_utf8($value->get('gname'));
			
            $Table[] = array(
                                    'fl_id' 		=> $TableFlId,
                                    'fl_name'       => $TableFlName,
                                    'fl_is_active'  => $TableFlIsActive,
                                    'fl_li_id'      => $TableFlLiId,
									'fl_gr_id'      => $TableFlGrId,
									'fl_gr_name'      => $TableFlGrName 									
            );
        
    }
	
		echo CJSON::encode(array(
    		'success' => true,
    		'record' => $Table
    	));//write to table
	
	}
	

}

?>