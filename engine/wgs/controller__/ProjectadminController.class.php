﻿<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsPrivilege.class.php');

class ProjectAdminController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::PROJECT_ADMIN);
        $this->wgsTier->commitSession();
    }

    function doDisplay()
    {       
        $topmenu = $this->createTopMenu();
        $this->setTemplate('main_pa.html');
        $this->passToView("title", ":: WGS3 :: Управление проектом ::");
        $this->passToView("body", $topmenu);
        $this->startFrontEndApplication(array(
            '/js/wgs/projectadmin/projectobjectadmin.js',
        	'/js/wgs/projectadmin/projectroleadmin.js',
        	'/js/wgs/projectadmin/projectuseradmin.js',
        	'/js/wgs/projectadmin.js'), 'Projectadmin');
    }
    
    function doRestart() {
        $shell = new COM("WScript.Shell");
        $shell->run(WGS3_PROJECT_DIR . "/tusur_production/solve-problems.bat", 0, false);
    }
}

?>