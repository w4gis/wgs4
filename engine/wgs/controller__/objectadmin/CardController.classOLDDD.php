<?php
/*ini_set('display_errors',1);
error_reporting(E_ALL);*/
lmb_require('wgs/controller/ObjectAdminController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
require_once('wgs/lib/fpdf/textbox.php');

class CardController extends ObjectAdminController
{
    function doDisplay()
    {
    }

    /////////////////////////Функции множественного заполнения
    function doGetCards($featureIds)
    {
        //$objectIds = $this->request->getPost('objectIds');
        $objectsArray = array();
        if ($this->request->getPost('featureIds'))
        {
            //$featureIds = json_decode($this->request->getPost('featureIds'));
            $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
            $objectsArray = array();
            foreach ($objectList as $object)
            {
                $objectsArray[] = array (
                    'featureLayerId'    => $object->get('featurelayer_id'),
                    'objectId'  => $object->get('object_id'),
                    'objectTypeId'  => $object->get('object_type_id')
                    );
            }
        }
        else
        {

            if ($this->request->getPost('objectIds'))
            {
                $objectIds = $this->request->getPost('objectIds');
                $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds, 1);
                foreach ($objectTypesList as $objectType)
                {
                   $objectsArray[] = array(
                    'objectTypeId' => $objectType->get('object_type_id'),
                    'objectId'  => $objectType->get('object_id')
                    );
               }
           }
       }
        //$this->response->write(json_encode(array_values($objectsArray)));
       return $objectsArray;

   }

   function doGetObjectTypes($featureIds)
   {
    if($this->request->getPost('featureIds')) {
        // if($featureIds) {
            // $featureIds =json_decode($this->request->getPost('featureIds'));
        $objectList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getAllObjectByFeatures($featureIds);
        $objectsArray = array();
        foreach ($objectList as $object) {
            $objectsArray[] = array (
                'featureLayerId'    => $object->get('featurelayer_id'),
                'featureLayerName'  => lmb_win1251_to_utf8($object->get('featurelayer_name')),
                'objectTypeId'  => $object->get('object_type_id'),
                'objectTypeName'  => lmb_win1251_to_utf8($object->get('object_type_name')),
                'objectId'  => $object->get('object_id')
                );
        }
        $layers = array();
        $uniqLayers = array();
        for ($i = 0; $i < count($objectsArray); $i++) {
            $layers[] = $objectsArray[$i]['featureLayerId'];
        }
        $uniqLayers = array_unique($layers);

        $layersWithTypes = array();

        foreach ($uniqLayers as $uniqLayer) {
            $featuresCount = 0;
            foreach ($objectsArray as $object) {
                if($object['featureLayerId'] == $uniqLayer) {
                    $layerLabel = $object['featureLayerName'];
                    $featuresCount++;
                }
            }
            $layersWithTypes[] = array(
                'name'              => $layerLabel.' ('.'карточек'.' : '.$featuresCount.' )',
                'label'             => $layerLabel.' ('.'карточек'.' : '.$featuresCount.' )',
                'featureLayerId'    => -1,
                'objectTypeId'      => -1
                );
            $featureTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($uniqLayer);
            foreach ($featureTypesList as $featureType) {
                if($featureType->get('is_current')) {
                    $layersWithTypes[] = array(
                        'name'              => '<div style="padding-left:15px; font-size:8pt;">'.lmb_win1251_to_utf8($featureType->get('Name')).' (основное)</div>',
                        'label'             => lmb_win1251_to_utf8($featureType->get('Name')).' (основное)',
                        'featureLayerId'    => $uniqLayer,
                        'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                } else {
                    $layersWithTypes[] = array(
                        'name'              => '<div style="padding-left:15px; font-size:8pt;">'.lmb_win1251_to_utf8($featureType->get('Name')).'</div>',
                        'label'             => lmb_win1251_to_utf8($featureType->get('Name')),
                        'featureLayerId'    => $uniqLayer,
                        'objectTypeId'      => $featureType->get('Object_Type_id')
                        );
                }
            }
        }
    } else {
        $layersWithTypes = array();
        if($this->request->getPost('objectIds')) {
            $objectIds = $this->request->getPost('objectIds');
            $objectTypesList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getObjectTypesByObjects($objectIds,0);
            foreach ($objectTypesList as $objectType) {
               $layersWithTypes[] = array(
                'label'=> lmb_win1251_to_utf8($objectType->get('type_name')),
                'name' => lmb_win1251_to_utf8($objectType->get('type_name')),
                'objectTypeId' => $objectType->get('object_type_id')
                );
           }
       }
   }

   return $layersWithTypes;
        // $this->response->write(json_encode(array_values($layersWithTypes)));
}


function doGetPropertiesByObjectType($objectTypeId)
{
        //$objectTypeId = $this->request->getPost('objectTypeId');
    if ($objectTypeId) {
        $properties = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($objectTypeId, null);
        $propertyList = array();

        foreach($properties as $property)
        {
            $propertyList[] = array(
                'id'            => $property->get('Property_Id'),
                'name'          => lmb_win1251_to_utf8($property->get('Name')),
                'valueTypeId'   => $property->get('Value_properties_type_id'),
                'groupId'       => $property->get('Properties_group_id'),
                'groupName'     => lmb_win1251_to_utf8($this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetGroupProperty($property->get('Properties_group_id')))
                );
        }
            //$this->response->write(json_encode(array_values($propertyList)));
        return $propertyList;
    }
}


function doGetBaseValueProperties ($objectIds) 
{
        //$objectIds = $this->request->getPost('objectIds');
    $names = array();
    $shortNames = array();
    $notes = array();
    foreach ($objectIds as $objectId) {
        $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
        foreach ($baseProperties as $baseProperty){
            $names[] = lmb_win1251_to_utf8($baseProperty->get('name'));
            $shortNames[] = lmb_win1251_to_utf8($baseProperty->get('short_name'));
            $notes[] = lmb_win1251_to_utf8($baseProperty->get('note'));
        }
    }
    $name = ((count(array_unique($names))==1))? $names[0]: '<...>';
    $shortName = ((count(array_unique($shortNames))==1))? $shortNames[0]: '<...>';
    $note = ((count(array_unique($notes))==1))?$notes[0]: '<...>';


    $values[0] = array(
        'name' => $name,
        'shortName' => $shortName,
        'note' => $note
        );
    return $values;
        // $this->response->write(json_encode(array_values($values)));
}


function doGetValuePropertiesMultiple($objectIds)
    {   //var_dump('test');
        // $objectIds = $this->request->getPost('objectIds');
    $propertyList = array();
    if ($objectIds) {
        $properties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValuePropertiesByObjects($objectIds);
        foreach ($properties as $property) {
            $propertyList[] = array (
                'propertyId' => $property->get('property_id'),
                'propertyValue' => lmb_win1251_to_utf8($property->get('property_value'))
                );
        }
            // $this->response->write(json_encode(array_values($propertyList)));
        $valuePropertyList = $propertyList; 
            //var_dump($valuePropertyList);
        return $valuePropertyList;
    }
}

    ////////////////////////Конец множественного заполнения////////////////////////////////////////////////////////////////////////////////////////
function doGetCardsByFeatureId()
{
   $WgsVersion =json_decode($this->request->getPost('WgsVersion'));
     //$featureIds = $this->request->getPost('featureIds');
        //var_dump(json_decode($featureIds));
   $featureIds=json_decode($this->request->getPost('featureIds'));
   if(isset($WgsVersion)){
    if(($WgsVersion >= 4)&&(count($featureIds, COUNT_RECURSIVE)==1))
    {
        $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($featureIds[0]);
        $user = $this->wgsTier->getAuthorizedUser();
        $User = $user->getId();
        $Host = $_SERVER['REMOTE_ADDR'];        

        $i = 1;
        foreach ($cardList as $value)
        {
            if($value->get('Name') != '')
            {

                $cardName = lmb_win1251_to_utf8($value->get('Name')).' - '.lmb_win1251_to_utf8($value->get('object_type_name'));
                $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','card','view_card');
                if($liId){
                 $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_view',lmb_utf8_to_win1251($cardName));
                       //$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'featurelayer',lmb_utf8_to_win1251($on));           
             }
         }else
         $cardName = 'Без наименования - '.lmb_win1251_to_utf8($value->get('object_type_name'));
         $objectId = $value->get('Object_id');
                    $cardsArray[] = array(//<li style="list-style:square;">
                        'cardName' => $cardName,
                        'cardId' => $objectId,
                        'cardIsCurrent' => $value->get('is_current'),
                        'cardTypeName'  => lmb_win1251_to_utf8($value->get('object_type_name')),
                        'cardRelationName' => '',
                        'cardLabel' => $cardName
                        );
                    $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
                    foreach ($childObjects as $childObject) {
                        if($childObject->get('name') != '')
                            $cardName = lmb_win1251_to_utf8($childObject->get('name')).' - '.lmb_win1251_to_utf8($childObject->get('type_name').' (Связь: ').lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                        else
                            $cardName = 'Без наименования - '.lmb_win1251_to_utf8($childObject->get('type_name')).' (Связь: '.lmb_win1251_to_utf8($childObject->get('relation_name')).')';

                        $cardsArray[] = array(
                            'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                            'cardId'            => $childObject->get('object_id'),
                            'cardIsCurrent'     => 0,
                            'cardTypeName'      => lmb_win1251_to_utf8($childObject->get('type_name')),
                            'cardRelationName'  => lmb_win1251_to_utf8($childObject->get('relation_name')),
                            'cardLabel'         => $cardName
                            );
                    }
                }
                $this->response->write(json_encode(array_values($cardsArray)));
            }
            elseif(($WgsVersion >= 4)&&(count($featureIds, COUNT_RECURSIVE) > 1)){
        //Получаем список карточек(doGetCards($featureIds))
                $objectsArray = array();
                $objectsArray = $this -> doGetCards($featureIds);

        //Получаем названия карточек(doGetObjectTypes($featureIds)) 
                $layersWithTypes = array();
                $layersWithTypes = $this -> doGetObjectTypes($featureIds);

        //Получаем группы и имена полей(doGetPropertiesByObjectType($objectTypeId))
                $propertyList = array();
                $temporaryObjectTypeId = array();
                $objectTypeId = array();

                foreach ($objectsArray as $value) {
                    array_push($temporaryObjectTypeId,$value['objectTypeId']);
                }

            //var_dump($temporaryObjectTypeId);
                for($i = 0;$i<count($temporaryObjectTypeId);$i++){
                    if($i==0){$objectTypeId[0]=$temporaryObjectTypeId[0];}
                    $flag=0;
                    foreach ($objectTypeId as $value) {
                        if($temporaryObjectTypeId[$i]==$value){$flag=1;}
                    }
                    if($flag==0){array_push($objectTypeId,$temporaryObjectTypeId[$i]);}
                }
                asort($objectTypeId);
            //var_dump($objectTypeId);
                for($i = 0;$i<count($objectTypeId);$i++){
                    $propertyList[$i] = $this -> doGetPropertiesByObjectType($objectTypeId[$i]);
                }


        //Получаем значения основных характеристик и остальных групп характеристик (doGetBaseValueProperties ($objectIds)),doGetValueProperties($objectIds)

                $values = array();
                $valuePropertyList = array();
                $j=0;
                foreach ($objectTypeId as $value) {
                    $objectIds = array();
                    for($i = 0;$i<(count($objectsArray));$i++){                    
                        if($value == $objectsArray[$i]['objectTypeId']){
                            $objectIds[$i] = $objectsArray[$i]['objectId'];                            
                        } 
                    }
                    $values[$j] = $this -> doGetBaseValueProperties($objectIds);
                    $valuePropertyList[$j] = $this -> doGetValuePropertiesMultiple($objectIds);
                    unset($objectIds);
                    $j++;                          
                }

        //Вывод результата множественного заполнения            
                $arrayNameGroupPropetyCard =array(
                    1 => $layersWithTypes, 
                    2 => $values,
                    3 => $propertyList,
                    4 => $valuePropertyList
                    );

             //var_dump($arrayNameGroupPropetyCard);

                $this->response->write(json_encode(array_values($arrayNameGroupPropetyCard)));
            // var_dump($layersWithTypes);

            }
        }
        else{
            $cardList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getListObjectByFeature($this->request->getPost('featureId'));
            $user = $this->wgsTier->getAuthorizedUser();
            $User = $user->getId();
            $Host = $_SERVER['REMOTE_ADDR'];        

            $i = 1;
            foreach ($cardList as $value) {
                if($value->get('Name') != ''){

                    $cardName = lmb_win1251_to_utf8($value->get('Name')).' - '.lmb_win1251_to_utf8($value->get('object_type_name'));
                    $liId = $this->wgsTier->getProjectTier()->getAuditService()->addLiEvent($User,$Host,'','card','view_card');
                    if($liId){
                        $this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'card_name_view',lmb_utf8_to_win1251($cardName));
                        //$this->wgsTier->getProjectTier()->getAuditService()->addLiEventValue($liId,'featurelayer',lmb_utf8_to_win1251($on));          
                    }
                }else
                $cardName = 'Без наименования - '.lmb_win1251_to_utf8($value->get('object_type_name'));
                $objectId = $value->get('Object_id');
                    $cardsArray[] = array(//<li style="list-style:square;">
                        'cardName' => $cardName,
                        'cardId' => $objectId,
                        'cardIsCurrent' => $value->get('is_current'),
                        'cardTypeName'  => lmb_win1251_to_utf8($value->get('object_type_name')),
                        'cardRelationName' => '',
                        'cardLabel' => $cardName
                        );
                    $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
                    foreach ($childObjects as $childObject) {
                        if($childObject->get('name') != '')
                            $cardName = lmb_win1251_to_utf8($childObject->get('name')).' - '.lmb_win1251_to_utf8($childObject->get('type_name')).' (Связь: '.lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                        else
                            $cardName = 'Без наименования - '.lmb_win1251_to_utf8($childObject->get('type_name')).' (Связь: '.lmb_win1251_to_utf8($childObject->get('relation_name')).')';

                        $cardsArray[] = array(
                            'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                            'cardId'            => $childObject->get('object_id'),
                            'cardIsCurrent'     => 0,
                            'cardTypeName'      => lmb_win1251_to_utf8($childObject->get('type_name')),
                            'cardRelationName'  => lmb_win1251_to_utf8($childObject->get('relation_name')),
                            'cardLabel'         => $cardName
                            );
                    }
                }
                //var_dump($cardsArray);
                $this->response->write(json_encode(array_values($cardsArray)));
            }    
        }

        function doGetValueBaseProperties()
        {
            $valueArray = array();
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($this->request->getPost('objectId'));
            foreach ($baseProperties as $baseProperty){

                $valueArray[] = array(
                    'id'            => $baseProperty->get('object_id'),
                    'name'          => encode($baseProperty->get('name')),
                    'shortName'     => encode($baseProperty->get('short_name')),
                    'typeName'      => encode($baseProperty->get('type_name')),
                    'createDate'    => $baseProperty->get('create_date'),
                    'edit_user'     => encode($baseProperty->get('edit_user')),
                    'note'          => encode($baseProperty->get('note'))
                    );
            }
       // var_dump($valueArray);

		//echo $baseProperty->get('create_date');
		//die();
            $this->response->write(json_encode(array_values($valueArray)));
        }

        function doGetCardsByObjectId()
        {
            $objectId = $this->request->getPost('objectId');
            $childObjects = array();
            if(isset($objectId)) {
                $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
                foreach ($baseProperties as $baseProperty){
                    if($baseProperty->get('name') != '')
                        $cardName = lmb_win1251_to_utf8($baseProperty->get('name')).' - '.lmb_win1251_to_utf8($baseProperty->get('type_name'));
                    else
                        $cardName = 'Без наименования - '.lmb_win1251_to_utf8($baseProperty->get('type_name'));

                    $valueArray[] = array(
                        'cardName'          => $cardName,
                        'cardId'            => $baseProperty->get('object_id'),
                        'cardIsCurrent'     => 0,
                        'cardTypeName'      => lmb_win1251_to_utf8($baseProperty->get('name')),
                        'cardRelationName'  => '',
                        'cardLabel'         => $cardName
                        );
                }
                $childObjects = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($objectId);
                foreach ($childObjects as $childObject) {
                    if($childObject->get('name') != '')
                        $cardName = lmb_win1251_to_utf8($childObject->get('name')).' - '.lmb_win1251_to_utf8($childObject->get('type_name')).' (Связь: '.lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                    else
                        $cardName = 'Без наименования - '.lmb_win1251_to_utf8($childObject->get('type_name')).' (Связь: '.lmb_win1251_to_utf8($childObject->get('relation_name')).')';
                    $valueArray[] = array(
                        'cardName'          => '<div class="relation-card">'.$cardName.'</div>',
                        'cardId'            => $childObject->get('object_id'),
                        'cardIsCurrent'     => 0,
                        'cardTypeName'      => lmb_win1251_to_utf8($childObject->get('type_name')),
                        'cardRelationName'  => lmb_win1251_to_utf8($childObject->get('relation_name')),
                        'cardLabel'         => $cardName
                        );
                }
                $this->response->write(json_encode(array_values($valueArray)));
            }
        }

        function doGetEntityIdByObject()
        {
            $objectId = $this->request->getPost('id');
            if (isset($objectId)) {
                $entityId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getEntityIdByObject($objectId);
                echo $entityId;
            }
        }

        function doGetEntityIdByFeature()
        {
            $featureId = $this->request->getPost('id');
            if (isset($featureId)) {
                $entityId = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getEntityIdByFeature($featureId);
                echo $entityId;
            }
        }

        function doPrint()
        {
            $objectId = $this->request->getPost('objectId');
            if($objectId) {
                $seq = rand();
                $pdf = new PDF();
                $pdf->Open();
                $pdf->SetMargins(15, 10, 15);
                $pdf->AddFont('Times','','times.php');
                $pdf->AddFont('TimesB','','timesbd.php');
                $this->previewCard($objectId, $pdf);
                $pdf->output(WGS3_PRINTCACHE_DIR."card-$seq.pdf");
                echo $seq;
            }
        }

        function previewCard($objectId, $pdf)
        {
            $pdf->addPage();
            $this->addBaseProperties($objectId, $pdf);
            $this->addAdvancedProperties($objectId, $pdf);
            $this->addChildCards($objectId, $pdf);
        }

        function doCreateFont()
        {
            lmb_require(FPDF_FONTPATH. "makefont/makefont.php");
            MakeFont('D:/fonts/verdana.ttf','D:/fonts/verdana.afm','cp1251');
            MakeFont('D:/fonts/verdanab.ttf','D:/fonts/verdanab.afm','cp1251');
            MakeFont('D:/fonts/verdanai.ttf','D:/fonts/verdanai.afm','cp1251');
            MakeFont('D:/fonts/verdanaz.ttf','D:/fonts/verdanaz.afm','cp1251');
        }

        function addHeader($pdf, $title)
        {
            $pdf-> SetFont('TimesB','',12);
        //$pdf->Cell(0, 15, $title, 1, 1, 'C');
            $pdf->MultiCell(0, 10, $title, 0, 'C' ,0);
        }

        function addBaseProperties($objectId, $pdf)
        {
            $baseProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject($objectId);
            foreach ($baseProperties as $baseProperty){
                $valueArray[] = array(
                    'id'            => $baseProperty->get('object_id'),
                    'name'          => $baseProperty->get('name'),
                    'shortName'     => $baseProperty->get('short_name'),
                    'typeName'      => $baseProperty->get('type_name'),
                    'createDate'    => $baseProperty->get('create_date'),
                    'note'          => $baseProperty->get('note')
                    );
            }

            if(isset($valueArray[0]['name'])) {
                $title = $valueArray[0]['name'].' - '.$valueArray[0]['typeName'];
            } else {
                $title = 'ГЃГҐГ§ Г­Г ГЁГ¬ГҐГ­Г®ГўГ Г­ГЁГї - '.$valueArray[0]['typeName'];
            }
            $this->addHeader($pdf, $title);

            $pdf->SetFillColor(230);
            $pdf->SetFont('TimesB','',11);
            $pdf->Cell(0, 7, ' ГЋГ±Г­Г®ГўГ­Г»ГҐ ГµГ Г°Г ГЄГІГҐГ°ГЁГ±ГІГЁГЄГЁ', 1, 1, 'L', 1);

            $pdf->SetFillColor(250);
            $pdf->SetFont('Times','',10);

            $this->createRow($pdf, ' ГЌГ ГЁГ¬ГҐГ­Г®ГўГ Г­ГЁГҐ', ' '.$valueArray[0]['name']);
            $this->createRow($pdf, ' ГЉГ°Г ГІГЄГ®ГҐ Г­Г ГЁГ¬ГҐГ­Г®ГўГ Г­ГЁГҐ', ' '.$valueArray[0]['shortName']);
            $this->createRow($pdf, ' ГЂГІГ°ГЁГЎГіГІГЁГўГ­Г®ГҐ Г®ГЇГЁГ±Г Г­ГЁГҐ', ' '.$valueArray[0]['typeName']);
            $this->createRow($pdf, ' Г„Г ГІГ  Г±Г®Г§Г¤Г Г­ГЁГї', ' '.$valueArray[0]['createDate']);
            $this->createRow($pdf, ' ГЏГ°ГЁГ¬ГҐГ·Г Г­ГЁГҐ', ' '.$valueArray[0]['note']);

            $pdf->Cell(0, 5, '', 0, 1, 'L', 0);
        }

        function addAdvancedProperties($objectId, $pdf)
        {
            $advancedProperties = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValuePropertiesByObject($objectId);
            if ($advancedProperties) {
                $propertiesArray = array();
                $groupArray = array();
                $groupArrayUniq = array();

                foreach($advancedProperties as $advancedProperty) {
                    $propertiesArray[] = array(
                        'id'            => $advancedProperty->get('property_id'),
                        'name'          => $advancedProperty->get('property_name'),
                        'valueTypeId'   => $advancedProperty->get('value_properties_type_id'),
                        'groupId'       => $advancedProperty->get('property_group_id'),
                        'groupName'     => $advancedProperty->get('property_group_name'),
                        'value'         => $advancedProperty->get('property_value')
                        );
                    $groupArray[] = $advancedProperty->get('property_group_name');
                }

                if(count($propertiesArray) > 0) {
                    $groupArrayUniq = array_unique($groupArray);
                    $valueDomainArray = array();
                    foreach ($groupArrayUniq as $group) {
                        $pdf->SetFillColor(230);
                        $pdf->SetFont('TimesB','',11);
                        $pdf->MultiCell(0, 7, ' Группа характеристик : '.$group, 1, 'L', 1);
                        $pdf->SetFillColor(250);
                        $pdf->SetFont('Times','',10);
                        foreach ($propertiesArray as $property) {
                            $name = ' '.$property['name'];
                            if ($property['valueTypeId'] == 5) {
                                $result = $this->wgsTier->getProjectObjectService()->getAttPropertyService()->GetListValueDomain($property['id']);
                                foreach($result as $object) {
                                    $valueDomainArray[$object->get('Value_Domain_ID')] = $object->get('Value_Domain');
                                }
                                $value = $valueDomainArray[$property['value']];
                            } else {
                                $value = $property['value'];
                            }
                            $value = ' '.$value;
                            if($property['groupName']== $group) {
                               $this->createRow($pdf, $name, $value);
                           }
                       }
                   }
               }
               $pdf->Cell(0, 5, '', 0, 1, 'L', 0);
           }
       }

       function addChildCards($objectId, $pdf)
       {
        $relations = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListRelByObject($objectId);

        if ($relations) {
            $relationList = array();
            foreach($relations as $relation)
            {
                $relationList[] = array(
                    'relationId'        => $relation->get('relation_id'),
                    'name'              => $relation->get('name'),
                    'relationTypeId'    => $relation->get('relation_type_id'),
                    'countTypeRelation' => $relation->get('count_type_relation')
                    );
            }
            foreach ($relationList as $relation) {
                $pdf->SetFillColor(230);
                $pdf->SetFont('TimesB','',11);
                $pdf->MultiCell(0, 7, ' Г‘ГўГїГ§Гј : '.$relation['name'], 1, 'L', 1);
                $cardsByRelation = $this->wgsTier->getProjectObjectService()->getAttObjectRelationService()->GetListChildObjectByRelation($relation['relationId'], $objectId);
                if($cardsByRelation) {
                    $cardsList = array();
                    $pdf->SetFillColor(250);
                    $pdf->SetFont('Times','',10);
                    foreach ($cardsByRelation as $cardByRelation) {
                        $cardsList[] = array(
                            'id'                => $cardByRelation->get('OBJECT_ID'),
                            'name'              => $cardByRelation->get('NAME'),
                            'shortName'         => $cardByRelation->get('SHORT_NAME'),
                            'typeid'            => $cardByRelation->get('OBJECT_TYPE_ID'),
                            'typeName'          => $cardByRelation->get('object_type_name'),
                            'invNumber'         => $cardByRelation->get('INVENTARY_NUMBER'),
                            'createDate'        => $cardByRelation->get('CREATE_DATE'),
                            'note'              => $cardByRelation->get('NOTE'),
                            'author'            => $cardByRelation->get('AUTHOR'),
                            'label'             => $cardByRelation->get('LABEL'),
                            'ObjTypeRelationId' => $cardByRelation->get('ObjType_relation_id'),
                            'objRelationId'     => $cardByRelation->get('Object_relation_id')
                            );
}

foreach ($cardsList as $card) {
    $name = ' '.$card['name'];
    $value = ' '.$card['typeName'];

    $this->createRow($pdf, $name, $value);
}
}
$pdf->Cell(0, 5, '', 0, 1, 'L', 0);
}
}
}

function createRow($pdf, $name, $value)
{
  $height = 5;
  $width = 180;
  $nameCellWidth = 70;
  $valueCellWidth = $width-$nameCellWidth;
  $marginLeft = 15;

  $y = $pdf->getY();

  $nameWidth = round($pdf->GetStringWidth($name));
  $valueWidth = round($pdf->GetStringWidth($value));
  $kn = ($nameWidth >= $nameCellWidth) ? floor(($nameCellWidth + $nameWidth) / $nameCellWidth) : 1;
  $kv = ($valueWidth >= $valueCellWidth) ? floor(($valueCellWidth + $valueWidth) / $valueCellWidth) : 1;

  $k = max($kn, $kv);

  $pdf->drawTextBox($name, $nameCellWidth, $height*$k, 'L', 'M');

  if ($pdf->getY() < $y) {
   $y = 10;
   $pdf->Rect($marginLeft, 10, $nameCellWidth, 10);
}

$pdf->setXY($nameCellWidth+$marginLeft, $y);

if ($kn > $kv) {
   $pdf->MultiCell($valueCellWidth, $height*$k, $value, 1, 'J', 0);
} else {
   $pdf->MultiCell($valueCellWidth, $height, $value, 1, 'J', 0);
}
}

    /*

    function createRow($p, $rows, $colls, $data)
    {
        $x0 = 45;
        $y0 = 760;
        $width = 510;
        $height = 680;
        $heightCell = 20;
        $x = $x0;
        for ($j = 0; $j < $rows; $j++) {
            if ($j == 0) {
                $y = $y0;
            }
            else {
                $y = $y0 - $heightCell*$j;
            }

            for ($i = 0; $i < $colls; $i++) {
                if ($i == 0) {
                    $p->rect($x0, $y, $width/$colls, $heightCell);
                    $p->moveto($x0+10, $y-10);
                    // $p->show_xy($data[$i][$j], $x0+10, $y-10);
                }
                else {
                    $x = $x+$width/$colls;
                    $p->rect($x, $y, $width/$colls, $heightCell);
                    //$p->show_xy($data[$i][$j], $x0+10, $y-10);
                }
            }
            $x = $x0;
        }
        $p->stroke();
    }

    */

    function doGetPdf()
    {
        $params = $this->getRequestParams();
        $seq = $params[2];
        $buf = @file_get_contents(WGS3_PRINTCACHE_DIR."card-$seq.pdf");
        $len = strlen($buf);
        $this->response->header("Content-type: application/pdf");
        $this->response->header("Content-Length: $len");
        $this->response->header("Content-Disposition: inline; filename=card-$seq.pdf");
        $this->response->write($buf);
        //@unlink(WGS3_PRINTCACHE_DIR."card-$seq.pdf");
    }

    function doTest()
    {
        $mass = array();
        for($i = 0; $i < 20; $i++) {
           $mass[] = array(
            "name" => rand(1, 100),
            "visits" => rand(1, rand(200, 10000))
            );
       }
       $this->response->write(json_encode(array_values($mass)));
   }




}
?>
