<?php
  ini_set('display_errors',1);
  error_reporting(E_ALL);
lmb_require('wgs/controller/ObjectAdmin/CardController.class.php');
lmb_require('wgs/controller/wgsSOAPClient/wgsSOAPClientController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
require_once( dirname(__FILE__) . '/../../../../application/projects/tusur_production/setup.php');

require_once('wgs/lib/fpdf/textbox.php');

include 'wgs/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';
include 'wgs/lib/PHPExcel/Classes/PHPExcel.php';

class DocumentController extends CardController
{
    public $soapClient;
    
    function __construct() {
        parent::__construct();
    }
    
    function printSingleStudentReport($studObjectId, $templateName, $reportName = '') {
        $card = $this -> doGetCardsByFeatureId(array($studObjectId), true);
        
        $studProps = $this->getStudProps($card, $studObjectId);
        $templatePath = WGS3_ROOT_DIR . '/application/docs/' . $templateName . '.docx';
        
        $templateArray = array('%fullName%', '%shortName%', '%firstName%', '%lastName%', '%middleName%', '%group%', '%faculty%', '%buildingId%', '%buildingAddress%', 
            '%birth%', 
            '%bd%',
            '%bm%', 
            '%by%',
            '%by0%', '%by1%', '%by2%', '%by3%', 
            '%birthPlace%', '%sex%', '%sexL%', '%citizenship%');
        $valueArray = array(
            $card[1][0]['name'], 
            $studProps['lastName'] . ' ' . mb_substr($studProps['firstName'], 0, 1) . '. ' . mb_substr($studProps['middleName'], 0, 1) . ".",
            $studProps['firstName'], 
            $studProps['lastName'], 
            $studProps['middleName'], 
            $studProps['group'], 
            $studProps['faculty'], 
            $studProps['buildingId'], 
            $studProps['buildingAddress'], 
            $studProps['birth'],
            $studProps['bd'], 
            $studProps['bm'], 
            $studProps['by'], 
            $studProps['by0'],
            $studProps['by1'], 
            $studProps['by2'], 
            $studProps['by3'], 
            ' ', 
            $studProps['sex'],
            $studProps['sexLayout'],
            $studProps['citizenship']
        );
        
        
        $fileName = '/doccache/report-' . rand() . '.docx';
        $FileResult = WGS3_ROOT_DIR . '/application/www'. $fileName;
        
        $this -> createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult);
        
        return array(
            'path' => $FileResult,
            'name' => $reportName . ' - ' . $card[1][0]['name'],
            'title'=> $card[1][0]['name']
        );
        // readfile($FileResult);
    }
    
    /*
    * Single student reports
    */
    
    function doPrintForeignRegistry() {
        $fileName = WGS3_ROOT_DIR . '/application/docs/foreign-citizen.xlsx';
        $cardId = $this -> request -> get('id');
        if($cardId == null) {
            die('no id');
        }

        $card = $this -> doGetCardsByFeatureId(array($cardId), true);
        
        $studProps = $this->getStudProps($card, $cardId);
        
        $objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');
        
        $objPHPExcel = $objPHPExcel->load("$fileName");
        //die('ok');
        $objPHPExcel->setActiveSheetIndex(0);

        $i=1;
        $row = 1;
        $col = 16;
        preg_match_all('#.#iu', mb_strtoupper($studProps["lastName"]), $charArray);
        
        foreach ($charArray[0] as $char) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $char);
            $col++;
        }
        $row = 2;
        $col = 16;
        preg_match_all('#.#iu', mb_strtoupper($studProps["firstName"]." ".$studProps['middleName']), $charArray);

        foreach ($charArray[0] as $char) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $char);
            $col++;
        }

        $row = 3;
        $col = 16;
        preg_match_all('#.#iu', mb_strtoupper($studProps['citizenship']), $charArray);

        foreach ($charArray[0] as $char) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $char);
            $col++;
        }

        $row = 9;
        $col = 16;
        preg_match_all('#\d#iu', $studProps['birth'], $charArray);

        foreach ($charArray[0] as $char) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $char);
            $col++;
        }

        switch($studProps['sex']) {
                
            case "М": {
                $row = 12;
                break;
            }
                
            case "Ж": {
                $row = 13;
                break;
            }
        }
        
        $col = 16;
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Х");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $resultFileName = WGS3_ROOT_DIR . '/application/www/doccache/foreign-citizen' . rand() . '.xlsx';
        $objWriter->save($resultFileName);
        
        ob_clean();
        $fsize = filesize($resultFileName);
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Length: ".$fsize);
        header('Content-Disposition: attachment; filename="' . $card[1][0]['name'] . '.xlsx"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        
        readfile($resultFileName);
    }
    
    function doDisplay() {
        $cardId = $this -> request -> get('id');
        if($cardId == null) {
            die('no id');
        }
        
        $fileInfo = $this -> printSingleStudentReport($cardId, 'report', 'Тестовый отчет');
        $this -> setHeaders($fileInfo['path'], $fileInfo['name']);
        readfile($fileInfo['path']);
        
    }
    
    function doRegistry() {
        $cardId = $this -> request -> get('id');
        if($cardId == null) {
           die('no id');
        }
        
        $zip = new ZipArchive();
        $fileName = WGS3_ROOT_DIR . '/application/www/doccache/registry-' . rand();
        $zip->open($fileName, ZIPARCHIVE::CREATE);
        $regFiles = array();
        
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-f3', 'Свидетельство о регистрации');
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-f2', 'Адресный листок прибытия');
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-f12p', 'Листок статистического учета');
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-f1', 'Заявление');
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-p3i6', 'Напрваление воинский учет');
        $regFiles[] = $this -> printSingleStudentReport($cardId, 'registry-f9', 'Карточка регистрации');
        
        foreach($regFiles as $regFile) {
            // var_dump(iconv("UTF-8","866",$regFile['name']));
            $zip->addFile($regFile['path'], iconv("UTF-8","866",$regFile['name']). ".docx");
        }
        // die();
        $zip -> close();
            
        ob_clean();
        $fsize = filesize($fileName);
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length: ".$fsize);
        header('Content-Disposition: attachment; filename="' . 'Регистрация - ' . $regFiles[0]['title'] . '.zip"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        
        readfile($fileName);
    }
    
    
    function doContractStudent() {
        $cardId = $this -> request -> get('id');
        if($cardId == null) {
            die('no id');
        }
        
        $fileInfo = $this -> printSingleStudentReport($cardId, 'contract-student', 'Договор');
        $this -> setHeaders($fileInfo['path'], $fileInfo['name']);
        readfile($fileInfo['path']);
    }
    
    /*
    * Multiple students reports
    */
    
    function doReplaceStudent() {
        $ids = json_decode($this -> request -> get('id'));
        
        if($ids == null) {
            die('no id');
        }
        
        $cards = array();
        
        $tplString = '</w:t></w:r></w:p>';
            
        $count = 0;
        foreach($ids as $cardId) {
            $card = $this -> doGetCardsByFeatureId(array($cardId), true);
            $studProps = $this->getStudProps($card, $cardId);
            
            $tplString .= '
                 <w:p><w:r><w:t>' . ++$count . '. ' .
                    $studProps['name'] . ", студент " . $studProps['course'] . ' курса гр. '. $studProps['group'] . ' ' . $studProps['faculty']  . '. Выселить из общежития, ...' .
                '</w:t></w:r></w:p>' .
                '<w:p><w:r><w:t>Основание: представление заведующего общежитием, виза проректора по развитию УК и СР, виза ППОС, виза декана.</w:t></w:r></w:p>
                <w:p><w:r><w:t></w:t></w:r></w:p>';
        }
        
        $tplString .= '<w:p><w:r><w:t>';
            
        
        
        $templatePath = WGS3_ROOT_DIR . '/application/docs/decontract-student.docx';
        $templateArray=array('%str%', '%date%');
        $valueArray=array($tplString, date_format(date_create('now'), 'd.m.y'));
        
        $fileName = '/doccache/report-' . rand() . '.docx';
        $FileResult = WGS3_ROOT_DIR . '/application/www'. $fileName;
        
        $this -> createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult);
        
        $this -> setHeaders($FileResult, 'Приказ на выселение - ' . date_format(date_create('now'), 'y-m-d'));
        readfile($FileResult);
    }
    
    function doPlaceStudent() {
        $ids = json_decode($this -> request -> get('id'));
        
        if($ids == null) {
            die('no id');
        }
        
        $cards = array();
        
        $tplString = '</w:t></w:r></w:p>';
            
        $count = 0;
        $buildingId = '';
        $buildingAddress = '';
        foreach($ids as $cardId) {
            $card = $this -> doGetCardsByFeatureId(array($cardId), true);
            $studProps = $this->getStudProps($card, $cardId);
            if($studProps['buildingId']) {
                $buildingId = $studProps['buildingId'];
                $buildingAddress = $studProps['buildingAddress'];
            }
            
            $tplString .= '
                 <w:p><w:r><w:t>' . ++$count . '. ' .
                    $studProps['name'] . "" . ', гр. '. $studProps['group'] . ', ' . $studProps['financing']  . ',     комн. ' . $studProps['room'] .
                '</w:t></w:r></w:p>';
                
            
        }
        
        $tplString .= '<w:p><w:r><w:t>';
          
        
        
        $templatePath = WGS3_ROOT_DIR . '/application/docs/place-student.docx';
        $templateArray=array('%str%', '%date%', '%buildingId%', '%buildingAddress%');
        $valueArray=array($tplString, date_format(date_create('now'), 'd.m.y'), $buildingId, $buildingAddress);
        
        $fileName = '/doccache/report-' . rand() . '.docx';
        $FileResult = WGS3_ROOT_DIR . '/application/www'. $fileName;
        
        $this -> createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult);
        
        $this -> setHeaders($FileResult, 'Приказ на заселение - ' . date_format(date_create('now'), 'y-m-d'));
        readfile($FileResult);
    }
    
    function doRectorat() {
        $layerAndType = json_decode($this -> request -> get('id'));
        $layerGroupId = $layerAndType -> layer;
        $typeId = $layerAndType -> type;
        
        if($layerGroupId == null || $typeId == null ) {
            die('no id');
        }
        
        $featureLayers = $this -> getFeatureLayerList(null, $layerGroupId);
        $hostelLayers = array();
        $addcount = 0;
        $clubcount =0;
            
        foreach($featureLayers as $layer) {
            $title = $layer['groupname'];
            $hostelLayers[$layer['id']] = array(
                'name' => $layer['name'], 
                'addcount' => 0,
                'clubcount' => 0,
                'tusurcount' => 0,
                'strcount' => 0,
                'perscount' => 0,
                'addrooms' => array(),
                'studrooms' => array(),
                'clubrooms' => array(),
                'persrooms' => array(),
                'tusurrooms' => array(),
            );
            $roomCards = $this->doGetCardsByTypeAndLayerId($layer['id'], 4, 0, 999999, '', '', false);
            $additionalCards = $this->doGetCardsByTypeAndLayerId($layer['id'], 7, 0, 999999, '', '', false);
            
            $stcount = 0;
            $tusurcount = 0;                
            $perscount = 0;
            foreach($additionalCards as $addCard) {
                $roomDescription = $this -> doGetCardsByFeatureId(array($addCard['cardId']), true);
                $rnum = 0;
                $rt = "";
                $rd = "";
                foreach($roomDescription[2][0] as $roomProperty) {
                    if($roomProperty['valueDescriptor'] == 'roomNumber') {
                        $rnum = $roomProperty['value'];
                    }
                    if($roomProperty['valueDescriptor'] == 'roomType') {
                        $rt = $roomProperty['value'];
                    }
                    if($roomProperty['valueDescriptor'] == 'roomDescr') {
                        $rd = $roomProperty['value'];
                    }
                   
                }
                
                if($rt == 43) {
                    $hostelLayers[$layer['id']]['addrooms'][] = $rnum . " (". $rd .")";
                    $hostelLayers[$layer['id']]['addcount'] += 1;
                    $addcount ++;
                }
                if($rt == 44) {
                    $hostelLayers[$layer['id']]['clubrooms'][] = $rnum. " (". $rd .")";
                    $hostelLayers[$layer['id']]['clubcount'] += 1;
                    $clubcount ++;
                }
            }
            
            foreach($roomCards as $roomCard) {
                $roomDescription = $this -> doGetCardsByFeatureId(array($roomCard['cardId']), true);
                
                $rnum = 0;
                $rt = "";
                $bedsNumber = 0;
                foreach($roomDescription[2][0] as $roomProperty) {
                    if($roomProperty['valueDescriptor'] == 'roomNumber') {
                        $rnum = $roomProperty['value'];
                    }
                    if($roomProperty['valueDescriptor'] == 'roomWhos') {
                        $rt = $roomProperty['value'];
                    }
                    if($roomProperty['valueDescriptor'] == 'maxcount') {
                        $bedsNumber = $roomProperty['value'];
                    }
                }
                
                // var_dump($bedsNumber);
                if($rt == 40) {
                    $hostelLayers[$layer['id']]['tusurrooms'][] = $rnum;
                    $hostelLayers[$layer['id']]['tusurcount'] += $bedsNumber;
                }
                if($rt == 39) {
                    $hostelLayers[$layer['id']]['studrooms'][] = $rnum;
                    $hostelLayers[$layer['id']]['stcount'] += $bedsNumber;
                }
                if($rt == 40) {
                    $hostelLayers[$layer['id']]['persrooms'][] = $rnum;
                    $hostelLayers[$layer['id']]['perscount'] += $bedsNumber;
                }
            }    
        }
        
        $totalBeds = 0;
        $tplString = '</w:t></w:r></w:p>';
        
        //Про обсл. персонал
        $tplString .= '<w:p><w:r><w:rPr>
                                     <w:u w:val="single" />
                                 </w:rPr><w:t>1. Комнаты, отведенные под обслуживающий персонал:</w:t></w:r></w:p><w:p><w:r><w:t>';
        foreach($hostelLayers as $hostelLayer) {
            if($hostelLayer['perscount'] == 0) { continue; }
            
            foreach($hostelLayer['persrooms'] as $rn) {
                $tplString .= $rn . ', ';
            }
            $tplString = mb_substr($tplString, 0, -2);
            
        }
        $tplString .= ' (Всего: ' . $hostelLayer['perscount'] . ') </w:t></w:r></w:p>';      
        
        //Про вспомогательные помещения
        $tplString .= '<w:p><w:r><w:rPr>
                                     <w:u w:val="single" />
                                 </w:rPr><w:t>Вспомогательные помещения:</w:t></w:r></w:p><w:p><w:r><w:t>';
        foreach($hostelLayers as $hostelLayer) {
            if($hostelLayer['addcount'] == 0) { continue; }
            
            foreach($hostelLayer['addrooms'] as $rn) {
                $tplString .= $rn . ', ';
            }
            $tplString = mb_substr($tplString, 0, -2);
            
        }
        $tplString .= ' (Всего: ' . $addcount . ') </w:t></w:r></w:p>';     
        
        
        //Про студентов.
        $tplString .= '<w:p><w:r><w:rPr>
                                     <w:u w:val="single" />
                                 </w:rPr><w:t>2. Комнаты для заселения студентов и аспирантов:</w:t></w:r></w:p>';
        foreach($hostelLayers as $hostelLayer) {
            if($hostelLayer['stcount'] == 0) { continue; }
            
            $tplString .= '<w:p><w:r><w:t>' . $hostelLayer['name'] . ' - ' . $hostelLayer['stcount'] . 'к.мест - к. ' ;
            $totalBeds += $hostelLayer['stcount'];
            foreach($hostelLayer['studrooms'] as $rn) {
                $tplString .= $rn . ', ';
            }
            $tplString = mb_substr($tplString, 0, -2);
            $tplString .= '</w:t></w:r></w:p>';      
        }
        $tplString .= '<w:p><w:r><w:t> Всего: ' . $totalBeds . ' к.мест';
        $tplString .= '</w:t></w:r></w:p>';      
        
        //Про free rooms
        $tplString .= '<w:p><w:r><w:rPr>
                                     <w:u w:val="single" />
                                 </w:rPr><w:t>3. Комнаты фонда студентов и аспирантов, подлежащие освобождению:';
        $tplString .= '</w:t></w:r></w:p>';      
        
        //Про клубы
        $tplString .= '<w:p><w:r><w:rPr>
                                     <w:u w:val="single" />
                                 </w:rPr><w:t>4. Комнаты, отведенные под клубы и учебные помещения:</w:t></w:r></w:p><w:p><w:r><w:t>';
        foreach($hostelLayers as $hostelLayer) {
            if($hostelLayer['clubcount'] == 0) { continue; }
            
            foreach($hostelLayer['clubrooms'] as $rn) {
                $tplString .= $rn . ', ';
            }
            $tplString = mb_substr($tplString, 0, -2);
            
        }
        $tplString .= ' (Всего: ' . $clubcount . ') </w:t></w:r></w:p>';    
        
        $tplString .= '<w:p><w:r><w:t>';
        
        $templatePath = WGS3_ROOT_DIR . '/application/docs/rectorat.docx';
        $templateArray=array('%str%', '%title%');
        
        $valueArray=array($tplString, $title);
        
        
        // var_dump($tplString);
        // die();
        $fileName = '/doccache/report-' . rand() . '.docx';
        $FileResult = WGS3_ROOT_DIR . '/application/www'. $fileName;
        
        $this -> createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult);
        
        $this -> setHeaders($FileResult, 'Решение ректората - ' . $title);
        readfile($FileResult);
    }
    
    function doElection() {
        $layerAndType = json_decode($this -> request -> get('id'));
        $layerGroupId = $layerAndType -> layer;
        $typeId = $layerAndType -> type;
        
        if($layerGroupId == null || $typeId == null ) {
            die('no id');
        }
        // var_dump($layerGroupId);
        // var_dump($typeId);
        
        $featureLayers = $this -> getFeatureLayerList(null, $layerGroupId);
        $roomCards = array();
        foreach($featureLayers as $layer) {
            $layerName = $layer ['name'];
            $buildingName = $layer ['groupname'];
            
            $roomCards = array_merge($roomCards, $this->doGetCardsByTypeAndLayerId($layer['id'], 4, 0, 999999, '', '', false));
        }
        // $cards = array();
        
        $tplString = '</w:t></w:r></w:p><w:tbl>';
        $tplString .= '
            <w:tr>
                  <w:tc><w:p><w:r><w:t>№пп</w:t></w:r></w:p></w:tc>' .
                  '<w:tc><w:p><w:r><w:t>ФИО</w:t></w:r></w:p></w:tc>' .
                  '<w:tc><w:p><w:r><w:t>Дата рождения</w:t></w:r></w:p></w:tc>' .
                  '<w:tc><w:p><w:r><w:t>Адрес проживания</w:t></w:r></w:p></w:tc>' .
                  '<w:tc><w:p><w:r><w:t>Адрес регистрации</w:t></w:r></w:p></w:tc>' .
                  '<w:tc><w:p><w:r><w:t>Номер комнаты</w:t></w:r></w:p></w:tc>' .
           '</w:tr>';            
        $count = 0;
        // $layers = $this->wgsTier->getProjectObjectService()->getFeatureLayerService()->get($layerId);
        
        foreach($roomCards as $roomCard) {
            if($roomCard['cardId']) {
                $studentCards = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getChildObjectsByParent($roomCard['cardId'], 1);
                foreach($studentCards as $studentCard) {
                    if($studentCard -> get('object_id') == null) continue;
                    // var_dump($studentCard -> get('object_id'));
                    // die();
                    $card = $this -> doGetCardsByFeatureId(array($studentCard -> get('object_id')), true);
                    $stCardId = $studentCard -> get('object_id');
                    $studProps = $this->getStudProps($card, $stCardId);
                    
                    $date1 = date_create_from_format('d.m.Y', $studProps['birth']);
                    
                    $fullYears = date_create('now') -> diff($date1) -> y;
                    
                    if(!$fullYears || $fullYears < 18) { continue; }
                    if(mb_strtolower($studProps['citizenship']) != "россия" ) { continue; }
                    
                    $buildingId = $studProps['buildingId'];
                    
                    $tplString .= '
                         <w:tr>
                            <w:tc><w:p><w:r><w:t>' . ++$count . '</w:t></w:r></w:p></w:tc>' .
                            '<w:tc><w:p><w:r><w:t>'.  $studProps['name'] . '</w:t></w:r></w:p></w:tc>' .
                            '<w:tc><w:p><w:r><w:t>' . $studProps['birth'] . '</w:t></w:r></w:p></w:tc>' .
                            '<w:tc><w:p><w:r><w:t>' . '' . '</w:t></w:r></w:p></w:tc>' .
                            '<w:tc><w:p><w:r><w:t>' . '' . '</w:t></w:r></w:p></w:tc>' .
                            '<w:tc><w:p><w:r><w:t>' . $studProps['room'] . '</w:t></w:r></w:p></w:tc>' .
                        '</w:tr>';    
                }
            }
        }
        
        $tplString .= '</w:tbl><w:p><w:r><w:t>';
      
        
        $templatePath = WGS3_ROOT_DIR . '/application/docs/election.docx';
        $templateArray=array('%str%', '%date%', '%buildingName%', '%layerName%');
        $valueArray=array($tplString, date_format(date_create('now'), 'd.m.y'), $buildingName, "");
        
        $fileName = '/doccache/report-' . rand() . '.docx';
        $FileResult = WGS3_ROOT_DIR . '/application/www'. $fileName;
        
        $this -> createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult);
        
        $this -> setHeaders($FileResult, 'Список студентов для выборов');
        readfile($FileResult);
    }
    
   
    
    function getStudProps($card, $cardId) {
        $result = array();
        $cardPropValues = $card[2][0];
        
        foreach($cardPropValues as $studProp) {
           switch($studProp['valueDescriptor']) {
                
                case "sex": {
                    $result['sex'] = $studProp['value'];
                    $result['sexLayout'] = "</w:t></w:r><w:r>" . ($studProp['value'] == "М" ? '
                        <w:rPr>
                            <w:sz w:val="16"/>
                            <w:szCs w:val="16"/>
                            <w:u w:val="single" />
                        </w:rPr>
                        <w:t xml:space="preserve">муж</w:t>
                    </w:r>
                    <w:r>
                        <w:rPr>
                          <w:sz w:val="16"/>
                          <w:szCs w:val="16"/>
                        </w:rPr>
                        <w:t>/жен</w:t>' : '
                    </w:r>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="16"/>
                            <w:szCs w:val="16"/>
                        </w:rPr>
                        <w:t>муж/</w:t>
                        <w:rPr>
                            <w:sz w:val="16"/>
                            <w:szCs w:val="16"/>
                            <w:u w:val="single" />
                        </w:rPr>
                        <w:t xml:space="preserve">жен</w:t>') . '</w:r><w:r><w:t>';
                    break;
                }
                
                case "firstName": {
                    $result['firstName'] = $studProp['value'];
                    break;
                }
                
                case "lastName": {
                    $result['lastName'] = $studProp['value'];
                    break;
                }
                
                case "middleName": {
                    $result['middleName'] = $studProp['value'];
                    break;
                }
                
                case "group" : {
                    $result['group'] = $studProp['value'];
                    break;     
                }
                
                case "faculty" : {
                    $result['faculty'] = $studProp['value'];
                    break;     
                }
                
                case "birth" : {
                    $result['birth'] = date_format(date_create_from_format('Y-m-d', $studProp['value']), 'd.m.Y');
                    $result['bd'] = date_format(date_create_from_format('Y-m-d', $studProp['value']), 'd');
                    $result['bm'] = date_format(date_create_from_format('Y-m-d', $studProp['value']), 'm');
                    $result['by'] = date_format(date_create_from_format('Y-m-d', $studProp['value']), 'Y');
                    $result['by0'] = mb_substr($result['by'], 0, 1);
                    $result['by1'] = mb_substr($result['by'], 1, 1);
                    $result['by2'] = mb_substr($result['by'], 2, 1);
                    $result['by3'] = mb_substr($result['by'], 3, 1);
                    break;     
                }
                
                case "course" : {
                    $result['course'] = $studProp['value'];
                    break;     
                }
                
                case "financing" : {
                    $result['financing'] = $studProp['value'];
                    break;     
                }
                
                case "citizenship" : {
                    $result['citizenship'] = $studProp['value'];
                    break;     
                }
                
                
           }
        }
        
        $result['name'] = $card[1][0]['name'];
        
        $parentCards = $this->wgsTier->getProjectObjectService()->getAttObjectService()->getParentObjectsByChild($cardId, 1);
        foreach($parentCards as $parentCard) {
            $result['parents'] = $this -> doGetCardsByFeatureId(array($parentCard -> get('object_id')), true);
        }
        
        
        if($result['parents'] != null) {
            $result['room'] = $result['parents'][1][0]['name'];
            
            foreach($result['parents'][2][0] as $parentProperty) {
                if ($parentProperty['valueDescriptor'] == "building") {
                    $PROJECT_BUILDINGS = getBuildings();
                    
                    $buildingId = $parentProperty['value'];
                    
                    $result['buildingName'] = $PROJECT_BUILDINGS[$buildingId]['label'];
                    $result['buildingAddress'] = $PROJECT_BUILDINGS[$buildingId]['address'];
                    $result['buildingId'] = $PROJECT_BUILDINGS[$buildingId]['number'];
                }
            }    
        }
        
        return $result;
        
    }
    
    
    
    function setHeaders($FileResult, $name) {
        ob_clean();
        $fsize = filesize($FileResult);
        header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        header("Content-Length: ".$fsize);
        header('Content-Disposition: attachment; filename="' . $name . '.docx"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
    }
    
    /*
    Params:
    $templatePath - Path to the template file
    $templateArray - Array of template in file $templatePath
    $valueArray - Array value of templates $templateArray 
    $FileResult - Path to file result
    */
    function createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult)
    {
        if (!file_exists($templatePath)) {
            die('File template not found.');
        }
        copy($templatePath, $FileResult);
        
        $zip = new ZipArchive();
        if (!$zip->open($FileResult)) {
            die('File template can not open.');
        }
        $documentXml = $zip->getFromName('word/document.xml');
                
        for ($i = 0; $i < count($templateArray); $i++) {
            $reg = ''.substr($templateArray[$i], 0, 1);
            for ($j = 1; $j < strlen($templateArray[$i]); $j++) 
                $reg .= '(<.*?>)*+' . substr($templateArray[$i], $j, 1);  
            $reg = '#' . str_replace(array('#', '{', '[', ']', '}'), array('#', '{', '[', ']', '}'), $reg) . '#';
            $documentXml = preg_replace($reg, $valueArray[$i], $documentXml);
        }
                
        $zip->deleteName('word/document.xml');
        $zip->addFromString('word/document.xml', $documentXml);
        $zip->close();
    }

    
}
?>
