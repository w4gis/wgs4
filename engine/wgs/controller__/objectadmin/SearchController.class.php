<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class SearchController extends wgsFrontController
{
    function doDisplay()
    {
    }

    function doGetValuesByPropertyId()
    {
        $propertyId = $this->request->getPost('propertyId');
        $propertyType = $this->request->getPost('propertyType');

        $featureTypes = $this->request->getPost('featureTypes');
        $featureLayers = $this->request->getPost('featureLayers');

        if($propertyId)
        {
            $typesAndLayers = $this->getLayersAndTypes($featureTypes, $featureLayers);
            $featureTypes = $typesAndLayers['featureTypes'];
            $featureLayers = $typesAndLayers['featureLayers'];
        }

        $propertyValueArray = array();
        if(substr_count($propertyId, 'b') == 0)
        {

        	if($propertyType != 5)
            {
                $objectTypeIds = implode(',', $this->request->getPost('featureTypes'));
                $valueList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetValueList($propertyId, $objectTypeIds);
                foreach ($valueList as $value)
                $propertyValueArray[] = array(
                	'propertyValue' => lmb_win1251_to_utf8($value->get('property_value'))
                );
            }
            else
            {
                $valueList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetDomainValueList($propertyId);
                foreach ($valueList as $value) {
                    $propertyValueArray[] = array(
                                                'propertyValue' => lmb_win1251_to_utf8($value->get('value_domain')),
                                                'propertyValueId' => $value->get('value_domain_id')
                    );
                }
            }
        }
        else
        {
            $propertyName;
            switch($propertyId){
                case 'b1':
                    $propertyName = 'name';
                    break;
                case 'b2':
                    $propertyName = 'short_name';
                    break;
                case 'b3':
                    $propertyName = 'create_date';
                    break;
                case 'b4':
                    $propertyName = 'note';
                    break;
            }
            $valueList = $this->wgsTier->getProjectTier()->getSearchService()->getValuesOfBaseProperty($propertyName, implode(',',$featureLayers), implode(',',$featureTypes));
            foreach ($valueList as $value)
            $propertyValueArray[] = array(
                                                'propertyValue' => lmb_win1251_to_utf8($value->get($propertyName))
            );

            if ($propertyType == 3 || $propertyName == 'create_date') {

            	$r = array();

            	foreach ($propertyValueArray as $p) {
            		$f = date_parse($p['propertyValue']);
            		if ($f) {
            			$month = (strlen($f['month'])==1) ? '0'.$f['month'] : $f['month'];
            			$day = (strlen($f['day'])==1) ? '0'.$f['day'] : $f['day'];
            			array_push($r, $f['year'].'-'.$month.'-'.$day);
            		}
            	}
            	$ur = array_unique($r);
            	$propertyValueArray = array();

            	foreach ($ur as $urv) {
            		$propertyValueArray[] = array(
            			'propertyValue' => $urv
            		);
            	}
            }
        }
        $this->response->write(json_encode(array_values($propertyValueArray)));
        //print_r($featureTypes);
        //print_r($featureLayers);
    }


    function doGetFeatureLayerList()
    {
        $featureLayerArray = array();
        if ($featureLayerList = $this->wgsTier->getProjectObjectService()->getFeatureLayerList()) {
            foreach ($featureLayerList as $featureLayer) {
                if ($featureLayer->getInMap()) {
                    $featureLayerArray[] = array(
                        'featureLayerName' => ($featureLayer->getObjectTypeCount())? lmb_win1251_to_utf8($featureLayer->getName()): "<span style=\"font-style:italic; color: gray;\">".lmb_win1251_to_utf8($featureLayer->getName())."</span>",
                        'featureLayerId' => $featureLayer->getId(),
                        'featureLayerGroup' => lmb_win1251_to_utf8($featureLayer->getFeatureLayerGroupName())
                    );
                }
            }
        }
        $this->response->write(json_encode(array_merge($featureLayerArray)));
    }

    /*    function doGetFeatureTypeList()
     {
     $featureTypeArray = array();
     $featureTypeFilter = $this->request->getPost('featureTypeFilter');
     $featureTypeFilterExists = false;
     if (is_array($featureTypeFilter)) {
     $featureTypeFilterExists = true;
     }
     $hasPost = ($this->request->getPost('featureLayers'))? true: false;
     $featureLayers = ($this->request->getPost('featureLayers'))? $this->request->getPost('featureLayers'): $this->wgsTier->getProjectObjectService()->getFeatureLayerList();
     if ($featureLayers = $this->request->getPost('featureLayers')) {
     foreach($featureLayers as $featureLayer) {
     $featureLayerId = ($hasPost)? $featureLayer: $featureLayer->getId();
     if ($featureTypeList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($featureLayerId->getId())) {
     foreach ($featureTypeList as $featureType) {
     $featureTypeName = lmb_win1251_to_utf8($featureType->get('name'));
     $featureTypeId = lmb_win1251_to_utf8($featureType->get('object_type_id'));
     if (($featureTypeFilterExists && in_array($featureTypeId, $featureTypeFilter)) || !$featureTypeFilterExists) {
     $featureTypeArray[$featureTypeId] = array(
     'featureTypeName' => $featureTypeName,
     'featureTypeId' => $featureTypeId
     );
     } else {
     $featureTypeArray[$featureTypeId] = array(
     'featureTypeName' => $featureTypeName,
     'featureTypeId' => $featureTypeId
     );
     }
     }
     }
     }
     ksort($featureTypeArray);
     }
     $this->response->write(json_encode(array_values($featureTypeArray)));
     }*/

    function doGetFeatureTypeList()
    {
        $featureTypeArray = array();
        $featureTypeFilter = $this->request->getPost('featureTypeFilter');
        $featureTypeFilterExists = false;
        if (is_array($featureTypeFilter)) {
            $featureTypeFilterExists = true;
        }
        $hasPost = ($this->request->getPost('featureLayers'))? true: false;
        $featureLayers = $this->request->getPost('featureLayers');
        if ($featureLayers) {
            foreach($featureLayers as $featureLayer) {
                $featureLayerId = ($hasPost)? $featureLayer: $featureLayer->getId();
                if ($featureTypeList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer($featureLayerId/*->getId()*/)) {
                    foreach ($featureTypeList as $featureType) {
                        $featureTypeName = lmb_win1251_to_utf8($featureType->get('name'));
                        $featureTypeId = lmb_win1251_to_utf8($featureType->get('object_type_id'));
                        if (($featureTypeFilterExists && in_array($featureTypeId, $featureTypeFilter)) || !$featureTypeFilterExists) {
                            $featureTypeArray[$featureTypeId] = array(
                                'featureTypeName' => $featureTypeName,
                                'featureTypeId' => $featureTypeId
                            );
                        }
                    }
                }
            }
        } else {
            if ($featureTypeList = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer()) {
                foreach ($featureTypeList as $featureType) {
                    $featureTypeName = lmb_win1251_to_utf8($featureType->get('name'));
                    $featureTypeId = lmb_win1251_to_utf8($featureType->get('object_type_id'));
                    if (($featureTypeFilterExists && in_array($featureTypeId, $featureTypeFilter)) || !$featureTypeFilterExists) {
                        $featureTypeArray[$featureTypeId] = array(
                            'featureTypeName' => "<span>$featureTypeName</span>",
                            'featureTypeId' => $featureTypeId
                        );
                    }
                }
            }
        }
        //ksort($featureTypeArray);
        $this->response->write(json_encode(array_values($featureTypeArray)));
    }

    function doGetPropertyList()
    {
        $propertyArray = array();
        $returnPropertyArray = array();

        $hasPost = ($this->request->getPost('featureTypes'))? true: false;
        $featureTypes = ($this->request->getPost('featureTypes'))? $this->request->getPost('featureTypes'): $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->GetListFeatureTypeByLayer();
        $propertyTypeFilter = ($this->request->getPost('propertyTypeFilter'))? $this->request->getPost('propertyTypeFilter'): array();
        $propertyFilter = ($this->request->getPost('propertyFilter'))? $this->request->getPost('propertyFilter'): array();
        $baseFilter = ($this->request->getPost('baseFilter'))? $this->request->getPost('baseFilter'): false;

        if ($featureTypes/* = $this->request->getPost('featureTypes')*/) {
            foreach($featureTypes as $featureType) {
                $featureTypeId = ($hasPost)? $featureType: $featureType->get('object_type_id');
                $propertyLists = array();
                if (!$baseFilter) {
                    $propertyLists[] = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getBaseProperties();
                }
                $propertyLists[] = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService()->getListPropertyByObjectType($featureTypeId);
                $propertiesPart = ($baseFilter)? 1: 0;
                foreach ($propertyLists as $propertyList) {
                    $propertiesPart++;
                    foreach ($propertyList as $property) {
                        $propertyName      = lmb_win1251_to_utf8($property->get('name'));
                        $propertyGroupName = lmb_win1251_to_utf8($property->get('property_group_name'));
                        $propertyId        = lmb_win1251_to_utf8($property->get('property_id'));
                        $propertyType      = lmb_win1251_to_utf8($property->get('value_properties_type_id'));
                        if (!count($propertyFilter) || (count($propertyFilter) && !in_array($propertyId, $propertyFilter))){
                            if (isset($propertyArray[$propertyName]) && $propertyArray[$propertyName]) {
                                $propertyArray[$propertyName]['count']++;
                            } else {
                                if (!count($propertyTypeFilter) || (count($propertyTypeFilter) && in_array($propertyType, $propertyTypeFilter))) {
                                    $propertyArray[$propertyName] = array(
                                        'propertyName' => $propertyName,
                                        'propertyId' => $propertyId,
                                        'propertyType' => $propertyType,
                                        'propertyGroupName' => $propertyGroupName,
                                        'class' => ($propertiesPart == 1)? "base-prop": "x-form-field",
                                        'count' => 1
                                    );
                                }
                            }
                        }
                    }
                }
            }
            if (count($propertyArray)) {
                foreach($propertyArray as $key => $property) {
                    if ($property['count'] == count($featureTypes)) {
                        $returnPropertyArray[$key] = $property;
                    }
                }
                //krsort($returnPropertyArray);
            }
        }
        $this->response->write(json_encode(array_values($returnPropertyArray)));
    }

    function doGetDomainValueList()
    {
        if ($propertyId = $this->request->getPost('propertyId')) {
            $valueArray = array();
            if ($valueList = $this->wgsTier->getProjectObjectService()->getAttObjectService()->GetDomainValueList($propertyId)) {
                foreach ($valueList as $value) {
                    $valueArray[] = array(
                        'domainValue' => lmb_win1251_to_utf8($value->get('value_domain')),
                        'domainValueId' => $value->get('value_domain_id')
                    );
                }
            }
            $this->response->write(json_encode(array_values($valueArray)));
        }
    }

    function search($featureLayers, $featureTypes, $condition, $start, $limit, $mode, $searchInUnbinded, $docSearch, $filter)
    {
        $condition = str_replace("PROP", "t.PROP", $condition);
	    $condition = preg_replace("('([0-9]{4}-[0-9]{2}-[0-9]{2})')","TO_DATE('\\1','YYYY-MM-DD')::text",$condition);

        preg_match_all("/(PROP_([b0-9]*)[ \)]?) (?:>|<|=|L|N|I)/", $condition, $properties);
        $properties = $properties[2];

	$properties = array_unique($properties);

//print_r($featureLayers);
//print_r($featureTypes);exit;
        $r = $this->wgsTier->getProjectTier()->getSearchService()->search($featureTypes, $featureLayers, $properties, $condition, $mode, $start, $limit, $searchInUnbinded, $docSearch,$filter, $totalCount);
        if($r instanceof lmbPgsqlRecordSet) {
            $r->rewind();
            $count = $r->countPaginated();
            $result = array();
            $result['success'] = true;
            $result['totalCount'] = $r->totalCount();

            for($i=0;$i<$count;$i++) {
                $value = $r->current();
                $name = ($mode)? lmb_win1251_to_utf8($value->get("prop_b1")): lmb_win1251_to_utf8($value->get("curent_name"));
                $result['rows'][] = array(
                    'name' => ($name)? $name: lmb_win1251_to_utf8('<span style="color:#bbb">��� ������������</span>'),
                    'entityId' => $value->get("entityid"),
                    'featureId' => $value->get("feature_id"),
                    'objectId' => $value->get("object_id"),
                    'hidebindcard'=> (!$value->get("feature_id") && $value->get("layersBinded"))? false: true,
                    'hidefeaturedoc'=> ($value->get("doc_exists")!=0)? false: true,
                    'layer' => lmb_win1251_to_utf8($value->get("admplayer")),
                    'type' => lmb_win1251_to_utf8($value->get("object_type_name")),
                    'featureTypeId' => $value->get("object_type_id"),
                );
                $r->next();
            }

        } else if (count($r)) {
            $result = array();
            $result['success'] = true;
            $result['totalCount'] = $totalCount;

            foreach($r as $value) {
                $name = ($mode)? lmb_win1251_to_utf8($value->get("prop_b1")): lmb_win1251_to_utf8($value->get("curent_name"));
                $result['rows'][] = array(
                    'name' => ($name)? $name: lmb_win1251_to_utf8('<span style="color:#bbb">��� ������������</span>'),
                    'entityId' => $value->get("entityid"),
                    'featureId' => $value->get("feature_id"),
                    'objectId' => $value->get("object_id"),
                    'hidebindcard'=> (!$value->get("feature_id") && $value->get("layersBinded"))? false: true,
                	'hidefeaturedoc'=> ($value->get("doc_exists")!=0)? false: true,
                    'layer' => lmb_win1251_to_utf8($value->get("admplayer")),
                    'type' => lmb_win1251_to_utf8($value->get("object_type_name")),
                    'featureTypeId' => $value->get("object_type_id"),
                );
            }
        } else {
            $result['success'] = true;
            $result['totalCount'] = 0;
            $result['rows'] = array();
        }
        return $result;
    }

    function getInMapFeatureLayers()
    {
        $result = array();
        $featureLayerList = $this->wgsTier->getProjectObjectService()->getFeatureLayerList();
        foreach ($featureLayerList as $featureLayer) {
            if ($featureLayer->getInMap()) {
                $result[] = $featureLayer->getId();
            }
        }
        return $result;
    }

    function doGetResult()
    {
        /*$condition = "PROP_b3 > '2010-03-22 14:35:17'";
		$docSearch = false;
		$featureLayers = array(20);
		$featureTypes =	array(390);
		$limit = 30;
		$mode = 1;*/
		$f = "";
		$filter = "";
		$wgsuser_roles = $this->wgsTier->getAuthorizedUser()->getParentRoles();				
		foreach ($wgsuser_roles as $role){
			$ur = $role->getId();
			$fp ="";
			$results = $this->wgsTier->getProjectRoleService()->getArea(1,$ur);
				
			foreach($results as $result) {
				$fp = $result->get('limit_sdo');
			}
			if($fp){
				$f = "SDO_INSIDE(
					fl.geometry,
					SDO_GEOMETRY(2003, NULL, NULL,
					SDO_ELEM_INFO_ARRAY(1,1003,1),
					SDO_ORDINATE_ARRAY($fp))
				) = 'TRUE' ";
				$filter .= " AND " . $f;
			}
		}	
		$condition = lmb_utf8_to_win1251($this->request->getPost('condition'));
        if ($condition == '') {
            $condition = '(PROP_b1 = PROP_b1 or 1 = 1)';
        }

        $featureTypes = $this->request->getPost('featureTypes');
        $featureLayers = $this->request->getPost('featureLayers');
        $start = $this->request->getPost('start')? $this->request->getPost('start'): 0;
        $limit = $this->request->getPost('limit')? $this->request->getPost('limit'): 30;
        $mode  = $this->request->getPost('mode');
        $docSearch  = $this->request->getPost('docSearch')=='true'? 1: 0;

        /*$condition = "PROP_b3 = '2010-03-22'";

		$docSearch = false;
		$featureLayers = array(20);
		$featureTypes =	array(390);
		$limit = 30;
		$mode = 1;*/

        $featureTypeFilter = $this->request->getPost('featureTypeFilter');
        $featureTypeFilterExists = false;
        if (is_array($featureTypeFilter)) {
            $featureTypeFilterExists = true;
        }

        $searchInUnbinded = $featureTypeFilterExists? 1: 0;

        // ���� ������ �������
        if ($condition) {
            // ���� ������ � ����, � ����
            if ( $featureTypes && count($featureTypes) > 0 && $featureLayers && count($featureLayers) > 0 ) {
                $a = array();
                $b = array();
                foreach ($featureTypes as $featureTypeId) {
                    foreach ($featureLayers as $featureLayerId) {
                        $a[] = $featureTypeId;
                        $b[] = $featureLayerId;
                    }
                }
                $featureTypes  = $a;
                $featureLayers = $b;
            }
            else
            // ���� ������ ������ ����
            if ( $featureTypes && count($featureTypes) > 0 && ( !$featureLayers || count($featureLayers) == 0 ) ) {
                $a = array();
                $b = array();

                $inMapFeatureLayers = $this->getInMapFeatureLayers();
                $featureLayerService = $this->wgsTier->getProjectObjectService()->getFeatureLayerService();
                foreach ($featureTypes as $featureTypeId) {
                    $featureLayers = $featureLayerService->GetListFeatureLayerByType($featureTypeId);
                    if (count($featureLayers) > 0) {
                        foreach ($featureLayers as $featureLayer) {
                            $featureLayerId = $featureLayer->get('featurelayer_id');
                            if (!$mode) {
                                if (in_array($featureLayerId, $inMapFeatureLayers)) {
                                    $a[] = $featureTypeId;
                                    $b[] = $featureLayerId;
                                }
                            } else {
                                $a[] = $featureTypeId;
                                $b[] = $featureLayerId;
                            }
                        }
                    } else {
                        $a[] = $featureTypeId;
                        $b[] = 0;
                    }
                }
                $featureTypes  = $a;
                $featureLayers = $b;
            }
            else
            // ���� ������ ������ ����
            if ( $featureLayers && count($featureLayers) > 0 && ( !$featureTypes || count($featureTypes) == 0 ) ) {
                $featureTypeService = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService();
                foreach ($featureLayers as $featureLayerId) {
                    $featureTypes = $featureTypeService->GetListFeatureTypeByLayer($featureLayerId);
                    foreach ($featureTypes as $featureType) {
                        $a[] = $featureType->get('object_type_id');
                        $b[] = $featureLayerId;
                    }
                }
                $featureTypes  = $a;
                $featureLayers = $b;
            }
            else
            // ���� �� ������ ������
            if ( (!$featureLayers || count($featureLayers) == 0) && ( !$featureTypes || count($featureTypes) == 0 ) ) {
                $a = array();
                $b = array();

                $inMapFeatureLayers  = $this->getInMapFeatureLayers();
                $featureLayerService = $this->wgsTier->getProjectObjectService()->getFeatureLayerService();
                $featureTypeService  = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService();

                $featureTypes = $featureTypeService->GetListFeatureTypeByLayer();
                
                foreach ($featureTypes as $featureType) {
                    $featureTypeId = $featureType->get('object_type_id');
                    if (($featureTypeFilterExists && in_array($featureTypeId, $featureTypeFilter)) || !$featureTypeFilterExists) {
                        $featureLayers = $featureLayerService->GetListFeatureLayerByType($featureTypeId);
                        if (count($featureLayers) > 0) {
                            foreach ($featureLayers as $featureLayer) {
                                $featureLayerId = $featureLayer->get('featurelayer_id');
                                if (!$mode) {
                                    if (in_array($featureLayerId, $inMapFeatureLayers)) {
                                        $a[] = $featureTypeId;
                                        $b[] = $featureLayerId;
                                    }
                                } else {
                                    $a[] = $featureTypeId;
                                    $b[] = $featureLayerId;
                                }
                            }
                        } else {
                            $a[] = $featureTypeId;
                            $b[] = 0;
                        }
                    }
                }
                $featureTypes  = $a;
                $featureLayers = $b;
            }
            if ($featureTypes && $featureLayers && $condition) {
                //print_r($featureLayers);
                //print_r($featureTypes);
                //echo $condition;
                $result = $this->search($featureLayers, $featureTypes, $condition, $start, $limit, $mode, $searchInUnbinded, $docSearch, $filter);
                $this->response->write(json_encode($result));
            }
        }
    }

    function getLayersAndTypes($featureTypes, $featureLayers)
    {
        $a = array();
        $b = array();
        // ���� ������ � ����, � ����
        if ( $featureTypes && count($featureTypes) > 0 && $featureLayers && count($featureLayers) > 0 ) {
            foreach ($featureTypes as $featureTypeId) {
                foreach ($featureLayers as $featureLayerId) {
                    $a[] = $featureTypeId;
                    $b[] = $featureLayerId;
                }
            }
        }
        // ���� ������ ������ ����
        if ( $featureTypes && count($featureTypes) > 0 && ( !$featureLayers || count($featureLayers) == 0 ) ) {
            $featureLayerService = $this->wgsTier->getProjectObjectService()->getFeatureLayerService();
            foreach ($featureTypes as $featureTypeId) {
                $featureLayers = $featureLayerService->GetListFeatureLayerByType($featureTypeId);
                foreach ($featureLayers as $featureLayer) {
                    $a[] = $featureTypeId;
                    $b[] = $featureLayer->get('featurelayer_id');
                }
            }
        }
        // ���� ������ ������ ����
        if ( $featureLayers && count($featureLayers) > 0 && ( !$featureTypes || count($featureTypes) == 0 ) ) {
            $featureTypeService = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService();
            foreach ($featureLayers as $featureLayerId) {
                $featureTypes = $featureTypeService->GetListFeatureTypeByLayer($featureLayerId);
                foreach ($featureTypes as $featureType) {
                    $a[] = $featureType->get('object_type_id');
                    $b[] = $featureLayerId;
                }
            }
        }
        // ���� �� ������ ������
        if ( (!$featureLayers || count($featureLayers) == 0) && ( !$featureTypes || count($featureTypes) == 0 ) ) {
            $featureLayerService = $this->wgsTier->getProjectObjectService()->getFeatureLayerService();
            $featureTypeService  = $this->wgsTier->getProjectObjectService()->getAttObjectTypeService();
            $featureTypes = $featureTypeService->GetListFeatureTypeByLayer();
            foreach ($featureTypes as $featureType) {
                $featureTypeId = $featureType->get('object_type_id');
                $featureLayers = $featureLayerService->GetListFeatureLayerByType($featureTypeId);
                foreach ($featureLayers as $featureLayer) {
                    $featureLayerId = $featureLayer->get('featurelayer_id');
                    $a[] = $featureTypeId;
                    $b[] = $featureLayerId;
                }
            }
        }
        return array( 'featureTypes' => $a, 'featureLayers' => $b);
    }
}

?>
