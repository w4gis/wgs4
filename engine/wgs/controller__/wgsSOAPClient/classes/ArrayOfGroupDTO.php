<?php

class ArrayOfGroupDTO
{

    /**
     * @var GroupDTO[] $GroupDTO
     */
    protected $GroupDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return GroupDTO[]
     */
    public function getGroupDTO()
    {
      return $this->GroupDTO;
    }

    /**
     * @param GroupDTO[] $GroupDTO
     * @return ArrayOfGroupDTO
     */
    public function setGroupDTO(array $GroupDTO)
    {
      $this->GroupDTO = $GroupDTO;
      return $this;
    }

}
