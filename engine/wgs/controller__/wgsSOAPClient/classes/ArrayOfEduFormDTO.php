<?php

class ArrayOfEduFormDTO
{

    /**
     * @var EduFormDTO[] $EduFormDTO
     */
    protected $EduFormDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EduFormDTO[]
     */
    public function getEduFormDTO()
    {
      return $this->EduFormDTO;
    }

    /**
     * @param EduFormDTO[] $EduFormDTO
     * @return ArrayOfEduFormDTO
     */
    public function setEduFormDTO(array $EduFormDTO)
    {
      $this->EduFormDTO = $EduFormDTO;
      return $this;
    }

}
