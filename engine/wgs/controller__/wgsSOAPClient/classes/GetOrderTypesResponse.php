<?php

class GetOrderTypesResponse
{

    /**
     * @var ArrayOfOrderTypeDTO $GetOrderTypesResult
     */
    protected $GetOrderTypesResult = null;

    /**
     * @param ArrayOfOrderTypeDTO $GetOrderTypesResult
     */
    public function __construct($GetOrderTypesResult)
    {
      $this->GetOrderTypesResult = $GetOrderTypesResult;
    }

    /**
     * @return ArrayOfOrderTypeDTO
     */
    public function getGetOrderTypesResult()
    {
      return $this->GetOrderTypesResult;
    }

    /**
     * @param ArrayOfOrderTypeDTO $GetOrderTypesResult
     * @return GetOrderTypesResponse
     */
    public function setGetOrderTypesResult($GetOrderTypesResult)
    {
      $this->GetOrderTypesResult = $GetOrderTypesResult;
      return $this;
    }

}
