<?php

class GroupDTO
{

    /**
     * @var int $GroupId
     */
    protected $GroupId = null;

    /**
     * @var string $GroupName
     */
    protected $GroupName = null;

    /**
     * @var int $YearForming
     */
    protected $YearForming = null;

    /**
     * @var int $Course
     */
    protected $Course = null;

    /**
     * @var int $Semestre
     */
    protected $Semestre = null;

    /**
     * @var EducationDTO $Education
     */
    protected $Education = null;

    /**
     * @param int $GroupId
     * @param string $GroupName
     * @param int $YearForming
     * @param int $Course
     * @param int $Semestre
     * @param EducationDTO $Education
     */
    public function __construct($GroupId, $GroupName, $YearForming, $Course, $Semestre, $Education)
    {
      $this->GroupId = $GroupId;
      $this->GroupName = $GroupName;
      $this->YearForming = $YearForming;
      $this->Course = $Course;
      $this->Semestre = $Semestre;
      $this->Education = $Education;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
      return $this->GroupId;
    }

    /**
     * @param int $GroupId
     * @return GroupDTO
     */
    public function setGroupId($GroupId)
    {
      $this->GroupId = $GroupId;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
      return $this->GroupName;
    }

    /**
     * @param string $GroupName
     * @return GroupDTO
     */
    public function setGroupName($GroupName)
    {
      $this->GroupName = $GroupName;
      return $this;
    }

    /**
     * @return int
     */
    public function getYearForming()
    {
      return $this->YearForming;
    }

    /**
     * @param int $YearForming
     * @return GroupDTO
     */
    public function setYearForming($YearForming)
    {
      $this->YearForming = $YearForming;
      return $this;
    }

    /**
     * @return int
     */
    public function getCourse()
    {
      return $this->Course;
    }

    /**
     * @param int $Course
     * @return GroupDTO
     */
    public function setCourse($Course)
    {
      $this->Course = $Course;
      return $this;
    }

    /**
     * @return int
     */
    public function getSemestre()
    {
      return $this->Semestre;
    }

    /**
     * @param int $Semestre
     * @return GroupDTO
     */
    public function setSemestre($Semestre)
    {
      $this->Semestre = $Semestre;
      return $this;
    }

    /**
     * @return EducationDTO
     */
    public function getEducation()
    {
      return $this->Education;
    }

    /**
     * @param EducationDTO $Education
     * @return GroupDTO
     */
    public function setEducation($Education)
    {
      $this->Education = $Education;
      return $this;
    }

}
