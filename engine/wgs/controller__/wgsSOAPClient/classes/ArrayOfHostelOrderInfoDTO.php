<?php

class ArrayOfHostelOrderInfoDTO
{

    /**
     * @var HostelOrderInfoDTO[] $HostelOrderInfoDTO
     */
    protected $HostelOrderInfoDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return HostelOrderInfoDTO[]
     */
    public function getHostelOrderInfoDTO()
    {
      return $this->HostelOrderInfoDTO;
    }

    /**
     * @param HostelOrderInfoDTO[] $HostelOrderInfoDTO
     * @return ArrayOfHostelOrderInfoDTO
     */
    public function setHostelOrderInfoDTO(array $HostelOrderInfoDTO)
    {
      $this->HostelOrderInfoDTO = $HostelOrderInfoDTO;
      return $this;
    }

}
