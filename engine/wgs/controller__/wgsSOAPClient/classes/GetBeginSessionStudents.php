<?php

class GetBeginSessionStudents
{

    /**
     * @var int $groupId
     */
    protected $groupId = null;

    /**
     * @var \DateTime $begin
     */
    protected $begin = null;

    /**
     * @var \DateTime $end
     */
    protected $end = null;

    /**
     * @param int $groupId
     * @param \DateTime $begin
     * @param \DateTime $end
     */
    public function __construct($groupId, \DateTime $begin, \DateTime $end)
    {
      $this->groupId = $groupId;
      $this->begin = $begin->format(\DateTime::ATOM);
      $this->end = $end->format(\DateTime::ATOM);
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
      return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return GetBeginSessionStudents
     */
    public function setGroupId($groupId)
    {
      $this->groupId = $groupId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBegin()
    {
      if ($this->begin == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->begin);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $begin
     * @return GetBeginSessionStudents
     */
    public function setBegin(\DateTime $begin)
    {
      $this->begin = $begin->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
      if ($this->end == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->end);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $end
     * @return GetBeginSessionStudents
     */
    public function setEnd(\DateTime $end)
    {
      $this->end = $end->format(\DateTime::ATOM);
      return $this;
    }

}
