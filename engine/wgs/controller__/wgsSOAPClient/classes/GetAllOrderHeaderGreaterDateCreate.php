<?php

class GetAllOrderHeaderGreaterDateCreate
{

    /**
     * @var \DateTime $date
     */
    protected $date = null;

    /**
     * @var string $internalNumb
     */
    protected $internalNumb = null;

    /**
     * @param \DateTime $date
     * @param string $internalNumb
     */
    public function __construct(\DateTime $date, $internalNumb)
    {
      $this->date = $date->format(\DateTime::ATOM);
      $this->internalNumb = $internalNumb;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
      if ($this->date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $date
     * @return GetAllOrderHeaderGreaterDateCreate
     */
    public function setDate(\DateTime $date)
    {
      $this->date = $date->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getInternalNumb()
    {
      return $this->internalNumb;
    }

    /**
     * @param string $internalNumb
     * @return GetAllOrderHeaderGreaterDateCreate
     */
    public function setInternalNumb($internalNumb)
    {
      $this->internalNumb = $internalNumb;
      return $this;
    }

}
