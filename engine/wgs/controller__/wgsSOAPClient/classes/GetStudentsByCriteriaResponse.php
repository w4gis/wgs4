<?php

class GetStudentsByCriteriaResponse
{

    /**
     * @var ArrayOfStudentDTO $GetStudentsByCriteriaResult
     */
    protected $GetStudentsByCriteriaResult = null;

    /**
     * @param ArrayOfStudentDTO $GetStudentsByCriteriaResult
     */
    public function __construct($GetStudentsByCriteriaResult)
    {
      $this->GetStudentsByCriteriaResult = $GetStudentsByCriteriaResult;
    }

    /**
     * @return ArrayOfStudentDTO
     */
    public function getGetStudentsByCriteriaResult()
    {
      return $this->GetStudentsByCriteriaResult;
    }

    /**
     * @param ArrayOfStudentDTO $GetStudentsByCriteriaResult
     * @return GetStudentsByCriteriaResponse
     */
    public function setGetStudentsByCriteriaResult($GetStudentsByCriteriaResult)
    {
      $this->GetStudentsByCriteriaResult = $GetStudentsByCriteriaResult;
      return $this;
    }

}
