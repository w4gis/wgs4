<?php

class GetAllStudentStatesResponse
{

    /**
     * @var ArrayOfStudentStateDTO $GetAllStudentStatesResult
     */
    protected $GetAllStudentStatesResult = null;

    /**
     * @param ArrayOfStudentStateDTO $GetAllStudentStatesResult
     */
    public function __construct($GetAllStudentStatesResult)
    {
      $this->GetAllStudentStatesResult = $GetAllStudentStatesResult;
    }

    /**
     * @return ArrayOfStudentStateDTO
     */
    public function getGetAllStudentStatesResult()
    {
      return $this->GetAllStudentStatesResult;
    }

    /**
     * @param ArrayOfStudentStateDTO $GetAllStudentStatesResult
     * @return GetAllStudentStatesResponse
     */
    public function setGetAllStudentStatesResult($GetAllStudentStatesResult)
    {
      $this->GetAllStudentStatesResult = $GetAllStudentStatesResult;
      return $this;
    }

}
