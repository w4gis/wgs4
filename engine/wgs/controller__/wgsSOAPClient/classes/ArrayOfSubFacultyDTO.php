<?php

class ArrayOfSubFacultyDTO
{

    /**
     * @var SubFacultyDTO[] $SubFacultyDTO
     */
    protected $SubFacultyDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SubFacultyDTO[]
     */
    public function getSubFacultyDTO()
    {
      return $this->SubFacultyDTO;
    }

    /**
     * @param SubFacultyDTO[] $SubFacultyDTO
     * @return ArrayOfSubFacultyDTO
     */
    public function setSubFacultyDTO(array $SubFacultyDTO)
    {
      $this->SubFacultyDTO = $SubFacultyDTO;
      return $this;
    }

}
