<?php

class AddressDTO
{

    /**
     * @var int $id
     */
    protected $id = null;

    /**
     * @var string $value
     */
    protected $value = null;

    /**
     * @var string $shortName
     */
    protected $shortName = null;

    /**
     * @var AddressLevelDTO $AddressLevel
     */
    protected $AddressLevel = null;

    /**
     * @var AddressPrefixDTO $AddressPrefix
     */
    protected $AddressPrefix = null;

    /**
     * @var AddressDTO $AddressParent
     */
    protected $AddressParent = null;

    /**
     * @param int $id
     * @param string $value
     * @param string $shortName
     * @param AddressLevelDTO $AddressLevel
     * @param AddressPrefixDTO $AddressPrefix
     * @param AddressDTO $AddressParent
     */
    public function __construct($id, $value, $shortName, $AddressLevel, $AddressPrefix, $AddressParent)
    {
      $this->id = $id;
      $this->value = $value;
      $this->shortName = $shortName;
      $this->AddressLevel = $AddressLevel;
      $this->AddressPrefix = $AddressPrefix;
      $this->AddressParent = $AddressParent;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param int $id
     * @return AddressDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return AddressDTO
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return AddressDTO
     */
    public function setShortName($shortName)
    {
      $this->shortName = $shortName;
      return $this;
    }

    /**
     * @return AddressLevelDTO
     */
    public function getAddressLevel()
    {
      return $this->AddressLevel;
    }

    /**
     * @param AddressLevelDTO $AddressLevel
     * @return AddressDTO
     */
    public function setAddressLevel($AddressLevel)
    {
      $this->AddressLevel = $AddressLevel;
      return $this;
    }

    /**
     * @return AddressPrefixDTO
     */
    public function getAddressPrefix()
    {
      return $this->AddressPrefix;
    }

    /**
     * @param AddressPrefixDTO $AddressPrefix
     * @return AddressDTO
     */
    public function setAddressPrefix($AddressPrefix)
    {
      $this->AddressPrefix = $AddressPrefix;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getAddressParent()
    {
      return $this->AddressParent;
    }

    /**
     * @param AddressDTO $AddressParent
     * @return AddressDTO
     */
    public function setAddressParent($AddressParent)
    {
      $this->AddressParent = $AddressParent;
      return $this;
    }

}
