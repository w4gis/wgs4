<?php

class HostelStudentDTO
{

    /**
     * @var int $StudentHosteId
     */
    protected $StudentHosteId = null;

    /**
     * @var HostelDTO $Hostel
     */
    protected $Hostel = null;

    /**
     * @var int $RoomNumber
     */
    protected $RoomNumber = null;

    /**
     * @var \DateTime $StartDate
     */
    protected $StartDate = null;

    /**
     * @var \DateTime $EndDate
     */
    protected $EndDate = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @param int $StudentHosteId
     * @param HostelDTO $Hostel
     * @param int $RoomNumber
     * @param \DateTime $StartDate
     * @param \DateTime $EndDate
     * @param int $StudyId
     */
    public function __construct($StudentHosteId, $Hostel, $RoomNumber, \DateTime $StartDate, \DateTime $EndDate, $StudyId)
    {
      $this->StudentHosteId = $StudentHosteId;
      $this->Hostel = $Hostel;
      $this->RoomNumber = $RoomNumber;
      $this->StartDate = $StartDate->format(\DateTime::ATOM);
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
      $this->StudyId = $StudyId;
    }

    /**
     * @return int
     */
    public function getStudentHosteId()
    {
      return $this->StudentHosteId;
    }

    /**
     * @param int $StudentHosteId
     * @return HostelStudentDTO
     */
    public function setStudentHosteId($StudentHosteId)
    {
      $this->StudentHosteId = $StudentHosteId;
      return $this;
    }

    /**
     * @return HostelDTO
     */
    public function getHostel()
    {
      return $this->Hostel;
    }

    /**
     * @param HostelDTO $Hostel
     * @return HostelStudentDTO
     */
    public function setHostel($Hostel)
    {
      $this->Hostel = $Hostel;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoomNumber()
    {
      return $this->RoomNumber;
    }

    /**
     * @param int $RoomNumber
     * @return HostelStudentDTO
     */
    public function setRoomNumber($RoomNumber)
    {
      $this->RoomNumber = $RoomNumber;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
      if ($this->StartDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->StartDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $StartDate
     * @return HostelStudentDTO
     */
    public function setStartDate(\DateTime $StartDate)
    {
      $this->StartDate = $StartDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
      if ($this->EndDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EndDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EndDate
     * @return HostelStudentDTO
     */
    public function setEndDate(\DateTime $EndDate)
    {
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return HostelStudentDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

}
