<?php

class FinishScholarshipOrderByOrderHeaderId
{

    /**
     * @var int $orderHeaderId
     */
    protected $orderHeaderId = null;

    /**
     * @var \DateTime $order_date
     */
    protected $order_date = null;

    /**
     * @var string $numb
     */
    protected $numb = null;

    /**
     * @param int $orderHeaderId
     * @param \DateTime $order_date
     * @param string $numb
     */
    public function __construct($orderHeaderId, \DateTime $order_date, $numb)
    {
      $this->orderHeaderId = $orderHeaderId;
      $this->order_date = $order_date->format(\DateTime::ATOM);
      $this->numb = $numb;
    }

    /**
     * @return int
     */
    public function getOrderHeaderId()
    {
      return $this->orderHeaderId;
    }

    /**
     * @param int $orderHeaderId
     * @return FinishScholarshipOrderByOrderHeaderId
     */
    public function setOrderHeaderId($orderHeaderId)
    {
      $this->orderHeaderId = $orderHeaderId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrder_date()
    {
      if ($this->order_date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->order_date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $order_date
     * @return FinishScholarshipOrderByOrderHeaderId
     */
    public function setOrder_date(\DateTime $order_date)
    {
      $this->order_date = $order_date->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getNumb()
    {
      return $this->numb;
    }

    /**
     * @param string $numb
     * @return FinishScholarshipOrderByOrderHeaderId
     */
    public function setNumb($numb)
    {
      $this->numb = $numb;
      return $this;
    }

}
