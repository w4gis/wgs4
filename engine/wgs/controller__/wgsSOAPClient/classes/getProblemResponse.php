<?php

class getProblemResponse
{

    /**
     * @var boolean $getProblemResult
     */
    protected $getProblemResult = null;

    /**
     * @param boolean $getProblemResult
     */
    public function __construct($getProblemResult)
    {
      $this->getProblemResult = $getProblemResult;
    }

    /**
     * @return boolean
     */
    public function getGetProblemResult()
    {
      return $this->getProblemResult;
    }

    /**
     * @param boolean $getProblemResult
     * @return getProblemResponse
     */
    public function setGetProblemResult($getProblemResult)
    {
      $this->getProblemResult = $getProblemResult;
      return $this;
    }

}
