<?php

class GetAllPrivelegesForPersona
{

    /**
     * @var int $privelegePersonalId
     */
    protected $privelegePersonalId = null;

    /**
     * @param int $privelegePersonalId
     */
    public function __construct($privelegePersonalId)
    {
      $this->privelegePersonalId = $privelegePersonalId;
    }

    /**
     * @return int
     */
    public function getPrivelegePersonalId()
    {
      return $this->privelegePersonalId;
    }

    /**
     * @param int $privelegePersonalId
     * @return GetAllPrivelegesForPersona
     */
    public function setPrivelegePersonalId($privelegePersonalId)
    {
      $this->privelegePersonalId = $privelegePersonalId;
      return $this;
    }

}
