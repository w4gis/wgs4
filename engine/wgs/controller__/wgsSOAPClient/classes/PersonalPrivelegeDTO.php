<?php

class PersonalPrivelegeDTO
{

    /**
     * @var PrivelegeDTO $Privelege
     */
    protected $Privelege = null;

    /**
     * @var int $PersonaId
     */
    protected $PersonaId = null;

    /**
     * @var \DateTime $BeginDate
     */
    protected $BeginDate = null;

    /**
     * @var \DateTime $EndDate
     */
    protected $EndDate = null;

    /**
     * @param PrivelegeDTO $Privelege
     * @param int $PersonaId
     * @param \DateTime $BeginDate
     * @param \DateTime $EndDate
     */
    public function __construct($Privelege, $PersonaId, \DateTime $BeginDate, \DateTime $EndDate)
    {
      $this->Privelege = $Privelege;
      $this->PersonaId = $PersonaId;
      $this->BeginDate = $BeginDate->format(\DateTime::ATOM);
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
    }

    /**
     * @return PrivelegeDTO
     */
    public function getPrivelege()
    {
      return $this->Privelege;
    }

    /**
     * @param PrivelegeDTO $Privelege
     * @return PersonalPrivelegeDTO
     */
    public function setPrivelege($Privelege)
    {
      $this->Privelege = $Privelege;
      return $this;
    }

    /**
     * @return int
     */
    public function getPersonaId()
    {
      return $this->PersonaId;
    }

    /**
     * @param int $PersonaId
     * @return PersonalPrivelegeDTO
     */
    public function setPersonaId($PersonaId)
    {
      $this->PersonaId = $PersonaId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginDate()
    {
      if ($this->BeginDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->BeginDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $BeginDate
     * @return PersonalPrivelegeDTO
     */
    public function setBeginDate(\DateTime $BeginDate)
    {
      $this->BeginDate = $BeginDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
      if ($this->EndDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EndDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EndDate
     * @return PersonalPrivelegeDTO
     */
    public function setEndDate(\DateTime $EndDate)
    {
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
      return $this;
    }

}
