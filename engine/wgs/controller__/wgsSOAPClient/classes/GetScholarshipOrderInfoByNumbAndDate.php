<?php

class GetScholarshipOrderInfoByNumbAndDate
{

    /**
     * @var string $numb
     */
    protected $numb = null;

    /**
     * @var \DateTime $date
     */
    protected $date = null;

    /**
     * @param string $numb
     * @param \DateTime $date
     */
    public function __construct($numb, \DateTime $date)
    {
      $this->numb = $numb;
      $this->date = $date->format(\DateTime::ATOM);
    }

    /**
     * @return string
     */
    public function getNumb()
    {
      return $this->numb;
    }

    /**
     * @param string $numb
     * @return GetScholarshipOrderInfoByNumbAndDate
     */
    public function setNumb($numb)
    {
      $this->numb = $numb;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
      if ($this->date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $date
     * @return GetScholarshipOrderInfoByNumbAndDate
     */
    public function setDate(\DateTime $date)
    {
      $this->date = $date->format(\DateTime::ATOM);
      return $this;
    }

}
