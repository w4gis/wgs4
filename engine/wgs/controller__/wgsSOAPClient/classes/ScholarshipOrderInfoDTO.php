<?php

class ScholarshipOrderInfoDTO
{

    /**
     * @var int $reasorId
     */
    protected $reasorId = null;

    /**
     * @var string $reasorName
     */
    protected $reasorName = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var OrderTypeDTO $OrderType
     */
    protected $OrderType = null;

    /**
     * @var float $Summa
     */
    protected $Summa = null;

    /**
     * @var \DateTime $BeginDate
     */
    protected $BeginDate = null;

    /**
     * @var \DateTime $EndDate
     */
    protected $EndDate = null;

    /**
     * @var PrivelegeDTO $Privelege
     */
    protected $Privelege = null;

    /**
     * @var SessionTypeDTO $SessionType
     */
    protected $SessionType = null;

    /**
     * @var SessionEndTypeDTO $SessionEndType
     */
    protected $SessionEndType = null;

    /**
     * @var string $ScholarOrderNumber
     */
    protected $ScholarOrderNumber = null;

    /**
     * @var \DateTime $ScholarOrderDate
     */
    protected $ScholarOrderDate = null;

    /**
     * @var string $Fund
     */
    protected $Fund = null;

    /**
     * @var OrderHeaderDTO $OrderHeader
     */
    protected $OrderHeader = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @param int $reasorId
     * @param string $reasorName
     * @param int $Id
     * @param OrderTypeDTO $OrderType
     * @param float $Summa
     * @param \DateTime $BeginDate
     * @param \DateTime $EndDate
     * @param PrivelegeDTO $Privelege
     * @param SessionTypeDTO $SessionType
     * @param SessionEndTypeDTO $SessionEndType
     * @param string $ScholarOrderNumber
     * @param \DateTime $ScholarOrderDate
     * @param string $Fund
     * @param OrderHeaderDTO $OrderHeader
     * @param int $StudyId
     */
    public function __construct($reasorId, $reasorName, $Id, $OrderType, $Summa, \DateTime $BeginDate, \DateTime $EndDate, $Privelege, $SessionType, $SessionEndType, $ScholarOrderNumber, \DateTime $ScholarOrderDate, $Fund, $OrderHeader, $StudyId)
    {
      $this->reasorId = $reasorId;
      $this->reasorName = $reasorName;
      $this->Id = $Id;
      $this->OrderType = $OrderType;
      $this->Summa = $Summa;
      $this->BeginDate = $BeginDate->format(\DateTime::ATOM);
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
      $this->Privelege = $Privelege;
      $this->SessionType = $SessionType;
      $this->SessionEndType = $SessionEndType;
      $this->ScholarOrderNumber = $ScholarOrderNumber;
      $this->ScholarOrderDate = $ScholarOrderDate->format(\DateTime::ATOM);
      $this->Fund = $Fund;
      $this->OrderHeader = $OrderHeader;
      $this->StudyId = $StudyId;
    }

    /**
     * @return int
     */
    public function getReasorId()
    {
      return $this->reasorId;
    }

    /**
     * @param int $reasorId
     * @return ScholarshipOrderInfoDTO
     */
    public function setReasorId($reasorId)
    {
      $this->reasorId = $reasorId;
      return $this;
    }

    /**
     * @return string
     */
    public function getReasorName()
    {
      return $this->reasorName;
    }

    /**
     * @param string $reasorName
     * @return ScholarshipOrderInfoDTO
     */
    public function setReasorName($reasorName)
    {
      $this->reasorName = $reasorName;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return ScholarshipOrderInfoDTO
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return OrderTypeDTO
     */
    public function getOrderType()
    {
      return $this->OrderType;
    }

    /**
     * @param OrderTypeDTO $OrderType
     * @return ScholarshipOrderInfoDTO
     */
    public function setOrderType($OrderType)
    {
      $this->OrderType = $OrderType;
      return $this;
    }

    /**
     * @return float
     */
    public function getSumma()
    {
      return $this->Summa;
    }

    /**
     * @param float $Summa
     * @return ScholarshipOrderInfoDTO
     */
    public function setSumma($Summa)
    {
      $this->Summa = $Summa;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginDate()
    {
      if ($this->BeginDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->BeginDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $BeginDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setBeginDate(\DateTime $BeginDate)
    {
      $this->BeginDate = $BeginDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
      if ($this->EndDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EndDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EndDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setEndDate(\DateTime $EndDate)
    {
      $this->EndDate = $EndDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return PrivelegeDTO
     */
    public function getPrivelege()
    {
      return $this->Privelege;
    }

    /**
     * @param PrivelegeDTO $Privelege
     * @return ScholarshipOrderInfoDTO
     */
    public function setPrivelege($Privelege)
    {
      $this->Privelege = $Privelege;
      return $this;
    }

    /**
     * @return SessionTypeDTO
     */
    public function getSessionType()
    {
      return $this->SessionType;
    }

    /**
     * @param SessionTypeDTO $SessionType
     * @return ScholarshipOrderInfoDTO
     */
    public function setSessionType($SessionType)
    {
      $this->SessionType = $SessionType;
      return $this;
    }

    /**
     * @return SessionEndTypeDTO
     */
    public function getSessionEndType()
    {
      return $this->SessionEndType;
    }

    /**
     * @param SessionEndTypeDTO $SessionEndType
     * @return ScholarshipOrderInfoDTO
     */
    public function setSessionEndType($SessionEndType)
    {
      $this->SessionEndType = $SessionEndType;
      return $this;
    }

    /**
     * @return string
     */
    public function getScholarOrderNumber()
    {
      return $this->ScholarOrderNumber;
    }

    /**
     * @param string $ScholarOrderNumber
     * @return ScholarshipOrderInfoDTO
     */
    public function setScholarOrderNumber($ScholarOrderNumber)
    {
      $this->ScholarOrderNumber = $ScholarOrderNumber;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScholarOrderDate()
    {
      if ($this->ScholarOrderDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScholarOrderDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScholarOrderDate
     * @return ScholarshipOrderInfoDTO
     */
    public function setScholarOrderDate(\DateTime $ScholarOrderDate)
    {
      $this->ScholarOrderDate = $ScholarOrderDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getFund()
    {
      return $this->Fund;
    }

    /**
     * @param string $Fund
     * @return ScholarshipOrderInfoDTO
     */
    public function setFund($Fund)
    {
      $this->Fund = $Fund;
      return $this;
    }

    /**
     * @return OrderHeaderDTO
     */
    public function getOrderHeader()
    {
      return $this->OrderHeader;
    }

    /**
     * @param OrderHeaderDTO $OrderHeader
     * @return ScholarshipOrderInfoDTO
     */
    public function setOrderHeader($OrderHeader)
    {
      $this->OrderHeader = $OrderHeader;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return ScholarshipOrderInfoDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

}
