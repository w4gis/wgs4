<?php

class FinishScholarshipOrderByOrderHeaderIdResponse
{

    /**
     * @var boolean $FinishScholarshipOrderByOrderHeaderIdResult
     */
    protected $FinishScholarshipOrderByOrderHeaderIdResult = null;

    /**
     * @param boolean $FinishScholarshipOrderByOrderHeaderIdResult
     */
    public function __construct($FinishScholarshipOrderByOrderHeaderIdResult)
    {
      $this->FinishScholarshipOrderByOrderHeaderIdResult = $FinishScholarshipOrderByOrderHeaderIdResult;
    }

    /**
     * @return boolean
     */
    public function getFinishScholarshipOrderByOrderHeaderIdResult()
    {
      return $this->FinishScholarshipOrderByOrderHeaderIdResult;
    }

    /**
     * @param boolean $FinishScholarshipOrderByOrderHeaderIdResult
     * @return FinishScholarshipOrderByOrderHeaderIdResponse
     */
    public function setFinishScholarshipOrderByOrderHeaderIdResult($FinishScholarshipOrderByOrderHeaderIdResult)
    {
      $this->FinishScholarshipOrderByOrderHeaderIdResult = $FinishScholarshipOrderByOrderHeaderIdResult;
      return $this;
    }

}
