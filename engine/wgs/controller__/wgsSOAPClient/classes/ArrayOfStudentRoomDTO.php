<?php

class ArrayOfStudentRoomDTO
{

    /**
     * @var StudentRoomDTO[] $StudentRoomDTO
     */
    protected $StudentRoomDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StudentRoomDTO[]
     */
    public function getStudentRoomDTO()
    {
      return $this->StudentRoomDTO;
    }

    /**
     * @param StudentRoomDTO[] $StudentRoomDTO
     * @return ArrayOfStudentRoomDTO
     */
    public function setStudentRoomDTO(array $StudentRoomDTO)
    {
      $this->StudentRoomDTO = $StudentRoomDTO;
      return $this;
    }

}
