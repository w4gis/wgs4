<?php

class GetStudentHostels
{

    /**
     * @var int $studyHostelRequestId
     */
    protected $studyHostelRequestId = null;

    /**
     * @param int $studyHostelRequestId
     */
    public function __construct($studyHostelRequestId)
    {
      $this->studyHostelRequestId = $studyHostelRequestId;
    }

    /**
     * @return int
     */
    public function getStudyHostelRequestId()
    {
      return $this->studyHostelRequestId;
    }

    /**
     * @param int $studyHostelRequestId
     * @return GetStudentHostels
     */
    public function setStudyHostelRequestId($studyHostelRequestId)
    {
      $this->studyHostelRequestId = $studyHostelRequestId;
      return $this;
    }

}
