<?php

class ArrayOfStudentStateDTO
{

    /**
     * @var StudentStateDTO[] $StudentStateDTO
     */
    protected $StudentStateDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StudentStateDTO[]
     */
    public function getStudentStateDTO()
    {
      return $this->StudentStateDTO;
    }

    /**
     * @param StudentStateDTO[] $StudentStateDTO
     * @return ArrayOfStudentStateDTO
     */
    public function setStudentStateDTO(array $StudentStateDTO)
    {
      $this->StudentStateDTO = $StudentStateDTO;
      return $this;
    }

}
