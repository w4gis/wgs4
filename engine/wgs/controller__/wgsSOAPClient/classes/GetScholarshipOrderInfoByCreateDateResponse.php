<?php

class GetScholarshipOrderInfoByCreateDateResponse
{

    /**
     * @var ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCreateDateResult
     */
    protected $GetScholarshipOrderInfoByCreateDateResult = null;

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCreateDateResult
     */
    public function __construct($GetScholarshipOrderInfoByCreateDateResult)
    {
      $this->GetScholarshipOrderInfoByCreateDateResult = $GetScholarshipOrderInfoByCreateDateResult;
    }

    /**
     * @return ArrayOfScholarshipOrderInfoDTO
     */
    public function getGetScholarshipOrderInfoByCreateDateResult()
    {
      return $this->GetScholarshipOrderInfoByCreateDateResult;
    }

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderInfoByCreateDateResult
     * @return GetScholarshipOrderInfoByCreateDateResponse
     */
    public function setGetScholarshipOrderInfoByCreateDateResult($GetScholarshipOrderInfoByCreateDateResult)
    {
      $this->GetScholarshipOrderInfoByCreateDateResult = $GetScholarshipOrderInfoByCreateDateResult;
      return $this;
    }

}
