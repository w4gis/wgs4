<?php

class GetHostelOrderResponse
{

    /**
     * @var ArrayOfHostelOrderInfoDTO $GetHostelOrderResult
     */
    protected $GetHostelOrderResult = null;

    /**
     * @param ArrayOfHostelOrderInfoDTO $GetHostelOrderResult
     */
    public function __construct($GetHostelOrderResult)
    {
      $this->GetHostelOrderResult = $GetHostelOrderResult;
    }

    /**
     * @return ArrayOfHostelOrderInfoDTO
     */
    public function getGetHostelOrderResult()
    {
      return $this->GetHostelOrderResult;
    }

    /**
     * @param ArrayOfHostelOrderInfoDTO $GetHostelOrderResult
     * @return GetHostelOrderResponse
     */
    public function setGetHostelOrderResult($GetHostelOrderResult)
    {
      $this->GetHostelOrderResult = $GetHostelOrderResult;
      return $this;
    }

}
