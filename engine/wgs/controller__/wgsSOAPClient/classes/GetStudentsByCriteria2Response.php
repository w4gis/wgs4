<?php

class GetStudentsByCriteria2Response
{

    /**
     * @var ArrayOfStudentDTO $GetStudentsByCriteria2Result
     */
    protected $GetStudentsByCriteria2Result = null;

    /**
     * @param ArrayOfStudentDTO $GetStudentsByCriteria2Result
     */
    public function __construct($GetStudentsByCriteria2Result)
    {
      $this->GetStudentsByCriteria2Result = $GetStudentsByCriteria2Result;
    }

    /**
     * @return ArrayOfStudentDTO
     */
    public function getGetStudentsByCriteria2Result()
    {
      return $this->GetStudentsByCriteria2Result;
    }

    /**
     * @param ArrayOfStudentDTO $GetStudentsByCriteria2Result
     * @return GetStudentsByCriteria2Response
     */
    public function setGetStudentsByCriteria2Result($GetStudentsByCriteria2Result)
    {
      $this->GetStudentsByCriteria2Result = $GetStudentsByCriteria2Result;
      return $this;
    }

}
