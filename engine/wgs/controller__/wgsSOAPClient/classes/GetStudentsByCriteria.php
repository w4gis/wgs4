<?php

class GetStudentsByCriteria
{

    /**
     * @var StudentRequestDTO $studentCriteria
     */
    protected $studentCriteria = null;

    /**
     * @param StudentRequestDTO $studentCriteria
     */
    public function __construct($studentCriteria)
    {
      $this->studentCriteria = $studentCriteria;
    }

    /**
     * @return StudentRequestDTO
     */
    public function getStudentCriteria()
    {
      return $this->studentCriteria;
    }

    /**
     * @param StudentRequestDTO $studentCriteria
     * @return GetStudentsByCriteria
     */
    public function setStudentCriteria($studentCriteria)
    {
      $this->studentCriteria = $studentCriteria;
      return $this;
    }

}
