<?php

class GetOrderByStudentsOrderIdResponse
{

    /**
     * @var OrderDTO $GetOrderByStudentsOrderIdResult
     */
    protected $GetOrderByStudentsOrderIdResult = null;

    /**
     * @param OrderDTO $GetOrderByStudentsOrderIdResult
     */
    public function __construct($GetOrderByStudentsOrderIdResult)
    {
      $this->GetOrderByStudentsOrderIdResult = $GetOrderByStudentsOrderIdResult;
    }

    /**
     * @return OrderDTO
     */
    public function getGetOrderByStudentsOrderIdResult()
    {
      return $this->GetOrderByStudentsOrderIdResult;
    }

    /**
     * @param OrderDTO $GetOrderByStudentsOrderIdResult
     * @return GetOrderByStudentsOrderIdResponse
     */
    public function setGetOrderByStudentsOrderIdResult($GetOrderByStudentsOrderIdResult)
    {
      $this->GetOrderByStudentsOrderIdResult = $GetOrderByStudentsOrderIdResult;
      return $this;
    }

}
