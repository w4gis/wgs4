<?php

class SemestrStudyDTO
{

    /**
     * @var float $PersonId
     */
    protected $PersonId = null;

    /**
     * @var float $StudyId
     */
    protected $StudyId = null;

    /**
     * @var \DateTime $ActivateDate
     */
    protected $ActivateDate = null;

    /**
     * @var \DateTime $DisactivateDate
     */
    protected $DisactivateDate = null;

    /**
     * @var GroupDTO $Group
     */
    protected $Group = null;

    /**
     * @var float $GroupID
     */
    protected $GroupID = null;

    /**
     * @var float $GroupGPEID
     */
    protected $GroupGPEID = null;

    /**
     * @var GroupDTO $GroupGPE
     */
    protected $GroupGPE = null;

    /**
     * @var StudentStateDTO $State
     */
    protected $State = null;

    /**
     * @param float $PersonId
     * @param float $StudyId
     * @param \DateTime $ActivateDate
     * @param \DateTime $DisactivateDate
     * @param GroupDTO $Group
     * @param float $GroupID
     * @param float $GroupGPEID
     * @param GroupDTO $GroupGPE
     * @param StudentStateDTO $State
     */
    public function __construct($PersonId, $StudyId, \DateTime $ActivateDate, \DateTime $DisactivateDate, $Group, $GroupID, $GroupGPEID, $GroupGPE, $State)
    {
      $this->PersonId = $PersonId;
      $this->StudyId = $StudyId;
      $this->ActivateDate = $ActivateDate->format(\DateTime::ATOM);
      $this->DisactivateDate = $DisactivateDate->format(\DateTime::ATOM);
      $this->Group = $Group;
      $this->GroupID = $GroupID;
      $this->GroupGPEID = $GroupGPEID;
      $this->GroupGPE = $GroupGPE;
      $this->State = $State;
    }

    /**
     * @return float
     */
    public function getPersonId()
    {
      return $this->PersonId;
    }

    /**
     * @param float $PersonId
     * @return SemestrStudyDTO
     */
    public function setPersonId($PersonId)
    {
      $this->PersonId = $PersonId;
      return $this;
    }

    /**
     * @return float
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param float $StudyId
     * @return SemestrStudyDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActivateDate()
    {
      if ($this->ActivateDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ActivateDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ActivateDate
     * @return SemestrStudyDTO
     */
    public function setActivateDate(\DateTime $ActivateDate)
    {
      $this->ActivateDate = $ActivateDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDisactivateDate()
    {
      if ($this->DisactivateDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DisactivateDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DisactivateDate
     * @return SemestrStudyDTO
     */
    public function setDisactivateDate(\DateTime $DisactivateDate)
    {
      $this->DisactivateDate = $DisactivateDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return GroupDTO
     */
    public function getGroup()
    {
      return $this->Group;
    }

    /**
     * @param GroupDTO $Group
     * @return SemestrStudyDTO
     */
    public function setGroup($Group)
    {
      $this->Group = $Group;
      return $this;
    }

    /**
     * @return float
     */
    public function getGroupID()
    {
      return $this->GroupID;
    }

    /**
     * @param float $GroupID
     * @return SemestrStudyDTO
     */
    public function setGroupID($GroupID)
    {
      $this->GroupID = $GroupID;
      return $this;
    }

    /**
     * @return float
     */
    public function getGroupGPEID()
    {
      return $this->GroupGPEID;
    }

    /**
     * @param float $GroupGPEID
     * @return SemestrStudyDTO
     */
    public function setGroupGPEID($GroupGPEID)
    {
      $this->GroupGPEID = $GroupGPEID;
      return $this;
    }

    /**
     * @return GroupDTO
     */
    public function getGroupGPE()
    {
      return $this->GroupGPE;
    }

    /**
     * @param GroupDTO $GroupGPE
     * @return SemestrStudyDTO
     */
    public function setGroupGPE($GroupGPE)
    {
      $this->GroupGPE = $GroupGPE;
      return $this;
    }

    /**
     * @return StudentStateDTO
     */
    public function getState()
    {
      return $this->State;
    }

    /**
     * @param StudentStateDTO $State
     * @return SemestrStudyDTO
     */
    public function setState($State)
    {
      $this->State = $State;
      return $this;
    }

}
