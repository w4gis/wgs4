<?php

class GetScholarshipOrderByOrderHeaderIdResponse
{

    /**
     * @var ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderByOrderHeaderIdResult
     */
    protected $GetScholarshipOrderByOrderHeaderIdResult = null;

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderByOrderHeaderIdResult
     */
    public function __construct($GetScholarshipOrderByOrderHeaderIdResult)
    {
      $this->GetScholarshipOrderByOrderHeaderIdResult = $GetScholarshipOrderByOrderHeaderIdResult;
    }

    /**
     * @return ArrayOfScholarshipOrderInfoDTO
     */
    public function getGetScholarshipOrderByOrderHeaderIdResult()
    {
      return $this->GetScholarshipOrderByOrderHeaderIdResult;
    }

    /**
     * @param ArrayOfScholarshipOrderInfoDTO $GetScholarshipOrderByOrderHeaderIdResult
     * @return GetScholarshipOrderByOrderHeaderIdResponse
     */
    public function setGetScholarshipOrderByOrderHeaderIdResult($GetScholarshipOrderByOrderHeaderIdResult)
    {
      $this->GetScholarshipOrderByOrderHeaderIdResult = $GetScholarshipOrderByOrderHeaderIdResult;
      return $this;
    }

}
