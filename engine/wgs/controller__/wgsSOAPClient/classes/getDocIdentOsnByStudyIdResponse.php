<?php

class getDocIdentOsnByStudyIdResponse
{

    /**
     * @var ArrayOfDocIdentDTO $getDocIdentOsnByStudyIdResult
     */
    protected $getDocIdentOsnByStudyIdResult = null;

    /**
     * @param ArrayOfDocIdentDTO $getDocIdentOsnByStudyIdResult
     */
    public function __construct($getDocIdentOsnByStudyIdResult)
    {
      $this->getDocIdentOsnByStudyIdResult = $getDocIdentOsnByStudyIdResult;
    }

    /**
     * @return ArrayOfDocIdentDTO
     */
    public function getGetDocIdentOsnByStudyIdResult()
    {
      return $this->getDocIdentOsnByStudyIdResult;
    }

    /**
     * @param ArrayOfDocIdentDTO $getDocIdentOsnByStudyIdResult
     * @return getDocIdentOsnByStudyIdResponse
     */
    public function setGetDocIdentOsnByStudyIdResult($getDocIdentOsnByStudyIdResult)
    {
      $this->getDocIdentOsnByStudyIdResult = $getDocIdentOsnByStudyIdResult;
      return $this;
    }

}
