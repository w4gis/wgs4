<?php

class GetAllEducationsResponse
{

    /**
     * @var ArrayOfEducationDTO $GetAllEducationsResult
     */
    protected $GetAllEducationsResult = null;

    /**
     * @param ArrayOfEducationDTO $GetAllEducationsResult
     */
    public function __construct($GetAllEducationsResult)
    {
      $this->GetAllEducationsResult = $GetAllEducationsResult;
    }

    /**
     * @return ArrayOfEducationDTO
     */
    public function getGetAllEducationsResult()
    {
      return $this->GetAllEducationsResult;
    }

    /**
     * @param ArrayOfEducationDTO $GetAllEducationsResult
     * @return GetAllEducationsResponse
     */
    public function setGetAllEducationsResult($GetAllEducationsResult)
    {
      $this->GetAllEducationsResult = $GetAllEducationsResult;
      return $this;
    }

}
