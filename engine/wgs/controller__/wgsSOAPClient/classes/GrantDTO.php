<?php

class GrantDTO
{

    /**
     * @var guid $id
     */
    protected $id = null;

    /**
     * @var int $studentId
     */
    protected $studentId = null;

    /**
     * @var int $typeId
     */
    protected $typeId = null;

    /**
     * @var \DateTime $dateBegin
     */
    protected $dateBegin = null;

    /**
     * @var \DateTime $dateEnd
     */
    protected $dateEnd = null;

    /**
     * @var \DateTime $dateNow
     */
    protected $dateNow = null;

    /**
     * @var int $protNumb
     */
    protected $protNumb = null;

    /**
     * @param guid $id
     * @param int $studentId
     * @param int $typeId
     * @param \DateTime $dateBegin
     * @param \DateTime $dateEnd
     * @param \DateTime $dateNow
     * @param int $protNumb
     */
    public function __construct($id, $studentId, $typeId, \DateTime $dateBegin, \DateTime $dateEnd, \DateTime $dateNow, $protNumb)
    {
      $this->id = $id;
      $this->studentId = $studentId;
      $this->typeId = $typeId;
      $this->dateBegin = $dateBegin->format(\DateTime::ATOM);
      $this->dateEnd = $dateEnd->format(\DateTime::ATOM);
      $this->dateNow = $dateNow->format(\DateTime::ATOM);
      $this->protNumb = $protNumb;
    }

    /**
     * @return guid
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param guid $id
     * @return GrantDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudentId()
    {
      return $this->studentId;
    }

    /**
     * @param int $studentId
     * @return GrantDTO
     */
    public function setStudentId($studentId)
    {
      $this->studentId = $studentId;
      return $this;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
      return $this->typeId;
    }

    /**
     * @param int $typeId
     * @return GrantDTO
     */
    public function setTypeId($typeId)
    {
      $this->typeId = $typeId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateBegin()
    {
      if ($this->dateBegin == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dateBegin);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dateBegin
     * @return GrantDTO
     */
    public function setDateBegin(\DateTime $dateBegin)
    {
      $this->dateBegin = $dateBegin->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
      if ($this->dateEnd == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dateEnd);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dateEnd
     * @return GrantDTO
     */
    public function setDateEnd(\DateTime $dateEnd)
    {
      $this->dateEnd = $dateEnd->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateNow()
    {
      if ($this->dateNow == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dateNow);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dateNow
     * @return GrantDTO
     */
    public function setDateNow(\DateTime $dateNow)
    {
      $this->dateNow = $dateNow->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getProtNumb()
    {
      return $this->protNumb;
    }

    /**
     * @param int $protNumb
     * @return GrantDTO
     */
    public function setProtNumb($protNumb)
    {
      $this->protNumb = $protNumb;
      return $this;
    }

}
