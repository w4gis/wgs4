<?php

class StudentRequestDTO
{

    /**
     * @var int $PersonId
     */
    protected $PersonId = null;

    /**
     * @var int $StudyId
     */
    protected $StudyId = null;

    /**
     * @var int $GroupId
     */
    protected $GroupId = null;

    /**
     * @var int $CitizenshipId
     */
    protected $CitizenshipId = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $MiddleName
     */
    protected $MiddleName = null;

    /**
     * @var string $ZachNumber
     */
    protected $ZachNumber = null;

    /**
     * @var string $GroupName
     */
    protected $GroupName = null;

    /**
     * @var int $EduId
     */
    protected $EduId = null;

    /**
     * @var int $EduFormId
     */
    protected $EduFormId = null;

    /**
     * @var int $SubFacultyId
     */
    protected $SubFacultyId = null;

    /**
     * @var int $FacultyId
     */
    protected $FacultyId = null;

    /**
     * @var int $SpecialityId
     */
    protected $SpecialityId = null;

    /**
     * @var int $SpecializationId
     */
    protected $SpecializationId = null;

    /**
     * @var int $SpecialityTypeId
     */
    protected $SpecialityTypeId = null;

    /**
     * @var int $StudentStateId
     */
    protected $StudentStateId = null;

    /**
     * @var int $CourseGroup
     */
    protected $CourseGroup = null;

    /**
     * @param int $PersonId
     * @param int $StudyId
     * @param int $GroupId
     * @param int $CitizenshipId
     * @param string $LastName
     * @param string $FirstName
     * @param string $MiddleName
     * @param string $ZachNumber
     * @param string $GroupName
     * @param int $EduId
     * @param int $EduFormId
     * @param int $SubFacultyId
     * @param int $FacultyId
     * @param int $SpecialityId
     * @param int $SpecializationId
     * @param int $SpecialityTypeId
     * @param int $StudentStateId
     * @param int $CourseGroup
     */
    public function __construct($PersonId, $StudyId, $GroupId, $CitizenshipId, $LastName, $FirstName, $MiddleName, $ZachNumber, $GroupName, $EduId, $EduFormId, $SubFacultyId, $FacultyId, $SpecialityId, $SpecializationId, $SpecialityTypeId, $StudentStateId, $CourseGroup)
    {
      $this->PersonId = $PersonId;
      $this->StudyId = $StudyId;
      $this->GroupId = $GroupId;
      $this->CitizenshipId = $CitizenshipId;
      $this->LastName = $LastName;
      $this->FirstName = $FirstName;
      $this->MiddleName = $MiddleName;
      $this->ZachNumber = $ZachNumber;
      $this->GroupName = $GroupName;
      $this->EduId = $EduId;
      $this->EduFormId = $EduFormId;
      $this->SubFacultyId = $SubFacultyId;
      $this->FacultyId = $FacultyId;
      $this->SpecialityId = $SpecialityId;
      $this->SpecializationId = $SpecializationId;
      $this->SpecialityTypeId = $SpecialityTypeId;
      $this->StudentStateId = $StudentStateId;
      $this->CourseGroup = $CourseGroup;
    }

    /**
     * @return int
     */
    public function getPersonId()
    {
      return $this->PersonId;
    }

    /**
     * @param int $PersonId
     * @return StudentRequestDTO
     */
    public function setPersonId($PersonId)
    {
      $this->PersonId = $PersonId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->StudyId;
    }

    /**
     * @param int $StudyId
     * @return StudentRequestDTO
     */
    public function setStudyId($StudyId)
    {
      $this->StudyId = $StudyId;
      return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
      return $this->GroupId;
    }

    /**
     * @param int $GroupId
     * @return StudentRequestDTO
     */
    public function setGroupId($GroupId)
    {
      $this->GroupId = $GroupId;
      return $this;
    }

    /**
     * @return int
     */
    public function getCitizenshipId()
    {
      return $this->CitizenshipId;
    }

    /**
     * @param int $CitizenshipId
     * @return StudentRequestDTO
     */
    public function setCitizenshipId($CitizenshipId)
    {
      $this->CitizenshipId = $CitizenshipId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return StudentRequestDTO
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return StudentRequestDTO
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return StudentRequestDTO
     */
    public function setMiddleName($MiddleName)
    {
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getZachNumber()
    {
      return $this->ZachNumber;
    }

    /**
     * @param string $ZachNumber
     * @return StudentRequestDTO
     */
    public function setZachNumber($ZachNumber)
    {
      $this->ZachNumber = $ZachNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
      return $this->GroupName;
    }

    /**
     * @param string $GroupName
     * @return StudentRequestDTO
     */
    public function setGroupName($GroupName)
    {
      $this->GroupName = $GroupName;
      return $this;
    }

    /**
     * @return int
     */
    public function getEduId()
    {
      return $this->EduId;
    }

    /**
     * @param int $EduId
     * @return StudentRequestDTO
     */
    public function setEduId($EduId)
    {
      $this->EduId = $EduId;
      return $this;
    }

    /**
     * @return int
     */
    public function getEduFormId()
    {
      return $this->EduFormId;
    }

    /**
     * @param int $EduFormId
     * @return StudentRequestDTO
     */
    public function setEduFormId($EduFormId)
    {
      $this->EduFormId = $EduFormId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSubFacultyId()
    {
      return $this->SubFacultyId;
    }

    /**
     * @param int $SubFacultyId
     * @return StudentRequestDTO
     */
    public function setSubFacultyId($SubFacultyId)
    {
      $this->SubFacultyId = $SubFacultyId;
      return $this;
    }

    /**
     * @return int
     */
    public function getFacultyId()
    {
      return $this->FacultyId;
    }

    /**
     * @param int $FacultyId
     * @return StudentRequestDTO
     */
    public function setFacultyId($FacultyId)
    {
      $this->FacultyId = $FacultyId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpecialityId()
    {
      return $this->SpecialityId;
    }

    /**
     * @param int $SpecialityId
     * @return StudentRequestDTO
     */
    public function setSpecialityId($SpecialityId)
    {
      $this->SpecialityId = $SpecialityId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpecializationId()
    {
      return $this->SpecializationId;
    }

    /**
     * @param int $SpecializationId
     * @return StudentRequestDTO
     */
    public function setSpecializationId($SpecializationId)
    {
      $this->SpecializationId = $SpecializationId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpecialityTypeId()
    {
      return $this->SpecialityTypeId;
    }

    /**
     * @param int $SpecialityTypeId
     * @return StudentRequestDTO
     */
    public function setSpecialityTypeId($SpecialityTypeId)
    {
      $this->SpecialityTypeId = $SpecialityTypeId;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudentStateId()
    {
      return $this->StudentStateId;
    }

    /**
     * @param int $StudentStateId
     * @return StudentRequestDTO
     */
    public function setStudentStateId($StudentStateId)
    {
      $this->StudentStateId = $StudentStateId;
      return $this;
    }

    /**
     * @return int
     */
    public function getCourseGroup()
    {
      return $this->CourseGroup;
    }

    /**
     * @param int $CourseGroup
     * @return StudentRequestDTO
     */
    public function setCourseGroup($CourseGroup)
    {
      $this->CourseGroup = $CourseGroup;
      return $this;
    }

}
