<?php

class getAddressByStudentIdResponse
{

    /**
     * @var ArrayOfStudentAddressDTO $getAddressByStudentIdResult
     */
    protected $getAddressByStudentIdResult = null;

    /**
     * @param ArrayOfStudentAddressDTO $getAddressByStudentIdResult
     */
    public function __construct($getAddressByStudentIdResult)
    {
      $this->getAddressByStudentIdResult = $getAddressByStudentIdResult;
    }

    /**
     * @return ArrayOfStudentAddressDTO
     */
    public function getGetAddressByStudentIdResult()
    {
      return $this->getAddressByStudentIdResult;
    }

    /**
     * @param ArrayOfStudentAddressDTO $getAddressByStudentIdResult
     * @return getAddressByStudentIdResponse
     */
    public function setGetAddressByStudentIdResult($getAddressByStudentIdResult)
    {
      $this->getAddressByStudentIdResult = $getAddressByStudentIdResult;
      return $this;
    }

}
