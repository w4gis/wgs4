<?php

class HostelOrderInfoDTO
{

    /**
     * @var string $prot
     */
    protected $prot = null;

    /**
     * @var int $id
     */
    protected $id = null;

    /**
     * @var int $studyId
     */
    protected $studyId = null;

    /**
     * @var string $studyBaseName
     */
    protected $studyBaseName = null;

    /**
     * @var string $studyFullName
     */
    protected $studyFullName = null;

    /**
     * @var string $studyName
     */
    protected $studyName = null;

    /**
     * @var string $studyOldName
     */
    protected $studyOldName = null;

    /**
     * @var int $studyGroopId
     */
    protected $studyGroopId = null;

    /**
     * @var string $studyGroopName
     */
    protected $studyGroopName = null;

    /**
     * @var string $financing
     */
    protected $financing = null;

    /**
     * @var int $hostelId
     */
    protected $hostelId = null;

    /**
     * @var string $hostelName
     */
    protected $hostelName = null;

    /**
     * @var \DateTime $beginDate
     */
    protected $beginDate = null;

    /**
     * @var \DateTime $endDate
     */
    protected $endDate = null;

    /**
     * @param string $prot
     * @param int $id
     * @param int $studyId
     * @param string $studyBaseName
     * @param string $studyFullName
     * @param string $studyName
     * @param string $studyOldName
     * @param int $studyGroopId
     * @param string $studyGroopName
     * @param string $financing
     * @param int $hostelId
     * @param string $hostelName
     * @param \DateTime $beginDate
     * @param \DateTime $endDate
     */
    public function __construct($prot, $id, $studyId, $studyBaseName, $studyFullName, $studyName, $studyOldName, $studyGroopId, $studyGroopName, $financing, $hostelId, $hostelName, \DateTime $beginDate, \DateTime $endDate)
    {
      $this->prot = $prot;
      $this->id = $id;
      $this->studyId = $studyId;
      $this->studyBaseName = $studyBaseName;
      $this->studyFullName = $studyFullName;
      $this->studyName = $studyName;
      $this->studyOldName = $studyOldName;
      $this->studyGroopId = $studyGroopId;
      $this->studyGroopName = $studyGroopName;
      $this->financing = $financing;
      $this->hostelId = $hostelId;
      $this->hostelName = $hostelName;
      $this->beginDate = $beginDate->format(\DateTime::ATOM);
      $this->endDate = $endDate->format(\DateTime::ATOM);
    }

    /**
     * @return string
     */
    public function getProt()
    {
      return $this->prot;
    }

    /**
     * @param string $prot
     * @return HostelOrderInfoDTO
     */
    public function setProt($prot)
    {
      $this->prot = $prot;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param int $id
     * @return HostelOrderInfoDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyId()
    {
      return $this->studyId;
    }

    /**
     * @param int $studyId
     * @return HostelOrderInfoDTO
     */
    public function setStudyId($studyId)
    {
      $this->studyId = $studyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyBaseName()
    {
      return $this->studyBaseName;
    }

    /**
     * @param string $studyBaseName
     * @return HostelOrderInfoDTO
     */
    public function setStudyBaseName($studyBaseName)
    {
      $this->studyBaseName = $studyBaseName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyFullName()
    {
      return $this->studyFullName;
    }

    /**
     * @param string $studyFullName
     * @return HostelOrderInfoDTO
     */
    public function setStudyFullName($studyFullName)
    {
      $this->studyFullName = $studyFullName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyName()
    {
      return $this->studyName;
    }

    /**
     * @param string $studyName
     * @return HostelOrderInfoDTO
     */
    public function setStudyName($studyName)
    {
      $this->studyName = $studyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyOldName()
    {
      return $this->studyOldName;
    }

    /**
     * @param string $studyOldName
     * @return HostelOrderInfoDTO
     */
    public function setStudyOldName($studyOldName)
    {
      $this->studyOldName = $studyOldName;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudyGroopId()
    {
      return $this->studyGroopId;
    }

    /**
     * @param int $studyGroopId
     * @return HostelOrderInfoDTO
     */
    public function setStudyGroopId($studyGroopId)
    {
      $this->studyGroopId = $studyGroopId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStudyGroopName()
    {
      return $this->studyGroopName;
    }

    /**
     * @param string $studyGroopName
     * @return HostelOrderInfoDTO
     */
    public function setStudyGroopName($studyGroopName)
    {
      $this->studyGroopName = $studyGroopName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFinancing()
    {
      return $this->financing;
    }

    /**
     * @param string $financing
     * @return HostelOrderInfoDTO
     */
    public function setFinancing($financing)
    {
      $this->financing = $financing;
      return $this;
    }

    /**
     * @return int
     */
    public function getHostelId()
    {
      return $this->hostelId;
    }

    /**
     * @param int $hostelId
     * @return HostelOrderInfoDTO
     */
    public function setHostelId($hostelId)
    {
      $this->hostelId = $hostelId;
      return $this;
    }

    /**
     * @return string
     */
    public function getHostelName()
    {
      return $this->hostelName;
    }

    /**
     * @param string $hostelName
     * @return HostelOrderInfoDTO
     */
    public function setHostelName($hostelName)
    {
      $this->hostelName = $hostelName;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBeginDate()
    {
      if ($this->beginDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->beginDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $beginDate
     * @return HostelOrderInfoDTO
     */
    public function setBeginDate(\DateTime $beginDate)
    {
      $this->beginDate = $beginDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
      if ($this->endDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->endDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $endDate
     * @return HostelOrderInfoDTO
     */
    public function setEndDate(\DateTime $endDate)
    {
      $this->endDate = $endDate->format(\DateTime::ATOM);
      return $this;
    }

}
