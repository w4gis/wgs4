<?php

class EduFormDTO
{

    /**
     * @var int $EduFormId
     */
    protected $EduFormId = null;

    /**
     * @var string $EduFormName
     */
    protected $EduFormName = null;

    /**
     * @param int $EduFormId
     * @param string $EduFormName
     */
    public function __construct($EduFormId, $EduFormName)
    {
      $this->EduFormId = $EduFormId;
      $this->EduFormName = $EduFormName;
    }

    /**
     * @return int
     */
    public function getEduFormId()
    {
      return $this->EduFormId;
    }

    /**
     * @param int $EduFormId
     * @return EduFormDTO
     */
    public function setEduFormId($EduFormId)
    {
      $this->EduFormId = $EduFormId;
      return $this;
    }

    /**
     * @return string
     */
    public function getEduFormName()
    {
      return $this->EduFormName;
    }

    /**
     * @param string $EduFormName
     * @return EduFormDTO
     */
    public function setEduFormName($EduFormName)
    {
      $this->EduFormName = $EduFormName;
      return $this;
    }

}
