<?php

class ArrayOfFacultyDTO
{

    /**
     * @var FacultyDTO[] $FacultyDTO
     */
    protected $FacultyDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FacultyDTO[]
     */
    public function getFacultyDTO()
    {
      return $this->FacultyDTO;
    }

    /**
     * @param FacultyDTO[] $FacultyDTO
     * @return ArrayOfFacultyDTO
     */
    public function setFacultyDTO(array $FacultyDTO)
    {
      $this->FacultyDTO = $FacultyDTO;
      return $this;
    }

}
