<?php

class GetStudentHostelsWithExclusions
{

    /**
     * @var int $studyHostelRequestId
     */
    protected $studyHostelRequestId = null;

    /**
     * @param int $studyHostelRequestId
     */
    public function __construct($studyHostelRequestId)
    {
      $this->studyHostelRequestId = $studyHostelRequestId;
    }

    /**
     * @return int
     */
    public function getStudyHostelRequestId()
    {
      return $this->studyHostelRequestId;
    }

    /**
     * @param int $studyHostelRequestId
     * @return GetStudentHostelsWithExclusions
     */
    public function setStudyHostelRequestId($studyHostelRequestId)
    {
      $this->studyHostelRequestId = $studyHostelRequestId;
      return $this;
    }

}
