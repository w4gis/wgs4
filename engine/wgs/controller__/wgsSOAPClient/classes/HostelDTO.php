<?php

class HostelDTO
{

    /**
     * @var int $HostelId
     */
    protected $HostelId = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @param int $HostelId
     * @param string $Name
     */
    public function __construct($HostelId, $Name)
    {
      $this->HostelId = $HostelId;
      $this->Name = $Name;
    }

    /**
     * @return int
     */
    public function getHostelId()
    {
      return $this->HostelId;
    }

    /**
     * @param int $HostelId
     * @return HostelDTO
     */
    public function setHostelId($HostelId)
    {
      $this->HostelId = $HostelId;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return HostelDTO
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
