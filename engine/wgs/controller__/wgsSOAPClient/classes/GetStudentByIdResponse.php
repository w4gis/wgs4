<?php

class GetStudentByIdResponse
{

    /**
     * @var StudentDTO $GetStudentByIdResult
     */
    protected $GetStudentByIdResult = null;

    /**
     * @param StudentDTO $GetStudentByIdResult
     */
    public function __construct($GetStudentByIdResult)
    {
      $this->GetStudentByIdResult = $GetStudentByIdResult;
    }

    /**
     * @return StudentDTO
     */
    public function getGetStudentByIdResult()
    {
      return $this->GetStudentByIdResult;
    }

    /**
     * @param StudentDTO $GetStudentByIdResult
     * @return GetStudentByIdResponse
     */
    public function setGetStudentByIdResult($GetStudentByIdResult)
    {
      $this->GetStudentByIdResult = $GetStudentByIdResult;
      return $this;
    }

}
