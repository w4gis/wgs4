<?php

class UpdateCourse
{

    /**
     * @var NextCourseDTO $next
     */
    protected $next = null;

    /**
     * @param NextCourseDTO $next
     */
    public function __construct($next)
    {
      $this->next = $next;
    }

    /**
     * @return NextCourseDTO
     */
    public function getNext()
    {
      return $this->next;
    }

    /**
     * @param NextCourseDTO $next
     * @return UpdateCourse
     */
    public function setNext($next)
    {
      $this->next = $next;
      return $this;
    }

}
