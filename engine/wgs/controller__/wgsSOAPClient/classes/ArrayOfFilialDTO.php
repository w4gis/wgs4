<?php

class ArrayOfFilialDTO
{

    /**
     * @var FilialDTO[] $FilialDTO
     */
    protected $FilialDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FilialDTO[]
     */
    public function getFilialDTO()
    {
      return $this->FilialDTO;
    }

    /**
     * @param FilialDTO[] $FilialDTO
     * @return ArrayOfFilialDTO
     */
    public function setFilialDTO(array $FilialDTO)
    {
      $this->FilialDTO = $FilialDTO;
      return $this;
    }

}
