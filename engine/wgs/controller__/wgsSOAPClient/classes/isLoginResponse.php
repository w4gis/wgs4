<?php

class isLoginResponse
{

    /**
     * @var boolean $isLoginResult
     */
    protected $isLoginResult = null;

    /**
     * @param boolean $isLoginResult
     */
    public function __construct($isLoginResult)
    {
      $this->isLoginResult = $isLoginResult;
    }

    /**
     * @return boolean
     */
    public function getIsLoginResult()
    {
      return $this->isLoginResult;
    }

    /**
     * @param boolean $isLoginResult
     * @return isLoginResponse
     */
    public function setIsLoginResult($isLoginResult)
    {
      $this->isLoginResult = $isLoginResult;
      return $this;
    }

}
