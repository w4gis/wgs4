<?php

class SendDownDTO
{

    /**
     * @var guid $id
     */
    protected $id = null;

    /**
     * @var int $studentId
     */
    protected $studentId = null;

    /**
     * @var int $sdCol
     */
    protected $sdCol = null;

    /**
     * @var \DateTime $dateNow
     */
    protected $dateNow = null;

    /**
     * @param guid $id
     * @param int $studentId
     * @param int $sdCol
     * @param \DateTime $dateNow
     */
    public function __construct($id, $studentId, $sdCol, \DateTime $dateNow)
    {
      $this->id = $id;
      $this->studentId = $studentId;
      $this->sdCol = $sdCol;
      $this->dateNow = $dateNow->format(\DateTime::ATOM);
    }

    /**
     * @return guid
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param guid $id
     * @return SendDownDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return int
     */
    public function getStudentId()
    {
      return $this->studentId;
    }

    /**
     * @param int $studentId
     * @return SendDownDTO
     */
    public function setStudentId($studentId)
    {
      $this->studentId = $studentId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSdCol()
    {
      return $this->sdCol;
    }

    /**
     * @param int $sdCol
     * @return SendDownDTO
     */
    public function setSdCol($sdCol)
    {
      $this->sdCol = $sdCol;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateNow()
    {
      if ($this->dateNow == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dateNow);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dateNow
     * @return SendDownDTO
     */
    public function setDateNow(\DateTime $dateNow)
    {
      $this->dateNow = $dateNow->format(\DateTime::ATOM);
      return $this;
    }

}
