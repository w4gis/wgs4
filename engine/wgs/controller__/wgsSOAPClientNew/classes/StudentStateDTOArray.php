<?php

class StudentStateDTOArray
{

    /**
     * @var StudentStateDTO[] $item
     */
    protected $item = null;

    /**
     * @param StudentStateDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return StudentStateDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param StudentStateDTO[] $item
     * @return StudentStateDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
