<?php

class AddressPrefixDTO
{

    /**
     * @var string $id
     */
    protected $id = null;

    /**
     * @var string $prefix
     */
    protected $prefix = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param string $id
     * @return AddressPrefixDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
      return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return AddressPrefixDTO
     */
    public function setPrefix($prefix)
    {
      $this->prefix = $prefix;
      return $this;
    }

}
