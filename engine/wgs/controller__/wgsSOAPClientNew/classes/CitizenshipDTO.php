<?php

class CitizenshipDTO
{

    /**
     * @var string $CitizenshipId
     */
    protected $CitizenshipId = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCitizenshipId()
    {
      return $this->CitizenshipId;
    }

    /**
     * @param string $CitizenshipId
     * @return CitizenshipDTO
     */
    public function setCitizenshipId($CitizenshipId)
    {
      $this->CitizenshipId = $CitizenshipId;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return CitizenshipDTO
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return CitizenshipDTO
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

}
