<?php

class FilialDTO
{

    /**
     * @var string $FilialId
     */
    protected $FilialId = null;

    /**
     * @var string $FilialName
     */
    protected $FilialName = null;

    /**
     * @var string $ShortName
     */
    protected $ShortName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getFilialId()
    {
      return $this->FilialId;
    }

    /**
     * @param string $FilialId
     * @return FilialDTO
     */
    public function setFilialId($FilialId)
    {
      $this->FilialId = $FilialId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFilialName()
    {
      return $this->FilialName;
    }

    /**
     * @param string $FilialName
     * @return FilialDTO
     */
    public function setFilialName($FilialName)
    {
      $this->FilialName = $FilialName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
      return $this->ShortName;
    }

    /**
     * @param string $ShortName
     * @return FilialDTO
     */
    public function setShortName($ShortName)
    {
      $this->ShortName = $ShortName;
      return $this;
    }

}
