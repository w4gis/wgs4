<?php

class GroupDTOArray
{

    /**
     * @var GroupDTO[] $item
     */
    protected $item = null;

    /**
     * @param GroupDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return GroupDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param GroupDTO[] $item
     * @return GroupDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
