<?php

class StudentAddressDTO
{

    /**
     * @var string $id
     */
    protected $id = null;

    /**
     * @var string $postString
     */
    protected $postString = null;

    /**
     * @var string $addressIndex
     */
    protected $addressIndex = null;

    /**
     * @var boolean $defaultItem
     */
    protected $defaultItem = null;

    /**
     * @var AddressDTO $Address
     */
    protected $Address = null;

    /**
     * @var AddressTypeDTO $AddressType
     */
    protected $AddressType = null;

    /**
     * @param boolean $defaultItem
     */
    public function __construct($defaultItem)
    {
      $this->defaultItem = $defaultItem;
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param string $id
     * @return StudentAddressDTO
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostString()
    {
      return $this->postString;
    }

    /**
     * @param string $postString
     * @return StudentAddressDTO
     */
    public function setPostString($postString)
    {
      $this->postString = $postString;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddressIndex()
    {
      return $this->addressIndex;
    }

    /**
     * @param string $addressIndex
     * @return StudentAddressDTO
     */
    public function setAddressIndex($addressIndex)
    {
      $this->addressIndex = $addressIndex;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDefaultItem()
    {
      return $this->defaultItem;
    }

    /**
     * @param boolean $defaultItem
     * @return StudentAddressDTO
     */
    public function setDefaultItem($defaultItem)
    {
      $this->defaultItem = $defaultItem;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getAddress()
    {
      return $this->Address;
    }

    /**
     * @param AddressDTO $Address
     * @return StudentAddressDTO
     */
    public function setAddress($Address)
    {
      $this->Address = $Address;
      return $this;
    }

    /**
     * @return AddressTypeDTO
     */
    public function getAddressType()
    {
      return $this->AddressType;
    }

    /**
     * @param AddressTypeDTO $AddressType
     * @return StudentAddressDTO
     */
    public function setAddressType($AddressType)
    {
      $this->AddressType = $AddressType;
      return $this;
    }

}
