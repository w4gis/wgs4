<?php


 function autoload_4275cd29f4a3b3f0aea8228b1c105ef0($class)
{
    $classes = array(
        'LocmanInfoService' => __DIR__ .'/LocmanInfoService.php',
        'EduFormDTO' => __DIR__ .'/EduFormDTO.php',
        'SubFacultyDTO' => __DIR__ .'/SubFacultyDTO.php',
        'FacultyDTO' => __DIR__ .'/FacultyDTO.php',
        'SpecializationDTO' => __DIR__ .'/SpecializationDTO.php',
        'SpecialityDTO' => __DIR__ .'/SpecialityDTO.php',
        'SpecTypeDTO' => __DIR__ .'/SpecTypeDTO.php',
        'EducationDTO' => __DIR__ .'/EducationDTO.php',
        'GroupDTO' => __DIR__ .'/GroupDTO.php',
        'StudentStateDTO' => __DIR__ .'/StudentStateDTO.php',
        'FilialDTO' => __DIR__ .'/FilialDTO.php',
        'StudentDTO' => __DIR__ .'/StudentDTO.php',
        'StudentRequestDTO' => __DIR__ .'/StudentRequestDTO.php',
        'StudentDTOArray' => __DIR__ .'/StudentDTOArray.php',
        'GroupDTOArray' => __DIR__ .'/GroupDTOArray.php',
        'FacultyDTOArray' => __DIR__ .'/FacultyDTOArray.php',
        'SubFacultyDTOArray' => __DIR__ .'/SubFacultyDTOArray.php',
        'EduFormDTOArray' => __DIR__ .'/EduFormDTOArray.php',
        'SpecialityDTOArray' => __DIR__ .'/SpecialityDTOArray.php',
        'HostelDTO' => __DIR__ .'/HostelDTO.php',
        'HostelDTOArray' => __DIR__ .'/HostelDTOArray.php',
        'HostelStudentDTO' => __DIR__ .'/HostelStudentDTO.php',
        'HostelStudentDTOArray' => __DIR__ .'/HostelStudentDTOArray.php',
        'OrderTypeDTO' => __DIR__ .'/OrderTypeDTO.php',
        'OrderTypeDTOArray' => __DIR__ .'/OrderTypeDTOArray.php',
        'PrivelegeDTO' => __DIR__ .'/PrivelegeDTO.php',
        'PrivelegeDTOArray' => __DIR__ .'/PrivelegeDTOArray.php',
        'SessionTypeDTO' => __DIR__ .'/SessionTypeDTO.php',
        'SessionEndTypeDTO' => __DIR__ .'/SessionEndTypeDTO.php',
        'SubDivisionDTO' => __DIR__ .'/SubDivisionDTO.php',
        'OrderHeaderDTO' => __DIR__ .'/OrderHeaderDTO.php',
        'ScholarshipOrderInfoDTO' => __DIR__ .'/ScholarshipOrderInfoDTO.php',
        'ScholarshipOrderInfoDTOArray' => __DIR__ .'/ScholarshipOrderInfoDTOArray.php',
        'OrderHeaderDTOArray' => __DIR__ .'/OrderHeaderDTOArray.php',
        'AddressLevelDTO' => __DIR__ .'/AddressLevelDTO.php',
        'AddressPrefixDTO' => __DIR__ .'/AddressPrefixDTO.php',
        'AddressDTO' => __DIR__ .'/AddressDTO.php',
        'AddressTypeDTO' => __DIR__ .'/AddressTypeDTO.php',
        'StudentAddressDTO' => __DIR__ .'/StudentAddressDTO.php',
        'StudentAddressDTOArray' => __DIR__ .'/StudentAddressDTOArray.php',
        'xsd_stringArray' => __DIR__ .'/xsd_stringArray.php',
        'DocIdentDTO' => __DIR__ .'/DocIdentDTO.php',
        'DocIdentDTOArray' => __DIR__ .'/DocIdentDTOArray.php',
        'StudentStateDTOArray' => __DIR__ .'/StudentStateDTOArray.php',
        'CitizenshipDTO' => __DIR__ .'/CitizenshipDTO.php',
        'CitizenshipDTOArray' => __DIR__ .'/CitizenshipDTOArray.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_4275cd29f4a3b3f0aea8228b1c105ef0');

// Do nothing. The rest is just leftovers from the code generation.
{
}
