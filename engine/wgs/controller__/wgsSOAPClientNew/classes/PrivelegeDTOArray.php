<?php

class PrivelegeDTOArray
{

    /**
     * @var PrivelegeDTO[] $item
     */
    protected $item = null;

    /**
     * @param PrivelegeDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return PrivelegeDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param PrivelegeDTO[] $item
     * @return PrivelegeDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
