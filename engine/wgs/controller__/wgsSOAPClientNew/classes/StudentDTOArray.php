<?php

class StudentDTOArray
{

    /**
     * @var StudentDTO[] $item
     */
    protected $item = null;

    /**
     * @param StudentDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return StudentDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param StudentDTO[] $item
     * @return StudentDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
