<?php

class OrderTypeDTOArray
{

    /**
     * @var OrderTypeDTO[] $item
     */
    protected $item = null;

    /**
     * @param OrderTypeDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return OrderTypeDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param OrderTypeDTO[] $item
     * @return OrderTypeDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
