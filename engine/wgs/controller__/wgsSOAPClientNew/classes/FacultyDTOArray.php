<?php

class FacultyDTOArray
{

    /**
     * @var FacultyDTO[] $item
     */
    protected $item = null;

    /**
     * @param FacultyDTO[] $item
     */
    public function __construct(array $item)
    {
      $this->item = $item;
    }

    /**
     * @return FacultyDTO[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param FacultyDTO[] $item
     * @return FacultyDTOArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
