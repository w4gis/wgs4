<?php
/**
 * /wgsSOAPClientController.class.php
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', TRUE);

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/controller/wgsSOAPClient/classes/autoload.php');
lmb_require('wgs/controller/wgsSOAPClient/ConfigContingentArrays.class.php');
lmb_require('wgs/controller/objectadmin/CardController.class.php');

class wgsSOAPClientController extends wgsFrontController
{
	private $client;
	private $config;
	private $lockmanStudentArray;
	private $mgTier;
	private $success = false;
	private $IS_ACTIVE_STUDENT = 1;
	private $IS_HOLIDAY_STUDENT = 2;
	private $IS_DISMISS_STUDENT	= 3;
	private $IS_FINISH_STUDENT = 4;
	private $citizenshipDirectory;
	private $citizenshipFileName;

	function __construct() {

		try {
			$this->client = new LocmanInfoService();
		} catch (Exception $fault) {
			trigger_error("Couldn't connect to Contingent Server.", E_USER_ERROR);
		}

		$this->config = new ConfigContingentArrays();
		$this->lockmanStudentArray = array();
    $this->mgTier = lmbToolkit :: instance() -> getMgTier();
    $this->citizenshipDirectory = __DIR__.'/localfiles/';
		$this->citizenshipFileName = 'AllCitizenships.txt';

    	$this->connectToServer();

		parent::__construct();
	}

	private function connectToServer() {

		try {
			$this->client->LogOn("CUser","010cod");
		} catch (Exception $e) {
			trigger_error("Database of Contingent is not accessible", E_USER_ERROR);
            return;
// 			exit();
		}

		try {
			$this->isLogin();
		} catch (Exception $e) {
			trigger_error("Could not login: authorization failed", E_USER_ERROR);
			return;
// 			exit();
		}

	}

	public function isLogin() {
		$this->client->CanUse("isLogin");
	}

	private function searchParams($paramArray, $DTOArray, $valueArray) {

		$paramArrayResponse = array();

		foreach ($paramArray as $paramKey => $paramValue)
			foreach ($DTOArray as $methodKey => $methodValue)
				if ($paramValue["type"]=="class") {
					if((is_object($valueArray->$methodValue())) && (get_class($valueArray->$methodValue()) == $paramValue["class"])) {
						$paramArrayResponse[$paramValue["newName"]] = $this->searchParams( $paramValue["fields"],
																					   $this->config->$paramValue["method"](),
																					   $valueArray->$methodValue()
																					 );
						break;
					}
				} elseif ($paramKey == $methodKey) {
					$paramArrayResponse[$paramValue] = $valueArray->$methodValue();
					break;
				}

		return $paramArrayResponse;
	}

	public function doGetAllStudentStates($paramArray)	{

		$studentStateDTOArray = $this->config->getStudentStateDTOArray();
		$allStudentStatesResponse = array();

		try {
			$allStudentStates = $this->client->GetAllStudentStates();
        } catch(Exception $e) {
            trigger_error("Function 'GetAllStudentStates' is not available", E_USER_ERROR);
            return null;
        }

		foreach ($allStudentStates->getItem() as $studentState) {
			$allStudentStatesResponse[] = $this->searchParams($paramArray, $studentStateDTOArray, $studentState);
		}

		return $allStudentStatesResponse;
	}

	public function getActiveStudents($paramArray) {

		$studentDTOArray = $this->config->getStudentDTOArray();
		$studentsResponse = array();

		$criteria = new StudentRequestDTO(0,0,0,0,null,null,null,null,null,0,0,0,0,0,0,0,0,0);

		$criteria->setStudentStateId($this->IS_ACTIVE_STUDENT);

		try {
           $students = $this->client->GetStudentsByCriteria(new GetStudentsByCriteria($criteria));
        } catch(Exception $e) {
            trigger_error("Function 'GetStudentsByCriteria' is not available", E_USER_ERROR);
            return null;
        }

		/*foreach ($students->getGetStudentsByCriteriaResult()->getStudentDTO() as $student) {
			$studentsResponse[] = $this->searchParams($paramArray, $studentDTOArray, $student);
		}*/

		return $studentsResponse;
	}

	//Test function
	public function doTest() {
		$paramArray = array("PersonId" => "PersonID",
						"StudyId" => "StudyID",
						"Education" => array("newName" => "Education",
										  	"class" => "EducationDTO",
										  	"method" => "getEducationDTOArray",
										  	"type" => "class",
										  	"fields" => array("EduId" => "EduId",
										  						"EduForm" => array("newName" => "EducationForm",
										  											"class" => "EduFormDTO",
										  											"method" => "getEduFormsDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("EduFormId"	 => "EducationFormId",
							   	  	  																	"EduFormName"  => "EducationFormName"
							   	 	  																	)
										  											),
										  						"IsActive" => "IsActive"
										  						)
										  	),
	 					"LastName" => "LastName",
	  					"FirstName" => "FirstName",
	   					"MiddleName" => "MiddleName",
	    				"BirthDate" => "BirthDate");
		$searchArray = array("LastName" => "Постолати", "FirstName" => "Максим");

		$paramArray2 = array("id" => "AddressID",
						"postString" => "PostString",
						"addressIndex" => "addressIndex",
						"defaultItem" => "defaultItem",
						"Address" => array("newName" => "Address",
										  	"class" => "AddressDTO",
										  	"method" => "getAddressDTOArray",
										  	"type" => "class",
										  	"fields" => array("id" => "AddressId",
										  						"value" => "AddressValue",
										  						"shortName" => "AddressShortName",
										  						"AddressLevel" => array("newName" => "AddressLevel",
										  											"class" => "AddressLevelDTO",
										  											"method" => "getAddressLevelDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("id"	 => "AddressLevelId",
							   	  	  																	"name"  => "AddressLevelName"
							   	 	  																	)
										  											),
										  						"AddressPrefix" => array("newName" => "AddressPrefix",
										  											"class" => "AddressPrefixDTO",
										  											"method" => "getAddressPrefixDTOArray",
																				  	"type" => "class",
																				  	"fields" => array("id"	 => "AddressPrefix",
							   	  	  																	"prefix"  => "AddressPrefixName"
							   	 	  																	)
										  											),
										  						"AddressParent" => "AddressParent"
										  						)
										  	),
	 					"AddressType" => array("newName" => "AddressType",
										  	"class" => "AddressTypeDTO",
										  	"method" => "getAddressTypeDTOArray",
										  	"type" => "class",
										  	"fields" => array("id" => "AddressTypeId",
										  						"name" => "AddressTypeName"
										  						)
										  	)
	 					);

		$paramStudentHostelArray = array(
			"StudentHosteId" => "HostelId",
			"HostelDTO" => array("newName" => "Hostel",
				"class" => "HostelDTO",
				"method" => "getHostelDTOArray",
		  	"type" => "class",
		  	"fields" => array("HostelId" => "HostelId",
		  						"Name" => "HostelName"
		  						)
				),
		 		"RoomNumber" => "Room",
				"StartDate" => "StartDate",
					"EndDate" => "EndDate",
				"StudyId" => "StudentId");

			$paramStudentStatesArray = array(
					"Id" => "Id",
					"Name" => "Name");

		$paramArray3 = array("StudyId" => "StudyID");

	  // $elements = $this->doGetAllStudentStates($paramStudentStatesArray);
		// var_dump($elements);

		set_time_limit(3*60 * 60 * 60);

		//$speciality = $this->client->GetAllGroups();

		//$studentsId = $this->getActiveStudents($paramArray3);
		//var_dump($speciality);
		//$criteria = new StudentRequestDTO("3B23F7932A70AAD247257C5B001FEAF2",'5','94355DE51F58AC7E4725801B006E8AA8');
		$criteria = new StudentRequestDTO("DE4AD92FD9D4B5EC47257CC800376AB6",'1','94355DE51F58AC7E4725801B006E8AA8');

		//$criteria->setLastName('Скляров');

		$students = $this->client->GetStudentsByCriteria($criteria);
		var_dump($students);
	}
}

?>
