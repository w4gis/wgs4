<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class UserController extends wgsFrontController
{
    public function __construct($params = false)
    {
        parent :: __construct($params);
        $this->checkPermissions(wgsSysPrivilege::PROJECTUSER_ADMIN);        
    }
    
    function doGetUserList()
    {
        $users = $this->wgsTier->getProjectUserService()->getList();

        $userList = array();
        foreach($users as $user)
        {
            $userList[$user->getId()]['description'] = lmb_win1251_to_utf8($user->getDescription());
            $userList[$user->getId()]['userName'] = lmb_win1251_to_utf8($user->getName());
            $parentRoles = $user->getParentRoles();
            $sParentRoles = '';
            if(count($parentRoles) > 0)
            {
                $k = 0;
                foreach ($parentRoles as $parentRole)
                {
                    if($k == count($parentRoles)-1)
                    $sParentRoles.=lmb_win1251_to_utf8($parentRole->getName());
                    else
                    $sParentRoles.=lmb_win1251_to_utf8($parentRole->getName()).', ';
                    $k++;
                }
            }
            $userList[$user->getId()]['assignedRoles'] = $sParentRoles;
            $userList[$user->getId()]['userId'] = $user->getId();
        }
        $this->response->write(json_encode(array_merge($userList)));
    }

    function doGetAllRoles()
    {

        $roleList = array();
        if($this->request->getPost('name')==0)
        {
            $roles = $this->wgsTier->getProjectRoleService()->getList();
        }
        else
        {
            //$roles = $this->wgsTier->getSysRoleService()->getAcceptableChildrenRolesById($this->request->getPost('id'));
        }

        foreach ($roles as $role)
        {
            //if(!(is_array($role->getParentSysRoleIds())))
            //{
            $id = $role->getId();
            $roleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
            );
            //}
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($roleList[$_POST['node']])));

    }

    function doGetEmptyTree()
    {

    }

    function doGetUserProperties ()
    {
        $editUser = $this->wgsTier->getProjectUserService()->getUserByName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $userList[$editUser->getId()]['userId'] = $editUser->getId();
        $userList[$editUser->getId()]['userName'] = lmb_win1251_to_utf8($editUser->getName());
        $userList[$editUser->getId()]['userDescription'] = lmb_win1251_to_utf8($editUser->getDescription());
        $userList[$editUser->getId()]['userLocked'] = $editUser->getLocked();
        $userList[$editUser->getId()]['userPassword'] = $editUser->getPassword();
        $this->response->write(json_encode(array_merge($userList)));
    }

    function doGetAvialableRoles()
    {

        $editUserName = $this->request->getPost('name');
        $user =  $this->wgsTier->getProjectUserService()->getUserByName(lmb_utf8_to_win1251($editUserName));
        $roles = $this->wgsTier->getProjectRoleService()->getList();
        $parentRoles = array();
        $availableRoles = array();
        $parentRoles = $user->getParentRoles();

        foreach($roles as $role)
        {
            if(count($parentRoles) > 0)
            {
                $k = 0;
                foreach($parentRoles as $parentRole)
                {
                    if($role->getId() != $parentRole->getId())
                    {
                        $k++;
                    }
                    if ($k == count($parentRoles))
                    {
                        $id = $role->getId();
                        $availableRoles['root'][] = array(
                        'id'=>$id,
                        'text'=>lmb_win1251_to_utf8($role->getName()),
                        'leaf'=>true
                        );
                    }
                }
            }
            else
            {
                $id = $role->getId();
                $availableRoles['root'][] = array(
                        'id'=>$id,
                        'text'=>lmb_win1251_to_utf8($role->getName()),
                        'leaf'=>true
                );
            }
        }
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($availableRoles[$_POST['node']])));
    }

    function doGetParentRoles()
    {

        $editUserName = $this->request->getPost('name');
        $user =  $this->wgsTier->getProjectUserService()->getUserByName(lmb_utf8_to_win1251($editUserName));
        $parentRoleList = array();
        $parentRoles = $user->getParentRoles();

        if(count($parentRoles) > 0)
        {
            foreach ($parentRoles as $role)
            {
                $id = $role->getId();
                $parentRoleList['root'][] = array(
                'id'=>$id,
                'text'=>lmb_win1251_to_utf8($role->getName()),
                'leaf'=>true
                );
            }
        }
         
        if (isset($_POST['node']))
        $this->response->write(json_encode(array_merge($parentRoleList[$_POST['node']])));

    }

    function doCreateUser()
    {
        $wgsProjectUser = new wgsProjectUser();
        $wgsProjectUser->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $wgsProjectUser->setPassword( md5(
				lmb_utf8_to_win1251($this->request->getPost('name')) . "." . $this->request->getPost('password')
			)
		);
        $wgsProjectUser->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));
        $wgsProjectUser->setLocked($this->request->getPost('locked'));
        $wgsUserId = $this->wgsTier->getProjectUserService()->addUser($wgsProjectUser);

        $wgsProjectUser->setId($wgsUserId);

        if(count($this->request->getPost('assignRoleIds')) > 0)
        {
            $roleParents = $this->request->getPost('assignRoleIds');

            foreach ($roleParents as $roleParent)
            {
                if($roleParent)
                {
                    $wgsProjectRole = new wgsProjectRole();
                    $wgsProjectRole->setId($roleParent);
                    $this->wgsTier->getProjectUserService()->grantWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }
         
    }

    function doEditUser()
    {

        $wgsProjectUser = new wgsProjectUser();
        $wgsProjectUser->setId($this->request->getPost('id'));
        $wgsProjectUser->setName(lmb_utf8_to_win1251($this->request->getPost('name')));
        $wgsProjectUser->setPassword( md5(
				lmb_utf8_to_win1251($this->request->getPost('name')) . "." . $this->request->getPost('password')
			)
		);
        $wgsProjectUser->setLocked($this->request->getPost('locked'));
        $wgsProjectUser->setDescription(lmb_utf8_to_win1251($this->request->getPost('description')));

        $this->wgsTier->getProjectUserService()->setUser($wgsProjectUser);

        if(count($this->request->getPost('assignNotRoleIds')) > 0)
        {
            $roleNotParents = $this->request->getPost('assignNotRoleIds');

            foreach ($roleNotParents as $roleNotParent)
            {
                if($roleNotParent)
                {
                    $wgsProjectRole = new wgsProjectRole();
                    $wgsProjectRole->setId($roleNotParent);
                    $this->wgsTier->getProjectUserService()->revokeWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }
        if(count($this->request->getPost('assignRoleIds')) > 0)
        {
            $roleParents = $this->request->getPost('assignRoleIds');

            foreach ($roleParents as $roleParent)
            {
                if($roleParent)
                {
                    $wgsProjectRole = new wgsProjectRole();
                    $wgsProjectRole->setId($roleParent);
                    $this->wgsTier->getProjectUserService()->grantWgsRoleMemberships($wgsProjectUser, $wgsProjectRole);
                }
            }
        }

    }

    function doRemoveUser()
    {
        if(count($this->request->getPost('usersForRemoveIds')) > 0)
        {
            $wgsUserIds = $this->request->getPost('usersForRemoveIds');

            foreach ($wgsUserIds as $wgsUserId)
            {
                $wgsProjectUser = new wgsProjectUser();
                $wgsProjectUser->setId($wgsUserId);
                $this->wgsTier->getProjectUserService()->removeUser($wgsProjectUser);
            }
        }
    }

}
?>
