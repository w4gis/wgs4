<?php

lmb_require('limb/web_app/src/controller/lmbController.class.php');

class wgsController extends lmbController
{
    protected $wgsTier;
    protected $requestParams;
    protected $requestNamedParams;
    protected $viewParametersCode = null;

    public static function logToFile($data) {
        $file = fopen("C:/log/log.log","a+");
        //fwrite($file,"\r\n");
        //fwrite($file,date("l dS of F Y h:i:s A")."\r\n");
        fwrite($file, "request: ".$data."\r\n");
        //fwrite($file,"\r\n");
    }

    public function __construct($params = false)
    {
        parent :: __construct();
        $this->wgsTier = lmbToolkit :: instance() -> getWgsTier();
        if (isset($params['requestParams'])) {
            $this->setRequestParams($params['requestParams']);
        }
        if (isset($params['requestNamedParams'])) {
            $this->setRequestNamedParams($params['requestNamedParams']);
        }

    }

    function checkPermissions($privileges)
    {
        $user = $this->wgsTier->getAuthorizedUser();
        $acl  = $this->wgsTier->getACL();
        if (is_array($privileges))
        {
            foreach($privileges as $privilege)
               if (!$acl->isAllowed($user, $this->wgsTier->getSysPrivilege($privilege), wgsPrivilege::SYSPRIVILEGE_ACCESS))
                    throw new wgsException(wgsException::INSUFFICIENT_PRIVILEGES);
        }
        elseif (!$acl->isAllowed($user, $this->wgsTier->getSysPrivilege($privileges), wgsPrivilege::SYSPRIVILEGE_ACCESS))
            throw new wgsException(wgsException::INSUFFICIENT_PRIVILEGES);        
        return true;
    }
        
    public function setRequestParams($requestParams)
    {
        $this->requestParams = $requestParams;
    }

    public function getRequestParams()
    {
        return $this->requestParams;
    }

    public function setRequestNamedParams($requestNamedParams)
    {
        $this->requestNamedParams = $requestNamedParams;
    }

    public function getRequestNamedParams($parameter = null)
    {
        if (is_null($parameter))
            return $this->requestNamedParams;
        else {
            if (isset($this->requestNamedParams[$parameter])) {
                return $this->requestNamedParams[$parameter];
            }
        }
        
    }

    public function pushViewParameters($parameters)
    {
        if (is_null($this->viewParametersCode))
            $this->viewParametersCode = md5(session_id().mt_rand().mt_rand().mt_rand());
        $viewparams = $this->wgsTier->sessionGet("viewparams");
        if (!isset($viewparams[$this->viewParametersCode]) || empty($viewparams[$this->viewParametersCode]))
            $viewparams[$this->viewParametersCode] = array();
        $viewparams[$this->viewParametersCode] = array_merge($viewparams[$this->viewParametersCode], $parameters);
        $this->wgsTier->sessionSet("viewparams", $viewparams);
        return $this->viewParametersCode;
    }

    public function popViewParameters()
    {
        $viewParametersCode = $this->request->getRequest('viewParameters');
        if ($viewParametersCode)
        {
            $viewparams = $this->wgsTier->sessionGet('viewparams');
            $this->wgsTier->sessionSet('viewparams', null);
            if (isset($viewparams[$viewParametersCode]))
                return $viewparams[$viewParametersCode];
            else
                return false;
        }
        return false;
    }
}

?>