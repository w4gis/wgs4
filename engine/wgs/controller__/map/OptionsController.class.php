<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');

class OptionsController extends wgsFrontController
{
    function doDisplay()
    {
        
    }
    
    function doGetCoordSystem()
    {
        $coordSystems = $GLOBALS['COORDSYSTEM'];
        foreach ($coordSystems as $key => $value){
            $coordSystems[$key]['LEGEND'] = lmb_win1251_to_utf8($coordSystems[$key]['LEGEND']);
        }    
        $this->response->write(json_encode($coordSystems));
    }
}

?>