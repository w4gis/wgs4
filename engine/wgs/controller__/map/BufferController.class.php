<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/web_app/src/command/lmbFormCommand.class.php');
lmb_require('wgs/model/WgsUserInformation.class.php');
lmb_require(MAPVIEWER_DIR."/constants.php");
lmb_require(MAPVIEWER_DIR."/common.php");
lmb_require('wgs/model/MgWebLayoutResource.class.php');
lmb_require(MAPVIEWER_DIR."/layerdefinitionfactory.php");
lmb_require('limb/i18n/utf8.inc.php');

class BufferController extends wgsFrontController
{
    protected $mgTier;

    function getMapSRS($map)
    {
        $srs = $map->GetMapSRS();
        if($srs != "")
        return $srs;
        return "LOCALCS[\"Non-Earth (Meter)\",LOCAL_DATUM[\"Local Datum\",0],UNIT[\"Meter\", 1],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";
    }
     
    function findLayer($layers, $layerName)
    {
        $layer = null;
        for($i = 0; $i < $layers->GetCount(); $i++)
        {
            $layer1 = $layers->GetItem($i);
            if($layer1->GetName() == $layerName)
            {
                $layer = $layer1;
                break;
            }
        }
        return $layer;
    }

    function buildLayerDefinitionContent($dataSource, $post)
    {
        $featureName = "Buffer";
        $fillstyle = $post['fillstyle'];
        $xtrans = sprintf("%02x", 255 * $post['foretrans'] / 100);
        $ffcolor = $post['ffcolor'];
        $fbcolor = $post['fbcolor'];
        $transparent = 1;
        $linestyle = $post['linestyle'];
        $thickness = $post['thickness'];
        $lcolor = $post['lcolor'];

        $layerTempl = file_get_contents(VIEWERFILES_DIR."/arealayerdef.templ");

	    $xmlStr = sprintf($layerTempl,
	                      $dataSource,
	                      $featureName,
	                      "GEOM",
	                      $fillstyle,
	                      $xtrans . $ffcolor,
	                      $post['foretrans']? "ff" . $fbcolor: "00" . $fbcolor,
	                      $linestyle,
	                      $thickness,
	                      $lcolor);
	    $src = new MgByteSource($xmlStr, strlen($xmlStr));
	    return $src->GetReader();
    }

    function clearDataSource($featureSrvc, $dataSourceId, $featureName)
    {
        $deleteCmd = new MgDeleteFeatures($featureName, "ID >= 0");
        $commands = new MgFeatureCommandCollection();
        $commands->Add($deleteCmd);
        $featureSrvc->UpdateFeatures($dataSourceId, $commands, false);
    }

	function addFeatureToCollection($propCollection, $agfRW, $featureId, $featureGeom)
	{
	    $bufferProps = new MgPropertyCollection();
	    $idProp = new MgInt32Property("ID", $featureId);
	    $bufferProps->Add($idProp);
	    $geomReader = $agfRW->Write($featureGeom);
	    $geomProp = new MgGeometryProperty("GEOM", $geomReader);
	    $bufferProps->Add($geomProp);
	    $propCollection->Add($bufferProps);
	}

    function doCreateBuffer()
    {
        //--------Получаем параметры
        $post = $this->request->getPost();
        $sessionId   = $post['session'];
        $mapName     = $post['mapname'];
        $bufferName  = $post['buffername'];
        $selText     = $post['selection'];
        $units       = $post['units'];
        $layerNames  = $post['layerNames'];
        $locale      = $post['locale'];
        $merge       = $post['merge'];//'on';
        $distance    = $post['distance'];
        $featureName = "Buffer";
        $foretrans   = $post['foretrans'];

        SetLocalizedFilesPath(GetLocalizationPath()); //TODO: interesno

        $dataSource  = "Session:" . $sessionId . "//" . ($bufferName) . "_Buffer.FeatureSource";
        $layerDef    = "Session:" . $sessionId . "//" . ($bufferName) . "_Buffer.LayerDefinition";

        try
        {
            $newBuffer = false;

            $this->mgTier = lmbToolkit :: instance() -> getMgTier();

            $mgMapName = $this->request->getPost('mapname');
            $mgSessionId = $this->request->getPost('session');

            $site = $this->mgTier->getSiteConnection();

            $featureSrvc = $site->CreateService(MgServiceType::FeatureService);
            $resourceSrvc = $site->CreateService(MgServiceType::ResourceService);

            $dataSourceId = new MgResourceIdentifier($dataSource);
            $layerDefId = new MgResourceIdentifier($layerDef);

            $map = new MgMap();
            $map->Open($resourceSrvc, $mgMapName);

            $layers = $map->GetLayers();
            //var_dump($layers);
            $layer = $this->findLayer($layers, $bufferName);

	        // convert distance to meters
	        if($units == "mi")              //miles
	            $distance *= 1609.35;
	        else if($units == "ki")         //kilometers
	            $distance *= 1000;
	        else if($units == "fe")         //feet
	            $distance *= .30480;

	        // Get the map SRS
	        //
	        $srsFactory = new MgCoordinateSystemFactory();
	        $srsDefMap = $this->getMapSRS($map);
	        $mapSrsUnits = "";
	        $srsMap = $srsFactory->Create($srsDefMap);
	        $arbitraryMapSrs = $srsMap->GetType() == MgCoordinateSystemType::Arbitrary;
	        if($arbitraryMapSrs)
	            $mapSrsUnits = $srsMap->GetUnits();

	        //Create/Modify layer definition
	        $layerDefContent = $this->buildLayerDefinitionContent($dataSource,$post);

            //var_dump(get_class($layerDefContent));
            //var_dump($layerDefId);
	        $resourceSrvc->SetResource($layerDefId, $layerDefContent);

	        if($layer == null)
	        {
	            $newBuffer = true;
	            //Targetting a new layer. create a data source for it
	            //
	            $classDef = new MgClassDefinition();
	
	            $classDef->SetName($featureName);
				//echo(GetLocalizedString("BUFFERCLASSDESCR", $locale))." !!";
	            $classDef->SetDescription(GetLocalizedString("BUFFERCLASSDESCR", $locale));
	            $classDef->SetDefaultGeometryPropertyName("geom");
	
	            //Set KEY property
	            $prop = new MgDataPropertyDefinition("KEY");
	            $prop->SetDataType(MgPropertyType::Int32);
	            $prop->SetAutoGeneration(true);
	            $prop->SetReadOnly(true);
	            $classDef->GetIdentityProperties()->Add($prop);
	            $classDef->GetProperties()->Add($prop);
	
	            //Set ID property.
	            $prop = new MgDataPropertyDefinition("ID");
	            $prop->SetDataType(MgPropertyType::Int32);
	            $classDef->GetProperties()->Add($prop);
	
	            //Set geometry property
	            $prop = new MgGeometricPropertyDefinition("geom");
	            //$prop->SetGeometryTypes(MgFeatureGeometricType::mfgtSurface); //TODO use the constant when exposed
	            $prop->SetGeometryTypes(4);
	            $classDef->GetProperties()->Add($prop);
	
	            //Create the schema
	            $schema = new MgFeatureSchema("BufferSchema", GetLocalizedString("BUFFERSCHEMADESCR", $locale));
	            $schema->GetClasses()->Add($classDef);
	
	            //finally, creation of the feature source
	            $sdfParams = new MgCreateSdfParams("LatLong", $srsDefMap, $schema);
	            $featureSrvc->CreateFeatureSource($dataSourceId, $sdfParams);

	            //Add layer to map
				//echo	($layerDefId);
	            $layer = new MgLayer($layerDefId, $resourceSrvc);
	            $layer->SetName($bufferName);
	            $layer->SetLegendLabel($bufferName);
	            $layer->SetDisplayInLegend(true);
	            $layer->SetSelectable(true);
	            $layers->Insert(0, $layer);
	        }
	        else
	        {
	            //data source already exist. clear its content
	            //
	            $this->clearDataSource($featureSrvc, $dataSourceId, $featureName);
	        }
	
	        $sel = new MgSelection($map, $selText);
	
	        $selLayers = $sel->GetLayers();
	
	        $agfRW = new MgAgfReaderWriter();
	        $bufferGeometries = new MgGeometryCollection();
	
	        $commands = new MgFeatureCommandCollection();
	        $featId = 0;
	
	        $propCollection = new MgBatchPropertyCollection();
	        $excludedLayers = 0;
	        $srsDs = null;
	            $inputGeometries = new MgGeometryCollection();
	
	        $bufferFeatures = 0;
	        $allCompatible = false;
	
	        for($li = 0; $li < $selLayers->GetCount(); $li++)
	        {
	            $selLayer = $selLayers->GetItem($li);
	            $inputLayer = false;
	            $selLayerName = $selLayer->GetName();
	            for($il = 0; $il < count($layerNames); $il ++)
	            {
	                if($layerNames[$il] == $selLayerName)
	                {
	                    $inputLayer = true;
	                    break;
	                }
	            }
	            if($inputLayer == false)
	                continue;
	
	            // get the data source SRS
	            //
	            $featSourceId = new MgResourceIdentifier($selLayer->GetFeatureSourceId());
	            $ctxs = $featureSrvc->GetSpatialContexts($featSourceId, false);
	            $srsDefDs = "";
	            if($ctxs != null && $ctxs->ReadNext())
	                $srsDefDs = $ctxs->GetCoordinateSystemWkt();
	
	            if($srsDefDs == "")
	            {
	                $excludedLayers ++;
	                continue;
	            }
	
	            $srsDs = $srsFactory->Create($srsDefDs);
	            $arbitraryDsSrs = $srsDs->GetType() == MgCoordinateSystemType::Arbitrary;
	            $dsSrsUnits = "";
	
	            if($arbitraryDsSrs)
	                $dsSrsUnits = $srsDs->GetUnits();
	
	            // exclude layer if:
	            //  the map is non-arbitrary and the layer is arbitrary or vice-versa
	            //     or
	            //  layer and map are both arbitrary but have different units
	            //
	            if(($arbitraryDsSrs != $arbitraryMapSrs) || ($arbitraryDsSrs && ($dsSrsUnits != $mapSrsUnits)))
	            {
	                $excludedLayers ++;
	                continue;
	            }
	
	            // calculate distance in the data source SRS units
	            //
	            $dist = $srsDs->ConvertMetersToCoordinateSystemUnits($distance);
	
	            // calculate great circle unless data source srs is arbitrary
	            if(!$arbitraryDsSrs)
	                $measure = $srsDs->GetMeasure();
	            else
	                $measure = null;
	
	            // create a SRS transformer if necessary.
	            if($srsDefDs != $srsDefMap)
	                $srsXform = $srsFactory->GetTransform($srsDs, $srsMap);
	            else
	                $srsXform = null;
	
	            $featureClassName = $selLayer->GetFeatureClassName();
	            $filter = $sel->GenerateFilter($selLayer, $featureClassName);
	            if($filter == "")
	                continue;
	
	            $query = new MgFeatureQueryOptions();
	            $query->SetFilter($filter);
	
	            $featureSource = new MgResourceIdentifier($selLayer->GetFeatureSourceId());
	
	            $features = $featureSrvc->SelectFeatures($featureSource, $featureClassName, $query);
	
	            if($features->ReadNext())
	            {
	                $classDef = $features->GetClassDefinition();
	                $geomPropName = $classDef->GetDefaultGeometryPropertyName();
	
	                do
	                {
	                    $geomReader = $features->GetGeometry($geomPropName);
	                    $geom = $agfRW->Read($geomReader);
	
	                    if(!$merge)
	                    {
	                        $geomBuffer = $geom->Buffer($dist, $measure);
	                        if($geomBuffer != null)
	                        {
	                            if($srsXform != null)
	                                $geomBuffer = $geomBuffer->Transform($srsXform);
	                            $this->addFeatureToCollection($propCollection, $agfRW, $featId++, $geomBuffer);
	                            $bufferFeatures++;
	                        }
	                    }
	                    else
	                    {
	                        if($srsXform != null)
	                            $geom = $geom->Transform($srsXform);
	                        $inputGeometries->Add($geom);
	                    }
	                }
	                while($features->ReadNext());
	
	                $features->Close();
	            }
	        }
	
	        if($merge)
	        {
	            if($inputGeometries->GetCount() > 0)
	            {
	                $dist = $srsMap->ConvertMetersToCoordinateSystemUnits($distance);
	                if(!$arbitraryMapSrs)
	                    $measure = $srsMap->GetMeasure();
	                else
	                    $measure = null;
	
	                $geomFactory = new MgGeometryFactory();
	                $geomBuffer = $geomFactory->CreateMultiGeometry($inputGeometries)->Buffer($dist, $measure);
	                if($geomBuffer != null)
	                {
	                    $this->addFeatureToCollection($propCollection, $agfRW, $featId, $geomBuffer);
	                    $bufferFeatures = 1;
	                }
	            }
	        }
	
	        if($propCollection->GetCount() > 0)
	        {
	            $commands->Add(new MgInsertFeatures($featureName, $propCollection));
	
	            //Insert the features in the temporary data source
	            //
	            $res = $featureSrvc->UpdateFeatures($dataSourceId, $commands, false);
	        }
	
	        // Save the new map state
	        //
	        $layer->ForceRefresh();
	        $map->Save($resourceSrvc);
        }
        catch(MgException $e) 
        {
            print $e;
        	print 'error in wgs/controller/map/BufferController.class.php';
        }
        catch(Exception $e)
        {
            print $e->getMessage();
        }
    }

    function doDisplay()
    {
    }
}

?>


