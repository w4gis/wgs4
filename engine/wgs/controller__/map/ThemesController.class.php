<?php

lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('limb/i18n/utf8.inc.php');
lmb_require('wgs/lib/cjson/cjson.inc.php');

require_once(MAPVIEWER_DIR.'/layerdefinitionfactory.php');

class ThemesController extends wgsFrontController
{
	function __construct(){
		parent::__construct();

		//$_SESSION['themeNames'];
	}
    function doCreate()
    {
    	$data = $this->request->getPost('data');
        // if get data from wgs3
        if ($data!=null) {
            $data = CJSON::decode($data);
            $data = $data['params'];
        } else {
        // if get from wgs4
            $data = $this->request->getPost('params');
            $data = json_decode($data, true);
        }

        $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();

        $themes = $this->wgsTier->getThemeService();

        $theme_id = $themes->Add(decode($data['NAME']), decode($data['MAP']), $data['SHARED'], $wgsuser_id, $data['VIEW_OPTIONS'], $data['POSITION'], $data['IS_ARCHIVE']);


        $data['AREA'] = 'private';
        $data['THEME_ID'] = $theme_id;

        echo CJSON::encode(array(
          'success' => true,
          'record' => $data
          ));
    }

    function doUpdate()
    {
    	$data = $this->request->getPost('data');

      if ($data!=null) {
          $data = CJSON::decode($data);
          $data = $data['params'];
      } else {
      // if get from wgs4
          $data = $this->request->getPost('params');
          $data = json_decode($data, true);
      }

    	$wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();

    	$theme_id = $data['THEME_ID'];
    	$name = decode($data['NAME']);
    	$shared = $data['SHARED'];
      $map = $data['MAP'];

		//added position
      $position = $data['POSITION'];

      $is_archive = isset($data['IS_ARCHIVE']) ? $data['IS_ARCHIVE'] : 0;
      $themes = $this->wgsTier->getThemeService();
      $themes->edit($wgsuser_id, $theme_id, $name, $shared, $position, $is_archive, $map);

      echo CJSON::encode(array(
          'success' => true,
          'record' => $data
          ));
  }

  function doUpdateView()
  {
   $data = $this->request->getPost('data');

   $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
   $data = CJSON::decode($data);

   $theme_id = $data['params']['THEME_ID'];
   $view_options = $data['params']['VIEW_OPTIONS'];
		//added position
   $position = $data['params']['POSITION'];


   $aview_options = CJSON::decode($view_options);
   $mapname = $aview_options['mapname'];

   $layers = implode(",",$this->getLayerVisibility($mapname));
   $aview_options['layers'] = $layers;
   $view_options = CJSON::encode($aview_options);

   $themes = $this->wgsTier->getThemeService();

   $themes->updateViewOptions($wgsuser_id, $theme_id, $view_options);

   echo CJSON::encode(array(
      'success' => true,
      'record' => array('THEME_ID'=> $theme_id, 'VIEW_OPTIONS' => $view_options, 'POSITION' => $position)
      ));
}

function doDelete()
{
    $data = $this->request->getPost('data');
     if ($data!=null) {
        $data = CJSON::decode($data);
        $theme_id = $data['params']['THEME_ID'];
    } else {
        // if get from wgs4
        $theme_id = json_decode($this->request->getPost('THEME_ID'));
    }
    $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
   // $theme_id = $data['params']['THEME_ID'];

    $themes = $this->wgsTier->getThemeService();


    $themes->remove($wgsuser_id, $theme_id);
    //WGS3
    //echo "{success: true}";
    //WGS4
    echo json_encode(array('success' => 'true'));
}

function doList()
{
        //echo "{success: true, records: []}";
	$whoIsUser = $this->request->getPost('whoIsUser');

  if($whoIsUser!=null){
     $isRootUser = 0;
     $wgsuser_roles = $this->wgsTier->getAuthorizedUser()->getParentRoles();
			//$firephp = FirePHP::getInstance(true);
     foreach ($wgsuser_roles as $role){
				//$firephp->log($role-> getName());
        if ($role -> getName() == 'root') $isRootUser = 1;
    }
    $response = array(
      'isRootUser' => $isRootUser,
      );
} else {
   $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();

    //data:{"object":"themes","params":{"THEME_ID":1}}
   $data = $this->request->getPost('data');

   $adata = json_decode($data, true);

   $theme_id = $adata['params']['THEME_ID'];

   $theme_id = isset($theme_id) ? $theme_id : false;

   $themes = $this->wgsTier->getThemeService();

   $results = $themes->GetList($wgsuser_id, $theme_id);

    	//print_r($results);

   $records = array();
   $_SESSION['themeNames'] = array();
   $iter = 0;
   foreach($results as $result) {
      //var_dump($result);
      $records[] = array(
         'THEME_ID' => $result->get('THEME_ID'),
         'NAME' => encode($result->get('NAME')),
         'SHARED' => $result->get('SHARED'),
         'AREA' => $result->get('AREA'),
         'VIEW_OPTIONS' => $result->get('VIEW_OPTIONS'),
         'POSITION' => $result -> get ('POSITION'),
         'IS_ARCHIVE' => $result -> get ('IS_ARCHIVE'),
         'MAP' => $result -> get ('MAP')
         );
      $_SESSION['themeNames'][$iter]['name'] = 'THEME_' . $result->get('THEME_ID');
      $_SESSION['themeNames'][$iter]['position'] = $result -> get ('POSITION');
      $_SESSION['themeNames'][$iter]['title'] = encode($result->get('NAME'));

      $iter++ ;
  }
		//$firephp = FirePHP::getInstance(true);

		/*foreach($_SESSION['themeNames'] as $tn){
			$firephp->log($tn);
		}*/

        $response = array(
            'success' => true,
            'records' => $records
            );
    }
    //var_dump($response);
    //die();
    $this->response->write(json_encode($response));
}

function doThemeLayerList() {
   $records = array();

   $themeId = $this->request->getPost('themeId');

   $themes = $this->wgsTier->getThemeService();
   $results = $themes->getThemeLayerList($themeId);
   //var_dump($results);
   if ($results) {
      foreach($results as $result) {
          if ($result->get('THEME_ID') != null) {
             $records[] = array(
                'themeId' => $result->get('THEME_ID'),
                'id' => $result->get('THEMELAYER_ID'),
                'position' => $result->get('POSITION'),
                'name' => encode($result->get('LEGEND')),
                'featureLayer' => $result->get('FEATURELAYER_ID'),
                'featureType' => $result->get('OBJECT_TYPE_ID'),
                'featureGeometry' => $result->get('GTYPE'),
                'condition' => encode($result->get('CONDITION')),
                'style' => $result->get('STYLE')
                );
          }
     }
 }
 $response = array(
    'success' => true,
    'records' => $records,
    'tolalcount' => count($records)
    );

 $this->response->write(json_encode($response));
}

function doAddThemeLayer() {
  $rpost = CJSON::decode($this->request->getPost('records'));

  if (!isset($rpost[0])) {
      $records[] = $rpost;
  } else {
      $records = $rpost;
  }

  $themes = $this->wgsTier->getThemeService();

  $data = array();

  foreach ($records as $r) {

      $featureLayerId = $themes->addThemeLayer(
         $r['themeId'],
         decode($r['name']),
         $r['featureLayer'],
         $r['featureType'],
         $r['featureGeometry'],
         $r['condition'] ? decode($r['condition']) : '[]',
         $r['style'],
         $r['position']);
      if ($featureLayerId) {
         $r['id'] = $featureLayerId;
         $data[] = $r;
     }
 }

 $response = array(
    'success' => true,
    'message' => count($data)." record(s) created",
    'records' => count($data) == 1 ? $data[0] : $data
    );

 $this->response->write(json_encode($response));
}

function doEditThemeLayer() {
   $rpost = CJSON::decode($this->request->getPost('records'));

   if (!isset($rpost[0])) {
      $records[] = $rpost;
  } else {
      $records = $rpost;
  }

  $themes = $this->wgsTier->getThemeService();
  $data = array();


  foreach ($records as $r) {

    		//editThemeLayer($themeLayerId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style)
      $themes->editThemeLayer(
         $r['id'],
         decode($r['name']),
         $r['featureLayer'],
         $r['featureType'],
         $r['featureGeometry'],
         $r['condition'] ? decode($r['condition']) : '[]',
         $r['style'],
         $r['position']
         );

      $data[] = $r;
  }

  $response = array(
    'success' => true,
    'message' => count($data)." record(s) updated",
    'records' => count($data) == 1 ? $data[0] : $data
    );

  $this->response->write(json_encode($response));
}

function doRemoveThemeLayer() {

  $rpost = CJSON::decode($this->request->getPost('records'));

  if (!is_array($rpost)) {
      $records[] = $rpost;
  } else {
      $records = $rpost;
  }

  $themes = $this->wgsTier->getThemeService();

  foreach ($records as $r) {
      $themes->removeThemeLayer($r);
  }

  $response = array(
    'success' => true,
    'message' => count($records)." record(s) removed",
    'records' => array()
    );

  $this->response->write(json_encode($response));

    	//removeThemeLayer($themeLayerId)
}

function doDisplayTheme() {
   
   $theme_id = $this->request->getPost('THEME_ID');
   $mgMapName = $this->request->getPost('mapname');
   $mgSessionId = $this->request->getPost('session');

    	//$view = CJSON::decode($this->request->getPost('VIEW_OPTIONS'));

    	//print_r($mgSessionId);
    	//print_r($view);
   $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();
   $themes = $this->wgsTier->getThemeService();
   $mglayers = $this->wgsTier->getProjectObjectService()->getMgLayerService();
		//print_r($mglayers);
   $results = $themes->GetList($wgsuser_id);

		//print_r($results);
   $theme = array();
    	//print_r($results);
   foreach($results as $result) {
		//if ($results) {
		//print_r($result);
      if ($result->get('theme_id') == $theme_id)
          $theme[] = array(
             'THEME_ID' => $result->get('theme_id'),
             'NAME' => $result->get('name'),
             'SHARED' => $result->get('shared'),
             'AREA' => $result->get('area'),
				//added POSITION
             'POSITION' => $result->get('position')
    			//'VIEW_OPTIONS' => CJSON::decode($result->get('VIEW_OPTIONS')),
             );
  }

  if (count($theme) == 1) {
    		//print_r($theme[0]);
      $layers = array();
      $layerList = $themes->getThemeLayerList($theme_id);
      $flayerIds = array();
      foreach($layerList as $layer) {
         array_unshift($layers, array(
            'THEMELAYER_ID' => $layer->get('themelayer_id'),
            'THEME_ID' => $layer->get('theme_id'),
            'LEGEND' => $layer->get('legend'),
            'FEATURELAYER_ID' => $layer->get('featurelayer_id'),
            'OBJECT_TYPE_ID' => $layer->get('object_type_id'),
            'GTYPE' => $layer->get('gtype'),
            'CONDITION' => $layer->get('condition'),
            'STYLE' => $layer->get('style'),
            ));
        
         $flayerIds[] = $layer->get('featurelayer_id');
     }

     if (count($layers) > 0) {
				//print_r($layers);
         $mls = $mglayers->getMgLayerListByFLayerIds($flayerIds);
				//print_r($mls);
         if (count($mls)) {
            $fs = array();
            foreach($mls as $ml) {
               $fs[$ml->get('featurelayer_id')][] = $ml->get('resource_id');
           }
           //print_r($fs);
           $this->_stylizeTheme($fs, $theme[0], $layers, $mgMapName, $mgSessionId);
       }
   }
   
   $this->response->write(json_encode($flayerIds));
}

}

function _findLayer($layers, $layerName)
{
    $layer = null;

    for($i = 0; $i < $layers->GetCount(); $i++)
    {
        $layer1 = $layers->GetItem($i);
        if($layer1->GetName() == $layerName)
        {
            $layer = $layer1;
            break;
        }
    }
    return $layer;
}
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

function get_func_argNames($funcName) {
    $f = new ReflectionFunction($funcName);
    $result = array();
    foreach ($f->getParameters() as $param) {
        $result[] = $param->name;
    }
    return $result;
}

function add_layer_definition_to_map($layerDefinition, $layerName, $layerLegendLabel, $sessionId, $resourceService, &$map)
	// Adds the layer definition (XML) to the map.
	// Returns the layer.
{
   global $schemaDirectory;

	    // Validate the XML.
   $domDocument = new DOMDocument;
   $domDocument->loadXML($layerDefinition);
	    /*
	    if (! $domDocument->schemaValidate($schemaDirectory . "LayerDefinition-1.3.0.xsd") ) // $schemaDirectory is defined in common.php
	    {
	        echo "ERROR: The new XML document is invalid.<BR>\n.";
	        return NULL;
	    }
		*/
	    // Save the new layer definition to the session repository
	    $byteSource = new MgByteSource($layerDefinition, strlen($layerDefinition));
	    $byteSource->SetMimeType(MgMimeType::Xml);
	    $resourceID = new MgResourceIdentifier("Session:$sessionId//$layerName.LayerDefinition");

        //var_dump($resourceService->SetResource);
        //mgresourceservice
	    $resourceService->SetResource($resourceID, $byteSource->GetReader(), null);

	    $newLayer = $this->add_layer_resource_to_map($resourceID, $resourceService, $layerName, $layerLegendLabel, $map);

	    return $newLayer;
	}

	//////////////////////////////////////////////////////////////
	//$position - describes order in legend and order of drawing
	/////////////////////////////////////////////////////////////////////////
	function add_layer_to_group($layer, $layerGroupName, $layerGroupLegendLabel, &$map, $position = 0)
	// Adds a layer to a layer group. If necessary, it creates the layer group.
	{
		//$firephp = FirePHP::getInstance(true);
		// Get the layer group
       $layerGroupCollection = $map->GetLayerGroups();

	    //
       if ($layerGroupCollection->Contains($layerGroupName))
       {
           $layerGroup = $layerGroupCollection->GetItem($layerGroupName);
       }
       else
       {
	        // It does not exist, so create it
           $layerGroup = new MgLayerGroup($layerGroupName);
           $layerGroup->SetVisible(true);
           $layerGroup->SetDisplayInLegend(true);
           $layerGroup->SetLegendLabel($layerGroupLegendLabel);
	        //$layerGroup->SetExpandInLegend(true);
	        //$layerGroupCollection->Add($layerGroup);
           $layerGroupCollection->Insert($position, $layerGroup);
       }

	    // Add the layer to the group
       $layer->SetGroup($layerGroup);
   }

	//////////////////////////////////////////////////////////////
   function add_layer_resource_to_map($layerResourceID, $resourceService, $layerName, $layerLegendLabel, &$map)
	// Adds a layer defition (which can be stored either in the Library or a session
	// repository) to the map.
	// Returns the layer.
   {
       // var_dump(get_class($resourceService));
       $newLayer = new MgLayer($layerResourceID, $resourceService);
        //var_dump("1");
	    // Add the new layer to the map's layer collection
       $newLayer->SetName($layerName);
       $newLayer->SetVisible(true);
       $newLayer->SetLegendLabel($layerLegendLabel);
       $newLayer->SetDisplayInLegend(true);

       $layerCollection = $map->GetLayers();
        //var_dump("2");
       if (! $layerCollection->Contains($layerName) )
       {
	        // Insert the new layer at position 0 so it is at the top
	        // of the drawing order

           $layerCollection->Insert(0, $newLayer);

       }
       // var_dump("3");
       //   var_dump("$layerCollection");

       return $newLayer;
   }

   private function _getFilter($cond)
   {

       $filter = "";
       $f="";
       $wgsuser_roles = $this->wgsTier->getAuthorizedUser()->getParentRoles();
       foreach ($wgsuser_roles as $role){
         $ur = $role->getId();
         $fp ="";
         $results = $this->wgsTier->getProjectRoleService()->getArea(1,$ur);

         foreach($results as $result) {
            $fp = $result->get('limit_ftf');
        }
        if($fp){
            $f .= (($f == "") ? "" : " AND ") . "Geometry INSIDE GeomFromText('".$fp."')";
        }
    }

    $filter = (($f == "") ? "" : ($f . " AND ")) ;

    for ($i=0; $i<count($cond); $i++) {
      //$prop = strtoupper($cond[$i]['property']);
      $prop = $cond[$i]['property'];
      $c = htmlspecialchars($cond[$i]['condition']);
      //$c = $cond[$i]['condition'];
      $v = $cond[$i]['value'];
      if ($c == 'LIKE') {
          $c = 'like';
         $v = "'%$v%'";
     } else
     if ($c == 'IS NULL') {
         $c = 'null';
         $v = null;
     } else
     if ($c == 'EQUAL') {
         $c = '=';
         $v = "'$v'";
     } else {
         $v = is_numeric($v) ? "$v" : ("'".$v."'");
     }
     if (@$cond[$i]['domainValue']) {
         $v = "'".$cond[$i]['domainValue']."'";
     }
     if ($i < count($cond)-1) {
         $l = $cond[$i]['logical'];
     } else {
         $l = "";
     }
    		/*
    		if ($v === '') {
    			$v = "''";
    		}
    		*/
    		$filter = $filter."(prop_$prop $c $v) $l ";
    	}

    	return "(".$filter.")";
    }

    private function _stylizeTheme($fs, $theme, $tlayers, $mgMapName, $mgSessionId)
    {
    	//print_r($fs);
    	//print_r($theme);
    	//print_r($tlayers);
    	//print_r($mgMapName);
    	//print_r($mgSessionId);

    	//get layer
    	$this->mgTier = lmbToolkit :: instance() -> getMgTier();
    	//$site = $this->mgTier->getSiteConnection();

    	$map = new MgMap();

        //////////////////////////////////////////////////////////////
        $sessionID = $_REQUEST['session'];
        session_start();

        $user = new MgUserInformation($sessionID);
        $user->SetClientIp(GetClientIp());
        $user->SetClientAgent(GetClientAgent());

        $siteConnection = new MgSiteConnection();
        $siteConnection->Open($user);

        $resourceSrvc = $siteConnection->CreateService(MgServiceType::ResourceService);
        ////////////////////////////////////////////////////////////


        $map->Open($resourceSrvc, $mgMapName);

        $layers = $map->GetLayers();
        $themeName = "THEME_".$theme['THEME_ID'];
		//for($i=0; $i<$layers->GetCount(); $i++)
		//	print_r($layers -> GetItem($i) -> GetName() );
		//print_r($map);
    	//print_r($resourceSrvc);
    	//print_r($layers);
		/*
		*	� ������ ����������� ������������ ����� ���� ���� � ����� ������� �����������
		*	(������� ��������� 'position' ��� 'position'>0) � ����� ������� ����������
		*	���� �������� ������� ������������ ������ �� 1.
		*
		*	� theme_order - ���������� ����� ����� ��� ������� ������
		*/
        $wgsuser_id = $this->wgsTier->getAuthorizedUser()->getId();

        $theme_prior = $theme['POSITION'] ? $theme['POSITION'] : -1;

				//������ �������� � ������, �� ���� ������ ����������, ��
        if(count($_SESSION['themeNames']) <= 1) {

					//$firephp->log('catched!');
           $themes = $this->wgsTier->getThemeService();
           $results = $themes->GetList($wgsuser_id, false);
           $_SESSION['themeNames'] = array();
           $iter = 0;
           foreach($results as $result) {
              $_SESSION['themeNames'][$iter]['name'] = 'THEME_' . $result->get('THEME_ID');
              $_SESSION['themeNames'][$iter]['position'] = $result -> get ('POSITION');
              $_SESSION['themeNames'][$iter]['title'] = encode($result->get('NAME'));

              $iter++ ;
          }
      }
				//

      $layerGroupCollection = $map->GetLayerGroups();
			$theme_order = $layerGroupCollection->GetCount();


				//$firephp->log('Styling...' . count($_SESSION['themeNames']));
      foreach($_SESSION['themeNames'] as $tn){
        if ($layerGroupCollection->Contains($tn['name'])){
          if($tn['position']>0){
							//$firephp->log($tn);
             if ( ($tn['position'] < $theme_prior) || ($theme_prior < 0) )	$theme_order ++	;
							//$firephp->log($tn['position'].' < '.$theme_prior);
          }
        }
      }
				//$firephp->log('Order is ' . $theme_order);

		/*
		*	������ ��� ��� ���������. �������� theme_order � �������� ����� ��� ������� ������
		*/


       foreach ($tlayers as $tlayer) {
          $themeLayerName = "THEMELAYER_".$tlayer['THEMELAYER_ID'];

          $layer = $this->_findLayer($layers, $themeLayerName);

          if ($layer != null)
          {
              $layers->Remove($layer);
          }

	        	//$newTheme = true;
          $layerDefId = new MgResourceIdentifier($fs[$tlayer['FEATURELAYER_ID']][0]);
          $mgLayer = new MgLayer($layerDefId, $resourceSrvc);

          $factory = new LayerDefinitionFactory();

          $legendLabel = '';
    			//$filter = "PROP_B1 = ''";
          $color = 'FF0000FF';
			    // Create a scale range.
          $minScale = '0';
          $maxScale = '1000000000000';

          $tstyle = CJSON::decode($tlayer['STYLE']);

          $filter = strtolower($this->_getFilter(CJSON::decode(encode($tlayer['CONDITION']))));
			    //print $filter;
          switch ($tlayer['GTYPE']) {
            case 1:
            if ($tstyle) {
              $color = $tstyle['Fill_ForegroundColor'];

              $shape = $tstyle['Shape'];
              $unit = $tstyle['Unit'];
              $dunits = $tstyle['SizeContext'];
              $sizex = $tstyle['SizeX'];
              $sizey = $tstyle['SizeY'];
              $fillPattern = $tstyle['Fill_FillPattern'];
              $fillbg = $tstyle['Fill_BackgroundColor'];
              $lyneStyle = $tstyle['Edge_LineStyle'];
              $thickness = $tstyle['Edge_Thickness'];
              $ecolor = $tstyle['Edge_Color'];
              $eunits = $tstyle['Edge_Unit'];
              $edunits = $tstyle['Edge_SizeContext'];
              $transparent = @$tstyle['Transparency']?$tstyle['Transparency']:'FF';

              $symbol = "
              <Mark>
                <Unit>$unit</Unit>
                <SizeContext>$dunits</SizeContext>
                <SizeX>$sizex</SizeX>
                <SizeY>$sizey</SizeY>
                <Shape>$shape</Shape>
                <Fill>
                   <FillPattern>$fillPattern</FillPattern>
                   <ForegroundColor>{$transparent}{$color}</ForegroundColor>
                   <BackgroundColor>$fillbg</BackgroundColor>
               </Fill>
               <Edge>
                   <LineStyle>$lyneStyle</LineStyle>
                   <Thickness>$thickness</Thickness>
                   <Color>FF$ecolor</Color>
                   <Unit>$eunits</Unit>
                   <SizeContext>$edunits</SizeContext>
               </Edge>
           </Mark>";

           $text = "";
           $fontHeight = "12";
           $textSymbol = $factory->CreateTextSymbol($text, $fontHeight, $color);
           $rule = $factory->CreatePointRule($legendLabel, $filter, $textSymbol, $symbol);

			    			// Create a point type style.
           $typeStyle = $factory->CreatePointTypeStyle($rule);

       }
       break;
       case 2:
       if ($tstyle) {
			    			//$rule = $factory->CreateLineRule($legendLabel, $filter, $color);

          $lineStyle = $tstyle['LineStyle'];
          $units = $tstyle['Unit'];
          $thickness = $tstyle['Thickness'];
          $color = strtoupper($tstyle['Color']);
          $dunits = $tstyle['SizeContext'];
          $transparent = @$tstyle['Transparency']?$tstyle['Transparency']:'FF';

          $rule = "
          <LineRule>
             <LegendLabel></LegendLabel>
             <Filter>$filter</Filter>
             <LineSymbolization2D>
                 <LineStyle>$lineStyle</LineStyle>
                 <Thickness>$thickness</Thickness>
                 <Color>{$transparent}{$color}</Color>
                 <Unit>$units</Unit>
                 <SizeContext>$dunits</SizeContext>
             </LineSymbolization2D>
         </LineRule>";

							// Create a line type style.
         $typeStyle = $factory->CreateLineTypeStyle($rule);
			    			//print $typeStyle;
     }
     break;
     case 3:
     if ($tstyle) {

      $fillPattern = $tstyle['Fill_FillPattern'];
      $color = $tstyle['Fill_ForegroundColor'];
      $fillbg = $tstyle['Fill_BackgroundColor'];
      $lyneStyle = $tstyle['Stroke_LineStyle'];
      $thickness = $tstyle['Srtoke_Thickness'];
      $scolor = $tstyle['Stroke_Color'];
      $sunits = $tstyle['Stroke_Unit'];
      $dunits = $tstyle['Stroke_SizeContext'];
      $transparent = @$tstyle['Transparency']?$tstyle['Transparency']:'FF';

      $rule = "
      <AreaRule>
         <LegendLabel></LegendLabel>
         <Filter>$filter</Filter>
         <AreaSymbolization2D>
             <Fill>
                 <FillPattern>$fillPattern</FillPattern>
                 <ForegroundColor>{$transparent}{$color}</ForegroundColor>
                 <BackgroundColor>$fillbg</BackgroundColor>
             </Fill>
             <Stroke>
                 <LineStyle>$lyneStyle</LineStyle>
                 <Thickness>$thickness</Thickness>
                 <Color>FF$scolor</Color>
                 <Unit>$sunits</Unit>
                 <SizeContext>$dunits</SizeContext>
             </Stroke>
         </AreaSymbolization2D>
     </AreaRule>";

							// Create a line type style.
     $typeStyle = $factory->CreateAreaTypeStyle($rule);
 }
 break;
}

$scaleRange = $factory->CreateScaleRange($minScale, $maxScale, $typeStyle);

$layerDefinition = $factory->CreateLayerDefinition(
    $mgLayer->GetFeatureSourceId(),
    $mgLayer->GetFeatureClassName(),
    $mgLayer->GetFeatureGeometryName(),
    $scaleRange
    );

	  			//print $layerDefinition;

$newLayer = $this->add_layer_definition_to_map(
 $layerDefinition,
 $themeLayerName,
 encode($tlayer['LEGEND']),
 $mgSessionId,
 $resourceSrvc,
 $map
 );


	  			//print $newLayer;

$this->add_layer_to_group(
 $newLayer,
 $themeName,
 encode($theme['NAME']),
 $map,
					$theme_order //������������ ����� ������� ������
                  );

$layerCollection = $map->GetLayers();
if ($layerCollection->Contains($themeLayerName))
{
 $squareFootageLayer = $layerCollection->GetItem($themeLayerName);
 $squareFootageLayer->SetVisible(true);
}

$sessionIdName = "Session:$mgSessionId//$mgMapName.Map";
$sessionResourceID = new MgResourceIdentifier($sessionIdName);
$sessionResourceID->Validate();
$map->Save($resourceSrvc, $sessionResourceID);

}
}


function getLayerSummary($mapName){
  if($mapName){
			//$layersArray = array();
        	//$layersGroupArray = array();
   $visibleArray = array();
   $visibleNames = array();
   $visibleRids = array();
   $visibleFids = array();

   $this->mgTier = lmbToolkit :: instance() -> getMgTier();
   $siteConnection = $this->mgTier->getSiteConnection();
   $map = new MgMap($siteConnection);

   $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

   $map->Open($resourceService, $mapName);

   $layers = $map->GetLayers();
   $layersCount = $layers->GetCount();

   for ($i = 0; $i < $layersCount; $i++) {
    $layer = $layers->GetItem($i);
    if ($layer->isVisible()) {
       $layerDefinition = $layer->GetLayerDefinition();
       $visibleArray[] = sprintf("%u",crc32($layerDefinition->GetPath().$layerDefinition->GetName().$layer->GetName()));
       $visibleNames[] = $layer->GetName();
       $visibleRids[] = 'Library://'.$layerDefinition->GetPath().'/'.$layerDefinition->GetName().'.LayerDefinition';
   }
}
			//$featureLayers = $this->mgTier->getMgLayerService()->getMgLayerListByFLayerIdsI($visibleRids);
foreach($visibleRids as $key=>$val){

    $featureLayer = $this->wgsTier->getMgLayerService()->getMgLayerListByFLayerIdsI($val);
    foreach($featureLayer as $k=>$v){
       $visibleFids[] = $v->get('FEATURELAYER_ID');
   }
}
			//die();
$layerGroups = $map->GetLayerGroups();
$layerGroupsCount = $layerGroups->GetCount();

for ($i = 0; $i < $layerGroupsCount; $i++) {
    $layerGroup = $layerGroups->GetItem($i);
    if ($layerGroup->isVisible()) {
       $visibleArray[] = sprintf("%u",crc32($layerGroup->GetName()));
   }
}

			//echo json_encode(array('l' => $layersArray, 'lg' => $layersGroupArray));

return array('l' => $visibleArray, 'lg' => $visibleNames, 'fl' => $visibleRids, 'fli' => $visibleFids);
}
}
function getLayerVisibility($mapName)
{
    if ($mapName) {
        	//$layersArray = array();
        	//$layersGroupArray = array();
       $visibleArray = array();

       $this->mgTier = lmbToolkit :: instance() -> getMgTier();
       $siteConnection = $this->mgTier->getSiteConnection();

       $map = new MgMap($siteConnection);

       $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

       $map->Open($resourceService, $mapName);

       $layers = $map->GetLayers();
       $layersCount = $layers->GetCount();

       for ($i = 0; $i < $layersCount; $i++) {
        $layer = $layers->GetItem($i);
        if ($layer->isVisible()) {
           $layerDefinition = $layer->GetLayerDefinition();
           $visibleArray[] = sprintf("%u",crc32($layerDefinition->GetPath().$layerDefinition->GetName().$layer->GetName()));
       }
   }

   $layerGroups = $map->GetLayerGroups();
   $layerGroupsCount = $layerGroups->GetCount();

   for ($i = 0; $i < $layerGroupsCount; $i++) {
    $layerGroup = $layerGroups->GetItem($i);
    if ($layerGroup->isVisible()) {
       $visibleArray[] = sprintf("%u",crc32($layerGroup->GetName()));
   }
}

			//echo json_encode(array('l' => $layersArray, 'lg' => $layersGroupArray));

return $visibleArray;
			/*
			return array(
				'l' => $layersArray,
				'lg' => $layersGroupArray
			);
			*/
			//print_r($layersArray);
			//print_r($layersGroupArray);
}
}
}

?>
