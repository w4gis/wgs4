<?php
lmb_require('wgs/controller/wgsFrontController.class.php');
lmb_require('wgs/controller/MapController.class.php');
lmb_require('limb/i18n/utf8.inc.php');


class FusionController extends wgsFrontController
{
    var $maps;
    var $mapName;

    function __construct(){
        parent::__construct();

        $mapController = new MapController();

        $this -> maps = $mapController -> getAllMaps();
        $this -> mapName = "tusur";

        $mapName = $_GET['map'] ? $_GET['map'] : 'tusur';

        foreach($this -> maps as $map) {
            if($mapName == $map['mapName']) {
                $this -> mapName = $map['mapName'];
                break;
            }

        }


    }

    function doLoad() {
        include '/../../../../application/www/fusion/layers/MapGuide/php/LoadMap.php';
    }
    function doLoadScale() {
        include '/../../../../application/www/fusion/layers/MapGuide/php/LoadScaleRanges.php';
    }
    function doSelection() {
        include '/../../../../application/www/fusion/layers/MapGuide/php/Selection.php';
    }
    function doSaveSelection() {
        include '/../../../../application/www/fusion/layers/MapGuide/php/SaveSelection.php';
    }

    function doCreateBuffer() {
        include '/../../../../application/www/fusion/layers/MapGuide/php/Buffer.php';
    }
    function doDisplay() {
        echo 'default';
    }

    function getMapWidgets() {
        $resultStr = "";
        $mapName = $this -> mapName;

            $resultStr .= "
        <WidgetSet>
            <MapWidget xsi:type=\"WidgetType\">
                <Name>Map-$mapName</Name>
                <Type>Map</Type>
                <StatusItem></StatusItem>
                <Extension xsi:type=\"CustomContentType\">
                    <MenuContainer>MapContextMenu</MenuContainer>
                    <!--Scales>100000,50000,25000,12500,8000,4000</Scales-->
                </Extension>
                <MapId>" . ( $mapName == "tusur" ? "openLayers-$mapName" : "noOpenLayers-$mapName" ). "</MapId>
            </MapWidget>

            <Widget xsi:type=\"UiWidgetType\">
              <Name>MapMenu-$mapName</Name>
              <Type>MapMenu</Type>
                  <Location />
                  <Extension>
                    <Folder></Folder>
                  </Extension>
                  <ImageUrl />
                  <ImageClass />
                  <Label>Maps</Label>
                  <Tooltip>Choose a map theme</Tooltip>
                  <StatusText />
                  <Disabled>false</Disabled>
            </Widget>


            <Container xsi:type=\"UiItemContainerType\">
              <Name>Modebar-$mapName</Name>
              <Type>Toolbar</Type>
              <Position>Top</Position>
              <Item xsi:type=\"WidgetItemType\">
                <Function>Widget</Function>
                <Widget>Maptip-$mapName</Widget>
              </Item>
            </Container>




            <!-- Pan -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>Pan-$mapName</Name>
              <Location>Top</Location>
              <Type>Pan</Type>
              <StatusItem>Select features by clicking and dragging.</StatusItem>
              <ImageUrl/>
              <ImageClass/>
              <Tooltip>${'SELECTION_'.$lang}</Tooltip>
              <Label>&lt;i class=\"fa margined fa-arrows\"&gt;&lt;/i&gt; ${'NAVIGATION_'.$lang} </Label>
              <Disabled/>
            </Widget>

            <!-- SELECT -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>Select-$mapName</Name>
              <Location>Top</Location>
              <Type>Select</Type>
              <StatusItem>Select features by clicking and dragging.</StatusItem>
              <ImageUrl/>
              <ImageClass/>
              <Tooltip>${'SELECTION_'.$lang}</Tooltip>
              <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt; ${'SELECTION_'.$lang} </Label>
              <Disabled/>
            </Widget>

            <!-- Zoom In Rectangle -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>ZoomIn-$mapName</Name>
              <Location>Top</Location>
              <Type>Zoom</Type>
              <StatusItem>Zoom in on an area.</StatusItem>
              <Extension xsi:type=\"CustomContentType\">
                <Tolerance>5</Tolerance>
                <Factor>2</Factor>
              </Extension>
              <ImageUrl/>
              <ImageClass/>
              <Tooltip>${'ZOOMIN_'.$lang}</Tooltip>
              <Label>Zoom In Area</Label>
              <Disabled/>
            </Widget>

            <!-- ZOOM ON CLICK (IN) -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>ZoomInFixed-$mapName</Name>
              <Type>ZoomOnClick</Type>
              <StatusItem>Zoom in by a preset increment.</StatusItem>
              <Extension xsi:type=\"CustomContentType\">
                <Factor>2</Factor>
              </Extension>
              <ImageUrl/>
              <ImageClass/>
              <Tooltip>${'ZOOMIN_SIMPLE_'.$lang}</Tooltip>
              <Label>Zoom In</Label>
              <Disabled/>
            </Widget>

            <!-- ZOOM ON CLICK (OUT) -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>ZoomOutFixed-$mapName</Name>
              <Type>ZoomOnClick</Type>
              <StatusItem>Zoom out by a preset increment</StatusItem>
              <Extension xsi:type=\"CustomContentType\">
                <Factor>0.5</Factor>
              </Extension>
              <ImageUrl/>
              <ImageClass/>
              <Tooltip>${'ZOOMOUT_SIMPLE_'.$lang}</Tooltip>
              <Label>Zoom Out</Label>
              <Disabled/>
            </Widget>


            <!-- SELECT RADIUS
            <Widget xsi:type=\"UiWidgetType\">
              <Name>SelectRadius-$mapName</Name>
              <Type>SelectRadius</Type>
              <Location />
              <Extension>
                <SelectionType>INTERSECTS</SelectionType>
                <Tolerance>3</Tolerance>
                <DefaultRadius>20</DefaultRadius>
                <RadiusTooltipContainer></RadiusTooltipContainer>
                <RadiusTooltipType>dynamic</RadiusTooltipType>
              </Extension>
              <ImageUrl>images/icons.png</ImageUrl>
              <ImageClass>select-radius</ImageClass>
              <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt;&lt;i class=\"fa margined fa-icon-sup fa-circle-o\"&gt;&lt;/i&gt; ${'SELECT_RADIUS_'.$lang} </Label>
              <Tooltip>Click to select within a radius</Tooltip>
              <StatusText />
              <Disabled>false</Disabled>
            </Widget>
            <Widget xsi:type=\"UiWidgetType\">
              <Name>SelectRadiusValue-$mapName</Name>
              <Type>SelectRadiusValue</Type>
              <Location />
              <Extension>
                <RadiusName>SelectRadius</RadiusName>
                <Label></Label>
                <ClassName></ClassName>
              </Extension>
              <ImageUrl />
              <ImageClass />
              <Label>Select radius value</Label>
              <Tooltip>enter the radius for the Select by Radius tool</Tooltip>
              <StatusText />
              <Disabled>false</Disabled>
            </Widget>-->

            <!--SELECT POLYGON -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>SelectPolygon-$mapName</Name>
              <Type>SelectPolygon</Type>
              <Location />
              <Extension>
                <SelectionType>INTERSECTS</SelectionType>
                <Tolerance>3</Tolerance>
              </Extension>
              <ImageUrl>images/icons.png</ImageUrl>
              <ImageClass>select-polygon</ImageClass>
              <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt;&lt;i class=\"fa margined fa-square-o fa-icon-sup\"&gt;&lt;/i&gt;  ${'SELECT_POLYGON_'.$lang} </Label>
              <Tooltip>Draw a polygon to perform a selection</Tooltip>
              <StatusText />
              <Disabled>false</Disabled>
            </Widget>

			<!--MeasureDistanceTool -->
            <Widget xsi:type=\"UiWidgetType\">
          		<Name>Measure-$mapName</Name>
          		<Type>Measure</Type>
          		<Location />
          		<Extension>
          		  <Type>distance</Type>
          		  <MeasureTooltipContainer>MeasureResult</MeasureTooltipContainer>
          		  <MeasureTooltipType>dynamic</MeasureTooltipType>
          		  <DistancePrecision>0</DistancePrecision>
          		  <AreaPrecision>0</AreaPrecision>
          		  <Units>meters</Units>
          		 <!-- <Target>TaskPane</Target>-->
          		</Extension>
          		<ImageUrl>images/icons.png</ImageUrl>
          		<ImageClass>measure</ImageClass>
          		<Label>&lt;i class=\"fa margined fa-arrows-h\"&gt;&lt;/i&gt; ${'MEASUREMENT_'.$lang} </Label>
          		<Tooltip>Measure</Tooltip>
          		<StatusText>Measure distances and areas on the map</StatusText>
          		<Disabled>false</Disabled>
        	</Widget>


            <!-- PREV, NEXT, FULL Extent buttons -->
            <Widget xsi:type=\"UiWidgetType\">
              <Name>PreviousView-$mapName</Name>
              <Type>ExtentHistory</Type>
              <Location />
              <Extension>
                <Direction>previous</Direction>
              </Extension>
              <ImageClass>view-back</ImageClass>
              <Label>&lt;i class=\"fa fa-mail-reply\"&gt;&lt;/i&gt;&lt;/br&gt;&lt;span class=\"icon-title\"&gt;Предыдущий вид&lt;/span&gt;</Label>
              <Tooltip>Go to previous view</Tooltip>
              <StatusText>Go to previous view</StatusText>
              <Disabled>false</Disabled>
            </Widget>

            <Widget xsi:type=\"UiWidgetType\">
              <Name>NextView-$mapName</Name>
              <Type>ExtentHistory</Type>
              <Location />
              <Extension>
                <Direction>next</Direction>
              </Extension>
              <ImageClass>view-forward</ImageClass>
              <Label>&lt;i class=\"fa fa-mail-forward\"&gt;&lt;/i&gt;&lt;/br&gt;&lt;span class=\"icon-title\"&gt;Следующий вид&lt;/span&gt;</Label>
              <Tooltip>Go to next view</Tooltip>
              <StatusText>Go to next view</StatusText>
              <Disabled>false</Disabled>
            </Widget>

            <Widget xsi:type=\"UiWidgetType\">
              <Name>InitialMapView-$mapName</Name>
              <Type>InitialMapView</Type>
              <Location />
              <Extension>
                <ViewType>initial</ViewType>
              </Extension>
              <ImageClass>zoom-full</ImageClass>
              <Label>&lt;i class=\"fa fa-arrows-alt\"&gt;&lt;/i&gt;</Label>
              <Tooltip>Click to zoom to the full map extents</Tooltip>
              <StatusText />
              <Disabled>false</Disabled>
            </Widget>


            <!-- LEGEND -->
            <Widget xsi:type=\"WidgetType\">
              <Name>legend-$mapName</Name>
              <Type>Legend</Type>
              <Location />
              <Extension>
                <HideInvisibleLayers>true</HideInvisibleLayers>
                <ShowRootFolder>false</ShowRootFolder>
                <ShowMapFolder>false</ShowMapFolder>
                <LayerRasterIcon>images/icons/legend-raster.png</LayerRasterIcon>
                <LayerDWFIcon>images/icons/legend-DWF.png</LayerDWFIcon>
                <LayerThemeIcon>images/icons/legend-theme.png</LayerThemeIcon>
                <DisabledLayerIcon>images/icons/legend-layer.png</DisabledLayerIcon>
                <LayerInfoIcon>images/icons/tree_layer_info.png</LayerInfoIcon>
                <GroupInfoIcon>images/icons/tree_group_info.png</GroupInfoIcon>
                <RootFolderIcon>images/icons/legend-map.png</RootFolderIcon>
              </Extension>
            </Widget>

            <!-- MapTip -->
            <Widget xsi:type=\"WidgetType\">
              <Name>Maptip-$mapName</Name>
              <Type>Maptip</Type>
              <Location />
              <Extension>
                <Delay>350</Delay>
                <Layer />
                <Tolerance>2</Tolerance>
                <Target>MaptipWindow</Target>
                <WinFeatures>menubar=no,location=no,resizable=no,status=no</WinFeatures>
              </Extension>
              <ImageUrl>images/icons.png</ImageUrl>
              <ImageClass>maptip</ImageClass>
              <Label>Map Tooltips</Label>
              <Tooltip>Click to Enable/Disable get information about features from Server</Tooltip>
              <StatusText />
              <Disabled>false</Disabled>
            </Widget>

            <!-- STATUS BAR-->
            <Widget>
              <Name>StatusCoords-$mapName</Name>
              <Type>CursorPosition</Type>
              <StatusText/>
              <Extension>
                <Template>X: {x} {units}, Y: {y} {units}</Template>
                <Precision>4</Precision>
                <EmptyText>...</EmptyText>
              </Extension>
            </Widget>

            <!-- STATUS SELECTION INFO
            <Widget>
              <Name>StatusSelection-$mapName</Name>
              <Type>SelectionInfo</Type>
              <StatusText/>
              <Extension>
                  <EmptyText>Выделение отсутствует</EmptyText>
             </Extension>
            </Widget>-->

            <!-- STATUS SCALE -->
            <Widget>
              <Name>StatusScale-$mapName</Name>
              <Type>EditableScale</Type>
              <StatusText/>
            </Widget>

            <!-- STATUS VIEWSIZE -->

            <Widget>
                <Name>StatusViewSize-$mapName</Name>
                <Type>ViewSize</Type>
                <StatusText/>
                <Extension>
                   <Template>{w} x {h} ({units})</Template>
                   <Precision>2</Precision>
                </Extension>
            </Widget>


            <!--<Widget xsi:type=\"UiWidgetType\">
                      <Name>FeatureInfo</Name>
                      <Type>FeatureInfo</Type>
                      <Location />
                      <Extension>
                        <Target>TaskPane</Target>
                      </Extension>
                      <ImageUrl>images/icons.png</ImageUrl>
                      <ImageClass>featureinfo</ImageClass>
                      <Label>Feature Info</Label>
                      <Tooltip>Click to display selected feature info</Tooltip>
                      <StatusText />
                      <Disabled>false</Disabled>
                    </Widget>-->

              <Widget xsi:type=\"UiWidgetType\">
                  <Name>QuickPlot-$mapName</Name>
                  <Type>QuickPlot</Type>
                  <Location />
                  <Extension>
                    <Target>TaskPane-$mapName</Target>
                    <RememberPlotOptions>false</RememberPlotOptions>
                    <ShowCoordinateLabels>true</ShowCoordinateLabels>
                    <ShowSubTitle>true</ShowSubTitle>
                    <DefaultDpi>96</DefaultDpi>
                    <PaperListEntry></PaperListEntry>
                    <ScaleListEntry></ScaleListEntry>
                    <DefaultMargin>25.4,12.7,12.7,12.7</DefaultMargin>
                    <Disclaimer>The materials available at this web site are for informational purposes only and do not constitute a legal document.</Disclaimer>
                  </Extension>
                  <ImageUrl>images/icons/print.png</ImageUrl>
                  <ImageClass />
                  <Label>Quick Plot</Label>
                  <Tooltip>Click to create a plot quickly</Tooltip>
                  <StatusText />
                  <Disabled>false</Disabled>
              </Widget>

             <Widget xsi:type=\"WidgetType\">
                <Name>TaskPane-$mapName</Name>
                <Type>TaskPane</Type>
                <StatusItem/>
                <Extension>
                  <InitialTask></InitialTask>
                  <MenuContainer>TaskMenu</MenuContainer>
                </Extension>
              </Widget>

        </WidgetSet>";




        return $resultStr;
    }

    function getAllWidgets() {

    }

    function getMapSets($projectName) {
        $resultStr = "";
        $mapName = $this -> mapName;

        $resultStr .= "
         <MapGroup id=\"noOpenLayers-$mapName\">
            <Map>
              <Type>MapGuide</Type>
              <SingleTile>True</SingleTile>
              <Extension>
                <ResourceId>Library://$projectName/maps/$mapName.MapDefinition</ResourceId>
                <ImageFormat>PNG8</ImageFormat>
              </Extension>
            </Map>
          </MapGroup>
         <MapGroup id=\"openLayers-$mapName\">
            <Map>
              <Type>MapGuide</Type>
              <SingleTile>True</SingleTile>
              <Extension>
                <ResourceId>Library://$projectName/maps/$mapName.MapDefinition</ResourceId>
                <ImageFormat>PNG8</ImageFormat>
                <Options>
                  <isBaseLayer>false</isBaseLayer>
                  <useOverlay>true</useOverlay>
                  <projection>EPSG:900913</projection>
                </Options>
              </Extension>
            </Map>
            <Map>
              <Type>OpenStreetMap</Type>
              <SingleTile>true</SingleTile>
              <Extension>
                <Options>
                  <name>Open Street Map</name>
                  <type>Mapnik</type>
                </Options>
              </Extension>
            </Map>
            <!--<Map>
              <Type>Stamen</Type>
              <SingleTile>true</SingleTile>
              <Extension>
                <Options>
                  <name>Stamen Toner</name>
                  <type>toner</type>
                </Options>
              </Extension>
            </Map>-->
          </MapGroup>";

        return $resultStr;
    }

    function doGetApplicationDefinition(){

        $lang = strtoupper($_GET['lang'] ? $_GET['lang'] : 'ru');

        $ok = false;
        $langs = array('EN','RU');
        foreach($langs as $l){
            if($l == $lang)
                $ok = true;
        }
        if(!$ok) {
            $lang = 'RU';
        }

        $projectName = getProjectName();
        $mapname = /*"Tusur";//*/getProjectName();

        $NAVIGATION_RU = "Панорамирование";
        $NAVIGATION_EN = "Pan";

        $ZOOMIN_RU = "Увеличить масштаб до выбранной зоны";
        $ZOOMIN_EN = "Zoom in to a rectangular region.";

        $ZOOMIN_SIMPLE_RU = "Увеличить масштаб карты";
        $ZOOMIN_SIMPLE_EN = "Zoom in.";

        $ZOOMOUT_SIMPLE_RU = "Уменьшить масштаб карты";
        $ZOOMOUT_SIMPLE_EN = "Zoom out.";

        $NAVIGATION_DESCR_RU = "Кликните на карте и перетащите указатель мыши для панорамирования";
        $NAVIGATION_DESCR_EN = "Click and drag the map to pan";


        $CLEAR_SELECTION_RU = "Очистить выделение";
        $CLEAR_SELECTION_EN = "Clear selection";

        $SELECTION_RU = "Выбор объектов";
        $SELECTION_EN = "Select objects";

        $SELECT_RADIUS_RU = "Выделение окружностью";
        $SELECT_RADIUS_EN = "Select within circle";

        $SELECT_POLYGON_RU = "Выделение полигоном";
        $SELECT_POLYGON_EN = "Select within polygon";

        $MEASUREMENT_RU = "Линейка";
        $MEASUREMENT_EN = "Measurement";

        $webLayoutId = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
        $prohibitedCommandList = [];
        $simpleMode = false;
        $acl        = $this->wgsTier->getACL();
        $user       = $this->wgsTier->getAuthorizedUser();

        if ($customCommandList = $this->wgsTier->getProjectObjectService()->getCustomCommandList()) {
            foreach ($customCommandList as $customCommand) {
                if ($customCommand->getMgWebLayoutId() == $webLayoutId &&
                    ($simpleMode || !$acl->isAllowed($user, $customCommand, wgsPrivilege::MGCUSTOMCOMMAND_ACCESS)))
                        $prohibitedCommandList[ $customCommand->getName() ] = true;
            }
        };

        return "
            <?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <ApplicationDefinition
                  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
                  xsi:noNamespaceSchemaLocation=\"ApplicationDefinition-1.0.0.xsd\">
              <Title>WGS4 :: ГИС ЭГП</Title>
              <MapSet xsi:type=\"MapSetType\">

              " . $this -> getMapSets($projectName) . "
              </MapSet>
                " . $this -> getMapWidgets($projectName) ."
              <Extension>
                <StamenScript>http://maps.stamen.com/js/tile.stamen.js?v1.3.0</StamenScript>
              </Extension>
            </ApplicationDefinition>";
    }

    function getWidget($widgetName) {

    }

    function doGetApplicationDefinitionM(){

    $lang = $_GET['lang'] ? $_GET['lang'] : 'ru';
    $ok = false;
    $lang = strtoupper($lang);

    $langs = array('EN','RU');
    foreach($langs as $l){
      if($l == $lang)
        $ok = true;
    }
    if(!$ok)
      $lang = 'RU';

    $mapname = getProjectName();

    $NAVIGATION_RU = "Панорамирование";
    $NAVIGATION_EN = "Pan";

    $ZOOMIN_RU = "Увеличить масштаб до выбранной зоны";
    $ZOOMIN_EN = "Zoom in to a rectangular region.";

    $ZOOMIN_SIMPLE_RU = "Увеличить масштаб карты";
    $ZOOMIN_SIMPLE_EN = "Zoom in.";

    $ZOOMOUT_SIMPLE_RU = "Уменьшить масштаб карты";
    $ZOOMOUT_SIMPLE_EN = "Zoom out.";

    $NAVIGATION_DESCR_RU = "Кликните на карте и перетащите указатель мыши для панорамирования";
    $NAVIGATION_DESCR_EN = "Click and drag the map to pan";


    $CLEAR_SELECTION_RU = "Очистить выделение";
    $CLEAR_SELECTION_EN = "Clear selection";

    $SELECTION_RU = "Выбор объектов";
    $SELECTION_EN = "Select objects";

    $SELECT_RADIUS_RU = "Выделение окружностью";
    $SELECT_RADIUS_EN = "Select within circle";

    $SELECT_POLYGON_RU = "Выделение полигоном";
    $SELECT_POLYGON_EN = "Select within polygon";

    $MEASUREMENT_RU = "Линейка";
    $MEASUREMENT_EN = "Measurement";

    $webLayoutId = $this->wgsTier->getProjectObjectService()->getMgWebLayoutService()->getDefaultWebLayout()->getId();
    $prohibitedCommandList = [];
    $simpleMode = false;
    $acl        = $this->wgsTier->getACL();
    $user       = $this->wgsTier->getAuthorizedUser();

    if ($customCommandList = $this->wgsTier->getProjectObjectService()->getCustomCommandList()) {
      foreach ($customCommandList as $customCommand) {
        if ($customCommand->getMgWebLayoutId() == $webLayoutId &&
          ($simpleMode || !$acl->isAllowed($user, $customCommand, wgsPrivilege::MGCUSTOMCOMMAND_ACCESS)))
            $prohibitedCommandList[ $customCommand->getName() ] = true;
      }
    };

    return "
      <?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <ApplicationDefinition
      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
      xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
      xsi:noNamespaceSchemaLocation=\"ApplicationDefinition-1.0.0.xsd\">
  <Title>WGS4 :: ГИС ЭГП</Title>
  <MapSet xsi:type=\"MapSetType\">
    <MapGroup id=\"noOpenLayers\">
      <Map>
        <Type>MapGuide</Type>
        <SingleTile>True</SingleTile>
        <Extension>
          <ResourceId>Library://$mapname/maps/$mapname.MapDefinition</ResourceId>
          <ImageFormat>PNG8</ImageFormat>
        </Extension>
      </Map>
    </MapGroup>
    <MapGroup id=\"openLayers\">
      <Map>
        <Type>MapGuide</Type>
        <SingleTile>True</SingleTile>
        <Extension>
          <ResourceId>Library://$mapname/maps/$mapname.MapDefinition</ResourceId>
          <ImageFormat>PNG8</ImageFormat>
          <Options>
            <isBaseLayer>false</isBaseLayer>
            <useOverlay>true</useOverlay>
            <projection>EPSG:900913</projection>
          </Options>
        </Extension>
      </Map>
      <Map>
        <Type>Google</Type>
        <SingleTile>true</SingleTile>
        <Extension>
          <Options>
            <name>Google Satellite</name>
            <type>G_SATELLITE_MAP</type>
          </Options>
        </Extension>
      </Map>
      <Map>
        <Type>Google</Type>
        <SingleTile>true</SingleTile>
        <Extension>
          <Options>
            <name>Google Streets</name>
            <type>G_NORMAL_MAP</type>
          </Options>
        </Extension>
      </Map>
      <Map>
        <Type>OpenStreetMap</Type>
        <SingleTile>true</SingleTile>
        <Extension>
          <Options>
            <name>Open Street Map</name>
            <type>Mapnik</type>
          </Options>
        </Extension>
      </Map>
      <Map>
        <Type>OpenStreetMap</Type>
        <SingleTile>true</SingleTile>
        <Extension>
          <Options>
            <name>Open Street Map (TransportMap)</name>
            <type>TransportMap</type>
          </Options>
        </Extension>
      </Map>
    </MapGroup>
  </MapSet>
  <WidgetSet>
    <MapWidget xsi:type=\"WidgetType\">
      <Name>Map</Name>
      <Type>Map</Type>
      <StatusItem></StatusItem>
      <Extension xsi:type=\"CustomContentType\">
        <MenuContainer>MapContextMenu</MenuContainer>
        <!--Scales>100000,50000,25000,12500,8000,4000</Scales-->
      </Extension>
      <MapId>noOpenLayers</MapId>
    </MapWidget>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>MapMenu</Name>
      <Type>MapMenu</Type>
      <Location />
      <Extension>
        <Folder></Folder>
      </Extension>
      <ImageUrl />
      <ImageClass />
      <Label>Maps</Label>
      <Tooltip>Choose a map theme</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>


    <Container xsi:type=\"UiItemContainerType\">
      <Name>ToolbarVertical</Name>
      <Type>Toolbar</Type>
      <Position>left</Position>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Pan</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Select</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>ZoomIn</Widget>
      </Item>

    </Container>

    <Container xsi:type=\"UiItemContainerType\">
      <Name>TaskMenu</Name>
      <Type>ContextMenu</Type>
      <Position>top</Position>
      <Extension />

      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Redline</Widget>
      </Item>

    </Container>


    <Widget xsi:type=\"UiWidgetType\">
      <Name>Redline</Name>
      <Type>Redline</Type>
      <Location />
      <Extension>
        <Target>TaskPane</Target>
        <AutogenerateLayerNames>true</AutogenerateLayerNames>
        <UseMapMessage>true</UseMapMessage>
        <DataStoreFormat></DataStoreFormat>
        <RedlineGeometryFormat>7</RedlineGeometryFormat>
        <AutoCreateOnStartup>true</AutoCreateOnStartup>
        <StylizationType>basic</StylizationType>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>redline</ImageClass>
      <Label>Redline</Label>
      <Tooltip>Click to draw redline features</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>


    <!-- SWITCH BETWEEN EXTERNAL MAP PROVIDERS-->
    <Widget xsi:type=\"UiWidgetType\">
      <Name>BasemapSwitcher</Name>
      <Type>BasemapSwitcher</Type>
      <Location />
      <Extension />
      <Label>Подложка</Label>
      <Tooltip>Click to change the basemap</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>


    <!-- MY LOCATION WIDGET -->
    <Widget xsi:type=\"UiWidgetType\">
      <Name>Geolocation</Name>
      <Type>Geolocation</Type>
      <Location />
      <Extension>
        <ZoomLevel>15000</ZoomLevel>
        <EnableHighAccuracy>false</EnableHighAccuracy>
        <Timeout>10000</Timeout>
        <MaximumAge>0</MaximumAge>
      </Extension>
      <Label>My Location</Label>
      <Tooltip>Click to zoom to your current geographic location</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

    <!-- Pan -->
    <Widget xsi:type=\"UiWidgetType\">
      <Name>Pan</Name>
      <Type>Pan</Type>
      <StatusText/>
      <Extension/>
      <ImageUrl/>
      <ImageClass/>
      <Label>&lt;i class=\"fa margined fa-arrows\"&gt;&lt;/i&gt; ${'NAVIGATION_'.$lang} </Label>
      <Tooltip>${'NAVIGATION_DESCR_'.$lang}</Tooltip>
      <Disabled/>
    </Widget>

    <!-- SELECT -->

    <Widget xsi:type=\"UiWidgetType\">
      <Name>Select</Name>
      <Type>Select</Type>
      <StatusItem>Select features by clicking and dragging.</StatusItem>
      <ImageUrl/>
      <ImageClass/>
      <Tooltip>${'SELECTION_'.$lang}</Tooltip>
      <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt; ${'SELECTION_'.$lang} </Label>
      <Disabled/>
    </Widget>


    <!-- Zoom In Rectangle -->
    <Widget xsi:type=\"UiWidgetType\">
      <Name>ZoomIn</Name>
      <Type>Zoom</Type>
      <StatusItem>Zoom in on an area.</StatusItem>
      <Extension xsi:type=\"CustomContentType\">
        <Tolerance>5</Tolerance>
        <Factor>2</Factor>
      </Extension>
      <ImageUrl/>
      <ImageClass/>
      <Tooltip>${'ZOOMIN_'.$lang}</Tooltip>
      <Label>Zoom In Area</Label>
      <Disabled/>
    </Widget>

  <!-- ZOOM ON CLICK (IN) -->

    <Widget xsi:type=\"UiWidgetType\">
      <Name>ZoomInFixed</Name>
      <Type>ZoomOnClick</Type>
      <StatusItem>Zoom in by a preset increment.</StatusItem>
      <Extension xsi:type=\"CustomContentType\">
        <Factor>2</Factor>
      </Extension>
      <ImageUrl/>
      <ImageClass/>
      <Tooltip>${'ZOOMIN_SIMPLE_'.$lang}</Tooltip>
      <Label>Zoom In</Label>
      <Disabled/>
    </Widget>

  <!-- ZOOM ON CLICK (OUT) -->

    <Widget xsi:type=\"UiWidgetType\">
      <Name>ZoomOut</Name>
      <Type>ZoomOnClick</Type>
      <StatusItem>Zoom out by a preset increment</StatusItem>
      <Extension xsi:type=\"CustomContentType\">
        <Factor>0.5</Factor>
      </Extension>
      <ImageUrl/>
      <ImageClass/>
      <Tooltip>${'ZOOMOUT_SIMPLE_'.$lang}</Tooltip>
      <Label>Zoom Out</Label>
      <Disabled/>
    </Widget>

  <!-- LEGEND -->
  <Widget xsi:type=\"WidgetType\">
      <Name>legend</Name>
      <Type>Legend</Type>
      <Location />
      <Extension>
        <HideInvisibleLayers>true</HideInvisibleLayers>
        <ShowRootFolder>false</ShowRootFolder>
        <ShowMapFolder>false</ShowMapFolder>
        <LayerRasterIcon>images/icons/legend-raster.png</LayerRasterIcon>
        <LayerDWFIcon>images/icons/legend-DWF.png</LayerDWFIcon>
        <LayerThemeIcon>images/icons/legend-theme.png</LayerThemeIcon>
        <DisabledLayerIcon>images/icons/legend-layer.png</DisabledLayerIcon>
        <LayerInfoIcon>images/icons/tree_layer_info.png</LayerInfoIcon>
        <GroupInfoIcon>images/icons/tree_group_info.png</GroupInfoIcon>
        <RootFolderIcon>images/icons/legend-map.png</RootFolderIcon>
      </Extension>
    </Widget>


  <!-- STATUS POSITION -->

    <Widget>
      <Name>StatusCoords</Name>
      <Type>CursorPosition</Type>
      <StatusText/>
      <Extension>
        <Template>X: {x} {units}, Y: {y} {units}</Template>
        <Precision>4</Precision>
        <EmptyText></EmptyText>
      </Extension>
    </Widget>

  <!-- STATUS SELECTION INFO -->

    <Widget>
      <Name>StatusSelection</Name>
      <Type>SelectionInfo</Type>
      <StatusText/>
      <Extension>
          <EmptyText></EmptyText>
     </Extension>
    </Widget>

  <!-- STATUS SCALE -->

    <Widget>
      <Name>StatusScale</Name>
      <Type>EditableScale</Type>
      <StatusText/>
    </Widget>

  <!-- STATUS VIEWSIZE -->

    <Widget>
      <Name>StatusViewSize</Name>
      <Type>ViewSize</Type>
      <StatusText/>
      <Extension>
        <Template>{w} x {h} ({units})</Template>
        <Precision>2</Precision>
      </Extension>
    </Widget>


  <!--<Widget xsi:type=\"UiWidgetType\">
      <Name>FeatureInfo</Name>
      <Type>FeatureInfo</Type>
      <Location />
      <Extension>
        <Target>TaskPane</Target>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>featureinfo</ImageClass>
      <Label>Feature Info</Label>
      <Tooltip>Click to display selected feature info</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>-->
    <!-- ******************************************
  * Statusbar (Status Bar)
  *
  *
  ****************************************** -->
  <Widget xsi:type=\"UiWidgetType\">
      <Name>QuickPlot</Name>
      <Type>QuickPlot</Type>
      <Location />
      <Extension>
        <Target>TaskPane</Target>
        <RememberPlotOptions>false</RememberPlotOptions>
        <ShowCoordinateLabels>true</ShowCoordinateLabels>
        <ShowSubTitle>true</ShowSubTitle>
        <DefaultDpi>96</DefaultDpi>
        <PaperListEntry></PaperListEntry>
        <ScaleListEntry></ScaleListEntry>
        <DefaultMargin>25.4,12.7,12.7,12.7</DefaultMargin>
        <Disclaimer>The materials available at this web site are for informational purposes only and do not constitute a legal document.</Disclaimer>
      </Extension>
      <ImageUrl>images/icons/print.png</ImageUrl>
      <ImageClass />
      <Label>Quick Plot</Label>
      <Tooltip>Click to create a plot quickly</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>
    <Widget xsi:type=\"UiWidgetType\">
      <Name>Redline</Name>
      <Type>Redline</Type>
      <Location />
      <Extension>
        <Target>TaskPane</Target>
        <PromptForRedlineLabels>false</PromptForRedlineLabels>
        <AutogenerateLayerNames>true</AutogenerateLayerNames>
        <UseMapMessage>true</UseMapMessage>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>redline</ImageClass>
      <Label>Redline</Label>
      <Tooltip>Click to draw redline features</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>
    <Widget xsi:type=\"UiWidgetType\">
      <Name>RefreshMap</Name>
      <Type>RefreshMap</Type>
      <Location />
      <Extension />
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>view-refresh</ImageClass>
      <Label>Refresh</Label>
      <Tooltip>Click to redraw the map</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>
    <Widget xsi:type=\"UiWidgetType\">
      <Name>SaveMap</Name>
      <Type>SaveMap</Type>
      <Location />
      <Extension>
        <Format>png</Format>
        <Scale></Scale>
        <ResourceId></ResourceId>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>file-save</ImageClass>
      <Label>Save map</Label>
      <Tooltip>Click to save the map as an image</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

     <Widget xsi:type=\"UiWidgetType\">
      <Name>QuickPlot</Name>
      <Type>QuickPlot</Type>
      <Location />
      <Extension>
        <Target>TaskPane</Target>
        <RememberPlotOptions>false</RememberPlotOptions>
        <ShowCoordinateLabels>true</ShowCoordinateLabels>
        <ShowSubTitle>true</ShowSubTitle>
        <DefaultDpi>96</DefaultDpi>
        <PaperListEntry></PaperListEntry>
        <ScaleListEntry></ScaleListEntry>
        <DefaultMargin>25.4,12.7,12.7,12.7</DefaultMargin>
        <Disclaimer>The materials available at this web site are for informational purposes only and do not constitute a legal document.</Disclaimer>
      </Extension>
      <ImageUrl>images/icons/print.png</ImageUrl>
      <ImageClass />
      <Label>Super printing</Label>
      <Tooltip>Click to create a plot quickly</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

  <Container xsi:type=\"UiItemContainerType\">
      <Name>Topbar</Name>
      <Type>Toolbar</Type>
      <Position>top</Position>
      <Extension />
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Print</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>QuickPlot</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>SaveMap</Widget>
      </Item>
       <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>ClearSelection</Widget>
      </Item>
    </Container>

  <Container xsi:type=\"UiItemContainerType\">
      <Name>MapContextMenu</Name>
      <Type>ContextMenu</Type>
      <Position>top</Position>
      <Extension />

      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Pan</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Select</Widget>
      </Item>
      <Item xsi:type=\"SeparatorItemType\">
        <Function>Separator</Function>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>SelectRadius</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>SelectPolygon</Widget>
      </Item>
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>ClearSelection</Widget>
      </Item>
      <Item xsi:type=\"SeparatorItemType\">
        <Function>Separator</Function>
      </Item> " . ( !$prohibitedCommandList [ 'WgsMeasure' ] ? "
      <Item xsi:type=\"WidgetItemType\">
        <Function>Widget</Function>
        <Widget>Measure</Widget>
      </Item> " : "" ) . "

    </Container>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>Print</Name>
      <Type>Print</Type>
      <Location />
      <Extension>
        <ShowPrintUI>true</ShowPrintUI>
        <ShowTitle>false</ShowTitle>
        <PageTitle></PageTitle>
        <ShowLegend>false</ShowLegend>
        <ShowNorthArrow>false</ShowNorthArrow>
        <ImageBaseUrl></ImageBaseUrl>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>file-print</ImageClass>
      <Label>Print</Label>
      <Tooltip>Print the current map view</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>
    " . ( !$prohibitedCommandList [ 'WgsMeasure' ] ? "
  <Widget xsi:type=\"UiWidgetType\">
      <Name>Measure</Name>
      <Type>Measure</Type>
      <Location />
      <Extension>
        <Type>both</Type>
        <MeasureTooltipContainer>MeasureResult</MeasureTooltipContainer>
        <MeasureTooltipType>dynamic</MeasureTooltipType>
        <DistancePrecision>0</DistancePrecision>
        <AreaPrecision>0</AreaPrecision>
        <Units>meters</Units>
        <Target>TaskPane</Target>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>measure</ImageClass>
      <Label>&lt;i class=\"fa margined fa-arrows-h\"&gt;&lt;/i&gt; ${'MEASUREMENT_'.$lang} </Label>
      <Tooltip>Measure</Tooltip>
      <StatusText>Measure distances and areas on the map</StatusText>
      <Disabled>false</Disabled>
    </Widget>" : "" ) . "


  <Widget xsi:type=\"UiWidgetType\">
      <Name>SelectRadius</Name>
      <Type>SelectRadius</Type>
      <Location />
      <Extension>
        <SelectionType>INTERSECTS</SelectionType>
        <Tolerance>3</Tolerance>
        <DefaultRadius>20</DefaultRadius>
        <RadiusTooltipContainer></RadiusTooltipContainer>
        <RadiusTooltipType>dynamic</RadiusTooltipType>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>select-radius</ImageClass>
      <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt;&lt;i class=\"fa margined fa-icon-sup fa-circle-o\"&gt;&lt;/i&gt; ${'SELECT_RADIUS_'.$lang} </Label>
      <Tooltip>Click to select within a radius</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

  <Widget xsi:type=\"UiWidgetType\">
      <Name>SelectRadiusValue</Name>
      <Type>SelectRadiusValue</Type>
      <Location />
      <Extension>
        <RadiusName>SelectRadius</RadiusName>
        <Label></Label>
        <ClassName></ClassName>
      </Extension>
      <ImageUrl />
      <ImageClass />
      <Label>Select radius value</Label>
      <Tooltip>enter the radius for the Select by Radius tool</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>SelectPolygon</Name>
      <Type>SelectPolygon</Type>
      <Location />
      <Extension>
        <SelectionType>INTERSECTS</SelectionType>
        <Tolerance>3</Tolerance>
      </Extension>
      <ImageUrl>images/icons.png</ImageUrl>
      <ImageClass>select-polygon</ImageClass>
      <Label>&lt;i class=\"fa margined fa-hand-o-up\"&gt;&lt;/i&gt;&lt;i class=\"fa margined fa-square-o fa-icon-sup\"&gt;&lt;/i&gt;  ${'SELECT_POLYGON_'.$lang} </Label>
      <Tooltip>Draw a polygon to perform a selection</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>


    <Widget xsi:type=\"UiWidgetType\">
      <Name>PreviousView</Name>
      <Type>ExtentHistory</Type>
      <Location />
      <Extension>
        <Direction>previous</Direction>
      </Extension>
      <ImageClass>view-back</ImageClass>
      <Label>&lt;i class=\"fa fa-mail-reply\"&gt;&lt;/i&gt;&lt;/br&gt;&lt;span class=\"icon-title\"&gt;Предыдущий вид&lt;/span&gt;</Label>
      <Tooltip>Go to previous view</Tooltip>
      <StatusText>Go to previous view</StatusText>
      <Disabled>false</Disabled>
    </Widget>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>NextView</Name>
      <Type>ExtentHistory</Type>
      <Location />
      <Extension>
        <Direction>next</Direction>
      </Extension>
      <ImageClass>view-forward</ImageClass>
      <Label>&lt;i class=\"fa fa-mail-forward\"&gt;&lt;/i&gt;&lt;/br&gt;&lt;span class=\"icon-title\"&gt;Следующий вид&lt;/span&gt;</Label>
      <Tooltip>Go to next view</Tooltip>
      <StatusText>Go to next view</StatusText>
      <Disabled>false</Disabled>
    </Widget>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>vertInitialMapView</Name>
      <Type>InitialMapView</Type>
      <Location />
      <Extension>
        <ViewType>initial</ViewType>
      </Extension>
      <ImageClass>zoom-full</ImageClass>
      <Label>&lt;i class=\"fa fa-arrows-alt\"&gt;&lt;/i&gt;</Label>
      <Tooltip>Click to zoom to the full map extents</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

    <Widget xsi:type=\"UiWidgetType\">
      <Name>ClearSelection</Name>
      <Type>ClearSelection</Type>
      <Location />
      <Extension />
      <ImageUrl/>
      <ImageClass/>
      <Label>&lt;i class=\"fa margined fa-square-o\"&gt;&lt;/i&gt; ${'CLEAR_SELECTION_'.$lang} </Label>
      <Tooltip>Click to clear the current selection</Tooltip>
      <StatusText />
      <Disabled>false</Disabled>
    </Widget>

  <Widget xsi:type=\"WidgetType\">
      <Name>TaskPane</Name>
      <Type>TaskPane</Type>
      <StatusItem/>
      <Extension>
        <InitialTask></InitialTask>
        <MenuContainer>TaskMenu</MenuContainer>
      </Extension>
    </Widget>
  </WidgetSet>


  <Extension />
  </ApplicationDefinition>";
  }
}


?>
