<?php
/**********************************************************************************
 * Copyright 2004 BIT, Ltd. http://limb-project.com, mailto: support@limb-project.com
 *
 * Released under the LGPL license (http://www.gnu.org/copyleft/lesser.html)
 ***********************************************************************************
 *
 * $Id: wgsWebApplication.class.php 4728 2007-01-18 14:03:48Z fisher $
 *
 ***********************************************************************************/
lmb_require('limb/filter_chain/src/lmbFilterChain.class.php');
lmb_require('limb/core/src/lmbHandle.class.php');

class wgsWebApplication extends lmbFilterChain
{
    function __construct()
    {
        $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbUncaughtExceptionHandlingFilter'));
        $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbSessionStartupFilter'));
        
        $this->registerFilter(new lmbHandle('wgs/filter/wgsSysAccessFilter'));
        //    $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbRequestDispatchingFilter',
        //                    array(new lmbHandle('limb/web_app/src/request/lmbRoutesRequestDispatcher'))));
        $this->registerFilter(new lmbHandle('wgs/filter/wgsRequestDispatchingFilter',
        array(new lmbHandle('wgs/request/wgsRoutesRequestDispatcher'))));
        $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbResponseTransactionFilter'));
        $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbActionPerformingFilter'));
        $this->registerFilter(new lmbHandle('limb/web_app/src/filter/lmbViewRenderingFilter'));
        
    }
}

?>
