<?php
/**
 * Limb Web Application Framework
 *
 * @link http://limb-project.com
 *
 * @copyright  Copyright &copy; 2004-2007 BIT
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 * @version    $Id: lmbRoutes.class.php 5012 2007-02-08 15:38:06Z pachanga $
 * @package    web_app
 */

@define('WGS_CONTROLLERS_INCLUDE_PATH', 'wgs/controller');

class wgsRoutes
{

  function __construct()
  {
  }

  function dispatch($uri)
  {
    $controllers_paths = explode(';', WGS_CONTROLLERS_INCLUDE_PATH);
    $path_elements_ = $uri->getPathElements();
	$path_elements = array();
    foreach($path_elements_ as $value)
	    if ($value)
	        $path_elements[] = $value;
	
	$result = array();

// �������� ������ ����������� ���������� � ������������ � routes.conf.php
    $result['named_params'] = $this->_getNamedParams($path_elements);

// � ������������ � URL ������ ���������� ���� � ����������
/*
    if (@$path_elements[0] == 'component')
    {
        $controllers_paths[0] .= '/component';
        array_shift($path_elements);
    }
    else
        $controllers_paths[0] .= '/backend';
*/
    $count_path_elements = count($path_elements);

// ���� �� ������ �� ���� ��������
    if ($count_path_elements == 0)
    {
        $result['controller'] = 'Index';
        $result['action'] = 'display';
        @define('WGS_CONTROLLERS_INCLUDE_PATH_EXT', implode(';',$controllers_paths)); 
        return $result;
    }
// ���� ������ ���� �� ���� ��������, ������� � �������� ����������� �� ��������� "Index"
    $inlcude_path = "";
    $old_parameters = false;
    $controller_path = WGS3_ENGINE_DIR."{$controllers_paths[0]}/";
   // var_dump(WGS3_ENGINE_DIR."{$controllers_paths[0]}/");
    $controller_name = "IndexController";
    $controller_file = "$controller_path$controller_name.class.php";
    if (is_file($controller_file))
    {
        lmb_require($controller_file);
        $controller = "Index";
	    $action_name = $path_elements[0];
	    $controller_object = new $controller_name;
	    if (method_exists($controller_object, "do{$action_name}"))
            $result['action'] = $action_name;
	    elseif (method_exists($controller_object, "doDisplay"))
            $result['action'] = "display";
		$result['controller'] = $controller;
		for ($j = 1; $j < $count_path_elements; $j++)
		    $result['params'][] = $path_elements[$j];
		$inlcude_path = $controllers_paths[0];
		$old_parameters = true;
    }

// ���� ������ ����������� � ��������
    for($i = 0; $i < $count_path_elements; $i++)
    {        
		$controller_path = WGS3_ENGINE_DIR."{$controllers_paths[0]}/";
        $controller_name = "{$path_elements[$i]}Controller";
        $controller_name = ucfirst($controller_name);
        $controller_file = "$controller_path$controller_name.class.php";
        if (is_file($controller_file))
        {           
            lmb_require($controller_file);
            $controller = $path_elements[$i];
            $param_offset = $i+1;
            if ($count_path_elements-1 == $i)
                $action_name = 'display';
            else
            {                    
                $action_name = $path_elements[$i+1];                
                //ini_set('display_errors',1);
                //error_reporting(E_ALL);
                $controller_object = new $controller_name;
                //ini_set('display_errors',0);
                
                if (method_exists($controller_object, "do{$action_name}"))
                    $param_offset = $i+2;
                elseif (method_exists($controller_object, 'doDisplay'))
                    $action_name = 'display';
                else
                {                    
                    $controllers_paths[0] .= '/'.$path_elements[$i];
		                continue;
                }
            }            
			$result['controller'] = $controller;
			$result['action'] = $action_name;
			
			if ($old_parameters)
			{
			    unset($result['params']);
			    $old_parameters = false;
			}
            for ($j = $param_offset; $j<$count_path_elements; $j++)
                $result['params'][] = $path_elements[$j];
            $inlcude_path = $controllers_paths[0];
        }
        $controllers_paths[0] .= '/'.$path_elements[$i];
    }
	// var_dump($result);
	// 		die();
// � �������� ����� �������� ���������� + �������� + ���������� ���� � �����������
    @define('WGS_CONTROLLERS_INCLUDE_PATH_EXT', WGS_CONTROLLERS_INCLUDE_PATH.';'.$inlcude_path);
    
    return $result;
  }

  function _getNamedParams($path_elements)
  {
    $config = lmbToolkit :: instance()->getConf('routes');
    $r = array();
    $found = false;
    foreach($config as $key => $value)
    {
    	$i = 0;
    	$r[$key] = 0;
    	$template_elements = explode("/", $value['path']);
    	foreach($template_elements as $element)
    	{
    	    if (count($path_elements) > $i && $element == $path_elements[$i++])
    	    {
    	        $r[$key]++;
    	        $found = true;
    	    }
    	    else
    	        break;
    	}
    }
    if ($found)
    {
        arsort($r);
        $matched_template = $config[key($r)];
        $template_elements = explode("/", $matched_template['path']);
        $count = (count($template_elements) > count($path_elements))? count($path_elements): count($template_elements);
        $result = array();
        for ($i = array_shift($r); $i < $count; $i++)
            $result[substr($template_elements[$i], 1)] = $path_elements[$i];
        return $result;
    }
    else
      return array();
  }
  
  function toUrl($params, $route_name = '')
  {
    if($route_name && isset($this->config[$route_name]))
    {
      if($path = $this->_makeUrlByRoute($params, $this->config[$route_name]))
        return $path;
    }
    elseif(!$route_name)
    {
      foreach($this->config as $name => $route)
      {
        if($path = $this->_makeUrlByRoute($params, $route))
          return $path;
      }
    }
    throw new lmbException($message = "Route '$route_name' not found for params '" . lmb_var_dump($params) . "'");
  }

  protected function _applyDispatchFilter($route, $dispatched)
  {
    if(!isset($route['dispatch_filter']) && !isset($route['rewriter']))
      return $dispatched;

    //'rewriter' is going to be obsolete
    $filter = isset($route['dispatch_filter']) ? $route['dispatch_filter'] : $route['rewriter'];

    if(!is_callable($filter))
      throw new lmbException('Dispatch filter is not callable!', array('filter' => $filter));

    call_user_func_array($filter, array(&$dispatched, $route));
    return $dispatched;
  }

  protected function _applyUrlFilter($route, $path)
  {
    if(!isset($route['url_filter']))
      return $path;

    $filter = $route['url_filter'];

    if(!is_callable($filter))
      throw new lmbException('Url filter is not callable!', array('filter' => $filter));

    call_user_func_array($filter, array(&$path, $route));
    return $path;
  }

  protected function _getResultMatchedParams($route, $url)
  {
    if(($matched_params = $this->_getMatchedParams($route, $url)) === null)
      return null;

    if(isset($route['defaults']))
      return array_merge($route['defaults'], $matched_params);
    else
      return $matched_params;
  }

  function _getMatchedParams($route, $url)
  {
    $named_params = array();

    $regexp = $this->_getRouteRegexp($route['path'], $named_params);

    if(!preg_match($regexp, $url, $matched_params))
      return null;

    array_shift($matched_params);

    $result = array();

    $index = 0;
    foreach($matched_params as $matched_item)
      if($param_name = $named_params[$index++])
        $result[$param_name] = $matched_item;

    return $result;
  }

  function _getRouteRegexp($route_path, &$named_params)
  {
    $elements = array();
    foreach (explode('/', $route_path) as $element)
      if (trim($element))
        $elements[] = $element;

    $final_regexp_parts = array();

    foreach ($elements as $element)
    {
      if($name = $this->_getNamedUrlParam($element))
      {
        $final_regexp_parts[] = self :: NAMED_PARAM_REGEXP;
        $named_params[] = $name;
      }
      elseif ($name = $this->_getExtraNamedParam($element))
      {
        $final_regexp_parts[] = self :: EXTRA_PARAM_REGEXP;
        $named_params[] = $name;
      }
      else
        $final_regexp_parts[] = '/' . $element;
    }

    return '#^' . implode('', $final_regexp_parts) . '[\/]*$#';
  }

  protected function _getNamedUrlParam($element)
  {
    if(preg_match('/^:(.+)$/', $element, $matches))
      return $matches[1];
    else
      return null;
  }

  protected function _getExtraNamedParam($element)
  {
    if(preg_match('/^\*(.+)?$/', $element, $matches))
    {
      if(isset($matches[1]))
        return $matches[1];
      else
        return 'extra';
    }
    else
      return null;
  }

  protected function _routeParamsMeetRequirements($route, $params)
  {
    foreach($params as $param_name => $param_value)
    {
      if(!$this->_singleParamMeetsRequirements($route, $param_name, $param_value))
        return false;
    }
    return true;
  }

  protected function _singleParamMeetsRequirements($route, $param_name, $param_value)
  {
     return (!isset($route['requirements'][$param_name]) ||
            preg_match($route['requirements'][$param_name], $param_value, $req_res));
  }

  function _makeUrlByRoute($params, $route)
  {
    $path = $route['path'];

    foreach($params as $param_name => $param_value)
    {
      if(strpos($path, ':'.$param_name) === false)
        continue;

      $path = str_replace(':'. $param_name, $param_value, $path);
      unset($params[$param_name]);
    }

    if(count($params))
      return '';

    if(isset($route['defaults']))
    {
      foreach($route['defaults'] as $param_name => $param_value)
        $path = str_replace(':'. $param_name, $param_value, $path);
    }

    if(strpos($path, "/:") !== false)
      return '';

    return $this->_applyUrlFilter($route, $path);
  }
}

?>
