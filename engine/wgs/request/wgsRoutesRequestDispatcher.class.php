<?php
/**
 * Limb Web Application Framework
 *
 * @link http://limb-project.com
 *
 * @copyright  Copyright &copy; 2004-2007 BIT
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 * @version    $Id: lmbRoutesRequestDispatcher.class.php 5286 2007-03-20 08:31:30Z serega $
 * @package    web_app
 */
lmb_require('limb/web_app/src/request/lmbRequestDispatcher.interface.php');

class wgsRoutesRequestDispatcher implements lmbRequestDispatcher
{
  protected $path_offset;
  protected $base_path;

  function __construct($path_offset = null, $base_path = null)
  {
  }

  function dispatch($request)
  {
    $wgsRoutes = lmbToolkit :: instance()->getWgsRoutes();
    $uri = $request->getUri();
    $uri->normalizePath();
    $result = $wgsRoutes->dispatch($uri);
    if($action = $request->get('action'))
      $result['action'] = $action;
      
	return $result;    
  }
}

?>
