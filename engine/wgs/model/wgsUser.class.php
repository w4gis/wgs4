<?php

lmb_require('wgs/model/wgsProjectUser.class.php');
lmb_require('wgs/model/wgsSysUser.class.php');
lmb_require('wgs/model/wgsRole.class.php');

abstract class wgsUser
{
	protected $name;
	protected $id;
	protected $password;
	protected $locked;
	protected $parentRoles;
	protected $description;

	public final static function factory($record)
	{
		if ($record->get('wgsuser_id') !== null)
		return new wgsProjectUser($record);
		if ($record->get('sysuser_id') !== null)
		return new wgsSysUser($record);
	}

	abstract public function import($importData);

	public function __construct($importData = null)
	{
        if (!is_null($importData))
            $this->import($importData);
	}

	public function getParentRoles()
	{
		return $this->parentRoles;
	}

	public function addParentRole($parentRole)
	{
        if (is_array($this->parentRoles))
            $this->parentRoles[] = $parentRole;
        else
            $this->parentRoles = array($parentRole);
	}

	public function getParentRoleIds()
	{
    	$parentRoleIds = array();
    	$roles = $this->getParentRoles();
    	if (is_array($roles))
        	foreach($roles as $role) {
                $parentRoleIds[] = $role->getRoleId();
            }
    	return $parentRoleIds;
	}

    public function getParentRoleNames()
    {
        $parentRoleNames = array();
        $roles = $this->getParentRoles();
        if (is_array($roles))
            foreach($roles as $role)
                $parentRoleNames[] = $role->getName();
        return $parentRoleNames;
    }
	
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setLocked($locked)
	{
		$this->locked = $locked;
	}

	public function getLocked()
	{
		return $this->locked;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getDescription()
	{
		return $this->description;
	}
	
    public function setParentRoles($roleRecords)
    {
        if (is_array($roleRecords))
			foreach ($roleRecords as $roleRecord)
				$this->addParentRole(wgsRole::factory($roleRecord));
		
    }
}

?>
