<?php

class MgBaseResource
{
	protected $mgTier;
	protected $content;
	protected $resourceId;
	protected $oldResourceId;
    protected $name;
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
	
    public function __construct($resourceId)
    {       
        $this->mgTier = lmbToolkit :: instance() -> getMgTier();
        $this->setResourceId($resourceId);
        $this->content = DOMDocument::loadXML($this->mgTier->getResourceContentAsString($resourceId));
    }
		
    public function getResourceId()
    {
        return $this->resourceId;
    }
    
    public function getOldResourceId()
    {
        return $this->oldResourceId;
    }
    
    protected function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;
    }

    protected function setOldResourceId($resourceId)
    {
        $this->oldResourceId = $resourceId;
    }
    
    public function save($resourceId)
    {
        $this->setResourceId($resourceId);
        $this->mgTier->saveResource($this->content->saveXML(), $resourceId);
    }
    
    public function saveToSession($sessionId = false)
    {
        if (!$sessionId)
            $sessionId = $this->mgTier->getMgSessionId();
        $this->setOldResourceId($this->resourceId);
        $this->save($this->getSessionResource($this->resourceId, $sessionId));
        return $this;
    }
    
    public function getSessionResource($resourceId = false, $sessionId = false)
    {
        if (!$sessionId)
            $sessionId = $this->mgTier->getMgSessionId();
        if (!$resourceId)
            $resourceId = $this->getResourceId();
        return str_replace("Library:", "Session:$sessionId", $resourceId);
    }
}

?>
