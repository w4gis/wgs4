<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */
 
lmb_require('wgs/model/wgsUser.class.php');
lmb_require('wgs/lib/wgsZendAclRoleInterface.interface.php');

class wgsSysUser extends wgsUser implements wgsZendAclRoleInterface
{    
	protected $parentSysRoleIds;
	
    public function import($importData)
    {
        $this->setName( $importData->get('name') );
        $this->setId( $importData->get('sysuser_id') );
        $this->setPassword( $importData->get('password') );
        $this->setLocked( $importData->get('locked') );
        $this->setDescription( $importData->get('description') );;
        $this->setParentRoleRawIds( $importData->get('roles') );
        $this->setParentRoles( $importData->get('roles') );
    }
        
    private function setParentRoleRawIds($parentSysRoleIds)
    {
        if (is_array($parentSysRoleIds))
        {
            foreach($parentSysRoleIds as $parentSysRoleId)
                $this->parentSysRoleIds[] = $parentSysRoleId->get('sysrole_id');
        }
    }

    public function getParentRoleRawIds()
    {
        return $this->parentSysRoleIds;
    }
    
    public function getRoleId()
    {
        return "su:{$this->getId()}";
    }
    
    public function getTier()
    {
    	return lmbToolkit :: instance() -> getWgsTier() -> getSysTier();
    }    
}