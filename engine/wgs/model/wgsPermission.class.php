<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/wgsRole.class.php');
lmb_require('wgs/model/wgsPrivilege.class.php');

class wgsPermission
{
    const DENY    = 0;
    const ALLOW   = 1;
    const INHERIT = 2;
    
    protected $role;
    protected $object;
    protected $privilege;
    protected $permission;

    public function __construct($importData = null)
    {
        if (!is_null($importData))
            $this->import($importData);
    }
    
    public function import($permissionRecord)
    {
        $this->setRole( $permissionRecord );
        $this->setObject( $permissionRecord );
        $this->setPrivilege( $permissionRecord );
        $this->setPermission( $permissionRecord->get('permission') );
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($permissionRecord)
    {
        if ($permissionRecord instanceof lmbOciRecord)
            $this->role = wgsRole :: factory($permissionRecord);
        else if($permissionRecord instanceof lmbPgsqlRecord || 
			$permissionRecord instanceof WgsDbalArrayPropertyObject|| 
			$permissionRecord instanceof WgsCppSingleDbRecord) {
            $this->role = wgsRole :: factory($permissionRecord);
        }
        else
            $this->role = $permissionRecord;
    }
    
    public function getObject()
    {
        return $this->object;
    }
    
    //TODO: HERE!
    public function setObject($permissionRecord)
    {
        if ($permissionRecord instanceof lmbOciRecord)
            $this->object = wgsObject :: factory($permissionRecord);
        else if (
			$permissionRecord instanceof lmbPgsqlRecord || 
			$permissionRecord instanceof WgsDbalArrayPropertyObject|| 
			$permissionRecord instanceof WgsCppSingleDbRecord) {
				$this->object = wgsObject :: factory($permissionRecord);
        }
        else
            $this->object = $permissionRecord;
    }

    public function setPrivilege($permissionRecord)
    {
        if ($permissionRecord instanceof lmbOciRecord)
            $this->privilege = new wgsPrivilege($permissionRecord);
        else if ($permissionRecord instanceof lmbPgsqlRecord|| 
			$permissionRecord instanceof WgsDbalArrayPropertyObject || 
			$permissionRecord instanceof WgsCppSingleDbRecord) {
            $this->privilege = new wgsPrivilege($permissionRecord);
        }
        else
            $this->privilege = $permissionRecord;
    }
    
    public function getPrivilege()
    {
        return $this->privilege;
    }
    
    public function setPermission($permission)
    {
        $this->permission = $permission;
    }
    
    public function getPermission()
    {
        return $this->permission;
    }
    
    public function isDeny()
    {
        return $this->permission == self::DENY;
    }

    public function isAllow()
    {
        return $this->permission == self::ALLOW;
    }

    public function isInherit()
    {
        return $this->permission == self::INHERIT;
    }
}

?>
