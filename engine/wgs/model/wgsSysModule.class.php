<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');
lmb_require('wgs/model/wgsSysObject.class.php');

class wgsSysModule extends wgsSysObject implements wgsZendAclResourceInterface
{
    public $description;
    public $urlString;
    public $label;
    public $inMenu;
    public $priority;
    public $container;
    //private $projectId;

    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('sysobject_id'));
        $this->setLocked($objectRecord->get('sysobject_locked'));
        $this->setName($objectRecord->get('sysmodule_name'));
        $this->setLabel($objectRecord->get('sysmodule_label'));
        $this->setContainer($objectRecord->get('sysmodule_container'));
        $this->setId($objectRecord->get('sysmodule_id'));
        $this->setUrlString($objectRecord->get('sysmodule_urlstring'));
        $this->setDescription($objectRecord->get('sysmodule_description'));
        $this->setInMenu($objectRecord->get('sysmodule_inmenu'));
        $this->setPriority($objectRecord->get('sysmodule_priority'));
        $this->setParentId($objectRecord->get('sysmodule_parent_id'));
        $this->setParentObjectId($objectRecord->get('sysmodule_parent_sysobject_id'));
        $this->setType($objectRecord->get('sysobjecttype_id'));
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
	
    public function setUrlString($urlString)
    {
        $this->urlString = $urlString;
    }
    
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }
    
    public function getLabel()
    {
        return $this->label;
    }
    
    public function setInMenu($inMenu)
    {
        $this->inMenu = $inMenu;
    }

    public function getInMenu()
    {
        return $this->inMenu;
    }
    
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
	    
	public function getDescription()
    {
    	return $this->description;
    }
    
    public function getPriority()
    {
        return $this->priority;
    }
    
    public function isContainer()
    {
        return $this->container;
    }    

    public function getUrlString()
    {
        return $this->urlString;
    }  
}

?>