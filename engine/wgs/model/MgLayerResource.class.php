<?php

lmb_require('wgs/model/MgBaseResource.class.php');
lmb_require('wgs/model/MgFeatureSrcResource.class.php');
class MgLayerResource extends MgBaseResource
{
    private $featureSource;
    
    public function __construct($resourceId) {
        parent::__construct($resourceId);
        $xpath = new DOMXPath($this->content);
        $nodes = $xpath->query("//LayerDefinition/VectorLayerDefinition/ResourceId");
        if ($nodes->length == 0) {
            $nodes = $xpath->query("//LayerDefinition/GridLayerDefinition/ResourceId");
        }
        if ($nodes->length) {
            $featureSourceId = $nodes->item(0)->nodeValue;
            if ($featureSourceId) {
                $this->featureSource = new MgFeatureSrcResource($featureSourceId);
            }
        }
    }
        
    public function removeGarbageLegends()
    {		    
		    $xpathlayer = new DOMXPath($this->content);
		    $layer_nodes = $xpathlayer->query('//VectorLayerDefinition/VectorScaleRange/PointTypeStyle');
		    foreach ($layer_nodes as $layer_node)
		    {
		        if ($layer_node->getElementsByTagName('PointRule')->item(0)->getElementsByTagName('*')->length < 2)
		            $layer_node->parentNode->removeChild($layer_node);
		    }
		    $layer_nodes = $xpathlayer->query('//VectorLayerDefinition/VectorScaleRange/LineTypeStyle');
		    foreach ($layer_nodes as $layer_node)
		    {
		        if ($layer_node->getElementsByTagName('LineRule')->item(0)->getElementsByTagName('*')->length < 2)
		            $layer_node->parentNode->removeChild($layer_node);
		    }
			
			$wgsTier = lmbToolkit :: instance() -> getWgsTier();
        
			$a=$xpathlayer->query('//VectorLayerDefinition');
			$b=$xpathlayer->query('//VectorLayerDefinition/PropertyMapping');
			$c=$xpathlayer->query('//VectorLayerDefinition/Filter');
			if(!$b->item(0))
				$b=$xpathlayer->query('//VectorLayerDefinition/Geometry');
			if($a->item(0)){
			if(!$c->item(0))
				$f="";
			else{
				$f = $c ->item(0)->nodeValue;
				$c->item(0)->parentNode->removeChild($c->item(0));
			}
				$wgsuser_roles = $wgsTier->getAuthorizedUser()->getParentRoles();
				
				foreach ($wgsuser_roles as $role){
					$ur = $role->getId();
					$fp ="";
					$results = $wgsTier->getProjectRoleService()->getArea(1,$ur);
					
					foreach($results as $result) {
						$fp = $result->get('limit_ftf');
						//var_dump($ur);
						//var_dump($fp);
					}
					if($fp){
						$f .= (($f == "") ? "" : " AND ") . "Geometry INSIDE GeomFromText('".$fp."')";
						//var_dump($f);				
					}
				}
			if($f)
				$a->item(0)->insertBefore($xpathlayer->document->createElement('Filter',$f),$b->item(0));
			}
			
			
		    $layer_nodes = $xpathlayer->query('//VectorLayerDefinition/VectorScaleRange/AreaTypeStyle');
		    foreach ($layer_nodes as $layer_node)
		    {
		        if ($layer_node->getElementsByTagName('AreaRule')->item(0)->getElementsByTagName('*')->length < 4)
		            $layer_node->parentNode->removeChild($layer_node);
		    }
    }
    
    public function saveToSession($sessionId = false)
    {
        //if ($this->featureSource && $this->featureSource->getName() != 'OSGeo.SDF') {
        if (!$this->mgTier->resourceExists($this->featureSource->getSessionResource())) {
            $featureSource = $this->featureSource->saveToSession();
        }
        $this->rebindFeatureSource($this->featureSource->getSessionResource());
        //}
        return parent::saveToSession($sessionId);
    }
    
    private function rebindFeatureSource($featureSourceId)
    {
        $xpath = new DOMXPath($this->content);
        $nodes = $xpath->query("//LayerDefinition/VectorLayerDefinition/ResourceId");
        if ($nodes->length == 0) {
            $nodes = $xpath->query("//LayerDefinition/GridLayerDefinition/ResourceId");
        }
        $nodes->item(0)->nodeValue = $featureSourceId;
        $this->mgTier->testConnection(($featureSourceId));
    }
}

?>