<?php

lmb_require('wgs/model/wgsProjectObject.class.php');

class wgsFeatureLayerGroup extends wgsProjectObject
{       
    public function import($objectRecord)
    {
        $this->setName($objectRecord->get('name'));
        $this->setId($objectRecord->get('featurelayergroup_id'));
        $this->setType(parent::FEATURELAYERGROUP);
    }
}

?>