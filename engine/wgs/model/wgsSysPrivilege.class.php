<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/wgsSysObject.class.php');
lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsSysPrivilege extends wgsSysObject implements wgsZendAclResourceInterface
{
    const PROJECT_ADMIN       = 'PROJECT_ADMIN';
    const SYSTEM_ADMIN        = 'SYSTEM_ADMIN';
    const CARD_EDIT           = 'CARD_EDIT';
    const CARD_BIND           = 'CARD_BIND';
    const FEATUREDOC_BIND     = 'FEATUREDOC_BIND';
    const PROJECTOBJECT_ADMIN = 'PROJECTOBJECT_ADMIN';
    const PROJECTROLE_ADMIN   = 'PROJECTROLE_ADMIN';
    const PROJECTUSER_ADMIN   = 'PROJECTUSER_ADMIN';
    
    protected $description;
    protected $sysObjectId;

    public function __construct($importData = null)
    {
        if (!is_null($importData))
            $this->import($importData);
    }
    
    public function import($permissionRecord)
    {        
        $this->setObjectId( $permissionRecord->get('sysobject_id') );
        $this->setId( $permissionRecord->get('sysprivilege_id') );
        $this->setName( $permissionRecord->get('sysprivilege_name') );
        $this->setLocked($permissionRecord->get('sysobject_locked'));
        $this->setDescription( $permissionRecord->get('sysprivilege_description') );
        $this->setSysObjectId( $permissionRecord->get('sysprivilege_sysobject_id') );
        $this->setType($permissionRecord->get('sysobjecttype_id'));
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }

    public function setSysObjectId($sysObjectId)
    {
        $this->sysObjectId = $sysObjectId;
    }
    
    public function getSysObjectId()
    {
        return $this->sysObjectId;
    }
}

?>