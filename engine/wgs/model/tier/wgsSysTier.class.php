<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */

lmb_require('wgs/model/tier/wgsBaseTier.class.php');
lmb_require('wgs/model/service/wgsSysObjectService.class.php');
lmb_require('wgs/model/service/wgsSysRoleService.class.php');
lmb_require('wgs/model/service/wgsSysUserService.class.php');
lmb_require('wgs/model/service/wgsSysPermissionService.class.php');
lmb_require('wgs/model/service/wgsSysUtilsService.class.php');
lmb_require('wgs/model/mapper/wgsSysObjectServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsSysRoleServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsSysUserServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsSysPermissionServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsSysUtilsServiceMapper.class.php');

class wgsSysTier extends wgsBaseTier
{
    protected function __construct()
    {
        parent::__construct();
        $this->dbConnection = lmbToolkit :: instance() -> getWgsSysDbConnection();
    }
    
    static public function instance()
    {
        if (!self::$instance instanceof self)
            self::$instance = new self();
        return self::$instance; 
    }

    public function getObjectService()
    {
    	return new wgsSysObjectService(
    		new wgsSysObjectServiceMapper,
    		$this->getDbConnection()
    	);
    }

    public function getRoleService()
    {
    	return new wgsSysRoleService(
    		new wgsSysRoleServiceMapper,
    		$this->getDbConnection()
    	);
    }

    public function getUserService()
    {
    	return new wgsSysUserService(
    		new wgsSysUserServiceMapper,
    		$this->getDbConnection()
    	);
    }
        
    public function getPermissionService()
    {
    	return new wgsSysPermissionService(
    		new wgsSysPermissionServiceMapper,
    		$this->getDbConnection()
    	);
    }
    
    public function getSysUtils()
    {
    	return new wgsSysUtilsService(
    		new wgsSysUtilsServiceMapper,
    		$this->getDbConnection()
    	);
    }
}

?>