<?php
lmb_require('wgs/model/tier/wgsAbstractTier.class.php');
lmb_require('wgs/model/tier/wgsSysTier.class.php');
lmb_require('wgs/model/tier/wgsProjectTier.class.php');
lmb_require('wgs/exception/wgsException.class.php');
lmb_require('wgs/model/wgsSysUser.class.php');
lmb_require('wgs/model/wgsProjectUser.class.php');
lmb_require('wgs/lib/wgsSession.class.php');
lmb_require('wgs/lib/wgsZendACL.class.php');
lmb_require('wgs/model/wgsPermission.class.php');

class wgsTier extends wgsAbstractTier
{
	private $userInformation;
    private $acl;    
    private $authorizedUser;
    private $sysPrivileges;

    static public function instance()
    {
        if (!self::$instance instanceof self)
            self::$instance = new self();
        return self::$instance; 
    }
    
    protected function __construct()
    {
	    parent :: __construct();
        $this->sysTier = wgsSysTier :: instance();
        $this->projectTier = wgsProjectTier :: instance();
        //var_dump($_SESSION['userInformation']);die();
        $this->setUserInformation( $_SESSION['userInformation'] );
        $this->setAuthorizedUser( $this->session->get('authorizedUser') );
    }
    
    protected function authorize(& $userInformation)
    {
        $this->getSysTier()->open($userInformation);
        if (!$this->getSysTier()->isAuthorized())
            $this->getProjectTier()->open($userInformation);
        if ($this->getSysTier()->isAuthorized() || $this->getProjectTier()->isAuthorized())
        {
            $this->session->set('userInformation', $userInformation);
            $this->saveAuthorizedUser( $userInformation );
            $this->createACL();
            return true;
        }
        throw new wgsException(wgsException::INCORRECT_USERNAME_OR_PASSWORD);
        return false;
    }

    public function isSysUser()
    {
        return $this->getSysTier()->isAuthorized();
    }

    public function isProjectUser()
    {
        return $this->getProjectTier()->isAuthorized();
    }
    
    public function getSysTier()
    {
		return $this->sysTier;
    }

    public function getProjectTier()
    {
		return $this->projectTier;
    }

    public function getSysObjectService()
    {
    	return $this->getSysTier()->getObjectService();
    }

    public function getSysUserService()
    {
    	return $this->getSysTier()->getUserService();
    }

    public function getSysRoleService()
    {
    	return $this->getSysTier()->getRoleService();
    }

    public function getSysPermissionService()
    {
    	return $this->getSysTier()->getPermissionService();
    }
    
    public function getProjectObjectService()
    {
    	return $this->getProjectTier()->getObjectService();
    }

    public function getThemeService()
    {
    	return $this->getProjectTier()->getThemeService();
    }

    public function getTusurFlatStatsService()
    {
        return $this->getProjectTier()->getTusurFlatStatsService();
    }
    
    public function getProjectUserService()
    {
    	return $this->getProjectTier()->getUserService();
    }

    public function getProjectRoleService()
    {
    	return $this->getProjectTier()->getRoleService();
    }

    public function getProjectPermissionService()
    {
    	return $this->getProjectTier()->getPermissionService();
    }
    
    public function setUserInformation(& $userInformation)
    {
        $this->userInformation = $userInformation;
    }
    
    public function getUserInformation()
    {
        return $this->userInformation;
    }
    
    private function setAuthorizedUser($authorizedUser)
    {
        $this->authorizedUser = $authorizedUser;
    }
    
    public function getAuthorizedUser()
    {
        if ($this->isAuthorized()) {
			return $this->authorizedUser;
        }
    }
    
    private function saveAuthorizedUser($userInformation)
    {
        if ($this->getSysTier()->isAuthorized()) {
            $authorizedUser = $this->getSysTier()->getAuthorizedUser( $userInformation->getUsername() );
        }
        if ($this->getProjectTier()->isAuthorized()) {
            $authorizedUser = $this->getProjectTier()->getAuthorizedUser( $userInformation->getUsername() );
        }
        $this->setAuthorizedUser($authorizedUser);
        $this->session->set('authorizedUser', $authorizedUser);
    }

    private function addResourcesToACL(&$acl, &$objects)
    {
        $additionObjects = array();
        foreach($objects as $object)
            $additionObjects[$object->getLevel()][] = $object;

        ksort($additionObjects);

        foreach($additionObjects as $additionObject)
            foreach($additionObject as $object)        
                if (is_object($object->getParent()))
                    $acl->addResource($object->getResourceId(), $object->getParent()->getResourceId());
                else
                    $acl->addResource($object->getResourceId());
    }
    
    private function addRolesToACL(&$acl, &$roles)
    {
        $additionRoles = array();
        foreach($roles as $role)
            $additionRoles[$role->getLevel()][] = $role;

        ksort($additionRoles);

        foreach($additionRoles as $additionRole) {
            foreach($additionRole as $role) {
                if (is_array($role->getParents())) {
                    $acl->addRole($role->getRoleId(), $role->getParentRoleIds());
                }
                else
                    $acl->addRole($role->getRoleId());
            }
        }
    }
    
    private function addUserToACL(&$acl, &$user)
    {
        //var_dump($user);
        //die();
        if (is_array($user->getParentRoles()))
            $acl->addRole($user->getRoleId(), $user->getParentRoleIds());
        else
            $acl->addRole($user->getRoleId());
    }
    
    private function addPermissionsToACL(&$acl, &$permissions)
    {
    	foreach($permissions as $permission)
		{
		    switch($permission->getPermission())
		    {
		        case wgsPermission::ALLOW:
                    $acl->allow($permission->getRole()->getRoleId(), $permission->getObject()->getResourceId(), $permission->getPrivilege()->getId());
		        break;
                case wgsPermission::DENY:
                    $acl->deny($permission->getRole()->getRoleId(), $permission->getObject()->getResourceId(), $permission->getPrivilege()->getId());
                break;
		    }                
		}
    }
    
    private function createACL()
    {
        if ($this->isAuthorized()) {
            $this->acl          = new wgsZendACL(); //268
            $authorizedUser     = $this->getAuthorizedUser(); //false 459
            $sysObjects         = $this->getSysTier()->getObjects(); //false little
            $sysRoles           = $this->getSysTier()->getRoles();
            $sysPermissions     = $this->getSysTier()->getPermissions();
            
            //if ($this->getProjectTier()->isAuthorized()) {
                $projectObjects     = $this->getProjectTier()->getObjects();
                $projectRoles       = $this->getProjectTier()->getRoles();
                $projectPermissions = $this->getProjectTier()->getPermissions();
            //} else {
            //    $projectObjects     = array();
            //    $projectRoles       = array();
            //    $projectPermissions = array();
            //}

            $this->addResourcesToACL($this->acl, $sysObjects);
            $this->addResourcesToACL($this->acl, $projectObjects);
            $this->addRolesToACL($this->acl, $sysRoles);
            $this->addRolesToACL($this->acl, $projectRoles);
//die();
            $this->addUserToACL($this->acl, $authorizedUser);
            $this->addPermissionsToACL($this->acl, $sysPermissions);
            $this->addPermissionsToACL($this->acl, $projectPermissions);
            $this->sessionSet('Acl', $this->acl);
            $aclRevision = $this->getSysTier()->getSysUtils()->getAclRevision();
            $this->setAclRevision($aclRevision);
        }
    }
    
    public function getACL()
    {
        if ($this->isAuthorized()) {
        	if (!is_null($this->acl))
        		return $this->acl;    		
        	require_once('wgs/lib/wgsZendACL.class.php');
        	$this->acl = $this->sessionGet('Acl');
        	if (is_null($this->acl) || $this->getAclRevision() != $this->getSysTier()->getSysUtils()->getAclRevision())
        		$this->createACL();
    	    return $this->acl;
        }
    }

    public function getAclRevision()
    {
        return $this->sessionGet('AclRevision');
    }
    
    public function setAclRevision($aclRevision)
    {
        $this->sessionSet('AclRevision', $aclRevision);
    }
    
    public function getSysPrivileges()
    {
        if ($this->isAuthorized()) {
            if (!is_null($this->sysPrivileges))
                return $this->sysPrivileges;
            require_once('wgs/model/wgsSysPrivilege.class.php');
            $this->sysPrivileges = $this->sessionGet('SysPrivileges');
            if (is_null($this->sysPrivileges))
            {
                if ($this->sysPrivileges = $this->getSysObjectService()->getSysPrivilegeService()->getSysPrivilegeList())
                {
                    foreach($this->sysPrivileges as $sysPrivilege)
                        $this->sessionSet('SysPrivileges[]', $sysPrivilege);
                }
            }
            return $this->sysPrivileges;
        }
    }
    
    public function getSysPrivilege($name)
    {
        if ($sysPrivileges = $this->getSysPrivileges())
        {
            foreach($sysPrivileges as $sysPrivilege)
                if ($sysPrivilege->getName() == $name)
                    return $sysPrivilege;
        }
        return null;
    }
    
    public function close()
    {
        $this->session->reset();
    }    
		public function getMgLayerService()
    {
    	return $this->getProjectTier()-> getMgLayerService();
    }
}

?>
