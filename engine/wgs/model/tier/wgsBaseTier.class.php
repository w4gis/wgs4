<?php

lmb_require('wgs/model/tier/wgsAbstractTier.class.php');

class wgsBaseTier extends wgsAbstractTier
{
    protected $authorizedUser;
    protected $roleRegistry;
    protected $objectRegistry;
    protected $permissionRegistry;
	    
    protected function authorize(& $userInformation)
    {	
    	$un =$this->getUserService()->checkPassword(
    		$userInformation->getPassword(),
        	$userInformation->getIp()
        );
		
		if ($un!==0 || $this->getUserService()->checkPassword2(
    		$userInformation->getUsername(),
        	$userInformation->getPassword(),
			$_SERVER['REMOTE_ADDR']
        )>0){
			$userInformation -> setUsername($un);
			return true;
		};
		return false;
    }

    public function getPermissions()
    {
        if (!is_null($this->permissionRegistry))
            return $this->permissionRegistry;
            
        $this->permissionRegistry = $this->getPermissionService()->getPermissions();

        return $this->permissionRegistry;
    }
    
    public function getAuthorizedUser($name)
    {
        if (!is_null($this->authorizedUser))
            return $this->authorizedUser;
        $this->authorizedUser = $this->getUserService()->getUserByName($name);
		return $this->authorizedUser;
    }
    
    public function getRoles()
    {
        if (!is_null($this->roleRegistry))
            return $this->roleRegistry;

        $this->roleRegistry = $this->getRoleService()->getList();

        return $this->roleRegistry;
    }

    public function getObjects()
    {
        if (!is_null($this->objectRegistry))
            return $this->objectRegistry;
        
        $this->objectRegistry = $this->getObjectService()->getList();
            
        return $this->objectRegistry;
    }
}

?>
