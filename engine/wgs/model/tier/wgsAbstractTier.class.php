<?php

lmb_require('wgs/lib/wgsSession.class.php');
lmb_require('wgs/exception/wgsException.class.php');

abstract class wgsAbstractTier
{
    protected static $instance;
    protected $session;
    protected $dbConnection;
        
    protected function __construct()
    {
        $this->session = wgsSession :: instance();
    }
    
    public function getDbConnection()
    {
        return $this->dbConnection;
    }
    
    public final function getSession()
    {
        if ($this->isAuthorized())
            return $this->session;
    }

    public final function commitSession()
    {
        // return $this->session->commit();
    }
    
    public final function sessionSet($name, $value)
    {
        $this->session->set(get_class($this).$name, $value);
    }

    public final function sessionGet($name)
    {
        return $this->session->get(get_class($this).$name);
    }
    
    public final function isAuthorized()
    {
        return $this->sessionGet('Authorized');
    }
    
    protected function setAuthorized()
    {
        $this->sessionSet('Authorized', true);
    }

    protected function setUnauthorized()
    {
        $this->sessionSet('Authorized', false);
    }
    
    abstract protected function authorize(& $userInformation);
 
    public function open(& $userInformation = null)
    {
        if (!$this->isAuthorized())
        {
            if ($userInformation === null)
                throw new wgsException(wgsException::AUTHORIZATION_REQUIRED);
            else if ($this->authorize($userInformation))
                $this->setAuthorized();
            else
                $this->setUnauthorized();
        }
    }
}

?>