<?php

lmb_require(MAPVIEWER_DIR."/constants.php");

class wgsMgTier
{
    private static $instance;
    private $session;
    private $siteConnection;
    private $transaction;
    private $transactionCount;
    
    static public function instance()
    {
        if (!self::$instance instanceof self)
            self::$instance = new self();
        return self::$instance;
    }
    
    
    
    private function __construct()
    {
        $this->session = lmbToolkit :: instance()->getSession();
    	try
        {
            if (function_exists('MgInitializeWebTier'))
            {
                MgInitializeWebTier(MGWEBTIER_CONFIG);
                $this->siteConnection = new MgSiteConnection();
                if ($mgSessionId = $this->getMgSessionId())
                {
                	try
                	{
                        $this->openMgSession($mgSessionId);
                	}
                	catch(MgSessionExpiredException $e)
                	{
                		$this->createMgSession();
                	}
                }
                else
                {
                	$this->createMgSession();
                }
            }
            else {
                throw new wgsException(wgsException::MAPGUIDE_EXCEPTION_WEB_TIER);
            }
        }
        catch(MgException $e)
        {
			//echo $e->GetMessage();
			//echo $e->GetDetails();
			//echo $e->GetStackTrace();
			throw new wgsException(wgsException::MAPGUIDE_EXCEPTION, 'MapGuide: '.nl2br($e->GetMessage()).' Unable to initialize?');
        }
    }

    function getSessionData($key) {
        return $this -> session -> get($key);
    }
    
    function setSessionData($key, $data) {
        $this -> session -> set($key, $data);
    }
    
    function getTransactionCount() {
        return $this -> session -> get('trc');
    }
    
    function setTransactionCount($trc) {
        $this -> session -> set('trc', $trc);
    }


    public function openMgSession($mgSessionId)
    {
    	$cred = new MgUserInformation($mgSessionId);
        $cred->SetClientIp($this->getClientIp());
        $cred->SetClientAgent($this->getClientAgent());
    	$this->siteConnection->Open($cred);
    }
    
    private function createMgSession()
    {
    	$cred = new MgUserInformation(MGAUTHOR_USERNAME, MGAUTHOR_PASSWORD);
        $cred->SetClientIp($this->getClientIp());
        $cred->SetClientAgent($this->getClientAgent());
		$this->siteConnection->Open($cred);
		$this->setMgSessionId($this->siteConnection->GetSite()->CreateSession());
    }
    
    public function close()
    {
        if ($mgSessionId = $this->getMgSessionId())
        {
            $this->session->set('mgsession', null);
            if ($this->getSiteConnection())
                $this->getSiteConnection()->GetSite()->DestroySession($mgSessionId);
        }
    }
        
    public function getMgSessionId()
    {
        return $this->session->get('mgsession');
    }
    
    private function setMgSessionId($mgSessionId)
    {
        $this->session->set('mgsession', $mgSessionId);
    }

    public function getSiteConnection()
    {
        return $this->siteConnection;
    }
    
    public function getResourceContentAsString($resource)
    {
        return $this->
        siteConnection->
        CreateService(MgServiceType::ResourceService)->
        GetResourceContent(new MgResourceIdentifier($resource))->
        ToString();
    }

    public function saveResource($content, $destination)
    {
        $this->
        siteConnection->
        CreateService(MgServiceType::ResourceService)->
        SetResource(new MgResourceIdentifier($destination), new MgByteReader($content,'text/xml'), null);
    }
    
    public function resourceExists($resourceId)
    {
        try {
            $this->
            siteConnection->
            CreateService(MgServiceType::ResourceService)->
            GetResourceContent(new MgResourceIdentifier($resourceId));
            $inSession = 1;
        } catch(MgResourceNotFoundException $e) {
            $inSession = 0;
        }
        return $inSession;
        //return $this->siteConnection->CreateService(MgServiceType::ResourceService)->ResourceExists(new MgResourceIdentifier($resourceId));
    }
    
    public function testConnection($featureSourceId)
    {
        return $this->siteConnection->CreateService(MgServiceType::FeatureService)->TestConnection(new MgResourceIdentifier($featureSourceId));
    }
    
    private function getClientIp()
    {
	    $clientIp = '';
	    if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)
	        && strcasecmp($_SERVER['HTTP_CLIENT_IP'], 'unknown') != 0)
	    {
	        $clientIp = $_SERVER['HTTP_CLIENT_IP'];
	    }
	    else if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)
	        && strcasecmp($_SERVER['HTTP_X_FORWARDED_FOR'], 'unknown') != 0)
	    {
	        $clientIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else if (array_key_exists('REMOTE_ADDR', $_SERVER))
	    {
	        $clientIp = $_SERVER['REMOTE_ADDR'];
	    }
	    return $clientIp;
    }
    
    private function getClientAgent()
    {
    	return "Ajax Viewer";
    }
}

?>
