<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2008 GISLab, Rybalov Nikita
 */

lmb_require('wgs/model/tier/wgsBaseTier.class.php');
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
lmb_require('wgs/model/service/wgsProjectRoleService.class.php');
lmb_require('wgs/model/service/wgsProjectUserService.class.php');
lmb_require('wgs/model/service/wgsProjectPermissionService.class.php');
lmb_require('wgs/model/service/wgsModuleService.class.php');
lmb_require('wgs/model/service/wgsSearchService.class.php');
lmb_require('wgs/model/service/wgsThemeService.class.php');
lmb_require('wgs/model/service/wgsTusurFlatStatsService.class.php');
lmb_require('wgs/model/mapper/wgsProjectObjectServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsProjectRoleServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsProjectUserServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsProjectPermissionServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsSearchServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsThemeServiceMapper.class.php');
lmb_require('wgs/model/mapper/wgsTusurFlatStatsServiceMapper.class.php');
lmb_require('wgs/model/service/wgsAuditService.class.php');
lmb_require('wgs/model/service/wgsMgLayerService.class.php');
lmb_require('wgs/model/mapper/wgsMgLayerServiceMapper.class.php');
class wgsProjectTier extends wgsBaseTier
{
    protected function __construct()
    {
        parent::__construct();
        $this->dbConnection = lmbToolkit :: instance() -> getWgsProjectDbConnection();
    }
    
    public static function instance()
    {
        if (!self::$instance instanceof self)
            self::$instance = new self();
        return self::$instance; 
    }

    public function getObjectService()
    {
    	return new wgsProjectObjectService(
    		new wgsProjectObjectServiceMapper,
    		$this->getDbConnection()
    	);
    }
    
    public function getModuleService()
    {
    	return new wgsModuleService(
    		new wgsPackageMapper,
    		$this->getDbConnection()
    	);
    }
    
    public function getRoleService()
    {
    	return new wgsProjectRoleService(
    		new wgsProjectRoleServiceMapper,
    		$this->getDbConnection()
    	);
    }

    public function getUserService()
    {
    	return new wgsProjectUserService(
    		new wgsProjectUserServiceMapper,
    		$this->getDbConnection()
    	);
    }

    public function getPermissionService()
    {
    	return new wgsProjectPermissionService(
    		new wgsProjectPermissionServiceMapper,
    		$this->getDbConnection()
    	);
    }
    
    public function getSearchService()
    {
        return new wgsSearchService(
            new wgsSearchServiceMapper,
            $this->getDbConnection()
        );
    }
    
    public function getThemeService()
    {
        return new wgsThemeService(
            new wgsThemeServiceMapper,
            $this->getDbConnection()
        );
    }
	
    public function getTusurFlatStatsService()
    {
        return new wgsTusurFlatStatsService(
            new wgsTusurFlatStatsServiceMapper,
            $this->getDbConnection()
        );
    }

    public function getAuditService()
    {
        return new wgsAuditService(
            new wgsAudit,
            $this->getDbConnection()
        );
    }
	public function getMgLayerService()
    {
    	 return new wgsMgLayerService(
            new wgsMgLayerServiceMapper,
            $this->getDbConnection()
        );
    }
}
?>