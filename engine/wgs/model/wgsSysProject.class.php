<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsSysProject extends wgsSysObject implements wgsZendAclResourceInterface
{
    public $domain;
    public $description;
    public $database;
    public $title;
    public $schemeWdb;
    public $schemeSdb;
    public $schemeAdb;
    public $schemeUser;
	public $sysmodilelist;
	public $sysobjecttypeid;
    public $sysprojectroles;

    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('sysobject_id'));
        $this->setId($objectRecord->get('sysproject_id'));
        $this->setName($objectRecord->get('sysproject_name'));
        $this->setLocked($objectRecord->get('sysobject_locked'));
        $this->setDomain($objectRecord->get('sysproject_domain'));
        $this->setDescription($objectRecord->get('sysproject_description'));
        $this->setDatabase($objectRecord->get('sysproject_database'));
        $this->setTitle($objectRecord->get('sysproject_title'));
        $this->setSchemeWdb($objectRecord->get('sysproject_scheme_wdb'));
        $this->setSchemeSdb($objectRecord->get('sysproject_scheme_sdb'));
        $this->setSchemeAdb($objectRecord->get('sysproject_scheme_adb'));
        $this->setSchemeUser($objectRecord->get('sysproject_scheme_user'));
        $this->setSysModuleList($objectRecord->get('sysmodulelist'));
        $this->setSysObjectTypeId($objectRecord->get('sysobjecttype_id'));
        $this->setType($objectRecord->get('sysobjecttype_id'));
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setSchemeWdb($schemeWdb)
    {
        $this->schemeWdb = $schemeWdb;
    }

    public function setSchemeSdb($schemeSdb)
    {
        $this->schemeSdb = $schemeSdb;
    }

    public function setSchemeAdb($schemeAdb)
    {
        $this->schemeAdb = $schemeAdb;
    }

    public function setSchemeUser($schemeUser)
    {
        $this->schemeUser = $schemeUser;
    }
	
	public function setSysModuleList($sysmodilelist)

    {
        $this->sysmodilelist = $sysmodilelist;
    }

    public function setSysObjectTypeId($sysobjecttypeid)
    {
        $this->sysobjecttypeid = $sysobjecttypeid;
    }

    public function setSysProjectRoles($sysprojectroles)
    {
        $this->sysprojectroles = $sysprojectroles;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSchemeWdb()
    {
        return $this->schemeWdb;
    }

    public function getSchemeSdb()
    {
        return $this->schemeSdb;
    }

    public function getSchemeAdb()
    {
        return $this->schemeAdb;
    }

    public function getSchemeUser()
    {
        return $this->schemeUser;
    }

    public function getSysModuleList()
    {
        return $this->sysmodilelist;
    }

    public function getSysObjectTypeId()
    {
        return $this->sysobjecttypeid;
    }

    public function getSysProjectRoles()
    {
        return $this->sysprojectroles;
    }
}
?>