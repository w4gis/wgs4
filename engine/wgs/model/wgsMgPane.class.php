<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsMgPane extends wgsProjectObject implements wgsZendAclResourceInterface
{
    private $description;    
    private $label;
    private $mgWebLayoutId;
    private $mgWebLayoutName;
    
    private function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    private function setLabel($label)
    {
        $this->label = $label;
    }
        
    private function setMgWebLayoutId($mgWebLayoutId)
    {
        $this->mgWebLayoutId = $mgWebLayoutId;
    }

    public function getMgWebLayoutId()
    {
        return $this->mgWebLayoutId;
    }
    
    private function setMgWebLayoutName($mgWebLayoutName)
    {
        $this->mgWebLayoutName = $mgWebLayoutName;
    }

    public function getMgWebLayoutName()
    {
        return $this->mgWebLayoutName;
    }
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setName($objectRecord->get('mgpane_name'));
        $this->setId($objectRecord->get('mgpane_id'));
        $this->setDescription($objectRecord->get('mgpane_description'));
        $this->setLabel($objectRecord->get('mgpane_label'));
        $this->setMgWebLayoutId($objectRecord->get('mgpane_mgweblayout_id'));
        $this->setMgWebLayoutName($objectRecord->get('mgpane_weblayout_name'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
}

?>