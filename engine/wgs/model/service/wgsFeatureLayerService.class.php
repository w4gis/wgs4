<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� �������� ������� ������ � ��������� ���� FeatureLayer.
 * 
 */
class wgsFeatureLayerService extends wgsProjectObjectService
{
	/**/
	public function Get($id)
    {
        return $this->pkgMapper->Get($id);
    }
	public function GetLayersByGroupName($layerName)
    {
        return $this->pkgMapper->GetLayersByGroupName($layerName);
    }
	
	public function GetListFeatureLayerByType($featureTypeId, $noEmpty = false)
	{
		return $this->pkgMapper->GetListFeatureLayerByType($featureTypeId, $noEmpty);
	}
	
	public function GetListFeatureLayerByGroup($featureLayerGroupId)
	{
		return $this->pkgMapper->GetListFeatureLayerByGroup($featureLayerGroupId);
	}
	public function Add($name,$groupid,$cs,$typeid)
	{
		return $this->pkgMapper->Add($name,$groupid,$cs,$typeid);
	}

    public function addTrigger($id)
    {
        return $this->pkgMapper->addTrigger($id);
    }

	public function Remove($id,$removeMode)
	{
		return $this->pkgMapper->Remove($id,$removeMode);
	}
	public function Set($id,$name,$viewName,$groupid)
	{
		return $this->pkgMapper->Set($id,$name,$viewName,$groupid);
	}
	public function GetName($id,$name) //replaced &$name for mg2.6
	{
		return $this->pkgMapper->GetName($id,$name);
	}
	public function AddRelationAtt($featureLayerId,$featureTypeId,$unBindDate,$IsCurrent,$RelationMode)
	{
		return $this->pkgMapper->AddRelationAtt($featureLayerId,$featureTypeId,$unBindDate,$IsCurrent,$RelationMode);
	}
	public function SetRelationAtt($featureLayerId,$featureTypeId,$IsCurrent)
	{
		return $this->pkgMapper->SetRelationAtt($featureLayerId,$featureTypeId,$IsCurrent);
	}
	public function RemoveRelationAtt($featureLayerId,$featureTypeId,$RemoveMode)
	{
		return $this->pkgMapper->RemoveRelationAtt($featureLayerId,$featureTypeId,$RemoveMode);
	}
	public function GetListUnBindObjectDateByType($featureLayerId, $featureTypeId)
	{
		return $this->pkgMapper->GetListUnBindObjectDateByType($featureLayerId, $featureTypeId);
	}
	
	public function GetArea($input)
	{
		return $this->pkgMapper->GetArea($input);
	}
}

?>
