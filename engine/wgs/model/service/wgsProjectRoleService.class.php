<?php

lmb_require('wgs/model/service/wgsBaseRoleService.class.php');

class wgsProjectRoleService extends wgsBaseRoleService
{
    public function setArea($m,$r,$l=null,$d=null, $sdo=null){
		
		return $this->pkgMapper->setArea($m,$r,$l,$d, $sdo);
	}
	public function getArea($m,$r){
		
		return $this->pkgMapper->getArea($m,$r);
	}
	
	private function assignParentWgsRoles(&$roleRegistry, $wgsRoles)
    {
        foreach ($roleRegistry as $wgsProjectRole)
        {
            $parentWgsRoleIds = $wgsProjectRole->getParentWgsRoleIds();
            if (is_array($parentWgsRoleIds))
            foreach($parentWgsRoleIds as $parentWgsRoleId)
            $wgsProjectRole->addParent($roleRegistry[$parentWgsRoleId]);
        }
    }

    public function getList($raw = 0)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() : 
			$this->pkgMapper->getList()->getArray();
        if (!$raw)
        {
            $this->fillRoleRegistry($roleRegistry);
            $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
            $wgsRoles = $roleRegistry;
            $this->assignParentSysRoles($roleRegistry, $sysRoles);
            $this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        }
        return $roleRegistry;
    }

    public function getRoleById($roleId)
    {
        $roleRegistry = $this->pkgMapper->getList()->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $sysRoles);
        $this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        return $roleRegistry[$roleId];
    }

    //TODO: ��������� ������ �����, ������� ����� ���� ��������� ��� �������
    public function getAcceptableChildrenRolesById($roleId)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getAcceptableChildrenRolesById($roleId) :
			$this->pkgMapper->getAcceptableChildrenRolesById($roleId)->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $sysRoles);
        $this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        return $roleRegistry;
    }

    //TODO: ��������� ������ �������� �����
    public function getChildrenRolesById($roleId)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getChildrenRolesById($roleId) :
			$this->pkgMapper->getChildrenRolesById($roleId)->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $sysRoles);
        $this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        return $roleRegistry;
    }

    //TODO: ���������� ����
    public function addRole($wgsProjectRole)
    {
        return $this->pkgMapper->addRole($wgsProjectRole->getName(), $wgsProjectRole->getDescription(),$wgsProjectRole->getLocked());
    }

    public function setRole($wgsProjectRole)
    {
        $this->pkgMapper->setRole($wgsProjectRole->getId(),$wgsProjectRole->getName(), $wgsProjectRole->getDescription(),$wgsProjectRole->getLocked());
    }

    public function revokeWgsRoleMemberships($wgsProjectRoleChild, $wgsProjectRole)
    {
        $this->pkgMapper->revokeWgsRoleMemberships($wgsProjectRoleChild->getId(), $wgsProjectRole->getId());
    }

    public function grantWgsRoleMemberships($wgsProjectRoleChild, $wgsProjectRole)
    {
        $this->pkgMapper->grantWgsRoleMemberships($wgsProjectRoleChild->getId(), $wgsProjectRole->getId());
    }

    public function revokeSysRoleMemberships($wgsSysRole, $wgsProjectRole)
    {
        $this->pkgMapper->revokeSysRoleMemberships($wgsSysRole->getId(), $wgsProjectRole->getId());
    }

    public function grantSysRoleMemberships($wgsSysRole, $wgsProjectRole)
    {
        $this->pkgMapper->grantSysRoleMemberships($wgsSysRole->getId(), $wgsProjectRole->getId());
    }

    public function getSysRoleMemberships($wgsProjectRole)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getSysRoleMemberships($wgsProjectRole->getId()):
			$this->pkgMapper->getSysRoleMemberships($wgsProjectRole->getId())->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $sysRoles);
        $this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        return $roleRegistry;
    }

    public function removeRole($wgsProjectRole)
    {
        $this->pkgMapper->removeRole($wgsProjectRole->getId());
    }

}
?>
