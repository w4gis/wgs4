<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsSysModuleServiceMapper.class.php');

class wgsSysModuleService extends wgsBaseObjectService
{ 
    public function getModuleList()
	{
        $objectRegistry = $this->pkgMapper->getModuleList()->getArray();
        $this->fillObjectRegistry($objectRegistry);
        $this->assignParentObjects($objectRegistry);
        return $objectRegistry;
	}
	public function checkIntegrity($sum)
	{
        return $this->pkgMapper->checkIntegrity($sum);
	}
	
    public function getProjectModuleList($projectName)
    {
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getProjectModuleList($projectName) :
			$this->pkgMapper->getProjectModuleList($projectName)->getArray();
        $this->fillObjectRegistry($objectRegistry);
        $this->assignParentObjects($objectRegistry);
        return $objectRegistry;
    }
	
	public function getParentModuleList()
	{
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getParentModuleList() :
			$this->pkgMapper->getParentModuleList()->getArray();
        $this->fillObjectRegistry($objectRegistry);
        $this->assignParentObjects($objectRegistry);
        return $objectRegistry;
	}
	
	public function getModuleListByProjectId($projectId)
	{
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getModuleListByProjectId($projectId) :
			$this->pkgMapper->getModuleListByProjectId($projectId)->getArray();
        $this->fillObjectRegistry($objectRegistry);
        //$this->assignParentObjects($objectRegistry);
        return $objectRegistry;
	}
	
	public function getChildModuleListByProjectId($sysModuleId, $sysProjectId)
	{
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getChildModuleListByProjectId($sysModuleId, $sysProjectId) :
			$this->pkgMapper->getChildModuleListByProjectId($sysModuleId, $sysProjectId)->getArray();
        $this->fillObjectRegistry($objectRegistry);
        return $objectRegistry;
	}
	
	public function getChildModuleListByModuleId($sysModuleId)
	{
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getChildModuleListByModuleId($sysModuleId) :
			$this->pkgMapper->getChildModuleListByModuleId($sysModuleId)->getArray();
        $this->fillObjectRegistry($objectRegistry);
        return $objectRegistry;
	}
	
	public function getAssignModuleList()
	{
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getAssignModuleList() :
			$this->pkgMapper->getAssignModuleList()->getArray();
        $this->fillObjectRegistry($objectRegistry);
        return $objectRegistry;
	}
	
    public function getProjectModuleByUrl($urlString, $projectName)
    {
        if ($moduleRecord = $this->pkgMapper->getProjectModuleByUrl($urlString, $projectName))
            return wgsObject :: factory($moduleRecord);     
    }

    public function getModuleByUrl($urlString)
    {
        if ($moduleRecord = $this->pkgMapper->getModuleByUrl($urlString))
            return wgsObject :: factory($moduleRecord);     
    }
    
	public function grantSysModuleMemberships($wgsSysModule,$wgsSysProject)
	{
		$this->pkgMapper->grantSysModuleMemberships($wgsSysModule->getId(),$wgsSysProject->getId());
	}

	public function revokeSysModuleMemberships($wgsSysModule,$wgsSysProject)
	{
		$this->pkgMapper->revokeSysModuleMemberships($wgsSysModule->getId(),$wgsSysProject->getId());
	}
	
    public function createSysModuleService($wgsSysModule)
	{
		$this->pkgMapper->createSysModuleService($wgsSysModule->getName(),$wgsSysModule->getLabel(),$wgsSysModule->getUrlString(),
		$wgsSysModule->getDescription(),$wgsSysModule->isContainer(),$wgsSysModule->getPriority(),$wgsSysModule->getParentId(),
		$wgsSysModule->getInMenu(), $wgsSysModule->getLocked());
	}
	
    public function editSysModuleService($wgsSysModule)
	{
		$this->pkgMapper->editSysModuleService($wgsSysModule->getId(), $wgsSysModule->getName(),$wgsSysModule->getLabel(),$wgsSysModule->getUrlString(),
		$wgsSysModule->getDescription(),$wgsSysModule->isContainer(),$wgsSysModule->getPriority(),$wgsSysModule->getParentId(),
		$wgsSysModule->getInMenu(), $wgsSysModule->getLocked());
	}
	
	public function removeSysModuleService($wgsSysModule)
	{
		$this->pkgMapper->removeSysModuleService($wgsSysModule->getId());
	}
	
}

?>
