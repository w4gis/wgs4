<?php

lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� ��������� ������� ������ �� ������� 
 * ������������ �������� � �����.
 */
class wgsAttObjectRelationService extends wgsProjectObjectService
{
	public function GetListObjTypeRelationByParent($ParentObjTypeId)
	{
		return $this->pkgMapper->GetListObjTypeRelationByParent($ParentObjTypeId);
	}
    public function GetListChildObjectTypeByRelEx($relationId, $objectId)
    {
        return $this->pkgMapper->GetListChildObjectTypeByRelEx($relationId, $objectId);
    }
	public function GetListObjTypeRelationByChild($ChildObjTypeId)
	{
		return $this->pkgMapper->GetListObjTypeRelationByChild($ChildObjTypeId);
	}
	
    public function GetListYears()
    {
        return $this->pkgMapper->GetListYears();
    }
 public function GetListLayers()
    {
        return $this->pkgMapper->GetListLayers();
    }
     public function GetListReports()
    {
        return $this->pkgMapper->GetListReports();
    }
    
	
	public function GetListPropertyByRelation($relationId)
	{
		return $this->pkgMapper->GetListPropertyByRelation($relationId);
	}
	
public function GetListGeometryByLayer($relationId)
    {
        return $this->pkgMapper->GetListGeometryByLayer($relationId);
    }
public function GetListGroupByAttr($relationId)
    {
        return $this->pkgMapper->GetListGroupByAttr($relationId);
    }
public function GetListPropByGroup($relationId)
    {
        return $this->pkgMapper->GetListPropByGroup($relationId);
    }

public function GetListAttrByLayer($relationId)
    {
        return $this->pkgMapper->GetListAttrByLayer($relationId);
    }
	public function GetListPropertyNotByRelation($relationId)
	{
		return $this->pkgMapper->GetListPropertyNotByRelation($relationId);
	}
	public function AddPropetyByRelation($relationId,$propertyId)
	{
		return $this->pkgMapper->AddPropetyByRelation($relationId,$propertyId);
	}
	public function RemovePropetyByRelation($relationId,$propertyId)
	{
		return $this->pkgMapper->RemovePropetyByRelation($relationId,$propertyId);
	}
	public function GetListObjTypeRelation($relationId)
	{
		return $this->pkgMapper->GetListObjTypeRelation($relationId);
	}
	public function AddObjTypeRelation($ParentObjectTypeId,$ChildObjectTypeId,$RelationId)
	{
		return $this->pkgMapper->AddObjTypeRelation($ParentObjectTypeId,$ChildObjectTypeId,$RelationId);
	}
	public function RemoveObjTypeRelation($objTypeRelationId,$removeMode)
	{
		return $this->pkgMapper->RemoveObjTypeRelation($objTypeRelationId,$removeMode);
	}
	public function RemoveObjTypeRelationByData($ParentObjectTypeId,$ChildObjectTypeId,$RelationId)
	{
		return $this->pkgMapper->RemoveObjTypeRelationByData($ParentObjectTypeId,$ChildObjectTypeId,$RelationId);
	}
	public function AddRelation($name,$relationTypeId)
	{
		return $this->pkgMapper->AddRelation($name,$relationTypeId);
	}
	public function SetRelation($relationId, $name)
	{
		return $this->pkgMapper->SetRelation($relationId, $name);
	}
	public function GetListRelByObject($objectId)
	{
		return $this->pkgMapper->GetListRelByObject($objectId);
	}	
	public function GetListChildObjectByRelation($relationId, $parentObjectId)
	{
		return $this->pkgMapper->GetListChildObjectByRelation($relationId, $parentObjectId);
	}
	public function GetListChildObjectTypeByRelation($relationId)
	{
		return $this->pkgMapper->GetListChildObjectTypeByRelation($relationId);
	}
	public function AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId)
	{
		return $this->pkgMapper->AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId);
	}
    public function AddObjectRelationEx($parentFeaturesIds,$childObjectId,$objTypeRelationId)
    {
        return $this->pkgMapper->AddObjectRelationEx($parentFeaturesIds,$childObjectId,$objTypeRelationId);
    }
	public function GetValuePropertiesByObjRel($objectRelationId)
	{
		return $this->pkgMapper->GetValuePropertiesByObjRel($objectRelationId);
	}	
	public function SetValuePropertyRelation($objectRelationId,$propertyId,$propertyValue)
	{
		return $this->pkgMapper->SetValuePropertyRelation($objectRelationId,$propertyId,$propertyValue);
	}
	public function RemoveObjectRelation($objectRelationId,$removeMode)
	{
		return $this->pkgMapper->RemoveObjectRelation($objectRelationId,$removeMode);
	}
    public function RemoveObjectRelationEx($childObjectIds,$parentFeatureIds,$objectTypeRelationIds,$removeMode)
    {
        return $this->pkgMapper->RemoveObjectRelationEx($childObjectIds,$parentFeatureIds,$objectTypeRelationIds,$removeMode);
    }
    public function GetListRelationByFeature($featureId)
    {
        return $this->pkgMapper->GetListRelationByFeature($featureId);
    }
    public function GetListChildObjectByFeatures($relationId, $featureIds)
    {
        return $this->pkgMapper->GetListChildObjectByFeatures($relationId, $featureIds);
    }
}
?>