<?php

lmb_require('wgs/model/service/wgsBaseService.class.php');
lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/wgsObjectIndex.interface.php');

class wgsBaseObjectService extends wgsBaseService
{	
	protected $objectRegistry;
	
    protected function assignParentObjects(&$objectRegistry)
    {
        foreach ($objectRegistry as $object)
        {
            $parentObjectId = $object->getParentObjectId();
            if ($parentObjectId)
                $object->setParent($objectRegistry[$parentObjectId]);
        }
    }
	
    protected function fillObjectRegistry(&$objectRegistry, wgsObjectIndexInterface $index = null)
    {
        $_objectRegistry = array();
        if (is_null($index))
        {
	        foreach ($objectRegistry as $objectRecord)
	            if ($object = wgsObject :: factory($objectRecord))
	                $_objectRegistry[$object->getObjectId()] = $object;
        }
	    else
	    {
            foreach ($objectRegistry as $objectRecord)
                if ($object = wgsObject :: factory($objectRecord))
                    $_objectRegistry[$index->getIndex($object)] = $object;
	    }
        $objectRegistry = $_objectRegistry;
    }
    
	public function getList()
	{
		if (!is_null($this->objectRegistry))
		    return $this->objectRegistry;
		$this->objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() : $this->pkgMapper->getList()->getArray();
		$this->fillObjectRegistry($this->objectRegistry);
        $this->assignParentObjects($this->objectRegistry);
		return $this->objectRegistry;
	}
}

?>
