<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsTusurFlatStatsServiceMapper.class.php');

class wgsTusurFlatStatsService extends wgsBaseObjectService {
    public function getCurrentPlacesByFeatureLayerId($featureLayerId) {
        return $this->pkgMapper->getCurrentPlacesByFeatureLayerId($featureLayerId);
    }

    public function getFreePlacesByFeatureLayerId($featureLayerId) {
        return $this->pkgMapper->getFreePlacesByFeatureLayerId($featureLayerId);
    }

    public function getMaxByDocsPlacesByFeatureLayerId($featureLayerId) {
        return $this->pkgMapper->getMaxByDocsPlacesByFeatureLayerId($featureLayerId);
    }

    public function getMaxPlacesByFeatureLayerId($featureLayerId) {
        return $this->pkgMapper->getMaxPlacesByFeatureLayerId($featureLayerId);
    }

    public function getSummarySexOfStudentsByFeatureLayerId($featureLayerId) {
        return $this->pkgMapper->getSummarySexOfStudentsByFeatureLayerId($featureLayerId);
    }
}

?>