<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');

class wgsPassportService extends wgsProjectObjectService
{
/*
 *  ��������� ����
 */    
    public function getTitleList($featureId) 
    {
        return $this->pkgMapper->getTitleList($featureId);
    }
/*
 * ��������� �������� ����������
 */ 
    public function getGeneralInfo($passportId) 
    {
        return $this->pkgMapper->getGeneralInfo($passportId);
    }    
    
/*
 *  ���������������� �����
 */
    public function getBasePpart($passportId) 
    {
        return $this->pkgMapper->getBasePpart($passportId);
    }     
    
    public function getPillarPpart($passportId) 
    {
        return $this->pkgMapper->getPillarPpart($passportId);
    }

    public function getCraneGirderPpart($passportId) 
    {
        return $this->pkgMapper->getCraneGirderPpart($passportId);
    }

    public function getWallPpart($passportId) 
    {
        return $this->pkgMapper->getWallPpart($passportId);
    }
  
    public function getPeregorodkiPpart($passportId) 
    {
        return $this->pkgMapper->getPeregorodkiPpart($passportId);
    }

    public function getNesKonstruktPerekrPpart($passportId) 
    {
        return $this->pkgMapper->getNesKonstruktPerekrPpart($passportId);
    }
    
    public function getNesKonstruktKrovliPpart($passportId) 
    {
        return $this->pkgMapper->getNesKonstruktKrovliPpart($passportId);
    }
    
    public function getYteplitPpart($passportId) 
    {
        return $this->pkgMapper->getYteplitPpart($passportId);
    }
    
    public function getKrovlyaPpart($passportId) 
    {
        return $this->pkgMapper->getKrovlyaPpart($passportId);
    }
/*
 * ��������-������� �����
 */
    public function getBaseSBpart($passportId) 
    {
        return $this->pkgMapper->getBaseSBpart($passportId);
    }
    
    public function getNesKarkasSBpart($passportId) 
    {
        return $this->pkgMapper->getNesKarkasSBpart($passportId);
    }
    
    public function getWallSBpart($passportId) 
    {
        return $this->pkgMapper->getWallSBpart($passportId);
    }
    
    public function getPeregorodkiSBpart($passportId) 
    {
        return $this->pkgMapper->getPeregorodkiSBpart($passportId);
    }
    
    public function getMejStorePerekrSBpart($passportId) 
    {
        return $this->pkgMapper->getMejStorePerekrSBpart($passportId);
    }
    
    public function getCherdakPerekrSBpart($passportId) 
    {
        return $this->pkgMapper->getCherdakPerekrSBpart($passportId);
    }
    
    public function getLestnizaSBpart($passportId) 
    {
        return $this->pkgMapper->getLestnizaSBpart($passportId);
    }
    
    public function getNesElemKrovliSBpart($passportId) 
    {
        return $this->pkgMapper->getNesElemKrovliSBpart($passportId);
    }
    
    public function getKrovlyaSBpart($passportId) 
    {
        return $this->pkgMapper->getKrovlyaSBpart($passportId);
    }
    
    public function getAreaOfRooms($passportId) 
    {
        return $this->pkgMapper->getAreaOfRooms($passportId);
    }
    
    public function getAreaOfOtherRooms($passportId) 
    {
        return $this->pkgMapper->getAreaOfOtherRooms($passportId);
    }
    
    public function getAreaOfFloor($passportId) 
    {
        return $this->pkgMapper->getAreaOfFloor($passportId);
    }
    
    public function getRazmeriNarujPoverhn($passportId) 
    {
        return $this->pkgMapper->getRazmeriNarujPoverhn($passportId);
    }
    
    public function getRazmeriStekol1($passportId) 
    {
        return $this->pkgMapper->getRazmeriStekol1($passportId);
    }
    
    public function getRazmeriStekol2($passportId) 
    {
        return $this->pkgMapper->getRazmeriStekol2($passportId);
    }
    
    public function getRoofingValue($passportId) 
    {
        return $this->pkgMapper->getRoofingValue($passportId);
    }
    
    public function getRazmeriVnytrPoverhn($passportId) 
    {
        return $this->pkgMapper->getRazmeriVnytrPoverhn($passportId);
    }
    
    public function getIsCeiling($passportId) 
    {
        return $this->pkgMapper->getIsCeiling($passportId);
    }
    
    public function getIsWall($passportId) 
    {
        return $this->pkgMapper->getIsWall($passportId);
    }
    
    public function getIsSepta($passportId) 
    {
        return $this->pkgMapper->getIsSepta($passportId);
    }
    
    public function getIsPillar($passportId) 
    {
        return $this->pkgMapper->getIsPillar($passportId);
    }
    
    public function getIsCraneGirder($passportId) 
    {
        return $this->pkgMapper->getIsCraneGirder($passportId);
    }
    
    public function getIsOther($passportId) 
    {
        return $this->pkgMapper->getIsOther($passportId);
    }
    
    public function getNote($passportId) 
    {
        return $this->pkgMapper->getNote($passportId);
    }
    
    public function getFloorPlan($passportId) 
    {
        return $this->pkgMapper->getFloorPlan($passportId);
    }
}
?>