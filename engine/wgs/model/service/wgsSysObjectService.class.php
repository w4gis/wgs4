<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsSysObjectServiceMapper.class.php');
lmb_require('wgs/model/service/wgsSysProjectService.class.php');
lmb_require('wgs/model/mapper/wgsSysProjectServiceMapper.class.php');
lmb_require('wgs/model/service/wgsSysModuleService.class.php');
lmb_require('wgs/model/mapper/wgsSysModuleServiceMapper.class.php');
lmb_require('wgs/model/service/wgsSysPrivilegeService.class.php');
lmb_require('wgs/model/mapper/wgsSysPrivilegeServiceMapper.class.php');

class wgsSysObjectService extends wgsBaseObjectService
{
    protected $sysModuleService;
    protected $sysProjectService;
    protected $sysPrivilegeService;
    
    public function getSysModuleService()
	{
	    if (is_null($this->sysModuleService))
            $this->sysModuleService = new wgsSysModuleService(new wgsSysModuleServiceMapper, $this->getDbConnection());
        return $this->sysModuleService;
	}
	
    public function getSysProjectService()
	{
        if (is_null($this->sysProjectService))
            $this->sysProjectService = new wgsSysProjectService(new wgsSysProjectServiceMapper, $this->getDbConnection());
        return $this->sysProjectService;
	}

    public function getSysPrivilegeService()
    {
        if (is_null($this->sysPrivilegeService))
            $this->sysPrivilegeService = new wgsSysPrivilegeService(new wgsSysPrivilegeServiceMapper, $this->getDbConnection());
        return $this->sysPrivilegeService;
    }
	public function checkIntegrity($sum){
		return $this->getSysModuleService()->checkIntegrity($sum);
	}
    
}

?>