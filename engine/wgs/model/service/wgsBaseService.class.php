<?php

class wgsBaseService
{
	protected $pkgMapper;
	protected $dbConnection;
	
    public function __construct($mapper, $dbConnection)
    {
    	//echo 'constructing...';
    	$this->pkgMapper = $mapper;
    	//echo 'setting db...';
    	$this->pkgMapper->setDbConnection($dbConnection);
    	//echo 'almost done...';
    	$this->dbConnection = $dbConnection;
    }
    
    public function getDbConnection()
    {
        return $this->dbConnection;
    }
}

?>
