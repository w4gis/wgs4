<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� �������� ������� ������ � ��������� ���� Map.
 * 
 */
class wgsMgMapService extends wgsProjectObjectService
{
	public function addMgMap($resourceId,$resourceIds)
	{

		return $this->pkgMapper->addMgMap($resourceId,$resourceIds);
	}

	public function setMgMap($mgMapId,$layerResourceId,$legendLabel,$name)
	{
		$this->pkgMapper->setMgMap($mgMapId,$layerResourceId,$legendLabel,$name);
	}

	public function renameMgMap($oldName,$newName)
	{
		$this->pkgMapper->renameMgMap($oldName,$newName);
	}

	public function copyMgMap($source,$destination)
	{
		$this->pkgMapper->copyMgMap($source,$destination);
	}
	
	public function removeMgMap($resourceId)
	{
		$this->pkgMapper->removeMgMap($resourceId);
	}
}

?>