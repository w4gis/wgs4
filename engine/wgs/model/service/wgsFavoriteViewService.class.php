<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsFavoriteViewServiceMapper.class.php');

class wgsFavoriteViewService extends wgsBaseObjectService
{
    public function insertView($number, $wgsuser_id, $center, $scale, $name, $defaultWebLayout, $picture, $cache_name, $layer_view)
    {
        return $this->pkgMapper->insertView($number, $wgsuser_id, $center, $scale, $name, $defaultWebLayout, $picture, $cache_name, $layer_view);
    }
    
    public function getGeometryPropretyByUserId($wgsuser_id, $defaultWebLayout)
    {
        $objectRegistry = $this->pkgMapper->getGeometryPropretyByUserId($wgsuser_id, $defaultWebLayout)->getArray();
        return $objectRegistry;
    }
    
    public function getGeometry($favoriteview, $wgsuser_id, $defaultWebLayout)
    {
        $objectRegistry = $this->pkgMapper->getGeometry($favoriteview, $wgsuser_id, $defaultWebLayout)->getArray();
        return $objectRegistry;
    }
    
	public function getCount($wgsuser_id)
    {
        $c = $this->pkgMapper->getCount($wgsuser_id);
        return $c;
    }
	
    public function removeView($number, $wgsuser_id, $defaultWebLayout)
    {
        return $this->pkgMapper->removeView($number, $wgsuser_id, $defaultWebLayout);
    }
    
    public function updateName($number, $wgsuser_id, $name, $defaultWebLayout)
    {
        return $this->pkgMapper->updateName($number, $wgsuser_id, $name, $defaultWebLayout);
    }
    
    public function getBlob($num, $wgsuser_id, $defaultWebLayout)
    {
        $objectRegistry = $this->pkgMapper->getBlob($num, $wgsuser_id, $defaultWebLayout)->getArray();
        return $objectRegistry;
    }
}

?>