<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� �������� ������� ������ � ��������� ���� WebLayout.
 * 
 */
class wgsMgWebLayoutService extends wgsProjectObjectService
{
	public function addMgWebLayout($resourceId,$title,$mapResourceId,$names,$invokeScripts)
	{
		return $this->pkgMapper->addMgWebLayout($resourceId,$title,$mapResourceId,$names,$invokeScripts);
	}

	public function setMgWebLayout($mgWebLayoutId,$name,$label,$description,$invokeScript,$imageURL)
	{
		$this->pkgMapper->setMgWebLayout($mgWebLayoutId,$name,$label,$description,$invokeScript,$imageURL);
	}
	/*
    public function updateMgWebLayout($webLayout)
    {
        $this->pkgMapper->updateMgWebLayout(
            $webLayout->getId(),
            null,//$name,
            null,//$webLayout->getMgResourceId(),
            null,//$label,
            null,//$description,
            null,//$invokeScript,
            null//$webLayout->getByDefault()
        );
    }
	*/
	public function setDefaultWebLayout($webLayout)
	{
        $this->pkgMapper->setDefaultWebLayout($webLayout->getId());
	}

    public function setOverviewWebLayout($webLayout)
    {
        $this->pkgMapper->setOverviewWebLayout($webLayout->getId());
    }
	
	public function renameMgWebLayout($oldName,$newName)
	{
		$this->pkgMapper->renameMgWebLayout($oldName,$newName);
	}
	
	public function copyMgWebLayout($source,$destination)
	{
		$this->pkgMapper->copyMgWebLayout($source,$destination);
	}

	public function removeMgWebLayout($resourceId)
	{
		$this->pkgMapper->removeMgWebLayout($resourceId);
	}

    public function getWebLayoutList($raw = 0)
    {
        if (!is_null($this->webLayoutRegistry))
            return $this->webLayoutRegistry;
        $this->webLayoutRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getWebLayoutList():
			$this->pkgMapper->getWebLayoutList()->getArray();
        if (!$raw)
            $this->fillObjectRegistry($this->webLayoutRegistry, new wgsWebLayoutIndex);
        return $this->webLayoutRegistry;
    }
	
    public function getOverviewWebLayout()
    {
        if ($webLayouts = $this->getWebLayoutList())
        {
            foreach($webLayouts as $overviewWebLayout)
                if ($overviewWebLayout->getOverview())
                    break;
            return $overviewWebLayout;
        }
    }
	
	//widget for documents
	public function getWidgetWebLayout()
    {
        if ($webLayouts = $this->getWebLayoutList())
        {
            foreach($webLayouts as $overviewWebLayout)
                if ($overviewWebLayout->getWidget())
                    break;
            return $overviewWebLayout;
        }
    }
    
    public function getDefaultWebLayout()
    {
        if ($webLayouts = $this->getWebLayoutList())
        {
            foreach($webLayouts as $defaultWebLayout)
                if ($defaultWebLayout->getByDefault())
                    break;
            return $defaultWebLayout;
        }
    }
    
    public function getFloorplanWebLayout($fetureId, $propertyType)
    {
    	$record = $this->pkgMapper->getFloorplanWebLayout($fetureId, $propertyType);
        if ($record instanceof lmbOciRecord && $webLayouts = $this->getWebLayoutList())
        {
            foreach($webLayouts as $webLayout) {
                if ($webLayout->getId() == $record->get('mgweblayout_id'))
                    break;
            }
            return $webLayout;
        }
    }
}

?>
