<?php

lmb_require('wgs/model/service/wgsBaseUserService.class.php');

class wgsProjectUserService extends wgsBaseUserService
{
			
	public function revokeWgsRoleMemberships($wgsProjectUser, $wgsProjectRole)
	{
		$this->pkgMapper->revokeWgsRoleMemberships($wgsProjectUser->getId(), $wgsProjectRole->getId());
	}

	public function grantWgsRoleMemberships($wgsProjectUser, $wgsProjectRole)
	{
		$this->pkgMapper->grantWgsRoleMemberships($wgsProjectUser->getId(), $wgsProjectRole->getId());
	}
}

?>