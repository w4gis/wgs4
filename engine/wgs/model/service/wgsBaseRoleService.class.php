<?php

lmb_require('wgs/model/service/wgsBaseService.class.php');
lmb_require('wgs/model/wgsRole.class.php');

class wgsBaseRoleService extends wgsBaseService
{
    protected function assignParentSysRoles(&$roleRegistry, $wgsSysRoles)
    {
        if (is_array($wgsSysRoles))
        {
	        foreach ($roleRegistry as $role)
	        {  
	            $parentSysRoleIds = $role->getParentSysRoleIds();
	            if (is_array($parentSysRoleIds))
	                foreach($parentSysRoleIds as $parentSysRoleId)
	                   if (isset($wgsSysRoles[$parentSysRoleId])) {
                            $role->addParent($wgsSysRoles[$parentSysRoleId]);
	                   }
	        }
        }
    }

    protected function fillRoleRegistry(&$roleRegistry)
    {
        $_roleRegistry = array();
        foreach ($roleRegistry as $roleRecord) {
            if ($role = wgsRole :: factory($roleRecord)) {
                if(isset($_roleRegistry[$role->getId()])) {
                    if($role instanceof wgsSysRole) {
                        $_roleRegistry[$role->getId()] = $this->prepareSysRoles(
                                                               $_roleRegistry[$role->getId()],
                                                               $role
                                                         );
                    } else if($role instanceof wgsProjectRole) {
                        $_roleRegistry[$role->getId()] = $this->prepareProjectRoles(
                                                               $_roleRegistry[$role->getId()],
                                                               $role
                                                         );
                    }
                } else {
            	    $_roleRegistry[$role->getId()] = $role;
                }
            }
        }
        $roleRegistry = $_roleRegistry;
    }

    /**
     * Объеденяет строки с одинаковыми id в одну
     */
    private function prepareSysRoles($role1, $role2) {
        $role2_sysUserMembers = $role2->getSysUserMembers();
        if(!in_array($role2_sysUserMembers[0], $role1->getSysUserMembers())) {
            $role1->setSysUserMembers($role2_sysUserMembers[0]);
        }

        $role2_parentSysRoleIds = $role2->getParentSysRoleIds();
        if(!in_array($role2_parentSysRoleIds[0], $role1->getParentSysRoleIds())) {
            $role1->setParentSysRoleIds($role2_parentSysRoleIds[0]);
        }

        return $role1;
    }

    /**
     * Объединяет две строки с одинаковым id из таблицы в одну
     */
    private function prepareProjectRoles($role1, $role2) {
        $role2_wgsUserMembers = $role2->getWgsUserMembers();
        if(!in_array($role2_wgsUserMembers[0], $role1->getWgsUserMembers())) {
            $role1->setWgsUserMembers($role2_wgsUserMembers[0]);
        }

        $role2_parentSysRoleIds = $role2->getParentSysRoleIds();
        if(!in_array($role2_parentSysRoleIds[0], $role1->getParentSysRoleIds())) {
            $role1->setParentSysRoleIds($role2_parentSysRoleIds[0]);
        }

        $role2_parentWgsRoleIds = $role2->getParentWgsRoleIds();
        if(!in_array($role2_parentWgsRoleIds[0], $role1->getParentWgsRoleIds())) {
            $role1->setParentWgsRoleIds($role2_parentWgsRoleIds[0]);
        }

        return $role1;
    }
    
    protected function fillShortRoleRegistry(&$roleRegistry)
    {
        $_roleRegistry = array();
        foreach ($roleRegistry as $roleRecord)
            if ($roleRecord->get('sysrole_id') <> 0)
            if ($role = wgsRole :: factory($roleRecord))
                $_roleRegistry[$role->getId()] = $role;
        $roleRegistry = $_roleRegistry;
    }
}

?>