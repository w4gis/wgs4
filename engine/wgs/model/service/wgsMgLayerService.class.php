<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� ��������� ������� ������ � ��������� ���� Layer.
 * 
 */
class wgsMgLayerService extends wgsProjectObjectService
{
	public function isMgLayerExist($resourceId)
	{
		return $this->pkgMapper->isMgLayerExist($resourceId);
	}

	public function addMgLayer($resourceId,$featureLayerName)
	{
		return $this->pkgMapper->addMgLayer($resourceId,$featureLayerName);
	}

	public function setMgLayer($resourceId,$featureLayerName)
	{
		return $this->pkgMapper->setMgLayer($resourceId,$featureLayerName);
	}
	
	public function renameMgLayer($oldName,$newName)
	{
		$this->pkgMapper->renameMgLayer($oldName,$newName);
	}

	public function copyMgLayer($source,$destination)
	{
		$this->pkgMapper->copyMgLayer($source,$destination);
	}

	public function removeMgLayer($resourceId)
	{
		$this->pkgMapper->removeMgLayer($resourceId);
	}

	public function getMgLayerListByEntityIds($features)
	{
	    return $this->pkgMapper->getMgLayerListByEntityIds($features);
	}
	public function getMgLayerListByFLayerIds($features)
	{
	    return $this->pkgMapper->getMgLayerListByFLayerIds($features);
	}
	
	public function getMgLayerListByFLayerIdsI($features)
	{
	    return $this->pkgMapper->getMgLayerListByFLayerIdsI($features);
	}
}

?>