<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/service/wgsMgLayerService.class.php');
lmb_require('wgs/model/mapper/wgsMgLayerServiceMapper.class.php');
lmb_require('wgs/model/service/wgsMgMapService.class.php');
lmb_require('wgs/model/mapper/wgsMgMapServiceMapper.class.php');
lmb_require('wgs/model/service/wgsMgWebLayoutService.class.php');
lmb_require('wgs/model/service/wgsMgResourceService.class.php');
lmb_require('wgs/model/mapper/wgsMgResourceServiceMapper.class.php');

lmb_require('wgs/model/service/wgsAttObjectTypeService.class.php');
lmb_require('wgs/model/mapper/wgsAttObjectTypeServiceMapper.class.php');

lmb_require('wgs/model/service/wgsAttPropertyService.class.php');
lmb_require('wgs/model/mapper/wgsAttPropertyServiceMapper.class.php');

lmb_require('wgs/model/service/wgsAttObjectRelationService.class.php');
lmb_require('wgs/model/mapper/wgsAttObjectRelationServiceMapper.class.php');

lmb_require('wgs/model/service/wgsAttObjectService.class.php');
lmb_require('wgs/model/mapper/wgsAttObjectServiceMapper.class.php');

lmb_require('wgs/model/service/wgsPassportService.class.php');
lmb_require('wgs/model/mapper/wgsPassportServiceMapper.class.php');

lmb_require('wgs/model/service/wgsFeatureLayerService.class.php');
lmb_require('wgs/model/mapper/wgsFeatureLayerServiceMapper.class.php');

lmb_require('wgs/model/service/wgsFeatureLayerGroupService.class.php');
lmb_require('wgs/model/mapper/wgsFeatureLayerGroupServiceMapper.class.php');

lmb_require('wgs/model/mapper/wgsMgWebLayoutServiceMapper.class.php');
lmb_require('wgs/model/wgsObjectIndexInterface.interface.php');

class wgsMapLayerIndex implements wgsObjectIndexInterface
{
	public function getIndex(&$object)
	{
		return $object->getId();
	}
}

class wgsWebLayoutIndex implements wgsObjectIndexInterface
{
	public function getIndex(&$object)
	{
		return $object->getId();
	}
}

class wgsFeatuerLayerGroupIndex implements wgsObjectIndexInterface
{
	public function getIndex(&$object)
	{
		return $object->getId();
	}
}

class wgsProjectObjectService extends wgsBaseObjectService
{
    protected $mgLayerService;
    protected $mgMapService;
    protected $mgWebLayoutService;
    
	protected $webLayoutRegistry;
	protected $mgMapLayerRegistry;
	protected $bottomLayerRegistry;
	protected $featureLayerRegistry;
	protected $featureLayerGroupRegistry;
	protected $builtInCommandRegistry;
	protected $customCommandRegistry;
	protected $paneRegistry;

	public function getAttObjectService()
	{
	   // ini_set('display_errors',1);
    //     error_reporting(E_ALL);
		return new wgsAttObjectService(
		  new wgsAttObjectServiceMapper,
          $this->getDbConnection()
		);
	}

    public function getPassportService()
    {
        return new wgsPassportService(
          new wgsPassportServiceMapper,
          $this->getDbConnection()
        );
    }
	
	public function getAttObjectRelationService()
	{
	    return new wgsAttObjectRelationService(
		  new wgsAttObjectRelationServiceMapper,
          $this->getDbConnection()
		);
	}
	
	public function getAttPropertyService()
	{
		return new wgsAttPropertyService(
		  new wgsAttPropertyServiceMapper,
          $this->getDbConnection()
		);
	}
	
	public function getAttObjectTypeService()
	{
		return new wgsAttObjectTypeService(
		  new wgsAttObjectTypeServiceMapper,
          $this->getDbConnection()
		);
	}
	public function getFeatureLayerService()
	{
		return new wgsFeatureLayerService(
		  new wgsFeatureLayerServiceMapper,
          $this->getDbConnection()
		);
	//	echo('done\n');
	}
	public function getFeatureLayerGroupService()
	{
		return new wgsFeatureLayerGroupService(
		  new wgsFeatureLayerGroupServiceMapper,
          $this->getDbConnection()
		);
	}
	public function getFeatureLayerGroupListEx()
	{
		return $this->pkgMapper->getFeatureLayerGroupList();
	}
	public function GetListFeatureLayerByType($featureTypeId)
	{
		return $this->pkgMapper->GetListFeatureLayerByType($featureTypeId);
	}
	
	public function getMgLayerService()
	{
	    if (is_null($this->mgLayerService))
    		$this->mgLayerService = new wgsMgLayerService(new wgsMgLayerServiceMapper, $this->getDbConnection());
        return $this->mgLayerService;
	}

	public function getMgMapService()
	{
	    if (is_null($this->mgMapService))
            $this->mgMapService = new wgsMgMapService(new wgsMgMapServiceMapper, $this->getDbConnection());
        return $this->mgMapService;
	}

	public function getMgWebLayoutService()
	{
        if (is_null($this->mgWebLayoutService))
            $this->mgWebLayoutService = new wgsMgWebLayoutService(new wgsMgWebLayoutServiceMapper, $this->getDbConnection());
        return $this->mgWebLayoutService;
	}
	
    public function getMgResourceService()
    {
        if (is_null($this->mgResourceService))
            $this->mgResourceService = new wgsMgResourceService(new wgsMgResourceServiceMapper, $this->getDbConnection());
        return $this->mgResourceService;
    }

	//TODO: to services =>
		
	public function getMgMapLayerList()
	{
		if (!is_null($this->mgMapLayerRegistry))
		    return $this->mgMapLayerRegistry;
		$this->mgMapLayerRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getMgMapLayerList():
			$this->pkgMapper->getMgMapLayerList()->getArray();
		$this->fillObjectRegistry($this->mgMapLayerRegistry, new wgsMapLayerIndex);
		return $this->mgMapLayerRegistry;
	}

	public function getBottomLayerList()
	{
		if (!is_null($this->bottomLayerRegistry))
		    return $this->bottomLayerRegistry;
		$this->bottomLayerRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getBottomLayerList():
			$this->pkgMapper->getBottomLayerList()->getArray();
		$this->fillObjectRegistry($this->bottomLayerRegistry);
		return $this->bottomLayerRegistry;
	}
	
	public function getFeatureLayerList()
	{
		if (!is_null($this->featureLayerRegistry))
		    return $this->featureLayerRegistry;
		$this->featureLayerRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getFeatureLayerList():
			$this->pkgMapper->getFeatureLayerList()->getArray();
		$this->fillObjectRegistry($this->featureLayerRegistry);
		return $this->featureLayerRegistry;
	}

	public function getFeatureLayerGroupList()
	{
		if (!is_null($this->featureLayerGroupRegistry))
		    return $this->featureLayerGroupRegistry;
		$this->featureLayerGroupRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getFeatureLayerGroupList():
			$this->pkgMapper->getFeatureLayerGroupList()->getArray();
		$this->fillObjectRegistry($this->featureLayerGroupRegistry, new wgsFeatuerLayerGroupIndex);
		return $this->featureLayerGroupRegistry;
	}

    public function getBuiltInCommandList()
    {
		if (!is_null($this->builtInCommandRegistry))
		    return $this->builtInCommandRegistry;
		$this->builtInCommandRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getBuiltInCommandList():
			$this->pkgMapper->getBuiltInCommandList()->getArray();
		$this->fillObjectRegistry($this->builtInCommandRegistry);
		return $this->builtInCommandRegistry;
    }

    public function getCustomCommandList()
    {
		if (!is_null($this->customCommandRegistry))
		    return $this->customCommandRegistry;
		$this->customCommandRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getCustomCommandList() : $this->pkgMapper->getCustomCommandList()->getArray();
		$this->fillObjectRegistry($this->customCommandRegistry);
		return $this->customCommandRegistry;
    }
    
    public function getPaneList()
    {
		if (!is_null($this->paneRegistry))
		    return $this->paneRegistry;
		$this->paneRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getPaneList():
			$this->pkgMapper->getPaneList()->getArray();
		$this->fillObjectRegistry($this->paneRegistry);
		return $this->paneRegistry;
    }
}

?>
