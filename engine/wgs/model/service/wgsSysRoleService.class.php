<?php

lmb_require('wgs/model/service/wgsBaseRoleService.class.php');

class wgsSysRoleService extends wgsBaseRoleService
{
    public function addRole($wgsSysRole)
    {
        return $this->pkgMapper->addRole(   $wgsSysRole->getName(),
        $wgsSysRole->getDescription());
    }

    public function setRole($wgsSysRole)
    {
        $this->pkgMapper->setRole(  $wgsSysRole->getId(),
        $wgsSysRole->getName(),
        $wgsSysRole->getDescription());
    }

    public function removeRole($wgsSysRole)
    {
        $this->pkgMapper->removeRole($wgsSysRole->getId());
    }

    public function getList()
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() : $this->pkgMapper->getList()->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry;
    }

    public function getShortList()
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() : $this->pkgMapper->getList()->getArray();
        $this->fillRoleRegistry($roleRegistry);
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry;
    }

    public function getAcceptableChildrenRolesById($wgsSysRoleId)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getAcceptableChildrenRolesById($wgsSysRoleId): $this->pkgMapper->getAcceptableChildrenRolesById($wgsSysRoleId)->getArray();
        $this->fillRoleRegistry($roleRegistry);
        //$wgsSysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry;
    }

    public function getRoleById($wgsSysRoleId)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() : $this->pkgMapper->getList()->getArray();
        $this->fillRoleRegistry($roleRegistry);
        //$wgsSysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry[$wgsSysRoleId];
    }

    public function getChildrenRolesById($wgsSysRoleId)
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getChildrenRolesById($wgsSysRoleId) : $this->pkgMapper->getChildrenRolesById($wgsSysRoleId)->getArray();
        $this->fillRoleRegistry($roleRegistry);
        //$wgsSysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry;
    }

    public function getHighestSysRoles()
    {
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getHighestSysRoles() : $this->pkgMapper->getHighestSysRoles()->getArray();
        $this->fillRoleRegistry($roleRegistry);
        //$wgsSysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsSysRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $wgsSysRoles);
        return $roleRegistry;
    }

    public function revokeSysRoleMemberships($wgsSysRole)
    {
        $this->pkgMapper->revokeSysRoleMemberships($wgsSysRole->getId());
    }

    public function grantSysRoleMemberships($wgsSysRoleChild, $wgsSysRole)
    {
        $this->pkgMapper->grantSysRoleMemberships($wgsSysRoleChild->getId(), $wgsSysRole->getId());
    }

    public function revokeSysRoleWgsRole($sysProject, $wgsSysRole, $wgsProjectRole)
    {
        $this->pkgMapper->revokeSysRoleWgsRole($sysProject->getId(),$wgsSysRole->getId(), $wgsProjectRole->getId());
    }

    public function grantSysRoleWgsRole($sysProject, $wgsSysRole, $wgsProjectRole)
    {
        $this->pkgMapper->grantSysRoleWgsRole($sysProject->getId(), $wgsSysRole->getId(), $wgsProjectRole->getId());
    }

    public function getSysRoleMemberships($sysProject,$wgsProjectRole)
    {         
        $roleRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getSysRoleMemberships($sysProject->getId(), $wgsProjectRole->getId()):
			$this->pkgMapper->getSysRoleMemberships($sysProject->getId(), $wgsProjectRole->getId())->getArray();

        $this->fillRoleRegistry($roleRegistry);
        $sysRoles = lmbToolkit :: instance()->getWgsTier()->getSysTier()->getRoles();
        $wgsRoles = $roleRegistry;
        $this->assignParentSysRoles($roleRegistry, $sysRoles);
        //$this->assignParentWgsRoles($roleRegistry, $wgsRoles);
        return $roleRegistry;
    }
}

?>
