<?php

lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� ��������� ������� ������ � ��������� ���� ObjectType.
 * 
 */
class wgsAttObjectTypeService extends wgsProjectObjectService
{
	public function Add($name,$shortname,$note,$adddate,$objectLabel)
	{
		return $this->pkgMapper->Add($name,$shortname,$note,$adddate,$objectLabel);
	}
	public function Set($featureTypeId,$name,$shortname,$note,$adddate,$objectLabel)
	{
		return $this->pkgMapper->Set($featureTypeId,$name,$shortname,$note,$adddate,$objectLabel);
	}
	public function Get($featureTypeId,&$name,&$shortname,&$note,&$adddate,&$objectLabel)
	{
		return $this->pkgMapper->Get($featureTypeId,$name,$shortname,$note,$adddate,$objectLabel);
	}
	
	public function GetName($featureTypeId,&$name)
	{
		return $this->pkgMapper->GetName($featureTypeId,$name);
	}
	
	public function Remove($featureTypeId,$removeMode)
	{
		return $this->pkgMapper->Remove($featureTypeId,$removeMode);
	}
	
	public function getListObjectByType($featureTypeId)
	{
		return $this->pkgMapper->getListObjectByType($featureTypeId);
	}
	public function GetCountListObjectByExample($featureId){
		return $this->pkgMapper->GetCountListObjectByExample($featureId);
	}
	public function GetListFeatureTypeByLayer($featureLayerId = null)
	{
		return $this->pkgMapper->GetListFeatureTypeByLayer($featureLayerId);
	}
	
	public function GetListFeatureTypeNotByLayer($featureLayerId)
	{
		return $this->pkgMapper->GetListFeatureTypeNotByLayer($featureLayerId);
	}
	
	public function getListPropertyByObjectType($featureTypeId,$ACADView=null)
	{
		return $this->pkgMapper->getListPropertyByObjectType($featureTypeId,$ACADView);
	}
	public function GetListPropertyNotByObjectType($featureTypeId)
	{
		return $this->pkgMapper->GetListPropertyNotByObjectType($featureTypeId);
	}
	
    public function getBaseProperties()
    {
        return array(
            new lmbPgsqlRecord(array(
                'name' => '�a����������',
                'property_group_name' => '��������',
                'property_id' => 'b1',
                'value_properties_type_id' => '7'
            )),
            new lmbPgsqlRecord(array(
                'name' => '��a���� ������������',
                'property_group_name' => '��������',
                'property_id' => 'b2',
                'value_properties_type_id' => '7'
            )),
            new lmbPgsqlRecord(array(
                'name' => '�a�� ���������',
                'property_group_name' => '��������',
                'property_id' => 'b3',
                'value_properties_type_id' => '2'
            )),
            new lmbPgsqlRecord(array(
                'name' => '������a���',
                'property_group_name' => '��������',
                'property_id' => 'b4',
                'value_properties_type_id' => '2'
            ))
        );
    }
	
	public function AddPropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		return $this->pkgMapper->AddPropertyByObjectType($featureTypeId,$PropertyId,$aCadView);
	}
	
	public function RemovePropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		return $this->pkgMapper->RemovePropertyByObjectType($featureTypeId,$PropertyId,$aCadView);
	}
	
	public function SetPropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		return $this->pkgMapper->SetPropertyByObjectType($featureTypeId,$PropertyId,$aCadView);
	}
	
    public function getFeatureTypesByFeature($entityId)
    {
        return $this->pkgMapper->getFeatureTypesByFeature($entityId);
    }
    
    public function getListUnbindCardsFeatureType()
    {
        return $this->pkgMapper->getListUnbindCardsFeatureType();
    }

    public function getListUnbindCardsDateByType($featureTypeId)
    {
        return $this->pkgMapper->getListUnbindCardsDateByType($featureTypeId);
    }
}

?>