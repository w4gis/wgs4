<?php

lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� ��������� ������� ������ � ��������� ���� ObjectType.
 * 
 */
class wgsAttPropertyService extends wgsProjectObjectService
{
	public function getListPropertyOfPropertyGroup($PropertyGroupId)
	{
		return $this->pkgMapper->getListPropertyOfPropertyGroup($PropertyGroupId);
	}
	public function RemoveProperty($propId,$removeMode)
	{
		return $this->pkgMapper->RemoveProperty($propId,$removeMode);
	}
	public function Add($propName, $autoCADname, $valueType, $propGroupId, $placDate)
	{
		return $this->pkgMapper->Add($propName, $autoCADname, $valueType, $propGroupId, $placDate);
	}
	public function Set($propId, $propName, $autoCADname, $valueType, $propGroupId, $placDate)
	{
		return $this->pkgMapper->Set($propId, $propName, $autoCADname, $valueType, $propGroupId, $placDate);
	}
	
	public function getListPropertyGroup()
	{
		return $this->pkgMapper->getListPropertyGroup();
	}
	public function AddGroupProperty($propGroupName)
	{
		return $this->pkgMapper->AddGroupProperty($propGroupName);
	}
	public function SetGroupProperty($propGroupId,$propGroupName)
	{
		return $this->pkgMapper->SetGroupProperty($propGroupId,$propGroupName);
	}
	public function RemoveGroupProperty($propGroupId,$removeMode)
	{
		return $this->pkgMapper->RemoveGroupProperty($propGroupId,$removeMode);
	}
	public function GetGroupProperty($propGroupId)
	{
		return $this->pkgMapper->GetGroupProperty($propGroupId);
	}
	
	public function GetValueType($valueTypeId)
	{
		return $this->pkgMapper->GetValueType($valueTypeId);
	}
	public function GetListValueType()
	{
		return $this->pkgMapper->GetListValueType();
	}
	
	public function GetListValueDomain($propId)
	{
		return $this->pkgMapper->GetListValueDomain($propId);
	}
	public function AddValueDomain($propId,$valueDomain)
	{
		return $this->pkgMapper->AddValueDomain($propId,$valueDomain);
	}
	public function SetValueDomain($valueDomainId,$valueDomain)
	{
		return $this->pkgMapper->SetValueDomain($valueDomainId,$valueDomain);
	}
	public function RemoveValueDomain($valueDomainId)
	{
		return $this->pkgMapper->RemoveValueDomain($valueDomainId);
	}
}

?>