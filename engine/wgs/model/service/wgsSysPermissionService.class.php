<?php

lmb_require('wgs/model/service/wgsBasePermissionService.class.php');

class wgsSysPermissionService extends wgsBasePermissionService
{
    public function getPrivileges()
    {
        return $this->pkgMapper->getPrivileges()->getArray();
    }
    
    public function getPrivilegeBySysObjectTypeId($sysobjecttype_id)
    {
        return $this->pkgMapper->getPrivilegeBySysObjectTypeId($sysobjecttype_id)->getArray();
    }
    
    public function getPermissionsByRoleId($roleId, $objectId)
    {
        $permissionRecordSet = $this->pkgMapper->getPermissionsByRoleId($roleId, $objectId)->getArray();
        $permissions = array();
        foreach ($permissionRecordSet as $permissionRecord)
        {
            if ($permission = new wgsPermission($permissionRecord))
                $permissions[] = $permission;
        }
        return $permissions;
    }
}

?>