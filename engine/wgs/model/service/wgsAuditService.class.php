<?php
	
	lmb_require('wgs/model/service/wgsBaseService.class.php');
	//lmb_require('wgs/model/service/wgsAudit.class.php');
	lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
	lmb_require('wgs/model/mapper/wgsAudit.class.php');
	
	class wgsAuditService extends wgsBaseService
{
	

//--Action - �������� � �������  
	public function getAuditActivity(){
		return $this->pkgMapper->getAuditActivity();
	}
	public function setAuditActivity($state, $id, $host, $reason){
		return $this->pkgMapper->setAuditActivity($state, $id, $host, lmb_utf8_to_win1251($reason));
	}
	public function AddAction($name, $guiName, $inUse, $description)//������ �������� ��������, �������� �������� ��� ���, ������������ ��, �������� ��������, �������� ��
    {
        return $this->pkgMapper->AddAction($name, $guiName, $inUse, $description);
    }
	public function deleteLiAction($name)//������� �������. �������� �������� 
    {
		$this->pkgMapper->deleteLiAction($name);
    }
	public function getLiActionIdByName($name)//�������� �� �� ����� 
    {
        
        return $this->pkgMapper->getLiActionIdByName($name);
    }
	public function setActionListeningOn($name)//������� ��������, �������� ��
    {
        
        return $this->pkgMapper->setActionListeningOn($name);
    }
	public function setActionListeningOff($name)//��������� ��������, ������� ��
    {
        return $this->pkgMapper->setActionListeningOff($name);
    }
		public function setLiActionGuiName($name, $guiName)//������ ��� ��� ������������ ���������� �������� �������� ��, ����� ��� � ��� ��� ��� ��������� ��
    {
        return $this->pkgMapper->setLiActionGuiName($name, $guiName);
    }
		public function setLiActionDescription($name, $description)//������ �������� ��� �������� �������� ��� ����� ���������, �������� ��
    {
       return $this->pkgMapper->setLiActionDescription($name, $description);
    }
		public function setLiActionName($name, $newName)//����� ��� ��������	������ ��� ����� � �������� ��
    {
        return $this->pkgMapper->setLiActionName($name, $newName);
    }
	//--Param - �������� ��������
	public function addLiParam($name, $guiName, $inUse, $description)//������ �������� ���������, �������� �������� ��� ���, ������������ ��, �������� ��������, �������� ��
    {
        return $this->pkgMapper->addLiParam($name, $guiName, $inUse, $description);
    }
		public function deleteLiParam($name)//������� ��������. �������� �������� 
    {
        $this->pkgMapper->deleteLiParam($name);
        
    }
	public function getLiParamIdByName($name)//�������� �� �� ����� 
    {
        return $this->pkgMapper->getLiParamIdByName($name);
    }
	public function setParamListeningOn($name)//������� ��������, �������� ��
    {
        return $this->pkgMapper->setParamListeningOn($name);
    }
	public function setParamListeningOff($name)//��������� ��������, ������� ��
    {
        return $this->pkgMapper->setParamListeningOff($name);
    }
		public function setLiParamGuiName($name, $guiName)//������ ��� ��� ������������ ���������� ��������� �������� ��, ����� ��� � ��� ��� ��� ��������� ��
    {
        return $this->pkgMapper->setLiParamGuiName($name, $guiName);
    }
		public function setLiParamDescription($name, $description)//������ �������� ��� ��������� �������� ��� ����� ���������, �������� ��
    {
        return $this->pkgMapper->setLiParamDescription($name, $description);
    }
		public function setLiParamName($name, $newName)//����� ��� ���������	������ ��� ����� � �������� ��
    {
        return $this->pkgMapper->setLiParamName($name, $newName);
    }
	//--Action params - ������������ ����� ����������� � ����������

	public function addLiActionParam($liActionId, $liParamId){//��������� ������ � ������� ������� ��������

		return $this->pkgMapper->addLiActionParam($liActionId, $liParamId);
	}
	public function getLiActionParamIdByIds($liActionId, $liParamId){//�������� �� �� ������ �� �������

		return $this->pkgMapper->getLiActionParamIdByIds($liActionId, $liParamId);
	}
		public function removeLiActionParam($liActionId, $liParamId){//������� ������ �� �������

		$this->pkgMapper->removeLiActionParam($liActionId, $liParamId);

	}
//	--Action param values - �������� ���������� ���������������� ��������
		public function setLiValueParamString($value, $liActionParamId)
    {
        return $this->pkgMapper->setLiValueParamString($value, $liActionParamId);
    }
	
			public function setLiValueParamNumeric($value)
    {
        return $this->pkgMapper->setLiValueParamNumeric($value);
    }
			public function setLiValueParamReal($value)
    {
        return $this->pkgMapper->setLiValueParamReal($value);
    }
			public function setLiValueParamDate($value)
    {
        return $this->pkgMapper->setLiValueParamDate($value);
    }
			public function setLiValueParamDomain($value)
    {
        return $this->pkgMapper->setLiValueParamDomain($value);
    }
	public function getLiEvent($userId, $controller, $action, $dateFrom, $dateTo, $start, $limit)
	{
		return $this->pkgMapper->getLiEvent($userId, $controller, $action, $dateFrom, $dateTo, $start, $limit);
	}
		public function addLiEvent($userid, $userhost, $lidate, $controller, $action)
	{
		return $this->pkgMapper->addLiEvent($userid, $userhost, $lidate, $controller, $action);
	}
	public function addLiEventValue($LiId, $paramId, $paramValue)
	{
		return $this->pkgMapper->addLiEventValue($LiId, $paramId, $paramValue);
	}
	public function getLiControllers()
	{
		return $this->pkgMapper->getLiControllers();
	}
	public function getLiActions()
	{
		return $this->pkgMapper->getLiActions();
	}public function getLiParams()
	{
		return $this->pkgMapper->getLiParams();
	}
	public function setLiParamActive($id, $val)
	{
		return $this->pkgMapper->setLiParamActive($id, $val);
	}
	public function setLiActionActive($id, $val, $c)
	{
		return $this->pkgMapper->setLiActionActive($id, $val, $c);
	}
	public function setLiControllerActive($id, $val)
	{
		return $this->pkgMapper->setLiControllerActive($id, $val);
	}
	public function getActiveFeatureLayersList()
	{
		return $this->pkgMapper->getActiveFeatureLayersList();
	}
	public function setFLayersActive($id, $val)
	{
		return $this->pkgMapper->setFLayersActive($id,$val);
	}
	public function setMapActivity($u, $h, $m, $s, $l)
	{
		return $this->pkgMapper->setMapActivity($u, $h, $m, $s, $l);
	}
	public function removeFeatureLayer($id, $u, $h)
	{
		return $this->pkgMapper->removeFeatureLayer($id, $u, $h);
	}
	public function removeObjectType($id, $u, $h)
	{
		return $this->pkgMapper->removeObjectType($id, $u, $h);
	}
	public function getLayersForAudit()
	{
		$db_response = $this->pkgMapper->getFLayersActiveOnly();
		$Table = array();
		foreach ($db_response as $value) {
			//var_dump($value);
    
		    $TableFlName = lmb_win1251_to_utf8($value->get('lname'));
			
			
            $Table[] = $TableFlName;
        
		}
		//var_dump($Table);
		return $Table;
	}
}
?>