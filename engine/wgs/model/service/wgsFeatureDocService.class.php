﻿<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsFeatureDocServiceMapper.class.php');

class wgsFeatureDocService extends wgsBaseObjectService
{

    public function getDocDescriptionList($entityId)
    {
        $objectRegistry = $this->pkgMapper->getDocDescriptionList($entityId)->getArray();
        return $objectRegistry;
    }
    
    public function getDocDescription($docId)
    {
        $objectRegistry = $this->pkgMapper->getDocDescription($docId)->getArray();
        return $objectRegistry;
    }
    
    public function getDocument($doc_id)
    {
        $objectRegistry = $this->pkgMapper->getDocument($doc_id)->getArray();
        return $objectRegistry;
    }
    
    public function getDocCount ($entityId)
    {
       return $this->pkgMapper->getDocCount($entityId); 
    }
    
    public function setDocParam($id, $name, $description)
    {
        $this->pkgMapper->setDocParam($id, $name, $description);
    }
    
    public function addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name)
    {
        return $this->pkgMapper->addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name);
    }
    
    public function updateDoc($doc_id, $doc_type, $document, $doc_cache_name)
    {
        return $this->pkgMapper->updateDoc($doc_id, $doc_type, $document, $doc_cache_name);
    }
    
    public function removeDoc($id)
    {
        return $this->pkgMapper->removeDoc($id);
    }
    
}

?>