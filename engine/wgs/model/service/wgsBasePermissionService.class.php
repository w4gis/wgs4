<?php

lmb_require('wgs/model/service/wgsBaseService.class.php');
lmb_require('wgs/model/wgsPermission.class.php');
lmb_require('wgs/model/wgsRole.class.php');
lmb_require('limb/dbal/src/drivers/oci/lmbOciRecord.class.php');

class wgsBasePermissionService extends wgsBaseService
{
    protected $wgsRoles = array();
    
    public function getPermissions()
    {
        $permissionRecordSet = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getPermissions() : $this->pkgMapper->getPermissions()->getArray();
        $permissions = array();
        foreach ($permissionRecordSet as $permissionRecord)
        {
            if ($permission = new wgsPermission($permissionRecord))
                $permissions[] = $permission;
        }
        return $permissions;
    }
    
    public function getPermissionsByObjectId($objectId)
    {
        $permissionRecordSet = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getPermissionsByObjectId($objectId) :
			$this->pkgMapper->getPermissionsByObjectId($objectId)->getArray();
        $permissions = array();
        foreach ($permissionRecordSet as $permissionRecord)
        {
            if ($permission = new wgsPermission($permissionRecord))
            {
                $childRoles = $permissionRecord->get('wgschildroles');
                $parentRoles = $permissionRecord->get('wgsparentroles');
                if (count($childRoles))
                    foreach($childRoles as $childRole)
                    {
                        //$wgsRoleId = $childRole['WGSROLE_ID'];
                        $wgsRoleId = $childRole->get('wgsrole_id');
                        if (!isset($this->wgsRoles[$wgsRoleId]))
                            //$this->wgsRoles[$wgsRoleId] = wgsRole::factory(new lmbOciRecord($childRole));
                            $this->wgsRoles[$wgsRoleId] = wgsRole::factory($childRole);
                        $permission->getRole()->addChild($this->wgsRoles[$wgsRoleId]);
                    }
                if (count($parentRoles))
                    foreach($parentRoles as $parentRole)
                    {
                        //$wgsRoleId = $parentRole['WGSROLE_ID'];
                        $wgsRoleId = $parentRole->get('wgsrole_id');
                        if (!isset($this->wgsRoles[$wgsRoleId]))
                            //$this->wgsRoles[$wgsRoleId] = wgsRole::factory(new lmbOciRecord($parentRole));
                            $this->wgsRoles[$wgsRoleId] = wgsRole::factory($parentRole);
                        $permission->getRole()->addParent($this->wgsRoles[$wgsRoleId]);
                    }
                $permissions[] = $permission;
            }
        }
        return $permissions;
    }

    public function getPermissionsByRoleId($roleId)
    {
        $permissionRecordSet = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getPermissionsByRoleId($roleId) :
			$this->pkgMapper->getPermissionsByRoleId($roleId)->getArray();
        $permissions = array();
        foreach ($permissionRecordSet as $permissionRecord)
        {
            if ($permission = new wgsPermission($permissionRecord))
                $permissions[] = $permission;
        }
        return $permissions;
    }
    
    public function setPermissions($permissions)
    {
        foreach($permissions as $permission)
        {
            $this->pkgMapper->setPermission(
                $permission->getRole()->getId(),
                $permission->getObject()->getObjectId(),
                $permission->getPrivilege()->getId(),
                $permission->getPermission()
            );
        }
    }
}

?>
