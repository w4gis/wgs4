<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/service/wgsBaseRoleService.class.php');
lmb_require('wgs/model/mapper/wgsSysProjectServiceMapper.class.php');

lmb_require('wgs/model/wgsRole.class.php');

class wgsSysProjectService extends wgsBaseObjectService
{
    protected function fillRoleRegistry(&$roleRegistry)
    {
        $_roleRegistry = array();
        foreach ($roleRegistry as $roleRecord)
        if ($role = wgsRole :: factory($roleRecord))
        $_roleRegistry[$role->getId()] = $role;
        $roleRegistry = $_roleRegistry;
    }

    public function getProjectList()
    {
        $objectRegistry = $this->pkgMapper->getProjectList()->getArray();
        $this->fillObjectRegistry($objectRegistry);
        $this->assignParentObjects($objectRegistry);
        return $objectRegistry;
    }

    public function getProjectsWithRoles()
    {   
        $objectRegistry = $this->pkgMapper->getRealProjectList()->getArray();

        $this->fillObjectRegistry($objectRegistry);

        $this->assignParentObjects($objectRegistry);

        $roleRegistry = $this->pkgMapper->getProjectsWithRoles()->getArray();

        foreach ($objectRegistry as $sysProject)
        {       
            $sysProjectRoles = array();

            for($i = 0; $i < count($roleRegistry); $i++)
            {   
                if($roleRegistry[$i]->get('id') == $sysProject->getId())
                {
                    $sysProjectRoles[] = $roleRegistry[$i];
                }
            }

            $this->fillRoleRegistry($sysProjectRoles);
            $sysProject->setSysProjectRoles($sysProjectRoles);
        }

        return($objectRegistry);
    }

	
    public function getProjectListByModuleId($moduleId)
	{
        $objectRegistry = $this->pkgMapper->getProjectListByModuleId($moduleId)->getArray();
        $this->fillObjectRegistry($objectRegistry);
        return $objectRegistry;
	}
	
    public function createSysProjectService($wgsSysProject, $install_schemes_array, $install_logics_array)
	{
		$this->pkgMapper->createSysProjectService($wgsSysProject->getName(),$wgsSysProject->getDomain(),$wgsSysProject->getDescription(),
		$wgsSysProject->getDatabase(),$wgsSysProject->getTitle(), $wgsSysProject->getLocked(), $install_schemes_array, $install_logics_array);
	}
	
    public function editSysProjectService($wgsSysProject)
	{
		$this->pkgMapper->editSysProjectService($wgsSysProject->getId(),$wgsSysProject->getName(),$wgsSysProject->getDomain(),$wgsSysProject->getDescription(),
		$wgsSysProject->getDatabase(),$wgsSysProject->getTitle(), $wgsSysProject->getLocked());
	}
	
	public function removeSysProjectService($wgsSysProject)
	{
		$this->pkgMapper->removeSysProjectService($wgsSysProject->getId());
	}
	
}

?>