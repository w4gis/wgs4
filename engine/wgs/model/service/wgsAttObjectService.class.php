<?php

lmb_require('wgs/model/service/wgsProjectObjectService.class.php');

class wgsAttObjectService extends wgsProjectObjectService
{
    public function getObjectsByFeatureName ($layerId, $name) {
        return $this->pkgMapper->getObjectsByFeatureName ($layerId, $name);
    }

    public function unbindStudents () {
        return $this->pkgMapper->unbindStudents ();
    }

    public function updateProps () {
        return $this->pkgMapper->updateProps ();
    }


    public function getObjectsByRelationIdAndName ($relationId, $name) {
        return $this->pkgMapper->getObjectsByRelationIdAndName ($relationId, $name);
    }

    public function getListObjectByFeaturesAndType ($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter, $arrayed) {
        return $this->pkgMapper->getListObjectByFeaturesAndType ($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter,$arrayed);
    }

    public function getListObjectByFeaturesAndTypeCount ($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter,$arrayed) {
        return $this->pkgMapper->getListObjectByFeaturesAndTypeCount ($layerIds, $typeIds, $rangeStart, $rangeLimit, $sortby, $filter,$arrayed);
    }

    public function getValues($objectIds, $propIds)
    {
        return $this->pkgMapper->getValues($objectIds, $propIds);
    }

	public function GetListObjectByTypes($typesId, $start, $limit, &$totalCount)
    {
    	return $this->pkgMapper->GetListObjectByTypes($typesId, $start, $limit, $totalCount);
    }

    public function GetValuePropertiesByObject($objectId)
    {
        return $this->pkgMapper->GetValuePropertiesByObject($objectId);
    }
    public function GetValuePropertiesByObjectNew($objectId,$year)
    {
        return $this->pkgMapper->GetValuePropertiesByObjectYear($objectId,$year);
    }
   public function GetCoordinatesByObjectId($objectId)
    {
        return $this->pkgMapper->GetCoordinatesByObjectId($objectId);
    }
public function GetListYears()
    {
        return $this->pkgMapper->getListYears();
    }
    public function getValuePropertiesByObjects($objectIds)
    {
        return $this->pkgMapper->getValuePropertiesByObjects($objectIds);
    }

    public function getObjectTypesByObjects($objectIds, $selectMode)
    {
        return $this->pkgMapper->getObjectTypesByObjects($objectIds, $selectMode);
    }

	public function getObjectTypeNameByObject($objectIds)
    {
        return $this->pkgMapper->getObjectTypeNameByObject($objectIds);
    }
public function getName($objectId)
    {
        return $this->pkgMapper->getName($objectId);
    }
    public function getObjectTypesByFeatures($featuresIds)
    {
        return $this->pkgMapper->getObjectTypesByFeatures($featuresIds);
    }

	public function Set($objectId,$objectName,$objectShortName,$objectNote,$ObjectEditUser)
	{
		return $this->pkgMapper->Set($objectId,$objectName,$objectShortName,$objectNote,$ObjectEditUser);
	}

    public function SetName($objectId,$name)
    {
        $this->pkgMapper->SetName($objectId,$name);
    }

    public function SetShortName($objectId,$shortName)
    {
        $this->pkgMapper->SetShortName($objectId,$shortName);
    }

    public function SetNote($objectId,$note)
    {
        $this->pkgMapper->SetNote($objectId,$note);
    }

    public function SetUser($objectId,$user)
    {
        $this->pkgMapper->SetUser($objectId,$user);
    }

	public function SetValueProperty($objectId,$propertyId,$propertyValue)
	{
		return $this->pkgMapper->SetValueProperty($objectId,$propertyId,$propertyValue);
	}

	public function Add($typeId)
	{
		return $this->pkgMapper->Add($typeId);
	}
	public function Remove($ObjectId,$removeMode)
	{
		return $this->pkgMapper->Remove($ObjectId,$removeMode);
	}

	public function GetValueList($propertyId, $objectTypeIds)
	{
	    return $this->pkgMapper->GetValueList($propertyId, $objectTypeIds);
	}

    public function GetDomainValueList($propertyId)
    {
        return $this->pkgMapper->GetDomainValueList($propertyId);
    }

    public function getListObjectByFeature($featureId)
    {
        return $this->pkgMapper->getListObjectByFeature($featureId);
    }

    public function getChildObjectsByParent($objectId)
    {
        return $this->pkgMapper->getChildObjectsByParent($objectId);
    }

    public function getParentObjectsByChild($objectId)
    {
        return $this->pkgMapper->getParentObjectsByChild($objectId);
    }

    public function getListObjectByFeatures($featureIds)
    {
        return $this->pkgMapper->getListObjectByFeatures($featureIds);
    }

    public function getAllObjectByFeatures($featureIds)
    {
        return $this->pkgMapper->getAllObjectByFeatures($featureIds);
    }

    public function getValueBasePropertiesByObject($objectId)
    {
        return $this->pkgMapper->getValueBasePropertiesByObject($objectId);
    }

    public function addRelationByFeature($objectId, $featureId, $isCurrent)
    {
        return $this->pkgMapper->addRelationByFeature($objectId, $featureId, $isCurrent);
    }

    public function removeRelationByFeature($ADBSDBRelationId, $mode)
    {
        return $this->pkgMapper->removeRelationByFeature($ADBSDBRelationId, $mode);
    }

    public function getListObjectByTypeUnbindDate($featureTypeId, $unbindedDate, $start_r, $limit_r, &$totalCount)
    {
        return $this->pkgMapper->getListObjectByTypeUnbindDate($featureTypeId, $unbindedDate, $start_r, $limit_r, $totalCount);
    }

    public function removeObjectsByTypeUnbindDate($featureTypeId, $unbindedDate) {
        return $this->pkgMapper->removeObjectsByTypeUnbindDate($featureTypeId, $unbindedDate);
    }

    public function getListAllUnbindedObject($start_r, $limit_r, &$totalCount)
    {
        return $this->pkgMapper->getListAllUnbindedObject($start_r, $limit_r, $totalCount);
    }

    public function getListAllOrphanObject($start_r, $limit_r, &$totalCount)
    {
        return $this->pkgMapper->getListAllOrphanObject($start_r, $limit_r, $totalCount);
    }

    public function getFeaturesByOrphanObjects($objects)
    {
        return $this->pkgMapper->getFeaturesByOrphanObjects($objects);
    }

    public function getListObjectByPolygon($coordsOfPolygon, $featureLayersIds)
    {
        return $this->pkgMapper->getListObjectByPolygon($coordsOfPolygon, $featureLayersIds);
    }

	public function getEntityIdByObject($objectId)
    {
        return $this->pkgMapper->getEntityIdByObject($objectId);
    }

	public function getEntityIdByFeature($featureId)
    {
        return $this->pkgMapper->getEntityIdByFeature($featureId);
    }
}
?>
