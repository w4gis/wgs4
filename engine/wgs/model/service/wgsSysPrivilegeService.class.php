<?php
lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsSysPrivilegeServiceMapper.class.php');

class wgsSysPrivilegeService extends wgsBaseObjectService
{ 
    public function getSysPrivilegeList()
    {
        $objectRegistry = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getSysPrivilegeList() : $this->pkgMapper->getSysPrivilegeList()->getArray();
        $this->fillObjectRegistry($objectRegistry);
        $this->assignParentObjects($objectRegistry);
        return $objectRegistry;
    }	
    
    public function createSysPrivilegeService($wgsSysPrivilege)
	{
		$this->pkgMapper->createSysPrivilegeService($wgsSysPrivilege->getName(), $wgsSysPrivilege->getDescription(), $wgsSysPrivilege->getLocked());
	}
	
    public function editSysPrivilegeService($wgsSysPrivilege)
	{
		$this->pkgMapper->editSysPrivilegeService($wgsSysPrivilege->getId(), $wgsSysPrivilege->getName(), $wgsSysPrivilege->getDescription(), $wgsSysPrivilege->getLocked());
	}
	
	public function removeSysPrivilegeService($wgsSysPrivilege)
	{
		$this->pkgMapper->removeSysPrivilegeService($wgsSysPrivilege->getId());
	}
}
?>
