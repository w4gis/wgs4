<?php

lmb_require('wgs/model/service/wgsBaseUserService.class.php');

class wgsSysUserService extends wgsBaseUserService
{
    public function revokeSysRoleMemberships($wgsSysUser, $wgsSysRole)
    {
        $this->pkgMapper->revokeSysRoleMemberships($wgsSysUser->getId(), $wgsSysRole->getId());
    }

    public function grantSysRoleMemberships($wgsSysUser, $wgsSysRole)
    {
        $this->pkgMapper->grantSysRoleMemberships($wgsSysUser->getId(), $wgsSysRole->getId());
    }
}

?>