<?php

lmb_require('wgs/model/service/wgsBaseService.class.php');

class wgsSearchService extends wgsBaseService
{
    public function search($featureTypes, $featureLayers, $properties, $condition, $objectSearch, $start, $limit, $searchInUnbinded, $docSearch, $filter, &$totalCount)
    {
        return $this->pkgMapper->search($featureTypes, $featureLayers, $properties, $condition, $objectSearch, $start, $limit, $searchInUnbinded, $docSearch, $filter ? $filter : "", $totalCount);
    }
    
    public function getValuesOfBaseProperty($propertyName, $featureLayers, $featureTypes)
    {
        return $this->pkgMapper->getValuesOfBaseProperty($propertyName, $featureLayers, $featureTypes);
    }

    public function getReportStatSum($featureIds, $featureTypes, $property, $addProperties)
    {
        return $this->pkgMapper->getReportStatSum($featureIds, $featureTypes, $property, $addProperties);
    }
}

?>