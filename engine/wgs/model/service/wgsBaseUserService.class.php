<?php

lmb_require('wgs/model/service/wgsBaseService.class.php');
lmb_require('wgs/model/wgsUser.class.php');

class wgsBaseUserService extends wgsBaseService
{
    public function addUser($wgsUser)
    {
        return $this->pkgMapper->addUser(   $wgsUser->getName(),
                                            $wgsUser->getPassword(),
                                            $wgsUser->getLocked(),
                                            $wgsUser->getDescription());
    }

    public function setUser($wgsUser)
    {
        $this->pkgMapper->setUser(  $wgsUser->getId(),
                                    $wgsUser->getName(),
                                    $wgsUser->getPassword(),
                                    $wgsUser->getLocked(),
                                    $wgsUser->getDescription());
    }

    public function removeUser($wgsUser)
    {
        $this->pkgMapper->removeUser($wgsUser->getId());
    }

    public function getUserByName($name)
    {
        $user = null;
        
        if ($userRecord = $this->pkgMapper->getUserByName($name)) {
			
            $user = wgsUser :: factory($userRecord);
        }
        
        //var_dump( $userRecord );
        return $user;
    }

    public function getList()
    {
        $users = array();
        $userRecords = WGS4_NO_DRIVERS_MODE ? $this->pkgMapper->getList() :
			$this->pkgMapper->getList()->getArray();
        foreach ($userRecords as $userRecord) {
            $users[] = wgsUser :: factory($userRecord);
        }
        return $users;
    }

    public function checkPassword($pasHash, $ip)
    {
		$name = $this->pkgMapper->getPassword($pasHash, $ip);
        if(!$name)
			return 0;
		return $name;
    }

	public function checkPassword2($name, $password, $ip)
    {
        return $this->pkgMapper->checkPassword2($name, $password, $ip);
    }
	
    public function getUserMembersByRoleId($roleId)
    {
        $users = array();
        $userRecords = WGS4_NO_DRIVERS_MODE ?  $this->pkgMapper->getUserMembersByRoleId($roleId) :
			$this->pkgMapper->getUserMembersByRoleId($roleId)->getArray();
        foreach ($userRecords as $userRecord) {
            $users[] = wgsUser :: factory($userRecord);
        }
        return $users;
    }

}

?>
