<?php

lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/mapper/wgsThemeServiceMapper.class.php');

class wgsThemeService extends wgsBaseService
{
	public function GetList($wgsUserId, $themeId = false)
	{
		if (!$themeId) {
			return $this->pkgMapper->GetList($wgsUserId);
		} else {
			return $this->pkgMapper->GetTheme($wgsUserId, $themeId);
		}

	}

    public function add($name, $map, $shared, $user_id, $view_options, $position, $is_archive)
    {
        return $this->pkgMapper->add($name, $map, $shared, $user_id, $view_options, $position, $is_archive);
    }

    public function updateViewOptions($wgsuser_id, $theme_id, $view_options){
    	$this->pkgMapper->updateViewOptions($wgsuser_id, $theme_id, $view_options);
    }

	public function edit($wgsuser_id, $theme_id, $name, $shared, $position, $is_archive, $map){
    	$this->pkgMapper->edit($wgsuser_id, $theme_id, $name, $shared, $position, $is_archive, $map);
    }

	public function remove($wgsuser_id, $theme_id){
	    return $this->pkgMapper->remove($wgsuser_id, $theme_id);
    }

	public function getThemeLayerList($themeId)
	{
		return $this->pkgMapper->getThemeLayerList($themeId);
	}

	public function addThemeLayer($themeId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position)
    {
        return $this->pkgMapper->addThemeLayer($themeId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position);
    }

	public function editThemeLayer($themeLayerId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position)
    {
        $this->pkgMapper->editThemeLayer($themeLayerId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position);
    }

	public function removeThemeLayer($themeLayerId)
	{
		$this->pkgMapper->removeThemeLayer($themeLayerId);
	}
    /*public function getDocDescriptionList($entityId)
    {
        $objectRegistry = $this->pkgMapper->getDocDescriptionList($entityId)->getArray();
        return $objectRegistry;
    }

    public function getDocument($doc_id)
    {
        $objectRegistry = $this->pkgMapper->getDocument($doc_id)->getArray();
        return $objectRegistry;
    }

    public function getDocCount ($entityId)
    {
       return $this->pkgMapper->getDocCount($entityId);
    }

    public function setDocParam($id, $name, $description)
    {
        $this->pkgMapper->setDocParam($id, $name, $description);
    }

    public function addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name)
    {
        return $this->pkgMapper->addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name);
    }

    public function updateDoc($doc_id, $doc_type, $document, $doc_cache_name)
    {
        return $this->pkgMapper->updateDoc($doc_id, $doc_type, $document, $doc_cache_name);
    }

    public function removeDoc($id)
    {
        return $this->pkgMapper->removeDoc($id);
    }*/

}

?>