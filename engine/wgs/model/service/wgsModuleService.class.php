<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/service/wgsBaseObjectService.class.php');
lmb_require('wgs/model/service/wgsFeatureDocService.class.php');
lmb_require('wgs/model/mapper/wgsFeatureDocServiceMapper.class.php');
lmb_require('wgs/model/service/wgsThemesService.class.php');
lmb_require('wgs/model/mapper/wgsThemesServiceMapper.class.php');
lmb_require('wgs/model/service/wgsFavoriteViewService.class.php');
lmb_require('wgs/model/mapper/wgsFavoriteViewServiceMapper.class.php');

class wgsModuleService extends wgsBaseObjectService
{
    protected $featureDocService;
    protected $themesService;
    protected $favoriteViewService;
    
    public function getFeatureDocService()
	{
	    if (is_null($this->featureDocService))
            $this->featureDocService = new wgsFeatureDocService(new wgsFeatureDocServiceMapper, $this->getDbConnection());
        return $this->featureDocService;
	}
	
    public function getThemesService()
    {
        if (is_null($this->themesService))
            $this->themesService = new wgsThemesService(new wgsThemesServiceMapper, $this->getDbConnection());
        return $this->themesService;
    }
	
    public function getFavoriteViewService()
    {
        if (is_null($this->favoriteViewService))
            $this->favoriteViewService = new wgsFavoriteViewService(new wgsFavoriteViewServiceMapper, $this->getDbConnection());
        return $this->favoriteViewService;
    }
    
}

?>