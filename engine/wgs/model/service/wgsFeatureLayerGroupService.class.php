<?php
lmb_require('wgs/model/service/wgsProjectObjectService.class.php');
/**
 * ����� ������������ ��� ���������� �������� ������� ������ � ��������� ���� FeatureLayerGroup.
 * 
 */
class wgsFeatureLayerGroupService extends wgsProjectObjectService
{
	public function Add($layerGroupName)
	{
		return $this->pkgMapper->Add($layerGroupName);
	}
	public function Remove($layerGroupId)
	{
		return $this->pkgMapper->Remove($layerGroupId);
	}
	public function Set($layerGroupId,$layerGroupName)
	{
		return $this->pkgMapper->Set($layerGroupId,$layerGroupName);
	}
	public function Get($featureLayerGroupId)
	{
		return $this->pkgMapper->Get($featureLayerGroupId);
	}
}

?>