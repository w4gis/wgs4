<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsFeatureTypeFork extends wgsProjectObject implements wgsZendAclResourceInterface
{    
    private $hash;
    private $featureLayerId;

    private function setHash($hash)
    {
        $this->hash = $hash;
    }
    
    public function getHash()
    {
        return $this->hash;
    }

    private function setFeatureLayerId($featureLayerId)
    {
        $this->featureLayerId = $featureLayerId;
    }
    
    public function getFeatureLayerId()
    {
        return $this->featureLayerId;
    }
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setHash($objectRecord->get('featuretypefork_hash'));
        $this->setId($objectRecord->get('featuretypefork_id'));
        $this->setFeatureLayerId($objectRecord->get('featuretypefork_layer_id'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
}

?>