<?php

lmb_require('wgs/model/mapper/wgsBaseRoleServiceMapper.class.php');

class wgsSysRoleServiceMapper extends wgsBaseRoleServiceMapper
{
    protected $package = 'wgs3logic.SysRoleService';

    public function addRole($sysRoleName, $sysRoleDescription)
    {
        $this->setInVarchar($sysRoleName);
        $this->setInVarchar($sysRoleDescription);
        $this->setOutInteger($sysRoleId);
        $this->execute('AddRole');
        return $sysRoleId;
    }

    public function setRole($sysRoleId,$sysRoleName, $sysRoleDescription)
    {
        $this->setInInteger($sysRoleId);
        $this->setInVarchar($sysRoleName);
        $this->setInVarchar($sysRoleDescription);
        $this->execute('SetRole');
    }

    public function removeRole($sysRoleId)
    {
        $this->setInInteger($sysRoleId);
        $this->execute('RemoveRole');
    }

    public function getChildrenRolesById($sysRoleId)
    {
        $this->setInInteger($sysRoleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetChildrenRolesById');
        return $recordset;
    }

    public function getHighestSysRoles()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetHighestSysRoles');
        return $recordset;
    }

    public function getAcceptableChildrenRolesById($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetAcceptableChildrenRolesById');
        return $recordset;
    }

    public function revokeSysRoleMemberships($wgsRoleId)
    {
        $this->setInInteger($wgsRoleId);
        $this->execute('RevokeSysRoleMemberships');
    }

    public function grantSysRoleMemberships($wgsRoleId, $wgsParentRoleId)
    {   
        $this->setInInteger($wgsRoleId);
        $this->setInInteger($wgsParentRoleId);
        $this->execute('GrantSysRoleMemberships');
    }

    public function revokeSysRoleWgsRole($sysProjectId,$wgsSysRoleId, $wgsProjectRoleId)
    {
        $this->setInInteger($sysProjectId);
        $this->setInInteger($wgsSysRoleId);
        $this->setInInteger($wgsProjectRoleId);
        $this->execute('RevokeSysRoleWgsRole');
    }

    public function grantSysRoleWgsRole($sysProjectId,$wgsSysRoleId, $wgsProjectRoleId)
    {
        $this->setInInteger($sysProjectId);
        $this->setInInteger($wgsSysRoleId);
        $this->setInInteger($wgsProjectRoleId);
        $this->execute('GrantSysRoleWgsRole');
    }

    public function getSysRoleMemberships($sysProjectId,$roleId)
    {   
        $this->setInInteger($sysProjectId);
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetSysRoleMemberships');
        return $recordset;
        
    }
}

?>
