<?php



lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsThemeServiceMapper extends wgsPackageMapper
{
    protected $package = 'wgs3logic.ThemeService';

	public function add($name, $map, $shared, $user_id, $view_options, $position, $is_archive)
	{
      $this->setInVarchar($name);
	    $this->setInVarchar($map);
	    $this->setInInteger($shared);
		$this->setInInteger($user_id);
		$this->setInVarchar($view_options);
	    //added
		$this->setInInteger($position);
		$this->setInInteger($is_archive);
		
		$this->setOutInteger($record);
	    $this->execute('Add');
	    return $record;
	}
	
	public function updateViewOptions($wgsuser_id, $theme_id, $view_options)
	{
		$this->setInInteger($wgsuser_id);
		$this->setInInteger($theme_id);
		$this->setInVarchar($view_options);
		$this->execute('UpdateViewOptions');
	}

	public function edit($wgsuser_id, $theme_id, $name, $shared, $position, $is_archive, $map)
	{
	//require_once('FirePHPCore/FirePHP.class.php');
	//$firephp = FirePHP::getInstance(true);
	//$firephp ->log("Editing");
	//$firephp ->log($is_archive);
	//$firephp ->log($position);
		$this->setInInteger($wgsuser_id);
		$this->setInInteger($theme_id);
		$this->setInVarchar($name);
		$this->setInInteger($shared);
		$this->setInInteger($position);
		$this->setInInteger($is_archive);
    $this->setInVarchar($map);
		
		$this->execute('Edit');
	}

	public function remove($wgsuser_id, $theme_id)
	{
		$this->setInInteger($wgsuser_id);
		$this->setInInteger($theme_id);
		$this->execute('Remove');
	}

	public function getList($user_id)
	{
		$this->setInInteger($user_id);
		$this->setOutRecordSet($recordset);
		$this->execute('GetList');
		return $recordset;
	}

	public function getTheme($user_id, $theme_id)
	{
		$this->setInInteger($user_id);
		$this->setInInteger($theme_id);
		$this->setOutRecordSet($recordset);
		$this->execute('GetTheme');
		return $recordset;
	}

	public function getThemeLayerList($themeId)
	{
		$this->setInInteger($themeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetThemeLayerList');
		return $recordset;
	}

	public function addThemeLayer($themeId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position)
	{
		$this->setInInteger($themeId);
		$this->setInVarchar($legend);
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setInInteger($featureGeomertyId);
		$this->setInVarchar($condition);
		$this->setInVarchar($style);
		$this->setInInteger($position);
		$this->setOutInteger($record);
		$this->execute('AddThemeLayer');
		return $record;
	}

	public function editThemeLayer($themeLayerId, $legend, $featureLayerId, $featureTypeId, $featureGeomertyId, $condition, $style, $position)
	{
		$this->setInInteger($themeLayerId);
		$this->setInVarchar($legend);
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setInInteger($featureGeomertyId);
		$this->setInVarchar($condition);
		$this->setInVarchar($style);
		$this->setInInteger($position);
		$this->execute('EditThemeLayer');
	}

	public function removeThemeLayer($themeLayerId)
	{
		$this->setInInteger($themeLayerId);
		$this->execute('RemoveThemeLayer');
	}


    /*public function getDocDescriptionList($entityId)
    {
        $this->setInInteger($entityId);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetDocDescriptionList');
        return $recordSet;
    }

    public function getDocument($doc_id)
    {
        $this->setInInteger($doc_id);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetDocument');
        return $recordSet;
    }

    public function getDocCount ($entityId)
    {
        $this->setInInteger($entityId);
        $this->setOutInteger($record);
        $this->execute('GetDocCount');
        return $record;
    }

	public function setDocParam($id, $name, $description)
	{
        $this->setInInteger($id);
        $this->setInVarchar($name);
        $this->setInVarchar($description);
        $this->execute('SetDocParam');
	}

	public function addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name)
	{
	    $this->setInInteger($entityId);
	    $this->setInVarchar($doc_name);
	    $this->setInVarchar($doc_type);
	    $this->setInVarchar($description);
	    $this->setInVarchar($doc_cache_name);
	    $this->setInBlob($document);
	    $this->setOutInteger($record);
	    $this->execute('Add');
	    return $record;
	}

	public function updateDoc($doc_id, $doc_type, $document, $doc_cache_name)
	{
	    $this->setInInteger($doc_id);
	    $this->setInVarchar($doc_type);
	    $this->setInVarchar($doc_cache_name);
	    $this->setInBlob($document);
	    $this->setOutVarchar($record);
	    $this->execute('UpdateDoc');
	    return $record;
	}

	public function removeDoc($id)
	{
        $this->setInInteger($id);
        $this->setOutVarchar($record);
        $this->execute('Remove');
        return $record;
	}*/

}
?>