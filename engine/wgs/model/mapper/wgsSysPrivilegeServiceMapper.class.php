<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsSysPrivilegeServiceMapper extends wgsSysObjectServiceMapper
{
    protected $package = 'wgs3logic.SysPrivilegeService';
    
    public function getSysPrivilegeList()
    {
        $this->setOutRecordSet($recordSet);
        $this->execute('GetSysPrivilegeList');
        return $recordSet;
    }
    
    public function createSysPrivilegeService($wgsSysPrivilegeName, $wgsSysPrivilegeDescription, $wgsSysPrivilegeLocked)
	{
		$this->setInVarchar($wgsSysPrivilegeName);
		$this->setInVarchar($wgsSysPrivilegeDescription);
		$this->setInInteger($wgsSysPrivilegeLocked);
		$this->setOutInteger($record);
		$this->execute('Add');
	}
	
    public function editSysPrivilegeService($wgsSysPrivilegeId, $wgsSysPrivilegeName, $wgsSysPrivilegeDescription, $wgsSysPrivilegeLocked)
	{
		$this->setInInteger($wgsSysPrivilegeId);
		$this->setInVarchar($wgsSysPrivilegeName);
		$this->setInVarchar($wgsSysPrivilegeDescription);
		$this->setInInteger($wgsSysPrivilegeLocked);
		$this->execute('Set');
	}
	
	public function removeSysPrivilegeService($wgsSysPrivilegeId)
	{
        $this->setInInteger($wgsSysPrivilegeId);
        $this->execute('Remove');    
	}
    
}

?>