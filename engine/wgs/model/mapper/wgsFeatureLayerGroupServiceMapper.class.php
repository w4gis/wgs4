<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 
 */
class wgsFeatureLayerGroupServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.featurelayergroup';

	public function Add($layerGroupName)
	{
		$this->setInVarchar($layerGroupName);
		$this->setOutInteger($layerGroupId);
		$this->execute('Add');
		return $layerGroupId;
	}
	public function Remove($layerGroupId)
	{
		$this->setInInteger($layerGroupId);
		$this->execute('Remove');
	}
	public function Set($layerGroupId,$layerGroupName)
	{
		$this->setInInteger($layerGroupId);
		$this->setInVarchar($layerGroupName);
		$this->execute('Set');
	}
	public function Get($layerGroupId)
	{
		$this->setInInteger($layerGroupId);
		$this->setOutVarchar($layerGroupName);
		$this->execute('Get');
		return $layerGroupName;
	}
}
?>