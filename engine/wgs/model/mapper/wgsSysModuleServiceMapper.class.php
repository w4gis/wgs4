<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsSysModuleServiceMapper extends wgsSysObjectServiceMapper
{
    protected $package = 'wgs3logic.SysModuleService';
	
	public function getModuleList()
	{
		$this->setOutRecordSet($recordSet);
		$this->execute('GetModuleList');
		return $recordSet;
	}
	
    public function getProjectModuleList($projectName)
    {
        $this->setInVarchar($projectName);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetProjectModuleList');
        return $recordSet;
    }
    
    public function getProjectModuleByUrl($urlString, $projectName)
    {
        $this->setInVarchar($urlString);
        $this->setInVarchar($projectName);
        $this->setOutRecord($record);
        $this->execute('GetProjectModuleByUrl');
        return $record;
    }

    public function getModuleByUrl($urlString)
    {
        $this->setInVarchar($urlString);
        $this->setOutRecord($record);
        $this->execute('GetModuleByUrl');
        return $record;
    }
    
	public function getParentModuleList()
	{
		$this->setOutRecordSet($recordSet);
		$this->execute('GetParentModuleList');
		return $recordSet;
	}
	
	public function getModuleListByProjectId($projectId)
	{
		$this->setInInteger($projectId);
		$this->setOutRecordSet($recordSet);
		$this->execute('GetModuleListByProjectId');
		return $recordSet;
	}
	
	public function getChildModuleListByProjectId($sysModuleId, $sysProjectId)
	{
		$this->setInInteger($sysModuleId);
		$this->setInInteger($sysProjectId);
		$this->setOutRecordSet($recordSet);
		$this->execute('GetChildModuleListByProjectId');
		return $recordSet;
	}
	
	public function getChildModuleListByModuleId($sysModuleId)
	{
		$this->setInInteger($sysModuleId);
		$this->setOutRecordSet($recordSet);
		$this->execute('GetChildModuleListByModuleId');
		return $recordSet;
	}
	
	public function getAssignModuleList()
	{
		$this->setOutRecordSet($recordSet);
		$this->execute('GetAssignModuleList');
		return $recordSet;
	}

	public function grantSysModuleMemberships($wgsSysModuleId,$wgsSysProjectId)
	{
		$this->setInInteger($wgsSysModuleId);
		$this->setInInteger($wgsSysProjectId);
		$this->execute('GrantSysModuleMemberships');
	}
	
	public function revokeSysModuleMemberships($wgsSysModuleId,$wgsSysProjectId)
	{
		$this->setInInteger($wgsSysModuleId);
		$this->setInInteger($wgsSysProjectId);
		$this->execute('RevokeSysModuleMemberships');
	}
	     
    public function createSysModuleService($wgsSysModuleName,$wgsSysModuleLabel, $wgsSysModuleUrlString, $wgsSysModuleDescription, 
                                           $wgsSysModuleContainer, $wgsSysModulePriority, $wgsSysModuleParentId, $wgsSysModuleInMenu, $wgsSysModuleLocked)
	{
		$this->setInVarchar($wgsSysModuleName);
		$this->setInVarchar($wgsSysModuleLabel);
		$this->setInVarchar($wgsSysModuleUrlString);
		$this->setInVarchar($wgsSysModuleDescription);
		$this->setInInteger($wgsSysModuleContainer);
		$this->setInInteger($wgsSysModulePriority);
		$this->setInInteger($wgsSysModuleParentId);
		$this->setInInteger($wgsSysModuleInMenu);
		$this->setInInteger($wgsSysModuleLocked);
		$this->setOutInteger($record);
		$this->execute('Add');
	}
	
    public function editSysModuleService($wgsSysModuleId, $wgsSysModuleName,$wgsSysModuleLabel, $wgsSysModuleUrlString, $wgsSysModuleDescription, 
                                           $wgsSysModuleContainer, $wgsSysModulePriority, $wgsSysModuleParentId, $wgsSysModuleInMenu, $wgsSysModuleLocked)
	{
		$this->setInVarchar($wgsSysModuleId);
		$this->setInVarchar($wgsSysModuleName);
		$this->setInVarchar($wgsSysModuleLabel);
		$this->setInVarchar($wgsSysModuleUrlString);
		$this->setInVarchar($wgsSysModuleDescription);
		$this->setInInteger($wgsSysModuleContainer);
		$this->setInInteger($wgsSysModulePriority);
		$this->setInInteger($wgsSysModuleParentId);
		$this->setInInteger($wgsSysModuleInMenu);
		$this->setInInteger($wgsSysModuleLocked);
		$this->execute('Set');
	}
	
	public function removeSysModuleService($wgsSysModuleId)
	{
        $this->setInInteger($wgsSysModuleId);
        $this->execute('Remove');    
	}
}

?>
