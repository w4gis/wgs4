<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 * ����� ������������ ��� ������ �������� �������� ��������� (��������, ��������� ���������,�������� � �.�.)
 * �� ��������, ���������� � ��. �������� � �������������� ����� � ��������� � ������.
 * ��� ������ ����������� � ������ MgResourceService.
 */
class wgsMgResourceServiceMapper extends wgsProjectObjectServiceMapper
{
    protected $package = 'wgs3logic.MgResourceService';

    public function removeFolder($resourceId)
    {
        $this->setInVarchar($resourceId);
        $this->execute('RemoveFolder');
    }

    public function copyFolder($source, $destination)
    {
        $this->setInVarchar($source);
        $this->setInVarchar($destination);
        $this->execute('CopyFolder');
    }

    public function renameFolder($source, $destination)
    {
        $this->setInVarchar($source);
        $this->setInVarchar($destination);
        $this->execute('RenameFolder');
    }
}
?>