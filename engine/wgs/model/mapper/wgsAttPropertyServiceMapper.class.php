<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 
 */
class wgsAttPropertyServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.property';

	public function getListPropertyOfPropertyGroup($PropertyGroupId)
	{
		$this->setInInteger($PropertyGroupId);
		$this->setOutRecordSet($recordset);
		$this->execute('getListPropertyOfPropertyGroup');
		return $recordset;
	}
	public function Add($propName,$autoCADname, $valueType, $propGroupId, $placDate)
	{
		$this->setOutInteger($propId);
		$this->setInVarchar($propName);
		$this->setInVarchar($autoCADname);
		$this->setInInteger($valueType);
		$this->setInInteger($propGroupId);
		$this->setInVarchar($placDate);
		$this->execute('Add');
		return $propId;
	}
	public function RemoveProperty($propId,$removeMode)
	{
		$this->setInInteger($propId);
		$this->setInInteger($removeMode);
		$this->execute('RemoveProperty');
	}
	public function Set($propId, $propName, $autoCADname, $valueType, $propGroupId, $placDate)
	{
		$this->setInInteger($propId);	
		$this->setInVarchar($propName);
		$this->setInVarchar($autoCADname);
		$this->setInInteger($valueType);
		$this->setInInteger($propGroupId);
		$this->setInVarchar($placDate);
		$this->execute('Set');
	}	
	
	
	
	public function getListPropertyGroup()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('getListPropertyGroup');
		return $recordset;
	}
	public function AddGroupProperty($propGroupName)
	{
		$this->setInVarchar($propGroupName);
		$this->setOutInteger($propGroupId);
		$this->execute('AddGroupProperty');
		return $propGroupId;
	}
	public function SetGroupProperty($propGroupId,$propGroupName)
	{
		$this->setInInteger($propGroupId);
		$this->setInVarchar($propGroupName);
		$this->execute('SetGroupProperty');
	}
	public function RemoveGroupProperty($propGroupId,$removeMode)
	{
		$this->setInInteger($propGroupId);
		$this->setInInteger($removeMode);
		$this->execute('RemoveGroupProperty');
	}
	public function GetGroupProperty($propGroupId)
	{
		$this->setInInteger($propGroupId);
		$this->setOutVarchar($propGroupName);
		$this->execute('GetGroupProperty');
		return $propGroupName;
	}
	
	
	public function GetValueType($valueTypeId)
	{
		$this->setInInteger($valueTypeId);
		$this->setOutVarchar($valueTypeName);
		$this->execute('GetValueType');
		return $valueTypeName;
	}
	public function GetListValueType()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('GetListValueType');
		return $recordset;
	}
	
	public function GetListValueDomain($propId)
	{
		$this->setInInteger($propId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListValueDomain');
		return $recordset;
	}
	public function AddValueDomain($propId,$valueDomain)
	{
		$this->setInInteger($propId);
		$this->setInVarchar($valueDomain);
		$this->setOutInteger($valueDomainId);
		$this->execute('AddValueDomain');
		return $valueDomainId;
	}
	public function SetValueDomain($valueDomainId,$valueDomain)
	{
		$this->setInInteger($valueDomainId);
		$this->setInVarchar($valueDomain);
		$this->execute('SetValueDomain');
	}
	public function RemoveValueDomain($valueDomainId)
	{
		$this->setInInteger($valueDomainId);
		$this->execute('RemoveValueDomain');
	}
}
?>