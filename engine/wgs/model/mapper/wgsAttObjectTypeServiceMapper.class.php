<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 
 */
class wgsAttObjectTypeServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.objecttype';
	
//  procedure Add(NameFeatureType out varchar2, ShortNameFeatureType out varchar2, NoteFeatureType out varchar2, featureTypeId out number);
	public function Add($name,$shortname,$note,$adddate,$objectLabel)
	{
		$this->setInVarchar($name);
		$this->setInVarchar($shortname);
		$this->setInVarchar($note);
		$this->setInVarchar($adddate);
		$this->setInInteger($objectLabel);
		$this->setOutInteger($featureTypeId);
		$this->execute('Add');
		return $featureTypeId;
	}
//	procedure Set(featureTypeId in number, NameFeatureType in varchar2, ShortNameFeatureType in varchar2, NoteFeatureType in varchar2, AddDate in date) is	
	public function Set($featureTypeId,$name,$shortname,$note,$adddate,$objectLabel)
	{
		$this->setInInteger($featureTypeId);
		$this->setInVarchar($name);
		$this->setInVarchar($shortname);
		$this->setInVarchar($note);
		$this->setInVarchar($adddate);
		$this->setInInteger($objectLabel);
		$this->execute('Set');
	}
//	procedure Get(featureTypeId in number, NameFeatureType out varchar2, ShortNameFeatureType out varchar2, NoteFeatureType out varchar2, AddDate out date, Label out number) is
	
public function Get($featureTypeId,&$name,&$shortname,&$note,&$adddate,&$objectLabel)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutVarchar($name);
		$this->setOutVarchar($shortname);
		$this->setOutVarchar($note);
		$this->setOutVarchar($adddate);
		$this->setOutInteger($objectLabel);
		$this->execute('Get');
	}

public function GetName($featureTypeId,&$name)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutVarchar($name);
		$this->execute('GetName');
	}	
//	procedure Remove(featureTypeId in number, removeMode in number)
	public function Remove($featureTypeId,$removeMode)
	{
		$this->setInInteger($featureTypeId);
		$this->setInInteger($removeMode);
		$this->execute('Remove');
	}
	
	public function getListObjectByType($featureTypeId)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListObjectByType');
		return $recordset;
	}
	public function GetCountListObjectByExample($featureTypeId)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutInteger($recordset);
		$this->execute('GetCountListObjectByExample');
		return $recordset;
	}
//	GetListFeatureTypeByLayer(featureLayerId in number, ListOfFeatureType out SYS_REFCURSOR, CountOfFeatureType out number);
	public function GetListFeatureTypeByLayer($featureLayerId = null)
	{
		$this->setInInteger($featureLayerId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListFeatureTypeByLayer');
		return $recordset;
	}
//	GetListFeatureTypeNotByLayer(featureLayerId in number, ListOfFeatureType out SYS_REFCURSOR, CountOfFeatureType out number);
	public function GetListFeatureTypeNotByLayer($featureLayerId)
	{
		$this->setInInteger($featureLayerId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListFeatureTypeNotByLayer');
		return $recordset;
	}
	public function getListPropertyByObjectType($featureTypeId,$ACADView)
	{
		$this->setInInteger($featureTypeId);
		$this->setInInteger($ACADView);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListPropertyByObjectType');
		return $recordset;
	}
	public function GetListPropertyNotByObjectType($featureTypeId)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListPropertyNotByObjectType');
		return $recordset;
	}

//	procedure AddPropertyByObjectType(featureTypeId in number, PropertyId in number, ACadView in number);
	public function AddPropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		$this->setInInteger($featureTypeId);
		$this->setInInteger($PropertyId);
		$this->setInInteger($aCadView);
		$this->execute('AddPropertyByObjectType');
	}
//	procedure RemovePropertyByObjectType(featureTypeId in number, PropertyId in number, ACadView in number);
	public function RemovePropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		$this->setInInteger($featureTypeId);
		$this->setInInteger($PropertyId);
		$this->setInInteger($aCadView);
		$this->execute('RemovePropertyByObjectType');
	}
//	procedure SetPropertyByObjectType(featureTypeId in number, PropertyId in number, ACadView in number);
	public function SetPropertyByObjectType($featureTypeId,$PropertyId,$aCadView)
	{
		$this->setInInteger($featureTypeId);
		$this->setInInteger($PropertyId);
		$this->setInInteger($aCadView);
		$this->execute('SetPropertyByObjectType');
	}
	
    public function getFeatureTypesByFeature($entityId)
    {
        $this->setInInteger($entityId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListFeatureTypeByFeature');
        return $recordset;
    }
    
    public function getListUnbindCardsFeatureType()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetListUnbindCardsFeatureType');
        return $recordset;
    }
    
    public function getListUnbindCardsDateByType($featureTypeId)
    {
        $this->setInInteger($featureTypeId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListUnbindCardsDateByType');
        return $recordset;
    }
}
?>