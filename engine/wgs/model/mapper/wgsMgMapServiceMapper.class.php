<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 * ����� ������������ ��� ������ �������� �������� ��������� (��������, ��������� ���������,�������� � �.�.) 
 * �� �������� ���� Map, ���������� � ��.
 * ��� ������ ����������� � ������ MgMapService.
 */
class wgsMgMapServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.MgMapService';

	public function addMgMap($resourceId,$resourceIds)
	{
		$this->setInVarchar($resourceId);
		$this->setInVarcharArray($resourceIds);
		$this->setOutInteger($mgMapId);
		$this->execute('add');
		return $mgMapId;
	}

	public function setMgMap($mgMapId,$layerResourceId,$legengLabel,$name)
	{
		$this->setInInteger($mgMapId);
		$this->setInVarchar($layerResourceId);
		$this->setInVarchar($legengLabel);
		$this->setInVarchar($name);
		$this->execute('set');
	}

	public function renameMgMap($oldName,$newName)
	{
		$this->setInVarchar($oldName);
		$this->setInVarchar($newName);
		$this->execute('set');
	}

	public function copyMgMap($source,$destination)
	{
		$this->setInVarchar($source);
		$this->setInVarchar($destination);
		$this->execute('copy');
	}

	public function removeMgMap($resourceId)
	{
		$this->setInVarchar($resourceId);
		$this->execute('remove');
	}
}
?>