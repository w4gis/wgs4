﻿<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsFeatureDocServiceMapper extends wgsSysObjectServiceMapper
{
    protected $package = 'wgs3logic.FeatureDocService';

    public function getDocDescriptionList($entityId)
    {
        $this->setInInteger($entityId);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetDocDescriptionList');
        return $recordSet;
    }
    
    public function getDocDescription($docId)
    {
        $this->setInInteger($docId);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetDocDescription');
        return $recordSet;
    }
    
    public function getDocument($doc_id)
    {
        $this->setInInteger($doc_id);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetDocument');
        return $recordSet;
    }
	
    public function getDocCount ($entityId)
    {
        $this->setInInteger($entityId);
        $this->setOutInteger($record);
        $this->execute('GetDocCount');
        return $record;
    }
    
	public function setDocParam($id, $name, $description)
	{
        $this->setInInteger($id);
        $this->setInVarchar($name);
        $this->setInVarchar($description);
        $this->execute('SetDocParam');    
	}
	
	public function addDoc($entityId, $document, $doc_name, $doc_type, $description, $doc_cache_name)
	{
	    $this->setInInteger($entityId);
	    $this->setInVarchar($doc_name);
	    $this->setInVarchar($doc_type);
	    $this->setInVarchar($description);
	    $this->setInVarchar($doc_cache_name);
	    $this->setInBlob($document);
	    $this->setOutInteger($record);
	    $this->execute('Add');
	    return $record;
	}
	
	public function updateDoc($doc_id, $doc_type, $document, $doc_cache_name)
	{
	    $this->setInInteger($doc_id);
	    $this->setInVarchar($doc_type);
	    $this->setInVarchar($doc_cache_name);
	    $this->setInBlob($document);
	    $this->setOutVarchar($record);
	    $this->execute('UpdateDoc');
	    return $record;
	}
	
	public function removeDoc($id)
	{
        $this->setInInteger($id);
        $this->setOutVarchar($record);
        $this->execute('Remove');  
        return $record;  
	}
	
}
?>