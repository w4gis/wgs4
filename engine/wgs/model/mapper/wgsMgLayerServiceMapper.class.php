<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 * ����� ������������ ��� ������ �������� �������� ��������� (��������, ��������� ���������,�������� � �.�.) 
 * �� �������� ���� Layer, ���������� � ��.
 * ��� ������ ����������� � ������ MgLayer.
 */
class wgsMgLayerServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.MgLayerService';

	public function isMgLayerExist($resourceId)
	{
		$this->setInVarchar($resourceId);
		$this->setOutInteger($isExist);
		$this->execute('isMgLayerExist');
		return $isExist;
	}

	public function addMgLayer($resourceId,$featureLayerName)
	{
		$this->setInVarchar($resourceId);
		$this->setInVarchar($featureLayerName);
		$this->setOutInteger($mgLayerId);
		$this->execute('add');
		return $mgLayerId;
	}

	public function setMgLayer($resourceId,$featureLayerName)
	{
		$this->setInVarchar($resourceId);
		$this->setInVarchar($featureLayerName);
		$this->setOutInteger($mgLayerId);
		$this->execute('set2');
        return $mgLayerId;
	}

	public function renameMgLayer($oldName,$newName)
	{
		$this->setInVarchar($oldName);
		$this->setInVarchar($newName);
		$this->execute('set');
	}

	public function copyMgLayer($source,$destination)
	{
		$this->setInVarchar($source);
		$this->setInVarchar($destination);
		$this->execute('copy');
	}

	public function removeMgLayer($resourceId)
	{
		$this->setInVarchar($resourceId);
		$this->execute('remove');
	}
	
    public function getMgLayerListByEntityIds($features)
    {
        $this->setInIntegerArray($features);
        $this->setOutRecordSet($result);
        $this->execute('GetMgLayerListByEntityIds');
        return $result;
    }
    
    public function getMgLayerListByFLayerIds($featurelayers)
    {
        $this->setInIntegerArray($featurelayers);
        $this->setOutRecordSet($result);
        $this->execute('GetMgLayerListByFLayerIds');
        return $result;
    }
	
	public function getMgLayerListByFLayerIdsI($featurelayers)
    {
        $this->setInVarchar($featurelayers);
        $this->setOutRecordSet($result);
        $this->execute('GetMgLayerListByFLayerIdsI');
        return $result;
    }
}
?>