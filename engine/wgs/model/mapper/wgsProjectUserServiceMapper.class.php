<?php

lmb_require('wgs/model/mapper/wgsBaseUserServiceMapper.class.php');

class wgsProjectUserServiceMapper extends wgsBaseUserServiceMapper
{
	protected $package = 'wgs3logic.ProjectUserService';

	public function getUserMembersByRoleId($roleId)
	{
		$this->setInInteger($roleId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetUserMembersByRoleId');
		return $recordset;
	}

	public function addUser($wgsUserName, $wgsUserPassword, $wgsUserLocked, $wgsUserDescription)
	{
		$this->setInVarchar($wgsUserName);
		$this->setInVarchar($wgsUserPassword);
		$this->setInInteger($wgsUserLocked);
		$this->setInVarchar($wgsUserDescription);
        $this->setInVarchar(WGS_KEYWORD);
		$this->setOutInteger($wgsUserId);
		$this->execute('AddUser');
		return $wgsUserId;
	}

	public function setUser($wgsUserId, $wgsUserName, $wgsUserPassword, $wgsUserLocked, $wgsUserDescription)
	{
		$this->setInInteger($wgsUserId);
		$this->setInVarchar($wgsUserName);
		$this->setInVarchar($wgsUserPassword);
		$this->setInInteger($wgsUserLocked);
		$this->setInVarchar($wgsUserDescription);
		$this->setInVarchar(WGS_KEYWORD);
		$this->execute('SetUser');
	}

	public function revokeWgsRoleMemberships($wgsUserId, $wgsRoleId)
	{
		$this->setInInteger($wgsUserId);
		$this->setInInteger($wgsRoleId);
		$this->execute('RevokeWgsRoleMemberships');
	}

	public function grantWgsRoleMemberships($wgsUserId, $wgsRoleId)
	{
		$this->setInInteger($wgsUserId);
		$this->setInInteger($wgsRoleId);
		$this->execute('GrantWgsRoleMemberships');
	}

	public function removeUser($wgsUserId)
	{
		$this->setInInteger($wgsUserId);
		$this->execute('RemoveUser');
	}

}

?>
