<?php

lmb_require('wgs/model/mapper/wgsPackageMapper.class.php');

class wgsBaseUserServiceMapper extends wgsPackageMapper
{
    public function getUserByName($name)
    {
		if( WGS4_NO_DRIVERS_MODE ){ 
			//var_dump(  $name );
			$this->setInVarchar($name);
			$this->setOutRecord($record);
			//$result = $this->execute('GetUser', true);
			$this->execute('GetUser', false);
			
			//var_dump(  $result );
		   // $values = pg_fetch_assoc($result);
		   // $values2 = pg_fetch_assoc($result);

			$values = $record -> getArray();
			$rolesArr = array();
			$result = array();
			for ($i = 0; $i < count( $values ); $i++) {
				$vi = $values[ $i ];
				$rolesArr[] = array();
				foreach($vi -> getProperties() as $key => $val) {
					//echo $key . " " .  ( strpos($key, "roles_") );
					
					if(strpos( strtolower ( $key ), "roles_" ) !== false ) {
						$rolesArr[ count($rolesArr) - 1 ][
							str_replace("roles_", "", strtolower($key))
						] = $val;						
					} else  {
						//echo $key . strpos( strtolower ( $key ), "passw");
						if( strpos( strtolower ( $key ), "passw") === false )
							$result [ strtolower ( $key ) ] = $val;
						else $result [ strtolower ( $key ) ] = '112';
					}
				}
			}
			
			//var_dump( $rolesArr );
			//$result['roles'] = $rolesArr;
			for( $i = 0; $i < count( $rolesArr ) ; $i++ ) {
				$rolesArr[ $i ] = new WgsDbalArrayPropertyObject( $rolesArr[ $i ] );
			}
			//$r = new WgsDbalArrayPropertyObject($rolesArr);
			$result['roles'] = $rolesArr;
			//var_dump( $result );
			//die();
			return new WgsDbalArrayPropertyObject( $result );
		}
		
		$this->setInVarchar($name);
        $this->setOutRecord($record);
        $result = $this->execute('GetUser', true);

        $values = pg_fetch_assoc($result);
        $values2 = pg_fetch_assoc($result);

        $rolesArr = array();
        foreach($values as $key => $val) {
            if(strpos($key, "roles_") > -1) {
                $rolesArr[str_replace("roles_", "", $key)] = $val;
                unset($values[$key]);
            }
        }
        $roles = new lmbPgsqlRecord();
        $roles->import($rolesArr);
        $values["roles"][] = $roles;

        while($value = pg_fetch_assoc($result)) {
            $rolesArr = array();
            foreach($value as $key => $val) {
                if(strpos($key, "roles_") > -1) {
                    $rolesArr[str_replace("roles_", "", $key)] = $val;
                }
            }
            $roles = new lmbPgsqlRecord();
            $roles->import($rolesArr);
            $values["roles"][] = $roles;
        }

        $record = new lmbPgsqlRecord();
        $record->import($values);
        pg_free_result($result);
		//var_dump($values);
        return $record;
    }
    
    /*
     public function getUserByName($name)
    {
        $this->setInVarchar($name);
        $this->setOutRecord($record);
        $result = $this->execute('GetUser', true);

        $values = pg_fetch_assoc($result);
        $values2 = pg_fetch_assoc($result);

        $rolesArr = array();
        foreach($values as $key => $val) {
            if(strpos($key, "roles_") > -1) {
                $rolesArr[str_replace("roles_", "", $key)] = $val;
                unset($values[$key]);
            }
        }
        $roles = new lmbPgsqlRecord();
        $roles->import($rolesArr);
        $values["roles"][] = $roles;

        while($value = pg_fetch_assoc($result)) {
            $rolesArr = array();
            foreach($value as $key => $val) {
                if(strpos($key, "roles_") > -1) {
                    $rolesArr[str_replace("roles_", "", $key)] = $val;
                }
            }
            $roles = new lmbPgsqlRecord();
            $roles->import($rolesArr);
            $values["roles"][] = $roles;
        }

        $record = new lmbPgsqlRecord();
        $record->import($values);
        pg_free_result($result);

        return $record;
    } 
     
     */ 

    public function getList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetList');
        return $recordset;
    }
	
	public function getPassword($password, $ip)
    {
        $this->setInVarchar($password);
        $this->setInVarchar($ip);
        $this->setInVarchar(WGS_KEYWORD);
        $this->setOutVarchar($name);
        $this->execute('getPasswordSecure');
        return $name;
    }
	
    public function checkPassword($name, $password)
    {
        $this->setInVarchar($name);
        $this->setInVarchar($password);
        $this->setOutInteger($isCorrect);
        $this->execute('CheckPassword');
        return $isCorrect;
    }
	public function checkPassword2($name, $password,$ip)
    {
        $this->setInVarchar($name);
        $this->setInVarchar($password);
        $this->setInVarchar($ip);
        $this->setOutInteger($isCorrect);
        $this->execute('CheckPassword2');
        return $isCorrect;
    }

    public function getUserMembersByRoleId($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetUserMembersByRoleId');
        return $recordset;
    }
}

?>
