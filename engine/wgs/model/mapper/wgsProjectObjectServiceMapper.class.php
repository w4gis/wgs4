<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsProjectObjectServiceMapper extends wgsBaseObjectServiceMapper
{	
	protected $package = 'wgs3logic.ProjectObjectService';
    
  
    public function getWebLayoutList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetWebLayoutList');
        return $recordset;
    }
    
    public function getMgMapLayerList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetMgMapLayerList');
        return $recordset;
    }

    public function getBottomLayerList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetBottomLayerList');
        return $recordset;
    }
    
    public function getFeatureLayerList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetFeatureLayerList');
        return $recordset;
    }

    public function getFeatureLayerGroupList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetFeatureLayerGroupList');
        return $recordset;
    }
    
    public function getBuiltInCommandList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetBuiltInCommandList');
        return $recordset;
    }
    
    public function getCustomCommandList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetCustomCommandList');
        return $recordset;
    }

    public function getPaneList()
    {
        $this->setOutRecordSet($recordset);
        $this->execute('GetPaneList');
        return $recordset;
    }
}

?>