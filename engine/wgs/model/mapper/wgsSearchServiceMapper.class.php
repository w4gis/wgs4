<?php

lmb_require('wgs/model/mapper/wgsPackageMapper.class.php');

class wgsSearchServiceMapper extends wgsPackageMapper
{
    protected $package = 'wgs3logic.SearchService';
    
    public function search($featureTypes, $featureLayers, $properties, $condition, $objectSearch, $start, $limit, $searchInUnbinded, $docSearch, $filter, &$totalCount)
    {
        $this->setInVarcharArray($featureTypes);
        $this->setInVarcharArray($featureLayers);
        $this->setInVarcharArray($properties);
        $this->setInVarchar($condition);
        $this->setInInteger($objectSearch);
        $this->setInInteger($start);
        $this->setInInteger($limit);
        $this->setInInteger($searchInUnbinded);
        $this->setInInteger($docSearch);
        $this->setInVarchar($filter);
        $this->setOutInteger($totalCount);
        $this->setOutRecordSet($result);
        $this->execute('SearchFeatures');
        return $result;
    }
    
    public function getValuesOfBaseProperty($propertyName, $featureLayers, $featureTypes)
    {
        $this->setInVarchar($propertyName);
        $this->setInVarchar($featureLayers);
        $this->setInVarchar($featureTypes);
        $this->setOutRecordSet($result);
        $this->execute('GetValuesOfBaseProperty');
        return $result;
    }

    public function getReportStatSum($featureIds, $featureTypes, $property, $addProperties)
    {
        $this->setInVarcharArray($featureIds);
        $this->setInVarcharArray($featureTypes);
        $this->setInInteger($property);
        $this->setInVarcharArray($addProperties);
        $this->setOutRecordSet($result);
        $this->execute('GetReportStatSum');
        return $result;
    }
}

?>