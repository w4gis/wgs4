<?php



lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsTusurFlatStatsServiceMapper extends wgsPackageMapper {
    protected $package = 'wgs3logic.featurelayer';

	public function getCurrentPlacesByFeatureLayerId($featureLayerId) {
	    $this->setInInteger($featureLayerId);
	    $this->setOutInteger($record);
		$this->execute('tusur_getcurrentplacesbyfeaturelayerid');
	    return $record;
	}

	public function getFreePlacesByFeatureLayerId($featureLayerId) {
	    $this->setInInteger($featureLayerId);
	    $this->setOutInteger($record);
		$this->execute('tusur_getfreeplacesbyfeaturelayerid');
	    return $record;
	}

	public function getMaxByDocsPlacesByFeatureLayerId($featureLayerId) {
	    $this->setInInteger($featureLayerId);
	    $this->setOutInteger($record);
		$this->execute('tusur_getmaxbydocsplacesbyfeaturelayerid');
	    return $record;
	}

	public function getMaxPlacesByFeatureLayerId($featureLayerId) {
	    $this->setInInteger($featureLayerId);
	    $this->setOutInteger($record);
		$this->execute('tusur_getmaxplacesbyfeaturelayerid');
	    return $record;
	}

	public function getSummarySexOfStudentsByFeatureLayerId($featureLayerId) {
	    $this->setInInteger($featureLayerId);
	    $this->setOutRecordSet($record);
		$this->execute('tusur_getsummarysexofstudentsbyfeaturelayerid');
	    return $record;
	}

}
?>
