<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsFavoriteViewServiceMapper extends wgsSysObjectServiceMapper
{
    protected $package = 'wgs3logic.FavoriteViewService';

    public function insertView($number, $wgsuser_id, $center, $scale, $name, $defaultWebLayout, $picture, $cache_name, $layer_view)
    {
        $this->setInInteger($number);
        $this->setInInteger($wgsuser_id);
        $this->setInInteger($defaultWebLayout);
        $this->setInVarchar($center);
        $this->setInVarchar($scale);
        $this->setInVarchar($name);
        $this->setInBlob($picture);
        $this->setInVarchar($cache_name);
        $this->setInVarchar($layer_view);
        $this->setOutVarchar($record);
        $this->execute('InsertView');
        return $record;
    }
   
    public function getGeometryPropretyByUserId($wgsuser_id, $defaultWebLayout)
    {
        $this->setInInteger($wgsuser_id);
        $this->setInInteger($defaultWebLayout);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetGeometryPropretyByUserId');
        return $recordSet;
    }
    public function getCount($wgsuser_id)
    {
        $this->setInInteger($wgsuser_id);
        $this->setOutinteger($recordSet);
        $this->execute('GetCount');
        return $recordSet;
    }
    
    public function getGeometry($favoriteview, $wgsuser_id, $defaultWebLayout)
    {
        $this->setInInteger($favoriteview);
        $this->setInInteger($wgsuser_id);
        $this->setInInteger($defaultWebLayout);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetGeometry');
        return $recordSet;
    }
    
    public function removeView($number, $wgsuser_id, $defaultWebLayout)
    {
        $this->setInInteger($number);
        $this->setInInteger($wgsuser_id);
        $this->setInInteger($defaultWebLayout);
        $this->setOutVarchar($record);
        $this->execute('Remove');  
        return $record;   
    }
    
    public function updateName($number, $wgsuser_id, $name, $defaultWebLayout)
    {
        $this->setInInteger($number);
        $this->setInInteger($wgsuser_id);
        $this->setInVarchar($name);
        $this->setInInteger($defaultWebLayout);
        $this->execute('UpdateName');
    }
    
    public function getBlob($num, $wgsuser_id, $defaultWebLayout)
    {
        $this->setInInteger($num);
        $this->setInInteger($wgsuser_id);
        $this->setInInteger($defaultWebLayout);
        $this->setOutRecordSet($recordSet);
        $this->execute('GetBlob');
        return $recordSet;
    }
	
}
?>