<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 * ����� ������������ ��� ������ �������� �������� ��������� (��������, ��������� ���������,�������� � �.�.) 
 * �� �������� ���� WebLayout, ���������� � ��.
 * ��� ������ ����������� � ������ MgWebLayout.
 */
class wgsMgWebLayoutServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.MgWebLayoutService';

	public function addMgWebLayout($resourceId,$title,$mapResourceId,$names,$invokeScripts)
	{
		$this->setInVarchar($resourceId);
		$this->setInVarchar($title);
		$this->setInVarchar($mapResourceId);
		$this->setInVarcharArray($names);
		$this->setInVarcharArray($invokeScripts);
		$this->setOutInteger($mgWebLayoutId);		
		$this->execute('add');
		return $mgWebLayoutId;
	}

	public function setMgWebLayout($mgWebLayoutId,$name,$label,$description,$invokeScript,$imageURL)
	{
		$this->setInInteger($mgWebLayoutId);
		$this->setInVarchar($name);
		$this->setInVarchar($label);
		$this->setInVarchar($description);
		$this->setInVarchar($invokeScript);
		$this->setInVarchar($imageURL);
		$this->execute('set');
	}
    /*
    public function updateMgWebLayout($mgWebLayoutId,$name,$mgResourceId,$label,$description,$invokeScript,$byDefault)
    {
        //$this->setInInteger($mgWebLayoutId);
        //$this->setInVarchar($name);
        //$this->setInVarchar($label);
        //$this->setInVarchar($description);
        //$this->setInVarchar($invokeScript);
        //print 1;
        $this->setInInteger($mgWebLayoutId);
        $this->execute('SetByDefault');
    }
	*/
    public function setDefaultWebLayout($mgWebLayoutId)
    {
        $this->setInInteger($mgWebLayoutId);
        $this->execute('SetByDefault');
    }

    public function setOverviewWebLayout($mgWebLayoutId)
    {
        $this->setInInteger($mgWebLayoutId);
        $this->execute('SetOverview');
    }
    
	public function renameMgWebLayout($oldName,$newName)
	{
		$this->setInVarchar($oldName);
		$this->setInVarchar($newName);
		$this->execute('set');
	}

	public function copyMgWebLayout($source,$destination)
	{
		$this->setInVarchar($source);
		$this->setInVarchar($destination);
		$this->execute('copy');
	}
	
	public function removeMgWebLayout($resourceId)
	{
		$this->setInVarchar($resourceId);
		$this->execute('remove');
	}
	
    public function getFloorplanWebLayout($fetureId, $propertyType)
    {
    	$this->setInInteger($fetureId);
    	$this->setInVarchar($propertyType);
    	$this->setOutRecord($record);
    	$this->execute('GetFloorplanWebLayout');
    	return $record;
    }
}
?>