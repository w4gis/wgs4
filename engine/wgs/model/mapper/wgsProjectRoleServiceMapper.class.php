<?php

lmb_require('wgs/model/mapper/wgsBaseRoleServiceMapper.class.php');

class wgsProjectRoleServiceMapper extends wgsBaseRoleServiceMapper
{
    protected $package = 'wgs3logic.ProjectRoleService';

	public function setArea($m,$r,$l,$d,$sdo){
		$this->setInInteger($r);
		$this->setInInteger($m);
        $this->setInVarchar($l);
		$this->setInVarchar($d);
		$this->setInVarchar($sdo);
		$this->setOutInteger($res);
        $this->execute('SetMapLimit');        
		return $res;
	}
	public function getArea($m,$r){
		$this->setInInteger($r);
		$this->setInInteger($m);       
		$this->setOutRecordSet($res);
        $this->execute('GetMapLimit');        
		return $res;
	}
    public function getAcceptableChildrenRolesById($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetAcceptableChildrenRolesById');
        return $recordset;
    }

    public function getChildrenRolesById($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetChildrenRolesById');
        return $recordset;
    }

    public function addRole($wgsRoleName, $wgsRoleDescription, $wgsRoleLocked)
    {
        $this->setInVarchar($wgsRoleName);
        $this->setInVarchar($wgsRoleDescription);
        $this->setInVarchar($wgsRoleLocked);
        $this->setOutInteger($wgsRoleId);
        $this->execute('AddRole');
        return $wgsRoleId;
    }

    public function setRole($wgsRoleId,$wgsRoleName, $wgsRoleDescription, $wgsRoleLocked)
    {
        $this->setInInteger($wgsRoleId);
        $this->setInVarchar($wgsRoleName);
        $this->setInVarchar($wgsRoleDescription);
        $this->setInVarchar($wgsRoleLocked);
        $this->execute('SetRole');
    }

    public function revokeWgsRoleMemberships($wgsRoleId, $wgsParentRoleId)
    {
        $this->setInInteger($wgsRoleId);
        $this->setInInteger($wgsParentRoleId);
        $this->execute('RevokeWgsRoleMemberships');
    }

    public function grantWgsRoleMemberships($wgsRoleId, $wgsParentRoleId)
    {
        $this->setInInteger($wgsRoleId);
        $this->setInInteger($wgsParentRoleId);
        $this->execute('GrantWgsRoleMemberships');
    }

    public function revokeSysRoleMemberships($wgsSysRoleId, $wgsProjectRoleId)
    {
        $this->setInInteger($wgsSysRoleId);
        $this->setInInteger($wgsProjectRoleId);
        $this->execute('RevokeSysRoleMemberships');
    }

    public function grantSysRoleMemberships($wgsSysRoleId, $wgsProjectRoleId)
    {
        $this->setInInteger($wgsSysRoleId);
        $this->setInInteger($wgsProjectRoleId);
        $this->execute('GrantSysRoleMemberships');
    }

    public function getSysRoleMemberships($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetSysRoleMemberships');
        return $recordset;
       
    }

    public function removeRole($wgsRoleId)
    {
        $this->setInInteger($wgsRoleId);
        $this->execute('RemoveRole');
    }
}

?>