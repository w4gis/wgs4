<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsAttObjectServiceMapper extends wgsProjectObjectServiceMapper
{
    protected $package = 'wgs3logic.Object';

    //	procedure GetListObjectByTypes(typesId in varchar2, ListOfObject out SYS_REFCURSOR, CountOfObject out number) is
   public function getObjectsByFeatureName($layerId, $name) {

       $this->setInVarchar($layerId);
       $this->setInVarchar($name);

       $this->setOutRecordSet($recordset);

       $this->execute('getObjectByFeatureName');
       return $recordset;
   }

	public function updateProps() {

       $this->setOutInteger($recordset);
       $this->execute('updateprops');
       return $recordset;
   }

	public function unbindStudents() {

       $this->setOutInteger($recordset);
       $this->execute('unbindstudents');
       return $recordset;
   }

   public function getObjectsByRelationIdAndName($relationId, $name) {

       $this->setInInteger($relationId);
       $this->setInVarchar($name);
       $this->setOutRecordSet($recordset);

       $this->execute('getlistobjectbyrelation');
       return $recordset;
   }

   public function getListObjectByFeaturesAndType($layerIds, $typeIds, $start, $limit, $sortby, $filter, $arrayed) {

       $this->setInIntegerArray($layerIds);
       $this->setInIntegerArray($typeIds);

      $this->setInInteger($start);
      $this->setInInteger($limit);
      $this->setInVarchar($sortby);
      $this->setInVarchar($filter);
      // var_dump($layerIds);
      // var_dump($typeIds);
      // var_dump($start);
      // var_dump($limit);
      // var_dump($sortby);
      // var_dump($filter);
      // var_dump($arrayed);
      // die();



       $this->setOutRecordSet($recordset);
       if($arrayed) {
         $this->execute('getlistobjectbyfeaturesandtype_a');
       } else {
         $this->execute('getlistobjectbyfeaturesandtype');
       }
       return $recordset;
   }

   public function getListObjectByFeaturesAndTypeCount($layerIds, $typeIds, $start, $limit, $sortby, $filter, $arrayed) {


        $this->setInIntegerArray($layerIds);
        $this->setInIntegerArray($typeIds);

        $this->setInInteger($start);
        $this->setInInteger($limit);
        $this->setInVarchar($sortby);
       $this->setInVarchar($filter);

        $this->setOutInteger($recordset);
        if($arrayed) {
          $this->execute('getlistobjectbyfeaturesandtypecount_a');
        } else {
          $this->execute('getlistobjectbyfeaturesandtypecount');
        }



        return $recordset;
   }




   public function getValues($objectIds, $propIds)
    {
        $this->setInIntegerArray($objectIds);
        $this->setInIntegerArray($propIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValues');
        return $recordset;
    }

   public function GetListObjectByTypes($typesId, $start, $limit, &$totalCount)
    {
        $this->setInInteger($typesId);
        $this->setInInteger($start);
        $this->setInInteger($limit);
        $this->setOutInteger($totalCount);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListObjectByTypes');
        return $recordset;
    }

    //	procedure GetValuePropertiesByObject(FeatureObjectId in number, ListOfValueProp out SYS_REFCURSOR, CountOfValueProp out number) is
    public function GetValuePropertiesByObject($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValuePropertiesByObject');
        return $recordset;
    }
public function GetValuePropertiesByObjectYear($objectId, $year)
    {
        $this->setInInteger($objectId);
        $this->setInVarchar($year);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValuePropertiesByObjectYear');
        return $recordset;
    }
public function GetCoordinatesByObjectId($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetCoordinatesByObjectId');

        return $recordset;
    }

    public function getValuePropertiesByObjects($objectIds)
    {
        $this->setInIntegerArray($objectIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValuePropertiesByObjects');
        return $recordset;
    }

    public function getObjectTypesByObjects($objectIds, $selectMode)
    {
        $this->setInIntegerArray($objectIds);
        $this->setInInteger($selectMode);
        $this->setOutRecordSet($recordset);
        $this->execute('GetObjectTypesByObjects');
        return $recordset;
    }

	public function getObjectTypeNameByObject($objectIds)
    {
        $this->setInInteger($objectIds);
        $this->setOutVarchar($name);
        $this->execute('GetObjectTypeNameByObject');
        return $name;
    }

	public function getName($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutVarchar($name);
        $this->execute('GetName');
        return $name;
    }
    public function getObjectTypesByFeatures($featureIds)
    {
        $this->setInIntegerArray($featureIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetObjectTypesByFeatures');
        return $recordset;
    }

    public function Set($objectId,$objectName,$objectShortName,$objectNote,$ObjectEditUser)
    {
        $this->setInInteger($objectId);
        $this->setInVarchar($objectName);
        $this->setInVarchar($objectShortName);
        $this->setInVarchar($objectNote);
        $this->setInVarchar($ObjectEditUser);

        $this->execute('Set');
    }

    public function SetName($objectId,$name)
    {
        $this->setInInteger($objectId);
        $this->setInVarchar($name);
        $this->execute('SetName');
    }

    public function SetShortName($objectId,$shortName)
    {
        $this->setInInteger($objectId);
        $this->setInVarchar($shortName);
        $this->execute('SetShortName');
    }

    public function SetNote($objectId,$note)
    {
        $this->setInInteger($objectId);
        $this->setInVarchar($note);
        $this->execute('SetNote');
    }

    //	procedure SetValueProperty(FeatureObjectId in number, PropertyId in number, PropertyValue in varchar2) is
    public function SetValueProperty($objectId,$propertyId,$propertyValue)
    {
        $this->setInInteger($objectId);
        $this->setInInteger($propertyId);
        $this->setInVarchar($propertyValue);
        $this->execute('SetValueProperty');
    }
    //procedure Add(featureTypeId in number, FeatureObjectId out number) is
    public function Add($typeId)
    {
        $this->setInInteger($typeId);
        $this->setOutInteger($objectId);
        $this->execute('Add');
        return $objectId;
    }
    //procedure Remove(FeatureObjectId in number, removeMode in number)
    public function Remove($ObjectId,$removeMode)
    {
        $this->setInInteger($ObjectId);
        $this->setInInteger($removeMode);
        $this->execute('Remove');
    }

    public function GetValueList($propertyId, $objectTypeIds)
    {
        $this->setInInteger($propertyId);
        $this->setInVarchar($objectTypeIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValueList');
        return $recordset;
    }

    public function GetDomainValueList($propertyId)
    {
        $this->setInInteger($propertyId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetDomainValueList');
        return $recordset;
    }

    public function getListObjectByFeature($featureId)
    {
        $this->setInInteger($featureId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListObjectByFeature');
        return $recordset;
    }

    public function getListObjectByFeatures($featureIds)
    {
        $this->setInIntegerArray($featureIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListObjectByFeatures');
        return $recordset;
    }

    public function getAllObjectByFeatures($featureIds)
    {
        $this->setInIntegerArray($featureIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetAllObjectByFeatures');
        return $recordset;
    }

    public function getChildObjectsByParent($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetChildObjectsByParent');
        return $recordset;
    }

    public function getParentObjectsByChild($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetParentObjectsByChild');
        return $recordset;
    }

    public function getValueBasePropertiesByObject($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetValueBasePropertiesByObject');
        return $recordset;
    }

    public function addRelationByFeature($featureLayerId, $objectId, $featureId, $isCurrent)
    {
        $this->setInInteger($featureLayerId);
        $this->setInInteger($objectId);
        $this->setInInteger($featureId);
        $this->setInInteger($isCurrent);
        $this->execute('AddRelationByFeature');
    }

    public function removeRelationByFeature($ADBSDBRelationId, $mode)
    {
        $this->setInInteger($ADBSDBRelationId);
        $this->setInInteger(0);
        $this->setInInteger($mode);
        $this->setInInteger(null);
        $this->execute('RemoveRelationByFeature');
    }

    public function getListObjectByTypeUnbindDate($featureTypeId, $unbindedDate, $start_r, $limit_r, &$totalCount)
    {
        $this->setInInteger($featureTypeId);
        $this->setInVarchar($unbindedDate);
        $this->setInInteger($start_r);
        $this->setInInteger($limit_r);
        $this->setOutInteger($totalCount);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListObjectByTypeUnbindDate');
        return $recordset;
    }

    public function getListAllUnbindedObject($start_r, $limit_r, &$totalCount)
    {
        $this->setInInteger($start_r);
        $this->setInInteger($limit_r);
        $this->setOutInteger($totalCount);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListAllUnbindedObject');
        return $recordset;
    }

    public function getListAllOrphanObject($start_r, $limit_r, &$totalCount)
    {
        $this->setInInteger($start_r);
        $this->setInInteger($limit_r);
        $this->setOutInteger($totalCount);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListAllOrphanObject');
        return $recordset;
    }

    public function removeObjectsByTypeUnbindDate($featureTypeId, $unbindedDate)
    {
        $this->setInInteger($featureTypeId);
        $this->setInVarchar($unbindedDate);
        $this->execute('RemoveObjectsByTypeUnbindDate');
    }

    public function getFeaturesByOrphanObjects($objects)
    {
        $this->setInIntegerArray($objects);
        $this->setOutRecordSet($result);
        $this->execute('GetFeaturesByOrphanObjects');
        return $result;
    }

    public function getListObjectByPolygon($coordsOfPolygon, $featureLayersIds)
    {
        $this->setInIntegerArray($coordsOfPolygon);
        $this->setInIntegerArray($featureLayersIds);
        $this->setOutRecordSet($result);
        $this->execute('GetListObjectByPolygon');
        return $result;
    }

	public function getEntityIdByObject($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutInteger($entityId);
        $this->execute('GetEntityIdByObject');
        return $entityId;
    }
public function getListYears()
    {
        $this->setOutRecordSet($entityId);
        $this->execute('GetListYears');
        return $entityId;
    }
    public function getListLayers()
    {
        $this->setOutRecordSet($entityId);
        $this->execute('GetListLayers');
        return $entityId;
    }

public function getListObjectsByLayer($layerId)
    {
        $this->setInInteger($layerId);
        $this->execute('GetListObjectsByLayer');
        return $entityId;
    }
	public function getEntityIdByFeature($featureId)
    {
        $this->setInInteger($featureId);
        $this->setOutInteger($entityId);
        $this->execute('GetEntityIdByFeature');
        return $entityId;
    }
}
?>
