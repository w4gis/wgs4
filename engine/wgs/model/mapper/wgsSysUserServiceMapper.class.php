<?php

lmb_require('wgs/model/mapper/wgsBaseUserServiceMapper.class.php');

class wgsSysUserServiceMapper extends wgsBaseUserServiceMapper
{
    protected $package = 'wgs3logic.SysUserService';

    public function addUser($wgsUserName, $wgsUserPassword, $wgsUserLocked, $wgsUserDescription)
    {
        $this->setInVarchar($wgsUserName);
        $this->setInVarchar($wgsUserPassword);
        $this->setInInteger($wgsUserLocked);
        $this->setInVarchar($wgsUserDescription);
        $this->setOutInteger($wgsUserId);
        $this->execute('AddUser');
        return $wgsUserId;
    }

    public function setUser($wgsUserId, $wgsUserName, $wgsUserPassword, $wgsUserLocked, $wgsUserDescription)
    {
        $this->setInInteger($wgsUserId);
        $this->setInVarchar($wgsUserName);
        $this->setInVarchar($wgsUserPassword);
        $this->setInInteger($wgsUserLocked);
        $this->setInVarchar($wgsUserDescription);
        $this->execute('SetUser');
    }

    public function removeUser($wgsSysUserId)
    {
        $this->setInInteger($wgsSysUserId);
        $this->execute('RemoveUser');
    }

    public function revokeSysRoleMemberships($wgsSysUserId, $wgsSysRoleId)
    {
        $this->setInInteger($wgsSysUserId);
        $this->setInInteger($wgsSysRoleId);
        $this->execute('RevokeSysRoleMemberships');
    }

    public function grantSysRoleMemberships($wgsSysUserId, $wgsSysRoleId)
    {
        $this->setInInteger($wgsSysUserId);
        $this->setInInteger($wgsSysRoleId);
        $this->execute('GrantSysRoleMemberships');
    }
}

?>
