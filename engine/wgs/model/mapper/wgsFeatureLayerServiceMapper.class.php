<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 
 */
class wgsFeatureLayerServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.featurelayer';

	
	public function Get($layerId)
    {
        $this->setInInteger($layerId);
        $this->setOutRecordSet($recordset);
        $this->execute('getlayer');
        return $recordset;
    }
    
	public function GetLayersByGroupName($layerName)
    {
        $this->setInVarchar($layerName);
        $this->setOutRecordSet($recordset);
        $this->execute('getLayersByGroupName');
        return $recordset;
    }
	
	public function GetListFeatureLayerByType($featureTypeId, $noEmpty)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute($noEmpty ? 'GetListFeatureLayerByType2' : 'GetListFeatureLayerByType');
		return $recordset;
	}
	
	public function GetListFeatureLayerByGroup($featureLayerGroupId)
	{
		$this->setInInteger($featureLayerGroupId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListFeatureLayerByGroup');
		return $recordset;
	}
	//Add(name in varchar2, featureLayerGroupId in number, featureTypeId in number, featureLayerId out number)
	public function Add($name,$groupid,$cs,$typeid)
	{
		$this->setInVarchar($name);
		$this->setInInteger($groupid);
		$this->setInInteger($cs);
		$this->setInInteger($typeid);
		$this->setOutInteger($id);
		$this->execute('Add');
		return $id;
	}

    /**
     * Добавляет тригеры на добавление в слой с id
     * @param $id слоя
     */
    public function addTrigger($id) {
        $this->setInInteger($id);
        $this->execute('add_trigger');
    }

	//Remove(featureLayerId in number, removeMode in number
	public function Remove($id,$removeMode)
	{
		$this->setInInteger($id);
		$this->setInInteger($removeMode);
		$this->execute('Remove');
	}
	//Set(featureLayerId in number, name in varchar2, viewName in varchar2, featureLayerGroupId in number) is
	public function Set($id,$name,$viewName,$groupid)
	{
		$this->setInInteger($id);
		$this->setInVarchar($name);
		$this->setInVarchar($viewName);
		$this->setInInteger($groupid);
		$this->execute('Set');
	}
	
	public function GetName($featureTypeId,&$name)
	{
		$this->setInInteger($featureTypeId);
		$this->setOutVarchar($name);
		$this->execute('GetName');
	}	
	
	//procedure AddRelationAtt(featureLayerId in number, featureTypeId in number, IsCurrent number, RelationMode number) is
	public function AddRelationAtt($featureLayerId,$featureTypeId,$unBindDate,$IsCurrent,$RelationMode)
	{
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setInVarchar($unBindDate);
		$this->setInInteger($IsCurrent);
		$this->setInInteger($RelationMode);
		$this->execute('AddRelationAtt');
	}
	
	//procedure SetRelationAtt(featureLayerId in number, featureTypeId in number, IsCurrent number) is
	public function SetRelationAtt($featureLayerId,$featureTypeId,$IsCurrent)
	{
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setInInteger($IsCurrent);
		$this->execute('SetRelationAtt');
	}
	
	//procedure RemoveRelationAtt(featureLayerId in number, featureTypeId in number, RemoveMode in number) is
	public function RemoveRelationAtt($featureLayerId,$featureTypeId,$RemoveMode)
	{
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setInInteger($RemoveMode);
		$this->execute('RemoveRelationAtt');
	}
	
	//procedure GetListUnBindObjectDateByType(featureLayerId in number, featureTypeId in number, ListOfUnBindDate out SYS_REFCURSOR, CountOfUnBindDate out number) is
	public function GetListUnBindObjectDateByType($featureLayerId, $featureTypeId)
	{
		$this->setInInteger($featureLayerId);
		$this->setInInteger($featureTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListUnBindObjectDateByType');
		return $recordset;
	}
	
	public function GetArea($input)
	{
		$this->setInVarchar($input);
		$this->setOutVarchar($recordset);
		$this->execute('GetArea');
		return $recordset;
	}
}
?>
