<?php

lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsSysProjectServiceMapper extends wgsSysObjectServiceMapper
{
    protected $package = 'wgs3logic.SysProjectService';

    public function getProjectList()
    {
        $this->setOutRecordSet($recordSet);
        $this->execute('GetProjectList');
        return $recordSet;
    }

    public function getRealProjectList()
    {
        $this->setOutRecordSet($recordSet);
        $this->execute('GetRealProjectList');
        return $recordSet;
    }

    public function getProjectsWithRoles()
    {  
        $this->setOutRecordSet($recordSet);
        $this->execute('GetProjectsWithRoles');
        return $recordSet;
    }
	
    public function getProjectListByModuleId($moduleId)
	{
	    $this->setInInteger($moduleId);
		$this->setOutRecordSet($recordSet);
		$this->execute('GetProjectListByModuleId');
		return $recordSet;
	}
	
    public function createSysProjectService($wgsSysProjectName,$wgsSysProjectDomain, $wgsSysModuleDescription, $wgsSysProjectDatabase, 
                                           $wgsSysProjectTitle, $wgsSysLocked, $install_schemes_array, $install_logics_array)
	{
		$this->setInVarchar($wgsSysProjectName);
		$this->setInVarchar($wgsSysProjectDomain);
		$this->setInVarchar($wgsSysModuleDescription);
		$this->setInVarchar($wgsSysProjectDatabase);
		$this->setInVarchar($wgsSysProjectTitle);
		$this->setInInteger($wgsSysLocked);
		$this->setInVarcharArray($install_schemes_array);
		$this->setInVarcharArray($install_logics_array);
		$this->setOutInteger($record);
		$this->execute('Add');
	}
	
    public function editSysProjectService($wgsSysProjectId, $wgsSysProjectName,$wgsSysProjectDomain, $wgsSysModuleDescription, $wgsSysProjectDatabase, 
                                           $wgsSysProjectTitle, $wgsSysLocked)
	{
		$this->setInVarchar($wgsSysProjectId);
		$this->setInVarchar($wgsSysProjectName);
		$this->setInVarchar($wgsSysProjectDomain);
		$this->setInVarchar($wgsSysModuleDescription);
		$this->setInVarchar($wgsSysProjectDatabase);
		$this->setInVarchar($wgsSysProjectTitle);
		$this->setInInteger($wgsSysLocked);
		$this->execute('Set');
	}
	
	public function removeSysProjectService($wgsSysProjectId)
	{
        $this->setInInteger($wgsSysProjectId);
        $this->execute('Remove');    
	}
	
}
?>