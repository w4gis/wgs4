<?php

class wgsPackageMapper
{
	private $parameters = array();
	private $parametersOut = array();
	private $paramcount = 0;
	private $paramOutCount = 0;
	private $dbConnection;
	private $stm;
	private $existRecordSet = false;
	private $existRecord = false;
	private $cleared = true;
	protected $package;

    public function setDbConnection($dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    private $stat;
    /**
     * @param $statement
     * @param bool|\true $ReturnResult true если надо вернуть result
     * @return mixed
     */
    protected function execute($statement, $returnResult = false)
	{

        $this->stat = $statement;
		if (/*!count($this->parameters) || */!$statement || !$this->package)
			return;

		/*if ($this->existRecordSet)
			$parameters = array_merge(array_keys($this->parameters), array('rowcount'));
		else*/
			$parameters = array_keys($this->parameters);

        $schema = explode('.',$this->package);
        $isReturnCursor = false;
        if($this->existRecord /*|| $this->existRecordSet*/) {
            $isReturnCursor = true;
        }
		$this->stm = $this->dbConnection->newProcedure($schema[1] . ".$statement", $parameters, $isReturnCursor);


		$cursorKey = null;
		foreach ($this->parameters as $key => $value)
		{
			if ($value['type'] == 'in')
			{
				if ($value['dtype'] == 'integer')
					$this->stm->setInteger($key, $value['value']);
				if ($value['dtype'] == 'varchar')
					$this->stm->setVarchar($key, $value['value']);
				if ($value['dtype'] == 'blob')
					$this->stm->setBlob($key, $value['value']);
				if ($value['dtype'] == 'varray') {
					$this->stm->setVarcharArray($key, $value['value']);
                }
				if ($value['dtype'] == 'iarray')
					$this->stm->setIntegerArray($key, $value['value']);
			}
			/*if ($value['type'] == 'out')
			{

                //var_dump($this->stm);exit;
				if ($value['dtype'] == 'bigint')
					;//$this->stm->setIntOutValue($key, $this->parameters[$key]['value']);
				if ($value['dtype'] == 'text')
					$this->stm->setVarcharOutValue($key, $this->parameters[$key]['value']);
				if ($value['dtype'] == 'record')
				{
					//$this->stm->setCursor($key, $this->parameters[$key]['value']);
					$cursorKey = $key;
				}
				if ($value['dtype'] == 'recordset')
				{
					$this->stm->setCursor($key, $this->parameters[$key]['value']);
					$cursorKey = $key;
				}
			}*/
		}

        if ($this->existRecordSet) {
            $result = $this->stm->getRecordSet();
        } else {
		    $result = $this->stm->execute();
        }

		unset($this->stm);
		/*if ($cursorKey !== null)
		{
			if ($this->existRecordSet)
				$this->parameters[$cursorKey]['value'] = $this->parameters[$cursorKey]['value']->getRecordSet();
			else
			if ($this->existRecord)
				$this->parameters[$cursorKey]['value'] = $this->parameters[$cursorKey]['value']->getOneRecord();
		}*/
		$this->cleared = false;
        //return $result;

        if($returnResult) {
            // todo sviga надо как то взять getOneRecord, как было, а то в базе 2 пользователя с именем demo
            return $result;
        }

        $this->bindParamsWithResult($result);

	}

    /**
     * Связывает результаты запроса с выходными параметрами
     */
    private function bindParamsWithResult($result) {
		$i = 0;
		//var_dump( $result );
		if ( WGS4_NO_DRIVERS_MODE ){
			foreach($this->parametersOut as $key => $value) {        
				$this->parametersOut[$key]['value'] = ( count($result) > $i ) ? ( $this->existRecordSet ? $result : $result[ $i ] ) : null;
				$i++;
			}
			return;
		} else {
			foreach($this->parametersOut as $key => $value) {        
			
				if ($value['dtype'] == 'record') {
					$record = new lmbPgsqlRecord();
					$values = pg_fetch_assoc($result);
					$record->import($values);
					pg_free_result($result);
					$this->parametersOut[$key]['value'] = $record;
				} else if ($value['dtype'] == 'recordset') {
					$this->parametersOut[$key]['value'] = $result;
				} else {
					if($res = pg_fetch_row($result)) {
						$this->parametersOut[$key]['value'] = $res[0];
					}
				}
			}
    
		}
    }

	protected function setInVarchar($value)
	{
		$this->clearAllParams();
		$this->parameters["p{$this->paramcount}"]['value'] = $value;
		$this->parameters["p{$this->paramcount}"]['dtype'] = 'varchar';
		$this->parameters["p{$this->paramcount}"]['type']  = 'in';
		$this->paramcount++;
	}

    protected function setInBlob($value)
	{
		$this->clearAllParams();
		$this->parameters["p{$this->paramcount}"]['value'] = $value;
		$this->parameters["p{$this->paramcount}"]['dtype'] = 'blob';
		$this->parameters["p{$this->paramcount}"]['type']  = 'in';
		$this->paramcount++;
	}

	protected function setInInteger($value)
	{
		$this->clearAllParams();
		$this->parameters["p{$this->paramcount}"]['value'] = $value;
		$this->parameters["p{$this->paramcount}"]['dtype'] = 'integer';
		$this->parameters["p{$this->paramcount}"]['type']  = 'in';
		$this->paramcount++;
	}

	protected function setInVarcharArray($value)
	{
		$this->clearAllParams();
		$this->parameters["p{$this->paramcount}"]['value'] = $value;
		$this->parameters["p{$this->paramcount}"]['dtype'] = 'varray';
		$this->parameters["p{$this->paramcount}"]['type']  = 'in';
		$this->paramcount++;
	}

	protected function setInIntegerArray($value)
	{
		$this->clearAllParams();
		$this->parameters["p{$this->paramcount}"]['value'] = $value;
		$this->parameters["p{$this->paramcount}"]['dtype'] = 'iarray';
		$this->parameters["p{$this->paramcount}"]['type']  = 'in';
		$this->paramcount++;
	}

	protected function setOutVarchar(&$value)
	{
		//$this->clearAllParams();
		$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
		$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'varchar';
		//$this->parameters["p{$this->paramOutCount}"]['type']  = 'out';
		$this->paramOutCount++;
	}

	protected function setOutInteger(&$value)
	{
		//$this->clearAllParams();
		$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
		$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'integer';
		//$this->parameters["p{$this->paramcount}"]['type']  = 'out';
		$this->paramOutCount++;
	}

	protected function setOutRecord(&$value)
	{
        $this->existRecord = true;
		//$this->clearAllParams();
		$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
		$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'record';
		//$this->parameters["p{$this->paramcount}"]['type']  = 'out';
		$this->paramOutCount++;
	}

	/*protected function setOutRecordSet(&$value)
	{
        $this->existRecordSet = true;
		//$this->clearAllParams();
		$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
		$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'recordset';
		//$this->parameters["p{$this->paramcount}"]['type']  = 'out';
		$this->paramOutCount++;
	}*/
	
	protected function setOutRecordSet(&$value)
	{  
		if( WGS4_NO_DRIVERS_MODE ) {
			$this->existRecord = true;
			$this->existRecordSet = true;
			//$this->clearAllParams();
			$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
			$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'record';
			//$this->parameters["p{$this->paramcount}"]['type']  = 'out';
			$this->paramOutCount++;				
		}else {
			$this->existRecordSet = true;
			//$this->clearAllParams();
			$this->parametersOut["p{$this->paramOutCount}"]['value'] = &$value;
			$this->parametersOut["p{$this->paramOutCount}"]['dtype'] = 'recordset';
			//$this->parameters["p{$this->paramcount}"]['type']  = 'out';
			$this->paramOutCount++;
		
		}
        
	}

	private function clearAllParams()
	{
		if (!$this->cleared)
		{
			unset($this->parameters);
			$this->parameters = array();
			$this->paramcount = 0;
			$this->stm = null;
			$this->existRecordSet = false;
			$this->existRecord = false;
			$this->cleared = true;
		}
	}
}

?>
