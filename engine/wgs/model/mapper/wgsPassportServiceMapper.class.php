<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');

class wgsPassportServiceMapper extends wgsProjectObjectServiceMapper
{
    protected $package = 'wgs3logic.PassportService';
/*
 *  ��������� ����
 */     
    public function getTitleList($featureId) {
        $this->setInInteger($featureId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetTitleList');
        return $recordset;
    }

/*
 * ��������� �������� ����������
 */ 
    public function getGeneralInfo($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecord($record);
         $this->execute('GetGeneralInfo');
        return $record;
    }    
    
/*
 *  ���������������� �����
 */
    public function getBasePpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetBasePpart');
        return $recordset;
    }     
    
    public function getPillarPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPillarPpart');
        return $recordset;
    }

    public function getCraneGirderPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetCraneGirderPpart');
        return $recordset;
    }

    public function getWallPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetWallPpart');
        return $recordset;
    }
  
    public function getPeregorodkiPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPeregorodkiPpart');
        return $recordset;
    }

    public function getNesKonstruktPerekrPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetNesKonstruktPerekrPpart');
        return $recordset;
    }
    
    public function getNesKonstruktKrovliPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetNesKonstruktKrovliPpart');
        return $recordset;
    }
    
    public function getYteplitPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetYteplitPpart');
        return $recordset;
    }
    
    public function getKrovlyaPpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetKrovlyaPpart');
        return $recordset;
    }
/*
 * ��������-������� �����
 */
    public function getBaseSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetBaseSBpart');
        return $recordset;
    }
    
    public function getNesKarkasSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetNesKarkasSBpart');
        return $recordset;
    }
    
    public function getWallSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetWallSBpart');
        return $recordset;
    }
    
    public function getPeregorodkiSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPeregorodkiSBpart');
        return $recordset;
    }
    
    public function getMejStorePerekrSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetMejStorePerekrSBpart');
        return $recordset;
    }
    
    public function getCherdakPerekrSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetCherdakPerekrSBpart');
        return $recordset;
    }
    
    public function getLestnizaSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetLestnizaSBpart');
        return $recordset;
    }
    
    public function getNesElemKrovliSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetNesElemKrovliSBpart');
        return $recordset;
    }
    
    public function getKrovlyaSBpart($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetKrovlyaSBpart');
        return $recordset;
    }
    
    public function getAreaOfRooms($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecord($record);
        $this->execute('GetAreaOfRooms');
        return $record;
    }
    
    public function getAreaOfOtherRooms($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetAreaOfOtherRooms');
        return $recordset;
    }
    
    public function getAreaOfFloor($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetAreaOfFloor');
        return $recordset;
    }
    
    public function getRazmeriNarujPoverhn($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecord($record);
        $this->execute('GetRazmeriNarujPoverhn');
        return $record;
    }
    
    public function getRazmeriStekol1($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetRazmeriStekol1');
        return $recordset;
    }
    
    public function getRazmeriStekol2($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetRazmeriStekol2');
        return $recordset;
    }
    
    public function getRoofingValue($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetRoofingValue');
        return $recordset;
    }
    
    public function getRazmeriVnytrPoverhn($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecord($record);
        $this->execute('GetRazmeriVnytrPoverhn');
        return $record;
    }
    
    public function getIsCeiling($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsCeiling');
        return $recordset;
    }
    
    public function getIsWall($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsWall');
        return $recordset;
    }
    
    public function getIsSepta($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsSepta');
        return $recordset;
    }
    
    public function getIsPillar($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsPillar');
        return $recordset;
    }
    
    public function getIsCraneGirder($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsCraneGirder');
        return $recordset;
    }
    
    public function getIsOther($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetIsOther');
        return $recordset;
    }
    
    public function getNote($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecord($record);
        $this->execute('GetNote');
        return $record;
    }
    
    public function getFloorPlan($passportId) 
    {
        $this->setInInteger($passportId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetFloorPlan');
        return $recordset;
    }
    
}    
?>