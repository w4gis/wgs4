<?php

lmb_require('wgs/model/mapper/wgsPackageMapper.class.php');

class wgsBasePermissionServiceMapper extends wgsPackageMapper
{
	public function getPermissions()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('GetPermissions');
		return $recordset;
	}
	
    public function getPermissionsByObjectId($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPermissionsByObjectId');
        return $recordset;
    }

    public function getPermissionsByRoleId($roleId)
    {
        $this->setInInteger($roleId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPermissionsByRoleId');
        return $recordset;
    }
    
    public function setPermission($roleId, $objectId, $privilegeId, $permission)
    {
        $this->setInInteger($roleId);
        $this->setInInteger($objectId);
        $this->setInInteger($privilegeId);
        $this->setInInteger($permission);
        $this->execute('SetPermission');
    }
}

?>