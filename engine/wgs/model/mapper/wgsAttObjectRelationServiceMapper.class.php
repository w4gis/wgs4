<?php
lmb_require('wgs/model/mapper/wgsBaseObjectServiceMapper.class.php');
/**
 
 */
class wgsAttObjectRelationServiceMapper extends wgsProjectObjectServiceMapper
{
	protected $package = 'wgs3logic.ObjectTypeRelation';
	//GetListObjTypeRelationByParent(ParentObjTypeId in number, ListOfObjTypeRelation out SYS_REFCURSOR, CountOfObjTypeRelation out number) is
	public function GetListObjTypeRelationByParent($ParentObjTypeId)
	{
		$this->setInInteger($ParentObjTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListObjTypeRelationByParent');
		return $recordset;
	}
	public function GetListObjTypeRelationByChild($ChildObjTypeId)
	{
		$this->setInInteger($ChildObjTypeId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListObjTypeRelationByChild');
		return $recordset;
	}
//	GetListObjTypeRelation(RelationId in number, ListOfObjTypeRelation out SYS_REFCURSOR, CountOfObjTypeRelation out number) is
	public function GetListObjTypeRelation($relationId)
	{
		$this->setInInteger($relationId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListObjTypeRelation');
		return $recordset;
	}
	
//	procedure GetListPropertyByRelation(RelationId in number,ListOfProperty out SYS_REFCURSOR,CountOfProperty out number) is
	public function GetListPropertyByRelation($relationId)
	{
		$this->setInInteger($relationId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListPropertyByRelation');
		return $recordset;
	}
//	procedure GetListPropertyNotByRelation(relationId in number, ListOfProperty out SYS_REFCURSOR, CountOfProperty out NUMBER);
	public function GetListPropertyNotByRelation($relationId)
	{
		$this->setInInteger($relationId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListPropertyNotByRelation');
		return $recordset;
	}
//	procedure AddPropetyByRelation(RelationId in number, PropertyId in number);
	public function AddPropetyByRelation($relationId,$propertyId)
	{
		$this->setInInteger($relationId);
		$this->setInInteger($propertyId);
		$this->execute('AddPropetyByRelation');
	}
//	procedure RemovePropetyByRelation(RelationId in number, PropertyId in number);
	public function RemovePropetyByRelation($relationId,$propertyId)
	{
		$this->setInInteger($relationId);
		$this->setInInteger($propertyId);
		$this->execute('RemovePropetyByRelation');
	}		
//	procedure AddObjTypeRelation(ParentObjectTypeId in number, ChildObjectTypeId in number, RelationId in number, ObjTypeRelationId out number)
	public function AddObjTypeRelation($ParentObjectTypeId,$ChildObjectTypeId,$RelationId)
	{
		$this->setInInteger($ParentObjectTypeId);
		$this->setInInteger($ChildObjectTypeId);
		$this->setInInteger($RelationId);
		$this->setOutInteger($objTypeRelationId);
		$this->execute('AddObjTypeRelation');
		return $objTypeRelationId;
	}
//	procedure RemoveObjTypeRelation(ObjTypeRelationId in number, removeMode in number) is	
	public function RemoveObjTypeRelation($objTypeRelationId,$removeMode)
	{
		$this->setInInteger($objTypeRelationId);
		$this->setInInteger($removeMode);
		$this->execute('RemoveObjTypeRelation');
	}
//	procedure RemoveObjTypeRelationByData(ParentObjectTypeId in number, ChildObjectTypeId in number, RelationId in number) is
	public function RemoveObjTypeRelationByData($ParentObjectTypeId,$ChildObjectTypeId,$RelationId)
	{
		$this->setInInteger($ParentObjectTypeId);
		$this->setInInteger($ChildObjectTypeId);
		$this->setInInteger($RelationId);
		$this->execute('RemoveObjTypeRelationByData');
	}

//	procedure AddRelation(Name in varchar2, RelationTypeId in number, RelationId out number) is
	public function AddRelation($name,$relationTypeId)
	{
		$this->setInVarchar($name);
		$this->setInInteger($relationTypeId);
		$this->setOutInteger($relationId);
		$this->execute('AddRelation');
		return $relationId;
	}
//	procedure SetRelation(RelationId in number, Name in varchar2, RelationTypeId in number) is
	public function SetRelation($relationId, $name)
	{
		$this->setInInteger($relationId);
		$this->setInVarchar($name);
		$this->execute('SetRelation');
	}
	
//	procedure GetListRelByObject(ObjectId in number,ListOfRelation out SYS_REFCURSOR,CountOfRelation out number) is
	public function GetListRelByObject($objectId)
	{
		$this->setInInteger($objectId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListRelByObject');
		
		return $recordset;
	}	
	
// procedure GetListChildObjectByRelation(relationId in number, parentObjectId in number, ListOfChildObject out SYS_REFCURSOR, CountOfChildObject out number)�
	public function GetListChildObjectByRelation($relationId, $parentObjectId)
	{
		$this->setInInteger($relationId);
		$this->setInInteger($parentObjectId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListChildObjectByRelation');
		return $recordset;
	}
//procedure GetListChildObjTypeByRelation(relationId in number,ListOfChildObjectType out SYS_REFCURSOR, CountOfChildObjectType out number);
	public function GetListChildObjectTypeByRelation($relationId)
	{
		$this->setInInteger($relationId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetListChildObjTypeByRelation');
		return $recordset;
	}
public function GetListYears()
    {        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListYears');
        return $recordset;
    }
public function GetListReports()
    {        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListReports');
        return $recordset;
    }
public function GetListLayers()
    {        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListLayers');
        return $recordset;
    }
    public function GetListChildObjectTypeByRelEx($relationId, $objectId)
    {
        $this->setInInteger($relationId);
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListChildObjTypeByRelEx');
        return $recordset;
    }
    public function GetListGeometryByLayer($layerId)
    {
        $this->setInInteger($layerId);        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListGeometryByLayer');
        return $recordset;
    }
public function GetListAttrByLayer($layerId)
    {
        $this->setInInteger($layerId);        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListAttrByLayer');
        return $recordset;
    }
public function GetListGroupByAttr($attrId)
    {
        $this->setInInteger($attrId);        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListGroupByAttr');
        return $recordset;
    }
public function GetListPropByGroup($groupId)
    {
        $this->setInInteger($groupId);        
        $this->setOutRecordSet($recordset);
        $this->execute('GetListPropByGroup');
        return $recordset;
    }
    
//procedure AddObjectRelation(ParentObjectId in number, ChildObjectId in number, ObjTypeRelationId in number, ObjectRelationId out number);
	public function AddObjectRelation($parentObjectId,$childObjectId,$objTypeRelationId)
	{
		$this->setInInteger($parentObjectId);
		$this->setInInteger($childObjectId);
		$this->setInInteger($objTypeRelationId);
		$this->setOutInteger($objectRelationId);
		$this->execute('AddObjectRelation');
		return $objectRelationId;
	}

    public function AddObjectRelationEx($parentFeaturesIds,$childObjectId,$objTypeRelationId)
    {
        $this->setInIntegerArray($parentFeaturesIds);
        $this->setInInteger($childObjectId);
        $this->setInInteger($objTypeRelationId);
        $this->execute('AddObjectRelation');
    }
	
//procedure GetValuePropertiesByObjRel(ObjectRelationId in number, ListOfValueProp out SYS_REFCURSOR, CountOfValueProp out number)
	public function GetValuePropertiesByObjRel($objectRelationId)
	{
		$this->setInInteger($objectRelationId);
		$this->setOutRecordSet($recordset);
		$this->execute('GetValuePropertiesByObjRel');
		return $recordset;
	}

//procedure SetValuePropertyRelation(ObjectRelationId in number, PropertyId in number, PropertyValue in varchar2);
	public function SetValuePropertyRelation($objectRelationId,$propertyId,$propertyValue)
	{
		$this->setInInteger($objectRelationId);
		$this->setInInteger($propertyId);
		$this->setInVarchar($propertyValue);
		$this->execute('SetValuePropertyRelation');
	}
	
//procedure RemoveObjectRelation(ObjectRelationId in number, removeMode in number) is
	public function RemoveObjectRelation($objectRelationId,$removeMode)
	{
		$this->setInInteger($objectRelationId);
		$this->setInInteger($removeMode);
		$this->execute('RemoveObjectRelation');
	}
	
    public function RemoveObjectRelationEx($childObjectIds,$parentFeatureIds,$objectTypeRelationIds,$removeMode)
    {
        $this->setInIntegerArray($childObjectIds);
        $this->setInIntegerArray($parentFeatureIds);
        $this->setInIntegerArray($objectTypeRelationIds);
        $this->setInInteger($removeMode);
        $this->execute('RemoveObjectRelation');
    }
	
    public function GetListRelationByFeature($featureId)
    {
        $this->setInInteger($featureId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListRelationByFeature');
        return $recordset;
    }
    
    public function getListChildObjectByFeatures($relationId, $featureIds)
    {
        $this->setInInteger($relationId);
        $this->setInIntegerArray($featureIds);
        $this->setOutRecordSet($recordset);
        $this->execute('GetListChildObjectByFeatures');
        return $recordset;
    }
}
?>