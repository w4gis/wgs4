<?php

lmb_require('wgs/model/mapper/wgsPackageMapper.class.php');

class wgsAudit extends wgsPackageMapper
{
protected $package = 'wgs3logic.proj_audit';

	public function getAuditActivity(){
        $this->setOutInteger($liActionId);
        $this->execute('getAuditActivity');
        return $liActionId;
	}
	public function setAuditActivity($state, $id, $host, $reason){
        $this->setInInteger($state);        
		$this->setInInteger($id);
		$this->setInVarchar($host);
		$this->setInVarchar($reason);
		$this->setOutInteger($liActionId);
		$this->execute('setAuditActivity');
        return $liActionId;
	}

//--Action - �������� � �������  
	public function AddAction($name, $guiName, $inUse, $description)//������ �������� ��������, �������� �������� ��� ���, ������������ ��, �������� ��������, �������� ��
    {
        $this->setInVarchar($name);
		$this->setInInteger($inUse);
		$this->setInVarchar($guiName);
		$this->setInVarchar($description);
        $this->setOutInteger($liActionId);
        $this->execute('addLiAction');
        return $liActionId;
    }
		public function deleteLiAction($name)//������� �������. �������� �������� 
    {
        $this->setInVarchar($name);
		$this->execute('deleteLiAction');
        
    }
	public function getLiActionIdByName($name)//�������� �� �� ����� 
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liActionId);
        $this->execute('getLiActionIdByName');
        return $liActionId;
    }
	public function setActionListeningOn($name)//������� ��������, �������� ��
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liActionId);
        $this->execute('setActionListeningOn');
        return $liActionId;
    }
	public function setActionListeningOff($name)//��������� ��������, ������� ��
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liActionId);
        $this->execute('setActionListeningOff');
        return $liActionId;
    }
		public function setLiActionGuiName($name, $guiName)//������ ��� ��� ������������ ���������� �������� �������� ��, ����� ��� � ��� ��� ��� ��������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($guiName);
        $this->setOutInteger($liActionId);
        $this->execute('setLiActionGuiName');
        return $liActionId;
    }
		public function setLiActionDescription($name, $description)//������ �������� ��� �������� �������� ��� ����� ���������, �������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($description);
        $this->setOutInteger($liActionId);
        $this->execute('setLiActionDescription');
        return $liActionId;
    }
		public function setLiActionName($name, $newName)//����� ��� ��������	������ ��� ����� � �������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($newName);
        $this->setOutInteger($liActionId);
        $this->execute('setLiActionName');
        return $liActionId;
    }
	//--Param - �������� ��������
	public function addLiParam($name, $guiName, $inUse, $description)//������ �������� ���������, �������� �������� ��� ���, ������������ ��, �������� ��������, �������� ��
    {
        $this->setInVarchar($name);
		$this->setInInteger($inUse);
		$this->setInVarchar($guiName);
		$this->setInVarchar($description);
        $this->setOutInteger($liParamId);
        $this->execute('addLiParam');
        return $liParamId;
    }
		public function deleteLiParam($name)//������� ��������. �������� �������� 
    {
        $this->setInVarchar($name);
		$this->execute('deleteLiParam');
        
    }
	public function getLiParamIdByName($name)//�������� �� �� ����� 
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liParamId);
        $this->execute('getLiParamIdByName');
        return $liParamId;
    }
	public function setParamListeningOn($name)//������� ��������, �������� ��
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liParamId);
        $this->execute('setParamListeningOn');
        return $liParamId;
    }
	public function setParamListeningOff($name)//��������� ��������, ������� ��
    {
        $this->setInVarchar($name);
        $this->setOutInteger($liParamId);
        $this->execute('setParamListeningOff');
        return $liParamId;
    }
		public function setLiParamGuiName($name, $guiName)//������ ��� ��� ������������ ���������� ��������� �������� ��, ����� ��� � ��� ��� ��� ��������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($guiName);
        $this->setOutInteger($liParamId);
        $this->execute('setLiParamGuiName');
        return $liParamId;
    }
		public function setLiParamDescription($name, $description)//������ �������� ��� ��������� �������� ��� ����� ���������, �������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($description);
        $this->setOutInteger($liParamId);
        $this->execute('setLiParamDescription');
        return $liParamId;
    }
		public function setLiParamName($name, $newName)//����� ��� ���������	������ ��� ����� � �������� ��
    {
        $this->setInVarchar($name);
		$this->setInVarchar($newName);
        $this->setOutInteger($liParamId);
        $this->execute('setLiParamName');
        return $liParamId;
    }
	//--Action params - ������������ ����� ����������� � ����������

	public function addLiActionParam($liActionId, $liParamId){//��������� ������ � ������� ������� ��������

		$this->setInInteger($liActionId);
		$this->setInInteger($liParamId);

        $this->setOutInteger($liActionParamId);
        $this->execute('addLiActionParam');
        return $liActionParamId;
	}
	public function getLiActionParamIdByIds($liActionId, $liParamId){//�������� �� �� ������ �� �������

		$this->setInInteger($liActionId);
		$this->setInInteger($liParamId);

        $this->setOutInteger($liActionParamId);
        $this->execute('addLiActionParam');
        return $liActionParamId;
	}
		public function removeLiActionParam($liActionId, $liParamId){//������� ������ �� �������

		$this->setInInteger($liActionId);
		$this->setInInteger($liParamId);


        $this->execute('removeLiActionParam');

	}
//	--Action param values - �������� ���������� ���������������� ��������
		public function setLiValueParamString($value,$liActionParamId)
    {
        $this->setInVarchar($value);
		
        $this->setInInteger($liActionParamId);
		$this->setOutInteger($liValueParamId);
        $this->execute('setLiValueParamString');
        return $liValueParamId;
    }
	
			public function setLiValueParamNumeric($value)
    {
        $this->setInInteger($value);
		
        $this->setOutInteger($liActionParamId);
        $this->execute('setLiValueParamNumeric');
        return $liActionParamId;
    }
			public function setLiValueParamReal($value)
    {
        $this->setInInteger($value);
		
        $this->setOutInteger($liActionParamId);
        $this->execute('setLiValueParamReal');
        return $liActionParamId;
    }
			public function setLiValueParamDate($value)
    {
        $this->setInVarchar($value);
		
        $this->setOutInteger($liActionParamId);
        $this->execute('setLiValueParamDate');
        return $liActionParamId;
    }
			public function setLiValueParamDomain($value)
    {
        $this->setInInteger($value);
		
        $this->setOutInteger($liActionParamId);
        $this->execute('setLiValueParamDomain');
        return $liActionParamId;
    }
	public function getLiEvent($userId, $controller, $action, $dateFrom, $dateTo, $start, $limit)
	{
		$this->setInInteger($userId);
		$this->setInVarchar($controller);
		$this->setInVarchar($action);
		$this->setInVarchar($dateFrom);
		$this->setInVarchar($dateTo);
		$this->setInInteger($start);
		$this->setInInteger($limit);
		
		$this->setOutInteger($total);
		
		$this->setOutRecordSet($recordset);
		$this->execute('getLiEvent');
		return array('total' => $total, 'recordset' => $recordset);
	}
		public function addLiEvent($userid, $userhost, $lidate, $controller, $action){
		$this->setInInteger(0);
		
		$this->setInInteger($userid);
		$this->setInVarchar($userhost);
		$this->setInVarchar($lidate);
		$this->setInVarchar($controller);
		$this->setInVarchar($action);		
		$this->setOutInteger($LiId);
		$this->execute('addLiEvent');
		
		
		
		return $LiId;
	}
	public function removeFeatureLayer($id, $userid, $userhost){
		$this->setInInteger(intval($id));
		$this->setInInteger($userid);
		$this->setInVarchar($userhost);
		$this->setOutInteger($LiId);
		$this->execute('removeFeatureLayer');	
		return $LiId;
	}
	public function removeObjectType($id, $userid, $userhost){
		$this->setInInteger(intval($id));
		$this->setInInteger($userid);
		$this->setInVarchar($userhost);
		$this->setOutInteger($LiId);
		$this->execute('removeObjectType');	
		return $LiId;
	}
	public function addLiEventValue($LiId, $paramId, $paramValue){
		$this->setInInteger(0);
		$this->setInInteger($LiId);
		$this->setInVarchar($paramId);
		$this->setInVarchar($paramValue);
		$this->setOutInteger($LiValueId);
		$this->execute('addLiParamValue');	
		return $LiValueId;
	} 
	
		public function getLiControllers()
	{
		
		$this->setOutRecordSet($recordset);
		$this->execute('getLiControllers');
		return $recordset;
	}
		public function getLiActions()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('getLiActions');
		return $recordset;
	}
	public function getLiParams()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('getLiParams');
		return $recordset;
	}
	public function setLiParamActive($id, $val)
	{
		$this->setInVarchar($id);
		$this->setInVarchar($val);
		$this->setOutInteger($LiParamId);
		
		$this->execute('setLiParamActive');
		return $LiParamId;
	}
	public function setLiActionActive($id, $val, $d)
	{
		$this->setInVarchar($id);
		$this->setInVarchar($val);
		$this->setInInteger($d);
		$this->setOutInteger($LiParamId);
		
		$this->execute('setLiActionActive');
		return $LiParamId;
	}
	public function setLiControllerActive($id, $val)
	{
		$this->setInVarchar($id);
		$this->setInVarchar($val);
		$this->setOutInteger($LiParamId);
		
		$this->execute('setLiControllerActive');
		return $LiParamId;
	}
	public function getActiveFeatureLayersList()
	{
		$this->setOutRecordSet($recordset);		
		$this->execute('getFeatureLayerActiveList');
		return $recordset;
	}
	public function setFLayersActive($id, $val){
		$this->setInInteger($id);		
		$this->setInInteger($val);		
		//var_dump($id);
		//var_dump($val);
		
		$this->setOutInteger($flId);		
		$this->execute('setFLayersActive');
		return $flId;
	
	}
	public function getFLayersActiveOnly(){
		$this->setOutRecordSet($recordset);		
		$this->execute('getFeatureLayerList');
		return $recordset;	
	}
	public function setMapActivity($u, $h, $m, $s, $l){
		$this->setInInteger($u);
		$this->setInVarchar($h);
		$this->setInVarchar($m);
		$this->setInVarchar($s);
		$this->setInIntegerArray($l);		
		$this->setOutInteger($res);		
		$this->execute('setMapActivity');
		return $res;	
	}
}

?>