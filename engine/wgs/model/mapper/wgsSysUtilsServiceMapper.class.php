<?php

lmb_require('wgs/model/mapper/wgsPackageMapper.class.php');

class wgsSysUtilsServiceMapper extends wgsPackageMapper
{
	protected $package = 'wgs3logic.SysUtils';

	public function getAclRevision()
	{
		$this->setOutInteger($value);
		$this->execute('GetAclRevision');
		return $value;
	}
}

?>