<?php

lmb_require('wgs/model/mapper/wgsBasePermissionServiceMapper.class.php');

class wgsSysPermissionServiceMapper extends wgsBasePermissionServiceMapper
{	
	protected $package = 'wgs3logic.SysPermissionService';	
	
	public function getPrivileges()
	{
		$this->setOutRecordSet($recordset);
		$this->execute('GetPrivileges');
		return $recordset;
	}
	
    public function getPrivilegeBySysObjectTypeId($sysobjecttype_id)
	{
	    $this->setInInteger($sysobjecttype_id);
		$this->setOutRecordSet($recordset);
		$this->execute('GetPrivilegeBySysObjectTypeId');
		return $recordset;
	}
	
    public function getPermissionsByObjectId($objectId)
    {
        $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPermissionsByObjectId');
        return $recordset;
    }
	public function getPermissionsByRoleId($roleId, $objectId)
	{
	    $this->setInInteger($roleId);
	    $this->setInInteger($objectId);
        $this->setOutRecordSet($recordset);
        $this->execute('GetPermissionsByRoleId');
        return $recordset;
	}
    public function grantPermission($roleId, $objectId, $privilegeId)
    {
        $this->setInInteger($roleId);
        $this->setInInteger($objectId);
        $this->setInInteger($privilegeId);
        $this->execute('GrantPermission');
    }

    public function revokePermission($roleId, $objectId, $privilegeId)
    {
        $this->setInInteger($roleId);
        $this->setInInteger($objectId);
        $this->setInInteger($privilegeId);
        $this->execute('RevokePermission');
    }
}

?>