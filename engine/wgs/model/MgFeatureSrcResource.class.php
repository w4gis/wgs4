<?php

lmb_require('wgs/model/MgBaseResource.class.php');

class MgFeatureSrcResource extends MgBaseResource
{
    public function __construct($resourceId) {
        parent::__construct($resourceId);
        $xpath = new DOMXPath($this->content);
        $nodes = $xpath->query("//FeatureSource/Provider");
        $this->setName($nodes->item(0)->nodeValue);
    }
        
    public function saveToSession($sessionId = false)
    {
        $content = $this->content->saveXML();
        $content = str_replace("%MG_USERNAME%", @FDO_ORACLE_USER, $content);
        $content = str_replace("%MG_PASSWORD%", @FDO_ORACLE_PASSWORD, $content);
        $this->content = DOMDocument::loadXML($content);
        $xpath = new DOMXPath($this->content);
        $nodes = $xpath->query("//FeatureSource/Parameter");
        for ($i=0; $i < $nodes->length; $i++) {
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'Username') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_ORACLE_USER;
            } else
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'Password') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_ORACLE_PASSWORD;
            } else
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'Service') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_DSN;
            } else
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'DataStore') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_ORACLE_USER;
            } else //Raster
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'DefaultRasterFileLocation') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = str_replace("%MG_DATA_FILE_PATH%", @FDO_RASTER_DIR, $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue);
            } else //OSGeo.SDF
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'File') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = str_replace("%MG_DATA_FILE_PATH%", @FDO_SDF_DIR, $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue);
            } else //OSGeo.SHP
            if ($nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue == 'DefaultFileLocation') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_SHP_DIR;
            }
			/* else //King.Oracle
            if ($nodes->item($i)->getElementsByTagName('Name') == 'OracleSchema') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_ORACLE_USER;
            } else
            if ($nodes->item($i)->getElementsByTagName('Name') == 'KingFdoClass') {
                $nodes->item($i)->getElementsByTagName('Value')->item(0)->nodeValue = @FDO_ORACLE_USER.'.fdometadata';
            }*/
        }
        return parent::saveToSession($sessionId);
    }
}

?>