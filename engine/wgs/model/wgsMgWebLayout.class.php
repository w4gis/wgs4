<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsMgWebLayout extends wgsProjectObject implements wgsZendAclResourceInterface
{
    private $mgResourceId;
    private $mgMapId;
    private $mgMapLayers;
    private $builtInCommands;
    private $customCommands;
    private $panes;
    private $byDefault;
    private $overview;
    private $widget;
    private $featurelayerId;

    public function setMgResourceId($mgResourceId)
    {
        $this->mgResourceId = $mgResourceId;
    }
    
    public function getMgResourceId()
    {
        return $this->mgResourceId;
    }
    
    private function setMgMapId($mgMapId)
    {
        $this->mgMapId = $mgMapId;
    }
    
    public function getMgMapId()
    {
        return $this->mgMapId;
    }

    public function getFeaturelayerId()
    {
        return $this->featurelayerId;
    }
    
    private function setFeaturelayerId($featurelayerId)
    {
        $this->featurelayerId = $featurelayerId;
    }
    
    public function getMgMapLayers()
    {
    	if (!is_null($this->mgMapLayers))
    	   return $this->mgMapLayers;
    	   
    	if ($mgMapLayers = lmbToolkit :: instance() -> getWgsTier() -> getProjectObjectService() -> getMgMapLayerList())
    	{
	    	$this->mgMapLayers = array();
	    	foreach($mgMapLayers as $value) {
	    		//if ($this->getMgMapId() == $value->getMgMapId()) todo sviga
	    		  $this->mgMapLayers[] = $value;
            }
    	}
    	return $this->mgMapLayers;
    }

    public function getBuiltInCommands()
    {
    	if (!is_null($this->builtInCommands))
    	   return $this->builtInCommands;
    	   
    	if ($builtInCommands = lmbToolkit :: instance() -> getWgsTier() -> getProjectObjectService() -> getBuiltInCommandList())
    	{
	    	$this->builtInCommands = array();
	    	foreach($builtInCommands as $value)
	    		if ($this->getId() == $value->getMgWebLayoutId())
	    		  $this->builtInCommands[] = $value;
    	}
    	return $this->builtInCommands;
    }

    public function getCustomCommands()
    {
    	if (!is_null($this->customCommands))
    	   return $this->customCommands;

    	if ($customCommands = lmbToolkit :: instance() -> getWgsTier() -> getProjectObjectService() -> getCustomCommandList())
    	{
            $this->customCommands = array();
            foreach($customCommands as $value)
            	if ($this->getId() == $value->getMgWebLayoutId())
            	    $this->customCommands[] = $value;
    	}
    	return $this->customCommands;
    }

    public function getPanes()
    {
    	if (!is_null($this->panes))
    	   return $this->panes;

    	if ($panes = lmbToolkit :: instance() -> getWgsTier() -> getProjectObjectService() -> getPaneList())
    	{
            $this->panes = array();
            foreach($panes as $value)
            	if ($this->getId() == $value->getMgWebLayoutId())
            	    $this->panes[] = $value;
    	}
    	return $this->panes;
    }
    
    public function setByDefault($byDefault)
    {
        $this->byDefault = $byDefault;
    }

    public function setOverview($overview)
    {
        $this->overview = $overview;
    }

    public function getByDefault()
    {
        return $this->byDefault;
    }

    public function getOverview()
    {
        return $this->overview;
    }
	
	//��������� ������� ��� ����������������
	public function getWidget()
    {
        return $this->widget;
    }
	
	public function setWidget($widget)
    {
        $this->widget = $widget;
    }
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setName($objectRecord->get('mgweblayout_name'));
        $this->setId($objectRecord->get('mgweblayout_id'));
        $this->setMgResourceId($objectRecord->get('mgweblayout_resource_id'));
        $this->setMgMapId($objectRecord->get('mgweblayout_mgmap_id'));
        $this->setFeaturelayerId($objectRecord->get('mgweblayout_featurelayer_id'));
        $this->setByDefault($objectRecord->get('mgweblayout_bydefault'));
        $this->setOverview($objectRecord->get('mgweblayout_overview'));
        $this->setWidget($objectRecord->get('mgweblayout_widget'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
}

?>