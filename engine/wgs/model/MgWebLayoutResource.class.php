<?php

lmb_require('wgs/model/MgBaseResource.class.php');
lmb_require('wgs/model/MgMapResource.class.php');

class MgWebLayoutResource extends MgBaseResource
{
    private $mapResource;

    public function __construct($resourceId)
    {       
        parent::__construct($resourceId);
        $this->mapResource = new MgMapResource($this->getMapResourceId(), $this->getMapName());
    }

    public function getMapResourceId()
    {
        $xpath = new DOMXPath($this->content);
        return $xpath->query('//Map/ResourceId')->item(0)->nodeValue;
    }

    public function getMapName()
    {
        $mapResourceId = $this->getMapResourceId();
        ereg("((.*)/(.*).MapDefinition)", $mapResourceId, $regs);
        return $regs[3];
    }
    
    public function getInitialView()
    {
        $CenterX = $this->content->getElementsByTagName("CenterX")->item(0)->nodeValue;
        $CenterY = $this->content->getElementsByTagName("CenterY")->item(0)->nodeValue;
        $Scale   = $this->content->getElementsByTagName("Scale")->item(0)->nodeValue;
        return (!$CenterX && !$CenterY && !$Scale)? null : array(
            'CenterX' => $CenterX,
            'CenterY' => $CenterY,
            'Scale' => $Scale
        );
    }
    
    private function setMapResourceId($mapResource)
    {       
        $xpath = new DOMXPath($this->content);
        $xpath->query('//Map/ResourceId')->item(0)->nodeValue = $mapResource->getResourceId();
    }

    public function getMapResource()
    {
        return $this->mapResource;
    }

    public function rebindMapResource($mapResource)
    {       
        $this->mapResource = $mapResource;
        $this->setMapResourceId($mapResource);
    }

    public function excludePane($paneName)
    {
        $xpath = new DOMXPath($this->content);
        switch($paneName)
        {
            case 'Layers pane': 
                $query = '//InformationPane/LegendVisible';
            break;
            case 'Properties pane':
                $query = '//InformationPane/PropertiesVisible';
            break;
            case 'Toolbar':
                $query = '//ToolBar/Visible';
            break;
            case 'Context menu':
                $query = '//ContextMenu/Visible';
            break;
            case 'Status bar':
                $query = '//StatusBar/Visible';
            break;
            case 'Zoom control':
                $query = '//ZoomControl/Visible';
            break;
            case 'Task pane':
                $query = '//TaskPane/Visible';
            break;
            case 'Task bar':
                $query = '//TaskPane/TaskBar/Visible';
            break;
        }
        if ($query)
        {
            $nodes = $xpath->query($query);
            $nodes->item(0)->nodeValue = 'false';
        }
    }
    
    public function excludeCommand($commandName)
    {
	    $xpath = new DOMXPath($this->content);
	    $queries = array(
	    	'//ContextMenu/MenuItem/SubItem/Command',
	    	'//ContextMenu/MenuItem/Command',
	    	'//ContextMenu/MenuItem/Label',
	    	'//ToolBar/Button/Command',
	    	'//TaskPane/TaskBar/MenuButton/Command',
	    	'//CommandSet/Command/Name'
	    );
	    foreach($queries as $query)
	    {
    	    $nodes = $xpath->query($query);
    	    foreach ($nodes as $node)
    	    {
    	        if ($node->nodeValue == $commandName)
    	            $node->parentNode->parentNode->removeChild($node->parentNode);
    	    }
	    }
	    //TODO: ��������� ������ <MenuItem xsi:type="FlyoutItemType">
	    //TODO: ������ ������ ������ <Button xsi:type="SeparatorItemType">
    }
    
    public function setSimpleMode()
    {
        $webLayoutResourceId = str_replace(".WebLayout", "Simple.WebLayout", $this->getResourceId());
        $this->setResourceId($webLayoutResourceId);
        $mapResourceId = str_replace(".MapDefinition", "Simple.MapDefinition", $this->getMapResource()->getResourceId());
        $this->getMapResource()->setResourceId($mapResourceId);
    }
}

?>