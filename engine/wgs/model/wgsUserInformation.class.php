<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */
 
class wgsUserInformation
{
    private $username;
    private $password;
    private $ip;
    private $rand;
    private $sessionId;
    // ...

    public function __construct($passwordOrSession = false, $ip = false)
    {
        /*if ($username_or_sessionid && $password)
            $this->setUsernamePassword($username_or_sessionid, $password);
        else* */ 
        if ($passwordOrSession && !$ip) {
            $this->setSessionId($passwordOrSession);
		}
        else {        
			$this->setPassword($passwordOrSession);
			$this->ip = $ip;       
		}
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    private function setPassword($password)
    {
        $this->password = $password;
    }
    
    private function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    public function getSessionId($sessionId)
    {
        return $this->sessionId;
    }
    
    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }
    
    public function getIp()
    {
        return $this->ip;
    }
    
    public function getRand()
    {
        return '';//$this->rand;
    }
}
