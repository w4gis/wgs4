<?php

lmb_require('wgs/model/wgsProjectRole.class.php');
lmb_require('wgs/model/wgsSysRole.class.php');

abstract class wgsRole
{
	protected $name;
	protected $id;
	protected $description;
	
	protected $parents = null;
	protected $children = null;
	protected $parentSysRoleIds;
	protected $parentWgsRoleIds;
    
	
	public final static function factory($record)
	{
        if ($record->get('wgsrole_id') !== null)
            return new wgsProjectRole($record);
        if ($record->get('sysrole_id') !== null)
            return new wgsSysRole($record);
	}

	abstract public function import($importData);

	public function __construct($importData = null)
	{
		if (!is_null($importData))
		    $this->import($importData);
	}

	public function addParent($parent)
	{
        if (is_array($this->parents))
            $this->parents[] = $parent;
        else
            $this->parents = array($parent);
	}

    public function addChild($child)
    {
        if (is_array($this->children))
            $this->children[] = $child;
        else
            $this->children = array($child);
    }
	
	public function getParents()
	{
		return $this->parents;
	}
	
	public function getParentsNames()
	{
	    $parentsNames = array();
	    if (is_array($this->parents))
    	    foreach($this->parents as $parent)
    	        $parentsNames[] = $parent->getName();
        return $parentsNames;   
	}

    public function getChildrenNames()
    {
        $childrenNames = array();
        if (is_array($this->children))
            foreach($this->children as $child)
                $childrenNames[] = $child->getName();
        return $childrenNames;   
    }
	
	public function getParentRoleIds()
	{
		$parentRoleIds = array();
		$roles = $this->getParents();
		if (is_array($roles))
		    foreach($roles as $role)
                $parentRoleIds[] = $role->getRoleId();
		return $parentRoleIds;
	}

	public function setParentSysRoleIds($parentSysRoleIds)
	{
        if (is_array($parentSysRoleIds))
        {
            foreach($parentSysRoleIds as $parentSysRoleId)
                $this->parentSysRoleIds[] = $parentSysRoleId->get('sysrole_id');
        } else {
            if($parentSysRoleIds != null) {
                $this->parentSysRoleIds[] = $parentSysRoleIds;
            }
        }
	}

	public function getParentSysRoleIds()
	{
        return $this->parentSysRoleIds;
	}

	public function getParentWgsRoleIds()
	{
        return $this->parentWgsRoleIds;
	}

	public function getName()
	{
        return $this->name;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
	
}

?>