<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsMgBuiltinCommand extends wgsProjectObject implements wgsZendAclResourceInterface
{
    private $description;    
    private $label;
    private $mgWebLayoutId;
    private $mgWebLayoutName;
    
    private function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    private function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }
    
    private function setMgWebLayoutId($mgWebLayoutId)
    {
        $this->mgWebLayoutId = $mgWebLayoutId;
    }

    public function getMgWebLayoutId()
    {
        return $this->mgWebLayoutId;
    }
    
    private function setMgWebLayoutName($mgWebLayoutName)
    {
        $this->mgWebLayoutName = $mgWebLayoutName;
    }

    public function getMgWebLayoutName()
    {
        return $this->mgWebLayoutName;
    }
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setName($objectRecord->get('mgbuiltcommand_name'));
        $this->setId($objectRecord->get('mgbuiltcommand_id'));
        $this->setDescription($objectRecord->get('mgbuiltcommand_description'));
        $this->setMgWebLayoutId($objectRecord->get('mgbuiltcommand_mgweblayout_id'));
        $this->setMgWebLayoutName($objectRecord->get('mgbuiltcommand_weblayout_name'));
        $this->setLabel($objectRecord->get('mgbuiltcommand_label'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
}

?>