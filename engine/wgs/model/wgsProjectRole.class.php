<?php

/**
 * Wgs Model
 *
 * @category
 * @package
 * @subpackage
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */

lmb_require('wgs/model/wgsRole.class.php');
lmb_require('wgs/lib/wgsZendACL.class.php');

class wgsProjectRole extends wgsRole implements wgsZendAclRoleInterface
{
	protected $locked;
	protected $wgsUserMembers;
		
	public function import($roleRecord)
	{
		$this->setId($roleRecord->get('wgsrole_id'));
		$this->setName($roleRecord->get('wgsrole_name'));
		$this->setDescription($roleRecord->get('wgsrole_description'));
		$this->setLocked($roleRecord->get('wgsrole_locked'));
		$this->setParentWgsRoleIds($roleRecord->get('wgsparentroles_wgsrole_id'));
		$this->setParentSysRoleIds($roleRecord->get('sysparentroles_sysrole_id'));
        $this->setWgsUserMembers($roleRecord->get('wgsrole_usermembers_name'));
	}

	public function setParentWgsRoleIds($parentWgsRoleIds)
	{
		if (is_array($parentWgsRoleIds))
		{
			foreach($parentWgsRoleIds as $parentWgsRoleId)
                $this->parentWgsRoleIds[] = $parentWgsRoleId->get('wgsrole_id');
		} else {
            if($parentWgsRoleIds != null) {
                $this->parentWgsRoleIds[] = $parentWgsRoleIds;
            }
        }
	}

	public function getRoleId()
	{
		return "wr:{$this->getId()}";
	}

	public function getLevel($level = 0)
	{
		$parents = $this->getParents();
		if (is_array($parents))
		{
			$levels = array();
			foreach ($parents as $key => $parent)
			{
				if (!is_null($parent))
				$levels[$key] = $parent->getLevel(++$level);
			}
			if (count($levels) > 0)
			{
				sort($levels);
				return $levels[count($levels)-1];
			}
			else
			return $level;
		}
		return $level;
	}

	public function setWgsUserMembers ($wgsUserMembers)
	{
		if (is_array($wgsUserMembers))
		{
            foreach($wgsUserMembers as $wgsUserMember)
                $this->wgsUserMembers[] = $wgsUserMember->get('name');
		} else {
            if($wgsUserMembers != null) {
                $this->wgsUserMembers[] = $wgsUserMembers;
            }
        }
	}

	public function getWgsUserMembers()
	{
		return $this->wgsUserMembers;
	}
	
	public function setLocked($locked)
	{
		$this->locked = $locked;
	}

	public function getLocked()
	{
		return $this->locked;
	}
		
}

?>