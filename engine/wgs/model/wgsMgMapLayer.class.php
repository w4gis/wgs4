<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsMgMapLayer extends wgsProjectObject implements wgsZendAclResourceInterface
{
    private $mgLayerId;
    private $featureLayerId;
    private $mgMapId;
    private $mgMapResourceId;
    private $legend;
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setMgMapId($objectRecord->get('mgmaplayer_mgmap_id'));
        $this->setId($objectRecord->get('mgmaplayer_id'));
        $this->setLegend($objectRecord->get('mgmaplayer_legend'));
        $this->setName($objectRecord->get('mgmaplayer_name'));
        $this->setMgLayerId($objectRecord->get('mgmaplayer_mglayer_id'));
        $this->setFeatureLayerId($objectRecord->get('mgmaplayer_featurelayer_id'));
        $this->setParentObjectId($objectRecord->get('mgmaplayer_parent_wgsobject_id'));
        $this->setMgResourceId($objectRecord->get('mgmaplayer_resource_id'));
        $this->setMgMapResourceId($objectRecord->get('mgmaplayer_mgmap_resource_id'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
    
    private function setMgResourceId($resourceId)
    {
        $this->resourceId = $resourceId;
    }

    private function setMgMapResourceId($resourceId)
    {
        $this->mgMapResourceId = $resourceId;
    }

    public function getMgMapResourceId()
    {
        return $this->mgMapResourceId;
    }
    
    public function getMgMapName()
    {
        $mapResourceId = $this->mgMapResourceId;
        ereg("((.*)/(.*).MapDefinition)", $mapResourceId, $regs);
        return $regs[3];
    }
    
    public function getMgResourceId()
    {
        return $this->resourceId;
    }
    
    private function setMgMapId($mgMapId)
    {
        $this->mgMapId = $mgMapId;
    }
    
    public function getMgMapId()
    {
        return $this->mgMapId;
    }
    
    private function setLegend($legend)
    {
        $this->legend = $legend;
    }
    
    public function getLegend()
    {
        return $this->legend;
    }

    private function setMgLayerId($mgLayerId)
    {
        $this->mgLayerId = $mgLayerId;
    }
    
    public function getMgLayerId()
    {
        return $this->mgLayerId;
    }    

    private function setFeatureLayerId($featureLayerId)
    {
        $this->featureLayerId = $featureLayerId;
    }
    
    public function getFeatureLayerId()
    {
        return $this->featureLayerId;
    }
    
    public function isBottomLayer()
    {
        return !$this->getFeatureLayerId();
    }
}

?>