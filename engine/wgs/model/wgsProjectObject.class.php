<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/wgsMgWebLayout.class.php');
lmb_require('wgs/model/wgsMgPane.class.php');
lmb_require('wgs/model/wgsMgCustomCommand.class.php');
lmb_require('wgs/model/wgsMgBuiltinCommand.class.php');
lmb_require('wgs/model/wgsMgMapLayer.class.php');
lmb_require('wgs/model/wgsFeatureLayer.class.php');
lmb_require('wgs/model/wgsFeatureTypeFork.class.php');
lmb_require('wgs/model/wgsFeatureLayerGroup.class.php');

class wgsProjectObject extends wgsObject
{
    const MGWEBLAYOUT       = 1;
    const MGPANE            = 2;
    const MGCUSTOMCOMMAND   = 3;
    const MGBUILTINCOMMAND  = 4;
    const MGMAPLAYER        = 5;
    const FEATURELAYER      = 6;
    const FEATURETYPEFORK   = 7;
    ////////////////////////////
    const FEATURELAYERGROUP = 100;
    
    public final static function factory( $objectRecord )
    {
    	if ($objectRecord->get('wgsobjecttype_id'))
	        switch ( $objectRecord->get('wgsobjecttype_id') )
	        {
	            case self :: MGWEBLAYOUT:
		            return new wgsMgWebLayout( $objectRecord );
		            
	            case self :: MGPANE:
		            return new wgsMgPane( $objectRecord );
		            
	            case self :: MGCUSTOMCOMMAND:
		            return new wgsMgCustomCommand( $objectRecord );
		            
	            case self :: MGBUILTINCOMMAND:
		            return new wgsMgBuiltinCommand( $objectRecord );
		            
	            case self :: MGMAPLAYER:
		            return new wgsMgMapLayer( $objectRecord );
		            
	            case self :: FEATURELAYER:
		            return new wgsFeatureLayer( $objectRecord );
		            
	            case self :: FEATURETYPEFORK:
		            return new wgsFeatureTypeFork( $objectRecord );
	
	            default:
	                return null;
	        }
	    elseif ($objectRecord->get('featurelayergroup_id'))
	    {
	    	return new wgsFeatureLayerGroup( $objectRecord );
	    }
    }
        
    public function getResourceId()
    {
        return "wo:{$this->getObjectId()}";
    }
}

?>