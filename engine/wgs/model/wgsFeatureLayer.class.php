<?php

lmb_require('wgs/lib/wgsZendACL.class.php');
lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');
lmb_require('wgs/model/wgsProjectObject.class.php');

class wgsFeatureLayer extends wgsProjectObject implements wgsZendAclResourceInterface
{    
    private $viewName;
    private $featureLayerGroupId;
    private $featureLayerGroupName;
    private $inMap;
    private $objectTypeCount;

    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setName($objectRecord->get('featurelayer_name'));
        $this->setId($objectRecord->get('featurelayer_id'));
        $this->setInMap($objectRecord->get('featurelayer_inmap'));
        $this->setViewName($objectRecord->get('featurelayer_view_name'));
        $this->setFeatureLayerGroupId($objectRecord->get('featurelayer_group_id'));
        $this->setFeatureLayerGroupName($objectRecord->get('featurelayer_group_name'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
        $this->setObjectTypeCount($objectRecord->get('object_type_count'));
    }

    private function setInMap($inMap)
    {
        $this->inMap = $inMap;
    }
    
    public function getInMap()
    {
        return $this->inMap;
    }
    
    private function setViewName($viewName)
    {
        $this->viewName = $viewName;
    }
    
    public function getViewName()
    {
        return $this->viewName;
    }

    private function setFeatureLayerGroupName($featureLayerGroupName)
    {
        $this->featureLayerGroupName = $featureLayerGroupName;
    }
    
    public function getFeatureLayerGroupName()
    {
        return $this->featureLayerGroupName;
    }

    private function setFeatureLayerGroupId($featureLayerGroupId)
    {
        $this->featureLayerGroupId = $featureLayerGroupId;
    }
    
    public function getFeatureLayerGroupId()
    {
        return $this->featureLayerGroupId;
    }
    
    public function setObjectTypeCount($objectTypeCount)
    {
        $this->objectTypeCount = $objectTypeCount;
    }
    
    public function getObjectTypeCount()
    {
        return $this->objectTypeCount;
    }
}

?>