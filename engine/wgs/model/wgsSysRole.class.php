<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */

lmb_require('wgs/model/wgsRole.class.php');
lmb_require('wgs/lib/wgsZendACL.class.php');

class wgsSysRole extends wgsRole implements wgsZendAclRoleInterface
{    
    protected $sysUserMembers;
    
    public function import($roleRecord)
    {
        $this->setId($roleRecord->get('sysrole_id'));
        $this->setName($roleRecord->get('sysrole_name'));
        $this->setDescription($roleRecord->get('sysrole_description'));
        $this->setParentSysRoleIds($roleRecord->get('sysparentroles_sysrole_id'));
        $this->setSysUserMembers($roleRecord->get('sysrole_usermembers_name'));
    }
    
    public function getRoleId()
    {
        return "sr:{$this->getId()}";
    }
    
    public function getLevel($level = 0)
    {
        $parents = $this->getParents();
        if (is_array($parents))
            return $parents[0]->getLevel(++$level);
        return $level;
    }
    
	public function setSysUserMembers ($sysUserMembers)
	{
		if (is_array($sysUserMembers))
		{
			foreach($sysUserMembers as $sysUserMember)
                $this->sysUserMembers[] = $sysUserMember->get('name');
		} else {
            if($sysUserMembers != null) {
                $this->sysUserMembers[] = $sysUserMembers;
            }
        }
	}

	public function getSysUserMembers()
	{
		return $this->sysUserMembers;
	}
    
}

?>