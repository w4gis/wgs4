<?php

lmb_require('wgs/model/MgBaseResource.class.php');
lmb_require('wgs/model/MgLayerResource.class.php');
class MgMapResource extends MgBaseResource
{
    private $layerResources = array();

    public function __construct($resourceId, $name)
    {
		//var_dump($resourceId);die();
        parent::__construct($resourceId);
        $this->name = $name;        
        $layerElements = array(
            '//MapLayer',
            '//BaseMapDefinition/BaseMapLayerGroup/BaseMapLayer'
        );
        foreach ($layerElements as $layerElement)
        {
            $xpath = new DOMXPath($this->content);
	        $nodes = $xpath->query($layerElement);
		    for ($i=0; $i < $nodes->length; $i++) {
				$layerName = $nodes->item($i)->getElementsByTagName('Name')->item(0)->nodeValue;
				$layerResourceId = $nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue;
				$layerResource = new MgLayerResource($layerResourceId);
				$layerResource->setName($layerName);
				$this->layerResources[$layerResourceId] = $layerResource;
		    }
        }
    }
    
    public function getLayerResources()
    {
        return $this->layerResources;
    }
    
    public function rebindLayerResource($layerResource)
    {
        if (!$this->layerResources[$layerResource->getOldResourceId()])
            return;
        $layerElements = array(
            '//MapLayer',
            '//BaseMapLayer'
        );
        $layerResources = array();
        foreach ($layerElements as $layerElement) {
            $xpath = new DOMXPath($this->content);
            $nodes = $xpath->query($layerElement);
            for ($i=0; $i < $nodes->length; $i++) {
                if ($nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue == $layerResource->getOldResourceId())
                    $nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue = $layerResource->getResourceId();
            }
        }
        $this->layerResources[$layerResource->getOldResourceId()] = new MgLayerResource($layerResource->getResourceId());
    }

    public function excludeLayer($layerResourceId)
    {
        $layerElements = array(
            '//MapLayer',
            '//BaseMapLayer'
        );
        $layerResources = array();
        foreach ($layerElements as $layerElement)
        {
            $xpath = new DOMXPath($this->content);
            $nodes = $xpath->query($layerElement);
			
			 $nodes_length = $nodes->length;
            for ($i=0; $i < $nodes_length; $i++) {
            	if ($nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue == $layerResourceId)
                    $nodes->item($i)->parentNode->removeChild($nodes->item($i));
                else
                    $layerResources[$nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue] = $this->layerResources[$nodes->item($i)->getElementsByTagName('ResourceId')->item(0)->nodeValue];
            }
        }
        $this->layerResources = $layerResources;   
    }
}

?>
