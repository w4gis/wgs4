<?php

class wgsPrivilege
{
    // wgsProjectObjectType
    const MGWEBLAYOUT_ACCESS      = 1;
    const MGPANE_ACCESS           = 2;
    const MGCUSTOMCOMMAND_ACCESS  = 3;
    const MGBUILTINCOMMAND_ACCESS = 4;
    const MGMAPLAYER_ACCESS       = 5;
    const FEATURELAYER_ACCESS     = 6;
    const FEATURETYPEFORK_ACCESS  = 7;
    // wgsSysObjectType   
    const SYSPROJECT_ACCESS       = 101;
    const SYSMODULE_ACCESS        = 102;
    const SYSPRIVILEGE_ACCESS     = 103;
    
    protected $id;
    protected $name;
    protected $description;

    public function __construct($importData = null)
    {
        if (!is_null($importData))
            $this->import($importData);
    }
    
    public function import($permissionRecord)
    {        
        $this->setId( $permissionRecord->get('privilege_id') );
        $this->setName( $permissionRecord->get('privilege_name') );
        $this->setDescription( $permissionRecord->get('privilege_description') );
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }    
}

?>