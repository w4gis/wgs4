<?php

lmb_require('wgs/model/wgsObject.class.php');
lmb_require('wgs/model/wgsSysModule.class.php');
lmb_require('wgs/model/wgsSysProject.class.php');
lmb_require('wgs/model/wgsSysPrivilege.class.php');

class wgsSysObject extends wgsObject
{
    const PROJECT   = 1;
    const MODULE    = 2;
    const PRIVILEGE = 3;
    
    final public static function factory( $objectRecord )
    {
        switch ( $objectRecord->get('sysobjecttype_id') )
        {
            case self :: MODULE:
	            return new wgsSysModule( $objectRecord );

            case self :: PROJECT:
	            return new wgsSysProject( $objectRecord );	            

            case self :: PRIVILEGE:
	            return new wgsSysPrivilege( $objectRecord );	            
	            
            default:
                return null;
        }
    }
    
    public function getResourceId()
    {
        return "so:{$this->getObjectId()}";
    }
}

?>