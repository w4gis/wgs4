<?php

lmb_require('wgs/lib/wgsZendAclResourceInterface.interface.php');

class wgsMgCustomCommand extends wgsProjectObject implements wgsZendAclResourceInterface
{
    private $description;    
    private $label;
    private $invokeScript;
    private $mgWebLayoutId;
    private $mgWebLayoutName;

    private function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    private function setLabel($label)
    {
        $this->label = $label;
    }

    private function setInvokeScript($invokeScript)
    {
        $this->invokeScript = $invokeScript;
    }
    
    private function setMgWebLayoutId($mgWebLayoutId)
    {
        $this->mgWebLayoutId = $mgWebLayoutId;
    }

    public function getMgWebLayoutId()
    {
        return $this->mgWebLayoutId;
    }

    private function setMgWebLayoutName($mgWebLayoutName)
    {
        $this->mgWebLayoutName = $mgWebLayoutName;
    }

    public function getMgWebLayoutName()
    {
        return $this->mgWebLayoutName;
    }
    
    public function import($objectRecord)
    {
        $this->setObjectId($objectRecord->get('wgsobject_id'));
        $this->setLocked($objectRecord->get('wgsobject_locked'));
        $this->setName($objectRecord->get('mgcustomcommand_name'));
        $this->setId($objectRecord->get('mgcustomcommand_id'));
        $this->setDescription($objectRecord->get('mgcustomcommand_description'));
        $this->setLabel($objectRecord->get('mgcustomcommand_label'));
        $this->setInvokeScript($objectRecord->get('mgcustomcommand_invokescript'));
        $this->setMgWebLayoutId($objectRecord->get('mgcustomcommand_mgweblayout_id'));
        $this->setMgWebLayoutName($objectRecord->get('mgcustomcommand_weblayout_name'));
        $this->setType($objectRecord->get('wgsobjecttype_id'));
    }
}

?>