<?php

/**
 * Wgs Model
 *
 * @category   
 * @package    
 * @subpackage 
 * @copyright  Copyright (c) 2007 GISLab, Rybalov Nikita
 */

lmb_require('wgs/model/wgsUser.class.php');
lmb_require('wgs/lib/wgsZendAclRoleInterface.interface.php');

class wgsProjectUser extends wgsUser implements wgsZendAclRoleInterface
{
	protected $parentWgsRoleIds;
	
    public function import($importData)
    {
        $this->setName( $importData->get('name') );
        $this->setId( $importData->get('wgsuser_id') );
        $this->setPassword( $importData->get('password') );
        $this->setLocked( $importData->get('locked') );
        $this->setParentRoleRawIds( $importData->get('roles') );
        //var_dump($importData->get('roles'));
        $this->setParentRoles( $importData->get('roles') );
        $this->setDescription( $importData->get('description') );
    }
        
    private function setParentRoleRawIds($parentWgsRoleIds)
    {
        if (is_array($parentWgsRoleIds))
        {
            foreach($parentWgsRoleIds as $parentWgsRoleId)
                $this->parentWgsRoleIds[] = $parentWgsRoleId->get('wgsrole_id');
        }
    }

    public function getParentRoleRawIds()
    {
        return $this->parentWgsRoleIds;
    }
    
    public function getRoleId()
    {
        return "wu:{$this->getId()}";
    }
    
    public function getTier()
    {
    	return lmbToolkit :: instance() -> getWgsTier() -> getProjectTier();
    }
}
