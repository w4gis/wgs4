<?php

lmb_require('wgs/model/wgsProjectObject.class.php');
lmb_require('wgs/model/wgsSysObject.class.php');

class wgsObject
{
    public $name;
    protected $id;
    protected $objectId;
    protected $locked;
	
    protected $type; 
    
    protected $parentId;
    protected $parentObjectId;    
    protected $parent;
    
    public static function factory($record)
    {
    	if ($record->get('wgsobjecttype_id') || $record->get('featurelayergroup_id'))
    		return wgsProjectObject :: factory($record);
    	if ($record->get('sysobjecttype_id'))
    		return wgsSysObject :: factory($record);
    }
    
    public function __construct($importData = null)
    {
        if (!is_null($importData))
            $this->import($importData);
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }
    
    public function getObjectId()
    {
        return $this->objectId;        
    }

    public function setLocked($locked)
    {
        $this->locked = $locked;
    }
    
    public function getLocked()
    {
        return $this->locked;
    }

    // ��� ��������, ������� "�������"
    
    protected function setParentObjectId($parentObjectId)
    {
        $this->parentObjectId = $parentObjectId;
    }
    
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    public function getParentId()
    {
        return $this->parentId;
    }
    
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
    
    public function getParent()
    {
        return $this->parent;
    }

    public function getParentObjectId()
    {
        return $this->parentObjectId;
    }
        
    public function getLevel($level = 0)
    {
        if (is_object($this->getParent()))
            return $this->getParent()->getLevel(++$level);
        return $level;
    }
    
    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }
}

?>