<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * class lmbOciCursor.
 *
 * @package dbal
 * @version $Id: lmbOciCursor.class.php 5945 2007-06-06 08:31:43Z fisher $
 */

lmb_require(dirname(__FILE__) . '/lmbOciRecordSet.class.php');

class lmbOciCursor
{
  protected $cursor = null;
  protected $connection;
  protected $count;

  function __construct($connection)
  {
    $this->connection = $connection;
    $this->cursor = oci_new_cursor($connection->getConnectionId());
  }

  function getCursor()
  {
    return $this->cursor;
  }

  function setCursor($cursor)
  {
    $this->cursor = $cursor;
  }
  
  function setCount(&$count)
  {
      $this->count = &$count;
  }
  
  function count()
  {
      return $this->count;
  }
  
  function execute()
  {
    return $this->connection->executeStatement($this->getCursor());
  }

  function free()
  {
    if($this->cursor)
    {
      oci_free_cursor($this->cursor);
      $this->cursor = null;
    }
  }

  function getOneRecord()
  {
    $queryId = $this->connection->executeStatement($this->getCursor());
    $values = oci_fetch_array($queryId, OCI_ASSOC+OCI_RETURN_NULLS);
    oci_free_statement($queryId);
    if(is_array($values))
      return new lmbOciRecord($values);
  }

  function getOneValue()
  {
    $queryId = $this->connection->executeStatement($this->getCursor());
    $row = oci_fetch_array($queryId, OCI_NUM+OCI_RETURN_NULLS);
    oci_free_statement($queryId);
    if(is_array($row) && isset($row[0]))
      return $row[0];
  }

  function getOneColumnAsArray()
  {
    $column = array();
    $queryId = $this->connection->executeStatement($this->getCursor());
    while(is_array($row = oci_fetch_array($queryId, OCI_NUM+OCI_RETURN_NULLS)))
      $column[] = $row[0];
    oci_free_statement($queryId);
    return $column;
  }
  
  function getRecordSet()
  {
    return new lmbOciRecordSet($this->connection, $this);
  }
  
  function getSQL()
  {
      return null;
  }
}
