<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */
lmb_require('limb/dbal/src/drivers/lmbDbQueryStatement.interface.php');
lmb_require(dirname(__FILE__) . '/lmbOciStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciRecord.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciRecordSet.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciArraySet.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciCursor.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciIntegerArray.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciVarcharArray.class.php');
lmb_require(dirname(__FILE__) . '/lmbOciBlob.class.php');

/**
 * class lmbOciQueryStatement.
 *
 * @package dbal
 * @version $Id: lmbOciQueryStatement.class.php 5945 2007-06-06 08:31:43Z pachanga $
 */
class lmbOciProcedureStatement extends lmbOciStatement
{
  protected $lobs = array();  
    
  protected function _prepareStatement()
  {
    $this->statement = oci_parse($this->connection->getConnectionId(),
                                 $this->_handleBindVars($this->sql));

    if(!$this->statement)
    {
      $this->connection->_raiseError();
      return;
    }

    foreach(array_keys($this->parameters) as $name)
    {
          switch(get_class($this->parameters[$name])) {
          case 'lmbOciCursor': {
    	      if(!oci_bind_by_name($this->statement, ':p_' . $name, $this->parameters[$name]->getCursor(), -1, OCI_B_CURSOR))
    	        $this->connection->_raiseError($this->statement);
    	      else
    	      {
    	        if (in_array('rowcount', array_keys($this->parameters)))
    	            $this->parameters[$name]->setCount($this->parameters['rowcount']);
    	      }
          } break;
          case 'lmbOciVarcharArray': {
                if (!oci_bind_array_by_name($this->statement, ':p_' . $name, $this->parameters[$name]->getArray(), count($this->parameters[$name]->getArray()), -1,SQLT_CHR))
                	$this->connection->_raiseError($this->statement);
          } break;
          case 'lmbOciIntegerArray': {
        		if (!oci_bind_array_by_name($this->statement, ':p_' . $name, $this->parameters[$name]->getArray(), count($this->parameters[$name]->getArray()), -1,SQLT_INT))
        			$this->connection->_raiseError($this->statement);
          } break;
          case 'lmbOciBlob': {
              $lob = oci_new_descriptor($this->connection->getConnectionId(), OCI_D_LOB);
    	      if(!oci_bind_by_name($this->statement, ':p_' . $name, $lob, -1, OCI_B_BLOB))
    	         $this->connection->_raiseError($this->statement);
     	         
    	      $this->parameters[$name]->setDescriptor($lob);
    	      $this->addLob($this->parameters[$name]);
          } break;
          default: {
    	      if(!oci_bind_by_name($this->statement, ':p_' . $name, $this->parameters[$name], -1))
    	        $this->connection->_raiseError($this->statement);              
          }
      }
    }

    $this->hasChanged = false;
  }
 
  function addLob($lob)
  {
      $this->lobs[] = $lob;
  }
  
  function writeLobs()
  {
      if (count($this->lobs)>0) 
      {
          foreach($this->lobs as $lob) {
              $lob->write();
          }
      }
  }
  
  function freeLobs()
  {
      if (count($this->lobs)) {
          foreach($this->lobs as $lob) {
              $lob->free();
          }
      }
  }
  
  function hasLobs()
  {
      return (count($this->lobs)> 0)? true: false;
  }
  
  function setCursor($name, &$value)
  {
    $value = new lmbOciCursor($this->connection);
    $this->parameters[$name] = is_null($value) ? null : $value;
    $this->hasChanged = true;
  }
  
  function setVarcharArray($name, $value)
  {
  	$value = new lmbOciVarcharArray($value);
    $this->parameters[$name] = is_null($value) ? null : $value;
    $this->hasChanged = true;
  }

  function setIntegerArray($name, $value)
  {
  	$value = new lmbOciIntegerArray($value);
    $this->parameters[$name] = is_null($value) ? null : $value;
    $this->hasChanged = true;
  }
  
  function setBlob($name, $value)
  {
  	$value = new lmbOciBlob($value);
    $this->parameters[$name] = is_null($value) ? null : $value;
    $this->hasChanged = true;
  }
  
  function setOutValue($name, &$value)
  {
    $this->parameters[$name] = &$value;
    $this->hasChanged = true;
  }

  function setVarcharOutValue($name, &$value = null)
  {
    $value = "";
    $value = str_pad($value, 4000);
    $this->parameters[$name] = &$value;
    $this->hasChanged = true;
  }
  
  function setIntOutValue($name, &$value = null)
  {
    $value = 999999999999999999999999999;
    $this->parameters[$name] = &$value;
    $this->hasChanged = true;
  }
}
