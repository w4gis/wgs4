<?php

class lmbOciVarcharArray
{
  protected $array = null;

  function __construct($array = array())
  {
  	if (is_array($array))
    	$this->array = $array;
  }
  
  public function getArray()
  {
  	return $this->array;
  }
}
