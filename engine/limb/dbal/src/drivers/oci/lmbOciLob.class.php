<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * abstract class lmbOciLob.
 *
 * @package dbal
 * @version $Id: lmbOciLob.class.php 6243 2007-08-29 11:53:10Z pachanga $
 */
abstract class lmbOciLob
{
  protected $value;
  protected $descriptor;
  
  function __construct($value)
  {
    $this->value = $value;
  }

  abstract function getNativeType();
  abstract function getEmptyExpression();
  abstract function getDescriptorType();

  function setDescriptor($descriptor)
  {
      $this->descriptor = $descriptor;
  }
  
  function read()
  {
    return $this->value;
  }
  
  function write()
  {
      $this->descriptor->savefile($this->value);
  }
  
  function free()
  {
      $this->descriptor->free();
  }
}


