<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */
lmb_require('limb/dbal/src/drivers/lmbDbBaseRecord.class.php');

/**
 * class lmbOciRecord.
 *
 * @package dbal
 * @version $Id: lmbOciRecord.class.php 6595 2007-12-06 20:10:05Z pachanga $
 */
class lmbOciRecord extends lmbDbBaseRecord
{
  protected $properties = array();

  function __construct($data = array())
  {
    $this->import($data);
  }

  function get($name, $default = LIMB_UNDEFINED)
  {
    //Character encoding issue? Charset of identifiers?
    $upname = strtoupper($name);
    if(isset($this->properties[$upname]))
      return $this->properties[$upname];
    elseif(isset($this->properties[$name])) //a quick hack
      return $this->properties[$name];
    
    if(LIMB_UNDEFINED != $default)
      return $default;
  }

  function remove($name)
  {
    $upname = strtoupper($name);
    if(isset($this->properties[$upname]))
      unset($this->properties[$upname]);
    elseif(isset($this->properties[$name]))
      unset($this->properties[$name]);
  }

  function has($name)
  {
    $upname = strtoupper($name);
    return isset($this->properties[$upname]) ||
           isset($this->properties[$name]);
  }

  function reset()
  {
    $this->properties = array();
  }

  function set($name, $value)
  {
    $this->properties[$name] = $value;
  }

  function export()
  {
    return array_change_key_case($this->properties, CASE_LOWER);
  }

  function import($values)
  {
    $this->properties = array();

    if(!is_array($values))
      return;

    foreach($values as $key => $value)
    {
      if(is_a($value, 'OCI-Lob')) //should we delay it until getter is called?
        $this->properties[$key] = $value->load();
      elseif(is_resource($value) && get_resource_type($value) == 'oci8 statement')
      {
        oci_execute($value);
        while ($data = oci_fetch_assoc($value))
            $this->properties[$key][] = new lmbOciRecord($data);
          //$this->properties[$key][] = $data;
      }        
      else
        $this->properties[$key] = $value;
    }
  }

  function getInteger($name)
  {
    $value = $this->get($name);
    return is_null($value) ?  null : (int) $value;
  }

  function getFloat($name)
  {
    $value = $this->get($name);
    return is_null($value) ?  null : (float) $value;
  }

  function getString($name)
  {
    $value = $this->get($name);
    return is_null($value) ?  null : (string) $value;
  }

  function getBoolean($name)
  {
    $value = $this->get($name);
    return is_null($value) ? null : (boolean) $value;
  }

  function getBlob($name)
  {
    return $this->get($name);
  }

  function getClob($name)
  {
    return $this->get($name);
  }

  function getIntegerTimeStamp($name)
  {
  }

  function getStringDate($name)
  {
  }

  function getStringTime($name)
  {
  }

  function getStringTimeStamp($name)
  {
  }

  function getStringFixed($name)
  {
    $value = $this->get($name);
    return is_null($value) ?  null : (string) $value;
  }
}


