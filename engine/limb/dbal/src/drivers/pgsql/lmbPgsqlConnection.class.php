<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */
lmb_require('limb/dbal/src/drivers/lmbDbConnection.interface.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlQueryStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlDropStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlInsertStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlManipulationStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlStatement.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlDbInfo.class.php');
lmb_require(dirname(__FILE__) . '/lmbPgsqlTypeInfo.class.php');


class WgsCppSingleDbRecord {
	protected $row;
	protected $associatedData;
	protected $parentObj;
	function __toString () {
		return $this -> row ? ("" . $this -> row -> GetPropertyCount()) : '';
	}
	
	function __construct( $rowData, $parentObj ){
		$this -> row = $rowData;
		$this -> parentObj = $parentObj;
		
		$this -> associatedData = array();
		if($this -> row){
			for( $i=0; $i < $this -> row -> GetPropertyCount(); $i++) {
				$key = $this->row->GetPropertyName($i);
				//  var_dump( $this->row->GetPropertyType($key));
				// //  var_dump( $this->row->GetPropertyName($key));
				// echo $key;
				// die();
				$type = "String";
				
				switch ( $this->row->GetPropertyType($key) ){
					case 1 : {
                        $type = 'Boolean';
                        break;
                    }
                    case 2 : {
                        $type = 'Byte';
                        break;
                    }
					case 3 : {
                        $type = 'DateTime';
                        break;
                    }
                    case 4 : {
                        $type = 'Single';
                        break;
                    }
                    case 5 : {
                        $type = 'Double';
                        break;
                    }
					case 6 : {
						$type = 'Int16';
						break;
					}
					case 7 : {
						$type = 'Int32';
						break;
					}
					case 8 : {
						$type = 'Int64';
						break;
					};
					case 9 : {
                        $type = 'String';
                        break;
                    };
					case 10 : {
						$type = 'Blob';
						break;
					};
					case 13 : {
                        $type = 'Geometry';
                        break;
                    };
					
				}
				//echo 'S-';
				
				if($this -> row -> IsNull ( $key )) {
				    $this -> associatedData [ $key ] = null;
				} else {
				    if($type == "DateTime" ) {
				        $dateTime = $this -> row -> { 'Get' . $type }( $key );
				        $month = $dateTime -> GetMonth();
				        if($month < 10) {
				            $month = '0' . $month;
				        }
				        $this -> associatedData [ $key ] = $dateTime -> GetYear() . '-' . $month . '-' . $dateTime -> GetDay();
				    } else {
				        $this -> associatedData [ $key ] = $this -> row -> { 'Get' . $type }( $key );
				    }
				}
				
				// $this -> associatedData [ $key ] = 
    //             $this -> row -> IsNull ( $key ) ? null :
    //                 $this -> row -> { 'Get' . $type }( $key );
			}
		}
		
		//var_dump($this -> associatedData);
	}
	
	function getArray (){
		return $this -> parentObj -> getArray();
	}
	
	function get( $key ){
		$key = strtoupper( $key );
		return array_key_exists ( $key, $this -> associatedData ) ? $this -> associatedData [ $key ] : null;
	}
	
	function getAssociatedData(){
		return $this -> associatedData;
	}
}

class WgsCppDbal {
	protected $queryResult;
	protected $allRows;
	function __construct($result){
		$this -> queryResult = $result;		
		$this -> allRows = array();
		while ($this -> queryResult -> ReadNext()) {
		    $this -> allRows[] = new WgsCppSingleDbRecord ( $this -> queryResult, $this );
		}
		if( count($this -> allRows) == 0 ){
		    $this -> allRows[] = new WgsCppSingleDbRecord ( null, $this );
		}
		
		$this -> queryResult -> Close();
	}
	
	function getAllData (){
		return $this -> allRows;
	}

	function getArray(){
		$res = array();
		foreach( $this -> getAllData() as $key => $value ){
			$res[] = new WgsDbalArrayPropertyObject( $value -> getAssociatedData() );
		}
		return $res;
	}
}

class WgsDbalArrayPropertyObject {
	protected $properties;
	
	function __construct( $data ) {
		$this -> properties = $data;
	}
	function get( $key ){
		if( array_key_exists ( $key, $this -> properties ) )
			return $this -> properties [ $key ];
		
		$key = strtoupper( $key );
		return array_key_exists ( $key, $this -> properties ) ? $this -> properties [ $key ] : null;	 
	}
	function getProperties (){
		return $this -> properties;
	}
}
/**
 * class lmbPgsqlConnection.
 *
 * @package dbal
 * @version $Id: lmbPgsqlConnection.class.php 6424 2007-10-16 08:12:07Z serega $
 */
class lmbPgsqlConnection implements lmbDbConnection
{
  protected $connectionId;
  protected $config;

    /**
     * @param $procedure
     * @param $arguments
     * @param bool $isReturnCursor true если запрос возвращает курсор
     * @return lmbPgsqlDropStatement|lmbPgsqlInsertStatement|lmbPgsqlManipulationStatement|lmbPgsqlQueryStatement|lmbPgsqlStatement
     */
    function newProcedure($procedure, $arguments, $isReturnCursor = false){
      $needRowcount = false;

      foreach($arguments as $key => $value)
      {
          //if ($value == 'rowcount')
          //    $needRowcount = true;
          $arguments[$key] = ":$value:";
          //$arguments[$key] = $value;
      }

      $arguments = implode(",", $arguments);
      if($isReturnCursor) {
          // cursor_data так называется курсов в запросах.
        if(WGS4_NO_DRIVERS_MODE)
			$statement = $this->newStatement("select * from $procedure($arguments);");
        else
			$statement = $this->newStatement("select * from $procedure($arguments); FETCH ALL from \"cursor_data\";");
		
      } else {
        $statement = $this->newStatement("select * from $procedure($arguments)");
      }
      //if ($needRowcount){}
          //$statement->setIntOutValue('rowcount');
      return $statement;
  }


  function __construct($config)
  {
    $this->config = $config;
  }

  function getType()
  {
    return 'pgsql';
  }

  function getConnectionId()
  {
    if(!isset($this->connectionId))
    {
      $this->connect();
    }
    return $this->connectionId;
  }

  function getHash()
  {
    return crc32(serialize($this->config));
  }

  function connect()
  {

    global $php_errormsg;

    $persistent = isset($this->config['persistent']) ? $this->config['persistent'] : null;

    $connstr = '';

    if($host = $this->config['host'])
    {
      $connstr = 'host=' . $host;
    }
    if($port = $this->config['port'])
    {
      $connstr .= ' port=' . $port;
    }
    if($database = $this->config['database'])
    {
      $connstr .= ' dbname=\'' . addslashes($database) . '\'';
    }
    if($user = $this->config['user'])
    {
      $connstr .= ' user=\'' . addslashes($user) . '\'';
    }
    if($password = $this->config['password'])
    {
      $connstr .= ' password=\'' . addslashes($password) . '\'';
    }

    if($persistent)
    {
      $conn = @pg_pconnect($connstr);
    }
    else
    {
      $conn = @pg_connect($connstr);
    }

    if(!is_resource($conn))
    {
      $this->_raiseError($php_errormsg);
    }

    if(isset($this->config['charset']) && ($charset = $this->config['charset']))
    {
      pg_set_client_encoding($conn, $charset);
    }

    $this->connectionId = $conn;
  }

  function __wakeup()
  {
    $this->connectionId = null;
  }

  function disconnect()
  {
    if($this->connectionId)
    {
      @pg_close($this->connectionId);
      $this->connectionId = null;
    }
  }

  function _raiseError($msg)
  {
    throw new lmbException($msg .($this->connectionId ?  ' last pgsql driver error: ' . pg_last_error($this->connectionId) : ''));
  }

  function execute($sql)
  {
	  
    if( !WGS4_NO_DRIVERS_MODE ) { 
		$result = @pg_query($this->getConnectionId(), $sql);
		if($result === false)
		{
		  $this->_raiseError($sql);
		}
	}
   // var_dump($sql);
    if( WGS4_NO_DRIVERS_MODE ) {
		$mgTier = lmbToolkit :: instance() -> getMgTier();
		$siteConnection = $mgTier->getSiteConnection();
		$featureService = $siteConnection->CreateService(MgServiceType::FeatureService);
		
		$dataSource = new MgResourceIdentifier('Library://'. getProjectName() .'/datasources/local_postgres.FeatureSource');
        
        // $featureService -> SetLongTransaction($dataSource, 'mg-trans');
        
        // var_dump($featureService -> GetLockedFeatures());   
        // echo "111";
        // try {
        //     $tr = $featureService->BeginTransaction($dataSource);
        
        //     $resultQuery = $featureService -> { 'ExecuteSqlNonQuery' }(
        //         $dataSource,
        //         "SELECT * from Object.getlistobjectbyfeaturesandtype(ARRAY[87],ARRAY[4]);",
        //         null, $tr
        //     );
            
        //     $resultQuery = $featureService -> ExecuteSqlQuery(
        //         $dataSource, 
        //         "FETCH ALL from \"cursor_data\";", null, $tr
        //     );
            
        //     $tr -> Commit();
            
        // } catch(Exception $e) {
        //     echo 'Exception';
        //     echo $e -> getExceptionMessage();
        // }
        // die('done');
        while(!$successAll) {
        
            $trc = $mgTier -> getTransactionCount() !== null ? $mgTier -> getTransactionCount() : 0; //active transactions count
            $waitCount = 0;
            $waitInterval = 0.1;
            //  var_dump($trc);
            while($trc > 0) {
            
                time_nanosleep(0, 100000000);
                $waitCount += $waitInterval;
                
                $trc = $mgTier -> getTransactionCount() != null ? $mgTier -> getTransactionCount() : 0;
                if($waitCount > 15) {
                    $mgTier -> setTransactionCount(0);
                    die('no response');
                }
            };
            
            $successAll = false;
            $resultQuery; $tr;
            $success = false;
            try{
                while(!$success) {
                    try{
                        $tr = $featureService->BeginTransaction($dataSource);
                        // var_dump('TR exists <br/>');
                        $mgTier -> setTransactionCount(1);
                        $success = true;
                        
                        $resultQuery = $featureService -> { 'ExecuteSql' . ( substr($sql, -1) == ";" ? 'Non' : '' ) . 'Query' }(
                            $dataSource,
                            $sql, 
                            null, 
                            $tr
                        );
                        
                        
                        // echo ' non-Gotcha ';
                    } catch (Exception $e) {
                        //  echo 'Gotcha!<BR/> ' .$success;
                        
                        
                        //   echo $e -> GetExceptionMessage();
                        //  flush();
                        
                        // var_dump($featureService -> GetLongTransactions());
                        // // die();
                        // flush();
                        time_nanosleep(0, 500000000);
                   
                    }
                }
                  
                  if ( substr($sql, -1) != ";"){
                          
                       $resultQuery -> ReadNext();
                       $type = "String";
                       $name = $resultQuery -> GetPropertyName (0);
                       //var_dump( $name );
                       
                       switch ( $resultQuery -> getPropertyType ( $name ) ){
                               case 6 : {
                                   $type = 'Int16';
                                   break;
                               }
                               case 7 : {
                                   $type = 'Int32';
                                   break;
                               }
                               case 8 : {
                                   $type = 'Int64';
                                   break;
                               };
                               case 10 : {
                                   $type = 'Blob';
                                   break;
                               };
                               
                           };
                           
                       if( $resultQuery -> IsNull( $name )){
                           $result = null;
                       } else {
                           $r = $resultQuery -> { 'Get' . $type }($name);
                           $result = array ($r);
                       }
                        
                        $tr -> Commit();
                        $mgTier -> setTransactionCount(0);
                        
                       
                   } else {
                       
                       if ( substr($sql, -1) == ";" ) {
                               $resultQuery = $featureService -> ExecuteSqlQuery(
                                   $dataSource, 
                                   "FETCH ALL from \"cursor_data\";", null, $tr
                               );
       
                       }
                       $tr -> Commit();
                       $mgTier -> setTransactionCount(0);
                       
                   
                       $r = new WgsCppDbal( $resultQuery );
                       $result = $r -> getAllData();
                       
                       
                       return $result;
                   }
                   
                   $successAll = true;
                   
        
            
        
        } catch( Exception $e) {
         //   echo "ERROR...";
            //  echo 'Gotcha!<BR/> ' .$success;
     
            // echo $e -> GetExceptionMessage();
            // flush();
            // die();
            // echo $e -> GetExceptionMessage();
            // flush();
            if($tr != null) {
                $tr -> Commit();
            }
            $mgTier -> setTransactionCount(0);
            time_nanosleep(0, 500000000);
         //   return;
        }
    }
    // 		echo(' HERE3 ');
    //         flush();
            
    	
        // } catch (Exception $e) {
        //     var_dump('ERROR');
        //     var_dump($e->getMessage());
            
        //     if(isset($tr)) {
        //         $tr -> Rollback();    
        //     }
        //     die();
            
        // }
        $tr -> Commit();
        $mgTier -> setTransactionCount(0);
        return $result;
	}
    return $result;
  }

  function beginTransaction()
  {
    $this->execute('BEGIN');
  }

  function commitTransaction()
  {
    $this->execute('COMMIT');
  }

  function rollbackTransaction()
  {
    $this->execute('ROLLBACK');
  }

  function newStatement($sql)
  {
    if(preg_match('/^\s*\(*\s*(\w+).*$/m', $sql, $match))
    {
      $statement = $match[1];
    }
    else
    {
      $statement = $sql;
    }
    switch(strtoupper($statement))
    {
      case 'SELECT':
      case 'SHOW':
      case 'DESCRIBE':
      case 'EXPLAIN':
      return new lmbPgsqlQueryStatement($this, $sql);
      case 'DROP':
      return new lmbPgsqlDropStatement($this, $sql);
      case 'INSERT':
      return new lmbPgsqlInsertStatement($this, $sql);
      case 'UPDATE':
      case 'DELETE':
      return new lmbPgsqlManipulationStatement($this, $sql);
      default:
      return new lmbPgsqlStatement($this, $sql);
    }
  }

  function getTypeInfo()
  {
    return new lmbPgsqlTypeInfo();
  }

  function getDatabaseInfo()
  {
    return new lmbPgsqlDbInfo($this, $this->config['database'], true);
  }

  function quoteIdentifier($id)
  {
    if(!$id)
      return '';
    $pieces = explode('.', $id);
    $quoted = '"' . $pieces[0] . '"';
    if(isset($pieces[1]))
       $quoted .= '."' . $pieces[1] . '"';
    return $quoted;
  }

  function escape($string)
  {
    return pg_escape_string($string);
  }

  function getSequenceValue($table, $colname)
  {
    $seq = "{$table}_{$colname}_seq";
    return (int)$this->newStatement("SELECT currval('$seq')")->getOneValue();
  }
}


