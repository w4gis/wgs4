<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */
lmb_require('limb/cache/src/lmbCacheBackend.interface.php');
lmb_require('limb/core/src/lmbSerializable.class.php');

/**
 * class lmbCacheMemcacheBackend.
 *
 * @package cache
 * @version $Id: lmbCacheFilePersister.class.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class lmbCacheMemcacheBackend implements lmbCacheBackend
{
  protected $_memcache;

  function __construct($host = 'localhost', $port = '11211')
  {
    $this->_memcache = new Memcache();
    $this->_memcache->connect($host, $port);
  }

  function set($key, $value, $params = array()) 
  {
    $container = new lmbSerializable($value);

    $this->_memcache->set($key, serialize($container), null, $this->_getTtl($params));
  }

  function get($key, $params = array())
  {
    if (!$value = $this->_memcache->get($key))
      return false;
        
    $container = unserialize($value);
    return $container->getSubject();
  }

  function delete($key, $params = array())
  {
    $this->_memcache->delete($key);
  }

  function flush()
  {
    $this->_memcache->flush();
  }
  
  protected function _getTtl($params)
  {
    if (!isset($params['ttl']))
      $params['ttl'] = 0;

    return $params['ttl'];
  }
}
