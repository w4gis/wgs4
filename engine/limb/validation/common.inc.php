<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package validation
 * @version $Id: common.inc.php 6353 2007-10-01 18:05:02Z pachanga $
 */
require_once('limb/core/common.inc.php');

