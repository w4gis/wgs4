<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package view
 * @version $Id$
 */
require_once('limb/core/common.inc.php');
lmb_require(dirname(__FILE__) . '/wact.inc.php');
lmb_require(dirname(__FILE__) . '/toolkit.inc.php');


