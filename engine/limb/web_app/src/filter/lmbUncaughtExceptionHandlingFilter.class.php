<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */
lmb_require('limb/web_app/src/filter/lmbErrorHandlingFilter.class.php');

/**
 * class lmbUncaughtExceptionHandlingFilter.
 *
 * @package web_app
 * @deprecated
 * @see lmbErrorHandlingFilter
 * @version $Id: lmbUncaughtExceptionHandlingFilter.class.php 6598 2007-12-07 08:01:45Z pachanga $
 */
class lmbUncaughtExceptionHandlingFilter extends lmbErrorHandlingFilter{}


