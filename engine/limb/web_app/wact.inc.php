<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package web_app
 * @version $Id: wact.inc.php 6598 2007-12-07 08:01:45Z pachanga $
 */
@define('WACT_CACHE_DIR', LIMB_VAR_DIR . '/limb/wact/');

require_once('limb/wact/common.inc.php');


