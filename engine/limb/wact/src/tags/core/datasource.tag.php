<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * Datasources act is "namespaces" for a template.
 * @tag core:DATASOURCE
 * @convert_to_expression from
 * @package wact
 * @version $Id: datasource.tag.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class WactCoreDatasourceTag extends WactRuntimeDatasourceComponentTag
{
  protected $runtimeComponentName = 'WactDatasourceRuntimeComponent';
}

