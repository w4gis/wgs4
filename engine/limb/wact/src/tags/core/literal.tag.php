<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * Prevents a section of the template from being parsed, placing the contents
 * directly into the compiled template
 * @tag core:LITERAL
 * @forbid_parsing
 * @package wact
 * @version $Id: literal.tag.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class WactCoreLiteralTag extends WactCompilerTag
{
}

