<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * Present a named location where content can be inserted at compile time
 * @tag core:PLACEHOLDER
 * @forbid_end_tag
 * @package wact
 * @version $Id: placeholder.tag.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class WactCorePlaceHolderTag extends WactCompilerTag
{
}


