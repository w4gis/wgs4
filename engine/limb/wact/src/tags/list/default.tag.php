<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * Default List tag for a list which failed to have any contents
 * @tag list:DEFAULT
 * @parent_tag_class WactListListTag
 * @package wact
 * @version $Id: default.tag.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class WactListDefaultTag extends WactSilentCompilerTag
{
}

