<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * @tag option
 * @restrict_self_nesting
 * @parent_tag_class WactSelectTag
 * @runat client
 * @runat_as WactSelectTag
 * @package wact
 * @version $Id: option.tag.php 6243 2007-08-29 11:53:10Z pachanga $
 */
class WactOptionTag extends WactSilentCompilerTag
{
}

