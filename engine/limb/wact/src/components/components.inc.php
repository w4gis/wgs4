<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package wact
 * @version $Id: components.inc.php 6598 2007-12-07 08:01:45Z pachanga $
 */
require_once(dirname(__FILE__) . '/WactRuntimeComponent.class.php');
require_once(dirname(__FILE__) . '/WactDatasourceRuntimeComponent.class.php');
require_once(dirname(__FILE__) . '/WactRuntimeTagComponent.class.php');
require_once(dirname(__FILE__) . '/WactArrayIterator.class.php');

