<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package wact
 * @version $Id: form.inc.php 6598 2007-12-07 08:01:45Z pachanga $
 */
require_once 'limb/wact/src/components/form/error.inc.php';
require_once 'limb/wact/src/components/form/WactFormComponent.class.php';
require_once 'limb/wact/src/components/form/WactFormElementComponent.class.php';
require_once 'limb/wact/src/components/form/WactInputComponent.class.php';
require_once 'limb/wact/src/components/form/WactLabelComponent.class.php';
require_once 'limb/wact/src/components/form/WactTextAreaComponent.class.php';
require_once 'limb/wact/src/components/form/WactCheckableInputComponent.class.php';


