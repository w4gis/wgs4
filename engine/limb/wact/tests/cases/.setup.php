<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

set_include_path(dirname(__FILE__) . '/../../../' . PATH_SEPARATOR . get_include_path());
require_once(dirname(__FILE__) . '/../../common.inc.php');
require_once('limb/wact/tests/cases/WactTemplateTestCase.class.php');


