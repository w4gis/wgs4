<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package tests_runner
 * @version $Id: common.inc.php 6598 2007-12-07 08:01:45Z pachanga $
 */
set_time_limit(0);
error_reporting(E_ALL);

require_once(dirname(__FILE__) . '/simpletest.inc.php');
require_once(dirname(__FILE__) . '/src/lmbTestGroup.class.php');


