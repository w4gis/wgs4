<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */

/**
 * class lmbTestHTMLReporter.
 *
 * @package tests_runner
 * @version $Id: lmbTestHTMLReporter.class.php 6230 2007-08-10 06:03:04Z pachanga $
 */
class lmbTestHTMLReporter extends HTMLReporter{}


