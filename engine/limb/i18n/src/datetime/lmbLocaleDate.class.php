<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com 
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html 
 */
lmb_require('limb/i18n/src/datetime/lmbLocaleDateTime.class.php');

/**
 * class lmbLocaleDateTime.
 *
 * This class is OBSOLETE, use lmbLocaleDateTime instead
 *
 * @package i18n
 * @version $Id: lmbLocaleDate.class.php 6569 2007-12-03 14:41:22Z korchasa $
 */
class lmbLocaleDate extends lmbLocaleDateTime
{
}


