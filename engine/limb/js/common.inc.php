<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package js
 * @version $Id$
 */
require_once('limb/core/common.inc.php');
require_once('limb/fs/toolkit.inc.php');


