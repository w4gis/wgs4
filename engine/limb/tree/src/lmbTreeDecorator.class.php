<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package tree
 * @version $Id: lmbTreeDecorator.class.php 6598 2007-12-07 08:01:45Z pachanga $
 */
lmb_require('limb/tree/src/lmbTree.interface.php');
lmb_require('limb/core/src/lmbDecorator.class.php');
lmbDecorator :: generate('lmbTree', 'lmbTreeDecorator');


