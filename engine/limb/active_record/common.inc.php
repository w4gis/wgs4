<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package active_record
 * @version $Id: common.inc.php 6354 2007-10-01 18:05:31Z pachanga $
 */
require_once('limb/core/common.inc.php');
require_once('limb/dbal/common.inc.php');
require_once(dirname(__FILE__) . '/toolkit.inc.php');
lmb_require('limb/active_record/src/lmbActiveRecord.class.php');


