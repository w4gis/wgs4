<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package session
 * @version $Id: common.inc.php 6598 2007-12-07 08:01:45Z pachanga $
 */
require_once('limb/core/common.inc.php');
lmb_require('limb/dbal/common.inc.php');
lmb_require('limb/session/src/lmbSession.class.php');


