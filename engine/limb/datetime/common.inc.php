<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */

/**
 * @package datetime
 * @version $Id: common.inc.php 6533 2007-11-21 20:03:24Z pachanga $
 */
require_once('limb/core/common.inc.php');


