<?php
/*
 * Limb PHP Framework
 *
 * @link http://limb-project.com
 * @copyright  Copyright &copy; 2004-2007 BIT(http://bit-creative.com)
 * @license    LGPL http://www.gnu.org/copyleft/lesser.html
 */
lmb_require('limb/datetime/src/lmbDateTime.class.php');

/**
 * class lmbDateTime.
 *
 * This class is OBSOLETE, use lmbDateTime instead!
 *
 * @package datetime
 * @version $Id: lmbDate.class.php 6534 2007-11-22 07:24:57Z pachanga $
 */
class lmbDate extends lmbDateTime
{
}

