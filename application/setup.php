<?php
/**********************************************************************************
* Copyright 2004 BIT, Ltd. http://limb-project.com, mailto: support@limb-project.com
*
* Released under the LGPL license (http://www.gnu.org/copyleft/lesser.html)
***********************************************************************************
*
* $Id: setup.php 4552 2006-12-14 10:03:02Z serega $
*
***********************************************************************************/
@define('LPKG_CORE_DIR', dirname(__FILE__) . '/../engine/limb/core');
@define('LIMB_VAR_DIR', dirname(__FILE__) . '/cache/var/');

require_once(dirname(__FILE__) . '/settings/projects.conf.php');
require_once(dirname(__FILE__) . '/settings/wgs.conf.php');
require_once(dirname(__FILE__) . '/settings/mg.conf.php');

@define('PROJECT_PATH', dirname(__FILE__) . '/projects/' . getProjectName());

require_once(PROJECT_PATH .'/setup.php');

set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
set_include_path(WGS3_ENGINE_DIR . PATH_SEPARATOR . get_include_path());

require_once('limb/core/common.inc.php');
require_once('limb/web_app/common.inc.php');

if (!empty($_SERVER['REDIRECT_URL']))
    $_SERVER['REQUEST_URI'] = $_SERVER['REDIRECT_URL'].((isset($_SERVER['REDIRECT_QUERY_STRING']))? ('?'.$_SERVER['REDIRECT_QUERY_STRING']): '');

?>
