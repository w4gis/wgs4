<?php

$conf = array(
    // BackEnd
    'backend.info' => array('path' => 'info/:type'),
    //'backend.map' => array('path' => 'map/:viewertype/:favoriteview'),
    'backend.map.object' => array('path' => 'map/object/:object'),
    //'backend.map' => array('path' => 'map/:favoriteview'),
    'backend.message' => array('path' => 'message/:code'),
    // Components        
    'component.map' => array('path' => 'component/:section'),
);

?>