--PROJECT_SDB
create or replace procedure project_sdb.GrantView(viewName in varchar2, schemaName in varchar2) is begin execute immediate 'grant insert, update, delete on project_sdb.'|| viewName ||' to '||schemaName||''; end GrantView;/
grant execute on PROJECT_SDB.GRANTVIEW to PROJECT_USER/