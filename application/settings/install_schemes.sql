-- PROJECT_WDB

create user PROJECT_WDB
  identified by "projweb"
  default tablespace GIS
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on gis;

grant wgs3_role to PROJECT_WDB;

-- PROJECT_SDB

create user PROJECT_SDB
  identified by "projspa"
  default tablespace GIS
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on gis;

grant wgs3_role to PROJECT_SDB;
-- ����������� ��� �������� ��������
grant create any index to PROJECT_SDB;
grant create any sequence to PROJECT_SDB;
grant create any table to PROJECT_SDB;

-- PROJECT_ADB

create user PROJECT_ADB
  identified by "projatt"
  default tablespace GIS
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on gis;

grant wgs3_role to PROJECT_ADB;

-- PROJECT_USER

create user PROJECT_USER
  identified by "projuser"
  default tablespace GIS
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on gis;
  
grant wgs3_role to PROJECT_USER;
CREATE SEQUENCE PROJECT_ADB.ADBSDBRELATION_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_SDB.ENTITYIDSEQUENCE 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 CACHE 20
;
CREATE SEQUENCE PROJECT_SDB.FDO_UNIQUE_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.FEATUREDOC_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.FEATURELAYERGROUP_ID_SEQ 
 INCREMENT BY 1 
 START WITH 2 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.FEATURELAYER_ID_SEQ 
 INCREMENT BY 1 
 START WITH 2 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.FEATURETYPEFORK_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_SDB.FEATURE_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.LAYERLIMITATION_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.MGCUSTOMCOMMAND_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.MGLAYER_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.MGMAPLAYER_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.MGMAP_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.MGWEBLAYOUT_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.OBJECT_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.OBJECT_RELATION_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.OBJECT_TYPE_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.OBJTYPE_RELATION_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.PROPERTY_GROUP_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 0
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.PROPERTY_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.RELATION_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.VALUE_DOMAIN_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_ADB.VALUE_PROPERTY_TYPE_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.WGSOBJECT_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.WGSROLE_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE SEQUENCE PROJECT_WDB.WGSUSER_ID_SEQ 
 INCREMENT BY 1 
 START WITH 1 
 MINVALUE 1
 NOCACHE
;
CREATE TABLE PROJECT_ADB.FEATURE_OBJECT (
       ADBSDBRELATION_ID    NUMBER(10) NOT NULL,
       OBJECT_ID            NUMBER(10) NOT NULL,
       FEATURE_ID           NUMBER(10) NULL,
       Geometry_Hash        VARCHAR2(100) NULL,
       FEATURELAYER_ID      NUMBER(10) NULL,
       is_current           NUMBER(1) NULL,
       UNBIND_DATE          DATE NULL,
       UNBIND_CUSTOM_DATE   DATE NULL,
       PRIMARY KEY (ADBSDBRELATION_ID)
);


CREATE TABLE PROJECT_ADB.FEATUREDOC (
       DOC_ID               NUMBER(10) NOT NULL,
       FEATURE_ID           NUMBER(10) NOT NULL,
       DOCUMENT             BLOB NULL,
       DOC_NAME             VARCHAR2(255) NOT NULL,
       DOC_TYPE             VARCHAR2(255) NOT NULL,
       GEOMETRY_HASH        VARCHAR2(100) NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       DOC_CACHE_NAME       VARCHAR2(255) NOT NULL,
       STORE_TYPE           NUMBER(10) default 0 not null,
       PRIMARY KEY (DOC_ID)
);


CREATE TABLE PROJECT_ADB.FEATURELAYER_OBJECT_TYPE (
       FEATURELAYER_ID      NUMBER(10) NOT NULL,
       OBJECT_TYPE_ID       NUMBER(10) NOT NULL,
       IS_CURRENT           NUMBER(1) NULL,
       PRIMARY KEY (FEATURELAYER_ID, OBJECT_TYPE_ID)
);


CREATE TABLE PROJECT_ADB.OBJECT (
       OBJECT_ID            NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       inventary_number     VARCHAR2(255) NULL,
       object_type_id       NUMBER(10) NOT NULL,
       create_date          DATE NULL,
       author               VARCHAR2(255) NULL,
       note                 VARCHAR2(255) NULL,
       label                VARCHAR2(255) NULL,
       SHORT_NAME           VARCHAR2(50) NULL,
       EDIT_USER            VARCHAR2(255) NULL,
       PRIMARY KEY (OBJECT_ID)
);


CREATE TABLE PROJECT_ADB.OBJECT_RELATION (
       Object_relation_id   NUMBER(10) NOT NULL,
       child_object_id      NUMBER(10) NOT NULL,
       parent_object_id     NUMBER(10) NOT NULL,
       ObjType_relation_id  NUMBER(10) NULL,
       PRIMARY KEY (Object_relation_id)
);


CREATE TABLE PROJECT_ADB.OBJECT_TYPE (
       Object_type_id       NUMBER(10) NOT NULL,
       Name                 VARCHAR2(255) NOT NULL,
       Short_Name           VARCHAR2(255) NULL,
       Add_DATE             DATE NULL,
       Note                 VARCHAR2(1000) NULL,
       is_child             NUMBER(3) NULL,
       is_abstract          NUMBER(3) NULL,
       LABEL                NUMBER(10) NULL,
       PRIMARY KEY (Object_type_id)
);


CREATE TABLE PROJECT_ADB.Object_Type_Relation (
       ObjType_relation_id  NUMBER(10) NOT NULL,
       Parent_object_type_id NUMBER(10) NOT NULL,
       Child_object_type_id NUMBER(10) NOT NULL,
       Relation_id          NUMBER(10) NULL,
       PRIMARY KEY (ObjType_relation_id)
);


CREATE TABLE PROJECT_ADB.opValue_Current (
       object_id            NUMBER(10) NOT NULL,
       PROPERTY_ID          NUMBER(10) NOT NULL,
       Disband_Date         DATE NOT NULL,
       Value_String         VARCHAR2(500) NULL,
       Value_Numeric        NUMBER(10) NULL,
       Value_Real           NUMBER(15,2) NULL,
       Value_Date           DATE NULL,
       Value_Domain         NUMBER(10) NULL,
       Value_ObjectID       NUMBER(10) NULL,
       Author_ID            NUMBER(10) NULL,
       VALUE_TEXT           VARCHAR2(3500) NULL,
       PRIMARY KEY (object_id, PROPERTY_ID)
);


CREATE TABLE PROJECT_ADB.Properties_PValue_Domain (
       Value_Domain_ID      NUMBER(10) NOT NULL,
       PROPERTY_ID          NUMBER(10) NOT NULL,
       PRIMARY KEY (Value_Domain_ID, PROPERTY_ID)
);


CREATE TABLE PROJECT_ADB.PROPERTY (
       PROPERTY_ID          NUMBER(10) NOT NULL,
       Name                 VARCHAR2(100) NULL,
       Placement_Date       DATE NULL,
       Value_Properties_Type_ID NUMBER(10) NOT NULL,
       Properties_Group_ID  NUMBER(10) NULL,
       CAD_NAME             VARCHAR2(100) NULL,
       PRIMARY KEY (PROPERTY_ID)
);


CREATE TABLE PROJECT_ADB.Property_Group (
       Property_Group_ID    NUMBER(10) NOT NULL,
       Name                 VARCHAR2(255) NULL,
       PRIMARY KEY (Property_Group_ID)
);


CREATE TABLE PROJECT_ADB.PROPERTYPRIORITY (
       object_type_id       NUMBER(10) NOT NULL,
       PROPERTY_ID          NUMBER(10) NOT NULL,
       Group_Priority       NUMBER(10) NULL,
       Priority             NUMBER(10) NULL,
       ACad_view            NUMBER(1) NULL,
       PRIMARY KEY (object_type_id, PROPERTY_ID)
);


CREATE TABLE PROJECT_ADB.pValue_Domain (
       Value_Domain_ID      NUMBER(10) NOT NULL,
       Value_domain         VARCHAR2(255) NULL,
       PRIMARY KEY (Value_Domain_ID)
);


CREATE TABLE PROJECT_ADB.pValue_Type (
       Value_Property_Type_ID NUMBER(10) NOT NULL,
       Name                 VARCHAR2(255) NULL,
       PRIMARY KEY (Value_Property_Type_ID)
);


CREATE TABLE PROJECT_ADB.RELATION (
       Relation_id          NUMBER(10) NOT NULL,
       RELATION_TYPE_ID     NUMBER(10) NULL,
       Name                 VARCHAR2(255) NULL,
       PRIMARY KEY (Relation_id)
);


CREATE TABLE PROJECT_ADB.Relation_Property (
       PROPERTY_ID          NUMBER(10) NOT NULL,
       Relation_id          NUMBER(10) NOT NULL,
       priority             NUMBER(10) NULL,
       PRIMARY KEY (PROPERTY_ID, Relation_id)
);


CREATE TABLE PROJECT_ADB.Relation_PropertyValue (
       PROPERTY_ID          NUMBER(10) NOT NULL,
       Object_relation_id   NUMBER(10) NOT NULL,
       Value_String         VARCHAR2(1000) NULL,
       Value_Numeric        NUMBER(10) NULL,
       Value_Real           NUMBER(15,2) NULL,
       Value_Date           DATE NULL,
       Value_Domain         NUMBER(10) NULL,
       VALUE_TEXT           VARCHAR2(3500) NULL,
       PRIMARY KEY (PROPERTY_ID, Object_relation_id)
);


CREATE TABLE PROJECT_ADB.RELATION_TYPE (
       RELATION_TYPE_ID     NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       PRIMARY KEY (RELATION_TYPE_ID)
);


CREATE TABLE PROJECT_SDB.ADMPIMPORTSETTINGS (
       NAME                 VARCHAR2(255) NOT NULL,
       USERNAME             VARCHAR2(255) NULL,
       SETTINGS             BLOB NULL,
       PRIMARY KEY (NAME)
);


CREATE TABLE PROJECT_SDB.ADMPLOCKDATA (
       LOCKID               NUMBER NOT NULL,
       NTUSER               VARCHAR2(255) NULL,
       ORACLEUSER           VARCHAR2(255) NULL,
       ADMPUSER             VARCHAR2(255) NULL,
       COMPUTERNAME         VARCHAR2(255) NULL,
       LOGINDATE            DATE NULL,
       PRIMARY KEY (LOCKID)
);


CREATE TABLE PROJECT_SDB.ADMPMETADATADYNAMIC (
       ADMP_ORA_COLNAME     VARCHAR2(30) NULL,
       ADMP_DIRECTION       NUMBER(2) NULL,
       ADMP_ORA_TAG         NUMBER(3) NULL,
       ADMP_ORA_TYPE        NUMBER(3) NULL,
       ADMP_ORA_SIZE        NUMBER(10) NULL,
       ADMP_ORA_SCALE       NUMBER(10) NULL,
       ADMP_ACAD_TAG        NUMBER(10) NULL,
       ADMP_ACAD_DATA       VARCHAR2(255) NULL,
       ADMP_ACAD_FIELD      VARCHAR2(255) NULL,
       ADMP_ACAD_TYPE       NUMBER(3) NULL,
       ADMP_ACAD_SIZE       NUMBER(10) NULL,
       ADMP_ACAD_SCALE      NUMBER(10) NULL,
       ADMP_ACAD_DEFAULTVALUE VARCHAR2(255) NULL,
       FEATURELAYER_ID      NUMBER(10) NOT NULL,
       PROPERTY_ID          NUMBER(10) NULL
);


CREATE TABLE PROJECT_SDB.ADMPMETADATASTATIC (
       ADMP_ORA_COLNAME     VARCHAR2(30) NULL,
       ADMP_DIRECTION       NUMBER(2) NULL,
       ADMP_ORA_TAG         NUMBER(3) NULL,
       ADMP_ORA_TYPE        NUMBER(3) NULL,
       ADMP_ORA_SIZE        NUMBER(10) NULL,
       ADMP_ORA_SCALE       NUMBER(10) NULL,
       ADMP_ACAD_TAG        NUMBER(10) NULL,
       ADMP_ACAD_DATA       VARCHAR2(255) NULL,
       ADMP_ACAD_FIELD      VARCHAR2(255) NULL,
       ADMP_ACAD_TYPE       NUMBER(3) NULL,
       ADMP_ACAD_SIZE       NUMBER(10) NULL,
       ADMP_ACAD_SCALE      NUMBER(10) NULL,
       ADMP_ACAD_DEFAULTVALUE VARCHAR2(255) NULL
);


CREATE TABLE PROJECT_SDB.ADMPOPTIONS (
       OPTIONS_NAME         VARCHAR2(255) NOT NULL,
       OPTIONS_VALUE        VARCHAR2(255) NULL,
       PRIMARY KEY (OPTIONS_NAME)
);


CREATE TABLE PROJECT_SDB.ADMPOSEMETADATA (
       ADMP_FEATURENAME     VARCHAR2(255) NOT NULL,
       ADMP_INTERPMETHOD    NUMBER NULL,
       ADMP_MAPCS           VARCHAR2(255) NULL,
       ADMP_EXTENTS         MDSYS.SDO_GEOMETRY NULL
);


CREATE TABLE PROJECT_SDB.FDOMETADATA (
       FDO_UNIQUE_ID        NUMBER(10) NOT NULL,
       FDO_ORA_OWNER        VARCHAR2(64) NULL,
       FDO_ORA_NAME         VARCHAR2(64) NULL,
       FDO_ORA_GEOMCOLUMN   VARCHAR2(1024) NULL,
       FDO_SPATIALTABLE_OWNER VARCHAR2(64) NULL,
       FDO_SPATIALTABLE_NAME VARCHAR2(64) NULL,
       FDO_SPATIALTABLE_GEOMCOLUMN VARCHAR2(1024) NULL,
       FDO_CLASS_NAME       VARCHAR2(256) NULL,
       FDO_SRID             NUMBER NULL,
       FDO_DIMINFO          MDSYS.SDO_DIM_ARRAY NULL,
       FDO_CS_NAME          VARCHAR2(256) NULL,
       FDO_WKTEXT           VARCHAR2(2046) NULL,
       FDO_LAYER_GTYPE      VARCHAR2(64) NULL,
       FDO_SEQUENCE_NAME    VARCHAR2(64) NULL,
       FDO_IDENTITY         VARCHAR2(1024) NULL,
       FDO_SDO_ROOT_MBR     MDSYS.SDO_GEOMETRY NULL,
       FDO_POINT_X_COLUMN   VARCHAR2(128) NULL,
       FDO_POINT_Y_COLUMN   VARCHAR2(128) NULL,
       FDO_POINT_Z_COLUMN   VARCHAR2(128) NULL,
       FDO_SPATIAL_CONTEXT  VARCHAR2(128) NULL
);


CREATE TABLE PROJECT_SDB.FEATURETABLE (
       ENTITYID             NUMBER(10) NOT NULL,
       ADMPBLOCKNAME        VARCHAR2(255 BYTE) NULL,
       ADMPBLOCKSCALE       MDSYS.SDO_POINT_TYPE NULL,
       ADMPCLOSED           CHAR(1 BYTE) NULL,
       ADMPCOLOR            NUMBER(3) NULL,
       ADMPENTITYTYPE       VARCHAR2(30 BYTE) NULL,
       ADMPHYPERLINK        VARCHAR2(255 BYTE) NULL,
       ADMPJUSTIFICATION    NUMBER NULL,
       ADMPLAYER            VARCHAR2(255 BYTE) NULL,
       ADMPLINETYPE         VARCHAR2(255 BYTE) NULL,
       ADMPLINETYPESCALE    NUMBER NULL,
       ADMPLINEWEIGHT       NUMBER NULL,
       ADMPNORMAL           MDSYS.SDO_POINT_TYPE NULL,
       ADMPOLYPATCOLOR      NUMBER NULL,
       ADMPOLYPATTYPE       VARCHAR2(255 BYTE) NULL,
       ADMPPLOTSTYLE        VARCHAR2(255 BYTE) NULL,
       ADMPROTATION         NUMBER NULL,
       ADMPTEXTCONTENT      VARCHAR2(512 BYTE) NULL,
       ADMPTEXTHEIGHT       NUMBER NULL,
       ADMPTEXTSTYLE        VARCHAR2(255 BYTE) NULL,
       ADMPTHICKNESS        NUMBER NULL,
       ADMPWIDTH            NUMBER NULL,
       GEOMETRY             MDSYS.SDO_GEOMETRY NULL,
       LOCKID               NUMBER NULL,
       VERSIONNUMBER        NUMBER NULL,
       FEATURE_ID           NUMBER(10) NULL,
       FEATURELAYER_ID      NUMBER(10) NOT NULL,
       ARCGEOMETRY          MDSYS.SDO_GEOMETRY NULL,
       GEOMETRYHASH         VARCHAR2(100) NULL,
       VERSION              NUMBER(10) DEFAULT 0 NOT NULL,
       CREATED              DATE NULL,
       AUTHOR               VARCHAR2(255) NULL,
       ISOFFLINE            NUMBER(1) DEFAULT 0 NOT NULL,
       ONLINEDATE           DATE NULL,
       OFFLINEDATE          DATE NULL,
       LABEL                VARCHAR2(255 BYTE) NULL,
       PRIMARY KEY (ENTITYID)
)
PARTITION BY RANGE (FEATURELAYER_ID)
(
PARTITION p0
VALUES LESS THAN (1)
    );


CREATE TABLE PROJECT_SDB.SDOGEOMMETADATA (
       TABLE_NAME           VARCHAR2(32) NOT NULL,
       COLUMN_NAME          VARCHAR2(1024) NULL,
       DIMINFO              MDSYS.SDO_DIM_ARRAY NULL,
       SRID                 NUMBER NULL,
       PRIMARY KEY (TABLE_NAME)
);


CREATE TABLE PROJECT_WDB.FAVORITEVIEW (
       NUM                  NUMBER(10) NOT NULL,
       WGSUSER_ID           NUMBER(10) NOT NULL,
       MGWEBLAYOUT_ID       NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       CENTER               VARCHAR2(255) NULL,
       SCALE                VARCHAR2(255) NULL,
       PICTURE              BLOB NULL,
       CACHE_NAME           VARCHAR2(255) NULL,
       LAYER_VIEW           VARCHAR2(4000) NULL,
       PRIMARY KEY (NUM, WGSUSER_ID, MGWEBLAYOUT_ID)
);


CREATE TABLE PROJECT_WDB.FEATURELAYER (
       FEATURELAYER_ID      NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       VIEW_NAME            VARCHAR2(255) NULL,
       FEATURELAYERGROUP_ID NUMBER(10) NOT NULL,
       PRIMARY KEY (FEATURELAYER_ID)
);


ALTER TABLE PROJECT_WDB.FEATURELAYER
       ADD  UNIQUE (NAME);


CREATE TABLE PROJECT_WDB.FEATURELAYER_MGLAYER (
       MGLAYER_ID           NUMBER(10) NOT NULL,
       FEATURELAYER_ID      NUMBER(10) NOT NULL,
       PRIMARY KEY (MGLAYER_ID, FEATURELAYER_ID)
);


CREATE TABLE PROJECT_WDB.FEATURELAYERGROUP (
       FEATURELAYERGROUP_ID NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       PRIMARY KEY (FEATURELAYERGROUP_ID)
);


CREATE TABLE PROJECT_WDB.FEATURETYPEASSOCIATION (
       FEATURETYPEFORK_ID   NUMBER(10) NOT NULL,
       FEATURETYPE1_ID      NUMBER(10) NOT NULL,
       FEATURETYPE2_ID      NUMBER(10) NOT NULL,
       PRIMARY KEY (FEATURETYPEFORK_ID, FEATURETYPE1_ID, 
              FEATURETYPE2_ID)
);


CREATE TABLE PROJECT_WDB.FEATURETYPEFORK (
       FEATURETYPEFORK_ID   NUMBER(10) NOT NULL,
       FEATURELAYER_ID      NUMBER(10) NULL,
       FEATURETYPEFORK_HASH VARCHAR2(100) NULL,
       STARTELEMENT         NUMBER(10) NULL,
       ENDELEMENT           NUMBER(10) NULL,
       PRIMARY KEY (FEATURETYPEFORK_ID)
);


CREATE TABLE PROJECT_WDB.LAYERLIMITATION (
       LAYERLIMITATION_ID   NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       WGSROLE_ID           NUMBER(10) NOT NULL,
       FEATURELAYER_ID      NUMBER(10) NULL,
       MGMAPLAYER_ID        NUMBER(10) NULL,
       RANGE_FROM           NUMBER(10) NULL,
       RANGE_TO             NUMBER(10) NULL,
       REGION               MDSYS.SDO_GEOMETRY NULL,
       PRIMARY KEY (LAYERLIMITATION_ID)
);


CREATE TABLE PROJECT_WDB.MGBUILTINCOMMAND (
       MGBUILTCOMMAND_ID    NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       LABEL                VARCHAR2(255) NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       PRIMARY KEY (MGBUILTCOMMAND_ID)
);


CREATE TABLE PROJECT_WDB.MGCUSTOMCOMMAND (
       MGCUSTOMCOMMAND_ID   NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       LABEL                VARCHAR2(255) NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       INVOKESCRIPT         VARCHAR2(4000) NULL,
       MGWEBLAYOUT_ID       NUMBER(10) NULL,
       IMAGEURL             VARCHAR2(255) NULL,
       PRIMARY KEY (MGCUSTOMCOMMAND_ID)
);


CREATE TABLE PROJECT_WDB.MGLAYER (
       MGLAYER_ID           NUMBER(10) NOT NULL,
       RESOURCE_ID          VARCHAR2(4000) NULL,
       PRIMARY KEY (MGLAYER_ID)
);


CREATE TABLE PROJECT_WDB.MGMAP (
       MGMAP_ID             NUMBER(10) NOT NULL,
       RESOURCE_ID          VARCHAR2(4000) NULL,
       PRIMARY KEY (MGMAP_ID)
);


CREATE TABLE PROJECT_WDB.MGMAP_MGLAYER (
       MGMAPLAYER_ID        NUMBER(10) NOT NULL,
       MGLAYER_ID           NUMBER(10) NOT NULL,
       MGMAP_ID             NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       LEGEND               VARCHAR2(255) NULL,
       FEATURELAYER_ID      NUMBER(10) NULL,
       PRIMARY KEY (MGMAPLAYER_ID)
);


CREATE TABLE PROJECT_WDB.MGPANE (
       MGPANE_ID            NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       LABEL                VARCHAR2(255) NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       PRIMARY KEY (MGPANE_ID)
);


CREATE TABLE PROJECT_WDB.MGWEBLAYOUT (
       MGWEBLAYOUT_ID       NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       RESOURCE_ID          VARCHAR2(4000) NULL,
       MGMAP_ID             NUMBER(10) NULL,
       BYDEFAULT            NUMBER(10) DEFAULT 0 NULL,
       OVERVIEW             NUMBER(1) DEFAULT 0 NULL,
       PRIMARY KEY (MGWEBLAYOUT_ID)
);


CREATE TABLE PROJECT_WDB.WGSOBJECT (
       WGSOBJECT_ID         NUMBER(10) NOT NULL,
       MGWEBLAYOUT_ID       NUMBER(10) NULL,
       MGPANE_ID            NUMBER(10) NULL,
       MGCUSTOMCOMMAND_ID   NUMBER(10) NULL,
       MGBUILTCOMMAND_ID    NUMBER(10) NULL,
       FEATURELAYER_ID      NUMBER(10) NULL,
       MGMAPLAYER_ID        NUMBER(10) NULL,
       WGSOBJECTTYPE_ID     NUMBER(10) NOT NULL,
       FEATURETYPEFORK_ID   NUMBER(10) NULL,
       LOCKED               NUMBER(1) NULL,
       PRIMARY KEY (WGSOBJECT_ID)
);


CREATE TABLE PROJECT_WDB.WGSOBJECTPERMISSION (
       WGSROLE_ID           NUMBER(10) NOT NULL,
       WGSOBJECT_ID         NUMBER(10) NOT NULL,
       PRIVILEGE_ID         NUMBER(10) NOT NULL,
       ALLOW                NUMBER(1) NOT NULL,
       PRIMARY KEY (WGSROLE_ID, WGSOBJECT_ID, PRIVILEGE_ID)
);


CREATE TABLE PROJECT_WDB.WGSROLE (
       WGSROLE_ID           NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       LOCKED               NUMBER(1) NOT NULL,
       PRIMARY KEY (WGSROLE_ID)
);


CREATE TABLE PROJECT_WDB.WGSROLE_SYSROLE (
       WGSROLE_ID           NUMBER(10) NOT NULL,
       SYSROLE_ID           NUMBER(10) NOT NULL,
       PRIMARY KEY (WGSROLE_ID, SYSROLE_ID)
);


CREATE TABLE PROJECT_WDB.WGSROLE_WGSUSER (
       WGSROLE_ID           NUMBER(10) NOT NULL,
       WGSUSER_ID           NUMBER(10) NOT NULL,
       PRIMARY KEY (WGSROLE_ID, WGSUSER_ID)
);


CREATE TABLE PROJECT_WDB.WGSROLERELATION (
       PARENTROLE_ID        NUMBER(10) NOT NULL,
       CHILDROLE_ID         NUMBER(10) NOT NULL,
       PRIMARY KEY (PARENTROLE_ID, CHILDROLE_ID)
);


CREATE TABLE PROJECT_WDB.WGSUSER (
       WGSUSER_ID           NUMBER(10) NOT NULL,
       NAME                 VARCHAR2(255) NOT NULL,
       PASSWORD             VARCHAR2(255) NOT NULL,
       LOCKED               NUMBER(1) DEFAULT 0 NULL,
       DESCRIPTION          VARCHAR2(255) NULL,
       PRIMARY KEY (WGSUSER_ID)
);

-- WGS3_ROLE
grant select on PROJECT_WDB.WGSROLE to WGS3_ROLE;
grant select on PROJECT_WDB.WGSUSER to WGS3_ROLE;
grant select, insert, update, delete on PROJECT_WDB.WGSROLE_SYSROLE to WGS3_ROLE;
grant insert on PROJECT_WDB.MGBUILTINCOMMAND to WGS3_ROLE;
grant insert on PROJECT_WDB.MGPANE to WGS3_ROLE;
grant insert on PROJECT_ADB.PVALUE_TYPE to WGS3_ROLE;
grant insert on PROJECT_ADB.RELATION_TYPE to WGS3_ROLE;
grant insert on PROJECT_SDB.ADMPMETADATASTATIC to WGS3_ROLE;
grant insert on PROJECT_SDB.ADMPOPTIONS to WGS3_ROLE;

-- PROJECT_USER
grant insert, delete on MDSYS.ALL_SDO_GEOM_METADATA to PROJECT_USER;
grant select on PROJECT_ADB.ADBSDBRELATION_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.FEATUREDOC to PROJECT_USER;
grant select on PROJECT_ADB.FEATUREDOC_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.FEATURELAYER_OBJECT_TYPE to PROJECT_USER;
grant select, insert, update, delete, references, alter on PROJECT_ADB.FEATURE_OBJECT to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.OBJECT to PROJECT_USER;
grant select on PROJECT_ADB.OBJECT_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.OBJECT_RELATION to PROJECT_USER;
grant select on PROJECT_ADB.OBJECT_RELATION_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.OBJECT_TYPE to PROJECT_USER;
grant select on PROJECT_ADB.OBJECT_TYPE_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.OBJECT_TYPE_RELATION to PROJECT_USER;
grant select on PROJECT_ADB.OBJTYPE_RELATION_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete, references on PROJECT_ADB.OPVALUE_CURRENT to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.PROPERTIES_PVALUE_DOMAIN to PROJECT_USER;
grant select, insert, update, delete, references on PROJECT_ADB.PROPERTY to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.PROPERTY_GROUP to PROJECT_USER;
grant select on PROJECT_ADB.PROPERTY_GROUP_ID_SEQ to PROJECT_USER;
grant select on PROJECT_ADB.PROPERTY_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.PROPERTYPRIORITY to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.PVALUE_DOMAIN to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.RELATION to PROJECT_USER;
grant select on PROJECT_ADB.RELATION_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.RELATION_PROPERTY to PROJECT_USER;
grant select, insert, update, delete on PROJECT_ADB.RELATION_PROPERTYVALUE to PROJECT_USER;
grant select on PROJECT_ADB.RELATION_TYPE to PROJECT_USER;
grant select on PROJECT_ADB.VALUE_DOMAIN_ID_SEQ to PROJECT_USER;
grant select on PROJECT_SDB.ADMPIMPORTSETTINGS to PROJECT_USER;
grant select, insert, update, delete on PROJECT_SDB.ADMPMETADATADYNAMIC to PROJECT_USER;
grant select on PROJECT_SDB.ADMPMETADATASTATIC to PROJECT_USER;
grant select on PROJECT_SDB.ENTITYIDSEQUENCE to PROJECT_USER;
grant insert, update on PROJECT_SDB.FDOMETADATA to PROJECT_USER;
grant select on PROJECT_SDB.FDO_UNIQUE_ID_SEQ to PROJECT_USER;
grant select on PROJECT_SDB.FEATURE_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete, references, alter on PROJECT_SDB.FEATURETABLE to PROJECT_USER;
grant select, insert, update, delete on PROJECT_SDB.SDOGEOMMETADATA to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FAVORITEVIEW to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FEATURELAYER to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FEATURELAYERGROUP to PROJECT_USER;
grant select on PROJECT_WDB.FEATURELAYERGROUP_ID_SEQ to PROJECT_USER;
grant select on PROJECT_WDB.FEATURELAYER_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FEATURELAYER_MGLAYER to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FEATURETYPEASSOCIATION to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.FEATURETYPEFORK to PROJECT_USER;
grant select on PROJECT_WDB.FEATURETYPEFORK_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.LAYERLIMITATION to PROJECT_USER;
grant select on PROJECT_WDB.LAYERLIMITATION_ID_SEQ to PROJECT_USER;
grant select on PROJECT_WDB.MGBUILTINCOMMAND to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.MGCUSTOMCOMMAND to PROJECT_USER;
grant select on PROJECT_WDB.MGCUSTOMCOMMAND_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.MGLAYER to PROJECT_USER;
grant select on PROJECT_WDB.MGLAYER_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.MGMAP to PROJECT_USER;
grant select on PROJECT_WDB.MGMAP_ID_SEQ to PROJECT_USER;
grant select on PROJECT_WDB.MGMAPLAYER_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.MGMAP_MGLAYER to PROJECT_USER;
grant select on PROJECT_WDB.MGPANE to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.MGWEBLAYOUT to PROJECT_USER;
grant select on PROJECT_WDB.MGWEBLAYOUT_ID_SEQ to PROJECT_USER;
grant select, insert on PROJECT_WDB.WGSOBJECT to PROJECT_USER;
grant select on PROJECT_WDB.WGSOBJECT_ID_SEQ to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.WGSOBJECTPERMISSION to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.WGSROLE to PROJECT_USER;
grant select on PROJECT_WDB.WGSROLE_ID_SEQ to PROJECT_USER;
grant select, insert, delete on PROJECT_WDB.WGSROLERELATION to PROJECT_USER;
grant select, insert, delete on PROJECT_WDB.WGSROLE_SYSROLE to PROJECT_USER;
grant select, insert, delete on PROJECT_WDB.WGSROLE_WGSUSER to PROJECT_USER;
grant select, insert, update, delete on PROJECT_WDB.WGSUSER to PROJECT_USER;
grant select on PROJECT_WDB.WGSUSER_ID_SEQ to PROJECT_USER;
grant select on WGS3.ACLREVISION to PROJECT_USER;
grant select on WGS3.SYSPROJECT to PROJECT_USER;
grant select, insert on PROJECT_ADB.PVALUE_TYPE to PROJECT_USER;
grant select, insert on PROJECT_ADB.RELATION_TYPE to PROJECT_USER;
grant select, insert, update, delete on WGS3.TRANSACTIONS to PROJECT_USER;

-- PROJECT_ADB
grant references on PROJECT_WDB.FEATURELAYER to PROJECT_ADB;

-- PROJECT_WDB
grant references on PROJECT_ADB.OBJECT_TYPE to PROJECT_WDB;
grant select, references on WGS3.PRIVILEGE to PROJECT_WDB with grant option;
grant references on WGS3.SYSROLE to PROJECT_WDB;
grant select, references on WGS3.WGSOBJECTTYPE to PROJECT_WDB with grant option;

-- PROJECT_SDB
grant select on PROJECT_ADB.FEATURELAYER_OBJECT_TYPE to PROJECT_SDB;
grant select, insert, update, delete on PROJECT_ADB.FEATURE_OBJECT to PROJECT_SDB with grant option;
grant select, insert, update, delete on PROJECT_ADB.OBJECT to PROJECT_SDB with grant option;
grant select on PROJECT_ADB.OBJECT_ID_SEQ to PROJECT_SDB;
grant select, insert, update, delete on PROJECT_ADB.OPVALUE_CURRENT to PROJECT_SDB with grant option;
grant select, insert, update, delete, references on PROJECT_ADB.PROPERTY to PROJECT_SDB with grant option;
grant select on PROJECT_ADB.PROPERTYPRIORITY to PROJECT_SDB;
grant select, references on PROJECT_WDB.FEATURELAYER to PROJECT_SDB with grant option;
grant select on PROJECT_WDB.FEATURELAYERGROUP to PROJECT_SDB with grant option;
grant select on WGS3.SYSPROJECT to PROJECT_SDB;

-- PROJECT_USER
grant execute on WGS3LOGIC.ARRAY to PROJECT_USER;
grant execute on WGS3LOGIC.FAVORITEVIEWSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.FEATUREDOCSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.FEATURELAYER to PROJECT_USER;
grant execute on WGS3LOGIC.FEATURELAYERGROUP to PROJECT_USER;
grant execute on WGS3LOGIC.FEATURESERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.FEATURETYPEFORK to PROJECT_USER;
grant execute on WGS3LOGIC.MGLAYERSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.MGMAPSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.MGWEBLAYOUTSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.MGRESOURCESERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.OBJECT to PROJECT_USER;
grant execute on WGS3LOGIC.OBJECTTYPE to PROJECT_USER;
grant execute on WGS3LOGIC.OBJECTTYPERELATION to PROJECT_USER;
grant execute on WGS3LOGIC.PROJECTOBJECTSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.PROJECTPERMISSIONSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.PROJECTROLESERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.PROJECTUSERSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.PROPERTY to PROJECT_USER;
grant execute on WGS3LOGIC.SDBINDEX to PROJECT_USER;
grant execute on WGS3LOGIC.SEARCHSERVICE to PROJECT_USER;
grant execute on WGS3LOGIC.SYSUTILS to PROJECT_USER;

-- WGS3LOGIC
grant select, insert, update, delete, references on PROJECT_ADB.FEATUREDOC to WGS3LOGIC;
grant select, insert, update, delete on PROJECT_WDB.FAVORITEVIEW to WGS3LOGIC;
create or replace view project_wdb.objects_view as
select

-- WGSOBJECT
t.wgsobject_id          wgsobject_id,
t.locked                wgsobject_locked,
t.wgsobjecttype_id      wgsobjecttype_id,
t9.name                 wgsobjecttype_name,

-- MGWEBLAYOUT
t1.mgweblayout_id       mgweblayout_id,
t1.name                 mgweblayout_name,
t1.mgmap_id             mgweblayout_mgmap_id,
t1.resource_id          mgweblayout_resource_id,
t1.bydefault            mgweblayout_bydefault,
t1.overview             mgweblayout_overview,

-- MGPANE
t2.mgpane_id            mgpane_id,
t2.name                 mgpane_name,
t2.label                mgpane_label,
t2.description          mgpane_description,
t.mgweblayout_id        mgpane_mgweblayout_id,
t1.name                 mgpane_weblayout_name,

-- MGCUSTOMCOMMAND
t3.mgcustomcommand_id   mgcustomcommand_id,
t3.name                 mgcustomcommand_name,
t3.label                mgcustomcommand_label,
t3.description          mgcustomcommand_description,
t3.invokescript         mgcustomcommand_invokescript,
t3.mgweblayout_id       mgcustomcommand_mgweblayout_id,
t1.name                 mgcustomcommand_weblayout_name,

-- MGBUILTCOMMAND
t4.mgbuiltcommand_id    mgbuiltcommand_id,
t4.name                 mgbuiltcommand_name,
t4.label                mgbuiltcommand_label,
t4.description          mgbuiltcommand_description,
t.mgweblayout_id        mgbuiltcommand_mgweblayout_id,
t1.name                 mgbuiltcommand_weblayout_name,

-- FEATURELAYER
t5.featurelayer_id      featurelayer_id,
t5.name                 featurelayer_name,
t5.view_name            featurelayer_view_name,
t5.featurelayergroup_id featurelayer_group_id,
t12.name                featurelayer_group_name,
decode(t11.featurelayer_id, null, 0, 1)
                        featurelayer_inmap,

-- MGMAPLAYER
t6.mgmaplayer_id        mgmaplayer_id,
t6.mglayer_id           mgmaplayer_mglayer_id,
t6.mgmap_id             mgmaplayer_mgmap_id,
t6.legend               mgmaplayer_legend,
t6.name                 mgmaplayer_name,
t6.featurelayer_id      mgmaplayer_featurelayer_id, -- if mgmaplayer_featurelayer_id is null => bottomLayer
t7.resource_id          mgmaplayer_resource_id,
t10.wgsobject_id        mgmaplayer_parent_wgsobject_id,
t13.resource_id         mgmaplayer_mgmap_resource_id,

-- FEATURETYPEFORK
t8.featuretypefork_id   featuretypefork_id,
t8.featurelayer_id      featuretypefork_layer_id,
t8.featuretypefork_hash featuretypefork_hash

from                    wgsobject t,
                        mgweblayout t1,
                        mgpane t2,
                        mgcustomcommand t3,
                        mgbuiltincommand t4,
                        featurelayer t5,
                        mgmap_mglayer t6,
                        mglayer t7,
                        featuretypefork t8,
                        wgs3.wgsobjecttype t9,
                        (select wgsobject_id, featurelayer_id from wgsobject) t10,
                        (select mgmaplayer_id, featurelayer_id from mgmap_mglayer) t11,
                        (select featurelayergroup_id, name from featurelayergroup) t12,
                        mgmap t13

where
                        t1.mgweblayout_id (+)= t.mgweblayout_id
and
                        t2.mgpane_id (+)= t.mgpane_id
and
                        t3.mgcustomcommand_id (+)= t.mgcustomcommand_id
and
                        t4.mgbuiltcommand_id (+)= t.mgbuiltcommand_id
and
                        t5.featurelayer_id (+)= t.featurelayer_id
and
                        t13.mgmap_id (+)= t6.mgmap_id
and
                        t6.mgmaplayer_id (+)= t.mgmaplayer_id
and
                        t7.mglayer_id (+)= t6.mglayer_id
and
                        t8.featuretypefork_id (+)= t.featuretypefork_id
and
                        t.wgsobjecttype_id = t9.wgsobjecttype_id
and
                        t10.featurelayer_id (+)= t6.featurelayer_id
and
                        t11.featurelayer_id (+)= t.featurelayer_id
and
                        t12.featurelayergroup_id (+)= t5.featurelayergroup_id;


create or replace view project_wdb.permissions_view as
select

t1.allow              permission,
t1.privilege_id       privilege_id,
t3.name               privilege_name,
t3.description        privilege_description,
t.wgsrole_id          wgsrole_id,
t.name                wgsrole_name,
t.description         wgsrole_description,
t2."WGSOBJECT_ID",t2."WGSOBJECT_LOCKED",t2."WGSOBJECTTYPE_ID",t2."WGSOBJECTTYPE_NAME",t2."MGWEBLAYOUT_ID",t2."MGWEBLAYOUT_NAME",t2."MGWEBLAYOUT_MGMAP_ID",t2."MGWEBLAYOUT_RESOURCE_ID",t2."MGWEBLAYOUT_BYDEFAULT",t2."MGWEBLAYOUT_OVERVIEW",t2."MGPANE_ID",t2."MGPANE_NAME",t2."MGPANE_LABEL",t2."MGPANE_DESCRIPTION",t2."MGPANE_MGWEBLAYOUT_ID",t2."MGPANE_WEBLAYOUT_NAME",t2."MGCUSTOMCOMMAND_ID",t2."MGCUSTOMCOMMAND_NAME",t2."MGCUSTOMCOMMAND_LABEL",t2."MGCUSTOMCOMMAND_DESCRIPTION",t2."MGCUSTOMCOMMAND_INVOKESCRIPT",t2."MGCUSTOMCOMMAND_MGWEBLAYOUT_ID",t2."MGCUSTOMCOMMAND_WEBLAYOUT_NAME",t2."MGBUILTCOMMAND_ID",t2."MGBUILTCOMMAND_NAME",t2."MGBUILTCOMMAND_LABEL",t2."MGBUILTCOMMAND_DESCRIPTION",t2."MGBUILTCOMMAND_MGWEBLAYOUT_ID",t2."MGBUILTCOMMAND_WEBLAYOUT_NAME",t2."FEATURELAYER_ID",t2."FEATURELAYER_NAME",t2."FEATURELAYER_VIEW_NAME",t2."FEATURELAYER_GROUP_ID",t2."FEATURELAYER_GROUP_NAME",t2."FEATURELAYER_INMAP",t2."MGMAPLAYER_ID",t2."MGMAPLAYER_MGLAYER_ID",t2."MGMAPLAYER_MGMAP_ID",t2."MGMAPLAYER_LEGEND",t2."MGMAPLAYER_NAME",t2."MGMAPLAYER_FEATURELAYER_ID",t2."MGMAPLAYER_RESOURCE_ID",t2."MGMAPLAYER_PARENT_WGSOBJECT_ID",t2."MGMAPLAYER_MGMAP_RESOURCE_ID",t2."FEATURETYPEFORK_ID",t2."FEATURETYPEFORK_LAYER_ID",t2."FEATURETYPEFORK_HASH"

from                  wgsrole t,
                      wgsobjectpermission t1,
                      project_wdb.objects_view t2,
                      wgs3.privilege t3

where
                      t1.wgsrole_id = t.wgsrole_id
and
                      t1.privilege_id = t3.privilege_id
and
                      t1.wgsobject_id = t2.wgsobject_id
and
                      t2.wgsobjecttype_id = t3.wgsobjecttype_id

order by              wgsrole_name,
                      privilege_name;


create or replace view project_wdb.permissions_all_view as
select 2 as permission, t."PRIVILEGE_ID",t."PRIVILEGE_NAME",t."PRIVILEGE_DESCRIPTION",t."WGSROLE_ID",t."WGSROLE_NAME",t."WGSROLE_DESCRIPTION",t."WGSOBJECT_ID",t."WGSOBJECT_LOCKED",t."WGSOBJECTTYPE_ID",t."WGSOBJECTTYPE_NAME",t."MGWEBLAYOUT_ID",t."MGWEBLAYOUT_NAME",t."MGWEBLAYOUT_MGMAP_ID",t."MGWEBLAYOUT_RESOURCE_ID",t."MGWEBLAYOUT_BYDEFAULT",t."MGWEBLAYOUT_OVERVIEW",t."MGPANE_ID",t."MGPANE_NAME",t."MGPANE_LABEL",t."MGPANE_DESCRIPTION",t."MGPANE_MGWEBLAYOUT_ID",t."MGPANE_WEBLAYOUT_NAME",t."MGCUSTOMCOMMAND_ID",t."MGCUSTOMCOMMAND_NAME",t."MGCUSTOMCOMMAND_LABEL",t."MGCUSTOMCOMMAND_DESCRIPTION",t."MGCUSTOMCOMMAND_INVOKESCRIPT",t."MGCUSTOMCOMMAND_MGWEBLAYOUT_ID",t."MGCUSTOMCOMMAND_WEBLAYOUT_NAME",t."MGBUILTCOMMAND_ID",t."MGBUILTCOMMAND_NAME",t."MGBUILTCOMMAND_LABEL",t."MGBUILTCOMMAND_DESCRIPTION",t."MGBUILTCOMMAND_MGWEBLAYOUT_ID",t."MGBUILTCOMMAND_WEBLAYOUT_NAME",t."FEATURELAYER_ID",t."FEATURELAYER_NAME",t."FEATURELAYER_VIEW_NAME",t."FEATURELAYER_GROUP_ID",t."FEATURELAYER_GROUP_NAME",t."FEATURELAYER_INMAP",t."MGMAPLAYER_ID",t."MGMAPLAYER_MGLAYER_ID",t."MGMAPLAYER_MGMAP_ID",t."MGMAPLAYER_LEGEND",t."MGMAPLAYER_NAME",t."MGMAPLAYER_FEATURELAYER_ID",t."MGMAPLAYER_RESOURCE_ID",t."MGMAPLAYER_PARENT_WGSOBJECT_ID",t."MGMAPLAYER_MGMAP_RESOURCE_ID",t."FEATURETYPEFORK_ID",t."FEATURETYPEFORK_LAYER_ID",t."FEATURETYPEFORK_HASH"
from
(
select

t3.privilege_id       privilege_id,
t3.name               privilege_name,
t3.description        privilege_description,
t.wgsrole_id          wgsrole_id,
t.name                wgsrole_name,
t.description         wgsrole_description,
t2.*

from                  wgsrole t,
                      project_wdb.objects_view t2,
                      wgs3.privilege t3

where
                      t2.wgsobjecttype_id = t3.wgsobjecttype_id

minus

select

t1.privilege_id       privilege_id,
t3.name               privilege_name,
t3.description        privilege_description,
t.wgsrole_id          wgsrole_id,
t.name                wgsrole_name,
t.description         wgsrole_description,
t2.*

from                  wgsrole t,
                      wgsobjectpermission t1,
                      project_wdb.objects_view t2,
                      wgs3.privilege t3

where
                      t1.wgsrole_id = t.wgsrole_id
and
                      t1.privilege_id = t3.privilege_id
and
                      t1.wgsobject_id = t2.wgsobject_id
and
                      t2.wgsobjecttype_id = t3.wgsobjecttype_id
--and
--                      allow = 1
) t
union
select 1 as permission, t."PRIVILEGE_ID",t."PRIVILEGE_NAME",t."PRIVILEGE_DESCRIPTION",t."WGSROLE_ID",t."WGSROLE_NAME",t."WGSROLE_DESCRIPTION",t."WGSOBJECT_ID",t."WGSOBJECT_LOCKED",t."WGSOBJECTTYPE_ID",t."WGSOBJECTTYPE_NAME",t."MGWEBLAYOUT_ID",t."MGWEBLAYOUT_NAME",t."MGWEBLAYOUT_MGMAP_ID",t."MGWEBLAYOUT_RESOURCE_ID",t."MGWEBLAYOUT_BYDEFAULT",t."MGWEBLAYOUT_OVERVIEW",t."MGPANE_ID",t."MGPANE_NAME",t."MGPANE_LABEL",t."MGPANE_DESCRIPTION",t."MGPANE_MGWEBLAYOUT_ID",t."MGPANE_WEBLAYOUT_NAME",t."MGCUSTOMCOMMAND_ID",t."MGCUSTOMCOMMAND_NAME",t."MGCUSTOMCOMMAND_LABEL",t."MGCUSTOMCOMMAND_DESCRIPTION",t."MGCUSTOMCOMMAND_INVOKESCRIPT",t."MGCUSTOMCOMMAND_MGWEBLAYOUT_ID",t."MGCUSTOMCOMMAND_WEBLAYOUT_NAME",t."MGBUILTCOMMAND_ID",t."MGBUILTCOMMAND_NAME",t."MGBUILTCOMMAND_LABEL",t."MGBUILTCOMMAND_DESCRIPTION",t."MGBUILTCOMMAND_MGWEBLAYOUT_ID",t."MGBUILTCOMMAND_WEBLAYOUT_NAME",t."FEATURELAYER_ID",t."FEATURELAYER_NAME",t."FEATURELAYER_VIEW_NAME",t."FEATURELAYER_GROUP_ID",t."FEATURELAYER_GROUP_NAME",t."FEATURELAYER_INMAP",t."MGMAPLAYER_ID",t."MGMAPLAYER_MGLAYER_ID",t."MGMAPLAYER_MGMAP_ID",t."MGMAPLAYER_LEGEND",t."MGMAPLAYER_NAME",t."MGMAPLAYER_FEATURELAYER_ID",t."MGMAPLAYER_RESOURCE_ID",t."MGMAPLAYER_PARENT_WGSOBJECT_ID",t."MGMAPLAYER_MGMAP_RESOURCE_ID",t."FEATURETYPEFORK_ID",t."FEATURETYPEFORK_LAYER_ID",t."FEATURETYPEFORK_HASH"
from
(
select

t1.privilege_id       privilege_id,
t3.name               privilege_name,
t3.description        privilege_description,
t.wgsrole_id          wgsrole_id,
t.name                wgsrole_name,
t.description         wgsrole_description,
t2.*

from                  wgsrole t,
                      wgsobjectpermission t1,
                      project_wdb.objects_view t2,
                      wgs3.privilege t3

where
                      t1.wgsrole_id = t.wgsrole_id
and
                      t1.privilege_id = t3.privilege_id
and
                      t1.wgsobject_id = t2.wgsobject_id
and
                      t2.wgsobjecttype_id = t3.wgsobjecttype_id
and
                      allow = 1
) t
union
select 0 as permission, t."PRIVILEGE_ID",t."PRIVILEGE_NAME",t."PRIVILEGE_DESCRIPTION",t."WGSROLE_ID",t."WGSROLE_NAME",t."WGSROLE_DESCRIPTION",t."WGSOBJECT_ID",t."WGSOBJECT_LOCKED",t."WGSOBJECTTYPE_ID",t."WGSOBJECTTYPE_NAME",t."MGWEBLAYOUT_ID",t."MGWEBLAYOUT_NAME",t."MGWEBLAYOUT_MGMAP_ID",t."MGWEBLAYOUT_RESOURCE_ID",t."MGWEBLAYOUT_BYDEFAULT",t."MGWEBLAYOUT_OVERVIEW",t."MGPANE_ID",t."MGPANE_NAME",t."MGPANE_LABEL",t."MGPANE_DESCRIPTION",t."MGPANE_MGWEBLAYOUT_ID",t."MGPANE_WEBLAYOUT_NAME",t."MGCUSTOMCOMMAND_ID",t."MGCUSTOMCOMMAND_NAME",t."MGCUSTOMCOMMAND_LABEL",t."MGCUSTOMCOMMAND_DESCRIPTION",t."MGCUSTOMCOMMAND_INVOKESCRIPT",t."MGCUSTOMCOMMAND_MGWEBLAYOUT_ID",t."MGCUSTOMCOMMAND_WEBLAYOUT_NAME",t."MGBUILTCOMMAND_ID",t."MGBUILTCOMMAND_NAME",t."MGBUILTCOMMAND_LABEL",t."MGBUILTCOMMAND_DESCRIPTION",t."MGBUILTCOMMAND_MGWEBLAYOUT_ID",t."MGBUILTCOMMAND_WEBLAYOUT_NAME",t."FEATURELAYER_ID",t."FEATURELAYER_NAME",t."FEATURELAYER_VIEW_NAME",t."FEATURELAYER_GROUP_ID",t."FEATURELAYER_GROUP_NAME",t."FEATURELAYER_INMAP",t."MGMAPLAYER_ID",t."MGMAPLAYER_MGLAYER_ID",t."MGMAPLAYER_MGMAP_ID",t."MGMAPLAYER_LEGEND",t."MGMAPLAYER_NAME",t."MGMAPLAYER_FEATURELAYER_ID",t."MGMAPLAYER_RESOURCE_ID",t."MGMAPLAYER_PARENT_WGSOBJECT_ID",t."MGMAPLAYER_MGMAP_RESOURCE_ID",t."FEATURETYPEFORK_ID",t."FEATURETYPEFORK_LAYER_ID",t."FEATURETYPEFORK_HASH"
from
(
select

t1.privilege_id       privilege_id,
t3.name               privilege_name,
t3.description        privilege_description,
t.wgsrole_id          wgsrole_id,
t.name                wgsrole_name,
t.description         wgsrole_description,
t2.*

from                  wgsrole t,
                      wgsobjectpermission t1,
                      project_wdb.objects_view t2,
                      wgs3.privilege t3

where
                      t1.wgsrole_id = t.wgsrole_id
and
                      t1.privilege_id = t3.privilege_id
and
                      t1.wgsobject_id = t2.wgsobject_id
and
                      t2.wgsobjecttype_id = t3.wgsobjecttype_id
and
                      allow = 0
) t
order by              wgsrole_name,
                      privilege_name;


CREATE OR REPLACE VIEW PROJECT_SDB.ADMPFEATURELAYER AS
SELECT t.NAME ADMP_FEATURE, t.NAME ADMP_FEATLAYER
    FROM PROJECT_WDB.FEATURELAYER t
;


CREATE OR REPLACE VIEW PROJECT_SDB.ADMPFEATURELAYERLOCATION AS
SELECT t.FEATURELAYER_ID, t1.FEATURELAYERGROUP_ID, t.VIEW_NAME, t.NAME LAYER_NAME
       FROM PROJECT_WDB.FEATURELAYER t, PROJECT_WDB.FEATURELAYERGROUP t1
       WHERE t.FEATURELAYERGROUP_ID = t1.FEATURELAYERGROUP_ID
;


CREATE OR REPLACE VIEW PROJECT_SDB.ADMPMETADATA AS
select "ADMP_FEATURENAME", "ADMP_DIRECTION", "ADMP_ORA_TABLENAME", "ADMP_ORA_COLNAME","ADMP_ORA_TAG","ADMP_ORA_TYPE","ADMP_ORA_SIZE","ADMP_ORA_SCALE","ADMP_ACAD_TAG","ADMP_ACAD_DATA","ADMP_ACAD_FIELD","ADMP_ACAD_TYPE","ADMP_ACAD_SIZE","ADMP_ACAD_SCALE","ADMP_ACAD_DEFAULTVALUE"
from
(SELECT VIEW_NAME ADMP_ORA_TABLENAME, LAYER_NAME ADMP_FEATURENAME, FEATURELAYER_ID FROM PROJECT_SDB.ADMPFEATURELAYERLOCATION),
PROJECT_SDB.admpmetadatastatic
UNION
select "ADMP_FEATURENAME", "ADMP_DIRECTION", "ADMP_ORA_TABLENAME", "ADMP_ORA_COLNAME","ADMP_ORA_TAG","ADMP_ORA_TYPE","ADMP_ORA_SIZE","ADMP_ORA_SCALE","ADMP_ACAD_TAG","ADMP_ACAD_DATA","ADMP_ACAD_FIELD","ADMP_ACAD_TYPE","ADMP_ACAD_SIZE","ADMP_ACAD_SCALE","ADMP_ACAD_DEFAULTVALUE"
from
(SELECT VIEW_NAME ADMP_ORA_TABLENAME, LAYER_NAME ADMP_FEATURENAME, FEATURELAYER_ID FROM PROJECT_SDB.ADMPFEATURELAYERLOCATION) t1,
PROJECT_SDB.admpmetadatadynamic t2
where
t1.featurelayer_id = t2.featurelayer_id
;


CREATE OR REPLACE VIEW PROJECT_SDB.ADMPFEATURELOCATION AS
SELECT t1.FEATURELAYER_ID, t1.FEATURELAYERGROUP_ID, t1.VIEW_NAME, t1.LAYER_NAME, t2.FEATURE_ID
       FROM PROJECT_SDB.ADMPFEATURELAYERLOCATION t1, PROJECT_SDB.FEATURETABLE t2
       WHERE t1.featurelayer_id = t2.featurelayer_id
       AND t2.isoffline = 0
;



-- PROJECT_USER
grant select on PROJECT_WDB.OBJECTS_VIEW to PROJECT_USER;
grant select on PROJECT_WDB.PERMISSIONS_ALL_VIEW to PROJECT_USER;
grant select on PROJECT_WDB.PERMISSIONS_VIEW to PROJECT_USER;
grant select on PROJECT_SDB.ADMPFEATURELAYER to PROJECT_USER;
grant select on PROJECT_SDB.ADMPMETADATA to PROJECT_USER;

ALTER TABLE PROJECT_ADB.FEATURE_OBJECT
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.FEATURE_OBJECT
       ADD FOREIGN KEY (OBJECT_ID)
                             REFERENCES PROJECT_ADB.OBJECT  (
              OBJECT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.FEATURELAYER_OBJECT_TYPE
       ADD FOREIGN KEY (OBJECT_TYPE_ID)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.FEATURELAYER_OBJECT_TYPE
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.OBJECT
       ADD FOREIGN KEY (object_type_id)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.OBJECT_RELATION
       ADD FOREIGN KEY (ObjType_relation_id)
                             REFERENCES PROJECT_ADB.Object_Type_Relation  (
              ObjType_relation_id)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.OBJECT_RELATION
       ADD FOREIGN KEY (child_object_id)
                             REFERENCES PROJECT_ADB.OBJECT  (
              OBJECT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.OBJECT_RELATION
       ADD FOREIGN KEY (parent_object_id)
                             REFERENCES PROJECT_ADB.OBJECT  (
              OBJECT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Object_Type_Relation
       ADD FOREIGN KEY (Child_object_type_id)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id);


ALTER TABLE PROJECT_ADB.Object_Type_Relation
       ADD FOREIGN KEY (Parent_object_type_id)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id);


ALTER TABLE PROJECT_ADB.Object_Type_Relation
       ADD FOREIGN KEY (Relation_id)
                             REFERENCES PROJECT_ADB.RELATION  (
              Relation_id)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.opValue_Current
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID);


ALTER TABLE PROJECT_ADB.opValue_Current
       ADD FOREIGN KEY (object_id)
                             REFERENCES PROJECT_ADB.OBJECT  (
              OBJECT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Properties_PValue_Domain
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Properties_PValue_Domain
       ADD FOREIGN KEY (Value_Domain_ID)
                             REFERENCES PROJECT_ADB.pValue_Domain  (
              Value_Domain_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.PROPERTY
       ADD FOREIGN KEY (Properties_Group_ID)
                             REFERENCES PROJECT_ADB.Property_Group  (
              Property_Group_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.PROPERTY
       ADD FOREIGN KEY (Value_Properties_Type_ID)
                             REFERENCES PROJECT_ADB.pValue_Type  (
              Value_Property_Type_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.PROPERTYPRIORITY
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID);


ALTER TABLE PROJECT_ADB.PROPERTYPRIORITY
       ADD FOREIGN KEY (object_type_id)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id);


ALTER TABLE PROJECT_ADB.RELATION
       ADD FOREIGN KEY (RELATION_TYPE_ID)
                             REFERENCES PROJECT_ADB.RELATION_TYPE  (
              RELATION_TYPE_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.Relation_Property
       ADD FOREIGN KEY (Relation_id)
                             REFERENCES PROJECT_ADB.RELATION  (
              Relation_id)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Relation_Property
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Relation_PropertyValue
       ADD FOREIGN KEY (Value_Domain)
                             REFERENCES PROJECT_ADB.pValue_Domain  (
              Value_Domain_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_ADB.Relation_PropertyValue
       ADD FOREIGN KEY (Object_relation_id)
                             REFERENCES PROJECT_ADB.OBJECT_RELATION  (
              Object_relation_id)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_ADB.Relation_PropertyValue
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_SDB.ADMPMETADATADYNAMIC
       ADD FOREIGN KEY (PROPERTY_ID)
                             REFERENCES PROJECT_ADB.PROPERTY  (
              PROPERTY_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_SDB.ADMPMETADATADYNAMIC
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID);


ALTER TABLE PROJECT_SDB.FDOMETADATA
       ADD FOREIGN KEY (FDO_UNIQUE_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_SDB.FEATURETABLE
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID);


ALTER TABLE PROJECT_WDB.FAVORITEVIEW
       ADD FOREIGN KEY (MGWEBLAYOUT_ID)
                             REFERENCES PROJECT_WDB.MGWEBLAYOUT  (
              MGWEBLAYOUT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FAVORITEVIEW
       ADD FOREIGN KEY (WGSUSER_ID)
                             REFERENCES PROJECT_WDB.WGSUSER  (
              WGSUSER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURELAYER
       ADD FOREIGN KEY (FEATURELAYERGROUP_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYERGROUP  (
              FEATURELAYERGROUP_ID);


ALTER TABLE PROJECT_WDB.FEATURELAYER_MGLAYER
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURELAYER_MGLAYER
       ADD FOREIGN KEY (MGLAYER_ID)
                             REFERENCES PROJECT_WDB.MGLAYER  (
              MGLAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURETYPEASSOCIATION
       ADD FOREIGN KEY (FEATURETYPEFORK_ID)
                             REFERENCES PROJECT_WDB.FEATURETYPEFORK  (
              FEATURETYPEFORK_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURETYPEASSOCIATION
       ADD FOREIGN KEY (FEATURETYPE2_ID)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURETYPEASSOCIATION
       ADD FOREIGN KEY (FEATURETYPE1_ID)
                             REFERENCES PROJECT_ADB.OBJECT_TYPE  (
              Object_type_id)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.FEATURETYPEFORK
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.LAYERLIMITATION
       ADD FOREIGN KEY (MGMAPLAYER_ID)
                             REFERENCES PROJECT_WDB.MGMAP_MGLAYER  (
              MGMAPLAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.LAYERLIMITATION
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.LAYERLIMITATION
       ADD FOREIGN KEY (WGSROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.MGCUSTOMCOMMAND
       ADD FOREIGN KEY (MGWEBLAYOUT_ID)
                             REFERENCES PROJECT_WDB.MGWEBLAYOUT  (
              MGWEBLAYOUT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.MGMAP_MGLAYER
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_WDB.MGMAP_MGLAYER
       ADD FOREIGN KEY (MGLAYER_ID)
                             REFERENCES PROJECT_WDB.MGLAYER  (
              MGLAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.MGMAP_MGLAYER
       ADD FOREIGN KEY (MGMAP_ID)
                             REFERENCES PROJECT_WDB.MGMAP  (MGMAP_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.MGWEBLAYOUT
       ADD FOREIGN KEY (MGMAP_ID)
                             REFERENCES PROJECT_WDB.MGMAP  (MGMAP_ID)
                             ON DELETE SET NULL;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (FEATURETYPEFORK_ID)
                             REFERENCES PROJECT_WDB.FEATURETYPEFORK  (
              FEATURETYPEFORK_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (MGMAPLAYER_ID)
                             REFERENCES PROJECT_WDB.MGMAP_MGLAYER  (
              MGMAPLAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (FEATURELAYER_ID)
                             REFERENCES PROJECT_WDB.FEATURELAYER  (
              FEATURELAYER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (MGBUILTCOMMAND_ID)
                             REFERENCES PROJECT_WDB.MGBUILTINCOMMAND  (
              MGBUILTCOMMAND_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (MGCUSTOMCOMMAND_ID)
                             REFERENCES PROJECT_WDB.MGCUSTOMCOMMAND  (
              MGCUSTOMCOMMAND_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (MGPANE_ID)
                             REFERENCES PROJECT_WDB.MGPANE  (
              MGPANE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (MGWEBLAYOUT_ID)
                             REFERENCES PROJECT_WDB.MGWEBLAYOUT  (
              MGWEBLAYOUT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECT
       ADD FOREIGN KEY (WGSOBJECTTYPE_ID)
                             REFERENCES WGS3.WGSOBJECTTYPE  (
              WGSOBJECTTYPE_ID);


ALTER TABLE PROJECT_WDB.WGSOBJECTPERMISSION
       ADD FOREIGN KEY (PRIVILEGE_ID)
                             REFERENCES WGS3.PRIVILEGE  (PRIVILEGE_ID);


ALTER TABLE PROJECT_WDB.WGSOBJECTPERMISSION
       ADD FOREIGN KEY (WGSROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSOBJECTPERMISSION
       ADD FOREIGN KEY (WGSOBJECT_ID)
                             REFERENCES PROJECT_WDB.WGSOBJECT  (
              WGSOBJECT_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLE_SYSROLE
       ADD FOREIGN KEY (SYSROLE_ID)
                             REFERENCES WGS3.SYSROLE  (SYSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLE_SYSROLE
       ADD FOREIGN KEY (WGSROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLE_WGSUSER
       ADD FOREIGN KEY (WGSUSER_ID)
                             REFERENCES PROJECT_WDB.WGSUSER  (
              WGSUSER_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLE_WGSUSER
       ADD FOREIGN KEY (WGSROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLERELATION
       ADD FOREIGN KEY (CHILDROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;


ALTER TABLE PROJECT_WDB.WGSROLERELATION
       ADD FOREIGN KEY (PARENTROLE_ID)
                             REFERENCES PROJECT_WDB.WGSROLE  (
              WGSROLE_ID)
                             ON DELETE CASCADE;

insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (7, '�����');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (1, '������');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (2, '����-�����');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (3, '����� �����');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (4, '����� ������������');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (5, '������ ��������');
insert into PROJECT_ADB.PVALUE_TYPE (VALUE_PROPERTY_TYPE_ID, NAME)
values (6, '������ �� ������');

insert into PROJECT_ADB.RELATION_TYPE (RELATION_TYPE_ID, NAME)
values (1, '���� � ������');
insert into PROJECT_ADB.RELATION_TYPE (RELATION_TYPE_ID, NAME)
values (2, '���� �� ������');

insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ENTITYID', 0, 2, 0, 0, null, null, null, null, null, null, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'VERSIONNUMBER', 2, 2, 0, 0, null, null, null, null, null, null, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'LOCKID', 3, 2, 0, 0, null, null, null, null, null, null, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPBLOCKNAME', 4, 1, 255, 0, 11, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPCLOSED', 4, 96, 1, 0, 21, null, null, 5, 1, 1, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPCOLOR', 4, 2, 3, 0, 4, null, null, 5, 3, 3, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPENTITYTYPE', 4, 1, 30, 0, 24, null, null, 12, 30, 30, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPHYPERLINK', 4, 1, 255, 0, 20, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPJUSTIFICATION', 4, 2, 0, 0, 16, null, null, 5, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPLAYER', 4, 1, 255, 0, 6, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPLINETYPE', 4, 1, 255, 0, 7, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPLINETYPESCALE', 4, 2, 0, 0, 8, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPLINEWEIGHT', 4, 2, 0, 0, 10, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPOLYPATCOLOR', 4, 2, 0, 0, 25, null, null, 5, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPOLYPATTYPE', 4, 1, 255, 0, 26, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPPLOTSTYLE', 4, 1, 255, 0, 5, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPROTATION', 4, 2, 0, 0, 17, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPTEXTCONTENT', 4, 1, 512, 0, 15, null, null, 12, 512, 512, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPTEXTHEIGHT', 4, 2, 0, 0, 14, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPTEXTSTYLE', 4, 1, 255, 0, 13, null, null, 12, 255, 255, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPTHICKNESS', 4, 2, 0, 0, 18, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPWIDTH', 4, 2, 0, 0, 22, null, null, 8, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPBLOCKSCALE', 4, 108, 0, 0, 12, null, null, 16, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'ADMPNORMAL', 4, 108, 0, 0, 23, null, null, 16, 0, 0, null);
insert into PROJECT_SDB.ADMPMETADATASTATIC (ADMP_DIRECTION, ADMP_ORA_COLNAME, ADMP_ORA_TAG, ADMP_ORA_TYPE, ADMP_ORA_SIZE, ADMP_ORA_SCALE, ADMP_ACAD_TAG, ADMP_ACAD_DATA, ADMP_ACAD_FIELD, ADMP_ACAD_TYPE, ADMP_ACAD_SIZE, ADMP_ACAD_SCALE, ADMP_ACAD_DEFAULTVALUE)
values (0, 'GEOMETRY', 1, 108, 0, 0, null, null, null, null, null, null, null);

insert into PROJECT_SDB.ADMPOPTIONS (OPTIONS_NAME, OPTIONS_VALUE)
values ('LOCKING', '0');

insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (1, 'Pan', null, '����� ���������������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (2, 'Zoom', null, '����� ������������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (3, 'Zoom Rectangle', null, '����������� ��������� �������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (4, 'Zoom Selection', null, '����������� ���������� �������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (5, 'Select', null, '����� �������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (6, 'Select Radius', null, '����� �������� �����������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (7, 'Select Polygon', null, '����� �������� ���������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (8, 'Copy', null, '����������� ������� ������� � �����');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (9, 'Buffer', null, '���������� �������� ����');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (10, 'Select Within', null, '����� �������� � ��������� �������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (11, 'Print', null, '������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (12, 'Measure', null, '��������� ����������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (13, 'View Options', null, '���������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (14, 'Get Printable Page', null, '��������������� ��������');
insert into PROJECT_WDB.MGBUILTINCOMMAND (MGBUILTCOMMAND_ID, NAME, LABEL, DESCRIPTION)
values (15, 'Help', null, '�������');

insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (1, 'Layers pane', null, '������ �����');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (2, 'Properties pane', null, '������ �������');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (3, 'Toolbar', null, '������ ������������');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (4, 'Context menu', null, '����������� ����');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (5, 'Status bar', null, '������ ���������');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (6, 'Zoom control', null, '������ ���������������');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (7, 'Task pane', null, '������ �����');
insert into PROJECT_WDB.MGPANE (MGPANE_ID, NAME, LABEL, DESCRIPTION)
values (8, 'Task bar', null, '��������� ������ �����');

create index PROJECT_SDB.IDX_GEOMETRYHASH on PROJECT_SDB.FEATURETABLE (GEOMETRYHASH);
create index PROJECT_SDB.IDX_FEATURE_ID on PROJECT_SDB.FEATURETABLE (FEATURE_ID);
create index PROJECT_SDB.IDX_ADMPLAYER on PROJECT_SDB.FEATURETABLE (ADMPLAYER);
create index PROJECT_ADB.IDX_FEATURE_ID on PROJECT_ADB.FEATURE_OBJECT (FEATURE_ID);
create index PROJECT_ADB.IDX_GEOMETRYHASH on PROJECT_ADB.FEATURE_OBJECT (GEOMETRY_HASH);