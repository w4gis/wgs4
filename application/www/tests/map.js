define([
    'intern!object',
    'intern/chai!assert',
    'wgs4/login',
    'require'
], function (registerSuite, assert, LoginModule, require) {
	var ip = window.IP /* your ip */;
	registerSuite({
        name: 'Map (mobile)',
		
		'Correct Login/Password' : function () {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'demo', 'demo' ).then(
					deferred.callback(
						function( response ){
							assert.strictEqual( response , '', 
								'If authorization is ok, nothing should be returned.');
						}
					)
				);
        },

        'Goto tasks' : function () {
			console.log(this.remote)
			window.self = this
			setTimeout( function(){
				console.log(self.remote)
			}, 1000)
			return this.remote.get(require.toUrl('http://localhost/mobile'))
				.setFindTimeout(5000)
				.findByCssSelector('#taskButton')
				.click()
				.findByCssSelector('#dojox_mobile_EdgeToEdgeCategory_0')
				.getVisibleText()
				.then(function (text) {
					var lengthMoreThan0 = text.length > 0;
					assert.isTrue(lengthMoreThan0, 'Hello, Elaine!',
					  'Greeting should be displayed when the form is submitted');
				  });
        }		
    });
});
