define([
    'intern!object',
    'intern/chai!assert',
    'wgs45/controllers/cardTable/cardTypeSelect'
], function (registerSuite, assert, CardTypeSelect) {
	registerSuite({
		name: 'CardTypeSelect',
		
		'getCardTypeSelect' : function(){
			var deferred = this.async(100000);

			new CardTypeSelect()
			.getCardTypes().then(
				deferred.callback(
					function(types){
						console.log("Оно = ",types);
						assert.strictEqual(types.length, 3, "not working");
					}));

		},

		'getCardTypeSelectLaers' : function(){
			var deferred = this.async(100000);

			new CardTypeSelect()
			.getCardTypes().then(
				deferred.callback(
					function(types){
						console.log("Оно = ",types);
						assert.strictEqual(types[0].name, 'Здания', "not working");
					}));

		},
		'getCardTypeSelectLayers' : function(){
			var deferred = this.async(100000);

			new CardTypeSelect()
			.getCardTypes().then(
				deferred.callback(
					function(types){
						
						assert.strictEqual('1', types[0].layers[0].id,  "not working");
					}));

		},
		
		'getCardTypeSelectRoom' : function(){
			var deferred = this.async(100000);

			new CardTypeSelect()
			.getCardTypes().then(
				deferred.callback(
					function(types){
						
						assert.strictEqual('Комната', types[1].name,  "not working");
					}));

		},
		
		'getCardTypeSelectStudent' : function(){
			var deferred = this.async(100000);

			new CardTypeSelect()
			.getCardTypes().then(
				deferred.callback(
					function(types){
						
						assert.strictEqual('Студент', types[2].name,  "not working");
					}));

		},
		
		
	});
});