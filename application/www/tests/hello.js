define([
    'intern!object',
    'intern/chai!assert',
    'wgs4/login'
], function (registerSuite, assert, LoginModule) {
	var ip = window.IP /* your ip */;
	registerSuite({
        name: 'hello',

        'Correct Login/Password' : function () {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'demo', 'demo' ).then(
					deferred.callback(
						function( response ){
							assert.strictEqual( response , '', 
								'If authorization is ok, nothing should be returned.');
						}
					)
				);
        },
        
        'Incorrect Password': function() {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'demo', 'kdfjlajf  fdsafsd' ).then(
					deferred.callback(
						function( response ){
							assert.equal( response.exception , 2, 
								'If password incorrect, 2 exception is thrown on server.');
						}
					)
				);
		},
		
		'Incorrect Login': function() {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'jdskjdagj', 'demo' ).then(
					deferred.callback(
						function( response ){
							assert.equal( response.exception , 2, 
								'If login incorrect, 2 exception is thrown on server.');
						}
					)
				);
		},
		
		'Incorrect Login & Password': function() {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'jdskjdagj', 'demoddsa' ).then(
					deferred.callback(
						function( response ){
							assert.equal( response.exception , 2, 
								'If login & password are incorrect, 2 exception is thrown on server.');
						}
					)
				);
		},
		
		'No password provided': function() {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( 'demo', '' ).then(
					deferred.callback(
						function( response ){
							assert.equal( response.exception , 4, 
								'If no password provided - 4 exception thrown on client or on server');
						}
					)
				);
		},
		
		'No username provided': function() {
			var deferred = this.async();
			
			new LoginModule( ip )
				._doLogin( '', 'demo' ).then(
					deferred.callback(
						function( response ){
							assert.equal( response.exception , 4, 
								'If no username provided - 4 exception thrown on client or on server');
						}
					)
				);
		},
		
    });
});
