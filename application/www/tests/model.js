define([
    'intern!object',
    'intern/chai!assert',
    'wgs45/model/cardModel'
], function (registerSuite, assert, CardModel) {
	registerSuite({
		name: 'card model',
		
		'getTestCardTruID' : function(){
			var deferred = this.async(1000000);
			new CardModel()
			.get(['2614']).then(
				deferred.callback(
					function(response){
						assert.strictEqual(response.length, 1, "not working");
					}));

		},
		

		
		'getTestCard1FalseID' : function(){
			var deferred = this.async(10000);
			new CardModel()
			.get(['5555555']).then(
				deferred.callback(
					function(response){
						assert.strictEqual(response.length, 0, "not woking");
					}));
			
		},
		'getTestCard1NullID' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['']).then(
				deferred.callback(
					// function(response){
					// 	assert.strictEqual(response.length, 3, "object 22829 has 3 elements");
					// }, 
					function(response)
					{
						assert.strictEqual(response.length,0,"not woking");
					}
					));
			
		},
		'getTestCard1mustlID' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['22822111']).then(
				deferred.callback(
					function(response){
						assert.strictEqual(response.length, 0, "not woking");
					}));
			
		},

		'getTestCardId' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['2614']).then(
				deferred.callback(
					function(response){
						console.log("Оно = ",response);
						assert.strictEqual(response[0].name, 'Главный корпус', "its false");
					}));
			
		},
		'getTestCardTypeName' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['2614']).then(
				deferred.callback(
					function(response){
						assert.strictEqual(response[0].typeName, 'Здания', "its false");
					}));
			
		},
		

		'getTestCardpropertyGroups' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['2614']).then(
				deferred.callback(
					function(response){
						assert.strictEqual(response[0].propertyGroups[1].name, 'Адресные', "its false");
					}));
			
		},
		
		'getTestCardMultipleCards' : function(){
			var deferred = this.async();
			new CardModel()
			.get(['2614','2609']).then(
				deferred.callback(
					function(response){
						console.log("Оно = ",response);
						assert.strictEqual(response[0].typeName,  'Здания (основное)', "its false");
					}));
			
		},


		
	});
});