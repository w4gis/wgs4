define("wgs45/controllers/updater", [
    'dijit/Dialog',
    'dojo/Deferred',
    'dojo/DeferredList',
    'dijit/registry',
    "dijit/ProgressBar",
    'dojo/request/xhr',
    'wgs45/controllers/helper',
    'dojo/request/iframe'], function(Dialog, Deferred, DeferredList, registry, ProgressBar, xhr, Helper, iframe) {
    function Updater() {
        var self = this;

        this.dialog = new Dialog({
            title: "Обновление БД студентов",
            content: "\
                <div id=\"cont-update-welcome\">\
                    <p>Вас приветствует мастер обновления базы данных ГИС ТУСУР.<p>\
                    <p>С помощью мастера обновления вы можете обновить базу данных студентов ТУСУР до актуального состояния АИС Контингент.<p>\
                    <p>Для того, чтобы получить сведения о текущем состоянии базы данных нажмите кнопку \"Загрузить сведения об обновлении\".<p>\
                </div>\
                <div id=\"cont-update-success\" style=\"display: none\">\
                    <p>Обновление базы данных студентов выполнено успешно!</p>\
                    <p>Обновлены/добавлены сведения по <span id=\"cont-update-count\"></span> студентам.</p>\
                </div>\
                <div id=\"cont-update-info\" style=\"display: none\">\
                    <table>\
                        <tr>\
                            <td>Количество студентнов в ГИС ТУСУР: </td><td class=\"cont-update-info__value\"><span style=\"font-weight: bold\" id=\"cont-our-count\"></span></td>\
                        </tr>\
                        <tr>\
                            <td>Количество студентнов в АИС Контингент: </td><td class=\"cont-update-info__value\"><span style=\"font-weight: bold\" id=\"cont-count\"></span></td>\
                        </tr>\
                        <tr>\
                            <td>Последнее обновление: </td><td class=\"cont-update-info__value\"><span style=\"font-weight: bold\" id=\"cont-our-updated\"></span></td>\
                        </tr>\
                    </table>\
                    <p style=\"margin-top: 10px; font-size: 90%\">\
                        Внимание. В зависимости от выбранных параметров процесс обновления базы данных может занять от 15-20 минут до нескольких часов.<br/>\
                        Внимание. Обновление базы данных необратимо.\
                    </p>\
                </div>\
                <div class=\"clearfix\" style=\"border-top: dashed 1px #ccc; padding: 5px 0 0 0; margin: 10px 0 0 0\">\
                    <div id=\"cont-get-update-params-progress\" class=\"wgs-progress big-progress\" style=\"display: none\">\
                        <div class=\"wgs-progress__progress wgs-progress__progress-wp\"></div>\
                        <div class=\"wgs-progress__message\">Получение сведений об обновлении. Пожалуйста, подождите...</div>\
                    </div>\
                    <div style=\"float: left; display: none\" id=\"cont-update-with-delete-container\">\
                        <p style=\"line-height: 30px\">\
                            <span id=\"cont-update-with-delete\" data-dojo-type=\"dijit/form/CheckBox\" data-dojo-props=\"checked: false\"></span>\
                            <label for=\"cont-update-with-delete\">Удалить неактивных студентов</label>\
                        </p>\
                        <p style=\"line-height: 30px\">\
                            <span id=\"cont-update-with-bind-new\" data-dojo-type=\"dijit/form/CheckBox\" data-dojo-props=\"checked: false\"></span>\
                            <label for=\"cont-update-with-bind-new\">Загрузить данные по комнатам для вновь загруженных студентов</label>\
                        </p>\
                        <p style=\"line-height: 30px\">\
                            <span id=\"cont-update-with-bind-old\" data-dojo-type=\"dijit/form/CheckBox\" data-dojo-props=\"checked: false\"></span>\
                            <label for=\"cont-update-with-bind-old\">Перезаписать данные по комнатам для обновленных студентов</label>\
                        </p>\
                    </div>\
                    <div style=\"float:left\" id=\"cont-update-progress\"></div>\
                    <button style=\"float: right; display: none\" data-dojo-type=\"dijit/form/Button\" id=\"cont-do-update\" data-dojo-props=\"disabled: true\">\
                        <i class=\"fa fa-cloud-download\"></i> Обновить\
                    </button>\
                    <button style=\"float: right;\" data-dojo-type=\"dijit/form/Button\" id=\"cont-check-update\">\
                        <i class=\"fa fa-info\"></i> Загрузить сведения об обновлении\
                    </button>\
                    <button style=\"float: right; display: none\" data-dojo-type=\"dijit/form/Button\" id=\"cont-ok-update\">\
                        <i class=\"fa fa-check\"></i> Закрыть мастер обновления\
                    </button>\
                    <div style=\"float:left\" id=\"cont-update-progress-message\"></div>\
                </div>\
            ",
            style: "width: 500px",
            onHide: function() {
                this.destroyRecursive();
            }
        });

        this.dialog.show();

        registry.byId('cont-check-update').onClick = function() {
            self.getUpdateInfo();
        };

        registry.byId('cont-do-update').onClick = function() {
            self.doUpdate();
        };
    }

    Updater.prototype.doUpdate = function() {
        var self = this;
        this._hideBlock('cont-update-with-delete-container');
        registry.byId('cont-do-update').set('disabled', true);
        registry.byId('cont-do-update').set('label', 'Выполняется обновление...');

        var progressBar = new ProgressBar({
            style: "width: 200px",
            places: 1
        }, 'cont-update-progress');
        progressBar.startup();
        var url = '/wgssoapclient/wgssoapclient/updatedb?';
        url += 'remove=' + (registry.byId('cont-update-with-delete').get('checked') ? 1 : 0);
        url += '&bindnew=' + (registry.byId('cont-update-with-bind-new').get('checked') ? 1 : 0);
        url += '&bindold=' + (registry.byId('cont-update-with-bind-old').get('checked') ? 1 : 0);
        
        iframe.get(url, {
            timeout: 6 * 1000 * 10 /* 1min * 60 */ * 60 /* =1 hour*/ ,
            handleAs: 'text'
        }).then(function(total) {
            document.getElementById('cont-update-count').innerHTML = total;
        }, function(e) {
            Helper.error('Возможно, во время выполннения обновления произошла ошибка', 20000);
            console.log(e);
        });

        var progressInt = setInterval(function() {
            xhr.get('/wgssoapclient/wgssoapclient/getupdateprogress', {
                handleAs: 'json'
            }).then(function(data) {
                if(data.rprogress != 0 && data.rprogress != 100) {
                    progressBar.set("value", data.rprogress);
                    document.getElementById('cont-update-progress-message').innerHTML = 'Шаг 1 из 3. Поиск и удаление неактивных студентов';
                } else {
                    if(data.progress == 0) {
                        progressBar.set("indeterminate", true);
                        document.getElementById('cont-update-progress-message').innerHTML = 'Шаг 2 из 3. Загрузка актуальных данных из АИС "Контингент"';
                    } else {
                        progressBar.set("indeterminate", false);
                        progressBar.set("value", data.progress);
                        
                        document.getElementById('cont-update-progress-message').innerHTML = 'Шаг 3 из 3. Обновление базы данных ГИС ТУСУР';
                    }
                }
                
                if (data.progress == 100) {
                    Helper.success('Обновление базы данных выполнено успешно!');
                    clearInterval(progressInt);
                    self._showSuccessDialog();
                }
            }, function(error) {
                console.log(error);
                Helper.error('Не удалось получить сведения о ходе обновления!');
            });
        }, 10000);
    };

    Updater.prototype._showSuccessDialog = function() {
        var self = this;
        this._hideBlock('cont-update-progress');
        this._hideBlock('cont-update-progress-message');
        this._hideBlock('cont-do-update');
        this._hideBlock('cont-update-info');

        this._showBlock('cont-update-success');
        this._showBlock('cont-ok-update');

        registry.byId('cont-ok-update').onClick = function() {
            self.dialog.hide();
        };
    };

    Updater.prototype._toggleBlockVisibility = function(blockId, displayStyle) {
        var blockElem;
        if (registry.byId(blockId) && registry.byId(blockId).domNode) {
            blockElem = registry.byId(blockId).domNode;
        } else {
            blockElem = document.getElementById(blockId);
        }

        blockElem.style.display = displayStyle;
    };

    Updater.prototype._showBlock = function(blockId) {
        this._toggleBlockVisibility(blockId, '');
    };
    
    Updater.prototype._hideBlock = function(blockId) {
        this._toggleBlockVisibility(blockId, 'none');
    };


    Updater.prototype.getUpdateInfo = function() {
        var self = this;
        this._showBlock('cont-get-update-params-progress');
        this._showBlock('cont-update-info');
        this._hideBlock('cont-update-welcome');
        this._hideBlock('cont-check-update');


        var updateReadyPromises = [];
        var getContingetCount = xhr.get('/wgssoapclient/wgssoapclient/getcount', {
            handleAs: 'json'
        });

        getContingetCount.then(function(data) {
            document.getElementById('cont-count').innerHTML = data.total;
        });

        var getOurCount = xhr.get('/objectadmin/cardtable/getCardTableValuesCount?typeId=3&layerId=-1', {
            handleAs: 'json'
        });

        updateReadyPromises.push(getContingetCount);
        updateReadyPromises.push(getOurCount);

        getOurCount.then(function(data) {
            console.log(data.total);
            document.getElementById('cont-our-count').innerHTML = data.total;
            document.getElementById('cont-our-updated').innerHTML = data.updated;
        });

        new DeferredList(updateReadyPromises).then(function() {
            registry.byId('cont-do-update').set('disabled', false);
            self._showBlock('cont-do-update');
            self._showBlock('cont-update-with-delete-container');
            self._hideBlock('cont-get-update-params-progress');
        });
    };
    
    return Updater;

})