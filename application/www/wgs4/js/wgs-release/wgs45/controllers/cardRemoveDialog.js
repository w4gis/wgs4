define("wgs45/controllers/cardRemoveDialog", ['dojo/Deferred',
	"dijit/ConfirmTooltipDialog",
	"dijit/popup",
    'dijit/registry',
    'wgs45/controllers/helper',
    'wgs45/model/cardModel'], function(Deferred, ConfirmTooltipDialog, popup, registry, Helper, CardModel){
	"use strict";

	function CardRemoveDialog(objectId, model){
		this._objectId = objectId;
		this._model = model || new CardModel();
	}

	CardRemoveDialog.prototype.show = function(node) {
		console.log("show");
        var self = this;
        this.dialogDeferred = new Deferred();

        if(!this.myDialog){
            this.myDialog = new ConfirmTooltipDialog({
            content:
                '<h2>Вы уверены? Удаление необратимо</h2></br>' + 
                '<input type="checkbox" data-dojo-type="dijit/form/CheckBox" name="name">&nbsp;Вместе с подкарточками',
            buttonOk: 'Удалить',
            onCancel: function() {
                            console.log('cancel button');
                			popup.close(self.myDialog);
                            self.popupIsClosed = true;
                			self.dialogDeferred.cancel();
            			  },
    	    onExecute: function() {
                            console.log('execute button');
    	   				    var removeMode = 0;//registry.byId(self.myDialog.domNode.getElementsByTagName('input')[0].parentNode.getAttribute('widgetid')).checked ? 0 : 1;
                			popup.close(self.myDialog);
                            self.popupIsClosed = true;

                			self.removeObject(removeMode).then(function(success) {
                                Helper.success('Карточка удалена');
                				self.dialogDeferred.resolve(success);
                			}, function(err) {
                                Helper.error('Ошибка при удалении карточки');
                                self.dialogDeferred.reject(err);
                            });
            			  },
            onMouseLeave: function() {
                            console.log('mouse leave');
                            if(!self.popupIsClosed) {
                                popup.close(self.myDialog);
                                self.popupIsClosed = true;
                                self.dialogDeferred.cancel();
                            }
            			  }
    	   });
        }

    	popup.open({
            popup: self.myDialog,
            around: node
        });
        this.popupIsClosed = false;

        return this.dialogDeferred;
	};

	CardRemoveDialog.prototype.removeObject = function(removeMode) {
		console.log(this._objectId, 'is removed', removeMode);
		return this._model.removeObject(this._objectId, removeMode);
	};

	return CardRemoveDialog;
});