define("wgs45/controllers/ClearableTextBox", ['dojo/_base/declare', 
    'dojo/dom-construct', 
    'dojo/dom-class',
    'dojo/dom-style',  
    'dijit/form/TextBox'], function (declare, domConstruct, domClass, domStyle,TextBox){
return declare(TextBox, {
	// The "Delete" word
	deleteText: "Delete",
	
	// Fire the change event for every text change
	intermediateChanges: true,
	
	// PostCreate method
	// Fires *after* nodes are created, before rendered to screen
	postCreate: function() {
		// Do what the previous does with this method
		this.inherited(arguments);

		// Add widget class to the domNode
		var domNode = this.domNode;
		domNode.classList.add("davidwalshClearBox");

		// Create the "X" link
		this.clearLink = domConstruct.create("a", {
			className: "davidwalshClear",
			innerHTML: this.deleteText
		}, domNode, "first");

		// Fix the width
		var startWidth = domStyle.get(domNode, "width"),
			pad = domStyle.get(this.domNode,"paddingRight");
		domStyle.set(domNode, "width", (startWidth - pad) + "px");

		// Add click event to focus node
		this.connect(this.clearLink, "onclick", function(){
			// Clear the value
			this.set("value", "");
			// Focus on the node, not the link
			this.textbox.focus();
		});

		// Add intermediate change for self so that "X" hides when no value
		this.connect(this, "onChange", "checkValue");

		// Check value right away, hide link if necessary
		this.checkValue();
	},
	
	checkValue: function(value) {
		domClass[value != "" && value != undefined ? "remove" : "add"](this.clearLink, "dijitHidden");
	}
});
});