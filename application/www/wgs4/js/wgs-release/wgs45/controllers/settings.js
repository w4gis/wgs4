//Fusion.getWidgetById('Map').loadMapGroup(Fusion.applicationDefinition.mapGroups['openLayers-fet']);

define("wgs45/controllers/settings", [], function() {

    function Settings() {
       
    };
    
    Settings.prototype.getDefaults = function(key) {
        if(!key || !window.localStorage) return;
        
        return localStorage.getItem(key);
    }
    
    Settings.prototype.setDefaults = function(defaultValue) {
        if(!defaultValue || !window.localStorage) return;
        
        for(var i in defaultValue) {
            localStorage.setItem(i, defaultValue[i]);    
        }
    }

    return new Settings();
});