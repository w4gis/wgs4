define("wgs45/controllers/wgs-app", [
		"wgs45/controllers/map",
		"wgs45/controllers/app-menu",
		"wgs45/controllers/helper",
		"wgs45/controllers/tusurFlatStats",
		'dojox/layout/Dock',
		"wgs45/model/wgs-app",
		"dijit/registry",
		"dojo/request/xhr",
		"dojo/Deferred",
		"wgs45/controllers/theme",
		"wgs45/controllers/card",
		'wgs45/controllers/cardTable/cardTable',

		"dijit/Dialog",
		'dojo/dom-construct',
		'wgs45/controllers/cardActionDialog',
		'wgs45/controllers/settingsView',

		"dijit/layout/BorderContainer",
		"dijit/layout/TabContainer",
		"dijit/layout/ContentPane"
	],

	function (Map, AppMenu, Helper, TusurFlatStats, Dock, AppModel, registry, xhr, Deferred, Theme, Card, CardTable, Dialog, domConstruct, CardActionDialog, SettingsView) {
		function WgsAppUi(wgsConfig) {
			this._mapWidgetIdBase = "Map";
			this._mapTabWidgetId = "map-tab-container";
			this.model = new AppModel(wgsConfig);
			this.model.initDeferred.then(this.startup.bind(this));

			this.selText = {};
			this.maps = {};
			this.cards = {};
			window.onError = function () {

			}
		}

		WgsAppUi.prototype.selectFeatures = function (mapName, ids, isObjects) {
			var self = this;

			window.WGS_APP.openMap(mapName).then(function (result) { //успех

				var mapProps = window.WGS_APP.getCurrentMapProps();
				var self = this;
				var query = {
					'mapname': mapProps.mapName,
					'session': mapProps.sessionId,
					'wgsVersion': 4
				}

				query[isObjects ? 'objects' : 'features'] = JSON.stringify(ids);

				xhr.post('/map/lib/selectFeatures', {
					handleAs: "text",
					data: query
				}).then(function (selText) {

					var layerId = selText.substr(selText.indexOf("layer_") + 6, 10);
					layerId = layerId.substr(0, layerId.indexOf('_'));
					window.WGS_APP.selectLayerGroupByLayerId(mapName, layerId);

					mapProps.map.setSelection(selText, true);
				});
			});
		}

		WgsAppUi.prototype.selectLayerGroupByLayerId = function (mapName, layerId) {
			console.log('LAYER_ID', layerId);
			var legend = window.WGS_APP.maps[mapName]._legend;
			if (legend._floorListItemElems) {
				for (var i = 0; i < legend._floorListItemElems.length; i++) {
					var legendGroup = legend._floorListItemElems[i];
					if (legendGroup.dataset.layers.indexOf(layerId + ',') != -1) {
						if (legend.floorSelectWidget) {
							legend.floorSelectWidget.set('value', 'value-' + i);
						}
					}
				}
			}
		}

		WgsAppUi.prototype.getAllLayerIds = function (mapName) {
			if (!window.WGS_APP.maps[mapName]) {
				throw new Error("Map " + mapName + " is not loaded");
			}

			var legend = window.WGS_APP.maps[mapName]._legend;
			var selectedGroupNumber = window.WGS_APP.maps[mapName]._selectedGroup;

			if (!legend._floorListItemElems || !selectedGroupNumber) {
				throw new Error("Map " + mapName + " doesn't have any layer groups");
			}

			var layers = [];

			[].forEach.call(legend._floorListItemElems, function (floorElem) {
				var selectedGroupLayers = floorElem.dataset.layers;
				var layerIds = selectedGroupLayers.split(',');
				var currentLayerId;
				layerIds.forEach(function (layerId) {
					// console.log(layerId, !~layerId.indexOf('_'));
					if (layerId && !~layerId.indexOf('_')) {
						layers.push(layerId);
					}
				});
			}, this);



			return layers;
		}

		WgsAppUi.prototype.getCurrentLayerId = function (mapName) {
			if (!window.WGS_APP.maps[mapName]) {
				throw new Error("Map " + mapName + " is not loaded");
			}

			var legend = window.WGS_APP.maps[mapName]._legend;
			var selectedGroupNumber = window.WGS_APP.maps[mapName]._selectedGroup;

			if (!legend._floorListItemElems || !selectedGroupNumber) {
				throw new Error("Map " + mapName + " doesn't have any layer groups");
			}

			var selectedGroupLayers = legend._floorListItemElems[selectedGroupNumber].dataset.layers;
			var layerIds = selectedGroupLayers.split(',');
			var currentLayerId;
			layerIds.forEach(function (layerId) {
				// console.log(layerId, !~layerId.indexOf('_'));
				if (layerId && !~layerId.indexOf('_')) {
					currentLayerId = layerId;
				}
			});

			return currentLayerId;
		};

		WgsAppUi.prototype.startup = function () {
			if (!this.model.getAuthStatus()) {
				return this.askForAuth();
			}

			this._runApplication();

			var dockContainerElem = document.createElement('div');
			document.getElementById('app-windows-container').appendChild(dockContainerElem);
			this.windowDock = new Dock({}, dockContainerElem);
		};

		WgsAppUi.prototype.getVersion = function () {
			return this.model.getVersion();
		};


		WgsAppUi.prototype.getMaps = function () {
			return this._getPermissionedMaps(this.model.getMaps());
		};

		WgsAppUi.prototype.getMapDescription = function (mapId) {
			var allMaps = this.getMaps();
			var map;
			allMaps.forEach(function (mapDescription) {
				if (mapDescription.mapName == mapId) {
					map = mapDescription;
				}
			});

			if (map) {
				return map;
			}

			throw new Error('map with specified mapId (' + mapId + ') does not exist');
		};

		WgsAppUi.prototype.askForAuth = function () {
			return location.assign('/info');
		};


		//////////// PRIVATE METHODS /////////////////

		WgsAppUi.prototype._runApplication = function () {
			console.log("WGS4.campus v" + this.getVersion());

			this._setupInitialState();
			new AppMenu({
				maps: this.getMaps()
			}).show();

			/*    this.maps['tusur'] = new Map({
			    mapName: 'tusur',
			    title: "Здания ТУСУР"
			}).startup();*/
			var loaderElem = document.querySelector('.app-loader');
			this.openMap("tusur").then(function (result) {
				if (loaderElem) {
					loaderElem.style.display = "none";
				}
				var tusurStats = new TusurFlatStats();

				document.body.addEventListener('legend-selection', function () {
					tusurStats.refreshData();
				});
			});

			registry.byId('appLayout').resize();
			this._subscribeToMenuEvents();
		};

		WgsAppUi.prototype._getPermissionedMaps = function (maps) {
			var availableMaps = [];
			var privileges = window.WGS.PRIVILEGE;
			if (!privileges || privileges.PROJECT_ADMIN) return maps;

			maps.forEach(function (map, i) {
				if (privileges[map.mapName]) {
					availableMaps.push(map);
				}
			});

			return availableMaps;
		}

		WgsAppUi.prototype._subscribeToMenuEvents = function () {
			var mapDescriptions = this.getMaps();
			var self = this;

			document.body.addEventListener('click', function (evt) {
				if (!evt.target.dataset) return;
				var _target = evt.target.dataset.action ? evt.target : evt.target.closest('a');
				var target;

				if (_target && _target.dataset.action) {
					target = _target;
				}

				if (!target) {
					return;
				}
				switch (target.dataset.action) {
				case 'plan-open':
					{
						self.openMap(target.dataset.actionParam);
						break;
					}
				case 'card-open-direct':
					{
						self.openCard(true, target.dataset.cardId);
						break;
					}
				case 'report-1':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						console.log(target);
						self.printSimpleReport(target.dataset.cardId);
						break;
					}
				case 'contract-student':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						console.log('CONTRACT', target);
						self.printContract(target.dataset.cardId);
						break;
					}
				case 'card-remove':
					{
						new CardActionDialog(target.dataset.cardId).show(target).then(function (success) {
							document.body.dispatchEvent(new CustomEvent('grid-refresh', {
								bubbles: true,
								cancelable: true
							}));
						}, function (error) {
							console.log(error);
						});
						break;
					}
				case 'registry':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						// console.log(evt.detail);
						self.printRegistry(target.dataset.cardId);
						break;
					}
				case 'foreign-registry':
					{
						self.printForeignRegistry(target.dataset.cardId);
						break;
					}
				case 'card-unbind':
					{
						xhr.post('/objectadmin/card/RemoveObjectRelation', {
							data: {
								relationsId: JSON.stringify([target.dataset.relationId]),
								removeMode: 1,
								WgsVersion: 4
							}
						}).then(function () {
							document.body.dispatchEvent(new CustomEvent('grid-refresh', {
								bubbles: true,
								cancelable: true
							}));
							Helper.success('Дочерняя карточка отвязаны');
						}, function () {
							Helper.error('Не удалось отвязать карточку');
						});
						break;
					}
				}
			});

			document.body.addEventListener('menu-select', function (evt) {
				console.log(evt.target);
				switch (evt.detail.action) {
				case 'plan-open':
					{
						var mapId = evt.detail.param;
						self.openMap(mapId);
						break;
					}
				case 'admin-restart':
					{

						if (confirm("Выполнить перезагрузку Веб-сервера и ГИС-сервера?\r\nВсе активные подключения будут нарушены.")) {
							xhr.post('/projectadmin/restart', {
								handleAs: "text"
							}).then(function () {
								var waitDialog = new Dialog({
									title: "Перезагрузка сервера",
									content: "\
                                <div>\
                                    <p>Выполняется перезапуск служб ГИС ТУСУР.<p>\
                                    </div>\
                                <div class=\"clearfix\" style=\"border-top: dashed 1px #ccc; padding: 5px 0 0 0; margin: 10px 0 0 0\">\
                                    <div class=\"wgs-progress big-progress\">\
                                        <div class=\"wgs-progress__progress wgs-progress__progress-wp\"></div>\
                                        <div class=\"wgs-progress__message\">Пожалуйста, подождите...</div>\
                                    </div>\
                                </div>\
                            ",
									style: "width: 300px"
								});
								waitDialog.show();

								setTimeout(function () {
									location.reload()
								}, 45000);

							});
						}
						break;
					}

				case 'admin-access':
					{
						window.open('/projectadmin');
						break;
					}
				case 'admin-data':
					{
						window.open('/objectadmin');
						break;
					}
				case 'admin-update-db':
					{
						require(['wgs45/controllers/updater'], function (DbUpdater) {
							new DbUpdater();
						})
						break;
					}
				case 'logout':
					{
						location.replace('/authorization/logout');
						break;
					}
				case 'theme-list':
					{
						var TC = new Theme();
						TC.showThemeWindow();

						break;
					}
				case 'card-add':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						console.log('Add card to feature');
						self.createCard(evt.target);
						break;
					}
				case 'card-open':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						self.openCard();
						break;
					}
				case 'cardTable-open':
					{
						console.log(self.cardTable, self);
						if (self.cardTable) {
							self.cardTable.maximize();
						} else {
							self.cardTable = new CardTable(self.windowDock);
							self.cardTable.show();
						}
						break;
					}
				case 'view-settings':
					{
						console.log('open up');
						self.settingsView = new SettingsView(self.windowDock);
						self.settingsView.show();
						break;
					}
				case 'report-1':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						self.printSimpleReport();
						break;
					}
				case 'replace-student':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						self.printReplaceReport(evt.detail.cardIds);
						break;
					}
				case 'place-student':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						self.printPlaceReport(evt.detail.cardIds);
						break;
					}
				case 'election':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						console.log(evt.detail);
						self.printElection(evt.detail.group, evt.detail.type);
						break;
					}
				case 'rectorat':
					{
						//self.currentMapWidget = Fusion.getWidgetById('Map-tusur');
						console.log(evt.detail);
						self.printRectorat(evt.detail.group, evt.detail.type);
						break;
					}

				case 'print-map':
					{

						self.printMap();
						break;
					}
				}

			});
		};
		WgsAppUi.prototype.printMap = function () {

			var mapName = window.WGS_APP.getCurrentMapProps().map.getMapTitle();
			if (!mapName) {
				Helper.error("Не удалось установить имя карты для печати");
				return;
			}

			try {
				Fusion.getWidgetById('QuickPlot-' + mapName).wgsMapName = mapName;
				Fusion.getWidgetById('QuickPlot-' + mapName).activate(); //TODO
				var QuickPlotWidjet = Fusion.getWidgetById('TaskPane-' + mapName); //TODO
				QuickPlotWidjet.activate();
			} catch (e) {
				Helper.error("Не удалось автивировать панель печати");
				return;
			}

			var printDialog = new Dialog({
				title: " Печать карты",
				'class': 'print-dialog',
				content: QuickPlotWidjet.domObj //place our taskPane into Dialog
			});

			printDialog.startup();
			printDialog.show();

			printDialog.onHide = function () {
				Fusion.getWidgetById('QuickPlot-' + mapName).deactivate(); //TODO
				QuickPlotWidjet.deactivate();
				document.getElementById('Modebar-' + mapName).appendChild(QuickPlotWidjet.domObj); //replace our TaskPane back to Modebar to escape it destroying with Dialog
				this.destroyRecursive();
			};

		};


		WgsAppUi.prototype.printReport = function (cardId, uri) {
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "/objectadmin/document/" + (uri || ''));
			form.setAttribute('target', '_blank');

			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("name", 'id');
			hiddenField.setAttribute("value", cardId);
			form.appendChild(hiddenField);
			document.body.appendChild(form); // Not entirely sure if this is necessary
			form.submit();
			document.body.removeChild(form);
		}

		WgsAppUi.prototype.printContract = function (cardId) {
			return this.printReport(cardId, 'contractstudent');
		};

		WgsAppUi.prototype.printRegistry = function (cardId) {
			return this.printReport(cardId, 'registry');
		};


		WgsAppUi.prototype.printForeignRegistry = function (cardId) {
			return this.printReport(cardId, 'PrintForeignRegistry');
		};


		WgsAppUi.prototype.printSimpleReport = function (cardId) {
			return this.printReport(cardId);
		};

		WgsAppUi.prototype.printReplaceReport = function (cardIds) {
			if (!cardIds) {
				Helper.error('Необходимо выделить хотя бы одну карточку');
				return;
			}

			return this.printReport(JSON.stringify(cardIds), 'replacestudent');
		};

		WgsAppUi.prototype.printPlaceReport = function (cardIds) {
			if (!cardIds) {
				Helper.error('Необходимо выделить хотя бы одну карточку');
				return;
			}

			return this.printReport(JSON.stringify(cardIds), 'placestudent');
		};

		WgsAppUi.prototype.printElection = function (layerId, typeId) {
			if (!layerId || !typeId) {
				Helper.error('Выберите тип объектов и слой');
				return;
			}

			return this.printReport(JSON.stringify({
				layer: layerId,
				type: typeId
			}), 'election');
		};

		WgsAppUi.prototype.printRectorat = function (layerId, typeId) {
			if (!layerId || !typeId) {
				Helper.error('Выберите тип объектов и слой');
				return;
			}

			return this.printReport(JSON.stringify({
				layer: layerId,
				type: typeId
			}), 'rectorat');
		};

		WgsAppUi.prototype.parseLayerId = function (layer) {
			var colon = layer.indexOf(':');
			return layer.substring(layer.indexOf('_', colon) + 1, layer.lastIndexOf('_'));
		};

		WgsAppUi.prototype.createCard = function (node, fids) {
			var self = this;
			var create = function (selection) {
				console.log(selection);
				var featureIds = selection.featureIds;
				console.log('create ', featureIds, ' layerName ', selection.layerName);
				self.cardCreateDialog = new CardActionDialog(featureIds[0], null, false, 0, self.parseLayerId(selection.layerName));
				self.cardCreateDialog.initPromise.then(function (cardtypes) {
					self.cardCreateDialog.show(node).then(function (success) {
						Helper.success('Карточка создана');
						self.openCard();
					}, function (error) {
						console.log(error);
					});
				});
			};

			if (fids) {
				create({
					layerName: ':_1_',
					featureIds: fids
				});
			} else this.getSelection().then(function (selection) {
				create(selection[0]);
			});
		};

		WgsAppUi.prototype.openCard = function (isDirectIds, cardId) {
			var self = this;
			if (isDirectIds) {
				new Card([cardId], self.windowDock, true, self).show();
			} else {
				this.getSelection().then(function (selected) {
					/*
					*   selection = [{
					        layerName: 'name',
					        featureIds: [1,2,3]
					    },{
					        layerName: 'name2',
					        featureIds: [1,2,3]
					    }]
					*
					*/
					var selection = selected[0];
					//maximize card if already opened
					if (self.cards[JSON.stringify(selection.featureIds)]) {
						self.cards[JSON.stringify(selection.featureIds)].maximize();
						return;
					}

					var geomProps;
					if (selected.length == 1 && selection.featureIds.length == 1) {
						self.getGeometryProps().then(function (geom) {
							if (geom) {
								var geomGroup = {
									name: 'Геометрические характеристики',
									properties: []
								};
								for (var i in geom) {
									if (geom[i].area) {
										geomGroup.properties.push({
											name: "Площадь",
											value: geom[i].area.toFixed(2) + ' м.кв.',
											readOnly: true,
											id: 'geom-area',
											type: 9
										});
									}
									if (geom[i].length) {
										geomGroup.properties.push({
											name: "Периметр",
											value: geom[i].length.toFixed(2) + ' м.',
											readOnly: true,
											id: 'geom-length',
											type: 9
										});
									}
								}

								var card = self.cards[JSON.stringify(selection.featureIds)] = new Card(selection.featureIds, self.windowDock, false, null, geomGroup.properties.length ? geomGroup : null, self.parseLayerId(selection.layerName));
								card.show();
							}
						});
					} else {
						var fids = [];
						selected.forEach(function (selLayer) {
							fids = fids.concat(selLayer.featureIds);
						});
						var card = new Card(fids, self.windowDock);
						card.show();
					}

				});
			}
		};

		WgsAppUi.prototype.updateByChildCard = function () {
			if (this.cardTable) {
				var dgrids = this.cardTable.tableView.cardDialog.domNode.getElementsByClassName('dgrid-grid');
				[].forEach.call(dgrids, function (dgridElem) {
					var widgetEvent = new CustomEvent('refresh', {
						bubbles: true,
						cancelable: true
					});
					dgridElem.dispatchEvent(widgetEvent);
				});
			}
		};

		WgsAppUi.prototype.getCurrentMapWidgetName = function () {
			var mapTabContainerId = 'map-tab-container';
			var mapWidgetElem = registry.byId(mapTabContainerId).selectedChildWidget.domNode.getElementsByClassName('map__map-widget')[0];

			if (!mapWidgetElem) {
				throw Error("Map Widget not found");
			}

			return mapWidgetElem.id;
		};

		WgsAppUi.prototype.getCurrentMapProps = function () {
			var mapWidgetId = this.getCurrentMapWidgetName();
			var mapName, sessionId, map;
			var Fusion = window.Fusion;
			var allMaps = Fusion.getWidgetById(mapWidgetId).getAllMaps();

			for (var j = 0; j < allMaps.length; j++) {
				if (allMaps[j].arch == 'MapGuide') {
					sessionId = allMaps[j].getSessionID()
					mapName = allMaps[j].getMapName();
					map = allMaps[j];
				}
			}
			if (!sessionId) {
				throw new Error('Session ID is undefined');
			}
			if (!mapName) {
				throw new Error('Map name is undefined');
			}

			return {
				sessionId: sessionId,
				mapName: mapName,
				map: map
			};
		};

		WgsAppUi.prototype.getGeometryProps = function () {
			var Fusion = window.Fusion;
			var mapProps = this.getCurrentMapProps();
			var mapWidgetId = this.getCurrentMapWidgetName();
			var selText = this.model.getSelectionXml(mapProps.mapName);

			return xhr.post("/map/geometry/GetSelectedFeatures", {
				handleAs: "json",
				data: {
					'mapname': mapProps.mapName,
					'selection': selText,
					'session': mapProps.sessionId,
					'version': '4'
				}
			});
		}

		WgsAppUi.prototype.getSelection = function () {
			var Fusion = window.Fusion;
			var mapProps = this.getCurrentMapProps();
			var mapWidgetId = this.getCurrentMapWidgetName();
			var mapWidget = Fusion.getWidgetById(mapWidgetId);
			var self = this;
			var fidsDeferred = new Deferred();

			console.log(this.getCurrentMapWidgetName());


			var selText = self.model.getSelectionXml(mapProps.mapName);
			console.log('!!!', selText, mapProps.mapName);

			if (!selText) {
				return fidsDeferred.reject();
			}


			xhr.post("/map/lib/getselectedfeatures", {
				handleAs: "json",
				data: {
					'mapname': mapProps.mapName,
					'selection': selText,
					'session': mapProps.sessionId,
					'version': '4'
				}
			}).then(function (response) {
				var selectionResponse = [];
				console.log('selection ', response);
				response.forEach(function (responcedLayer, layerIndex) {
					selectionResponse[layerIndex] = {
						layerName: responcedLayer.featureClass,
						featureIds: []
					};
					responcedLayer.features.forEach(function (feature, featureIndex) {
						selectionResponse[layerIndex].featureIds.push(feature.entityId);
					});
				});

				fidsDeferred.resolve(selectionResponse);

			}, function (response) {
				console.error("Error occured while getting selection FIDS");
				fidsDeferred.reject();
			});


			return fidsDeferred;
		}


		WgsAppUi.prototype.openMap = function (mapId) {
			var deferredResult = new Deferred();
			var mapWidgetId = this._mapWidgetIdBase + "-" + mapId;
			var mapTabId = mapWidgetId + "-tab";
			var mapEventsCount = 0; //when 2 - one for legend and one for map itself -- map is ready
			var loaderElem = document.querySelector('.app-loader');

			if (registry.byId(mapTabId)) {
				registry.byId(this._mapTabWidgetId).selectChild(mapTabId);
				setTimeout(function () {
					deferredResult.resolve();
				})

				return deferredResult;
			}

			try {
				if (loaderElem && mapId != "tusur") {
					loaderElem.style.display = "block";
				}

				this.maps[mapId] = new Map(this.getMapDescription(mapId)).startup();

				document.body.addEventListener('map-loaded', mapLoadedEventHandler);
				document.body.addEventListener('wgs-legend-initialized', mapLoadedEventHandler);


			} catch (e) {
				console.log(e);
				Helper.error('Карта не существует или отсутствуют права доступа');
				if (loaderElem) {
					loaderElem.style.display = "none";
				}
			}

			function mapLoadedEventHandler(evt) {
				if (evt.target.id != mapWidgetId && (!evt.detail || evt.detail.mapName != mapId)) {
					return;
				}

				if (++mapEventsCount == 2) {
					deferredResult.resolve();
					document.body.removeEventListener('map-loaded', mapLoadedEventHandler);
					if (loaderElem) {
						loaderElem.style.display = "none";
					}
				}
			}
			return deferredResult;
		};

		WgsAppUi.prototype._setupInitialState = function () {
			var self = this;

			this.appState = {
				//Счетчик, использующийся для перехода в debug версию по нажатию на номер версии в приложении
				_dCounter: 0,

				//list of prohibited user commands
				//received in map.js
				prohibited: {},
				WGS_BUSY: false, //by default, map is not busy, becomes true when loading map
				MapGroupSwitcher: {},


				//переход в debug версию из production по нажатию на какую-нибудь кнопку/div/... два раза
				//Используется счетчик _dCounter
				maybeDebug: function () {
					this._dCounter += 1;
					if (this._dCounter > 1 && !isDebug()) {
						location.assign('/mobile/debug');
					}
				},

				/*
				 * Все функции, перечисленные в selectionCallbacks выполняются по очереди при выделении объекта на
				 * карте (в качестве параметра получают список выделенных объектов в формате Fusion 2.6). Флаг active
				 * используется для активации/деактивации обработчика. Сама функция передается в параметре fn.
				 */
				selectionCallbacks: {
					'console': {
						'fn': function (selectionData) {
							console.log('selectionData ', selectionData);
						},
						'active': isDebug()
					},

					'closeCardCreateDialog': {
						'fn': function (selectionData, isCleared) {
							console.log(isCleared);
							if (self.cardCreateDialog) {
								self.cardCreateDialog.close();
								self.cardCreateDialog = null;
							}
						},
						'active': true
					}
				}
			};
		};

		return WgsAppUi;


		function isDebug() {
			return true; //location.toString().indexOf('debug') > -1;
		}

	});
