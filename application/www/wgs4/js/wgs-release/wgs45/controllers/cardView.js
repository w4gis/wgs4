require({cache:{
'url:wgs45/views/card.ejs':"<%\r\nvar PROPERTY_TYPE_STRING = 1;\r\nvar PROPERTY_TYPE_DATE = 2;\r\nvar PROPERTY_TYPE_INTEGER = 3;\r\nvar PROPERTY_TYPE_REAL = 4; //TODO: check\r\nvar PROPERTY_TYPE_DOMAIN = 5;\r\nvar PROPERTY_TYPE_SOAP = 8;\r\nvar PROPERTY_TYPE_COUNTABLE = 9;\r\n\r\n function renderProperty (props){\r\n    var propertyName = props.name;\r\n    var propertyValue = props.value;\r\n    var propertyId = props.id;\r\n    var readOnly = props.readOnly;\r\n    var placeHolder = props.placeHolder;\r\n    var propertyTypeId = props.type;\r\n%>\r\n\r\n<li class=\"clearfix\">\r\n    <label class=\"card__property-label\">\r\n        <span style=\"float: left; width: 100%;\">\r\n            <span class=\"card__property-label__input-wrapper\">\r\n                <input\r\n                    name=\"property-<%= propertyId %>\"\r\n                    placeholder=\"<%= placeHolder %>\"\r\n                    <%= readOnly ? \"readonly\" : \"\" %>\r\n\r\n                    <% if(propertyTypeId != PROPERTY_TYPE_DATE) { %>\r\n                        data-dojo-type=\"dijit/form/ValidationTextBox\"\r\n                    <% } else { %>\r\n                        data-dojo-type=\"dijit/form/DateTextBox\"\r\n                    <% } %>\r\n\r\n                    <%  var dataPropsString = \"required: false, value: \\\"\" + propertyValue + \"\\\", tooltipPosition: ['below', 'above', 'before', 'after']\";\r\n\r\n                        switch (+propertyTypeId) {\r\n                            case PROPERTY_TYPE_INTEGER:\r\n                                {\r\n                                   dataPropsString += \", invalidMessage: 'Введите целое число, пожалуйста', validator: function(value) { return parseInt(value).toString() === value }\";\r\n                                   break;\r\n                                }\r\n                            case PROPERTY_TYPE_REAL:\r\n                                {\r\n                                   dataPropsString += \", invalidMessage: 'Введите число, пожалуйста', validator: function(value) { return parseFloat(value).toString() === value }\";\r\n                                   break;\r\n                                }\r\n                            case PROPERTY_TYPE_SOAP:\r\n                                {\r\n                                    dataPropsString += \", readOnly: true\";\r\n                                    break;\r\n                                }\r\n                            case PROPERTY_TYPE_COUNTABLE:\r\n                                {\r\n                                    dataPropsString += \", readOnly: true\";\r\n                                    break;\r\n                                }\r\n                            default :\r\n                                {\r\n\r\n                                }\r\n                        }\r\n                    %>\r\n\r\n                    data-dojo-props=\"<%= dataPropsString %>\";\r\n                    />\r\n            </span>\r\n        </span>\r\n        <span class=\"card__property-label__title\">\r\n            <%= propertyName %>:\r\n        </span>\r\n    </label>\r\n</li>\r\n\r\n<% } %>\r\n\r\n<% function renderDomainProperty (propertyName, propertyValue, propertyValues, propertyId){ %>\r\n<li>\r\n    <label data-property-id=\"<%= propertyId %>\">\r\n        <span style=\"float: left; width: 100%;\">\r\n            <span class=\"card__property-label__input-wrapper\">\r\n                <select name=\"<%=propertyId%>\" data-dojo-type=\"dijit/form/Select\">\r\n                    <% if (!propertyValue || propertyValue == \"\") {%>\r\n                        <option value=\"\" selected=\"selected\" disabled=\"disabled\">Выберите значение...</option>\r\n                        <% } %>\r\n                    <% if (propertyValue == \"<...>\") {%>\r\n                        <option value=\"<...>\" selected=\"selected\">&lt;...&gt;</option>\r\n                        <% } %>\r\n                    <% if(propertyValues){\r\n                        propertyValues.forEach (function(domain, domainId) { %>\r\n                        <option value=\"<%= domainId %>\"\r\n                            <% if (domainId == propertyValue){ %> selected=\"selected\" <% } %>>\r\n                            <%= domain.value %>\r\n                        </option>\r\n                    <% });\r\n                    } %>\r\n                </select>\r\n            </span>\r\n        </span>\r\n        <span class=\"card__property-label__title\">\r\n            <%= propertyName %>:\r\n        </span>\r\n    </label>\r\n</li>\r\n\r\n<% } %>\r\n\r\n<div data-dojo-type=\"dijit/layout/BorderContainer\">\r\n\r\n    <div\r\n        data-dojo-type=\"dijit/layout/ContentPane\"\r\n        data-dojo-props=\"region: 'center'\">\r\n        <div data-dojo-type=\"dijit/layout/TabContainer\"\r\n            style=\"width: 300px; height: 300px;\"\r\n            data-dojo-props=\"region: 'center', tabPosition: 'top'\">\r\n            <% for (var i=0; i < cards.length; i++) { %>\r\n            <div\r\n                data-dojo-type=\"dijit/layout/ContentPane\"\r\n                title=\"<%=cards[i].typeName%>\"\r\n                data-dojo-props=\"\">\r\n                <form data-card-id=\"<%= cards[i].id||cards[i].objectTypeId %>\" name=\"<%= cards[i].id||cards[i].objectTypeId %>\" >\r\n\r\n\r\n                    <!-- Основные описания -->\r\n                    <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Основные характеристики', open: true\">\r\n\r\n                    <% if(!isEmpty) { %>\r\n\r\n                        <ul class=\"clearfix\">\r\n                            <% if(cards[i].name != undefined) {\r\n                                renderProperty({\r\n                                    name: \"Наименование\",\r\n                                    value: cards[i].name,\r\n                                    id: 'name',\r\n                                    readOnly: false,\r\n                                    placeHolder: 'Без наименования'\r\n                                });\r\n                            } %>\r\n\r\n                            <% if(cards[i].shortName != undefined) {\r\n                                renderProperty({\r\n                                    name: \"Краткое наименование\",\r\n                                    value: cards[i].shortName,\r\n                                    id: 'shortName',\r\n                                    readOnly: false,\r\n                                    placeHolder: cards[i].name || 'Без наименования'\r\n                                });\r\n                            } %>\r\n\r\n                            <% if(cards[i].createDate != undefined) {\r\n                                renderProperty({\r\n                                    name: \"Дата изменения\",\r\n                                    value: cards[i].createDate,\r\n                                    id: 'date',\r\n                                    readOnly: true,\r\n                                    placeHolder: 'Новая карточка'\r\n                                });\r\n                            } %>\r\n\r\n                            <% if(cards[i].edit_user != undefined) {\r\n                                renderProperty({\r\n                                    name: \"Отредактировано\",\r\n                                    value: cards[i].edit_user,\r\n                                    id: 'user',\r\n                                    readOnly: true,\r\n                                    placeHolder: 'Новая карточка'\r\n                               });\r\n                            } %>\r\n\r\n                            <% if(cards[i].note != undefined) {\r\n                                renderProperty({\r\n                                    name: \"Примечание\",\r\n                                    value: cards[i].note,\r\n                                    id: 'note',\r\n                                    readOnly: false,\r\n                                    placeHolder: 'Примечания и комментарии к объекту...'\r\n                               });\r\n                            } %>\r\n\r\n                        </ul>\r\n\r\n                    <% } else { %>\r\n\r\n                                <div>\r\n                                    Этот объект не имеет атрибутивных описаний\r\n                                </div>\r\n                                <button\r\n                                    data-dojo-type=\"dijit/form/Button\"\r\n                                    type=\"button\"\r\n                                    class=\"card-action\">\r\n                                        <span\r\n                                            class=\"card-action__data-elem\"\r\n                                            data-action=\"card-create-new\">\r\n                                                Добавить\r\n                                        </span>\r\n                                </button>\r\n\r\n                            <% } %>\r\n\r\n                    </div>\r\n\r\n                    <!-- конец основных описаний -->\r\n\r\n\r\n                    <!-- Группы характеристик -->\r\n                    <% cards[i].propertyGroups.forEach (function(group, groupId ) { %>\r\n                        <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: '<%= group.name %>', open: false\">\r\n                            <ul class=\"clearfix\">\r\n                                <% group.properties.forEach(function (property, id ) {\r\n                                    if(property.valueTypeId == PROPERTY_TYPE_DOMAIN) {\r\n                                        renderDomainProperty(property.name, property.value, property.domains, id);\r\n                                    } else renderProperty({\r\n                                        name: property.name,\r\n                                        value: property.value,\r\n                                        readOnly: property.readOnly || false,\r\n                                        id: id,\r\n                                        type: property.valueTypeId\r\n                                    });\r\n                                }) %>\r\n                            </ul>\r\n                        </div>\r\n                    <% }) %>\r\n\r\n                    <!-- Связи -->\r\n\r\n                    <% if(cards[i].relations) for(var relationId in cards[i].relations) {\r\n                        if(!cards[i].relations.hasOwnProperty(relationId))\r\n                            continue;\r\n                        var relation = cards[i].relations[relationId];\r\n                        var relCount = relation.linkedCards ? relation.linkedCards.length : 0;\r\n                        %>\r\n                        <div\r\n                            data-dojo-type=\"dijit/TitlePane\"\r\n                            data-dojo-props=\"title: '<%=relation.name %>: <%= relCount %>', open:  false, 'class' : 'card__relation'\">\r\n                            <div class=\"card__relation__wrapper\">\r\n                                <table class=\"card__relation__list\" data-relation-type-id=\"<%= relationId %>\">\r\n                                    <tr>\r\n                                        <th class=\"column-0\">\r\n                                            Атрибутивное описание\r\n                                        </th>\r\n                                        <th class=\"column-1\">\r\n                                            Действия\r\n                                        </th>\r\n                                    </tr>\r\n                                <% var isFirst = true;\r\n                                   if(relation.linkedCards)\r\n                                    relation.linkedCards.forEach (function(childCard) { %>\r\n                                        <tr class=\"card__relation__nested-record\">\r\n                                            <td class=\"column-0 <%= isFirst ? \"card__nested-card-first\" : \"\" %>\">\r\n                                                <a href=\"#\" data-card-id=\"<%=childCard.id%>\" class=\"card__nested-card\">\r\n                                                    <%= (childCard.name || 'Без наименования') + \" : \" + childCard.typeName %>\r\n                                                </a>\r\n                                            </td>\r\n                                            <td class=\"column-1 <%= isFirst ? \"card__nested-card-first\" : \"\" %>\">\r\n                                                <button\r\n                                                    data-dojo-type=\"dijit/form/Button\"\r\n                                                    type=\"button\"\r\n                                                    class=\"card-action\">\r\n                                                    <span\r\n                                                        class=\"card-action__data-elem\"\r\n                                                        data-action=\"card-unbind\"\r\n                                                        data-obj-relation-id=\"<%= childCard.objRelationId %>\"\r\n                                                        data-relation-id=\"<%= relationId %>\">\r\n                                                        Отвязать\r\n                                                    </span>\r\n                                                </button>\r\n                                            </td>\r\n                                        </tr>\r\n                                        <% isFirst = false;\r\n                                    }); %>\r\n                                </table>\r\n                            </div>\r\n\r\n                            <table class=\"card__relation__actions\">\r\n                                <tr>\r\n                                    <th class=\"column-0\">\r\n                                        Добавить вложенное описание\r\n                                    </th>\r\n                                    <th class=\"column-1\" >\r\n\r\n                                    </th>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td class=\"column-0 card__relation__actions__first\">\r\n                                        <select class=\"card__relation__actions__bind-select\"></select>\r\n                                    </td>\r\n                                    <td class=\"column-1 card__relation__actions__first\">\r\n                                        <button\r\n                                            data-dojo-type=\"dijit/form/Button\"\r\n                                            type=\"button\"\r\n                                            class=\"card-action\">\r\n                                            <span\r\n                                                class=\"card-action__data-elem\"\r\n                                                data-action=\"card-bind\"\r\n                                                data-relation-id=\"<%= relationId %>\"\r\n                                                data-card-id=\"<%= cards[i].id %>\">\r\n                                                Добавить\r\n                                            </span>\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <table class=\"card__relation__actions\">\r\n                                <tr>\r\n                                    <th class=\"column-0\">\r\n                                        Создать новое вложенное описание\r\n                                    </th>\r\n                                    <th class=\"column-1\" >\r\n\r\n                                    </th>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td class=\"column-0 card__relation__actions__first\">\r\n                                        <select data-dojo-type=\"dijit/form/Select\" class=\"card__relation__actions__create-select\">\r\n                                            <% relation.objTypes.forEach(function(type) { %>\r\n                                                <option value=\"<%=type.objTypeRelationId%>\"><%=type.name%></option>\r\n                                            <% }); %>\r\n                                        </select>\r\n                                    </td>\r\n                                    <td class=\"column-1 card__relation__actions__first\">\r\n                                        <button\r\n                                            data-dojo-type=\"dijit/form/Button\"\r\n                                            type=\"button\"\r\n                                            class=\"card-action\">\r\n                                            <span\r\n                                                class=\"card-action__data-elem\"\r\n                                                data-action=\"card-create-child\"\r\n                                                data-relation-id=\"<%= relationId %>\"\r\n                                                data-card-id=\"<%= cards[i].id %>\">\r\n                                                Создать\r\n                                            </span>\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                        </div>\r\n                    <% } %>\r\n\r\n\r\n                </form>\r\n            </div>\r\n\r\n            <% } %>\r\n\r\n        </div>\r\n    </div>\r\n    <div\r\n        data-dojo-type=\"dijit/layout/ContentPane\"\r\n        data-dojo-props=\"region: 'bottom'\">\r\n\r\n        <!-- Form buttons -->\r\n        <% if(!isEmpty) { %>\r\n            <!-- <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__reset\" type=\"reset\"><i class=\"fa fa-undo\"></i> Сбросить изменения</button> -->\r\n            <% if(window.WGS.PRIVILEGE.CARD_EDIT) { %>\r\n              <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__ok\" type=\"button\"><i class=\"fa fa-check\"></i> Ок</button>\r\n              <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__save\" type=\"submit\"><i class=\"fa fa-save\"></i> Сохранить</button>\r\n            <% } else { %>\r\n              <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__ok\" type=\"button\"><i class=\"fa fa-check\"></i> Закрыть</button>\r\n            <% } %>\r\n            <% if(!isMultiple) { %>\r\n                <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__print\" type=\"submit\"><i class=\"fa fa-print\"></i> Печать</button>\r\n                <% if(window.WGS.PRIVILEGE.CARD_EDIT) { %>\r\n                  <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__remove\" type=\"submit\"><i class=\"fa fa-remove\"></i> Удалить</button>\r\n                <% } %>\r\n            <% } %>\r\n\r\n        <% } else { %>\r\n                    <button data-dojo-type=\"dijit/form/Button\" class=\"card__buttons__ok\" type=\"submit\"><i class=\"fa fa-check\"></i> Закрыть</button>\r\n                <% } %>\r\n    </div>\r\n\r\n</div>\r\n",
'url:wgs45/views/card-nested-cards.ejs':"<% var isFirst = true; \r\n   relation.linkedCards.forEach (function(childCard, childCardIndex) {  %>\r\n       <tr class=\"card__relation__nested-record\">\r\n           <td class=\"column-0 <%= isFirst ? \"card__nested-card-first\" : \"\" %>\">\r\n               <a href=\"#\" data-card-id=\"<%=childCard.id%>\" class=\"card__nested-card\">\r\n                   <%= (childCard.name || 'Без наименования') + \" : \" + childCard.typeName %>\r\n               </a>\r\n           </td>\r\n           <td class=\"column-1 <%= isFirst ? \"card__nested-card-first\" : \"\" %>\">\r\n               <button \r\n                   data-dojo-type=\"dijit/form/Button\"\r\n                   type=\"button\" \r\n                   class=\"card-action\">\r\n                   <span\r\n                       class=\"card-action__data-elem\"\r\n                       data-action=\"card-unbind\" \r\n                       data-obj-relation-id=\"<%= childCard.objRelationId %>\"\r\n                       data-relation-id=\"<%= relation.relationId %>\">\r\n                       Отвязать\r\n                   </span>\r\n               </button>\r\n           </td>\r\n       </tr>\r\n       <% isFirst = false \r\n   }); %>"}});
define("wgs45/controllers/cardView", [
    'dijit/registry',
    'dojo/text!wgs45/views/card.ejs',
    'dojo/text!wgs45/views/card-nested-cards.ejs',
    'dojox/layout/FloatingPane',
    'dijit/TitlePane',
    "dojo/parser",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane"],

function(registry, template, relationTemplate, FloatingPane, TitlePane, parser) {
    "use strict";

    function CardView(CardDescriptions, windowsDock, isEmpty, isMultiple) {
        this.cardDescriptions = CardDescriptions;
        this._dock = windowsDock;
        this._isEmpty = isEmpty;
        this._isMultiple = isMultiple;
    }

    CardView.prototype.show = function() {

        var self = this;
        var htmlCard = window.ejs.render(template, {
            cards: this.cardDescriptions,
            isEmpty: self._isEmpty,
            isMultiple: self._isMultiple
        });

        var cardDialogContainerElem = document.createElement("div");
        document.body.appendChild(cardDialogContainerElem);
        var bodyWidth = document.body.clientWidth;

        this.cardDialog = new FloatingPane({
            'title': this.getTitle(),
            'class': 'card-dialog',

            'resizable': true,
            'dockable': true,

            'content': htmlCard,
            'dockTo': this._dock,

            style: 'left: ' + (bodyWidth / 2 - 350) + 'px',

            onShow: function() {
                console.log('SHOW');
                registry.byId('appLayout').resize();
            }
        }, cardDialogContainerElem);

        this.cardDialog.startup();
        this.cardDialog.domNode.getElementsByClassName('dojoxFloatingCloseIcon')[0].addEventListener('click', function() {
            self._onClose();
        });
        
        this.cardDialog.bringToTop();
        this._registerEventHandlers();
        
        this.resize();

        //registry.byId('card-tab').selectChild(registry.byId('card-tab').getChildren()[0]);
        // parser.parse(cardDialog.containerNode);

    };
    
    CardView.prototype._onClose = function() {
        if(window.WGS_APP && window.WGS_APP.cards[this.ids]) {
            window.WGS_APP.cards[this.ids] = null;
        }
    }

    CardView.prototype.updateNestedCards = function(cardDescriptions, relationTypeId) {
        console.log('carddesc', cardDescriptions, relationTypeId);
        var relationUlElems = this.cardDialog.domNode.getElementsByClassName('card__relation__list');
        console.log('relul ', relationUlElems);
        var targetElems = [];
        [].forEach.call(relationUlElems, function(ul) {
            console.log('reltypeids ', ul.dataset.relationTypeId, ' ', relationTypeId);
            if (ul.dataset.relationTypeId == relationTypeId) {
                targetElems.push(ul);
            }
        });

        if (targetElems) {

            cardDescriptions.forEach(function(card, index) {
                
                for (var relationId in card.relations) {
                    if(!card.relations.hasOwnProperty(relationId))
                        continue;
                    var relation = card.relations[relationId];
                    if (relationId == relationTypeId) {
                        var htmlRelations = window.ejs.render(relationTemplate, {
                            relation: relation
                        });
                        [].forEach.call(targetElems, function(targetElem) {
                            //replace old relation cards with new ones

                            //while there are <tr> for child cards, remove them
                            var nestedRowElems = targetElem.getElementsByClassName('card__relation__nested-record');
                            while (nestedRowElems.length > 0) {
                                var nestedRowElem = nestedRowElems[0];
                                nestedRowElem.parentElement.removeChild(nestedRowElem);

                                nestedRowElems = targetElem.getElementsByClassName('card__relation__nested-record');
                            }

                            //add trs for current child cards
                            var nestedRowTableElem = targetElem.getElementsByTagName('tr')[0].parentElement;
                            nestedRowTableElem.innerHTML += htmlRelations;

                            //parser unbind dojo buttons
                            parser.parse(nestedRowTableElem);


                            //update relation count
                            var relationsCount = 0;
                            relation.linkedCards.forEach(function() {
                                relationsCount++;
                            });
                            registry.byId(targetElem.closest('.dijitTitlePane').getAttribute('widgetid')).set('title', relation.name + ": " + relationsCount);
                        });
                    }
                }
            });

        }

    };

    CardView.prototype.createRelationSelectWidget = function(elem) {
        require([
            "dojo/store/Memory", "dijit/form/FilteringSelect", 'dojo/store/JsonRest'], function(Memory, FilteringSelect, JsonRestStore) {

            var relationButtons = elem.closest('.dijitTitlePane').getElementsByClassName('card-action__data-elem'); //[0].dataset.relationId;
            var relationId;

            [].forEach.call(relationButtons, function(buttonElem) {
                if (buttonElem.dataset.action == "card-bind") {
                    relationId = buttonElem.dataset.relationId;
                }
            });

            console.log(relationId);
            var stateStore = new JsonRestStore({
                target: '/objectadmin/card/getchildcardsbyrelationidandname/?relationId=' + relationId
            });

            var filteringSelect = new FilteringSelect({
                name: "name",
                placeHolder: "Карточка...",
                store: stateStore,
                searchAttr: "name",
                searchDelay: 500
            }, elem).startup();
        });
    };
    CardView.prototype.getTitle = function() {
        var title;
        if (this.cardDescriptions[0] && this.cardDescriptions[0].name) {
            title = this.cardDescriptions[0].name;
        } else {
            title = 'Без наименования';
        }

        title += " :: Карточка объекта";
        return title;
    };

    CardView.prototype.close = function() {
        this.cardDialog.close();
        this._onClose();
    };

    CardView.prototype.resize = function() {
        if(!this.cardDialog) {
            return;
        }
        var self = this;
        var windowOffset = 20;
        
        var thisWidth = self.cardDialog.domNode.clientWidth;
        var thisHeight = self.cardDialog.domNode.clientHeight;
        var bodyWidth = document.body.clientWidth - windowOffset;
        var bodyHeight = document.body.clientHeight - windowOffset;
        var resizeFlag = false;
        var resizeParams = {};

        if (bodyWidth < thisWidth) {
            thisWidth = bodyWidth;
            resizeFlag = true;
            resizeParams.w = thisWidth;
            resizeParams.l = windowOffset / 2;
        }

        if (bodyHeight < thisHeight) {
            thisHeight = bodyHeight;
            resizeFlag = true;
            resizeParams.h = thisHeight;
            resizeParams.t = windowOffset / 2;
        }

        if (resizeFlag) {
            self.cardDialog.resize(resizeParams);
        }
    };

    CardView.prototype._registerEventHandlers = function() {
        var self = this;
        console.log('REGISTER EVENTS!');
        var resizeTimeout;
        //RESIZE WINDOW ON BODY RESIZE
        window.addEventListener('resize', function() {
            // prevent calling event handler too often
            if (!resizeTimeout) {
                resizeTimeout = setTimeout(function() {
                    self.resize();
                    resizeTimeout = null;
                }, 500);
            }
        });

        //MINIMIZE WINDOW
        this.cardDialog.domNode.getElementsByClassName('dojoxFloatingMinimizeIcon')[0].addEventListener('click', function() {
            setTimeout(function() {
                registry.byId('appLayout').resize();
            }, 1000);
        });

        //OPEN NESTED CARDS
        [].forEach.call(this.cardDialog.domNode.getElementsByClassName('card__relation'), function(cardRelationPaneElem) {
            var selectElem = cardRelationPaneElem.getElementsByClassName('card__relation__actions__bind-select')[0];
            console.log(selectElem);
            self.createRelationSelectWidget(selectElem);
        });
    };
    return CardView;
});