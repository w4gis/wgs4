define("wgs45/controllers/cardActionDialog", ['dojo/Deferred',
	"dijit/ConfirmTooltipDialog",
	"dijit/popup",
    'dijit/registry',
    'wgs45/controllers/helper',
    'wgs45/model/cardModel'], function(Deferred, ConfirmTooltipDialog, popup, registry, Helper, CardModel){
	"use strict";

	function CardActionDialog(objectId, model, leaving, type, filterLayer){
		this._objectId = objectId;
        if(leaving !== undefined) {
            this._leaving = leaving;
        }
        else this._leaving = true;

        console.log(filterLayer);

		this._model = model || new CardModel();
        if(type !== undefined) {
            this._type = type;
        }
        else this._type = 1; // 0 - create card, 1 - remove
        if(this._type === 0) {

            this.initPromise = new Deferred();

            var self = this;

            self._model.getCardTypes(filterLayer, objectId).then(function(types){

                self.types = types;
                self.initPromise.resolve(types);

            }, function(error){

                self.initPromise.reject(error);

            });
        }
	}

	CardActionDialog.prototype.show = function(node) {
		console.log("show");
        var self = this;
        this.dialogDeferred = new Deferred();

        var content = "";
        var buttonOk = "";

        switch(this._type){
            case 0:
                    console.log(self.types);
                    content = '<h2>Добавление новой карточки:</h2></br>' +
                    '<select name="cardTypeSelector" data-dojo-type="dijit/form/Select">'+
                    '<option value="sel" disabled="disabled">Выберите тип:</option>';
                    self.types.forEach(function(type, index){
                        content += '<option value="'+type.id+'">'+type.name+'</option>';
                    });
                    content+='</select>';
                    buttonOk = "Добавить";
                    break;
                break;
            case 1: content = '<h2>Вы уверены? Удаление необратимо</h2></br>'; 
                //'<input type="checkbox" data-dojo-type="dijit/form/CheckBox" name="name">&nbsp;Вместе с подкарточками';
                buttonOk = "Удалить";
                break;
        }

        if(!this.myDialog){
            this.myDialog = new ConfirmTooltipDialog({
            content: content,
            buttonOk: buttonOk,
            onCancel: function() {
                            console.log('cancel button');
                			self.close();
            			  },
    	    onExecute: function() {
                            console.log('execute button');
    	   				    var param;
                            if(self._type === 0) {
                                var selector = registry.byId(self.myDialog.domNode.getElementsByTagName('table')[0].getAttribute('widgetid'));
                                param = selector.get('value');
                                console.log(param);
                            } else if(self._type == 1) {
                                param = 0;//registry.byId(self.myDialog.domNode.getElementsByTagName('input')[0].parentNode.getAttribute('widgetid')).checked ? 0 : 1;
                            }
                            if(param == 'sel') {
                                Helper.warning('Необходимо выбрать тип новой карточки');
                                return;
                            }
                			popup.close(self.myDialog);
                            self.popupIsClosed = true;

                			self.act(param).then(function(success) {
                                Helper.success(self._type === 0? 'Карточка создана' : 'Карточка удалена');
                				self.dialogDeferred.resolve(success);
                			}, function(err) {
                                Helper.error('Ошибка при работе с карточкой');
                                self.dialogDeferred.reject(err);
                            });
            			  },
            onMouseLeave: function() {
                            console.log('mouse leave');
                            if(self._leaving && !self.popupIsClosed)
                                self.close();
            			  }
    	   });
        }

    	popup.open({
            popup: self.myDialog,
            around: node
        });
        this.popupIsClosed = false;

        return this.dialogDeferred;
	};

    CardActionDialog.prototype.close = function() {
        // if(!self.popupIsClosed) {
            popup.close(self.myDialog);
            self.popupIsClosed = true;
            if(self.dialogDeferred)
                self.dialogDeferred.cancel();
        // }
    };

    CardActionDialog.prototype.act = function(param) {
        switch(this._type) {
            case 0: return this.createObject(param);
            break;
            case 1: return this.removeObject(param);
            break;
        }

        return null;
    };

    CardActionDialog.prototype.createObject = function(typeId) {
        console.log('for ', this._objectId, 'created', typeId);
        return this._model.createObjectForFeature(typeId, this._objectId);
    };

	CardActionDialog.prototype.removeObject = function(removeMode) {
		console.log(this._objectId, 'is removed', removeMode);
		return this._model.removeObject(this._objectId, removeMode);
	};

	return CardActionDialog;
});