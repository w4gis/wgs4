//Fusion.getWidgetById('Map').loadMapGroup(Fusion.applicationDefinition.mapGroups['openLayers-fet']);

define("wgs45/controllers/legend", [
    'wgs45/model/wgs-fusion',
    'dijit/form/Select',
    'dijit/layout/ContentPane',
    'wgs45/controllers/map-controls',
    'dijit/registry'], function(WgsFusion, Select, ContentPane, MapControls, registry) {

    function Legend(options) {
        this._mapName = options.mapName || 'tusur';
        this._mapWidgetIdBase = options.widgetId || 'Map';
        // this._title = options.title || "Здания ТУСУР"

        this.legendLoadEventHandler = this.legendLoadEventHandler.bind(this);
    }

    Legend.prototype.legendLoadEventHandler = function() {
        console.log('Init wgs Super legend', document.getElementById('legend-' + this._mapName), this);
        // if(!document.getElementById('legend-' + this._mapName)) return;
        this._floorListItemElems = document.getElementById('legend-' + this._mapName).getElementsByClassName('wgs-legend__folder-floorplan');


        var floors = this.getFirstLevelGroups();
        console.log('FLOORS', floors);
        if(this.floorSelectWidget) {
            this.floorSelectWidget.destroyRecursive();
        }

        this.createLayerGroupToggleWidget(floors);

    };

    Legend.prototype.getFirstLevelGroups = function() {
        var floors = [];
        [].forEach.call(this._floorListItemElems, function(floorListItem, i) {
           floors.push({
               value: 'value-' + i,
               label: floorListItem.dataset.floorplanLabel
           });
        });

        return floors;
    };

    Legend.prototype.createLayerGroupToggleWidget = function(floors) {

        var self = this;
        var legendTreeElem = document.getElementById('legend-' + this._mapName);
        var legendContainerElem = document.getElementById('legend-' + this._mapName + "-container");

        this.selectContainerElem = document.createElement('div');
        this.selectContainerElem.classList.add('legend__floor-select');

        if(!legendContainerElem || !legendTreeElem) return;


        legendContainerElem.insertBefore(this.selectContainerElem, legendTreeElem);

        if(floors.length) {
            this.floorSelectWidget = new Select({
                options: floors,
                'class' : 'wgs-legend__select'
            }, this.selectContainerElem);

            this.floorSelectWidget.on("change", function(){
                console.log("my value: ", this.get("value"));
                self._setupLayerElemsVisibility();
                var widgetEvent = new CustomEvent("legend-selection", {
                    bubbles: true,
                    detail: {
                        mapName: self._mapName
                    }
                });

                var value = this.get('value').split('-')[1];
                window.WGS_APP.maps[self._mapName]._selectedGroup = value;

                document.body.dispatchEvent(widgetEvent);
            });
            this.floorSelectWidget.startup();

            var layerGroupElems = legendContainerElem.getElementsByClassName('wgs-legend__folder-floorplan');
            var initialValue = 0;
            [].forEach.call(layerGroupElems, function(elem, index) {
                var checkboxContainerElem = elem.getElementsByClassName('fusionLegendCheckboxContainer')[0];
                var inputElem;
                if(checkboxContainerElem && checkboxContainerElem.closest('.wgs-legend__folder-floorplan') == elem /* А вдруг это не первый вложенный элемент*/) {
                    inputElem = checkboxContainerElem.getElementsByClassName('fusionLegendCheckboxInput')[0];
                    if(inputElem && inputElem.checked) {
                        initialValue = index;
                    }
                }
            });

            console.log('initial lg', initialValue);
            // Set initial layers visibility
            this.floorSelectWidget.set('value', 'value-' + (initialValue || 0));
            self._setupLayerElemsVisibility();
        }
    };

    Legend.prototype._setupLayerElemsVisibility = function() {
        console.log("CALLED SETUP LAYER VISIBILITY")
        var self = this;
        var select = this.floorSelectWidget;
        [].forEach.call(self._floorListItemElems, function(floorElem, i) {
            var dojoCheckBoxElem = floorElem.getElementsByClassName('fusionLegendCheckboxInput')[0].nextElementSibling;
            if(dojoCheckBoxElem) {
                var dojoCheckBoxId = dojoCheckBoxElem.getAttribute('widgetid');

                setTimeout(function() {
                  // console.log('!!!!!!!');
                    registry.byId(dojoCheckBoxId).set('checked', 'value-' + i == select.get('value'));
                    // var rc = floorElem.getElementsByClassName('fusionLegendCheckboxInput')[0];
                    // console.log(rc);
                    // rc.checked = i != select.get('value');
                    // var evt = document.createEvent("HTMLEvents");
                    // evt.initEvent("change", false, true);
                    // rc.dispatchEvent(evt);
                }, 100 * i);


                // floorElem.getElementsByClassName('fusionLegendCheckboxInput')[0].dispatchEvent(new MouseEvent('click', {
                //     bubbles: true,
                //     cancelable: true
                // }));
                floorElem.style.display = 'value-' + i == select.get('value') ? "" : "none";
            }
        });
    }

    Legend.prototype.destroy = function() {
        var self = this;
        console.log('call destroy for select');
        if(this.floorSelectWidget) {
            this.floorSelectWidget.destroyRecursive();
        }
        // document.body.appendChild(document.getElementById('legend-' + this._mapName));

        // require(['dojo/dom-construct'], function(domConstruct) {
        //     var legendContainerElem = document.getElementById('legend-' + self._mapName + "-container");
        //     if(legendContainerElem) {
        //         // domConstruct.empty(legendContainerElem);
        //         legendContainerElem.parentElement.removeChild(legendContainerElem);
        //     }
        // });
    }
    return Legend;
});
