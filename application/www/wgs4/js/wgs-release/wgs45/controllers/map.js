//Fusion.getWidgetById('Map').loadMapGroup(Fusion.applicationDefinition.mapGroups['openLayers-fet']);
define("wgs45/controllers/map", [
	'wgs45/model/wgs-fusion',
	'dijit/layout/ContentPane',
	'wgs45/controllers/map-controls',
	'wgs45/controllers/legend',
	'dijit/registry'
], function (WgsFusion, ContentPane, MapControls, Legend, registry) {
	function Map(options) {
		this._mapName = options.mapName || 'tusur';
		this._mapWidgetIdBase = options.widgetId || 'Map';
		this._title = options.title || "Р—РґР°РЅРёСЏ РўРЈРЎРЈР ";
		this._initLegend(options);
		this._mapLoadEventHandler = this._onMapLoad.bind(this);
		this._fusionLoadEventHandler = this._onFusionLoad.bind(this);
		this._fusionInitEventHandler = this._configureFusionApplication.bind(this);
		this._hasSelectionEventHandler = this._hasSelectionEventHandler.bind(this);
		this._mapIsBusy = false;
		this._mapBusyEventHandlerFunction = this._mapBusyEventHandler.bind(this);
		this._resizeInterval = null;
	}

	Map.prototype._mapBusyEventHandler = function () {
		var map = window.Fusion.getWidgetById(this._getMapWidgetId());
		// console.log(map.isBusy())
		document.getElementById(this._getMapWidgetTabId()).getElementsByClassName('map__progress')[0].style.display = map.isBusy() ? "" : "none";
	};

	Map.prototype.startup = function () {
		this._getMapWidgetContainer();
		this._startFusionApplication(this._mapName);
		return this;
	};

	Map.prototype.getTitle = function () {
		return this._title;
	};

	Map.prototype._getMapWidgetContainer = function () {
		var self = this;
		var mapContainer = registry.byId(this._getMapWidgetTabId());
		if (!this.mapContainer) {
			this.mapContainer = new ContentPane({
				'id': this._getMapWidgetTabId(),
				'closable': self._mapName !== "tusur", //TODO: РіСЂСЏР·РЅС‹Р№ С…Р°Рє
				'content': this._getMapPaneLayout(),
				'title': this.getTitle(),
				'class': 'map__map-pane',
				onShow: function () {
					console.log('SHOW', document.getElementById('legend-' + self._mapName));
					document.getElementById('legend-' + self._mapName + "-container").classList.add('visible');
					document.body.addEventListener('wgs-legend-loaded', self._legendEventHandler);
					document.body.addEventListener('legend-selection', self._legendChangeEventHandler);
					self._resizeInterval = setInterval(function () {
						self._onMapDivResize();
					}, 2500);
				},
				onHide: function () {
					console.log('HIDE');
					document.getElementById('legend-' + self._mapName + "-container").classList.remove('visible');
					// document.getElementById(self._getMapWidgetTabId()).onresize = function() {}
					document.body.removeEventListener('wgs-legend-loaded', self._legendEventHandler);
					document.body.removeEventListener('legend-selection', self._legendChangeEventHandler);
					clearInterval(self._resizeInterval);
				},
				onClose: function () {
					var Fusion = window.Fusion;
					clearInterval(self._resizeInterval);
					document.body.removeEventListener('wgs-legend-loaded', self._legendEventHandler);
					document.body.removeEventListener('legend-selection', self._legendChangeEventHandler);
					if (self._legend) {
						self._legend.destroy();
					}
					self.legendContainerElem.parentElement.removeChild(self.legendContainerElem);
					registry.findWidgets(document.getElementById(self._getMapWidgetTabId())).forEach(function (nestedWidget) {
						nestedWidget.destroyRecursive();
					});
					Fusion.getWidgetById(self._getMapWidgetId()).deregisterForEvent(Fusion.Event.MAP_LOADED, self._fusionLoadEventHandler);
					Fusion.getWidgetById(self._getMapWidgetId()).deregisterForEvent(Fusion.Event.MAP_RELOADED, self._mapLoadEventHandler);
					Fusion.getWidgetById(self._getMapWidgetId()).deregisterForEvent(Fusion.Event.MAP_SELECTION_ON, this._hasSelectionEventHandler);
					Fusion.getWidgetById(self._getMapWidgetId()).deregisterForEvent(Fusion.Event.MAP_SELECTION_OFF, this._hasSelectionEventHandler);
					return true;
				}
			});
			registry.byId('map-tab-container').addChild(this.mapContainer);
			registry.byId('map-tab-container').selectChild(this.mapContainer);
			self._mapWidth = document.getElementById(self._getMapWidgetId()).clientWidth;
			self._mapHeight = document.getElementById(self._getMapWidgetId()).clientHeight;
		}
		return this.mapContainer;
	};

	Map.prototype._initLegend = function (options) {
		this.legendContainerElem = document.createElement('div');
		this.legendContainerElem.id = 'legend-' + this._mapName + "-container";
		this.legendContainerElem.classList.add("map__legend");
		document.getElementById('legend').appendChild(this.legendContainerElem);
		var self = this;
		this._legend = new Legend(options);
		this._legendChangeEventHandler = function (evt) {
			var Fusion = window.Fusion;
			if (evt.detail.mapName == self._mapName) {
				Fusion.getWidgetById(self._getMapWidgetId()).clearSelection();
			}
		}.bind(this);
		this._legendEventHandler = function (evt) {
			console.log('CALLED FROM MAP')
			self._legend.legendLoadEventHandler();
			var widgetEvent = new CustomEvent("wgs-legend-initialized", {
				bubbles: true,
				detail: evt.detail
			});
			document.body.dispatchEvent(widgetEvent);
			// self.legendLoaded = true;
		}.bind(this);
	};

	Map.prototype._getMapPaneLayout = function () {
		var mapWidgetId = this._getMapWidgetId();
		var widgetIdUniqueStr = this._mapName;
		return '\
        <div class="map__map-widget" id="' + mapWidgetId + '">\
             <div id="stats-' + mapWidgetId + '__container" class="map-widget__stats-panel">\
             <p>Занято мест   : <span id="currentPlaces-' + mapWidgetId + '"></span></p>\
             <p>Из них : <span id="sexOfStudents-' + mapWidgetId + '"></span></p>\
             <p>Свободно мест : <span id="freePlaces-' + mapWidgetId + '"></span></p>\
             <p>Всего мест : <span id="maxPlaces-' + mapWidgetId + '"></span></p>\
             </div>\
             <div id="room-stats-' + mapWidgetId + '__container" class="map-widget__room-stats-panel"></div>\
         </div>\
          <div id="legend-' + widgetIdUniqueStr + '"></div>\
          <div><div id="Maptip-' + widgetIdUniqueStr + '"></div></div>\
          <div class="wgs-progress map__progress">\
              <div class="wgs-progress__progress"></div>\
              <div class="wgs-progress__message"></div>\
          </div>\
          <div class="map__zoom-toolbar" id="zoom-toolbar-' + widgetIdUniqueStr + '">\
              <button id="ZoomInFixed-' + widgetIdUniqueStr + '-button"></button>\
              <button id="ZoomOutFixed-' + widgetIdUniqueStr + '-button"></button>\
          </div>\
          <div id="status-bar-' + widgetIdUniqueStr + '" class="map__status-bar">\
              <div class="map__status-bar__item" id="StatusScale-' + widgetIdUniqueStr + '"></div>\
              <div class="map__status-bar__item" id="StatusCoords-' + widgetIdUniqueStr + '"></div>\
              <div class="map__status-bar__item" id="StatusViewSize-' + widgetIdUniqueStr + '"></div>\
              <div class="map__status-bar__item wgs-version-container"><span class="wgs-version">WGS4 ' + window.WGS_APP.getVersion() + '</span></div>\
          </div>\
          <div class="map__toolbar" id="toolbar-' + widgetIdUniqueStr + '"></div>\
          <div class="mode-bar" id="Modebar-' + widgetIdUniqueStr + '">\
              <div id="MapMenu-' + widgetIdUniqueStr + '"></div>\
              <div id="Dop"></div>\
              <div id="Pan-' + widgetIdUniqueStr + '"></div>\
              <div id="Select-' + widgetIdUniqueStr + '"></div>\
              <div id="PreviousView-' + widgetIdUniqueStr + '"></div>\
              <div id="NextView-' + widgetIdUniqueStr + '"></div>\
              <div id="InitialMapView-' + widgetIdUniqueStr + '"></div>\
              <div id="ZoomIn-' + widgetIdUniqueStr + '"></div>\
              <div id="ZoomInFixed-' + widgetIdUniqueStr + '"></div>\
              <div id="ZoomOutFixed-' + widgetIdUniqueStr + '"></div>\
              <div id="SelectPolygon-' + widgetIdUniqueStr + '"></div>\
              <div id="Measure-' + widgetIdUniqueStr + '"></div>\
              <div id="SelectRadiusValue-' + widgetIdUniqueStr + '"></div>\
              <div id="TaskPane-' + widgetIdUniqueStr + '"></div>\
              <div id="QuickPlot-' + widgetIdUniqueStr + '"></div>\
          </div>';
		//<div class="map__status-bar__item" id="StatusSelection-' + widgetIdUniqueStr + '"></div>\
		//<div id="SelectRadius-' + widgetIdUniqueStr + '"></div>\
	};

	Map.prototype._getMapWidgetId = function () {
		return this._mapWidgetIdBase + "-" + this._mapName;
	};

	Map.prototype._getMapWidgetTabId = function () {
		return this._mapWidgetIdBase + "-" + this._mapName + "-tab";
	};

	Map.prototype._configureFusionApplication = function () {
		var Fusion = window.Fusion;
		console.log('FUSION REGISTER FOR EVENT!')
		Fusion.getWidgetById(this._getMapWidgetId()).registerForEvent(Fusion.Event.MAP_LOADED, this._fusionLoadEventHandler);
		Fusion.getWidgetById(this._getMapWidgetId()).registerForEvent(Fusion.Event.MAP_RELOADED, this._mapLoadEventHandler);
		Fusion.getWidgetById(this._getMapWidgetId()).registerForEvent(Fusion.Event.MAP_BUSY_CHANGED, this._mapBusyEventHandlerFunction);
	};

	Map.prototype._onMapDivResize = function () {
		var Fusion = window.Fusion;
		if (this._mapWidth != document.getElementById(this._getMapWidgetId()).clientWidth || this._mapHeight != document.getElementById(this._getMapWidgetId()).clientHeight) {
			Fusion.getWidgetById(this._getMapWidgetId()).resize();
		};
	}

	Map.prototype._onMapLoad = function () {
		document.getElementById(this._getMapWidgetId()).dispatchEvent(new CustomEvent('map-reloaded', {
			bubbles: true
		}));
	}

	Map.prototype._onFusionLoad = function () {
		var Fusion = window.Fusion;
		var mapWidget = Fusion.getWidgetById(this._getMapWidgetId());
		this.legendContainerElem.appendChild(document.getElementById('legend-' + this._mapName));
		this._mapControls = new MapControls({
			mapName: this._mapName
		});
		this._mapControls.createControls();
		this._initializeSelectionEvents();
		document.getElementById(this._getMapWidgetId()).dispatchEvent(new CustomEvent('map-loaded', {
			bubbles: true
		}));
		mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_ON, this._hasSelectionEventHandler);
		mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_OFF, this._hasSelectionEventHandler);
	};

	Map.prototype._hasSelectionEventHandler = function () {
		var Fusion = window.Fusion;
		var mapWidget = Fusion.getWidgetById(this._getMapWidgetId());
		console.log('mapWidget ', mapWidget);
		var mapName;
		var allMaps = mapWidget.getAllMaps();
		for (var j = 0; j < allMaps.length; j++) {
			if (allMaps[j].arch == 'MapGuide') {
				mapName = allMaps[j].getMapName();
			}
		}
		if (!mapName) {
			throw new Error('Map name is undefined');
		}
		var selText = window.WGS_APP.selText[mapName];
		var widgetEvent = new CustomEvent("map-selection", {
			bubbles: true,
			detail: {
				hasSelection: mapWidget.hasSelection(),
				numSelected: selText.split('<ID>').length - 1
			}
		});
		console.log('fired at:', document.getElementById(this._getMapWidgetId()))
		document.getElementById(this._getMapWidgetId()).dispatchEvent(widgetEvent);
	}

	Map.prototype._startFusionApplication = function (mapName) {
		var self = this;
		var Fusion = window.Fusion;
		new WgsFusion(); // redefine Fusion widgets to add some Dojo (TM).
		//get localized Application Definition
		var locale = localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale').substr(0, 2) : '';
		var options = {
			//sessionId: getCookie('PHPSESSID'),
			applicationDefinitionURL: "/map/fusion/getApplicationDefinition?lang=" + locale + "&map=" + mapName
		};
		//if ANYTHING wrong happens
		function onFusionError(e) {
			alert("Error occured, see console for details");
			console.error('Fusion error: ', e);
		}
		var loadInterval = setInterval(function () {
			console.log(Fusion.loadState);
			if (Fusion.loadState == Fusion.LOAD_COMPLETE) {
				self._fusionInitEventHandler();
				clearInterval(loadInterval);
			}
		}, 300);
		// Fusion.registerForEvent(Fusion.Event.FUSION_INITIALIZED, );
		// Fusion.registerForEvent(Fusion.Event.FUSION_ERROR, onFusionError);
		Fusion.initialize(options);
	};

	Map.prototype._initializeSelectionEvents = function () {
		var Fusion = window.Fusion;
		var mapWidget = Fusion.getWidgetById(this._getMapWidgetId());
		// call each ACTIVE selection callbacks
		// @selection - selection object from Fusion
		// @isSelectionCleared - true when nothing selected
		function doActiveCallbacksWithSelection(selection, isSelectionCleared) {
			//display the selection to the user in some way ...
			var c = window.WGS_APP.appState.selectionCallbacks;
			console.log('SELECTION XML', window.selText);
			for (var i in c) {
				if (c[i].active) {
					c[i].fn(selection, isSelectionCleared);
				}
			}
		}

		function doSelectionOff(selection) {
			doActiveCallbacksWithSelection(selection, true);
		}

		function doSelectionOn(selection) {
			doActiveCallbacksWithSelection(selection, false);
		}
		//clear the selection results
		function selectionOffEventHandler() {
			mapWidget.getSelection(doSelectionOff);
			console.log('selection cleared');
		}
		//a new selection has been made, request it
		function selectionOnEventHandler() {
			mapWidget.getSelection(doSelectionOn);
		}
		//setup selection events callbacks
		mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_ON, selectionOnEventHandler);
		mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_OFF, selectionOffEventHandler);
	}

	return Map;
});
