define("wgs45/controllers/theme", ['wgs45/model/theme',
	"dojo/Deferred",
	'wgs45/views/theme/themeList',
	'wgs45/views/theme/themeEdit',
	'wgs45/views/theme/themeSetup',
	'wgs45/views/theme/themeLayerEdit',
	"dijit/form/ComboBox",
	"dojo/store/Memory",
	'dijit/registry',
	'dojo/on',
	"dijit/form/Button",
	"dijit/layout/ContentPane",
	"wgs45/controllers/helper"
	], function(themeModel,  Deferred, ThemeListView, ThemeEditView, ThemeSetupView, ThemeLayerEditView, ComboBox, Memory, registry, on, Button, ContentPane, Helper){

	function ThemeController() {
		this.loaderElem = document.querySelector('.app-loader');
		this.themeListView = new ThemeListView();
		this.themeEditView = new ThemeEditView();
		this.themeSetupView = new ThemeSetupView();
		this.themeLayerEditView = new ThemeLayerEditView();
		this.featureTypesTemplate = [
			{	"name": "Точка",
				"id": 1},
			{	"name": "Линия",
			"id": 2},
			{	"name": "Полигон",
			"id": 3},];
		this.styleTemplate = {"Fill_FillPattern":"Solid",
					"Fill_ForegroundColor":"0dee20",
					"Fill_BackgroundColor":"000000",
					"Stroke_LineStyle":"Solid",
					"Stroke_Unit":"Centimeters",
					"Stroke_Color":"000000",
					"Srtoke_Thickness":"0",
					"Stroke_SizeContext":"DeviceUnits",
					"TransparencyPercent":0,
					"Transparency":""};
		this.numberConditionsArray = [
				{	'title': 'Больше',
					'value': '>'},
				{	'title': 'Больше или равно',
					'value': '>='},
				{	'title': 'Равно',
					'value': '='},
				{	'title': 'Меньше или равно',
					'value': '<='},
				{	'title': 'Меньше',
					'value': '<'} ]; //+ не определено
		this.textConditionsArray = [
				{	'title': 'Равно',
					'value': 'EQUAL'},
				{	'title': 'Не равно',
					'value': '!='},
				{	'title': 'Содержит',
					'value': 'LIKE'} ];
		this.domainConditionsArray = [
				{	'title': 'Равно',
					'value': '='},
				{	'title': 'Не равно',
					'value': '!='} ];
		this.recursiveDeep = 3; //максимальная глубина вложенности внутри элемента, где может располагаться target с нужными нам параметрами
	}

	ThemeController.prototype.showThemeWindow = function() {
		var self = this;
		if (self.loaderElem) {
			self.loaderElem.style.display = "block";
		}
		themeModel.get(function(err, themeList){
			if (err) {
				if (self.loaderElem) {
					self.loaderElem.style.display = "none";
				}
				Helper.error("Ошибка получения списка тематических отчетов");
				return;
			}
			Helper.hide();
			var maps = window.WGS_APP.getMaps();
		//	console.log(maps);
			themeList.forEach(function(theme, i){
				theme.MAPTITLE = findMap(theme.MAP, maps) ? findMap(theme.MAP, maps).title : 'notitle';
			});
			//sorting themeList firstly by MAPTITLE, then by MAP and finally by NAME
			// if MAPTITLE=notitle for all, we will sort by MAP
			themeList.sort(dynamicSortMultiple("MAPTITLE", "MAP", "NAME"));
			if (self.loaderElem) {
				self.loaderElem.style.display = "none";
			}
			self.themeListView.showThemeListDialog(themeList, self.registryThemeListDialogEvents.bind(self));
		});
	}

	ThemeController.prototype.registryThemeListDialogEvents = function(themeList) {
		var self = this;

		var themeWindow = document.getElementsByClassName('theme-list-window')[0];
		//вешаем обработчик на все главное окно
		themeWindow.addEventListener('click', function(e){
			//console.log("target", e.target);
			//поднимаемся по родителям, пока не найдем нужный нам елемент, но не более 3-х уровней
			var deep = self.recursiveDeep;
			var target = e.target;
			while (!target.getAttribute('data-action') && (deep-- > 0)) {
				target = target.parentNode;
			}

			switch (target.getAttribute('data-action')) {
/*				case "new":
					self.themeEditView.showThemeEditDialog('new', null, self.registryThemeEditDialogEvents.bind(self));
				break;*/

				case "show":
					self.showTheme(+target.getAttribute('data-theme-id'), target.getAttribute('data-theme-map'));

				break;

				case "edit":
					var currentTheme = findThemeByThemeId(target.getAttribute('data-theme-id'), themeList) ? findThemeByThemeId(target.getAttribute('data-theme-id'), themeList) : null;
					self.themeEditView.showThemeEditDialog('edit', currentTheme, self.registryThemeEditDialogEvents.bind(self));
				break;

				case "delete":
					var confirmDelete = confirm("Вы уверены что хотите удалить отчет?")
					if (confirmDelete) {
						self.deleteTheme(+target.getAttribute('data-theme-id'));
					}
				break;

				case "setup":
					self.showThemeSetupDialog(+target.getAttribute('data-theme-id'));
				break;

				case "refresh":
					self.showThemeWindow();
				break;
			}

		});

		var addThemeButton = registry.byId('add-theme-button');
		addThemeButton.on('click', function(){
			self.themeEditView.showThemeEditDialog('new', null, self.registryThemeEditDialogEvents.bind(self));
		});

	};

	ThemeController.prototype.registryThemeEditDialogEvents = function(theme) {
		var self = this;

		var themeMapComboBox = registry.byId('theme-map-select');
		self.fillThemeMapComboBox(themeMapComboBox, theme);

		var themeEditSubmitButton = document.getElementById('theme-edit-submit-button');
		//Обрабатываем клие на кнопку submit, то есть сохраняем/добавляем отчет
		themeEditSubmitButton.addEventListener('click', function(){
			//Проверяем, ввел ли пользователь имя и выбрал ли карту
			var themeName = document.getElementById('theme-name');
			if (!themeName.value) {
				Helper.warning("Введите наименование отчета...");
				return;
			}
			if (!themeMapComboBox.item) {
				Helper.warning("Выберите карту для отображения отчета...");
				return;
			}
			var mapName = themeMapComboBox.item.id;

			switch (themeEditSubmitButton.getAttribute('data-edit-action')) {
				case "new":
					themeModel.add(themeName.value, 1, 0, mapName, function(err, result){
						if (err) {
							Helper.error("Не удалось добавить тематический отчет");
							return;
						};
						Helper.success("Отчет успешно создан, выполняем обновление списка отчетов", 10000);
						self.showThemeWindow();
					});
				break;
				case "edit":
					var themeParams = {};
					themeParams.id = +themeEditSubmitButton.getAttribute('data-theme-id');
					themeParams.isShared = 1;	//TODO
					themeParams.isArchive = 0;	//TODO
					themeParams.name = themeName.value;
					themeParams.position = 0; //TODO присваивать позиции отчетам
					themeParams.map = mapName;
					themeModel.updateTheme(themeParams, function(err, result){
						if (err) {
							Helper.error("Не удалось сохранить тематический отчет");
							return;
						};
						Helper.success("Отчет успешно сохранен, выполняем обновление списка отчетов", 10000);
						self.showThemeWindow();
					});
				break;
				default:
					console.log('cannot understand is this a new theme or editing exist'); //debug
				break;
			}
			//по нажатии кнопки скрываем текущую форму
			self.themeEditView.hideThemeEditDialog();
		});
	};

	ThemeController.prototype.showThemeSetupDialog = function(themeId) {
		var self = this;
		if (self.loaderElem) {
			self.loaderElem.style.display = "block";
		}
		themeModel.getThemeLayersParams(themeId, function(err, themeLayersParams){
			if (err) {
				if (self.loaderElem) {
					self.loaderElem.style.display = "none";
				}
				Helper.error("Не удалось получить параметры отчета");
				return;
			};
			Helper.hide();
			//TODO console.log(getPermissionedFeatureLayers(themeLayersParams.featureLayers));
			if (self.loaderElem) {
				self.loaderElem.style.display = "none";
			}
			self.themeSetupView.showThemeSetupDialog(themeLayersParams, self.registryThemeSetupDialogEvents.bind(self));
		});

	}


	ThemeController.prototype.registryThemeSetupDialogEvents = function(themeLayersParams) {
		var self = this;

		var themeSetupWindow = document.getElementsByClassName('theme-setup-window')[0];
		var themeLayerCount = document.getElementsByClassName('theme-layer-wrapper').length; //для вычисления позиции слоя в отчете
		themeLayersParams.count = themeLayerCount;

		themeSetupWindow.addEventListener('click', function(e){

			var deep = self.recursiveDeep;
			var target = e.target;
			while (!target.getAttribute('data-action') && (deep-- > 0)) {
				target = target.parentNode;
			}

			switch (target.getAttribute('data-action')) {
			/*	case "new":
					themeLayersParams.currentTheme = null;
					themeLayersParams.action = "new";
					self.themeLayerEditView.showThemeLayerEditDialog(themeLayersParams, self.registryThemeLayerEditDialogEvents.bind(self));
				break;*/

				case "refresh":
					self.showThemeSetupDialog(+target.getAttribute('data-theme-id'))
				break;

				case "delete-theme-layer":
					var confirmDelete = confirm("Вы уверены что хотите удалить слой из отчета?");
					if (confirmDelete) {
						themeModel.removeThemeLayer(+target.getAttribute('data-theme-layer-id'), +e.target.getAttribute('data-theme-id'), function(err, result){
							if (err) {
								Helper.error("Не удалось удалить отчет");
								return;
							};
							Helper.success("Слой отчета успешно удален, обновляем список слоев", 10000);
							self.showThemeSetupDialog(+target.getAttribute('data-theme-id'));
						});
					};
				break;

				case "edit-theme-layer":
					themeLayersParams.currentThemeId = +target.getAttribute('data-theme-layer-id');
					themeLayersParams.action = "edit";

					for (var i=0; i<themeLayersParams.themeLayers.length; i++) {
			      if (+themeLayersParams.themeLayers[i].id == +themeLayersParams.currentThemeId) {
			        themeLayersParams.currentTheme = themeLayersParams.themeLayers[i];
			      }
			    }

	    		themeLayersParams.currentTheme.style = (typeof(themeLayersParams.currentTheme.style) == 'string') ? JSON.parse(themeLayersParams.currentTheme.style) : themeLayersParams.currentTheme.style;

					self.themeLayerEditView.showThemeLayerEditDialog(themeLayersParams, self.registryThemeLayerEditDialogEvents.bind(self));

				break;
			}

		});

		var addThemeLayerButton = registry.byId('add-theme-layer-button');
		addThemeLayerButton.on('click', function(){
			themeLayersParams.currentTheme = null;
			themeLayersParams.action = "new";
			self.themeLayerEditView.showThemeLayerEditDialog(themeLayersParams, self.registryThemeLayerEditDialogEvents.bind(self));
		});

	}

	ThemeController.prototype.registryThemeLayerEditDialogEvents = function(themeLayersParams) {
		var self = this;
		// регистрируем dom объекты
		var conditionMainContainer = registry.byId('theme-layer-condition-container');//весь контейнер, в котором происходит настройка условий
		var featureLayerComboBox = registry.byId('featurelayer-select');
		var objectTypeComboBox = registry.byId('objecttype-select');
		var featureTypeComboBox = registry.byId('geometry-select');
		var isRestoreMode = false;

		//навешиваем обработчики
		objectTypeComboBox.on('change', function(value){
			if (!this.item) return;

			if (themeLayersParams.currentTheme && objectTypeComboBox.item && (+objectTypeComboBox.item.id == +themeLayersParams.currentTheme.featureType)) {
				isRestoreMode = true;
			}

			var objectTypes = [];
			objectTypes.push(this.item.id);
			getPropertyListByObjectTypes(objectTypes, function(propertyList){
				propertyList.sort(dynamicSortMultiple("propertyName"));
				themeLayersParams.propertyList = propertyList;

				self.resetConditionContainer(conditionMainContainer);
				self.fillPropertiesCombobox(propertyList);
				if (isRestoreMode  && (+objectTypeComboBox.item.id == +themeLayersParams.currentTheme.featureType)) {
					self.restoreConditions(JSON.parse(themeLayersParams.currentTheme.condition), conditionMainContainer, propertyList/*, themeLayersParams.currentTheme.featureType*/);
				}

			});
		});

		featureLayerComboBox.on('change', function(){
			for (var i=0; i<themeLayersParams.featureLayers.length; i++) {
				if (featureLayerComboBox.item && (+themeLayersParams.featureLayers[i].id == +featureLayerComboBox.item.id)) {
					resetComboBoxWithData(objectTypeComboBox, themeLayersParams.featureLayers[i].objectTypes, themeLayersParams.currentTheme ? themeLayersParams.currentTheme.featureType : null);
				}
			}

			resetComboBoxWithData(featureTypeComboBox, self.featureTypesTemplate, themeLayersParams.currentTheme ? themeLayersParams.currentTheme.featureGeometry : null);

			self.resetConditionContainer(conditionMainContainer);
		});

		//заполняем значения данными, для current theme еще и подставятся значения
		// если new, то просто заполнится, причем атрибутика будет без значений, а если
		// был current theme, то в момент подставновки сработает featureLayerComboBox.on('change', function(){
		// и атрибутика заполнится верными значениями, да еще и подставится

		//Изменаем названия для featureLayer, добавляя к ним наименование группы
		themeLayersParams.featureLayers.forEach(function(layer, i, layers){
			layer.name = layer.layerGroupName+" : "+layer.name;
		});
		//сортируем слои по алфавиту для удобства
		themeLayersParams.featureLayers.sort(dynamicSortMultiple("name"));
		resetComboBoxWithData(featureLayerComboBox, themeLayersParams.featureLayers, themeLayersParams.currentTheme ? themeLayersParams.currentTheme.featureLayer : null);
		resetComboBoxWithData(objectTypeComboBox, null, themeLayersParams.currentTheme ? themeLayersParams.currentTheme.featureType : null);



		// -- PropertyContainer
		//событие change не срабатывало при выборе значения, поэтому создали свое и подписались на него
		conditionMainContainer.domNode.addEventListener('select-combobox', function(e){
			var currentPropertyComboBox = e.detail;
			if (!currentPropertyComboBox || !currentPropertyComboBox.item || !currentPropertyComboBox.item.id) {
				return;  //если не выбрано значение, то далее ничего не делаем
			}
			var childsForMain = currentPropertyComboBox.getParent().getChildren();
			var conditionComboBoxForCurrentProperty = childsForMain[1];

			if (currentPropertyComboBox.item.type == '7' || currentPropertyComboBox.item.type == '1' || currentPropertyComboBox.item.type == '9') {
				self.fillConditionCombobox(conditionComboBoxForCurrentProperty, self.textConditionsArray);
			}
			if (currentPropertyComboBox.item.type == '3' || currentPropertyComboBox.item.type == '4' || currentPropertyComboBox.item.type == '2') {
				self.fillConditionCombobox(conditionComboBoxForCurrentProperty, self.numberConditionsArray);
			}
			if (currentPropertyComboBox.item.type == '5') {
				self.fillConditionCombobox(conditionComboBoxForCurrentProperty, self.domainConditionsArray);
			}
			if (currentPropertyComboBox.item.type == '6' || currentPropertyComboBox.item.type == '8') {
				Helper.info("Извините, данный тип не поддерживается тематическими отчетами");
			}

			themeLayersParams.propertyId = currentPropertyComboBox.item.id;
			var featureTypes = [];
		  featureTypes.push(objectTypeComboBox.item.id);
			themeModel.getValuesByPropertyId(featureTypes, currentPropertyComboBox.item.id, currentPropertyComboBox.item.type, function(err, values){
				if (err) {
					Helper.error("Не удалось получить значения для указанной характеристики");
					return;
				};
				self.fillValuesCombobox(childsForMain[2], values);
				if (isRestoreMode && (+objectTypeComboBox.item.id == +themeLayersParams.currentTheme.featureType)) {
					self.restoreConditionValues(JSON.parse(themeLayersParams.currentTheme.condition), childsForMain[1], childsForMain[2]);
				}
			});

		});


		var addConditionButton = document.getElementById('theme-layer-condition-add');
		addConditionButton.addEventListener('click', function(){
			self.addNewCondition(conditionMainContainer, false);
		});


		//click submit button
		document.getElementById('theme-layer-edit-button-submit').addEventListener('click', function(){

			if (!featureLayerComboBox.item || !objectTypeComboBox.item || !featureTypeComboBox.item) {
				Helper.warning("Проверьте параметры слоя");
				return;
			}

			var conditions = self.collectConditions(conditionMainContainer);
			if (!conditions) {
				Helper.warning("Проверьте условия");
				return;
			}

			//этот объект будет участвовать и в сохранении и в добавлении слоя
			var theme = {};
			theme.themeId = themeLayersParams.themeId;
			theme.condition = JSON.stringify(conditions);
			theme.name = document.getElementsByName('theme-layer-name')[0].value || "Без наименования";
			theme.featureLayer = +featureLayerComboBox.item.id;
			theme.featureType = +objectTypeComboBox.item.id;
			theme.featureGeometry = featureTypeComboBox.item.id;
			theme.style = self.styleTemplate;
			theme.style.Fill_ForegroundColor = document.getElementById('style-preview').getAttribute('data-foreground-color').substr(1);
			theme.style = JSON.stringify(theme.style);

			switch (this.getAttribute('data-theme-layer-action')) {
				case "new":
					theme.position = +themeLayersParams.count+1;
					themeModel.addThemeLayer(theme, function(err, themeLayer){
						if (err) {
							Helper.error("Не удалось добавить тематический слой");
							return;
						};
						Helper.success("Cлой успешно добавлен, обновляем список слоев", 10000);
						self.showThemeSetupDialog(+theme.themeId);
					});
				break;
				case "edit":
					theme.position = +themeLayersParams.currentTheme.position;
					theme.id = +themeLayersParams.currentTheme.id;
					themeModel.updateThemeLayer(theme, function(err, themeLayer){
						if (err) {
							Helper.error("Не удалось сохранить новые параметры слоя");
							return;
						};
						Helper.success("Параметры слоя успешно обновлены, обновляем список слоев", 10000);
						self.showThemeSetupDialog(+theme.themeId);
					});
				break;
			}
			self.themeLayerEditView.hideThemeLayerEditDialog();
		});

	};

	function resetComboBoxWithData(combo, data, currentSelectValue) {
		var match = false;
		for (var i=combo.store.data.length-1; i>=0; i--) {
			combo.store.remove(combo.store.data[i].id);
		}
		if (data && data.length > 0) {
			data.forEach(function(item, index){
				try {
					combo.store.add({name: item.name, id: item.id});
					if (currentSelectValue && (item.id == currentSelectValue)) {
						combo.set('item', combo.store.data[index]);
						match = true;
					}
				} catch(e) {
					console.log('error filling = ', e);
				}
			});
			if (!match) {
				combo.set('value', '- Выберите.. -');
			}
		} else {
			combo.set('value', '- Нет данных -');
		}
	}


	ThemeController.prototype.deleteTheme = function(themeId) {
		var self = this;
		themeModel.deleteTheme(themeId, function(err){
			if (err) {
				Helper.error("Не удалось удалить отчет");
				return;
			};
			Helper.success("Отчет успешно удален, обновляем список отчетов", 10000);
			self.showThemeWindow();
		});
	};


	ThemeController.prototype.showTheme = function(themeId, mapName) {
		var self = this;
		if (self.loaderElem) {
			self.loaderElem.style.display = "block";
		}
		Helper.info("Выполняется отображение отчета", 10000);
		self.themeListView.hideThemeListDialog();
		themeModel.showTheme(themeId, mapName, function(err){
			if (err) {
				if (self.loaderElem) {
					self.loaderElem.style.display = "none";
				}
				Helper.error("Не удалось отобразить отчет");
				return;
			};
			if (self.loaderElem) {
				self.loaderElem.style.display = "none";
			}
			Helper.success("Отчет добавлен на карту");

		});
	};




	ThemeController.prototype.resetConditionContainer = function(conditionMainContainer) {
		var self = this;

		if (!conditionMainContainer) {
			Helper.error('Не удалось определить контейнер для условий');
		}

		//Очищаем полностью контейнер
		var conditionMainContainerChildList = conditionMainContainer.getChildren();
		for (var i=conditionMainContainerChildList.length-1; i>=0; i--) {
			conditionMainContainer.removeChild(conditionMainContainerChildList[i]);
		}

		//обнуляем store для каждого из ComboBox из условий
		self.propertyStore = new Memory({	data: [{name: "-Выберите-"}] });

		//добавляем новую строчку (true говорит о том, что она будет первой)
		self.addNewCondition(conditionMainContainer, true);

	}

	function getPropertyListByObjectTypes(objectTypes, callback) {
		themeModel.getPropertyList(objectTypes, function(err, propertyList){
    	if (err) {
				Helper.error("Не удалось получить список характеристик для выбранного описания");
				return;
			};
			callback(propertyList);
		});
	}

	ThemeController.prototype.fillPropertiesCombobox = function(propertyList) {
		var self = this;
		//очищаем
		for (var i=self.propertyStore.data.length-1; i>=1; i--) {
			self.propertyStore.remove(i);
		}
		//заполняем
		for (var i=0; i<propertyList.length; i++) {
			if (["b2", "b3", "b4"].indexOf(propertyList[i].propertyId) >= 0 ) continue; // Пока так хардкорно пропускаем нелепые свойства
			self.propertyStore.put({id: propertyList[i].propertyId, name: propertyList[i].propertyName, type: propertyList[i].propertyType });
		}
	}

	ThemeController.prototype.fillConditionCombobox = function(combobox, data) {
		var self = this;
		//очищаем
		for (var i=combobox.store.data.length-1; i>=0; i--) {
			combobox.store.remove(combobox.store.data[i].id);
		}
		//заполняем
		for (var i=0; i<data.length; i++) {
			combobox.store.put({name: data[i].title, value: data[i].value, id: data[i].value });
		}
		combobox.set('item', combobox.store.data[0]);
	}

	ThemeController.prototype.fillValuesCombobox = function(combobox, data) {
		var self = this;
		//очищаем
		for (var i=combobox.store.data.length-1; i>=0; i--) {
			combobox.store.remove(combobox.store.data[i].id);
		}
		//заполняем
		if (data.length > 0) {
			for (var i=0; i<data.length; i++) {
				var obj = {name: data[i].propertyValue};
				if (data[i].propertyValueId) {
					obj.valueId = data[i].propertyValueId;
				}
				combobox.store.put(obj);
			}
		} else {
			combobox.store.put({name: "-Нет значений-"});
		}
		combobox.set('item', combobox.store.data[0]);
	}

	ThemeController.prototype.restoreConditions = function(conditions, container, propertyList) {
		var self = this;

		conditions.forEach(function(item, i, arr){
			//для первого уже 100% есть контейнер
			if (i == 0) {
				var singleCondition = container.getChildren()[0];
			} else {
				var singleCondition = self.addNewCondition(container, false);
			}

			for (var j=0; j<self.propertyStore.data.length; j++) {
				if (item.property == self.propertyStore.data[j].id) {
					singleCondition.getChildren()[0].set("item", self.propertyStore.data[j]);
				}
			}

		});

	}

	ThemeController.prototype.restoreConditionValues = function(conditions, conditionComboBox, valueComboBox) {
		var currentConditionIndex = conditionComboBox.getParent().getParent().getIndexOfChild(conditionComboBox.getParent());
		var currentCondition = conditions[currentConditionIndex];
		if (!currentCondition) return;
		//restore conditions
		conditionComboBox.store.data.forEach(function(data, i, arr){
			if (data.id == currentCondition.condition) {
				conditionComboBox.set('item', data);
			}
		});
		//restore values
		var valueMatch = false;
		valueComboBox.store.data.forEach(function(data, i, arr){
			if (data.id == currentCondition.value) {
				valueComboBox.set('item', data);
				valueMatch = true;
			}
		});
		//если значения не было, то ставим то, которое в базе
		if (!valueMatch) {
			valueComboBox.set('value', currentCondition.value);
		}

	}


	ThemeController.prototype.addNewCondition = function(conditionMainContainer, isFirstLine) {
		var self = this;

		var propertyComboBox = new ComboBox({
			name: "property-combobox",
			store: self.propertyStore,
			onChange: function() {
				this.getParent().domNode.dispatchEvent(new CustomEvent('select-combobox', {'detail': propertyComboBox, 'bubbles': true}));
			}
		});
		var conditionComboBox = new ComboBox({
			name: "condition-combobox",
			store: new Memory({
				data: [
					{name: "-Выберите-"}
				]
			})
		});
		var valueComboBox = new ComboBox({
			name: "value-combobox",
			store: new Memory({
				data: [
					{name: "-Выберите-"}
				]
			})
		});

		propertyComboBox.set('item', propertyComboBox.store.data[0]);
		conditionComboBox.set('item', conditionComboBox.store.data[0]);
		valueComboBox.set('item', valueComboBox.store.data[0]);

		var conditionLinePane = new ContentPane();

		conditionLinePane.addChild(propertyComboBox);
		conditionLinePane.addChild(conditionComboBox);
		conditionLinePane.addChild(valueComboBox);


		//для всех кроме первой линии добавляем кнопку "Удалить"
		if (!isFirstLine) {
			var removeButton = new Button({
				label: "Удалить...",
				"removeItems": [
					propertyComboBox,
					conditionComboBox,
					valueComboBox
				],
				onClick: function(){
					this.removeItems.push(this);
					self.removeCondition(conditionMainContainer, conditionLinePane);
				}
			});
			conditionLinePane.addChild(removeButton);
		}

		conditionMainContainer.addChild(conditionLinePane);
		return conditionLinePane;
	};

	ThemeController.prototype.removeCondition = function(conditionMainContainer, removeItem) {
				conditionMainContainer.removeChild(removeItem);
	}

	ThemeController.prototype.collectConditions = function(conditionMainContainer) {
		var conditions = [];
		var missingFlag = false; //есть ли пропущенные значения

		conditionMainContainer.getChildren().forEach(function(item, i, arr){
			itemChilds = item.getChildren();
			if (!itemChilds[0].item || !itemChilds[1].item || !itemChilds[0].item.id || !itemChilds[1].item.id || !itemChilds[2].value) {
				missingFlag = true;
				return;
			}

			//если домен, то обрабатываем его ID, а не значение
			var value;
			if (+itemChilds[0].item.type == 5 && itemChilds[2].item) {
				value = itemChilds[2].item.valueId;
			} else {
				value = itemChilds[2].value;
			}

			conditions.push({
				"property": itemChilds[0].item.id,
				"condition": itemChilds[1].item.id,
				"value": value,
				"logical": "AND"
			});
		});

		return missingFlag ? null : conditions;
	}


	ThemeController.prototype.fillThemeMapComboBox = function(combobox, theme) {
		for (var i=combobox.store.data.length-1; i>=0; i--) {
			combobox.store.remove(combobox.store.data[i].id);
		}
		var maps = window.WGS_APP.getMaps();
		maps.forEach(function(map, i, maps){
			combobox.store.add({
				id: map.mapName,
				name: map.title
			});
			if (theme && theme.MAP == map.mapName) {
				combobox.set('item', combobox.store.data[i]);
			}
		});
		if (!theme) {
			combobox.set('item', combobox.store.data[0]);
		}
	}


	//функции сортировки по нескольким параметрам
	function dynamicSort(property) {
		 return function (obj1,obj2) {
				 return obj1[property] > obj2[property] ? 1
						 : obj1[property] < obj2[property] ? -1 : 0;
		 }
 	}

	function dynamicSortMultiple() {
			//save the arguments object as it will be overwritten
			//note that arguments object is an array-like object
			//consisting of the names of the properties to sort by
			var props = arguments;
			return function (obj1, obj2) {
					var i = 0, result = 0, numberOfProperties = props.length;
					//try getting a different result from 0 (equal)
					//as long as we have extra properties to compare
					while(result === 0 && i < numberOfProperties) {
							result = dynamicSort(props[i])(obj1, obj2);
							i++;
					}
					return result;
			}
	}


	function getPermissionedFeatureLayers(layers) {
		//в перспективе отфильтровывать все слои, не входящие в текущие карты
		return layers;
	}

	function findMap(map, arr) {
		for (var i=0; i<=arr.length-1; i++) {
			if (arr[i].mapName == map) {
				return arr[i];
			}
		}
	}

	function findThemeByThemeId(themeId, arr) {
		for (var i=0; i<=arr.length-1; i++) {
			if (+arr[i].THEME_ID == +themeId) {
				return arr[i];
			}
		}
	}

	return ThemeController;
})
