require({cache:{
'url:wgs45/views/cardTable.ejs':"<div data-dojo-type=\"dijit/layout/BorderContainer\" data-dojo-props=\"gutters:true, liveSplitters:false\" id=\"card-table\">\r\n    <div data-dojo-type=\"dijit/layout/ContentPane\" data-dojo-props=\"region:'top', splitter:false\" id=\"card-table-ctrl\">\r\n    \t<div class=\"card-table__control clearspace\">\r\n            <span>Тип объектов:</span>\r\n            <%- selectHtml %>\r\n        </div>\r\n        \r\n        <div class=\"card-table__control clearspace\">\r\n            <span>Слой:</span>\r\n            <span id=\"card-table-layer-groups-select-container\"></span>\r\n        </div>\r\n        \r\n        <div class=\"card-table__control\">\r\n            <button data-dojo-type=\"dijit/form/Button\" id=\"cardTableRetrieveButton\">\r\n                <i class=\"fa fa-refresh\"></i> Перезагрузить таблицу\r\n            </button>\r\n    \t</div>\r\n        \r\n        <div class=\"card-table__control\">\r\n            <button data-dojo-type=\"dijit/form/Button\" id=\"cardTableSaveButton\">\r\n                <i class=\"fa fa-save\"></i> Сохранить\r\n            </button>            \r\n        </div>\r\n    </div>\r\n    <div id=\"card-table-container\" \r\n        data-dojo-type=\"dijit/layout/ContentPane\" \r\n        data-dojo-props=\"region:'center', splitter:false\" ></div>\r\n</div>"}});
define("wgs45/controllers/cardTable/cardTableView", ['dojox/layout/FloatingPane',
        'dojo/text!wgs45/views/cardTable.ejs',
        'dijit/registry',
        'dojo/parser',
        'dojo/dom',
        'dojo/html',
        'wgs45/controllers/cardTable/cardPanes',
        "dijit/layout/ContentPane",
        "dijit/layout/BorderContainer",
        "dijit/layout/TabContainer"
    ],

    function(FloatingPane, template, registry, parser, dom, html, cardPanes) {
        "use strict";

        function CardTableView(selectHtml, windowsDock) {
            this.selectHtml = selectHtml;
            this._dock = windowsDock;

            var cardDialogContainerElem = document.createElement("div");
            var self = this;

            document.body.appendChild(cardDialogContainerElem);
            this.cardDialog = new FloatingPane({
                title: 'Таблица карточек',
                resizable: false,
                dockable: true,
                dockTo: this._dock,
                'class': 'card-dialog cart-table',
                onShow: function() {
                    console.log('SHOW Table');
                    registry.byId('appLayout').resize();
                },
                onHide: function() {
                    console.log('HIDE')
                },
                onClose: function() {
                    console.log('CLOSE')
                }
            }, cardDialogContainerElem);


            this.cardDialog.domNode.getElementsByClassName('dojoxFloatingMinimizeIcon')[0].addEventListener('click', function() {
                setTimeout(function() {
                    registry.byId('appLayout').resize();
                }, 1000);
            });
            
            this.cardDialog.domNode.getElementsByClassName('dojoxFloatingCloseIcon')[0].addEventListener('click', function() {
                if(window.WGS_APP && window.WGS_APP.cardTable) {
                    window.WGS_APP.cardTable = null;
                }
            });

            // this.cardDialog.domNode.getElementsByClassName('dojoxFloatingMaximizeIcon')[0].addEventListener('click', function() {
            //     self._emitGridResizeEvent();
            // });

            //RESIZE WINDOW ON BODY RESIZE
            var resizeTimeout;
            window.addEventListener('resize', function() {
                // prevent calling event handler too often
                if (!resizeTimeout) {
                    resizeTimeout = setTimeout(function() {
                        self.resize();
                        resizeTimeout = null;
                    }, 500);
                }
            });
            
            document.body.addEventListener('hide-card-table', function() {
                self.cardDialog.minimize();
                setTimeout(function() {
                    registry.byId('appLayout').resize();
                }, 500);
            })
        }

        CardTableView.prototype.show = function() {
            var htmlCard = window.ejs.render(template, {
                selectHtml: this.selectHtml
            });
            var self = this;

            this.cardDialog.setContent(htmlCard);
            this.cardDialog.startup();


            var resizeElem = this.cardDialog.domNode.getElementsByClassName('dojoxResizeHandle')[0];
            if (resizeElem) {
                registry.byId(resizeElem.id).on('resize', function() {
                    self._emitGridResizeEvent();
                });
            }
        };

        CardTableView.prototype._emitGridResizeEvent = function() {
            var dgrids = this.cardDialog.domNode.getElementsByClassName('dgrid-grid');
            [].forEach.call(dgrids, function(dgridElem) {
                var widgetEvent = new CustomEvent("resize", {
                    bubbles: true,
                    cancelable: true
                });
                dgridElem.dispatchEvent(widgetEvent);
            });
        };

        CardTableView.prototype.showTabs = function(tabsHtml) {
            console.log("showTabsStart");
            var self = this;
            var tabsElem = registry.byId('card-table-container').containerNode;
            tabsElem.innerHTML = tabsHtml;


            // registry.byId('card-table-tabs').startup();
            parser.parse(dom.byId('card-table-container'));
            registry.byId('card-table-tabs').startup();

            registry.byId('card-table-tabs').getChildren().forEach(function(tabPane) {
                tabPane.on('show', function() {
                    self._emitGridResizeEvent();
                });
            });
            this.cardDialog.resize();
        };

        //resizes card table dialog
        CardTableView.prototype.resize = function() {
            var self = this;
            var windowOffset = 0;

            var bodyWidth = document.body.clientWidth - windowOffset;
            var bodyHeight = document.body.clientHeight - windowOffset;
            var resizeParams = {
                w: bodyWidth,
                h: bodyHeight
            };

            self.cardDialog.resize(resizeParams);

        };

        return CardTableView;
    });