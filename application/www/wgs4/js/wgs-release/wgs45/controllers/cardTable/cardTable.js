define("wgs45/controllers/cardTable/cardTable", ['wgs45/model/cardTableModel',
        'wgs45/controllers/settings',
        'wgs45/controllers/cardTable/cardTableView',
        'wgs45/controllers/cardTable/cardTypeSelect',
        'wgs45/controllers/cardTable/cardPanes',
        'wgs45/controllers/helper',
        'dojo/request/xhr',
        'dijit/registry', 'dojo/on',
        'dojo/parser',
        'dojo/dom'
    ],

    function(CardTableModel, settings, CardTableView, CardTypeSelect, CardPanes, Helper, xhr, registry, on, parser, dom) {
        "use strict";

        function CardTable(windowsDock) {
            this.model = new CardTableModel();
            this.typeSelect = new CardTypeSelect();
            this.dock = windowsDock; //dock - is the bottom pane, where window will be minimized
            
            window.w = this;
        }
        
        // CardTable.prototype.getCurrentPane = function() {
        //     var tabContainer = registry.byId('card-table-tabs');
        //     window.w = tabContainer;
        //     console.log(tabContainer.selectedChild);
            
        //     return 
        // }

        CardTable.prototype._getCardTypes = function() {
            return this.model.getCardTypes();
        };

        CardTable.prototype._getTableColumns = function(cardType) {
            return this.model.getTableColumns(cardType);
        };

        CardTable.prototype.maximize = function() {
            this.tableView.cardDialog.show();
        }

        CardTable.prototype.show = function() {
            var self = this;

            self._getCardTypes().then(function(types) { // когда получили все типы со слоями
                    // self.cardTypes = types;

                    self.sortLayersAndTypes(types);

                    var typeSelectHtml = self.typeSelect.renderHtml(types, 'card-types-select');
                    
                    //Get last-time used card type
                    var defaultType = settings.getDefaults('card-table-type');
                    var defaultValue = 0;
                    if(defaultType) {
                        types.forEach(function(typeDescription, i) {
                            if(typeDescription.id == defaultType) {
                                defaultValue = i;
                            }
                        });    
                    }
                    
                    
                    self.tableView = new CardTableView(typeSelectHtml, self.dock);
                    self.tableView.show();
                    
                    var typeSelect = registry.byId("card-types-select");
                    
                    typeSelect.on("change", function() {
                        self.showPanesByType(types[this.get('value')]);
                        settings.setDefaults({
                            'card-table-type': types[this.get('value')].id
                        });
                    });
                    
                    
                    //try select last-time used card type
                    //set 0 if any errors occured.
                    if(typeSelect.get('value') != defaultValue) {
                        try{
                            console.log('SET VALUE', defaultValue);
                            typeSelect.set('value', defaultValue);
                        } catch(e) {
                            typeSelect.set('value', 0);
                        }
                    } else {
                        self.showPanesByType(types[typeSelect.get('value')]); 
                    }
                    
                    // 
                },

                function(typesError) {
                    alert("typesError: ", typesError);
                });
        };

        CardTable.prototype.sortLayersAndTypes = function(types) {
            types.forEach(function(type, typeId) {
                type.layers.sort(function(a, b) {
                    return a.name < b.name ? -1 : 1;
                });
            });
            types.sort(function(a, b) {
                return a.name < b.name ? -1 : 1;
            });
        };

        CardTable.prototype.showPanesByType = function(type) {
            var self = this;

            //if current type doesn't have layers we use special layer with id -1
            if (type.layers.length == 0) {
                type.layers[0] = {
                    groupId: -1,
                    groupName: 'Без слоя',
                    id: -1,
                    name: 'Без слоя'
                };
            }

            
            var layersGroup = this.getLayerGroupFromCardType(type);
            this.showLayersGroupSelect(type, layersGroup);
            var defaultLayer = settings.getDefaults('card-table-layer-group') || layersGroup[0].groupId;
            var defaultType = settings.getDefaults('card-table-layer-type');
            var layerGroupSelect = registry.byId('card-table-layer-groups-select');
            
            if(layerGroupSelect.get('value') != defaultLayer && defaultType == type.id) {
                try {
                    window.s = layerGroupSelect;
                    layerGroupSelect.set('value', defaultLayer);
                    if(layerGroupSelect.get('value') != defaultLayer) {
                        this.showPanesByTypeAndLayerGroup(type, layerGroupSelect.get('value'));
                    }
                } catch(e) {
                    layerGroupSelect.set('value', layersGroup[0].groupId);
                }
            } else {
                this.showPanesByTypeAndLayerGroup(type, layerGroupSelect.get('value')); 
            }
            
        };

        /*
         *   shows select element which contains layers group
         */
        CardTable.prototype.showLayersGroupSelect = function(type, layerGroups) {
            console.log('SHOW LAYER GROUPs')
            //let's destroy the previous select element if it exists
            if (registry.byId('card-table-layer-groups-select')) {
                registry.byId('card-table-layer-groups-select').destroyRecursive();
                document.getElementById('card-table-layer-groups-select-container').innerHTML = "";
            }

            //show the select for layer groups
            var layerGroupSelectHtml = this.typeSelect.renderHtml(layerGroups, 'card-table-layer-groups-select');
            var ctrlElem = document.getElementById('card-table-layer-groups-select-container');
            ctrlElem.innerHTML = layerGroupSelectHtml;
            parser.parse(dom.byId('card-table-layer-groups-select-container'));
            // registry.byId('card-table-layer-groups-select').startup();

            //if another layer group is selected
            var self = this;
            
            //register onChange event handlers
            // and set last-time value
            var layerGroupSelect = registry.byId('card-table-layer-groups-select');
            layerGroupSelect.onChange = function() {
                var select = this;
                
                self.showPanesByTypeAndLayerGroup(type, this.get('value'));
                settings.setDefaults({
                    'card-table-layer-group': select.get('value'),
                    'card-table-layer-type': type.id
                });
            };
            
        };

        /*
         *   gets Group of layers in one card type
         */
        CardTable.prototype.getLayerGroupFromCardType = function(type) {
            var layersGroup = [];

            type.layers.forEach(function(layer, layerIndex) {
                var layerGroupAlreadyAdded = false;

                layersGroup.forEach(function(layerGroupRecord) {
                    if (layerGroupRecord.groupId == layer.groupId) {
                        layerGroupAlreadyAdded = true;
                    }
                });
                if (!layerGroupAlreadyAdded) {
                    layersGroup.push({
                        groupId: layer.groupId,
                        groupName: layer.groupName
                    });
                }
            });

            layersGroup.sort(function(a, b) {
                return a.groupName < b.groupName ? -1 : 1;
            });

            //console.log('groups ', layerGroups);
            return layersGroup;
        };

        CardTable.prototype.getPanes = function() {
            console.log(this.panes);
        }
        
        CardTable.prototype.showPanesByTypeAndLayerGroup = function(type, group) {
            var self = this;
            //let's destroy the previous pane and tabs if they exist
            console.log('SELF PANES', self.panes)
            if (self.panes) {
                self.panes.destroy();
            }

            // console.log(type, group);
            self.panes = new CardPanes(type, group);
            self.panes.initPromise.then(function() {
                var panesHtml = self.panes.createGridsAndRenderHtml();
                self.tableView.showTabs(panesHtml);
                self.panes.showGrids();

                self.dispatchButtonEventForAllGrids("cardTableSaveButton", "saving");
                self.dispatchButtonEventForAllGrids("cardTableRetrieveButton", "refresh");

            }, function() {
                console.log('не сложилось');
            });
        };

        CardTable.prototype.dispatchButtonEventForAllGrids = function(buttonId, eventName) {
            var self = this;
            on(registry.byId(buttonId), "click", function() {
                var dgrids = self.tableView.cardDialog.domNode.getElementsByClassName('dgrid-grid');
                [].forEach.call(dgrids, function(dgridElem) {
                    var widgetEvent = new CustomEvent(eventName, {
                        bubbles: true,
                        cancelable: true
                    });
                    dgridElem.dispatchEvent(widgetEvent);
                });
            });
        };

        return CardTable;
    });
