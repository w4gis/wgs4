require({cache:{
'url:wgs45/views/cardPanes.ejs':"<div\r\n    data-dojo-type=\"dijit/layout/TabContainer\"\r\n    data-dojo-attach-point=\"card-table-tabs\"\r\n    data-dojo-props=\"region:'center'\"\r\n\r\n    id=\"card-table-tabs\">\r\n        <% if(layers){\r\n        \t layers.forEach(function(layer, layerId) {\r\n                if(layer.groupId == groupId) {%>\r\n                    <div\r\n                        data-dojo-type=\"dijit/layout/ContentPane\"\r\n                        title=\"<%=layer.name%>\"\r\n                        id=\"cardTable-tab-<%=layer.id%>\">\r\n                            <div\r\n                                data-dojo-type=\"dijit/layout/BorderContainer\">\r\n                                <!-- grid controls-->\r\n                                <div\r\n                                    data-dojo-type=\"dijit/layout/ContentPane\"\r\n                                    data-dojo-props=\"region: 'top', splitter: false\">\r\n                                    <form name='filterTableForm' class=\"card-table__filter-form\" onsubmit=\"return false;\">\r\n                                        <div style=\"float:left\">\r\n                                            <p>\r\n                                                <input\r\n                                                    style=\"width: 400px\"\r\n                                                    placeholder=\"Например: общежитие%\"\r\n                                                    data-dojo-type=\"wgs45/controllers/ClearableTextBox\"\r\n                                                    class=\"card-table__filter\"/>\r\n                                            </p>\r\n                                            <!-- <p style=\"margin: 10px 0\">\r\n                                                Искать в:\r\n                                                <input type=\"radio\" name=\"filterDB\" data-dojo-type=\"dijit/form/RadioButton\" class=\"card-table__filter-radiobutton\" checked/>\r\n                                                ГИС ТУСУР\r\n                                                <input type=\"radio\" name=\"filterDB\" data-dojo-type=\"dijit/form/RadioButton\" class=\"card-table__filter-radiobutton\"/>\r\n                                                АИС \"Контингент\"\r\n                                            </p> -->\r\n                                        </div>\r\n                                        <button type=\"submit\" data-dojo-type=\"dijit/form/Button\" class=\"card-table__filter-button card-table__toolbar-button\">\r\n                                            <i class=\"fa fa-search\"></i> Поиск\r\n                                        </button>\r\n                                        <button type=\"button\" id=\"excel-<%= layer.id %>\" data-dojo-type=\"dijit/form/Button\" class=\"card-table__filter-button card-table__toolbar-button\">\r\n                                            <i class=\"fa fa-file-excel-o\"></i> Экспорт в Excel\r\n                                        </button>\r\n\r\n                                        <% if(cardType.id == 4 || cardType.id == 7){ %>\r\n                                            <button\r\n                                                data-dojo-type=\"dijit/form/Button\"\r\n                                                    class=\"card-table__show-map-<%= layer.id %> card-table__toolbar-button\">\r\n                                                    <i class=\"fa fa-map-marker\"></i> На карте\r\n                                            </button>\r\n                                        <% } %>\r\n\r\n                                        <% if([3,4,7].indexOf(+cardType.id) != -1) { %>\r\n                                            <div data-dojo-type=\"dijit/form/DropDownButton\" class=\"card-table__toolbar-button\">\r\n                                                <span>Генерация документов...</span>\r\n                                                <div data-dojo-type=\"dijit/DropDownMenu\">\r\n                                                    <% if(cardType.id == 3) { %>\r\n                                                        <div\r\n                                                            data-dojo-type=\"dijit/MenuItem\"\r\n                                                            class=\"card-table__report-<%=layer.id%>\">\r\n                                                            <i class=\"fa fa-print\"></i> <span class=\"card-table__report__btn\" data-action=\"replace-student\">Приказ на выселение</span>\r\n                                                        </div>\r\n\r\n                                                        <div\r\n                                                            data-dojo-type=\"dijit/MenuItem\"\r\n                                                            class=\"card-table__report-<%=layer.id%>\">\r\n                                                            <i class=\"fa fa-print\"></i> <span class=\"card-table__report__btn\" data-action=\"place-student\">Приказ на заселение</span>\r\n                                                        </div>\r\n                                                    <% } %>\r\n                                                    <% if(cardType.id == 4 || cardType.id == 7){ %>\r\n                                                    <div\r\n                                                        data-dojo-type=\"dijit/MenuItem\"\r\n                                                        class=\"card-table__report-<%=layer.id%>\">\r\n                                                        <i class=\"fa fa-print\"></i> <span class=\"card-table__report__btn\" data-action=\"election\" data-group=\"<%= groupId %>\">Список на выборы</span>\r\n                                                    </div>\r\n                                                    <div\r\n                                                        data-dojo-type=\"dijit/MenuItem\"\r\n                                                        class=\"card-table__report-<%=layer.id%>\">\r\n                                                        <i class=\"fa fa-print\"></i> <span class=\"card-table__report__btn\" data-action=\"rectorat\" data-group=\"<%= groupId %>\">Решение ректората</span>\r\n                                                    </div>\r\n                                                    <% } %>\r\n                                                </div>\r\n                                            </div>\r\n                                        <% } %>\r\n                                    </form>\r\n                                </div>\r\n                                <!-- grid itself -->\r\n                                <div\r\n                                    class=\"card-table__grid-content-pane\"\r\n                                    data-dojo-type=\"dijit/layout/ContentPane\"\r\n                                    data-dojo-props=\"region: 'center', splitter: false\">\r\n                                </div>\r\n                            </div>\r\n\r\n                    </div>\r\n        <%      }\r\n            });\r\n        }%>\r\n</div>\r\n"}});
define("wgs45/controllers/cardTable/cardPanes", ["dijit/layout/TabContainer", 'wgs45/model/cardTableModel',
    'dojo/text!wgs45/views/cardPanes.ejs',
    'dojo/request/xhr', 'dojo/_base/lang',
    'dojo/Deferred',
    'dojo/date/locale',
    'dojo/json',
    'wgs45/controllers/cardTable/cardGrid',
    'wgs45/controllers/helper',
    'dijit/registry', 'dgrid/editor',
    'dojo/dom',
    'dijit/form/SimpleTextarea',
    'dijit/form/ValidationTextBox',
    'dijit/form/Select',
    'dijit/form/DateTextBox'],

function(TabContainer, Model, template, xhr, lang, Deferred, locale, json, CardGrid, Helper, registry, editor, dom, Textarea, TextBox, Select, DateTextBox) {
    "use strict";

    function CardPanes(cardType, groupId) {
        this.cardType = cardType;
        this.groupId = groupId;
        this.model = new Model();
        this.tableHeader = null;
        var self = this;

        this.initPromise = new Deferred();

        this.getTableColumns().then(function(header) {
            self.tableHeader = header;
            console.log('table header ', header);

            self.setEditorsForHeader(self.tableHeader);

            self.sortTableHeader(self.tableHeader);

            self.initPromise.resolve();
        }, function(error) {
            Helper.error('Невозможно получить данные с сервера. АИС "Контингент" не доступна', 10000);
            self.initPromise.reject();
        });
    }

    /*
     *   sorting of table header
     */
    CardPanes.prototype.sortTableHeader = function(tableHeader) {
        console.log(tableHeader[1][0]);
        tableHeader[1][0].sort(function(a, b) {
            if (a.label && b.label) {
                if (~a.label.toLowerCase().indexOf('дополнительная информация')) {
                    return 1;
                }
                if (~a.label.toLowerCase().indexOf('вложенные карточки') || ~b.label.toLowerCase().indexOf('вложенные карточки')) {
                    return~a.label.toLowerCase().indexOf('вложенные карточки') ? 1 : -1;
                }
                if (~a.label.toLowerCase().indexOf('вложенные карточки') && ~b.label.toLowerCase().indexOf('дополнительная информация')) {
                    return -1;
                }

                if (~a.label.toLowerCase().indexOf('основные характеристики')) {
                    return -1;
                }

                return a.label > b.label ? 1 : -1;
            } else {
                return a > b ? 1 : -1;
            }
        });
    }

    /*
     *   Recursive function that watches whole table header and sets editor for column
     */
    CardPanes.prototype.setEditorsForHeader = function(THeader) {
        var self = this;
        if (THeader.children) {
            self.setColumnEditor(THeader.children);
        } else {
            if (Array.isArray(THeader)) {
                THeader.forEach(function(header, headerIndex) {
                    self.setEditorsForHeader(header);
                });
            } else {
                self.setColumnEditor([THeader]);
            }
        }
    };

    /*
     *   sets editor for every column dependently of value type
     *   @columns - array of columns
     */
    CardPanes.prototype.setColumnEditor = function(columns) {
        var self = this;
        columns.forEach(function(header, headerIndex) {
            //console.log(header);
            switch (header['valueTypeId']) {
                case 0:
                    break;
                case 1:
                    header['editor'] = Textarea;
                    header.editOn = "dgrid-cellfocusin";
                    break;
                case 2:
                    self.setDateColumnEditor(header);
                    break;
                case 3:
                    self.setNumberColumnEditor(header);
                    break;
                case 4:
                    header['editor'] = TextBox;
                    header.editOn = "dgrid-cellfocusin";
                    break;
                case 5:
                    self.setDomainColumnEditor(header);
                    break;
                case 6:
                    header['editor'] = TextBox;
                    header.editOn = "dgrid-cellfocusin";
                    break;
                case 7:
                    header['editor'] = TextBox;
                    header.editOn = "dgrid-cellfocusin";
                    break;
                case 101 /* Nested cards */ :
                    self.setRelationColumnEditor(header);
                    break;
                case 102 /* Card actions */ :
                    self.setOpenCardColumnEditor(header);
                    break;
            }
        });
    };

    CardPanes.prototype.setDateColumnEditor = function(header) {
        header['editor'] = DateTextBox;
        header.set = function(item) {
            var dt = item[header['field']];
            //console.log("item ", dt);

            return typeof dt == 'string' ? dt : locale.format(dt, {
                selector: "date",
                datePattern: 'yyyy-MM-dd'
            });
        }
    };

    CardPanes.prototype.setNumberColumnEditor = function(header) {
        header['editor'] = TextBox;
        header.editorArgs = {
            validator: function(value) {
                return (+value).toString() == value
            },
            invalidMessage: 'Введите число'
        }
        header.editOn = "dgrid-cellfocusin";
    };

    CardPanes.prototype.setDomainColumnEditor = function(header) {
        header.editor = Select;
        var options = [];
        options.push({
            value: '',
            disabled: true,
            label: 'Выберите значение...'
        })
        header.domain.forEach(function(domain, domainIndex) {
            options.push({
                value: domain['Value_Domain_ID'],
                label: domain['Value_Domain']
            });
        });

        header.editorArgs = {
            options: options
        };

        header['get'] = function(item) {
            //console.log('item');
            return item[header['field']];
        }
    };

    CardPanes.prototype.setRelationColumnEditor = function(header) {
        //header['editor'] = TextBox;
        header.formatter = function(item) {

            if (!item) {
                return item;
            }
            var childCards = json.parse(item);
            var result = '<table class="card-table__relation-field__content">';
            childCards.forEach(function(card, cardIndex) {
                result += '\
                    <tr>\
                        <td>\
                            <a onclick="event.preventDefault()" href="#" data-card-id="' + card.id + '" data-action="card-open-direct" class="card__nested-card">' + (card.name || 'Без наименования') + ' : ' + card.typeName + '\
                            </a>\
                        </td>\
                        <td class="card-table__relation-field__content__action">\
                            <a onclick="event.preventDefault()" href="#" class="card-action" data-action="card-unbind" data-relation-id="' + card.objRelationId + '">\
                                <i class="fa fa-times"></i> Отвязать\
                            </a>\
                        </td>\
                    </tr>';
            });
            result += '</table>';
            //console.log(result);
            return result;
        }
        header.set = function(item) {
            return null;
        }
    };

    CardPanes.prototype.setOpenCardColumnEditor = function(header) {
        header.formatter = function(value) {
            // console.log(item);
            var resultLayoutStr = '\
                <a href="#" onclick="event.preventDefault();" \
                    class="dgrid__control-link"\
                    data-action="card-open-direct" \
                    data-card-id="' + value.id + '">\
                    <i class="fa fa-list-alt"></i> Карточка\
                </a>';
            resultLayoutStr += '\
                <a href="#" onclick="event.preventDefault();" \
                    class="dgrid__control-link"\
                    data-action="card-remove" \
                    data-card-id="' + value.id + '">\
                    <i class="fa fa-remove"></i>\
                </a>';
            switch (+value.type) {
                case 3:
                    {
                        /*resultLayoutStr += '<br/>\
                            <a href="#" onclick="event.preventDefault()" \
                                class="dgrid__control-link"\
                                data-action="report-1" \
                                data-card-id="' + value.id + '">\
                                <i class="fa fa-file-word-o"></i> Сведения\
                            </a>';
                        */
                        resultLayoutStr += '<br/>\
                            <a href="#" onclick="event.preventDefault()" \
                                class="dgrid__control-link"\
                                data-action="contract-student" \
                                data-card-id="' + value.id + '">\
                                <i class="fa fa-file-word-o"></i> Договор аренды\
                            </a>';

                        resultLayoutStr += '<br/>\
                            <a href="#" onclick="event.preventDefault()" \
                                class="dgrid__control-link"\
                                data-action="registry" \
                                data-card-id="' + value.id + '">\
                                <i class="fa fa-file-word-o"></i> Документы для регистрации\
                            </a>';
                        
                        resultLayoutStr += '<br/>\
                            <a href="#" onclick="event.preventDefault()" \
                                class="dgrid__control-link"\
                                data-action="foreign-registry" \
                                data-card-id="' + value.id + '">\
                                <i class="fa fa-file-word-o"></i> Уведомление о прибытии\
                            </a>';
                    }
            }

            return resultLayoutStr;

        };
        header.set = function(item) {
            return null;
        }
    };

    CardPanes.prototype.getTableHeader = function() {
        return lang.clone(this.tableHeader);
    };

    CardPanes.prototype.getTableColumns = function() {
        console.log('cardType ', this.cardType);
        return this.model.getTableColumns(this.cardType, this.groupId);
    };

    CardPanes.prototype.createGridsAndRenderHtml = function() {
        var self = this;

        self.grids = [];
        self.getLayers().forEach(function(layer, layerIndex) {
            if (layer.groupId == self.groupId) {
                var grid = new CardGrid(self.cardType, layer.id, 'cardTable-tab-' + layer.id, self.getTableHeader());
                self.grids.push(grid);
            }
        });

        //console.log('GRIDS', self.grids);
        return window.ejs.render(template, {
            layers: self.getLayers(),
            groupId: self.groupId,
            cardType: self.cardType
        });
    };


    CardPanes.prototype.getLayers = function() {
        //return [0,1];
        return this.cardType.layers;
    };

    CardPanes.prototype.showGrids = function() {
        var self = this;
        if (this.grids) {
            this.grids.forEach(function(grid, gridId) {
                registry.byId('cardTable-tab-' + grid.layerId).onShow = function() {
                    grid.show();
                    self._currentLayer = grid.layerId;
                    setTimeout(function() {
                        grid.grid.resize();
                    });

                    registry.byId('cardTable-tab-' + grid.layerId).onShow = function() {
                        self._currentLayer = grid.layerId;
                        grid.grid.resize();
                    };
                }
            });

            if (this.grids.length > 0) {
                this.grids[0].show();
                self._currentLayer = self.grids[0].layerId;

                registry.byId('cardTable-tab-' + this.grids[0].layerId).onShow = function() {
                    self.grids[0].grid.resize();
                    self._currentLayer = self.grids[0].layerId;
                };
            }

        }

    };

    CardPanes.prototype.destroy = function() {
        if (this.getLayers()) {
            this.getLayers().forEach(function(layer, layerId) {
                if (registry.byId('cardTable-tab-' + layer.id)) {
                    registry.byId('cardTable-tab-' + layer.id).destroy();
                }
            });
        }
        if (registry.byId('card-table-tabs')) {
            registry.byId('card-table-tabs').destroyRecursive();
        }
        if (this.grids) {
            this.grids.forEach(function(grid, gridId) {
                if (grid) {
                    grid.destroy();
                }
            });
        }
        //this=null;
    };

    return CardPanes;
});