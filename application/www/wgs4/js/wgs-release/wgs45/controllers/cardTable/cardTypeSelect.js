require({cache:{
'url:wgs45/views/cardTypeSelect.ejs':"<select id=\"<%=selectId%>\" \r\n    data-dojo-type=\"dijit/form/Select\">\r\n\t<% for(var i=0; i<cardTypes.length; i++) { %>\r\n    <option value=\"<%=cardTypes[i].groupId || i%>\"><%=cardTypes[i].groupName || cardTypes[i].name%></option>\r\n    <% } %>\r\n</select>"}});
define("wgs45/controllers/cardTable/cardTypeSelect", ['dojo/text!wgs45/views/cardTypeSelect.ejs', 'dojo/Deferred', 'dojo/request/xhr', 'dojo/json'], function(template, Deferred, xhr, json) {
    "use strict";
    
    function CardTypeSelect() {
    }

    

    CardTypeSelect.prototype.renderHtml = function(cardTypes, id){
        return window.ejs.render(template, {cardTypes : cardTypes, selectId: id});
    };

    return CardTypeSelect;
});