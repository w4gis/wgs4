define("wgs45/controllers/cardTable/cardGrid", ['dojo/_base/declare',
		'dijit/registry',
		'dgrid/OnDemandGrid',
		"dojox/data/QueryReadStore",
		'dgrid/extensions/CompoundColumns',
		'wgs45/controllers/helper',
		'dstore/Rest',
		'dojo/json',
		'dojo/request/xhr',
		'dgrid/Selection',
		'dgrid/Keyboard',
		'dgrid/Editor',
		"dgrid/extensions/ColumnHider", "dgrid/ColumnSet", "dstore/Trackable", "dstore/Cache",

		"dijit/form/DropDownButton", "dijit/DropDownMenu", "dijit/MenuItem"
	],

	function (declare, registry, OnDemandGrid, QueryStore, CompoundColumns, Helper, Rest, json, xhr, Selection, Keyboard, Editor, ColumnHider, ColumnSet, Trackable, Cache) {
		"use strict";

		function CardGrid(cardType, layerId, tabId, tableHeader) {
			this.tabId = tabId;
			this.tableHeader = tableHeader;
			this.layerId = layerId;
			this.cardType = cardType;
		}

		CardGrid.prototype.show = function () {
			// console.log("grid testo: ", this.tabId, this.tableHeader, this.cardType, this.layerId);
			var self = this;

			var CustomStore = declare([Rest, Trackable, Cache]);
			var store = new CustomStore({
				target: "/objectadmin/cardTable/CardTableValues", //?typeId=" + self.cardType.id + "&layerId=" + self.layerId,
				useRangeHeaders: true,
				query: { //Да наконец-то!
					typeId: self.cardType.id,
					layerId: self.layerId
				}
			});

			this.grid = new(declare([OnDemandGrid, CompoundColumns, Selection, Keyboard, Editor, ColumnHider, ColumnSet]))({
				// collection: CS2,
				collection: store,
				sort: 'id',
				getBeforePut: false,
				loadingMessage: 'Загрузка данных...',
				noDataMessage: "По вашему запросу ничего не найдено",
				columnSets: this.tableHeader,
				bufferRows: 0,
				allowSelectAll: true,
				keepScrollPosition: true
			});

			[].forEach.call(this.grid.domNode.getElementsByTagName('th'), setRelationClass);

			require(['dojo/aspect'], function (aspect) {
				aspect.after(self.grid, "renderRow", function (row, args) {
					[].forEach.call(row.getElementsByTagName('td'), setRelationClass);
					return row;
				});
			});

			var gridWrapper = registry.byId(this.tabId).domNode.getElementsByClassName('card-table__grid-content-pane')[0];
			registry.byId(gridWrapper.getAttribute('widgetid')).addChild(this.grid);

			this.grid.startup();
			this.registerEventHandlers();
			this._resizeRelationHeader();

			function setRelationClass(cellNode) {
				if (~cellNode.className.indexOf('field-relation-')) {
					cellNode.classList.add('field-relation');
				}
			}

		};

		CardGrid.prototype.getSelection = function () {
			return this.grid.selection;
		}

		CardGrid.prototype.destroy = function () {
			if (this.grid) {
				this.grid.destroy();
			}
		};

		CardGrid.prototype._resizeRelationHeader = function () {
			var gridHeaderElem = this.grid.domNode.getElementsByClassName('dgrid-header')[0];
			[].forEach.call(gridHeaderElem.getElementsByTagName('tr')[0].children, function (cellNode) {
				if (~cellNode.className.indexOf('field-relation-')) {
					cellNode.classList.add('field-relation');
				}
			});

			this.grid.resize();
		};

		CardGrid.prototype.registerEventHandlers = function () {
			var self = this;



			this.grid.domNode.addEventListener('resize', function () {
				self.grid.resize();
				console.log('resized...');
			});

			this.grid.domNode.addEventListener('saving', function () {
				console.log(self.grid);
				self.grid.save().then(function () {
					self.grid.refresh();
					Helper.success('Изменения успешно сохранены');
				});

			});

			this.grid.domNode.addEventListener('refresh', function () {
				self.grid.refresh();
				Helper.debug('Получение данных с сервера...');
				console.log('refreshed...');
			});

			document.body.addEventListener('grid-refresh', function () {
				self.grid.refresh();
				Helper.debug('Получение данных с сервера...');
				console.log('refreshed...');
			});

			var filterForm = document.getElementById(self.tabId).getElementsByClassName('card-table__filter-form')[0];
			filterForm.addEventListener("submit", function () {
				var inputNodeElem = this.getElementsByClassName('card-table__filter')[0];
				// var radio1 = this.getElementsByClassName('card-table__filter-radiobutton')[0];
				// var radioBtn = radio1.children[0];
				// self.grid.collection.query.contingent = radioBtn.checked ? '0' : '1';
				self.grid.collection.query.filter = registry.byId(inputNodeElem.getAttribute('widgetid')).get('value');
				self.grid.refresh();
				Helper.debug('Поиск по базе данных...');
			});

			// код для поиска по таймеру, а не по кнопке. Пока не нужен
			/*console.log(registry.byId(inputWidgetElem.getAttribute('widgetid')));
			    registry.byId(inputWidgetElem.getAttribute('widgetid')).on('input', function() {
			        var thisObj = this;
			        console.log('new ',thisObj.get('value'),', old ', self.filteringValue);
			        if(thisObj.get('value') != self.filteringValue) {
			            var filtering = function() {
			                console.log('filtering');
			                self.grid.collection.query.filter = thisObj.get('value');
			                self.grid.refresh();
			                self.filteringTimerId = undefined;
			                self.filteringValue = thisObj.get('value');
			            };
			            if(self.filteringTimerId){
			                console.log('timerid runs ',self.filteringTimerId);
			                clearTimeout(self.filteringTimerId);
			                console.log('timerid destroyed ',self.filteringTimerId);
			            }
			            self.filteringTimerId = setTimeout(filtering, 3000);
			            self.filteringValue = thisObj.get('value');
			            console.log('timerid created ',self.filteringTimerId);
			        }
			});*/

			var waitCount = 0;
			var waitTimeout = 15000;

			var anotherInt = setInterval(function () {
				waitCount += 200;

				if (waitTimeout < waitCount) {
					clearInterval(anotherInt);
				} else {
					var excelButton = registry.byId('excel-' + self.layerId);
					console.log('excel-' + self.layerId, excelButton);
					if (excelButton) {
            clearInterval(anotherInt);
						excelButton.on('click', function () {
							console.log('open');
              var input = registry.byId(excelButton.domNode.closest('.card-table__filter-form').getElementsByClassName('card-table__filter')[0].getAttribute('widgetid'));
							window.open('/objectadmin/cardTable/excel?typeId=' + self.cardType.id + "&layerId=" + self.layerId + "&filter=" + input.get('value'));
						});
					}
				}
			}, 200)

			var buttonsParsedInterval = setInterval(function () {

				var reportBtns = document.body.getElementsByClassName('card-table__report-' + self.layerId);
				var showMapBtns = document.body.getElementsByClassName('card-table__show-map-' + self.layerId);

				waitCount += 200;



				if (waitTimeout < waitCount) {
					clearInterval(buttonsParsedInterval);
				} else if (reportBtns.length) { //if there are report buttons...

					if (registry.byId(reportBtns[0].getAttribute('widgetid'))) {
						clearInterval(buttonsParsedInterval);
					} else {
						if (showMapBtns.length) { // or show selection buttons...  It is OK
							if (registry.byId(showMapBtns[0].getAttribute('widgetid'))) {
								clearInterval(buttonsParsedInterval);
							}
						} else { //else - wait
							return;
						}
					}
				} else {
					return;
				}

				[].forEach.call(reportBtns, function (buttonElem) {
					var target = buttonElem.getElementsByClassName('card-table__report__btn')[0];

					if (registry.byId(buttonElem.getAttribute('widgetid'))) {
						registry.byId(buttonElem.getAttribute('widgetid')).onClick = function () {
							var selection = self.getSelection();
							var cardIds = [];
							for (var i in selection) {
								cardIds.push(i);
							}

							var widgetEvent = new CustomEvent('menu-select', {
								bubbles: true,
								cancelable: true,
								detail: {
									action: target.dataset.action,
									group: target.dataset.group,
									layer: self.layerId,
									type: self.cardType.id,
									cardIds: cardIds
								}
							});
							document.body.dispatchEvent(widgetEvent);

						};
					}
				});

				[].forEach.call(showMapBtns, function (buttonElem) {

					if (registry.byId(buttonElem.getAttribute('widgetid'))) {

						var widget = registry.byId(buttonElem.getAttribute('widgetid'));
						// window.w = widget;
						// window.g = self.grid;
						widget.set('disabled', true);
						for (var i in self.grid.selection) {
							if (self.grid.selection[i]) {
								widget.set('disabled', false);
							}
						}

						self.grid.on("dgrid-select", function (event) {
							// Get the rows that were just selected
							var rows = event.rows;
							widget.set('disabled', rows.length === 0);
						});


						widget.onClick = function () {
							var map;
							self.cardType.layers.forEach(function (layer) {
								if (layer.id == self.layerId) {
									map = layer.mapName;
								}
							});
							console.log(map);
							var objects = [];
							for (var i in self.grid.selection) {
								if (self.grid.selection[i]) {
									objects.push(i);
								}
							}

							console.log(objects);

							window.WGS_APP.selectFeatures(map, objects, true);

							var widgetEvent = new CustomEvent("hide-card-table", {
								bubbles: true,
								cancelable: true
							});
							document.body.dispatchEvent(widgetEvent);
						};
					}
				});
			}, 200);
		};

		return CardGrid;
	});



// var gridScrollerElem = this.grid.domNode.getElementsByClassName('dgrid-scroller')[0];
// var gridHeaderElem = this.grid.domNode.getElementsByClassName('dgrid-header')[0];

// gridScrollerElem.addEventListener('scroll', function(e) {
//     //console.log('scroll', gridScrollerElem.scrollLeft);
//     gridHeaderElem.style.transform = 'translateX(-' + gridScrollerElem.scrollLeft + "px" + ')';
// });
