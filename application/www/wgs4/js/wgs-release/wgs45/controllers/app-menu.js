require({cache:{
'url:wgs45/config/menu-items.json':"[{\r\n    \"title\": \"Меню\",\r\n    \"priority\": 100,\r\n    \"children\": [{\r\n        \"title\": \"Печать карты...\",\r\n        \"labelClass\": \"fa fa-print\",\r\n        \"action\": \"print-map\"\r\n    }, {\r\n        \"title\": \"\",\r\n        \"action\": \"\",\r\n        \"type\": \"separator\"\r\n    }, {\r\n        \"title\": \"Выход\",\r\n        \"labelClass\": \"fa fa-sign-out\",\r\n        \"action\": \"logout\"\r\n    }]\r\n}, {\r\n    \"title\": \"Инструменты\",\r\n    \"priority\": 50,\r\n    \"children\": [{\r\n        \"title\": \"Табличное представление\",\r\n        \"labelClass\": \"fa fa-table\",\r\n        \"action\": \"cardTable-open\"\r\n    }, {\r\n        \"title\": \"Тематические отчеты\",\r\n        \"labelClass\": \"fa fa-list\",\r\n        \"action\": \"theme-list\"\r\n    }]\r\n}, {\r\n    \"title\": \"Администрирование\",\r\n    \"priority\": 10,\r\n    \"children\": [{\r\n        \"title\": \"Управление доступом...\",\r\n        \"labelClass\": \"fa fa-users\",\r\n        \"action\": \"admin-access\",\r\n        \"privilege\": \"PROJECT_ADMIN\"\r\n    }, {\r\n        \"title\": \"Управление данными...\",\r\n        \"labelClass\": \"fa fa-database\",\r\n        \"action\": \"admin-data\",\r\n        \"privilege\": \"PROJECTOBJECT_ADMIN\"\r\n    }, {\r\n        \"title\": \"\",\r\n        \"action\": \"\",\r\n        \"type\": \"separator\"\r\n    }, {\r\n        \"title\": \"Обновление БД из АИС Контингент...\",\r\n        \"labelClass\": \"fa fa-cloud-download\",\r\n        \"action\": \"admin-update-db\",\r\n        \"privilege\": \"PROJECT_ADMIN\"\r\n    }, {\r\n        \"title\": \"\",\r\n        \"action\": \"\",\r\n        \"type\": \"separator\"\r\n    }, {\r\n        \"title\": \"Перезагрузить сервер\",\r\n        \"labelClass\": \"fa fa-refresh\",\r\n        \"action\": \"admin-restart\",\r\n        \"privilege\": \"PROJECT_ADMIN\"\r\n    }]\r\n}]\r\n"}});
//Fusion.getWidgetById('Map').loadMapGroup(Fusion.applicationDefinition.mapGroups['openLayers-fet']);

define("wgs45/controllers/app-menu", [
    "dojo/text!wgs45/config/menu-items.json",
    "dijit/MenuBar",
    "dijit/PopupMenuBarItem",
    "dijit/CheckedMenuItem",
    "dijit/MenuSeparator",
    "dijit/Menu",
    "dijit/MenuItem",
    "dijit/DropDownMenu",
    'dijit/registry'], function(menuItemList, MenuBar, PopupMenuBarItem, CheckedMenuItem, MenuSeparator, Menu, MenuItem, DropDownMenu, registry) {

    function AppMenu(options) {

        var self = this;
        var maps = options.maps;
        /*console.log(maps)*/

        function getTitle() {
            return (this.labelClass ? "<i class=\"" + this.labelClass + "\"></i> " : "") + this.title;
        }
        var menuItems = JSON.parse(menuItemList);

        var planMenuItems = {
            title: "Поэтажные планы",
            priority: 80,
            children: []
        };

        maps.sort(function(a, b) {
            if(a.mapName == "tusur" ) {
                return -1;
            }
            if(b.mapName == "tusur" ) {
                return 1;
            }
            return a.title > b.title ? 1 : -1;
        });

        maps.forEach(function(mapDescription) {
            planMenuItems.children.push({
                title: mapDescription.title,
                id: mapDescription.mapName,
                action: "plan-open"
            });
        });


        menuItems.push(planMenuItems);
        menuItems.sort(function(a, b) {
            return -a.priority + b.priority
        })


        var pMenuBar = new MenuBar({});

        function addSubmenu(parentItem, menuItems) {

            menuItems.forEach(function(menuItem) {
                var menuItemWidget = new DropDownMenu({});
                parentItem.addChild(menuItemWidget);
                if (menuItem.children) {
                    var childPermissionCounter = menuItem.children.length; //reduce for every permission and separator
                    menuItem.children.forEach(function(childMenuItem) {
                        if(childMenuItem.active === false) {
                            childPermissionCounter--;
                            return;
                        }
                        //check for user privileges/ If NO, menu not shows
                        if(childMenuItem.privilege && !window.WGS.PRIVILEGE[childMenuItem.privilege]) {
                            childPermissionCounter--;
                            return;
                        }
                        var menuButton;
                        switch (childMenuItem.type) {
                            case "checked":
                                {
                                    menuItemWidget.addChild(menuButton = new CheckedMenuItem({
                                        label: getTitle.call(childMenuItem)
                                    }));
                                    break;
                                }

                            case "separator":
                                {
                                    childPermissionCounter--;
                                    menuItemWidget.addChild(menuButton = new MenuSeparator({}));
                                    break;
                                }

                            default:
                                {
                                    menuItemWidget.addChild(menuButton = new MenuItem({
                                        label: getTitle.call(childMenuItem),
                                        onClick: function() {
                                            self.dispatchClickEvent(this)
                                        }
                                    }));
                                }
                        }
                        menuButton.domNode.dataset.menuAction = childMenuItem.action;
                        menuButton.domNode.dataset.menuActionParam = childMenuItem.id;
                        menuButton.domNode.classList.add('has-wgs-action')

                    });
                    if (!childPermissionCounter) {
                        //if nothing to show
                        menuItemWidget.destroy();
                        return;
                    }
                }

                parentItem.addChild(new PopupMenuBarItem({
                    label: getTitle.call(menuItem),
                    popup: menuItemWidget
                }));
            });
        }
        addSubmenu(pMenuBar, menuItems);

        registry.byId('app-menu-container').addChild(pMenuBar);
        pMenuBar.startup();
    }

    AppMenu.prototype.show = function() {

    };

    AppMenu.prototype.dispatchClickEvent = function(menuItem) {
        var widgetEvent = new CustomEvent("menu-select", {
            bubbles: true,
            detail: {
                action: menuItem.domNode.dataset.menuAction,
                param: menuItem.domNode.dataset.menuActionParam
            }

        });
        menuItem.domNode.dispatchEvent(widgetEvent);

    };

    return AppMenu;
});
