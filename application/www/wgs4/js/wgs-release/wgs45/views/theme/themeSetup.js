require({cache:{
'url:wgs45/views/theme/themeSetup.ejs':"<div class=\"theme-layers-control-pane\">\r\n  <button id=\"add-theme-layer-button\" data-dojo-type='dijit/form/Button' role=\"new\"><span data-action=\"new\"><i class=\"fa fa-plus\"></i> Добавить слой</span></button>\r\n</div>\r\n\r\n<% if (!themeLayersParams.themeLayers.length) {%>\r\n    <div class=\"theme-empty-message\">На данный момент не добавлено ни одного слоя для тематического отчета</div>\r\n<% } else { %>\r\n\r\n  <div class=\"theme-layer-label-panel\">\r\n    <span class=\"theme-layer-label label style\">Стиль</span>\r\n    <span class=\"theme-layer-label label name\">Наименование</span>\r\n    <span class=\"theme-layer-label label featurelayer\">Слой геоданных</span>\r\n    <span class=\"theme-layer-label label objecttype\">Описание</span>\r\n  </div>\r\n  <div class=\"theme-layers-list-wrapper\">\r\n    <% for (var i=0; i<themeLayersParams.themeLayers.length; i++) { \r\n      var themeStyle = JSON.parse(themeLayersParams.themeLayers[i].style).Fill_ForegroundColor;\r\n      %>\r\n      <div class=\"theme-layer-wrapper\">\r\n        \r\n       \r\n\r\n        <div class=\"theme-layer-info\">\r\n          <div class=\"theme-layer-style-preview\" style=\"background-color: #<%= themeStyle ? themeStyle : \"fff\"%>\">\r\n            \r\n          </div>\r\n          <span class=\"theme-layer-value name\" name=\"theme-layer-<%= themeLayersParams.themeLayers[i].id %>\"><%= themeLayersParams.themeLayers[i].name %></span>\r\n      \r\n          <% for (var j=0; j<themeLayersParams.featureLayers.length; j++) {\r\n               if (+themeLayersParams.featureLayers[j].id == + themeLayersParams.themeLayers[i].featureLayer) { %>\r\n                  \r\n                  <span class=\"theme-layer-value featurelayer\"><%= themeLayersParams.featureLayers[j].name %></span>\r\n                  <% for (var k=0; k<themeLayersParams.featureLayers[j].objectTypes.length; k++) {%>\r\n                    <% if (+themeLayersParams.themeLayers[i].featureType == +themeLayersParams.featureLayers[j].objectTypes[k].id) { %>\r\n                \r\n                      <span class=\"theme-layer-value objecttype\"><%= themeLayersParams.featureLayers[j].objectTypes[k].name %></span>\r\n                  \r\n                    <% } %>\r\n                  <% } %>\r\n               <% }            \r\n           } %>\r\n        </div>\r\n          \r\n          <div class=\"theme-list-item\" title=\"Удалить\" data-action=\"delete-theme-layer\" data-theme-id=\"<%= themeLayersParams.themeId %>\" data-theme-layer-id=\"<%= themeLayersParams.themeLayers[i].id %>\"><span><i class=\"fa fa-times\"></i></span></div>\r\n          <div class=\"theme-list-item\" title=\"Редактировать\" data-action=\"edit-theme-layer\" data-theme-id=\"<%= themeLayersParams.themeId %>\" data-theme-layer-id=\"<%= themeLayersParams.themeLayers[i].id %>\"><span><i class=\"fa fa-pencil-square-o\"></i></span></div>\r\n         \r\n     \r\n       \r\n      </div>\r\n    <% } %>\r\n  </div>\r\n<% } %>"}});
 define("wgs45/views/theme/themeSetup", ["dijit/Dialog", "dojo/text!wgs45/views/theme/themeSetup.ejs"], function(Dialog, themeSetupEjs){
  function ThemeSetup() {
    this.themeSetupDialog = null;
     
  }
   
  ThemeSetup.prototype.showThemeSetupDialog = function(themeLayersParams, callback) {
    // TODO: Ajax dialog
    if (this.themeSetupDialog) {
      this.themeSetupDialog.destroyRecursive();
    }
    this.themeSetupDialog = new Dialog({
      title: "Настройка отчета",
      content: window.ejs.render(themeSetupEjs, {themeLayersParams: themeLayersParams}),
      'class': "theme-setup-window"
    });
    this.themeSetupDialog.onHide = function() {
      this.destroyRecursive();
    }
    this.themeSetupDialog.show();
    callback(themeLayersParams);
  };


  
  ThemeSetup.prototype.hideThemeSetupDialog = function() {
    this.themeSetupDialog.hide();
  };
  
  return ThemeSetup;
 })
 
 
 