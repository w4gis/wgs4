require({cache:{
'url:wgs45/views/theme/theme.ejs':"<%\r\nfunction findMap(map, arr) {\r\n  for (var i=0; i<=arr.length-1; i++) {\r\n    if (arr[i].mapName == map) {\r\n      return arr[i];\r\n    }\r\n  }\r\n}\r\n\r\nvar maps = window.WGS_APP.getMaps();\r\n %>\r\n<div class=\"theme-window-control-pane\">\r\n  <button data-dojo-type='dijit/form/Button' id=\"add-theme-button\" role=\"new\">\r\n    <span data-action=\"new\">\r\n      <i class=\"fa fa-plus\"></i>\r\n      Создать новый отчет</span>\r\n  </button>\r\n<!-- <div data-action=\"new\" class=\"theme-list-control-item\"><span><i class=\"fa fa-plus\"></i> Добавить</span></div>\r\n    <div data-action=\"refresh\" class=\"theme-list-control-item\"><span><i class=\"fa fa-refresh\"></i> Обновить</span></div> -->\r\n</div>\r\n<% if (!themeList.length) {%>\r\n    <div class=\"theme-empty-message\">На данный момент не создано ни одного тематического отчета</div>\r\n\r\n<% } else {\r\n  var currentMap; %>\r\n  <div data-dojo-type=\"dijit/layout/TabContainer\" style=\"width: 600px; height: 400px;\">\r\n    <% for (var i=0; i < themeList.length; i++) {\r\n\r\n      if (!currentMap || (currentMap.mapName!=themeList[i].MAP)) {\r\n        if (currentMap) { %>\r\n          </div>\r\n        <% } %>\r\n        <% currentMap = findMap(themeList[i].MAP, maps);\r\n          if (!currentMap) continue; %>\r\n\r\n        <div data-dojo-type=\"dijit/layout/ContentPane\" title=\"<%= currentMap.title %>\" class=\"theme-list-wrapper\">\r\n          <!-- <div class=\"theme-map-title\"><%= currentMap.title %></div> -->\r\n          <div class=\"theme-layer-label-panel\">\r\n            <span class=\"theme-layer-label label theme-name\">Наименование отчета</span>\r\n            <span class=\"theme-layer-label label theme-control\">Управление</span>\r\n          </div>\r\n      <% } %>\r\n  \t\t<div class=\"theme-list-record\">\r\n  \t\t\t<span class=\"theme-layer-name\"><%= themeList[i].NAME ? themeList[i].NAME : \"Без наименования\"%></span>\r\n        <div data-action=\"delete\" title=\"Удалить\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"><span><i class=\"fa fa-times\"></i></span></div>\r\n        <div data-action=\"setup\" title=\"Настроить слои\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"><span><i class=\"fa fa-cogs\"></i></span></div>\r\n        <div data-action=\"edit\" title=\"Редактировать\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"><span><i class=\"fa fa-pencil-square-o\"></i></span></div>\r\n        <div data-action=\"show\" title=\"Отобразить\" data-theme-map=\"<%= themeList[i].MAP ? themeList[i].MAP : 0 %>\"  data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"><span><i class=\"fa fa-globe\"></i></span></div>\r\n  \t\t</div>\r\n  \t<% } %>\r\n    </div>\r\n    </div>\r\n    </div>\r\n  </div>\r\n<% } %>\r\n"}});
 define("wgs45/views/theme/themeList", ["dijit/Dialog", "dojo/text!wgs45/views/theme/theme.ejs", "dijit/layout/TabContainer", "dijit/layout/ContentPane"], function(Dialog, themeEjs){
  function ThemeList() {
    this.themeListDialog = null;

  }

  ThemeList.prototype.showThemeListDialog = function(themeList, callback) {
   // console.log('themeListDialog before destroy ', this.themeListDialog);
    if (this.themeListDialog) {
      this.themeListDialog.destroyRecursive();
      //console.log('destroyed');
    }
   // console.log('themeListDialog after destroy ', this.themeListDialog);
    this.themeListDialog = new Dialog({
      title: "Тематические отчеты",
      content: window.ejs.render(themeEjs, {themeList: themeList}),
      'class': "theme-list-window"
    });
  // console.log('themeListDialog after create new ', this.themeListDialog);

    this.themeListDialog.onHide = function() {
      this.destroyRecursive();
    }

    this.themeListDialog.show();
    callback(themeList);
  };



  ThemeList.prototype.hideThemeListDialog = function() {
    this.themeListDialog.hide();
  };

  return ThemeList;
 })
