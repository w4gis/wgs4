 define("wgs45/views/theme/themeEdit", ["dijit/Dialog", "dijit/form/ComboBox"], function(Dialog, ComboBox){
  function ThemeEdit() {
    this.themeEditDialog = null;

  }

  ThemeEdit.prototype.showThemeEditDialog = function(action, theme, callback) {
    // TODO: Ajax dialog
    if (this.themeEditDialog) {
      this.themeEditDialog.destroyRecursive();
    }
    var dialogTitle = (action == 'new') ? "Добавление нового отчета" : "Редактирование отчета";
    var submitButtonTitle = (action == 'new') ? "Добавить" : "Сохранить";
    this.themeEditDialog = new Dialog({
      title: dialogTitle,
      content:  "<div class='theme-edit-container'> "+
                  "<label> Имя: " +
                    "<input name='theme-name' placeholder='Наименование отчета' value='"+(theme ? theme.NAME : '')+"'  id='theme-name'/>" +
                  "</label>" +
                "</div>" +
                "<div style='margin-bottom: 10px'>" +
                  "<span><i><b> Карта для отображения отчета </b></i></span>" +
                "</div>" +
                "<div id='theme-map-select' >" +

                "</div>" +
                "<div class='theme_submit_panel'>" +
                  "<button id='theme-edit-submit-button' data-edit-action="+action+ " data-theme-id="+(theme ? theme.THEME_ID : 0)+ " data-dojo-type='dijit/form/Button'>"+submitButtonTitle+"</button>" +
                "</div>",
      'class': "theme-edit-window"
    });
    new ComboBox({
        name: "select0",
        style: "width: 300px"
    }, 'theme-map-select').startup();

    this.themeEditDialog.onHide = function() {
      this.destroyRecursive();
    }

    this.themeEditDialog.show();
    callback(theme);
  };



  ThemeEdit.prototype.hideThemeEditDialog = function() {
    this.themeEditDialog.hide();
  };

  return ThemeEdit;
 })
