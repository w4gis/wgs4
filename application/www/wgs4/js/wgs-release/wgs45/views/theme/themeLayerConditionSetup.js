require({cache:{
'url:wgs45/views/theme/themeLayerConditionSetup.ejs':"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem blanditiis porro dolorem, placeat vel magnam, iste illum, deleniti maxime quis velit expedita ipsam, provident animi ex harum ipsa veniam commodi.</p>"}});
 define("wgs45/views/theme/themeLayerConditionSetup", ["dijit/Dialog", "dojo/text!wgs45/views/theme/themeLayerConditionSetup.ejs"], function(Dialog, themeLayerConditionSetupEjs){
  function ThemeLayerConditionSetup() {
    this.themeLayerConditionSetupDialog = null;
     
  }
   
  ThemeLayerConditionSetup.prototype.showThemeLayerConditionSetupDialog = function(themeLayerParams, callback) {
    // TODO: Ajax dialog
    if (this.themeLayerConditionSetupDialog) {
      this.themeLayerConditionSetupDialog.destroyRecursive();
    }
    this.themeLayerConditionSetupDialog = new Dialog({
      title: "Настройка условий отчета",
      content: window.ejs.render(themeLayerConditionSetupEjs, {themeLayerParams: themeLayerParams}),
      'class': "theme-layer-condition-setup-window"
    });
    this.themeLayerConditionSetupDialog.show();
    callback(themeLayerParams);
  };


  
  ThemeLayerConditionSetup.prototype.hideThemeLayerConditionSetupDialog = function() {
    this.themeLayerConditionSetupDialog.hide();
  };
  
  return ThemeLayerConditionSetup;
 })
 
 
 