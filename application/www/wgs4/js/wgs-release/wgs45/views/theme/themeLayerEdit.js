require({cache:{
'url:wgs45/views/theme/themeLayerEdit.ejs':"<div class=\"theme-layers-edit-wrapper\">\r\n  \r\n  <div class=\"theme-layer-main-wrapper\">\r\n    <div class=\"theme-layer-label-pane\">\r\n      <span class=\"theme-layer-label label tl-name\">Наименование</span>\r\n      <span class=\"theme-layer-label label tl-featurelayer\">Слой геоданных</span>\r\n      <span class=\"theme-layer-label label tl-objecttype\">Описание</span>\r\n      <span class=\"theme-layer-label label tl-geometry\">Геометрия</span>\r\n    </div>\r\n    <div>\r\n      <input class=\"theme-layer-label tl-name\" name=\"theme-layer-name\" value=\"<%= themeLayersParams.currentTheme ? themeLayersParams.currentTheme.name : \"\" %>\"  data-theme-id=\"<%= themeLayersParams.themeId %>\" placeholder=\"Наименование слоя...\"/>\r\n      <div class=\"theme-layer-label tl-featurelayer\" id=\"featurelayer-select\"></div>\r\n      <div class=\"theme-layer-label tl-objecttype\" id=\"objecttype-select\"></div>\r\n      <div class=\"theme-layer-label tl-geometry\" id=\"geometry-select\"></div>\r\n    </div>\r\n  </div>\r\n  \r\n\r\n  <hr/>\r\n\r\n  <button id=\"theme-layer-condition-add\" data-dojo-type='dijit/form/Button'><i class=\"fa fa-plus\"></i> Добавить условие</button>\r\n  <div class=\"theme-layer-condition-label-pane\">\r\n      <span class=\"theme-layer-condition property\">Характеристика</span>\r\n      <span class=\"theme-layer-condition condition\">Условие</span>\r\n      <span class=\"theme-layer-condition value\">Значение</span>\r\n  </div>\r\n  <div id=\"theme-layer-condition-container\" data-dojo-type=\"dijit.layout.ContentPane\">\r\n   \r\n  </div>\r\n  \r\n  <br/>\r\n    \r\n    \r\n  <div id=\"style-preview\" style=\"background-color: #<%= themeLayersParams.currentTheme ? themeLayersParams.currentTheme.style.Fill_ForegroundColor : \"0099CC\" %>\" data-foreground-color=\"#<%= themeLayersParams.currentTheme ? themeLayersParams.currentTheme.style.Fill_ForegroundColor : \"0099CC\" %>\">\r\n  </div>\r\n  <button data-dojo-type=\"dijit/form/DropDownButton\">\r\n    <span>Цвет заливки</span>\r\n     <div data-dojo-type=\"dijit/TooltipDialog\">\r\n        <span id=\"foreground-color\"></span>  \r\n     </div>\r\n  </button>\r\n   \r\n  <div class=\"theme_submit_panel\">\r\n    <button id=\"theme-layer-edit-button-submit\" data-theme-layer-action=\"<%= themeLayersParams.action %>\" data-dojo-type='dijit/form/Button'>Сохранить</button>\r\n  </div>\r\n\r\n</div>"}});
 define("wgs45/views/theme/themeLayerEdit", ["dijit/Dialog", 
      "dojo/text!wgs45/views/theme/themeLayerEdit.ejs", 
      "dijit/ColorPalette",
      "dijit/form/ComboBox",

      "dijit/form/DropDownButton",
      "dijit/TooltipDialog",
      "dijit/layout/ContentPane"], 
function(Dialog, 
        themeLayerEditEjs, 
        ColorPalette,
        ComboBox ){


  function ThemeLayerEdit() {
    this.themeLayerEditDialog = null;
     
  }
   
  ThemeLayerEdit.prototype.showThemeLayerEditDialog = function(themeLayersParams, callback) {
    // TODO: Ajax dialog
    if (this.themeLayerEditDialog) {
      this.themeLayerEditDialog.destroyRecursive();
    }
    this.themeLayerEditDialog = new Dialog({
      title: "Тематический слой",
      content: window.ejs.render(themeLayerEditEjs, {themeLayersParams: themeLayersParams}),
      'class': "theme-layer-edit-window"
    });
    this.themeLayerEditDialog.onHide = function() {
      this.destroyRecursive();
    }
    new ComboBox({
        name: "select0",
        style: "width: 300px"
    }, 'featurelayer-select').startup();
    new ComboBox({
        name: "select1",
        style: "width: 150px"
    }, 'objecttype-select').startup();
    new ComboBox({
        name: "select2",
        style: "width: 150px"
    }, 'geometry-select').startup();

    this.themeLayerEditDialog.show();
    this.addColorPallete();
    callback(themeLayersParams);
    
  };

  ThemeLayerEdit.prototype.addColorPallete = function() {
    var stylePreview = document.getElementById('style-preview');
    var foregroundColor = new ColorPalette({
      palette: "7x10",
      onChange: function(val){ 
        console.log(val);
        stylePreview.setAttribute('data-foreground-color', val);
        stylePreview.style.backgroundColor = val;
        //stylePreview.innerHTML = val; 
      }
    }, "foreground-color").startup();
  };

  
  ThemeLayerEdit.prototype.hideThemeLayerEditDialog = function() {
    this.themeLayerEditDialog.hide();
  };

  
  return ThemeLayerEdit;
 })
 
 
 