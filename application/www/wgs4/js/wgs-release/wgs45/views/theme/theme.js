 define("wgs45/views/theme/theme", ["dijit/Dialog", "dojo/text!wgs45/views/theme.ejs", "dojo/text!wgs45/views/themeEdit.ejs"], function(Dialog, themeEjs, themeEditEjs){
	function ThemeView() {
		 
	}

	ThemeView.prototype.showThemeListDialog = function(themeList, callback) {
		// TODO: Ajax dialog
		console.log(themeList, callback);
		this.themeListDialog = new Dialog({
			title: "Тематические отчеты",
			content: window.ejs.render(themeEjs, {themeList: themeList}),
			'class': "theme-list-window"
		});
		this.themeListDialog.show();
		callback();
	};
	
	ThemeView.prototype.showThemeEditDialog = function(callback) {
		// TODO: Ajax dialog
		if (this.themeEditDialog) {
			this.themeEditDialog.destroyRecursive();
		}
		this.themeEditDialog = new Dialog({
			title: "Тематический отчет :: Параметры",
			content: window.ejs.render(themeEditEjs),
			'class': "theme-edit-window"
		});
		callback();
		this.themeEditDialog.show();
		
	};

	
	ThemeView.prototype.hideThemeEditDialog = function() {
		this.themeEditDialog.hide();
	};
	
	return new ThemeView;
 })
 
 
 