define("wgs45/model/theme", ['dojo/request/xhr', "dojo/DeferredList", "dojo/_base/Deferred"], function(xhr, DeferredList, Deferred) {
    function ThemeModel() {
        this.styleTemplate = {
            "Fill_FillPattern": "Solid",
            "Fill_ForegroundColor": "0dee20",
            "Fill_BackgroundColor": "000000",
            "Stroke_LineStyle": "Solid",
            "Stroke_Unit": "Centimeters",
            "Stroke_Color": "000000",
            "Srtoke_Thickness": "0",
            "Stroke_SizeContext": "DeviceUnits",
            "TransparencyPercent": 0,
            "Transparency": ""
        };
    };

    ThemeModel.prototype.get = function(callback) {
        xhr.post("/map/themes/list", {
            handleAs: "json",
            data: {}
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };

    ThemeModel.prototype.add = function(themeName, isThemeShared, isThemeArhive, mapName, callback) {
        xhr.post("/map/themes/create", {
            handleAs: "json",
            data: {
                object: "themes",
                params: JSON.stringify({
                    "NAME": themeName,
                    "SHARED": isThemeShared,
                    "IS_ARCHIVE": isThemeArhive,
                    "MAP": mapName
                })
            }
        }).then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });

    };

    ThemeModel.prototype.updateTheme = function(theme, callback) {
        xhr.post("/map/themes/update", {
            handleAs: "json",
            data: {
                object: "themes",
                params: JSON.stringify({
                    "THEME_ID": theme.id,
                    "NAME": theme.name,
                    "SHARED": theme.isShared,
                    "IS_ARCHIVE": theme.isArhive,
                    "POSITION": theme.position,
                    "MAP": theme.map
                })
            }
        }).then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });
    };

    ThemeModel.prototype.showTheme = function(themeId, mapName, callback) {

        window.WGS_APP.openMap(mapName).then(function(result) { //успех
            var mapProps = window.WGS_APP.getCurrentMapProps();

            xhr.post("/map/themes/displaytheme", {
                data: {
                    'THEME_ID': themeId,
                    'mapname': mapProps.mapName,
                    'session': mapProps.sessionId
                },
                handleAs: 'json'
            }).then(function(result) {
                //TODO: Сделать без reloadMap
                //map.reloadMap()
                callback(null);
                console.log(window.m = mapProps.map);
                mapProps.map.reloadMap();
                
                if(result.length) {
                    var mapEventsCount = 0;
                    document.body.addEventListener('map-reloaded', selectLayerOnmapLoad);
                    document.body.addEventListener('wgs-legend-loaded', selectLayerOnmapLoad);
                }
                
                function selectLayerOnmapLoad(evt) {
                    if (++mapEventsCount == 2) {
                        window.WGS_APP.selectLayerGroupByLayerId(mapName, result[0]);
                        document.body.removeEventListener('wgs-legend-loaded', selectLayerOnmapLoad);
                        document.body.removeEventListener('map-loaded', selectLayerOnmapLoad);
                    }
                }
                
            }, function(error) {
                callback(error);
            });
        }, function(error) {
            callback(error);
        });


    };

    ThemeModel.prototype.deleteTheme = function(themeId, callback) {
        xhr.post("/map/themes/delete", {
            handleAs: "json",
            data: {
                'THEME_ID': themeId
            }
        }).then(function(result) {
            callback(null);
        }, function(error) {
            callback(error);
        });
    };

    ThemeModel.prototype.getThemeLayers = function(themeId, callback) {
        xhr.post("/map/themes/themelayerlist", {
            handleAs: "json",
            data: {
                'themeId': themeId
            }
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });
    };



    ThemeModel.prototype.getThemeLayersParams = function(themeId, callback) {
        var d1 = getThemeLayers(themeId);
        var d2 = getFeatureLayersAndObjectTypes();

        var dl = new DeferredList([d1, d2 /*,d3*/ ]);
        dl.then(function(result) {
            var themeLayersParams = {};
            themeLayersParams.themeLayers = result[0][1].records;
            themeLayersParams.featureLayers = result[1][1];
            //themeLayersParams.objectTypes = result[2][1];
            themeLayersParams.themeId = themeId;
            callback(null, themeLayersParams);
        }, function(error) {
            callback(error);
        });
    };

    function getFeatureLayersAndObjectTypes() {
        var d = new Deferred();
        xhr.get("/objectadmin/getlayers", {
            handleAs: "json"
        }).then(function(result) {
            d.resolve(result);
        });
        return d;
    };

    function getThemeLayers(themeId) {
        var d = new Deferred();
        xhr.post("/map/themes/themelayerlist", {
            handleAs: "json",
            data: {
                'themeId': themeId
            }
        }).then(function(result) {
            d.resolve(result);
        });
        return d;
    };


    ThemeModel.prototype.addThemeLayer = function(themeLayerParams, callback) {
        var themeId = themeLayerParams.themeId;
        themeLayerParams = JSON.stringify(themeLayerParams);

        xhr.post("/map/themes/addthemelayer", {
            handleAs: "json",
            data: {
                records: themeLayerParams,
                themeId: themeId
            }
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });
    };

    ThemeModel.prototype.removeThemeLayer = function(themeLayerId, themeId, callback) {
        xhr.post("/map/themes/removethemelayer", {
            handleAs: "json",
            data: {
                records: themeLayerId,
                themeId: themeId
            }
        }).then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });
    };


    ThemeModel.prototype.updateThemeLayer = function(themeLayerParams, callback) {
        themeLayerParams = JSON.stringify(themeLayerParams);

        xhr.post("/map/themes/editthemelayer", {
            handleAs: "json",
            data: {
                records: themeLayerParams
            }
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });
    };



    ThemeModel.prototype.getPropertyList = function(featureTypes, callback) {
        featureTypes = JSON.stringify(featureTypes);
        xhr.post("/objectadmin/search/getpropertylist", {
            handleAs: "json",
            data: {
                featureTypes: featureTypes
            }
        }).then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });
    };

    ThemeModel.prototype.getValuesByPropertyId = function(featureTypes, propertyId, propertyType, callback) {
        featureTypes = JSON.stringify(featureTypes);
        xhr.post("/objectadmin/search/getvaluesbypropertyid", {
            handleAs: "json",
            data: {
                featureTypes: featureTypes,
                propertyId: propertyId,
                propertyType: propertyType
            }
        }).then(function(result) {
            callback(null, result);
        }, function(error) {
            callback(error);
        });
    };



    return new ThemeModel();
})