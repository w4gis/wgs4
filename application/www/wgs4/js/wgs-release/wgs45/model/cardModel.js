define("wgs45/model/cardModel", ['dojo/request/xhr', 'dojo/Deferred'], function(xhr, Deferred) {
    "use strict";

    function CardModel(isDirectIds) {
        this.isDirectIds = isDirectIds || false;
    }

    CardModel.prototype.getCardTypes = function(filterLayer, featureId) {
        return xhr.get("/objectadmin/getAttrTypes"+(filterLayer!==undefined && featureId!==undefined ? "?filterLayer="+filterLayer+"&featureId="+featureId  : ""), {
            handleAs: 'json'
        });
    };

    CardModel.prototype.get = function(featureIds) {
        var self = this;
        var deferred = new Deferred();
        this["getNative" + (this.isDirectIds ? "ByObjectId": "")] (featureIds).then(
        //success
        function(cardDescriptions) {
            console.log(cardDescriptions);
            if (featureIds.length > 1) deferred.resolve(self.getParsedMultipleCardDescription(cardDescriptions));
            else deferred.resolve(self.getParsedCardDescription(cardDescriptions));
        },
        //error
        function(error) {
            console.log("Exception: server error. ", error);
            deferred.reject(error);
        });
        return deferred;
    };

    CardModel.prototype.save = function(card) {
        if(~card.name.toLowerCase().indexOf('без наименования')) {
            card.name = '';
        }
        var url = card.featureIds?'/objectadmin/card/setmultiplecards':'/objectadmin/card/setcard';
        return xhr.post(url/*+'x'*/, {
            data: card,
            handleAs: 'json'
        });
    };

    CardModel.prototype.printCardOne = function(cardId) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "/objectadmin/card/Print");
        form.setAttribute('target', '_blank');
        
        var hiddenField = document.createElement("input");        
        hiddenField.setAttribute("name", 'objectId');
        hiddenField.setAttribute("value", cardId);
        form.appendChild(hiddenField);
        document.body.appendChild(form);    // Not entirely sure if this is necessary            
        form.submit();
        document.body.removeChild(form);
    };

    CardModel.prototype.getParsedCardDescription = function(cardDescriptions) {
        var self = this;
        var result = [];
        cardDescriptions[1].forEach(function(description, descriptionId) {

            /*var isChild = false;

            for (var relationIndex = 0; relationIndex < cardDescriptions[3][descriptionId].length; relationIndex++) {
                for (var relationLinkIndex = 0; relationLinkIndex < cardDescriptions[3][descriptionId][relationIndex].length; relationLinkIndex++) {
                    if (description.id == cardDescriptions[3][descriptionId][relationIndex][relationLinkIndex].id) {
                        isChild = true;
                        break;
                    }
                }
            }
            if (isChild) {
                return;

            }*/

            result.push(description);
            var currentCard = result[result.length - 1];
            currentCard.isCurrent = cardDescriptions[0][descriptionId].cardIsCurrent;
            currentCard.propertyGroups = [];
            if (cardDescriptions[2][descriptionId]) {
                console.log(cardDescriptions[2][descriptionId]);
                cardDescriptions[2][descriptionId].forEach(function(property, propertyId) {
                    console.log('!!', property)
                    self._addPropertyIntoGroup(currentCard, property)
                });
            }
            currentCard.relations = [];
            //console.log("!!!", cardDescriptions[3][descriptionId]);
            
            if (cardDescriptions[3][descriptionId] !== null) {
                currentCard.relations = cardDescriptions[3][descriptionId];
                /*cardDescriptions[3][descriptionId].forEach(function(relation, relationIndex) {
                    if(!currentCard.relations[relation.relationId])
                    currentCard.relations[relation.relationId] = {
                        relationTypeId: relation.relationTypeId,
                        name: relation.name,
                        objectTypes: []
                    };
                    currentCard.relations[relation.relationId].objectTypes[relation.objtypeRelationId] = relation.linkedCards;
                });*/
            }
            /*currentCard.relations = {
                countTypeRelation: cardDescriptions[3][descriptionId].countTypeRelation,
                name: cardDescriptions[3][descriptionId].name,
                relationId: cardDescriptions[3][descriptionId].relationId,
                relationTypeId: cardDescriptions[3][descriptionId].relationTypeId,
                links: []
            };
            var links = result[result.length - 1].relation.links;
            cardDescriptions[4][descriptionId].forEach(function(relation, relationIndex) {
                links[relation.objRelationId] = cardDescriptions[4][descriptionId][relationIndex];
            });*/
        });

        return result;
    };

    CardModel.prototype._addPropertyIntoGroup = function(currentCard, property) {
        if (!currentCard.propertyGroups[property.groupId]) {
            currentCard.propertyGroups[property.groupId] = {
                properties: [],
                name: property.groupName
            };
        }

        var group = currentCard.propertyGroups[property.groupId];
        group.properties[property.id] = {
            name: property.name,
            valueTypeId: property.valueTypeId,
            value: property.value || ""
        };
        var currentProperty = group.properties[property.id];
        if(property.domain){
            currentProperty.domains = [];
            property.domain.forEach(function(domain, domainIndex) {
                currentProperty.domains[domain.Value_Domain_ID] = {
                    value: domain.Value_Domain
                };
            });
        }
    };


    CardModel.prototype.getParsedMultipleCardDescription = function(cardDescriptions) {
        var self = this;
        var result = [];
        cardDescriptions[0].forEach(function(cardBaseProperty, cardBasePropertyIndex) {
            console.log(cardBasePropertyIndex + 1);
            var currentCard = {};
            currentCard = cardBaseProperty;
            console.log(currentCard.name + "; " + cardBaseProperty.name);
            // currentCard.featureLayerId = cardDescriptions[1][cardBasePropertyIndex + 1].featureLayerId;
            currentCard.typeName = cardDescriptions[0][cardBasePropertyIndex].objectTypeName + ': ' + cardDescriptions[0][cardBasePropertyIndex].cardCount;
            currentCard.objectTypeId = cardDescriptions[0][cardBasePropertyIndex].objectTypeId;
            currentCard.propertyGroups = [];
            cardDescriptions[1][cardBasePropertyIndex].forEach(function(property, propertyIndex) {
                
                self._addPropertyIntoGroup(currentCard, property);

                cardDescriptions[2][cardBasePropertyIndex].forEach(function(propertyValue, propertyValueIndex) {
                    if (property.id == propertyValue.propertyId){
                        currentCard.propertyGroups[property.groupId].properties[property.id].value = propertyValue.propertyValue;
                    }
                });

            });
            result[cardBasePropertyIndex] = currentCard;
        });
        return result;
    };
    
    CardModel.prototype.getNative = function(featureIds) {
        
        return xhr.post("/objectadmin/card/getcardsbyfeatureid", {
            // sync: true,
            handleAs: "json",
            data: {
                'WgsVersion': 4,
                'featureIds': JSON.stringify(featureIds)
            }
        });
    };
    
    CardModel.prototype.getNativeByObjectId = function(featureIds) {
        return xhr.post("/objectadmin/card/getcardsbyfeatureid", {
            // sync: true,
            handleAs: "json",
            data: {
                'WgsVersion': 4,
                'isDirectIds' : 1,
                'featureIds': JSON.stringify(featureIds)
            }
        });
    };

    CardModel.prototype.removeObjectRelation = function(objRelationId) {
        return xhr.post('/objectadmin/card/RemoveObjectRelation', {
            data: {
                relationsId: JSON.stringify([objRelationId]),
                removeMode: 1,
                WgsVersion: 4
            }
        });
    };

    CardModel.prototype.bindChildObject = function(parentId, childId, relationId) {
        return xhr.post('/objectadmin/card/BindChildObject', {
            data: {
                parentObjectId: parentId,
                childObjectId: childId,
                objTypeRelationId: relationId
            }
        });
    };

    CardModel.prototype.createChildObject = function(parentId, objTypeRelationId, relationId) {
        return xhr.post('/objectadmin/card/createChildCard', {
            data: {
                parentId: parentId,
                objTypeRelationId: objTypeRelationId,
                relationId: relationId
            }
        });
    };

    CardModel.prototype.createObjectForFeature = function(objectTypeId, featureId) {
        return xhr.post('/objectadmin/CreateAndBindObjectToFeature', {
            data: {
                objectTypeId: objectTypeId,
                featureId: featureId
            }
        });
    };

    CardModel.prototype.removeObject = function(objectId, removeMode) {
        return xhr.post('/objectadmin/card/removeObjectNew', {
            data: {
                objectId: objectId,
                removeMode: removeMode
            }
        });
    };

    return CardModel;
});