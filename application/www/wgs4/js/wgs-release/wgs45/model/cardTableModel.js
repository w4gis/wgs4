define("wgs45/model/cardTableModel", ['dojo/request/xhr', 'dojo/Deferred'],
    function(xhr, Deferred) {
        "use strict";

        function CardTableModel() {

        }


        CardTableModel.prototype.getCardTypes = function() {
            var types = [];
            var deferredResult = new Deferred();
            xhr.get("/objectadmin/getAttrTypes?noempty=1", {
                handleAs: 'json'
            }).then(function(types) {
                var permittedMaps = window.WGS_APP.getMaps();
                types.forEach(function(type) {
                    var permittedLayers = [];
                    type.layers.forEach(function(layerDef) {
                        permittedMaps.forEach(function(map) {
                            if(layerDef.groupId == map.layerGroup) {
                                layerDef.mapName = map.mapName;
                                permittedLayers.push(layerDef);
                            }
                        });
                    });
                    type.layers = permittedLayers;
                });
                deferredResult.resolve(types);
            });
            
            return deferredResult;
        };

        CardTableModel.prototype.getTableColumns = function(cardType, groupId) {
            var layerId = -1;
            for (var i = 0; i < cardType.layers.length; i++) {
                if (cardType.layers[i].groupId == groupId) {
                    layerId = cardType.layers[i].id;
                    break;
                }
            }
            console.log('layerId ', layerId);
            var url = "/objectadmin/cardTable/getCardTableHeader?cardType=" + cardType.id + "&layerId=" + layerId;
            return xhr.get(url, {
                handleAs: 'json'
            });
        };

        return CardTableModel;
    });
