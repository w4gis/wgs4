require({cache:{
'url:wgs45/package.json':"{\r\n    \"name\": \"wgs4.campus\",\r\n    \"description\": \"Web-based geographic information system\",\r\n    \"version\": \"0.25.2\",\r\n    \"keywords\": [\"GIS\",\"web\",\"WGS\"],\r\n    \"maintainers\": [{\r\n        \"name\": \"Mikhael Milikhin\"\r\n    }],\r\n    \"contributors\": [{\r\n        \"name\": \"Mikhael\"\r\n    },{\r\n        \"name\": \"MixRich1\"\r\n    }],\r\n    \"licenses\": [{\r\n    }],\r\n    \"dependencies\": {\r\n        \"dojo\": \"~1.10\",\r\n        \"dijit\": \"~1.10\",\r\n        \"dojox\": \"~1.10\"\r\n    },\r\n    \"main\": \"Application\",\r\n    \"dojoBuild\": \"wgs45.profile.js\"\r\n}\r\n"}});
define("wgs45/model/wgs-app", [
    "dojo/Deferred",
    "dojo/DeferredList",
    "dojo/request/xhr",
    "dojo/text!wgs45/package.json"], function(Deferred, DeferredList, xhr, packageInfo) {

    function WgsApp(wgsConfig) {
        var appInfo = JSON.parse(packageInfo);

        this._prohibitedCommands = {};
        this._version = appInfo.version;
        this._versionFlags = {
            'debug': isDebug(),
            'next': wgsConfig.FLAGS.NO_DRIVERS > 0
        };
        this._maps = [];
        this._authenticated = false;

        var authInfoDeferred = this._setupAuthInfo();
        var getMapsDeferred = this._getAllMaps();


        this.initDeferred = new DeferredList([authInfoDeferred, getMapsDeferred]);

    }

    WgsApp.prototype.getSelectionXml = function(mapName) {
        return window.WGS_APP.selText[mapName];

    };

    WgsApp.prototype.getVersion = function() {
        var flagsString = "";
        for (var i in this._versionFlags) {
            flagsString += (this._versionFlags[i] ? (i + ", ") : "");
        }

        flagsString = flagsString.slice(0, -2);
        
        return this._version +  (flagsString ? (" (" + flagsString + ")") : '');
    };

    WgsApp.prototype.getMaps = function() {
        return this._maps;
    };

    WgsApp.prototype.getAuthStatus = function() {
        return this._authenticated;
    };

    WgsApp.prototype.getProhibitedCommands = function() {
        return this._prohibitedCommands;
    }
    /**
     *  PRIVATE METHODS
     */
    WgsApp.prototype._setupAuthInfo = function() {
        var authInfoDeferred = new Deferred();
        var self = this;

        xhr.get("/authorization/status", {
            handleAs: "json"
        }).then(function(state) {
            for (var i in state) {
                if (i != "auth") {
                    self._prohibitedCommands[i] = true;
                }
            }
            self._authenticated = !! state.auth;

            authInfoDeferred.resolve({
                isAuthenticated: !! state.auth
            });
        }, function() {
            self._authenticated = false;

            authInfoDeferred.resolve({
                isAuthenticated: false
            });
        });

        return authInfoDeferred;
    }


    WgsApp.prototype._getAllMaps = function() {
        var self = this;
        var getMapsDeferred = new Deferred();

        xhr.get('/map/all', {
            handleAs: 'json'
        }).then(function(maps) {
            self._maps = maps;
            getMapsDeferred.resolve(maps);
        }, function() {
            getMapsDeferred.reject();
        });

        return getMapsDeferred;
    };


    return WgsApp;


    function isDebug() {
        return location.toString().indexOf('debug') > -1 || location.toString().indexOf('/tusur') > -1;
    }
});