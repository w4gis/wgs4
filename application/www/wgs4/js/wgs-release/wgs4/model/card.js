define("wgs4/model/card", ['dojo/_base/window',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/dom',
    "dojo/date",

    'dojo/sniff',
    'dijit/registry',
    'dojo/topic', 'dojo/date/locale',
    'dojo/dom-attr', 'dojo/on', 'dojo/dom-style', 'dojo/query', 'dojo/_base/array',
    'dojox/mobile/Pane', 'dojo/store/Memory', 'dijit/form/Select', 'dojo/data/ObjectStore',
    'dojo/request/xhr', 'dojo/json'],

function(win, domConstruct, domClass, dom, ddate, has, registry, topic, datelocale, domAttr, on, domStyle,
query, arrayUtil, Pane, Memory, Select, ObjectStore, xhr, JSON) {
 
 
    // 1. function CardModel() {}
    // 2. В модели обращаемся к интерфейсу
    // 3. get(){}
    // 4. var z;
    
    
    return {

        test: function() {
            alert('it works!');
        },

        save: function(fid) {
            if (fid.length > 1) saveMultipleCard();
            else saveCard();
        },

        /*
         *
         *
         *
         *
         *
         *
         *
         *
         */

        saveCard: function(card) {

            var self = this;
            if (this.changedCards.length > 0) {
                for (var i2 = 0; i2 < this.changedCards.length; i2++) {
                    var cid = this.changedCards[i2];
                    var dfr = {};
                    dfr.id = cid;
                    dfr.name = registry.byId("valueNameCard-" + cid + '-').get('value');
                    dfr.shortName = registry.byId("valueShortName-" + cid + '-').get('value');
                    dfr.typeName = registry.byId("valueTypeName-" + cid + '-').get('value');
                    dfr.note = registry.byId("valueNote-" + cid + '-').get('value');
                    if ("ids" in this.objProps[cid.toString()]) {
                        var ids = this.objProps[cid.toString()].ids;
                        for (var i = 0; i < ids.length; i++) {
                            if (ids[i] >= 0) {
                                dfr['property-' + ids[i]] = registry.byId("propValue-" + cid + '-' + ids[i]).get('value');
                            } else {
                                console.log(ids[i]);
                                var iddmn = registry.byId("divForPropertyDomain" + cid + (-ids[i])).get('value');
                                dfr['property-' + (-ids[i])] = iddmn.substr(iddmn.indexOf('domn') + 4);
                            }
                        }

                    }
                    console.log(dfr);
                    xhr.post("/objectadmin/card/SetCard", {
                        handleAs: "json",
                        data: dfr
                    }).then(function() {
                        console.log('Saved!!');
                        domStyle.set('cardChangesSaved', 'display', 'block');
                        setTimeout(function() {
                            domStyle.set('cardChangesSaved', 'display', 'none');
                        }, 3000);
                    });                    console.log("select not add");
                    
                }
            }

        },

        /*
         *
         *featureIds;
         *objectTypeId;
         *propIds; values (name, shortName, note)
         *
         */

        saveMultipleCards: function() {

        },


        get:function() {
            return {
                cards: [{
                    cardId: "16234",
                    cardLabel: "второе - Здания - Тестовое описание",
                    cardName: "второе - Здания - Тестовое описание",
                    createDate: "2015-04-29 11:04:02",
                    isCurrent: true,
                    editUser: "источник : ГИС;  пользователь : demo; хост : 109.227.227.180",
                    name: '',
                    note: '',
                    shortName: "",
                    typeName: "Здания - Тестовое описание",
                    props: [{
                        groupId: '3',
                        groupName: "Ремонт и обслуживание",
                        id: "11",
                        name: "Обслуга",
                        value: "",
                        valueTypeId: "1"
                    }, {
                        groupId: '3',
                        groupName: "Ремонт и обслуживание",
                        id: "12",
                        name: "Обслуга2",
                        value: "dfzbdzrh",
                        valueTypeId: "1"
                    }]
                }, {
                    cardId: '16235',
                    cardLabel: 'dfbfzdbzdb',
                    cardName: 'аиваяиаиа',
                    isCurrent: false,
                    createDate: "2015-04-29 11:04:02",
                    editUser: "источник : ГИС;  пользователь : demo; хост : 109.227.227.180",
                    name: '',
                    note: '',
                    shortName: "",
                    typeName: "Здания - Тестовое описание",
                }]
            };

        },

        /*
         * Gets information about cards of selected object;
         *
         * @fid - feature ID
         *
         */
        getCardsOfSelectedFeature: function(fid) {
            var self = this;
            var cards = {};
            // запрос на список карточек объекта
            console.log(fid.length);
            //fid=JSON.stringify(fid);
            xhr.post("/objectadmin/card/getcardsbyfeatureid", {
                sync: true,
                handleAs: "json",
                data: {
                    'WgsVersion': 4,
                    'featureIds': JSON.stringify(fid)
                }
            }).then(function(data) {
                //fid = JSON.parse(fid);
                //console.log('data');
                //console.log(data);
                ///*
                if (fid.length > 1) {
                    //self.clearCard();

                    //console.log('основные характеристики='+data[1][0].name);
                    cards.cards = [];
                    cards.cards[0] = [];
                    //var dataForMemory = [];
                    for (var i = 0; i < data[0].length; i++) {
                        cards.cards[0][i] = {};
                        var z = cards.cards[0][i];
                        z.cardName = data[0][i].name;
                        z.label = data[0][i].label;
                        z.objectTypeId = data[0][i].objectTypeId;
                        z.featureLayerId = data[0][i].featureLayerId;

                        /*dataForMemory[i] =
							{
								id: 'ccbi' + z.objectTypeId,
								value: 'ccbi' + z.objectTypeId,
								label: z.cardName
							};*/
                    }

                    cards.cards[1] = [];
                    for (i = 0; i < data[1].length; i++) {
                        cards.cards[1][i] = {};
                        var z = cards.cards[1][i];
                        z.name = data[1][i][0].name;
                        z.note = data[1][i][0].note;
                        z.shortName = data[1][i][0].shortName;
                    }

                    cards.cards[2] = [];
                    for (i = 0; i < data[2].length; i++) {
                        cards.cards[2][i] = [];
                        for (j = 0; j < data[2][i].length; j++) {
                            cards.cards[2][i][j] = {};
                            var z = cards.cards[2][i][j];
                            z.groupId = data[2][i][j].groupId;
                            z.groupName = data[2][i][j].groupName;
                            z.id = data[2][i][j].id;
                            z.Propertyname = data[2][i][j].name;
                            z.valueTypeId = data[2][i][j].valueTypeId;
                            for (k = 0; k < data[3][i].length; k++) {
                                if (z.id == data[3][i][k].propertyId) {
                                    z.propertyValue = data[3][i][k].propertyValue;
                                    break;
                                }
                            }
                        }
                    }
                    //console.log(cards);

                } else {
                    cards = data;
                    //cards.cards = [];
                    /*var dataForMemory = [];
						for (var i = 0; i < data.length; i++)
						{

							cards.cards[i] = {};
							var CardObject = cards.cards[i];
							CardObject.cardId = data[i].cardId;
							CardObject.cardName = data[i].cardName;

							CardObject.isCurrent = data[i].cardIsCurrent === '1';
							CardObject.relationName = data[i].cardRelationName;
							CardObject.cardLabel = data[i].cardLabel;
							self.getPropertiesOfNCard(CardObject);
						}*/
                    //while (!CardObject.note || !CardObject.props || !(CardObject.props[CardObject.props.length - 1].value)) {}
                    // || !CardObject.rels || !(CardObject.rels[CardObject.rels.length-1].countTypeRelation)){}
                }
                //*/
            }, function(err) {
                domConstruct.place("<p>error!!!: <p>" + err.response.text + "</p></p>", "targetForCardSwaps");
            });
            return cards;
        },

        /*
         * Gets properties of <CardObject.cardId> card;
         *
         * @cardObject
         *
         */
        getPropertiesOfNCard: function(CardObject) {
            var self = this;
            var cnt = 0;
            // запрос на основное описание карточки
            xhr.post("/objectadmin/card/getvaluebaseproperties", {
                sync: true,
                handleAs: "json",
                data: {
                    'objectId': CardObject.cardId
                }
            }).then(function(data2) {
                console.log("daaaataaaa");
                console.log(data2);
                cnt++;
                //for (var i2=0; i2<data2.length;i2++){
                //CardObject.id = data2[0].id;
                CardObject.name = data2[0].name;
                CardObject.shortName = data2[0].shortName;
                CardObject.typeName = data2[0].typeName;
                CardObject.createDate = data2[0].createDate;
                CardObject.editUser = data2[0].edit_user;
                CardObject.note = data2[0].note;
                /*self.selCard.addOption(
					{
						value: 'ccbi' + CardObject.cardId,
						label: (CardObject.name == '' ? 'Без наименования' : CardObject.name) + ' - ' + CardObject.typeName
					});*/
                /*this.cardsMemoryStore.set({
					id : 'ccbi' + CardObject.cardId,
					label : CardObject.name
					});
					this.cardSelectWidget.addOption({
					id : 'ccbi' + CardObject.cardId,
					label : CardObject.name
					});*/
            });

            // запрос на характеристики
            xhr.post("/objectadmin/card/GetValueProperties", {
                sync: true,
                handleAs: "json",
                data: {
                    'objectId': CardObject.cardId
                }
            }).then(function(data2) {
                cnt++;
                var ic2 = 0;
                CardObject.props = [];
                data2.sort(function(a, b) {
                    return (a.groupName > b.groupName) ? -1 : 1
                })
                for (var i2 = 0; i2 < data2.length; i2++) {
                    CardObject.props[i2] = {};
                    var CardObjectPropetry = CardObject.props[i2];
                    CardObjectPropetry.id = data2[i2].id;
                    CardObjectPropetry.name = data2[i2].name;
                    CardObjectPropetry.valueTypeId = data2[i2].vulueTypeId; //
                    CardObjectPropetry.groupId = data2[i2].groupId;
                    CardObjectPropetry.groupName = data2[i2].groupName;
                    CardObjectPropetry.value = data2[i2].value;

                    //if (CardObjectPropetry.valueTypeId == '5') ids[i2] = -CardObjectPropetry.id;
                    //else ids[i2] = CardObjectPropetry.id;

                    var queryForDomains = function(CardObject, CardObjectPropetry1) {
                        xhr.post("/objectadmin/card/GetValueDomainList", {
                            sync: true,
                            handleAs: "json",
                            data: {
                                'propId': CardObjectPropetry1.id
                            }
                        }).then(function(data3) {
                            cnt++;
                            //console.log('Server responsed domains');
                            CardObjectPropetry1.domains = [];
                            var dataForDomain = [{
                                id: "",
                                label: "Выберите значение...",
                                disabled: true
                            }];
                            for (var i3 = 0; i3 < data3.length; i3++) {
                                CardObjectPropetry1.domains[i3] = {};
                                var cd = CardObjectPropetry1.domains[i3];
                                cd.id = data3[i3].id;
                                cd.name = data3[i3].name;
                                //console.log(cd.id+cd.name);
                                dataForDomain[dataForDomain.length] = {
                                    id: "domainValue" + CardObject.cardId + "prop" + CardObjectPropetry1.id + "domn" + cd.id,
                                    label: cd.name
                                };
                                //console.log('DOMAIN!!:' + dataForDomain[i3].label);
                            }
                            console.log('!!!!!', dataForDomain);
                        });
                    }
                    // запрос на список значений доменов

                    console.log(CardObjectPropetry.valueTypeId);
                    if (Number(CardObjectPropetry.valueTypeId) == 5) {
                        queryForDomains(CardObject, CardObjectPropetry);
                    }
                }
            });
            // запрос на список отношений
            xhr.post("/objectadmin/card/GetListRelByObject", {
                sync: true,
                handleAs: "json",
                data: {
                    'objectId': CardObject.cardId
                }
            }).then(function(data2) {
                cnt++;
                CardObject.rels = [];
                for (var i2 = 0; i2 < data2.length; i2++) {
                    CardObject.rels[i2] = {};
                    var CardObjectPropetry = CardObject.rels[i2];
                    CardObjectPropetry.relationId = data2[i2].relationId;
                    CardObjectPropetry.name = data2[i2].name;
                    CardObjectPropetry.relationTypeId = data2[i2].relationTypeId;
                    CardObjectPropetry.countTypeRelation = data2[i2].countTypeRelation;

                    var addRelation = function(name, cardId, relationId) {
                        xhr.post("/objectadmin/card/GetListChildObjectByRelation", {
                            handleAs: "json",
                            data: {
                                sync: true,
                                'objectId': cardId,
                                'relationId': relationId
                            }
                        }).then(function(data3) {
                            cnt++;
                            CardObjectPropetry.cards = [];
                            for (var i3 = 0; i3 < data3.length; i3++) {
                                CardObjectPropetry.cards[i3] = {};
                                var CardObjectRelations = CardObjectPropetry.cards[i3];
                                CardObjectRelations.id = data3[i3].id;
                                CardObjectRelations.name = (data3[i3].name ? data3[i3].name : 'Без наименования') + ' (' + data3[i3].typeName + ')';
                                CardObjectRelations.relId = data3[i3].objRelationId;
                                console.log(CardObjectRelations, data3[i3]);
                            }

                        });
                    }
                }
            });
            //while(cnt<4){}
        }


    }

})