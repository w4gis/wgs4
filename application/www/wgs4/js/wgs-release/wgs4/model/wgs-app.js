define("wgs4/model/wgs-app", [
    "dojo/sniff",
    'dijit/registry',
    'wgs4/model/view-base'],

function(has, registry, ViewBase) {

    function WgsApp(wgsConfig) {
        var self = this;

        this.version = {
            major: 0,
            minor: 4,
            patch: 2,
            isDebug: location.toString().indexOf('debug') > -1,
            state: location.toString().indexOf('debug') > 0 ? ('(dev' + (wgsConfig.FLAGS.NO_DRIVERS > 0 ? ", next)" : ")")) : (wgsConfig.FLAGS.NO_DRIVERS > 0 ? " next" : ""),
            toString: function() {
                return self.version.major + '.' + self.version.minor + '.' + self.version.patch + ' ' + self.version.state + '';
            },
        };

        this._setupOptions();
        this.startApplication();
    }

    WgsApp.prototype._setupOptions = function() {
        var self = this;

        this.options = {
            //Счетчик, использующийся для перехода в debug версию по нажатию на номер версии в приложении
            _dCounter: 0,
            _hasOpenLayers: false,

            //list of prohibited user commands
            //received in map.js
            prohibited: {},
            WGS_BUSY: false, //by default, map is not busy, becomes true when loading map
            MapGroupSwitcher: {},

            //Версия приложения строкой
            getVersion: function() {
                return self.version.toString();
            },

            //переход в debug версию из production по нажатию на какую-нибудь кнопку/div/... два раза
            //Используется счетчик _dCounter
            maybeDebug: function() {
                this._dCounter += 1;
                if (this._dCounter > 1 && !self.version.isDebug) {
                    location.assign('/mobile/debug');
                }
            },

            hasOpenLayersChangeHandler: function() {
                this._hasOpenLayers = !self._hasOpenLayers;
            },

            /* 
             * Все функции, перечисленные в selectionCallbacks выполняются по очереди при выделении объекта на
             * карте (в качестве параметра получают список выделенных объектов в формате Fusion 2.6). Флаг active 
             * используется для активации/деактивации обработчика. Сама функция передается в параметре fn.
             */
            selectionCallbacks: {
                'console': {
                    'fn': function(selectionData) {
                        console.log(selectionData);
                    },
                    'active': this.version.isDebug
                }
            }
        };
    };

    WgsApp.prototype.startApplication = function() {
        var self = this;

        require(['dojo/request/xhr'], function(xhr) {
            xhr.get("/authorization/status", {
                handleAs: "json"
            }).then(self._onLoginStateReceived.bind(self), self._onLoginStateError.bind(self));
        });
    };

    WgsApp.prototype._doStartApplication = function() {
        var self = this;
        console.log("WGS4 v" + this.version);

        require(["dojox/app/main", "dojo/text!/wgs4/js/etc/config/app-config.json"], function(Application, appConf) {
            self.appConf = JSON.parse(appConf);
            self._setupViews();

            //start application from defaultView
            location.hash = "";
            Application(self.appConf);
        });
    };

    WgsApp.prototype._setupViews = function() {
        /*
         * If device 'css width' is < 600 use phonemod.
         * Else use tablet mode
         * */
        var width = window.innerWidth || document.documentElement.clientWidth;
        if (width <= 600) {
            has.add("phone", true);
            document.body.classList.add('phone');

            for (var i in this.appConf.views) {
                if (this.appConf.views[i].constraint == "left") {
                    this.appConf.views[i].constraint = "center";
                }
            }
        } else {
            document.body.classList.add('tablet');
        }

        //view history management is different in ie9orLess and web browsers
        has.add("ie9orLess", has("ie") && (has("ie") <= 9));
    };

    WgsApp.prototype._onLoginStateError = function() {
        location.assign('/info');
        return;
    };

    WgsApp.prototype._onLoginStateReceived = function(state) {
        if (state.auth === 0) {
            location.assign('/info');
            return;
        }

        for (var i in state) {
            if (i != "auth") {
                window.AppData.prohibited[i] = true;
            }
        }

        //we are using different events for mouse- and touch devices
        window.IS_MOUSE = false;
        window.IS_TOUCH = false;

        window.AppData.ViewBaseClass = ViewBase;

        //run Applications
        this._doStartApplication();
    };

    return WgsApp;
});