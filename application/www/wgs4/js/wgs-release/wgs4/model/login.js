/*if 'return' cookie is set - redirect there after login
  else if browser is mobile - goto mobile
else redirect to information
*/

define("wgs4/model/login", [
    "dojo/dom-attr", 'dojo/when', 
    "dojo/request/xhr", 'dojo/Deferred'],

function(domAttr, when, xhr, Deferred) {
    
    function Login(ip) {
        this.ip = ip || '127.0.0.1';
    }

    
    Login.prototype.doLogin = function(user, password) {
        if (!user || !password) {
            var result = new Deferred();
            setTimeout(function() {
                result.resolve({
                    message: 'Пожалуйста, введите логин и пароль',
                    exception: '4'
                });
            }, 100);
            return result;
        } else {
            var pasHash = md5(user + '.' + password).toUpperCase();
            var pasWithIpHash = md5(pasHash + this.ip).toUpperCase();
            return xhr.post("/authorization/login", {
                handleAs: "json",
                data: {
                    'password': pasWithIpHash
                }
            });
        }
    };
    
    return Login;
    
    ///////////////////////

    function md5(r){function n(r,n){return r<<n|r>>>32-n}function t(r,n){var t,o,e,u,f;return e=2147483648&r,u=2147483648&n,t=1073741824&r,o=1073741824&n,f=(1073741823&r)+(1073741823&n),t&o?2147483648^f^e^u:t|o?1073741824&f?3221225472^f^e^u:1073741824^f^e^u:f^e^u}function o(r,n,t){return r&n|~r&t}function e(r,n,t){return r&t|n&~t}function u(r,n,t){return r^n^t}function f(r,n,t){return n^(r|~t)}function i(r,e,u,f,i,a,c){return r=t(r,t(t(o(e,u,f),i),c)),t(n(r,a),e)}function a(r,o,u,f,i,a,c){return r=t(r,t(t(e(o,u,f),i),c)),t(n(r,a),o)}function c(r,o,e,f,i,a,c){return r=t(r,t(t(u(o,e,f),i),c)),t(n(r,a),o)}function C(r,o,e,u,i,a,c){return r=t(r,t(t(f(o,e,u),i),c)),t(n(r,a),o)}function g(r){for(var n,t=r.length,o=t+8,e=(o-o%64)/64,u=16*(e+1),f=Array(u-1),i=0,a=0;t>a;)n=(a-a%4)/4,i=a%4*8,f[n]=f[n]|r.charCodeAt(a)<<i,a++;return n=(a-a%4)/4,i=a%4*8,f[n]=f[n]|128<<i,f[u-2]=t<<3,f[u-1]=t>>>29,f}function h(r){var n,t,o="",e="";for(t=0;3>=t;t++)n=r>>>8*t&255,e="0"+n.toString(16),o+=e.substr(e.length-2,2);return o}function d(r){r=r.replace(/\r\n/g,"\n");for(var n="",t=0;t<r.length;t++){var o=r.charCodeAt(t);128>o?n+=String.fromCharCode(o):o>127&&2048>o?(n+=String.fromCharCode(o>>6|192),n+=String.fromCharCode(63&o|128)):(n+=String.fromCharCode(o>>12|224),n+=String.fromCharCode(o>>6&63|128),n+=String.fromCharCode(63&o|128))}return n}var m,v,S,l,A,s,y,b,p,w=Array(),L=7,j=12,k=17,q=22,x=5,z=9,B=14,D=20,E=4,F=11,G=16,H=23,I=6,J=10,K=15,M=21;for(r=d(r),w=g(r),s=1732584193,y=4023233417,b=2562383102,p=271733878,m=0;m<w.length;m+=16)v=s,S=y,l=b,A=p,s=i(s,y,b,p,w[m+0],L,3614090360),p=i(p,s,y,b,w[m+1],j,3905402710),b=i(b,p,s,y,w[m+2],k,606105819),y=i(y,b,p,s,w[m+3],q,3250441966),s=i(s,y,b,p,w[m+4],L,4118548399),p=i(p,s,y,b,w[m+5],j,1200080426),b=i(b,p,s,y,w[m+6],k,2821735955),y=i(y,b,p,s,w[m+7],q,4249261313),s=i(s,y,b,p,w[m+8],L,1770035416),p=i(p,s,y,b,w[m+9],j,2336552879),b=i(b,p,s,y,w[m+10],k,4294925233),y=i(y,b,p,s,w[m+11],q,2304563134),s=i(s,y,b,p,w[m+12],L,1804603682),p=i(p,s,y,b,w[m+13],j,4254626195),b=i(b,p,s,y,w[m+14],k,2792965006),y=i(y,b,p,s,w[m+15],q,1236535329),s=a(s,y,b,p,w[m+1],x,4129170786),p=a(p,s,y,b,w[m+6],z,3225465664),b=a(b,p,s,y,w[m+11],B,643717713),y=a(y,b,p,s,w[m+0],D,3921069994),s=a(s,y,b,p,w[m+5],x,3593408605),p=a(p,s,y,b,w[m+10],z,38016083),b=a(b,p,s,y,w[m+15],B,3634488961),y=a(y,b,p,s,w[m+4],D,3889429448),s=a(s,y,b,p,w[m+9],x,568446438),p=a(p,s,y,b,w[m+14],z,3275163606),b=a(b,p,s,y,w[m+3],B,4107603335),y=a(y,b,p,s,w[m+8],D,1163531501),s=a(s,y,b,p,w[m+13],x,2850285829),p=a(p,s,y,b,w[m+2],z,4243563512),b=a(b,p,s,y,w[m+7],B,1735328473),y=a(y,b,p,s,w[m+12],D,2368359562),s=c(s,y,b,p,w[m+5],E,4294588738),p=c(p,s,y,b,w[m+8],F,2272392833),b=c(b,p,s,y,w[m+11],G,1839030562),y=c(y,b,p,s,w[m+14],H,4259657740),s=c(s,y,b,p,w[m+1],E,2763975236),p=c(p,s,y,b,w[m+4],F,1272893353),b=c(b,p,s,y,w[m+7],G,4139469664),y=c(y,b,p,s,w[m+10],H,3200236656),s=c(s,y,b,p,w[m+13],E,681279174),p=c(p,s,y,b,w[m+0],F,3936430074),b=c(b,p,s,y,w[m+3],G,3572445317),y=c(y,b,p,s,w[m+6],H,76029189),s=c(s,y,b,p,w[m+9],E,3654602809),p=c(p,s,y,b,w[m+12],F,3873151461),b=c(b,p,s,y,w[m+15],G,530742520),y=c(y,b,p,s,w[m+2],H,3299628645),s=C(s,y,b,p,w[m+0],I,4096336452),p=C(p,s,y,b,w[m+7],J,1126891415),b=C(b,p,s,y,w[m+14],K,2878612391),y=C(y,b,p,s,w[m+5],M,4237533241),s=C(s,y,b,p,w[m+12],I,1700485571),p=C(p,s,y,b,w[m+3],J,2399980690),b=C(b,p,s,y,w[m+10],K,4293915773),y=C(y,b,p,s,w[m+1],M,2240044497),s=C(s,y,b,p,w[m+8],I,1873313359),p=C(p,s,y,b,w[m+15],J,4264355552),b=C(b,p,s,y,w[m+6],K,2734768916),y=C(y,b,p,s,w[m+13],M,1309151649),s=C(s,y,b,p,w[m+4],I,4149444226),p=C(p,s,y,b,w[m+11],J,3174756917),b=C(b,p,s,y,w[m+2],K,718787259),y=C(y,b,p,s,w[m+9],M,3951481745),s=t(s,v),y=t(y,S),b=t(b,l),p=t(p,A);var N=h(s)+h(y)+h(b)+h(p);return N.toLowerCase()}
})