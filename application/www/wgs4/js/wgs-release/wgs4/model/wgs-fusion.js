/*
 *	This class redefines some mapGuide Fusion built in classes
 * 	The aim is to update layout/add some functions we needed
 * 	
 * 	Methods
 * 		redefineMessages	- new message window
 * 		redefineLegend		- new legend items checkboxes
 * 		redefineLegendRoot	- new legend folder checkboxes
 * 		redefineLegendWidget- new legend widget with added dojo events
 */

define("wgs4/model/wgs-fusion", ["dojo/_base/declare",'dojo/topic','dojo/query','dojo/dom-class','dojo/sniff'], function(declare, topic, query, domClass, has){
  return declare(null, {

	constructor: function(){
		this.redefineLegendWidget()
		//this.redefineLegend()
		//this.redefineLegendRoot()
		this.redefineMessages()
		this.redefineLayers()
		this.redefineMapMenu()
	},  
	
	//redefine MapMenu
	redefineMapMenu: function(){
		/**
 * Fusion.Widget.MapMenu
 *
 * $Id: MapMenu.js 2770 2013-08-27 11:40:25Z jng $
 *
 * Copyright (c) 2007, DM Solutions Group Inc.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

 /****************************************************************************
 * Class: Fusion.Widget.MapMenu
 *
 * A widget that displays a selection of maps that can be loaded into the 
 * application.  The list of maps is configured in the ApplicationDefinition.
 * 
 * Inherits from:
 *  - <Fusion.Widget>
 * **********************************************************************/

Fusion.Widget.MapMenu = OpenLayers.Class(Fusion.Widget,  {
    uiClass: Jx.Menu,
    domObj: null,
    mapGroupData: null,
    loadOverlaysOnly: false,
    rootFolder: '',
    menus: null,
    initializeWidget: function(widgetTag) {       
        this.extension = this.widgetTag.extension;       
        var widgetLayer = this.getMapLayer();
        this.arch = widgetLayer.arch;
        if (this.arch == 'MapGuide' && this.extension.Folder) {
            this.rootFolder = this.extension.Folder ? this.extension.Folder[0] : 'Library://';
            this.requestURL = 'layers/' + this.arch + '/' + Fusion.getScriptLanguage() +
                          '/MapMenu.' + Fusion.getScriptLanguage();
        } else if (this.arch == 'MapServer' && this.extension.Folder) {
            this.rootFolder = this.extension.Folder ? this.extension.Folder[0] : '/';
            //var s = 'layers/' + this.arch + '/' + Fusion.getScriptLanguage() + '/MapMenu.' + Fusion.getScriptLanguage();
            this.requestURL = this.extension.ListURL ? this.extension.ListURL[0] : '/platform/api/mapsherpa.php';
            this.publishedMapsOnly = this.extension.PublishedMapsOnly ? this.extension.PublishedMapsOnly : false;
            if(this.publishedMapsOnly){
                this.platformRequest = 'listpublishedmaps';
            }
            else
            {
                this.platformRequest = 'listresources';
            }
        }
        this.loadOverlaysOnly = (this.extension.LoadOverlaysOnly && this.extension.LoadOverlaysOnly[0] == 'true') ? true:false;
       
        this.getMapLayer().registerForEvent(Fusion.Event.MAP_SESSION_CREATED, OpenLayers.Function.bind(this.loadMenu, this));
        this.enable();
    },
    
    setUiObject: function(uiObj) {
        Fusion.Widget.prototype.setUiObject.apply(this, [uiObj]);
        
        this.loadMaxExtent = this.extension.LoadMaxExtent ? 
             (this.extension.LoadMaxExtent[0].toLowerCase() == 'true') : false;
        
        //If no folder is specified for enumeration, build a menu
        //from the mapgroup alone. Folders are only supported with MapGuide.
        //Otherwise, create a hash of mapgroup resourceId to mapGroup data
        //to be used to assign mapgroup extensions to enumerated maps.
        
        var mapGroups = Fusion.applicationDefinition.mapGroups;
        this.mapGroupData = {};
        var buttonSet = new Jx.ButtonSet();
        for (var key in mapGroups) {
            if (mapGroups[key].mapId) {
                var mapGroup = mapGroups[key];
                if (this.extension.Folder) {
                    this.mapGroupData[mapGroup.maps[0].resourceId] = mapGroup; 
                } else {
                    var data = mapGroup;
                    window.AppData.MapGroupSwitcher[mapGroup.mapId] = OpenLayers.Function.bind(this.switchMap, this, data)
                    var menuItem = new Jx.Menu.Item({
                        label: mapGroup.mapId,
                        toggle: true,
                        onClick: OpenLayers.Function.bind(this.switchMap, this, data)
                    });
                    buttonSet.add(menuItem);
                    this.uiObj.add(menuItem);
                    
                    // var data = mapGroup;
                    // window.WGS_APP.appState.MapGroupSwitcher[mapGroup.mapId] = OpenLayers.Function.bind(this.switchMap, this, data)
                    // var menuItem = new Jx.Menu.Item({
                    //     label: mapGroup.mapId,
                    //     toggle: true,
                    //     onClick: OpenLayers.Function.bind(this.switchMap, this, data)
                    // });
                    // buttonSet.add(menuItem);
                    // this.uiObj.add(menuItem);
                }
            }
        }
        
    },
    
    loadMenu: function() {

        //console.log("LOADMENU");
        //debugger;
        
        //get the mapdefinitions as xml if there  is a folder specified
        //in the widget tag. All subfolders will be enumerated.
        //FIXME: this should be platform agnostic, Library:// isn't!
        //FIXME: use JSON rather than XML        
        if (this.arch == 'MapGuide' && this.extension.Folder) {
            var params =  {
              parameters: {'folder': this.rootFolder},
              onComplete: OpenLayers.Function.bind(this.processMapMenu, this)
            };
            Fusion.ajaxRequest(this.requestURL, params);
        } else if (this.arch == 'MapServer' && this.extension.Folder) {
            var map = this.getMapLayer();
            var options =  {
                  parameters: {
                    request: this.platformRequest,
                    type: 'map',
                    session:  map.getSessionID(),
                    depth: -1,
                    folder: this.rootFolder
                  },
                  method: 'GET',
                  onComplete: OpenLayers.Function.bind(this.processMSMapMenu, this)
            };
            var temp = new OpenLayers.Ajax.Request(this.requestURL, options);
        };
    },
    
    processMSMapMenu: function(r) {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            //var testData = '{"success":true,"errorMessages":[],"values":[{
            //  "sPath":"/ms4w/apps/gmap/cap/HamiltonLowIncome.map",
            //  "sPermissions":"2",
            //  "sResourceId":"/Hamilton/Hamilton Low Income",
            //  "sMapResource":"/Hamilton/hamilton_low_income"},
            //  {"sPath":"/mnt/FGS_ENVIRONS/fgs-cap/apps/platform/data/home/root/Canada1.map","sPermissions":"2","sResourceId":"/Canada/Canada1","sMapResource":"/Canada/Canada"}],"request":"listpublishedmaps","version":1}';
            //eval("o="+testData);
            this.menus = {};
            this.uiObj.empty();
            if (o.values && o.values.resources) {
              var list = o.values.resources;
              var widgetLayer = this.getMapLayer();
              for (var i=0; i<list.length; i++) {
                  var resource = list[i];
                  var mapId = resource.sResourceId;
                  mapId = mapId.replace(this.rootFolder, '');
                  var folders = mapId.split('/');
                  var label = folders.pop();
                  var path = folders.join('/');
                  this.createFolders(path);
                  
                  // check for mapgroup data and if there is none,
                  // create a maptag that will be passed to the map
                  // widget constructor 
                  var data = {maps:[{'resourceId': resource.sResourceId,
                          'singleTile':true,
                          'type': widgetLayer.arch,
                          'sid': widgetLayer.getSessionID(),
                          'layerOptions': {},
                          'id': widgetLayer.id,
                          'extension':{
                            'MapFile': ['platform:/'+resource.sResourceId],
                            'MapMetadata': [widgetLayer.mapMetadataKeys],
                            'LayerMetadata': [widgetLayer.layerMetadataKeys]
                          }
                         }]};
                  //set up needed accessor
                  data.getInitialView = function() {
                      return this.initialView;
                  };
                  if (this.loadOverlaysOnly) {
                    data.maps[0].layerOptions.isBaseLayer = false;
                  }
                  var menuItem = new Jx.Menu.Item({
                      label: label,
                      onClick: OpenLayers.Function.bind(this.switchMap, this, data)
                  });
                  
                  if (path == '') {
                      this.uiObj.add(menuItem);
                  } else {
                      this.menus[path].add(menuItem);
                  }
              }
            }
        }
    },
    
    processMapMenu: function(r) {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            this.menus = {};
            for (var i=0; i<o.maps.length; i++) {
                var map = o.maps[i];
                var path = map.path.replace(this.rootFolder, '');
                if (path.lastIndexOf('/') > -1) {
                    path = path.slice(0, path.lastIndexOf('/'));
                    this.createFolders(path);
                } else {
                    path = '';
                }
                
                // check for mapgroup data and if there is none,
                // create a maptag that will be passed to the map
                // widget constructor 
                var data = null;
                if (this.mapGroupData[map.path]) {
                    data = this.mapGroupData[map.path];
                } else {
                    data = {maps:[{'resourceId':map.path,
                            'singleTile':true,
                            'type': this.arch,
                            'extension':{'ResourceId': [map.path]}
                           }]};
                    //set up needed accessor
                    data.getInitialView = function() {
                        return this.initialView;
                    };
                }
                var menuItem = new Jx.Menu.Item({
                    label: map.name,
                    onClick: OpenLayers.Function.bind(this.switchMap, this, data)
                });
                
                if (path == '') {
                    this.uiObj.add(menuItem);
                }else {
                    this.menus[path].add(menuItem);
                }
            }
        }
    },
    
    createFolders: function(id) {
        var folders = id.split('/');
        //loop through folders, creating them if they don't exist
        var parent = '';
        var pathSeparator = '';
        for (var i=0; i<folders.length; i++) {
            if (!this.menus[parent + pathSeparator + folders[i]]){
                var menu = new Jx.Menu.SubMenu({label:folders[i]});
                if (parent == '') {
                    this.uiObj.add(menu);
                } else {
                    this.menus[parent].add(menu);
                }
                this.menus[parent + pathSeparator + folders[i]] = menu;
            }
            parent = parent + pathSeparator + folders[i];
            pathSeparator = '/';
        }
    },
    
    //action to perform when the button is clicked
    //activateTool: function() {
    //    this.oMenu.show();
    //},
        
    //change the map, preserving current extents
    switchMap: function(data) {
        data.initialView = null;
        if (!this.loadMaxExtent) {
          var ce = this.getMap().getCurrentExtents();
          var dest = null;
          for (var i=0; i<data.maps.length; ++i) {
            if (data.maps[i].layerOptions && data.maps[i].layerOptions.projection) {
              //This may not be an "EPSG:XXXX" string. It could already be an OpenLayers.Projection
              //so re-use it if that's the case
              var oProj = data.maps[i].layerOptions.projection;
              var projCode = "";
              if (oProj instanceof OpenLayers.Projection)
                dest = oProj;
              else
                dest = new OpenLayers.Projection(oProj);
                
              if (data.maps[i].layerOptions.isBaseLayer) {
                break;
              }
            }
          }
          if (dest) {
            ce = ce.transform(this.oMap.oMapOL.getProjectionObject(), dest);
          } else {
            dest = this.oMap.oMapOL.getProjectionObject();
          }
          data.initialView = {
            minX: ce.left,
            minY: ce.bottom,
            maxX: ce.right,
            maxY: ce.top,
            projection: dest
          };        
        }
        this.getMap().loadMapGroup(data, this.loadOverlaysOnly);
    }
});

		
	},
	
	
	//define Message window for Fusion
	redefineMessages: function(){
		/*****************************************************************************
		* Class: Fusion.MapMessage
		*
		* It is to show a floating message bar over the main map viewer
		*/
		Fusion.MapMessage = OpenLayers.Class({
		parentNode : null,
		domObj : null,
		leadingIcon : null,
		textCell : null,
		message : "",
    
		infoIconName : "/wgs4/images/icons/info.png",
		warningIconName : "/wgs4/images/icons/warning.png",
		errorIconName : "/wgs4/images/icons/error.png",
    
		containerCssText : "position:absolute; \
		    z-index:1000; left:10px; top:10px; right:10px; \
		    padding:10px; border:solid 2px #ECECEC; background:#FFFFBB;\		    ",
		iconCssText : "margin-right:10px; margin-top:3px",
		textCellCssText : "vertical-align:top; font-size: 17px",

		opacity: 0.95,
		
		initialize : function(parentNode)
		{
			this.parentNode = $(parentNode);
		},
    
		createBar : function()
		{
			var bb = query('.bottom-button-bar')[0]
			//console.log('ShowTools', bb, domClass.contains(bb, 'hidden'))
			if(has('phone'))
				domClass['add'] (bb, 'hidden')
					
			if (!this.container)
			{
				// Create the div container
				var container   = document.createElement("div");
				container.style.visibility = "hidden";
				this.container  = $(container);
				this.parentNode.appendChild(container);
			}
			var self = this
			this.container.onclick = function(){
				self.removeBar()
			}
			this.container.style.cssText = this.containerCssText;
			var offset = {left:65, top:8};
			//this.container.style.left = offset.left + "px";
			//this.container.style.top  = offset.top  + "px";
        
			this.container.innerHTML = "";
        
			// Create the inner table
			var table = document.createElement("table");
			this.container.appendChild(table);
			//table.style.width = "100%";
			table.cellSpacing = "0";
			table.cellPadding = "0";
			table.border      = "0";
			// Create the table row
			var row   = table.insertRow(0);
			// The icon cell
			var cell  = row.insertCell(0);
			// Add the info icon by default
			var icon  = document.createElement("img");
			icon.src  = this.infoIconName;
			cell.appendChild(icon);
			icon.style.cssText = this.iconCssText;
			this.leadingIcon   = icon;
			// Create the text cell
			cell      = row.insertCell(1);
			cell.style.cssText = this.textCellCssText;
			this.textCell = $(cell);
			this.textCell.innerHTML = this.message;
			
			// this.refreshLayout();
			// Hide message bar by default
			this.container.setOpacity(0);
			this.container.style.visibility = "visible";
		},
    
    removeBar : function()
    {
		if (typeof (this.container) != "undefined" && this.container != null)
        {
            this.container.fade(0);
            window.setTimeout((function()
            {
                if (typeof (this.container) != "undefined" && this.container != null)
                {
                    this.container.parentNode.removeChild(this.container);
                    this.container = null;
                }
                
            }).bind(this), 500);
        }
    },
    
    /**
     * Function: info
     * 
     * Displays an informational message on the Map Message notification bar
     * 
     * Parameters:
     * 
     *   message - The message to display
     */
    info : function(message)
    {
        this.message = message;
        this.show();
        this.leadingIcon.src = this.infoIconName;
    },
    
    /**
     * Function: warn
     * 
     * Displays a warning message on the Map Message notification bar
     * 
     * Parameters:
     * 
     *   message - The message to display
     */
    warn : function(message)
    {
        this.message = message;
        this.show();
        this.leadingIcon.src = this.warningIconName;
    },
    
    /**
     * Function: error
     * 
     * Displays a error message on the Map Message notification bar
     * 
     * Parameters:
     * 
     *   message - The message to display
     */
    error : function(message)
    {
        this.message = message;
        this.show();
        this.leadingIcon.src = this.errorIconName;
    },
    
    /**
     * Function: clear
     * 
     * Hides the notification bar and clears all content
     */
    clear : function()
    {
        this.message = "";
        this.textCell.innerHTML = "";
        this.hide();
    },
    
    /**
     * Function: show
     * 
     * Displays the notification bar
     */
    show : function()
    {
        this.createBar();
        this.textCell.innerHTML = this.message;
        this.container.fade(this.opacity);
    },
    
				/**
				* Function: hide
				* 
				* Hides the notification bar
				*/
				hide : function()
				{
					this.removeBar();
				},
    
				hideDesignatedMessage: function(message)
				{
					if(message == this.message)
					{
						this.removeBar();
					}
				},
    
				refreshLayout: function()
				{
					if (typeof (this.container) != "undefined" && this.container != null)
					{
						// 44 = 2 * padding (10) + 2 * offset(10) + 2 * border (2)
						var newWidth = this.parentNode.offsetWidth - 44;
						//if (newWidth >= 0)
							//this.container.style.width  = this.parentNode.offsetWidth - 44 + "px";
					}
				},
    
				CLASS_NAME: "Fusion.MapMessage"
				});
	}, 
			
	//Redefine Fusion Legend checkboxes to add touch support
	redefineLegend : function(){
				Fusion.Widget.Legend.TreeItem = new Class({
					Extends: Jx.TreeItem,
					options: {
						template: '<li class="jxTreeContainer jxTreeLeaf"><img class="jxTreeImage" src="'+Jx.aPixel.src+'" alt="" title=""><span class="fusionLegendCheckboxContainer"><input type="checkbox" class="fusionLegendCheckboxInput"></span><a class="fusionLayerInfo" target="_blank"><img class="fusionLayerInfoIcon" src="'+Jx.aPixel.src+'"></a><a class="jxTreeItem fusionCheckboxItem" href="javascript:void(0);"><img class="jxTreeIcon" src="'+Jx.aPixel.src+'" alt="" title=""><span class="jxTreeLabel" alt="" title=""></span></a></li>'
					},
					classes: new Hash({
						domObj: 'jxTreeContainer', 
						domA: 'jxTreeItem', 
						domImg: 'jxTreeImage', 
						domIcon: 'jxTreeIcon',
						domLabel: 'jxTreeLabel',
						checkbox: 'fusionLegendCheckboxInput',
						layerInfo: 'fusionLayerInfo',
						layerInfoIcon: 'fusionLayerInfoIcon'
					}),
					init: function() {
						//new bound.onClick. e.target.checked replaced with e.checked
						this.bound.onClick = function(e){
							if (this.options.data) {
								if ( e.checked && this.options.data.show) {
									this.options.data.show();
								} else if ( !e.checked && this.options.data.hide) {
									this.options.data.hide();
								}
							}
						}.bind(this);
						this.bound.enabled = function() {
							this.checkbox.disabled = false;
						}.bind(this);
						this.bound.disabled = function() {
							this.checkbox.disabled = true;
						}.bind(this);
						this.parent();
					},
					render: function() {
						this.parent();
						this.domLabel.set('alt', this.options.label);
						this.domLabel.set('title', this.options.label);
						if (this.checkbox) {
							if ($defined(this.options.checked)) {
								this.check(this.options.checked);
							}
							
							//new data-on-change method. should be executed externally
							var self = this
							this.checkbox['data-on-change'] = function(){self.bound.onClick(this)}
							
							//comment thisto prevent executing onclick
							//this.checkbox.addEvent('click', this.bound.onClick);
							
							this.addEvents({
								enabled: this.bound.enabled,
								disabled: this.bound.disabled
							});
						}
					},
					cleanup: function() {
						this.removeEvents({
							enabled: this.bound.enabled,
							disabled: this.bound.disabled
						});
						if (this.checkbox) {
							this.checkbox.removeEvent('click', this.bound.onClick);
						}
						this.bound.onClick = null;
						this.bound.enabled = null;
						this.bound.disabled = null;
						this.parent();
					},
					check: function(state) {
						if (this.checkbox) {
							this.checkbox.set('checked', state);
						}
					},
					isChecked: function() {
						return this.checkbox && this.checkbox.checked;
					},
					setLayerInfo: function(url, icon) {
						//change class to make fusionLayerInfo display block
						this.domObj.addClass('fusionShowLayerInfo');
						if (this.layerInfo) {
							this.layerInfo.set('href', url);
						}
						if (this.layerInfoIcon) {
							this.layerInfoIcon.set('src', icon);
						}
					}
				});
			},

	//Redefine Fusion Legend Folder checkboxes to add touch support
	redefineLegendRoot: function(){
				Fusion.Widget.Legend.TreeFolder = new Class({
					Extends: Jx.TreeFolder,
					options: {
						template: '<li class="jxTreeContainer jxTreeBranch"><img class="jxTreeImage" src="'+Jx.aPixel.src+'" alt="" title=""><span class="fusionLegendCheckboxContainer"><input type="checkbox" class="fusionLegendCheckboxInput"></span><a class="jxTreeItem fusionCheckboxItem" href="javascript:void(0);"><img class="jxTreeIcon" src="'+Jx.aPixel.src+'" alt="" title=""><span class="jxTreeLabel" alt="" title=""></span></a><a class="fusionGroupInfo"><img class="fusionGroupInfoIcon" src="'+Jx.aPixel.src+'"></a><a class="fusionLayerInfo"><img class="fusionLayerInfoIcon" src="'+Jx.aPixel.src+'"></a><ul class="jxTree"></ul></li>'
					},
					classes: new Hash({
						domObj: 'jxTreeContainer', 
						domA: 'jxTreeItem', 
						domImg: 'jxTreeImage', 
						domIcon: 'jxTreeIcon',
						domLabel: 'jxTreeLabel',
						domTree: 'jxTree',
						checkbox: 'fusionLegendCheckboxInput',
						layerInfo: 'fusionLayerInfo',
						layerInfoIcon: 'fusionLayerInfoIcon',
						groupInfo: 'fusionGroupInfo',
						groupInfoIcon: 'fusionGroupInfoIcon'
					}),
					init: function() {
						this.bound.onClick = function(e){
							if (this.options.data) {
								if (e.checked && this.options.data.show) {
									this.options.data.show();
								} else if (!e.checked && this.options.data.hide) {
									this.options.data.hide();
								}
							}
						}.bind(this);
						this.bound.enabled = function() {
							this.checkbox.disabled = false;
						}.bind(this);
						this.bound.disabled = function() {
							this.checkbox.disabled = true;
						}.bind(this);
						this.parent();
					},
    
					render: function() {
						this.parent();
						this.domLabel.set('alt', this.options.label);
						this.domLabel.set('title', this.options.label);
						if (this.checkbox) {
							if ($defined(this.options.checked)) {
								this.check(this.options.checked);
							}
							
							//new data-on-change method. should be executed externally
							var self = this
							this.checkbox['data-on-change'] = function(){self.bound.onClick(this)}
							//comment this to prevent executing onclick
							//this.checkbox.addEvent('click', this.bound.onClick);
							
							this.addEvents({
								enabled: this.bound.enabled,
								disabled: this.bound.disabled
							});
						}
					},
					cleanup: function() {
						this.removeEvents({
							enabled: this.bound.enabled,
							disabled: this.bound.disabled
						});
						if (this.checkbox) {
							this.checkbox.removeEvent('click', this.bound.onClick);
						}
						this.bound.onClick = null;
						this.bound.enabled = null;
						this.bound.disabled = null;
						this.parent();
					},
					check: function(state) {
						if (this.checkbox) {
							this.checkbox.set('checked', state);
						}
					},
					isChecked: function() {
						return this.checkbox && this.checkbox.checked;
					},
					setLayerInfo: function(url, icon) {
						//change class to make fusionLayerInfo display block
						this.domObj.addClass('fusionShowLayerInfo');
						if (this.layerInfo) {
							this.layerInfo.set('href', url);
						}
						if (this.layerInfoIcon) {
							this.layerInfoIcon.set('src', icon);
						}
					},
    
					setGroupInfo: function(url, icon) {
						//change class to make fusionGroupInfo display block
						this.domObj.addClass('fusionShowGroupInfo');
						if (this.groupInfo) {
							this.groupInfo.set('href', url);
						}
						if (this.groupInfoIcon) {
							this.groupInfoIcon.set('src', icon);
						}
					}
				});
			},
	
	//Redefine Fusion Legend widget itself to add events for legend refreshing
	redefineLegendWidget:function(){
		/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

 /********************************************************************
 * Class: Fusion.Widget.Legend
 *
 * A widget to display a legend of all layers.
 * 
 * Inherits from:
 *  - <Fusion.Widget>
 * **********************************************************************/
		



/* Class: Fusion.Widget.Legend.LegendRendererDefault
 * This class provide a default legend as a collapsable tree.
 *
 */

Fusion.Widget.Legend.LegendRendererDefault = OpenLayers.Class(Fusion.Widget.Legend.LegendRenderer,
{
    /**
     * Property: showRootFolder
     * {Boolean} This controls whether the tree will have a single root node that
     * contains the name of the map as its label.  By default, the root node does
     * not appear.  Set to "true" or "1" to make the root node appear.
     */
    showRootFolder: false,

    /**
     * Property: currentNode
     * {Jx.TreeNode} The current selected node.
     */
    currentNode: null,
    
    /**
     * Property: bIsDrawn
     * {Boolean} Determine if the map is drawn.
     */
    bIsDrawn: false,

    /**
     * Property: updateDelay
     * {Integer} number of milliseconds to delay the update of legend
     */
    updateDelay: 500,

    /**
     * Property: targetFolder
     * {Jx.TreeFolder} The current TreeFolder that the mouse will interact with.
     */
    targetFolder: null,

    /**
     * Property: bIncludeVisToggle
     * {Boolean} Determine if non-visible layer must be draw in the legend.
     */
    bIncludeVisToggle: true,
    offsetsCalculated: false,
   
    initialize: function(legend, widgetTag) {   
        Fusion.Widget.Legend.LegendRenderer.prototype.initialize.apply(this, [legend]);
		
        var json = widgetTag.extension;
        this.imgLayerDWFIcon = json.LayerDWFIcon ? json.LayerDWFIcon[0] : this.oLegend.defaultLayerDWFIcon;
        this.imgLayerRasterIcon = json.LayerRasterIcon ? json.LayerRasterIcon[0] : this.oLegend.defaultLayerRasterIcon;
        this.imgLayerThemeIcon = json.LayerThemeIcon ? json.LayerThemeIcon[0] : this.oLegend.defaultLayerThemeIcon;
        this.imgDisabledLayerIcon = json.DisabledLayerIcon ? json.DisabledLayerIcon[0] : this.oLegend.defaultDisabledLayerIcon;
        this.imgLayerInfoIcon = json.LayerInfoIcon ? json.LayerInfoIcon[0] : this.oLegend.defaultLayerInfoIcon;
        this.imgGroupInfoIcon = json.GroupInfoIcon ? json.GroupInfoIcon[0] : this.oLegend.defaultGroupInfoIcon;

        //not used?
        //this.layerInfoURL = json.LayerInfoURL ? json.LayerInfoURL[0] : '';
        this.selectedLayer = null;
        
        this.selection = new Jx.Selection({
            onSelect: function(item) {
                var treeItem = item.retrieve('jxTreeItem');
                var data = treeItem.options.data;
                if (data instanceof Fusion.Layers.Group) {
                    this.getMap().setActiveLayer(null);
                } else {
                    this.getMap().setActiveLayer(data);
                }
            }.bind(this)
        });
       
        this.oTree = new Jx.Tree({
            template: '<ul class="jxTreeRoot fusionLegendTreeRoot"></ul>',
            selection:this.selection
        }).addTo(this.oLegend.domObj);
       
        this.hideInvisibleLayers = (json.HideInvisibleLayers && json.HideInvisibleLayers[0]) == 'true' ? true : false;
        //don't show the root folder by default
        this.showRootFolder = (json.ShowRootFolder && json.ShowRootFolder[0] == 'true') ? true:false;
        //do show the map folder by default
        this.showMapFolder = (json.ShowMapFolder && json.ShowMapFolder[0] == 'false') ? false:true;
        
        if (!this.showRootFolder) {
            //console.log('supressing root folder');
            this.oRoot = this.oTree;
        } else {
           // console.log('showing root folder');
            var opt = {
                label: OpenLayers.i18n('defaultMapTitle'),
                // contextMenu: this.getContextMenu(),
                open: true
            };
            this.oRoot = new Jx.TreeFolder(opt);
            this.oTree.add(this.oRoot);
            // this.oRoot.options.contextMenu.add([
            //     new Jx.Menu.Item({
            //         label: OpenLayers.i18n('collapse'),
            //         onClick: OpenLayers.Function.bind(this.collapseBranch, this, this.oRoot)
            //     }),
            //     new Jx.Menu.Item({
            //         label: OpenLayers.i18n('expand'),
            //         onClick: OpenLayers.Function.bind(this.expandBranch, this, this.oRoot)
            //     })]
            // );
        }
        
        this.extentsChangedWatcher = this.update.bind(this);
    },
    
    getContextMenu: function() {
        return new Jx.Menu.Context(this.name).add([
            new Jx.Menu.Item({
                label: OpenLayers.i18n('refresh'),
                onClick: OpenLayers.Function.bind(this.update, this)
            }),
            new Jx.Menu.Item({
                label: OpenLayers.i18n('collapseAll'),
                onClick: OpenLayers.Function.bind(this.collapseAll, this)
            }),
            new Jx.Menu.Item({
                label: OpenLayers.i18n('expandAll'),
                onClick: OpenLayers.Function.bind(this.expandAll, this)
            })]
        );
    },
    
    expandAll: function(folder) {
        this.oTree.items().each(function(item){
            if (item instanceof Jx.TreeFolder) {
                this.recurseTree('expand', item);
            }
        },this);
        if (this.showRootFolder) {
          this.oRoot.expand();
        }
    },
    
    collapseAll: function(folder) {
        this.oTree.items().each(function(item){
            if (item instanceof Jx.TreeFolder) {
                this.recurseTree('collapse', item);
            }
        },this);
        if (this.showRootFolder) {
          this.oRoot.collapse();
        }
    },
    
    collapseBranch: function(folder) {
        folder.collapse();
    },
    
    expandBranch: function(folder) {
        folder.expand();
    },
    
  /**
     * recursively descend the tree applying the request operation which is either 'collapse' or 'expand'
     *
     * @param op the operation to execute
     * @param the folder to operate on
     */
    recurseTree: function(op, folder) {
        folder.items().each(function(item){
            if (item instanceof Jx.TreeFolder) {
                this.recurseTree(op, item);
                item[op]();
            }
        },this);
    },
   
    scaleRangesLoaded: function() {
        this.layerRoot = this.getMap().layerRoot;
        this.renderLegend();
    },
    
    mapLoading: function() {
        this.getMap().deregisterForEvent(Fusion.Event.MAP_EXTENTS_CHANGED, this.extentsChangedWatcher);
        this.clear();
    },
   
    mapLoaded: function() {
        this.getMap().registerForEvent(Fusion.Event.MAP_EXTENTS_CHANGED, this.extentsChangedWatcher);
        var baseLayer = this.oLegend.getMapLayer(); 
        baseLayer.registerForEvent(Fusion.Event.MAP_LAYER_ORDER_CHANGED, OpenLayers.Function.bind(this.mapRefresh, this));
        this.layerRoot = this.getMap().layerRoot;
        //this.renderLegend();
    },
    
    mapReloaded: function() {
        this.layerRoot = this.getMap().layerRoot;
        this.renderLegend();
    },
    
    mapRefresh: function() {
        var baseLayer = this.oLegend.getMapLayer();
        baseLayer.parseLayers();
        this.layerRoot = this.getMap().layerRoot;
        this.renderLegend();
         
    },
    
    /**
     * Callback for legend XML response. Creates a list of layers and sets up event
     * handling. Create groups if applicable.
     * TODO: error handling
     *
     * @param r Object the reponse xhr object
     */
    renderLegend: function(r) {
        this.bIsDrawn = false;
        this.clear();
        if (this.showRootFolder) {
            this.oRoot.setLabel(this.getMap().mapGroup.mapId);
        }
        
        var startGroup = this.layerRoot;
        if (!this.showMapFolder)
            startGroup = this.layerRoot.groups[0];
        
        if (!startGroup.legend) {
            startGroup.legend = {};
            startGroup.legend.treeItem = this.oRoot;
        }
        
        if (this.layerRoot.groups.length > 1) { //Multi-map
            if (this.showMapFolder) {
                for (var i = 0; i < this.layerRoot.groups.length; i++) {
                    this.processMapGroup(this.layerRoot.groups[i], this.oRoot);
                }
            } else {
                for (var i = 0; i < this.layerRoot.groups.length; i++) {
                    var mapRoot = this.layerRoot.groups[i];
                    if (!mapRoot.legend) {
                        mapRoot.legend = {};
                        mapRoot.legend.treeItem = this.oRoot;
                    }
                    for (var j = 0; j < mapRoot.groups.length; j++) {
                        this.processMapGroup(mapRoot.groups[j], this.oRoot);
                    }
                    for (var j = 0; j < mapRoot.layers.length; j++) {
                        this.processMapLayer(mapRoot.layers[j], this.oRoot);
                    }
                }
            }
        } else { //Single-map
            if (!startGroup.legend) {
                startGroup.legend = {};
                startGroup.legend.treeItem = this.oRoot;
            }
            for (var i = 0; i < startGroup.groups.length; i++) {
                this.processMapGroup(startGroup.groups[i], this.oRoot);
            }
            for (var i = 0; i < startGroup.layers.length; i++) {
                this.processMapLayer(startGroup.layers[i], this.oRoot);
            }
        }
        
        this.bIsDrawn = true;
        this.update();
       
    },

    processMapGroup: function(group, folder) {
        if (group.displayInLegend) {
            /* make a 'namespace' on the group object to store legend-related info */
            group.legend = {};
            var opt = {
                label: group.legendLabel,
                open: group.expandInLegend,
                // contextMenu: this.getContextMenu(),
                checked: group.visible
            };
            var treeItem = new Fusion.Widget.Legend.TreeFolder(opt);
            treeItem.options.data = group;
            group.legend.treeItem = treeItem;
            // treeItem.options.contextMenu.add(
            //     new Jx.Menu.Item({
            //         label: OpenLayers.i18n('collapse'),
            //         onClick: OpenLayers.Function.bind(this.collapseBranch, this, treeItem)
            //     }),
            //     new Jx.Menu.Item({
            //         label: OpenLayers.i18n('expand'),
            //         onClick: OpenLayers.Function.bind(this.expandBranch, this, treeItem)
            //     })
            // );

            folder.add(treeItem);
            var groupInfo = group.oMap.getGroupInfoUrl(group.groupName);
            if (groupInfo) {
                treeItem.setGroupInfo(groupInfo, this.imgGroupInfoIcon);
            }
            for (var i=0; i<group.groups.length; i++) {
                this.processMapGroup(group.groups[i], treeItem);
            }
            for (var i=0; i<group.layers.length; i++) {
                this.processMapLayer(group.layers[i], treeItem);
            }
        }
    },
   
    processMapLayer: function(layer, folder) {
        /* make a 'namespace' on the layer object to store legend-related info */
        layer.legend = {};
        layer.legend.parentItem = folder;
        layer.legend.currentRange = null;
        layer.oMap.registerForEvent(Fusion.Event.LAYER_PROPERTY_CHANGED, OpenLayers.Function.bind(this.layerPropertyChanged, this));
    },
   
    layerPropertyChanged: function(eventID, layer) {
        layer.legend.treeItem.check(layer.isVisible());
    },

    updateTimer: null,
    update: function() {
        if (this.bIsDrawn) {
          if (this.updateTimer) {
            window.clearTimeout(this.updateTimer);
            this.updateTimer = null;
          }
          this.updateTimer = window.setTimeout(OpenLayers.Function.bind(this._update, this), this.updateDelay);
        }
    },
   
    /**
     * update the tree when the map scale changes
     */
    _update: function() {
        this.oTree.freeze();
        this.updateTimer = null;
        var map = this.getMap();
        var currentScale = map.getScale();
        for (var i=0; i<map.layerRoot.groups.length; i++) {
            this.updateGroupLayers(map.layerRoot.groups[i], currentScale);
        }
        //Loop reversed. See addLayerTreeItem() for why we do this
        for (var i=map.layerRoot.layers.length-1; i>=0; i--) {
            this.updateLayer(map.layerRoot.layers[i], currentScale);
        }
        this.oTree.thaw();
        topic.publish('legendRender')
    },
   
    /**
     * remove the dom objects representing the legend layers and groups
     */
    clear: function() {
        //console.log('clear legend');
        var map = this.getMap();
        for (var i=0; i<map.layerRoot.groups.length; i++) {
            this.clearGroup(map.layerRoot.groups[i]);
        }
        for (var i=0; i<map.layerRoot.layers.length; i++) {
          if (map.layerRoot.layers[i].legend) {
            map.layerRoot.layers[i].legend.treeItem = null;
            map.layerRoot.layers[i].legend.checkbox = null;
            map.layerRoot.layers[i].legend.currentRange = null;
          }
        }
        this.oRoot.empty();
    },
    
    clearGroup: function(group) {
      for (var i=0; i<group.groups.length; i++) {
        this.clearGroup(group.groups[i]);
      }
      for (var i=0; i<group.layers.length; i++) {
        if (group.layers[i].legend) {
          group.layers[i].legend.treeItem = null;
          group.layers[i].legend.currentRange = null;
        }
      }
    },
    addLayerStyleTreeItems: function(treeItem, items) {
        treeItem.tree.freeze();
        treeItem.add(items);
        treeItem.tree.thaw();
    },
    addLayerTreeItem: function(treeItem, layerTreeItem) {
        //Here's the problem: The layers are being iterated in the correct draw order, but they are
        //being appended *after* groups at any particular level due to the out-of-order tree node rendering
        //setup between group and layer nodes (groups are drawn first before layers). 
        //
        //To do this in a way that preserves draw order, but makes sure thes layer nodes are always 
        //before group nodes at any particular level. We have to do 2 things:
        //
        // 1. Prepend this item instead of appending
        // 2. Ensure the outermost loop that calls updateLayer() (that calls this method), is iterating
        //    in *reverse* order.
        //
        //This allows for layers to be added in correct draw order *before* the group nodes at any
        //particular level.
        //
        //updateLayer() is the only method that calls this method, so the above contract is satisified.
        //If this is no longer the case, please re-read what I just wrote to make sure the two points
        //above are still valid.
        treeItem.add(layerTreeItem, 0);
    },
    updateGroupLayers: function(group, fScale) {
        for (var i=0; i<group.groups.length; i++) {
            this.updateGroupLayers(group.groups[i], fScale);
        }
        //Loop reversed. See addLayerTreeItem() for why we do this
        for (var i=group.layers.length-1 ; i >= 0; i--) {
            this.updateLayer(group.layers[i], fScale);
        }
    },
    updateLayer: function(layer, fScale) {
        /* no need to do anything if we are hiding the layer */
        if (!layer.displayInLegend || !layer.legend) {
            return;
        }
        /* check the layer's current scale range against the previous one
         * if the range hasn't changed, don't do anything
         */
        var range = layer.getScaleRange(fScale);
        if (range == layer.legend.currentRange && layer.legend.treeItem) {
            return;
        }
        
        /* remember the range we are now representing for the next update */
        layer.legend.currentRange = range;
        
        /* if layer is in range */
        if (range != null && range.styles) {
            /* if it has more than one style, we represent it as a folder
             * with classes as items in it
             */
            if (range.styles.length > 1) {
                //tree item needs to be a folder
                if (!layer.legend.treeItem) {
                    layer.legend.treeItem = this.createFolderItem(layer);
                    this.addLayerTreeItem(layer.parentGroup.legend.treeItem, layer.legend.treeItem);
                } else if (layer.legend.treeItem instanceof Fusion.Widget.Legend.TreeItem) {
                    this.clearTreeItem(layer);
                    layer.legend.treeItem = this.createFolderItem(layer);
                    this.addLayerTreeItem(layer.parentGroup.legend.treeItem, layer.legend.treeItem);
                } else {
                    layer.legend.treeItem.empty();
                }
                //This style range has the compression flag set. This would have been set server-side
                //if it contains more than a pre-defined number of style rules (see LoadScaleRanges.php for
                //more information)
                if (range.isCompressed) {
                    //Attach required data for theme expansion later on
                    layer.legend.treeItem.layer = layer;
                    layer.legend.treeItem.range = range;
                    layer.legend.treeItem.scale = fScale;
                    layer.legend.treeItem.hasDecompressedTheme = false;
                    //console.assert(range.styles.length > 2);
                    var children = [];
                    children.push(this.createTreeItem(layer, range.styles[0], fScale, false));
                    children.push(this.createThemeCompressionItem(range.styles.length - 2, layer.legend.treeItem));
                    children.push(this.createTreeItem(layer, range.styles[range.styles.length-1], fScale, false));
                    this.addLayerStyleTreeItems(layer.legend.treeItem, children);
                } else {
                    var children = [];
                    for (var i=0; i<range.styles.length; i++) {
                        children.push(this.createTreeItem(layer, range.styles[i], fScale, false));
                    }
                    this.addLayerStyleTreeItems(layer.legend.treeItem, children);
                }
            /* if there is only one style or no style, we represent it as a tree item */
            } else {
                var style = range.styles[0];
                if (style && !style.legendLabel) {
                    style.legendLabel = layer.legendLabel;
                }
                if (!layer.legend.treeItem) {
                    layer.legend.treeItem = this.createTreeItem(layer, style, fScale, !layer.isBaseMapLayer);
                    this.addLayerTreeItem(layer.parentGroup.legend.treeItem, layer.legend.treeItem);
                } else if (layer.legend.treeItem instanceof Fusion.Widget.Legend.TreeFolder) {
                    this.clearTreeItem(layer);
                    layer.legend.treeItem = this.createTreeItem(layer, style, fScale, !layer.isBaseMapLayer);
                    this.addLayerTreeItem(layer.parentGroup.legend.treeItem, layer.legend.treeItem);
                } else {
                    if (range.styles.length > 0) {
                        var url;
                        if(style.iconOpt && style.iconOpt.url){
                            url = style.iconOpt.url;
                            var img = layer.legend.treeItem.elements.get('jxTreeIcon');
                            var iconX = -1 * style.iconX;
                            var iconY = -1 * style.iconY;
                            img.style.backgroundPosition = iconX + 'px ' + iconY + 'px';
                        }else{
                            url = layer.oMap.getLegendImageURL(fScale, layer, style);
                        }
                        layer.legend.treeItem.setImage(url);
                        layer.legend.treeItem.enable(true);
                    } else {
                        layer.legend.treeItem.enable(false);
                    }
                }
            }
        } else {
            /* the layer is to be displayed but is not visible in the map
             * at the current map scale so disable it and display as a tree
             * item or hide it altogether if necessary;
             */
            if (this.hideInvisibleLayers) {
                if (layer.legend.treeItem) {
                    layer.parentGroup.legend.treeItem.remove(layer.legend.treeItem);
                    layer.legend.treeItem = null;
                }
            } else {
                var newTreeItem = this.createTreeItem(layer, {legendLabel: layer.legendLabel, iconOpt: { url: this.oLegend.outOfRangeIcon } }, null, !layer.isBaseMapLayer);
                if (layer.legend.treeItem) {
                    layer.parentGroup.legend.treeItem.replace(layer.legend.treeItem, newTreeItem);
                    layer.legend.treeItem.finalize();
                } else {
                    this.addLayerTreeItem(layer.parentGroup.legend.treeItem, newTreeItem);
                }
                layer.legend.treeItem = newTreeItem;
            }
        }
        if (layer.legend.treeItem) {
            layer.legend.treeItem.options.data = layer;
            if (!layer.isBaseMapLayer) //Tiled layers don't have a checkbox so there's nothing to check
                layer.legend.treeItem.check(layer.visible);
        }
    },
    getThemeExpandContextMenu: function(node) {
        return new Jx.Menu.Context(this.name).add([
            new Jx.Menu.Item({
                label: OpenLayers.i18n('expandTheme'),
                onClick: OpenLayers.Function.bind(function() { this.expandTheme(node); }, this)
            })]
        );
    },
    expandTheme: function(node) {
        if (node.hasDecompressedTheme !== true && confirm(OpenLayers.i18n('expandCompressedThemeConfirmation'))) {
            var range = node.range;
            var layer = node.layer;
            var fScale = node.scale;
            node.empty();
            //FIXME: JxLib really needs an API to add these in a single batch that doesn't hammer
            //the DOM (if it's even possible)
            for (var i = 0; i < range.styles.length; i++) {
                var item = this.createTreeItem(layer, range.styles[i], fScale, false);
                node.add(item);
            }
            node.hasDecompressedTheme = true;
        }
    },
    createThemeCompressionItem: function(number, node) {
        var opt = {
            label: OpenLayers.i18n('otherThemeItems', { count: number }),
            draw: this.renderItem,
            contextMenu: this.getThemeExpandContextMenu(node),
            image: this.imgBlankIcon
        };
        var item = new Jx.TreeItem(opt);
        return item;
    },
    createFolderItem: function(layer) {
        var opt = {
            label: layer.legendLabel == '' ? '&nbsp;' : layer.legendLabel,
            open: layer.expandInLegend,
            // contextMenu: this.getContextMenu(),
            image: this.imgLayerThemeIcon
        };
        if (layer.metadata) {
          opt.selectable = !layer.metadata.jxUnselectable || (layer.metadata.jxUnselectable && layer.metadata.jxUnselectable != 'true');
        } else {
          opt.selectable = false;
        }
        var folder;
        if (!layer.isBaseMapLayer) {
            folder = new Fusion.Widget.Legend.TreeFolder(opt);
            /* only need to add layer info if it has a check box too */
            var layerInfo = layer.oMap.getLayerInfoUrl(layer.layerName);
            if (layerInfo) {
                folder.setLayerInfo(layerInfo, this.imgLayerInfoIcon);
            }
        }  else {
            opt.selectable = false;
            folder = new Jx.TreeFolder(opt);
        }
        var img = folder.elements.get('jxTreeIcon');
        img.style.backgroundPosition = '0px 0px';
        // folder.options.contextMenu.add([
        //     new Jx.Menu.Item({
        //         label: OpenLayers.i18n('collapse'),
        //         onClick: OpenLayers.Function.bind(this.collapseBranch, this, folder)
        //     }),
        //     new Jx.Menu.Item({
        //         label: OpenLayers.i18n('expand'),
        //         onClick: OpenLayers.Function.bind(this.expandBranch, this, folder)
        //     })]
        // );
       
        return folder;
    },
    
    createTreeItem: function(layer, style, scale, checkbox) {
        var opt = {};
        opt.statusIsDefault = layer.statusDefault;
        
        //Set the label. Use style label IFF there are more than one style rule.
        //Otherwise layer's legend label takes precedence
        if (style && layer.legend.currentRange.styles.length > 1) {
            opt.label = style.legendLabel == '' ? '&nbsp;' : style.legendLabel;
        } else {
            opt.label = layer.legendLabel == '' ? '&nbsp;' : layer.legendLabel;
        }
        
        if (layer.metadata) {
          opt.selectable = !layer.metadata.jxUnselectable || (layer.metadata.jxUnselectable && layer.metadata.jxUnselectable != 'true');
        } else {
          opt.selectable = false;
        }

        var LAYER_RASTER = 4;
        var LAYER_DWF = 5;

        if (!style) {
            //This could be a DWF or Raster layer
            if (layer.layerTypes.length == 1) {
                if (layer.layerTypes[0] == LAYER_RASTER) {
                    opt.image = this.imgLayerRasterIcon;
                } else if (layer.layerTypes[0] == LAYER_DWF) {
                    opt.image = this.imgLayerDWFIcon;
                } else {
                    opt.image = this.imgDisabledLayerIcon;
                    opt.enabled = false;
                }
            } else {
                opt.image = this.imgDisabledLayerIcon;
                opt.enabled = false;
            }
        } else {
            if(style.iconOpt && style.iconOpt.url){
                opt.image = style.iconOpt.url;
            }else{
                opt.image = layer.oMap.getLegendImageURL(scale, layer, style);
            }
        }

        var item;
        if (!layer.isBaseMapLayer&&checkbox) {
            // opt.contextMenu = this.getContextMenu();
            item = new Fusion.Widget.Legend.TreeItem(opt);
            /* only need to add layer info if it has a check box too */
            var layerInfo = layer.oMap.getLayerInfoUrl(layer.layerName);
            if (layerInfo) {
                item.setLayerInfo(layerInfo, this.imgLayerInfoIcon);
            }
        }  else {
            opt.selectable = false;
            item = new Jx.TreeItem(opt);
        }
        
        var iconX = 0;
        var iconY = 0;
        var img = item.elements.get('jxTreeIcon');
        if (style && style.iconX >= 0 && style.iconY >= 0) {
            /* calculate the size of the image that holds the icon
             * only once and cache the values as it is an expensive operation
             * We use the size to center the class/layer icon as a background
             * image inside the image that holds it so that if the icon is
             * not the same size it is represented in a reasonable way
             */
            if (!this.offsetsCalculated) {
                var parent = img.parentNode;
                var sibling = img.getPrevious();
                var d = new Element('div', {'class':'fusionLegendTreeRoot'});
                img.setStyle('visiblity','hidden');
                img.inject(d);
                //TODO: img.getStyle doesn't seem to work for IE, need another solution here
                var w = 16;//img.getStyle('width').toInt();
                var h = 16;//img.getStyle('height').toInt();
                if (!sibling) {
                    img.inject(parent,'top');
                } else {
                    img.inject(sibling, 'after');
                }
                img.setStyle('visibility','visible');
                this.iconWidth = ((style.iconOpt?style.iconOpt.width:16) - w)/2;
                this.iconHeight = ((style.iconOpt?style.iconOpt.height:16) - h)/2;
                //alert(w+":"+h);
                this.offsetsCalculated = true;
            }
            iconX = -1 * (style.iconX + this.iconWidth);
            iconY = -1 * (style.iconY + this.iconHeight);
            img.style.backgroundPosition = iconX + 'px ' + iconY + 'px';
        }
        
        return item;
    },
    clearTreeItem: function(layer) {
        if (layer.legend.treeItem && layer.legend.treeItem.owner) {
            layer.legend.treeItem.owner.remove(layer.legend.treeItem);
            // layer.legend.treeItem.finalize();
            layer.legend.treeItem.destroy();
            layer.legend.treeItem = null;
        }
    }
});


},

	redefineLayers: function(){
		Fusion.Layers.MapGuide = OpenLayers.Class(Fusion.Layers, {
    arch: 'MapGuide',
    session: [null],
    aShowLayers: null,
    aHideLayers: null,
    aShowGroups: null,
    aHideGroups: null,
    aRefreshLayers: null,
    sActiveLayer: null,
    selectionType: 'INTERSECTS',
    bSelectionOn: false,
    oSelection: null,
    //Specifies how long to delay a map redraw (in ms) upon a layer/group toggle. 
    //This allows for rapid layer/group toggling without having a redraw made for each individual toggle
    //as long as each toggle is done within the time period defined here. If the delay is 0 or less, the
    //draw is immediate like before.
    drawDelay: 0,
    nCmsScaleTolerance: 2.0,  //When checking the scale list of a tiled map to determine if it is compatible with commercial layers, this value determines how much leeway a given scale can have to be considered equal
    bUsesCommercialLayerScaleList: false,
    //This is the CMS scale list as defined by MG Studio and interpreted by OpenLayers
    aCmsScales: [
        1128.49722, 2256.99444, 4513.98888, 9027.977761000002, 18055.95552,
        36111.91104, 72223.82208999999, 144447.6442, 288895.2884, 577790.5767000001, 
        1155581.153, 2311162.307, 4622324.614, 9244649.227, 18489298.45, 
        36978596.91, 73957193.82, 147914387.6, 295828775.3, 591657550.5
    ],
    bUseNativeServices: false,
    selectionAsOverlay: true,
    useAsyncOverlay: false,
    defaultFormat: 'PNG',
    oLayersOLTile: null,      //a list of baselayers
    oLayerOLDynamic: false,   //a layer object for tiled maps that also contains dynamic layers
    supports: {
      query: true,
      edit: true
    },
    alternateHostNames: null, //a comma-delimited list of alternate host names to use

    initialize: function(map, mapTag, isMapWidgetLayer) {
        // console.log('MapGuide.initialize');
        Fusion.Layers.prototype.initialize.apply(this, arguments);

        var newTheme = Fusion.getQueryParam('theme');
        if (newTheme != '') {
          this.sMapResourceId = newTheme;
          //clear the query param after it has been used once 
          Fusion.queryParams['theme'] = null;
        }

        this.registerEventID(Fusion.Event.MAP_SESSION_CREATED);

        this.mapInfo = mapTag.mapInfo;
        this.imageFormat = mapTag.extension.ImageFormat ? mapTag.extension.ImageFormat[0] : this.defaultFormat;
        this.selectionType = mapTag.extension.SelectionType ? mapTag.extension.SelectionType[0] : 'INTERSECTS';
        this.selectionColor = mapTag.extension.SelectionColor ? mapTag.extension.SelectionColor[0] : '';
        this.selectionFormat = mapTag.extension.SelectionFormat ? mapTag.extension.SelectionFormat[0] : 'PNG';
        if (mapTag.extension.SelectionAsOverlay && mapTag.extension.SelectionAsOverlay[0] == 'false') {
          this.selectionAsOverlay = false;
        }
        if (!this.bIsMapWidgetLayer) {
          this.selectionAsOverlay = false;
        }
        
        if (mapTag.extension.DrawDelay) {
          this.drawDelay = parseInt(mapTag.extension.DrawDelay[0]);
          //console.log("Draw delay set to: " + this.drawDelay + "ms");
        }

        //add in the handler for CTRL-click actions for the map, not an overviewmap
        if (this.bIsMapWidgetLayer) {
          var ctrlClickEnabled = true;
          if (mapTag.extension.DisableCtrlClick && mapTag.extension.DisableCtrlClick[0] == 'true') {
              ctrlClickEnabled = false;
          }
          if (ctrlClickEnabled) {
            this.map = this.mapWidget.oMapOL;
            this.handler = new OpenLayers.Handler.Click(this,
                {click: OpenLayers.Function.bind(this.mouseUpCRTLClick, this)},
                {keyMask: OpenLayers.Handler.MOD_CTRL});
            this.handler.activate();
            this.nTolerance = 2; //pixels, default pixel tolernace for a point click; TBD make this configurable
          }
        }
        
        //Store the list of alternate host names
        if (mapTag.layerOptions.AlternateHostNames) {
            this.alternateHostNames = mapTag.layerOptions.AlternateHostNames;
        }
        
        rootOpts = {
          displayInLegend: this.bDisplayInLegend,
          expandInLegend: this.bExpandInLegend,
          legendLabel: this._sMapname,
          uniqueId: 'layerRoot',
          groupName: 'layerRoot',
          visible: true,
          actuallyVisible: true
          //TODO: set other opts for group initialization as required
        };
        this.layerRoot = new Fusion.Layers.Group(rootOpts,this);

        this.keepAliveInterval = parseInt(mapTag.extension.KeepAliveInterval ? mapTag.extension.KeepAliveInterval[0] : 300);
        this.noCache = true;
        this.oLayersOLTile = [];
        
        if (Fusion.siteVersion) {
            this.siteVersion = Fusion.siteVersion;
            this.checkNativeServiceSupport();
        }
        var sid = Fusion.sessionId;
        if (sid) {
            this.session[0] = sid;
            this.mapSessionCreated();
        } else {
            this.createSession();
        }
    },

    createSession: function() {
        if (!this.session[0]) {
            this.session[0] = this;
            var sl = Fusion.getScriptLanguage();
            var scriptURL = 'layers/' + this.arch + '/' + sl + '/CreateSession.' + sl;
            var options = {onSuccess: OpenLayers.Function.bind(this.createSessionCB, this)};
            Fusion.ajaxRequest(scriptURL, options);
        }
        if (this.session[0] instanceof Fusion.Layers.MapGuide) {
            // console.log('register for event');
            this.session[0].registerForEvent(Fusion.Event.MAP_SESSION_CREATED,
                OpenLayers.Function.bind(this.mapSessionCreated, this));
        } else {
            this.mapSessionCreated();
        }
    },

    checkNativeServiceSupport: function() {
        //NOTE: Using native services may cause a slight (but not too much) delay in any requests to PHP scripts 
        //that use layer property mappings as they will be lazy loaded due to us not calling LoadMap.php, which 
        //would've pre-cached such information. but we get much better map init performance
        this.bUseNativeServices = false;
        var vMajor = this.siteVersion[0];
        var vMinor = this.siteVersion[1];
        if (vMajor > 2) { // 3.0 or higher
            this.bUseNativeServices = true;
        } else {
            if (vMajor == 2) { // 2.x
                if (vMinor >= 6) { // >= 2.6
                    this.bUseNativeServices = true;
                }
            }
        }
    },

    createSessionCB: function(xhr) {
        if (xhr.status == 200) {
            var o = Fusion.parseJSON(xhr.responseText);
            if (o.success === false) {
                Fusion.reportFatalError(o.message);
            } else {
                var version = o.siteVersion;
                var bits = version.split('.');
                this.siteVersion = new Array(parseInt(bits[0]),
                                             parseInt(bits[1]),
                                             parseInt(bits[2]),
                                             parseInt(bits[3])
                );
                this.checkNativeServiceSupport();
                this.session[0] = o.sessionId;
                
                if (!Fusion.siteVersion)
                    Fusion.siteVersion = this.siteVersion;
                if (!Fusion.sessionId)
                    Fusion.sessionId = o.sessionId;
                
                var acceptLang = o.acceptLanguage.split(',');
                //IE - en-ca,en-us;q=0.8,fr;q=0.5,fr-ca;q=0.3
                for (var i=0; i<acceptLang.length; ++i) {
                  var locale = acceptLang[i].split(";");
                  Fusion.initializeLocale(locale[0]);
                  break;
                }
                this.triggerEvent(Fusion.Event.MAP_SESSION_CREATED);
            }
        }
    },

    mapSessionCreated: function() {
        if (this.sMapResourceId != '') {
          var options = {};
          if (this.bIsMapWidgetLayer) {
            var showlayers = Fusion.getQueryParam('showlayers');
            Fusion.queryParams['showlayers'] = null;
            var hidelayers = Fusion.getQueryParam('hidelayers');
            Fusion.queryParams['hidelayers'] = null;
            var showgroups = Fusion.getQueryParam('showgroups');
            Fusion.queryParams['showgroups'] = null;
            var hidegroups = Fusion.getQueryParam('hidegroups');
            Fusion.queryParams['hidegroups'] = null;
            var options = {
              showlayers: showlayers == '' ? [] : showlayers.split(','),
              hidelayers: hidelayers == '' ? [] : hidelayers.split(','),
              showgroups: showgroups == '' ? [] : showgroups.split(','),
              hidegroups: hidegroups == '' ? [] : hidegroups.split(',')
            };
        }
          this.loadMap(this.sMapResourceId, options);
        }
        this.keepAliveTimer = window.setInterval(OpenLayers.Function.bind(this.pingServer, this), this.keepAliveInterval * 1000);
    },

    sessionReady: function() {
        return (typeof this.session[0] == 'string');
    },

    getSessionID: function() {
        return this.session[0];
    },

    calcMapName: function(resourceId, bAppendUniqueId) {
        var slIdx = resourceId.lastIndexOf("/") + 1;
        var dIdx = resourceId.lastIndexOf(".");
        var name = resourceId.substring(slIdx, dIdx);
        if (bAppendUniqueId)
            name += (new Date()).getTime();
        return name;
    },

    loadMap: function(resourceId, options) {
        this.bMapLoaded = false;
        
        if (!this.sessionReady()) {
            this.sMapResourceId = resourceId;
            return;
        }

        this.triggerEvent(Fusion.Event.LAYER_LOADING);
        this.mapWidget._addWorker();

        this._fScale = -1;

        options = options || {};

        this.aShowLayers = options.showlayers || [];
        this.aHideLayers = options.hidelayers || [];
        this.aShowGroups = options.showgroups || [];
        this.aHideGroups = options.hidegroups || [];
        this.aRefreshLayers = options.refreshlayers || [];
        this.aLayers = [];

        this.oSelection = null;
        this.aSelectionCallbacks = [];
        this._bSelectionIsLoading = false;
        
        if (this.bUseNativeServices) {
            var features = (1 | 2 | 4); //We want the whole lot
            var r = new Fusion.Lib.MGRequest.MGCreateRuntimeMap(resourceId, features, 25);
            var mapName = this.calcMapName(resourceId, true);
            r.setParams({ 
                targetMapName: mapName,
                iconFormat: "GIF"
            });
            if (this.session.length == 1)
                r.setParams({ session: this.session[0] });
            Fusion.oBroker.dispatchRequest(r, OpenLayers.Function.bind(this.onRuntimeMapCreated, this));
        } else {
            var sl = Fusion.getScriptLanguage();
            var loadmapScript = 'layers/' + this.arch + '/' + sl  + '/LoadMap.' + sl;

            var sessionid = this.getSessionID();

            var params = {'mapid': resourceId, "session": sessionid};
            var options = {onSuccess: OpenLayers.Function.bind(this.mapLoaded,this),
                           parameters:params};
            Fusion.ajaxRequest(loadmapScript, options);
        }
    },
    /**
     * Re-shapes the CREATERUNTIMEMAP response to match the structure that our
     * existing initialization code is expecting
     */
    convertResponse: function(o) {
        var rt = o.RuntimeMap;
        
        //LoadMap.php response
        var lm = {
            backgroundColor: ("#" + rt.BackgroundColor[0].substring(2)),
            siteVersion: rt.SiteVersion[0],
            mapId: rt.MapDefinition[0],
            mapName: rt.Name[0],
            mapTitle: rt.Name[0],
            //backgroundColor: rt.BackgroundColor[0],
            metersPerUnit: rt.CoordinateSystem[0].MetersPerUnit ? parseFloat(rt.CoordinateSystem[0].MetersPerUnit[0]) : 1,
            wkt: rt.CoordinateSystem[0].Wkt ? rt.CoordinateSystem[0].Wkt[0] : "",
            epsg: rt.CoordinateSystem[0].EpsgCode ? parseInt(rt.CoordinateSystem[0].EpsgCode[0]) : 4326,
            extent: [
                parseFloat(rt.Extents[0].LowerLeftCoordinate[0].X[0]),
                parseFloat(rt.Extents[0].LowerLeftCoordinate[0].Y[0]),
                parseFloat(rt.Extents[0].UpperRightCoordinate[0].X[0]),
                parseFloat(rt.Extents[0].UpperRightCoordinate[0].Y[0])
            ],
            hasBaseMapLayers: false,
            hasDynamicLayers: false,
            FiniteDisplayScales: [],
            groups: [],
            layers: []
        };
        if (rt.FiniteDisplayScale) {
            for (var i = 0; i < rt.FiniteDisplayScale.length; i++) {
                lm.FiniteDisplayScales.push(parseFloat(rt.FiniteDisplayScale[i]));
            }
        }
        if (rt.Group) {
            for (var i = 0; i < rt.Group.length; i++) {
                var grp = rt.Group[i];
                var cg = {
                    groupName: grp.Name[0],
                    legendLabel: (grp.LegendLabel ? grp.LegendLabel[0] : ""),
                    uniqueId: grp.ObjectId[0],
                    displayInLegend: (grp.DisplayInLegend[0] == "true"),
                    expandInLegend: (grp.ExpandInLegend[0] == "true"), 
                    parentUniqueId: grp.ParentId ? grp.ParentId[0] : "",
                    visible: (grp.Visible[0] == "true"),
                    actuallyVisible: (grp.ActuallyVisible[0] == "true"),
                    isBaseMapGroup: (grp.Type[0] == "2")
                };
                if (grp.Type[0] == "2")
                    lm.hasBaseMapLayers = true;
                else
                    lm.hasDynamicLayers = true;
                lm.groups.push(cg);
            }
        }
        //LoadScaleRanges.php response
        var lsr = {
            layers: []
        };
        if (rt.Layer) {
            for (var i = 0; i < rt.Layer.length; i++) {
                var lyr = rt.Layer[i];
                var cl = {
                    uniqueId: lyr.ObjectId[0],
                    layerName: lyr.Name[0],
                    layerTypes: [],
                    resourceId: lyr.LayerDefinition[0],
                    parentGroup: lyr.ParentId ? lyr.ParentId[0] : "",
                    selectable: (lyr.Selectable[0] == "true"),
                    visible: (lyr.Visible[0] == "true"),
                    actuallyVisible: (lyr.ActuallyVisible[0] == "true"),
                    editable: false,
                    isBaseMapLayer: (lyr.Type[0] == "2"),
                    legendLabel: (lyr.LegendLabel ? lyr.LegendLabel[0] : ""),
                    displayInLegend: (lyr.DisplayInLegend[0] == "true"),
                    expandInLegend: (lyr.ExpandInLegend[0] == "true")
                };
                
                lm.layers.push(cl);
                
                var clsr = {
                    uniqueId: cl.uniqueId,
                    scaleRanges: []
                };
                
                var ltypes = {};
                
                var minScale = 1.0e10;
                var maxScale = 0;
                
                if (lyr.ScaleRange) {
                    for (var j = 0; j < lyr.ScaleRange.length; j++) {
                        var sr = lyr.ScaleRange[j];
                        var csr = {
                            isCompressed: false,
                            maxScale: sr.MaxScale[0],
                            minScale: sr.MinScale[0],
                            styles: []
                        };
                        
                        minScale = Math.min(minScale, sr.MinScale[0]);
                        maxScale = Math.max(maxScale, sr.MaxScale[0]);
                        
                        if (sr.FeatureStyle) {
                            for (var f = 0; f < sr.FeatureStyle.length; f++) {
                                var fts = sr.FeatureStyle[f];
                                for (var k = 0; k < fts.Rule.length; k++) {
                                    var rule = fts.Rule[k];
                                    var cr = {
                                        categoryIndex: k,
                                        filter: rule.Filter ? rule.Filter[0] : "",
                                        geometryType: parseInt(fts.Type[0]),
                                        legendLabel: rule.LegendLabel ? rule.LegendLabel[0] : ""
                                    };
                                    if (typeof(ltypes[cr.geometryType]) == 'undefined')
                                        ltypes[cr.geometryType] = cr.geometryType;
                                    //One single absence of an icon is enough to hint that it's compressed
                                    if (!rule.Icon) {
                                        csr.isCompressed = true;
                                    } else {
                                        cr.imageData = "data:" + rt.IconMimeType[0] + ";base64," + rule.Icon[0];
                                    }
                                    csr.styles.push(cr);
                                }
                            }
                        }
                        clsr.scaleRanges.push(csr);
                    }
                } else {
                    //For a drawing layer which doesn't have scale ranges, we need add a dummy scale range for it.
                    //Otherwise, the legend tree will not display correctly.
                    minScale = 0;
                    maxScale = 1.0e10;
                    clsr.scaleRanges.push(new Fusion.Layers.ScaleRange({
                        minScale: 0,
                        maxScale: 1.0e10}, 
                        Fusion.Constant.LAYER_DWF_TYPE, {label:lyr.layerName}));
                }

                for (var lt in ltypes)
                    cl.layerTypes.push(lt);
                
                cl.minScale = minScale;
                cl.maxScale = maxScale;
                
                lsr.layers.push(clsr);
            }
        }
        return {
            LoadMap: lm,
            LoadScaleRanges: lsr
        };
    },
    initLoadMapResponse: function(o) {
        this._sResourceId = o.mapId;
        this._sMapname = o.mapName;
        this._sMapTitle = o.mapTitle;
        
        // Fix defect that background color in overview map will affect background color in main map.
        // We'll first check if the loaded map is the one shown in main map.
        var currentMaps = this.mapWidget.mapGroup.maps;
        var isInMapWidget = false;
        for(var index = 0, len = currentMaps.length; index < len; index++) {
            var mapInMaps = currentMaps[index];
            if(mapInMaps.resourceId == this._sResourceId) {
                isInMapWidget = this;
                break;
            }
        }
        if(isInMapWidget) {
            this.mapWidget.setMetersPerUnit(o.metersPerUnit);
            this.mapWidget.setBackgroundColor(o.backgroundColor);
        }

        this.mapTag.layerOptions.maxExtent = OpenLayers.Bounds.fromArray(o.extent);

        this.layerRoot.clear();
        this.layerRoot.legendLabel = this._sMapTitle;
        this.layerRoot.displayInLegend = true;
        this.layerRoot.expandInLegend = true;

        this.parseMapLayersAndGroups(o);

        this.minScale = 1.0e10;
        this.maxScale = 0;
        for (var i=0; i<this.aLayers.length; i++) {
          this.minScale = Math.min(this.minScale, this.aLayers[i].minScale);
          this.maxScale = Math.max(this.maxScale, this.aLayers[i].maxScale);
        }
        //a scale value of 0 is undefined
        if (this.minScale <= 0) {
          this.minScale = 1.0;
        }

        for (var i=0; i<this.aShowLayers.length; i++) {
            var layer =  this.layerRoot.findLayerByAttribute('layerName', this.aShowLayers[i]);
            if (layer) {
                this.aShowLayers[i] = layer.uniqueId;
            } else {
                this.aShowLayers[i] = '';
            }
        }
        for (var i=0; i<this.aHideLayers.length; i++) {
            var layer =  this.layerRoot.findLayerByAttribute('layerName', this.aHideLayers[i]);
            if (layer) {
                this.aHideLayers[i] = layer.uniqueId;
            } else {
                this.aHideLayers[i] = '';
            }
        }

        for (var i=0; i<this.aShowGroups.length; i++) {
            var group =  this.layerRoot.findGroupByAttribute('groupName', this.aShowGroups[i]);
            if (group) {
                this.aShowGroups[i] = group.uniqueId;
            } else {
                this.aShowGroups[i] = '';
            }
        }

        for (var i=0; i<this.aHideGroups.length; i++) {
            var group =  this.layerRoot.findGroupByAttribute('groupName', this.aHideGroups[i]);
            if (group) {
                this.aHideGroups[i] = group.uniqueId;
            } else {
                this.aHideGroups[i] = '';
            }
        }

        if (o.hasBaseMapLayers && this.bIsMapWidgetLayer) {	//Use tile if there is base layer and in main map
            this.bSingleTile = false;
        }

        //set projection units and code if supplied
        var wktProj;
        if (o.wkt && o.wkt.length > 0){
            //Proj4js prefers EPSG codes over raw WKT. So if an EPSG code exists, use that over the WKT
            if (o.epsg != 0) {
                wktProj = new OpenLayers.Projection("EPSG:" + o.epsg);
                this.mapTag.layerOptions.projection = "EPSG:" + o.epsg;
            } else {
                wktProj = new OpenLayers.Projection(o.wkt);
            }
        } 
        if (!wktProj || (wktProj && wktProj.proj && !wktProj.proj.readyToUse)) {
            if (o.epsg != 0) {
                wktProj = new OpenLayers.Projection("EPSG:" + o.epsg);
                this.mapTag.layerOptions.projection = "EPSG:" + o.epsg;
            } else {
                //default to the local non-projected system if not otherwise specified
                o.wkt = "LOCAL_CS[\"Non-Earth (Meter)\",LOCAL_DATUM[\"Local Datum\",0],UNIT[\"Meter\", 1],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";
                wktProj = new OpenLayers.Projection(o.wkt);
            }
        }
        //TODO: consider passing the metersPerUnit value into the framework
        //to allow for scaling that doesn't match any of the pre-canned units
        this.mapTag.layerOptions.units = Fusion.getClosestUnits(o.metersPerUnit);

        //add in scales array if supplied
        if (o.FiniteDisplayScales && o.FiniteDisplayScales.length>0) {
            this.scales = o.FiniteDisplayScales;
            this.mapWidget.fractionalZoom = false;
            this.mapWidget.oMapOL.fractionalZoom = false;
        }
            
        if (!this.bSingleTile) {
            if (o.groups.length >0) {
                var tiledLayerIndex = 0;
                this.noCache = false;
                this.mapWidget.registerForEvent(Fusion.Event.MAP_EXTENTS_CHANGED, OpenLayers.Function.bind(this.mapExtentsChanged, this));
                
                for (var i=0; i<o.groups.length; i++) {
                    if(o.groups[i].isBaseMapGroup) {
                        this.oLayersOLTile[tiledLayerIndex] = this.createOLLayer(this._sMapname + "_Tiled[" + tiledLayerIndex + "]", false, 2, false, o.groups[i].groupName);              
                        tiledLayerIndex++;
                     }
                }
            } else {
                this.bSingleTile = true;
            }
        }
 
        //remove this layer if it was already created
        if (this.oLayerOL) {
            this.oLayerOL.events.unregister("loadstart", this, this.loadStart);
            this.oLayerOL.events.unregister("loadend", this, this.loadEnd);
            this.oLayerOL.events.unregister("loadcancel", this, this.loadEnd);
            this.oLayerOL.destroy();
        }

        if (this.oLayersOLTile.length != 0) {
            this.oLayerOL = this.oLayersOLTile[this.oLayersOLTile.length-1]; // The last baselayer at the bottom.
        } else {
            this.oLayerOL = this.createOLLayer(this._sMapname, this.bSingleTile, 2, false, "");
        }
        
        if (wktProj && wktProj.proj && wktProj.proj.readyToUse) {
          this.oLayerOL.projection = wktProj;
          this.oLayerOL.projection.proj.units = this.mapTag.layerOptions.units;
        }
        this.oLayerOL.events.register("loadstart", this, this.loadStart);
        this.oLayerOL.events.register("loadend", this, this.loadEnd);
        this.oLayerOL.events.register("loadcancel", this, this.loadEnd);

        
        //remove the dynamic overlay layer if it was already created
        if (this.oLayerOL2) {
            this.oLayerOL2.destroy();
        }

        //this is to distinguish between a regular map and an overview map
        this.bMapLoaded = true;
        if (this.bIsMapWidgetLayer) {
            this.mapWidget.addMap(this);
            
            if(this.oLayersOLTile.length > 1) {
                for(var i=this.oLayersOLTile.length-2; i>=0; i--) {
                    // Workaround to make multiple baselayers display. 
                    // Openlayers only supports single baselayer.
                    this.oLayersOLTile[i].isBaseLayer = false; 
                    this.mapWidget.oMapOL.addLayer(this.oLayersOLTile[i]);
                }                               
            }
            
            //if we have a tiled map that also contains dynamic layers, we need to create
            //an additional overlay layer to render them on top of the tiles
            if(!this.bSingleTile && o.hasDynamicLayers) {
                this.oLayerOL2 = this.createOLLayer(this._sMapname + "_DynamicOverlay",true,2,true, "");
                this.mapWidget.oMapOL.addLayer(this.oLayerOL2);
                this.oLayerOL2.setVisibility(true);
            }
        }
    },
    /**
     * Callback function from a LoadMap.php request
     */
    mapLoaded: function(r) {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            this.initLoadMapResponse(o);
        }
        this.mapWidget._removeWorker();
        this.triggerEvent(Fusion.Event.LAYER_LOADED);

    },
    /**
     * Callback function from a CREATERUNTIMEMAP request
     */
    onRuntimeMapCreated: function(r) {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            var co = this.convertResponse(o);
            this.initLoadMapResponse(co.LoadMap);
            //Need to wait for the right event to trigger loadScaleRanges, so stash our
            //prepared result for when it comes
            this._initScaleRanges = co.LoadScaleRanges;
        }
        this.mapWidget._removeWorker();
        this.triggerEvent(Fusion.Event.LAYER_LOADED);
    },
//TBD: this function not yet converted for OL
    reloadMap: function() {

        this.mapWidget._addWorker();
        //console.log('loadMap: ' + resourceId);
        this.aShowLayers = [];
        this.aHideLayers = [];
        this.aShowGroups = [];
        this.aHideGroups = [];
        this.aRefreshLayers = [];
        this.layerRoot.clear();
        var oldLayers = $A(this.aLayers);
        this.aLayers = [];

        if (this.bUseNativeServices) {
            var features = (1 | 2 | 4); //We want the whole lot
            var r = new Fusion.Lib.MGRequest.MGDescribeRuntimeMap(this._sMapname, features, 25);
            r.setParams({
                iconFormat: "GIF",
                session: this.getSessionID()
            });
            Fusion.oBroker.dispatchRequest(r, OpenLayers.Function.bind(this.onRuntimeMapReloaded, this, oldLayers));
        } else {
            var sl = Fusion.getScriptLanguage();
            var loadmapScript = 'layers/' + this.arch + '/' + sl  + '/LoadMap.' + sl;

            var sessionid = this.getSessionID();

            var params = {'mapname': this._sMapname, 'session': sessionid};
            var options = {
                  onSuccess: OpenLayers.Function.bind(this.mapReloaded,this,oldLayers),
                  onException: OpenLayers.Function.bind(this.reloadFailed, this),
                  parameters: params};
            Fusion.ajaxRequest(loadmapScript, options);
        }
    },

    reloadFailed: function(r) {
        this.mapWidget._removeWorker();
        Fusion.reportFatalError(OpenLayers.i18n('mapLoadError', {'error':r.transport.responseText}));
    },

    /**
     * Function: loadScaleRanges
     *
     * This function should be called after the map has loaded. It
     * loads the scsle ranges for each layer. I tis for now only
     * used by the legend widget.
     */

    loadScaleRanges: function() {
        if (this.bUseNativeServices && this._initScaleRanges) {
            this.initLoadScaleRangeResponse(this._initScaleRanges);
            delete this._initScaleRanges;
        } else {
            var sl = Fusion.getScriptLanguage();
            var loadmapScript = 'layers/' + this.arch + '/' + sl  + '/LoadScaleRanges.' + sl;

            //IE7 or lower: No pre-caching for you!
            var preCacheIcons = !(Browser.Engine.trident4 || Browser.Engine.trident5);
            //console.log("Layer icon pre-caching enabled: " + preCacheIcons);
            var sessionid = this.getSessionID();

            var params = {'mapname': this._sMapname, "session": sessionid, "preCacheIcons": preCacheIcons};
            var options = {onSuccess: OpenLayers.Function.bind(this.scaleRangesLoaded,this),
                           parameters:params};
            Fusion.ajaxRequest(loadmapScript, options);
        }
    },

    initLoadScaleRangeResponse: function(o) {
        if (o.layers && o.layers.length > 0) {
            var iconOpt = {
                url: o.icons_url || null,
                width: o.icons_width || 16,
                height: o.icons_height || 16
            };
            for (var i=0; i<o.layers.length; i++)  {
                var oLayer = this.getLayerById(o.layers[i].uniqueId);
                if (oLayer) {
                    oLayer.scaleRanges = [];
                    for (var j=0; j<o.layers[i].scaleRanges.length; j++) {
                        var scaleRange = new Fusion.Layers.ScaleRange(o.layers[i].scaleRanges[j],
                                                                             oLayer.layerType, iconOpt);
                        oLayer.scaleRanges.push(scaleRange);
                    }
                }
            }
        }
        this.mapWidget.triggerEvent(Fusion.Event.MAP_SCALE_RANGE_LOADED);
    },

    scaleRangesLoaded: function(r)
    {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            this.initLoadScaleRangeResponse(o);
        }
    },
    
    onRuntimeMapReloaded: function(oldLayers, r) {
        if (r.status == 200) {
            var json = Fusion.parseJSON(r.responseText);
            var co = this.convertResponse(json);
            var o = co.LoadMap;
            this.parseMapLayersAndGroups(o);
            //Need to wait for the right event to trigger loadScaleRanges, so stash our
            //prepared result for when it comes
            this._initScaleRanges = co.LoadScaleRanges;
            for (var i=0; i<this.aLayers.length; ++i) {
              var newLayer = this.aLayers[i];
              for (var j=0; j<oldLayers.length; ++j){
                if (oldLayers[j].uniqueId == newLayer.uniqueId) {
                  newLayer.selectedFeatureCount = oldLayers[j].selectedFeatureCount;
                  newLayer.noCache = oldLayers[j].noCache;
                  break;
                }
              }
            }
            this.mapWidget.triggerEvent(Fusion.Event.MAP_RELOADED);
            this.drawMap();
        }
        this.mapWidget._removeWorker();
    },
    
//TBD: this function not yet converted for OL
    mapReloaded: function(oldLayers,r) {
        if (r.status == 200) {
            var o = Fusion.parseJSON(r.responseText);
            this.parseMapLayersAndGroups(o);
            for (var i=0; i<this.aLayers.length; ++i) {
              var newLayer = this.aLayers[i];
              for (var j=0; j<oldLayers.length; ++j){
                if (oldLayers[j].uniqueId == newLayer.uniqueId) {
                  newLayer.selectedFeatureCount = oldLayers[j].selectedFeatureCount;
                  newLayer.noCache = oldLayers[j].noCache;
                  break;
                }
              }
            }
            this.mapWidget.triggerEvent(Fusion.Event.MAP_RELOADED);
            this.drawMap();
        }
        this.mapWidget._removeWorker();
    },

    reorderLayers: function(aLayerIndex) {
        var sl = Fusion.getScriptLanguage();
        var loadmapScript = 'layers/' + this.arch + '/' + sl  + '/SetLayers.' + sl;

        var params = {
            'mapname': this._sMapname,
            'session': this.getSessionID(),
            'layerindex': aLayerIndex.join()
        };

        var options = {
            onSuccess: OpenLayers.Function.bind(this.mapLayersReset, this, aLayerIndex),
            parameters: params};
        Fusion.ajaxRequest(loadmapScript, options);
    },

    mapLayersReset: function(aLayerIndex,r) {
      if (r.status == 200) {
        var o = Fusion.parseJSON(r.responseText);
            if (o.success) {
                var layerCopy = $A(this.aLayers);
                this.aLayers = [];
                this.aVisibleLayers = [];
          for (var i=0; i<aLayerIndex.length; ++i) {
            this.aLayers.push( layerCopy[ aLayerIndex[i] ] );
            if (this.aLayers[i].visible) {
                this.aVisibleLayers.push(this.aLayers[i].layerName);
            }
          }

                this.drawMap();
                this.triggerEvent(Fusion.Event.MAP_LAYER_ORDER_CHANGED);
            } else {
                alert(OpenLayers.i18n('setLayersError', {'error':o.layerindex}));
            }
        }
    },

    parseMapLayersAndGroups: function(o) {
        for (var i=0; i<o.groups.length; i++) {
            var group = new Fusion.Layers.Group(o.groups[i], this);
            var parent;
            console.log('!', group.parentUniqueId);
            if (group.parentUniqueId != '') {
                parent = this.layerRoot.findGroupByAttribute('uniqueId', group.parentUniqueId);
            } else {
                parent = this.layerRoot;
            }
            
            if(!parent) {
                parent = this.layerRoot;
            }
            console.log(parent, group);
            parent.addGroup(group, this.bLayersReversed);
        }

        for (var i=0; i<o.layers.length; i++) {
            var layer = new Fusion.Layers.Layer(o.layers[i], this);
            var parent;
            console.log('!', layer.parentGroup);
            
            if (layer.parentGroup != '') {
                parent = this.layerRoot.findGroupByAttribute('uniqueId', layer.parentGroup);
            } else {
                parent = this.layerRoot;
            }
            
            if(!parent) {
                parent = this.layerRoot;
            }
            
            parent.addLayer(layer, this.bLayersReversed);
            this.aLayers.push(layer);
        }
    },

    _drawMapInternal: function() {
        var params = {
            ts : (new Date()).getTime(),  //add a timestamp to prevent caching on the server
            showLayers : this.aShowLayers.length > 0 ? this.aShowLayers.toString() : null,
            hideLayers : this.aHideLayers.length > 0 ? this.aHideLayers.toString() : null,
            showGroups : this.aShowGroups.length > 0 ? this.aShowGroups.toString() : null,
            hideGroups : this.aHideGroups.length > 0 ? this.aHideGroups.toString() : null,
            refreshLayers : this.aRefreshLayers.length > 0 ? this.aRefreshLayers.toString() : null
        };

        this.aShowLayers = [];
        this.aHideLayers = [];
        this.aShowGroups = [];
        this.aHideGroups = [];
        this.aRefreshLayers = [];

        if (this.oLayerOL2) {
            this.oLayerOL2.mergeNewParams(params);
        } else {
            this.oLayerOL.mergeNewParams(params);
        }
        //console.log("Draw call completed");
        this.drawTimeout = null;
    },

    drawMap: function() {
        if (!this.bMapLoaded) {
            return;
        }
        if (this.drawDelay <= 0) { //No delay, draw immediately
            //console.log("Draw immediate");
            this._drawMapInternal();
        } else {
            //Check if we have a pending draw call and re-schedule if there is one
            if (this.drawTimeout) {
                //console.log("Delay pending draw");
                clearTimeout(this.drawTimeout);
                this.drawTimeout = null;
            }
            this.drawTimeout = setTimeout(OpenLayers.Function.bind(this._drawMapInternal, this), this.drawDelay);
            //console.log("Schedule draw in " + this.drawDelay + "ms");
        }
    },

    drawSelection: function() {
        if (this.queryLayer) {
            this.queryLayer.redraw(true);
        } else {
            this.drawMap();
        }
    },

    /**
     * Function: createOLLayer
     *
     * Returns an OpenLayers MapGuide layer object
     */
    createOLLayer: function(layerName, bSingleTile, behavior, forceAsOverlay, baselayerGroupName) {
      /* prevent the useOverlay flag based on MapGuide config element */
      this.useAsyncOverlay = Fusion.getConfigurationItem('mapguide', 'useAsyncOverlay');
      if (!this.useAsyncOverlay) {          //v2.0.1 or earlier
        this.selectionAsOverlay = false;
      }
      
      var layerOptions = {
        maxResolution: 'auto',
        useOverlay: this.selectionAsOverlay,
        useAsyncOverlay: this.useAsyncOverlay,
        ratio: this.ratio,
        transitionEffect: 'resize'
      };

      //add in scales array if supplied
      if (this.scales && this.scales.length>0) {
        layerOptions.scales = this.scales;
      }
      if (this.maxScale != Infinity) {
        layerOptions.minScale = this.maxScale;    //OL interpretation of min/max scale is reversed from Fusion
      } else {
        if (this.mapWidget.minScale) {
          layerOptions.minScale = this.mapWidget.maxScale;
        }// otherwise minscale is set automatically by OL
      }
      //only set both max and min scale when not using scales array
      if (!this.mapWidget.oMapOL.scales && !this.scales) {
        layerOptions.maxScale = this.minScale;
      }

      layerOptions.displayOutsideMaxExtent = true;
      layerOptions.singleTile = bSingleTile;
      OpenLayers.Util.extend(layerOptions, this.mapTag.layerOptions);

      var params = {};
      if ( bSingleTile ) {
        params = {        //single tile params
          session: this.getSessionID(),
          mapname: this._sMapname,
          format: this.imageFormat,
          behavior: behavior,
          clientagent: this.clientAgent
        };
        params.showLayers = this.aShowLayers.length > 0 ? this.aShowLayers.toString() : null;
        params.hideLayers = this.aHideLayers.length > 0 ? this.aHideLayers.toString() : null;
        params.showGroups = this.aShowGroups.length > 0 ? this.aShowGroups.toString() : null;
        params.hideGroups = this.aHideGroups.length > 0 ? this.aHideGroups.toString() : null;
        params.refreshLayers = this.aRefreshLayers.length > 0 ? this.aRefreshLayers.toString() : null;

        if (behavior == 5) {
          params.selectioncolor = this.selectionColor;
          params.format = this.selectionFormat;
        }
        
        if(forceAsOverlay)
        {
            layerOptions.isBaseLayer = false;
        }

      } else {
        params = {      //tiled version
          mapdefinition: this._sResourceId,
          basemaplayergroupname: baselayerGroupName, 
          session: this.getSessionID(),
          clientagent: this.clientAgent
        };
      }
      
      //Fix for IE6 PNG transparency
      if (params.format && params.format.toLowerCase().indexOf('png') >= 0) {
        layerOptions.alpha = true;
      }

      var url;
      if ( !bSingleTile && layerOptions.useHttpTile) {
        url = Fusion.getConfigurationItem('mapguide', 'tileCacheUrl');
      } else {
        url = Fusion.getConfigurationItem('mapguide', 'mapAgentUrl');
      }
      
      if (this.alternateHostNames)
      {
        var hosts = this.alternateHostNames.split(",");
        var httpIndex = url.indexOf("http://") + 7;
        if (httpIndex < 7) {
            httpIndex = url.indexOf("https://") + 8;
        }
        var proto = url.substring(0, httpIndex);
        var relIndex = url.indexOf("/", httpIndex+1);
        var relPath = url.substring(relIndex);
        
        layerOptions.alternateUrls = [];
        
        for (var i = 0; i < hosts.length; i++) {
            var altUrl = proto + hosts[i] + relPath;
            layerOptions.alternateUrls.push(altUrl);
        }
      }
      
      var oNewLayerOL = new OpenLayers.Layer.MapGuide( layerName, url, params, layerOptions );
      if (!bSingleTile) {
        if (oNewLayerOL.scales.length == this.aCmsScales.length) { 
            //NOTE: This is not a property of OpenLayers.Layer.MapGuide, it is something we've bolted on
            oNewLayerOL.bUsesCommercialLayerScaleList = false;
            for (var i = 0; i < this.aCmsScales.length; i++) {
                if (!this.scalesAreApproximate(oNewLayerOL.scales[i], this.aCmsScales[i]))
                {
                    return oNewLayerOL; //Doesn't match. Nothing more to do here
                }
            }
            oNewLayerOL.bUsesCommercialLayerScaleList = true;
            this.bUsesCommercialLayerScaleList = true;
        }
      }
      return oNewLayerOL;
    },
    
    scalesAreApproximate: function(scale1, scale2) {
        return Math.abs(scale1 - scale2) < this.nCmsScaleTolerance;
    },
    
    applyZoomOffset: function(offset) {
        //console.log("Applying zoom offset of: " + offset);
        //We need to redraw to prevent potential mismatch after switching of commerical layers
        //TODO: This is called for each commerical layer in the basemap switcher widget, do
        //redraw() calls at this point result in redundant requests?
        if (this.oLayerOL && this.oLayerOL.bUsesCommercialLayerScaleList === true) {
            this.oLayerOL.zoomOffset = offset;
            this.oLayerOL.redraw();
        }
        if (this.oLayerOL2 && this.oLayerOL2.bUsesCommercialLayerScaleList === true) {
            this.oLayerOL2.zoomOffset = offset;
            this.oLayerOL2.redraw();
        }
        if (this.oLayersOLTile) {
            for (var i = 0; i < this.oLayersOLTile.length; i++) {
                if (this.oLayersOLTile[i].bUsesCommercialLayerScaleList === true) {
                    this.oLayersOLTile[i].zoomOffset = offset;
                    this.oLayersOLTile[i].redraw();
                }
            }
        }
    },

    /**
     * Function: getLayerByName
     *
     * Returns the MapGuide layer object as identified by the layer name
     */
    getLayerByName : function(name)
    {
        var oLayer = null;
        for (var i=0; i<this.aLayers.length; i++)
        {
            if (this.aLayers[i].layerName == name)
            {
                oLayer = this.aLayers[i];
                break;
            }
        }
        return oLayer;
    },

    /**
     * Function: getLayerById
     *
     * Returns the MapGuide layer object as identified by the layer unique id
     */
    getLayerById : function(id)
    {
        var oLayer = null;
        for (var i=0; i<this.aLayers.length; i++)
        {
            if (this.aLayers[i].uniqueId == id)
            {
                oLayer = this.aLayers[i];
                break;
            }
        }
        return oLayer;
    },

    getSelectionCB: function(userFunc, r) {
      if (r.status == 200) {
          var o = Fusion.parseJSON(r.responseText);
          var oSelection = new Fusion.SelectionObject(o);
          userFunc(oSelection);
      }
    },

    /**
     * advertise a new selection is available
     */
    newSelection: function() {
        if (this.oSelection) {
            this.oSelection = null;
        }
        this.bSelectionOn = true;
        this.triggerEvent(Fusion.Event.MAP_SELECTION_ON);
    },

    /**
     * Returns the number of features selected for this map layer
     */
    getSelectedFeatureCount: function() {
      var total = 0;
      for (var j=0; j<this.aLayers.length; ++j) {
        total += this.aLayers[j].selectedFeatureCount;
      }
      return total;
    },

    /**
     * Returns the number of features selected for this map layer
     */
    getSelectedLayers: function() {
      var layers = [];
      for (var j=0; j<this.aLayers.length; ++j) {
        if (this.aLayers[j].selectedFeatureCount>0) {
          layers.push(this.aLayers[j]);
        }
      }
      return layers;
    },

    /**
     * Returns the number of features selected for this map layer
     */
    getSelectableLayers: function() {
      var layers = [];
      for (var j=0; j<this.aLayers.length; ++j) {
        if (this.aLayers[j].selectable) {
          layers.push(this.aLayers[j]);
        }
      }
      return layers;
    },

    /**
     * Updates the current map selection with the provided XML selection string.
     * Optionally zooms to the new selection on the map, if zoomTo is set to true.
     */
    setSelection: function (selText, zoomTo) {

        //TODO Update this.previousSelection when the selection is set using
        //this API to allow the selection to be extended with a shift-click.

        if(selText != "" && selText != null) {
            this.updateSelection(selText, zoomTo, false);
        }
        else {
            this.clearSelection();
        }
    },

    updateSelection: function (selText, zoomTo, extendSelection) {
        this.updateMapSelection(selText, zoomTo);
        this.getSelectedFeatureProperties(selText);
    },


    getSelectedFeatureProperties: function (selText) {
      this.mapWidget._addWorker();
      var sl = Fusion.getScriptLanguage();
      var getPropertiesScript = 'layers/' + this.arch + '/' + sl  + '/GetSelectionProperties.' + sl;
      var params = {
          'mapname': this.getMapName(),
          'session': this.getSessionID(),
          'selection': selText,
          'seq': Math.random()
      };
      var options = {onSuccess: OpenLayers.Function.bind(this.processSelectedFeatureProperties, this),
                     parameters:params};
      Fusion.ajaxRequest(getPropertiesScript, options);
    },

    updateMapSelection: function (selText, zoomTo) {
      this.mapWidget._addWorker();
      var sl = Fusion.getScriptLanguage();
      var updateSelectionScript = 'layers/' + this.arch + '/' + sl  + '/SaveSelection.' + sl;
      var params = {
          'mapname': this.getMapName(),
          'session': this.getSessionID(),
          'selection': selText,
          'seq': Math.random(),
          'getextents' : zoomTo ? 'true' : 'false'
      };
      var options = {onSuccess: OpenLayers.Function.bind(this.renderSelection, this, zoomTo),
                     parameters:params};
      Fusion.ajaxRequest(updateSelectionScript, options);
    },



     /**
     * asynchronously load the current selection.  When the current
     * selection changes, the selection is not loaded because it
     * could be a lengthy process.  The user-supplied function will
     * be called when the selection is available.
     *
     * @param userFunc {Function} a function to call when the
     *        selection has loaded
     *
     * @param layers {string} Optional parameter.  A comma separated
     *        list of layer names (Roads,Parcels). If it is not
     *        given, all the layers that have a selection will be used
     *
     * @param startcount {string} Optional parameter.  A comma separated
     *        list of a statinh index and the number of features to be retured for
     *        each layer given in the layers parameter. Index starts at 0
     *        (eg: 0:4,2:6 : return 4 elements for the first layers starting at index 0 and
     *         six elements for layer 2 starting at index 6). If it is not
     *        given, all the elemsnts will be returned.
     */
    getSelection: function(userFunc, layers, startcount) {
        /*for now always go back to server to fetch selection */
        if (userFunc)
        {
            if (this.previousAttributes) {
                userFunc(new Fusion.SelectionObject(this.previousAttributes));
            } else {
                //this.aSelectionCallbacks.push(userFunc);
                //this.mapWidget._addWorker();
                // this._bSelectionIsLoading = true;
                var s = 'layers/' + this.arch + '/' + Fusion.getScriptLanguage() + "/Selection." + Fusion.getScriptLanguage() ;
                var options = {
                    parameters: {
                        'session': this.getSessionID(),
                        'mapname': this._sMapname,
                        'layers': layers,
                        'startcount': startcount
                    },
                    onSuccess: OpenLayers.Function.bind(this.getSelectionCB, this, userFunc)
                };
                Fusion.ajaxRequest(s, options);
            }
        }
    },

    /**
       Call back function when selection is cleared
    */
    selectionCleared: function()
    {
        //clear the selection count for the layers
        for (var j=0; j<this.aLayers.length; ++j) {
          this.aLayers[j].selectedFeatureCount = 0;
        }

        this.bSelectionOn = false;
        if (this.queryLayer) {
          this.queryLayer.setVisibility(false);
        }
        this.triggerEvent(Fusion.Event.MAP_SELECTION_OFF);
        this.drawMap();
        this.oSelection = null;
    },

    /**
       Utility function to clear current selection
    */
    clearSelection: function() {
      if (this.hasSelection()) {
          var s = 'layers/' + this.arch + '/' + Fusion.getScriptLanguage() + "/ClearSelection." + Fusion.getScriptLanguage() ;
          var options = {
              parameters: {'session': this.getSessionID(),
                          'mapname': this._sMapname},
              onSuccess: OpenLayers.Function.bind(this.selectionCleared, this)
          };
          Fusion.ajaxRequest(s, options);
      }
      if (this.previousSelection != null)
      {
          this.previousSelection.clear();
      }
    },

    /**
       removes the queryLayer from the map
    */
    removeQueryLayer: function() {
      if (this.queryLayer) {
        this.queryLayer.destroy();
        this.queryLayer = null;
      }
    },

    /**
       Call back function when select functions are called (eg queryRect)
    */
    processQueryResults: function(zoomTo, r) {
        this.renderSelection(zoomTo, r);
        this.processSelectedFeatureProperties(r);
    },

    processSelectedFeaturePropertiesNode: function(oNode) {
        if (oNode.hasSelection) {
            this.newSelection();
        } else {
            this.clearSelection();
        }
    },

    /**
       Call back function when select functions are called (eg queryRect)
       to handle feature attributes
    */
    processSelectedFeatureProperties: function(r) {
        this.mapWidget._removeWorker();
        if (r.responseText) {   //TODO: make the equivalent change to MapServer.js
            var oNode = Fusion.parseJSON(r.responseText);
            this.processSelectedFeaturePropertiesNode(oNode);
        }
    },

    /**
       Call back function when select functions are called (eg queryRect)
       to render the selection
    */
    renderSelection: function(zoomTo, r) {
        this.mapWidget._removeWorker();
        if (r.responseText) {   //TODO: make the equivalent change to MapServer.js
            var oNode = Fusion.parseJSON(r.responseText);

            if (oNode.hasSelection) {
              if (this.selectionAsOverlay) {
                if (!this.queryLayer) {
                  this.queryLayer = this.createOLLayer("query layer", true, 5, true, "");
                  this.mapWidget.oMapOL.addLayer(this.queryLayer);
                  this.mapWidget.registerForEvent(Fusion.Event.MAP_LOADING,
                        OpenLayers.Function.bind(this.removeQueryLayer, this));
                } else {
                  this.queryLayer.setVisibility(true);
                }
              }

              //Fix Ticket #1145.
              //When the user invokes the setSelection() function to update the selection,
              //clear the selection count for all layers before proceeding
              for (var j=0; j<this.aLayers.length; ++j) {
                this.aLayers[j].selectedFeatureCount = 0;
              }

              // set the feature count on each layer making up this map
              for (var i=0; i<oNode.layers.length; ++i) {
                var layerName = oNode.layers[i];
                for (var j=0; j<this.aLayers.length; ++j) {
                  if (layerName == this.aLayers[j].layerName) {
                    this.aLayers[j].selectedFeatureCount = oNode[layerName].featureCount;
                  }
                }
              }

              if (zoomTo) {
                var ext = oNode.extents;
                var extents = new OpenLayers.Bounds(ext.minx, ext.miny, ext.maxx, ext.maxy);
                this.mapWidget.setExtents(extents);
              }
              this.drawSelection();
            } else {
              this.clearSelection();
              return;
            }
        }
    },

    /**
       Do a query on the map
    */
    query: function(options) {
        this.mapWidget._addWorker();

        //clear the selection count for the layers
        for (var j=0; j<this.aLayers.length; ++j) {
          this.aLayers[j].selectedFeatureCount = 0;
        }

        var persist = 1;
        var layerAttributeFilter = 3;
        var maxFeatures = options.maxFeatures;
        if(maxFeatures == null || maxFeatures == 0)
        {
            maxFeatures = -1;
        }
        if(options.filter == null)
        {
            options.filter = '';
        }
        
        if (this.bUseNativeServices) {
            var reqData = 1; //attributes
            //TODO: Can't use inline selection image yet as we'll have to modify OpenLayers to accept an inline selection
            //over doing a GETDYNAMICMAPOVERLAYIMAGE request. When we can, or the value of 2 into the mask for inline 
            //selection as well
            var r = new Fusion.Lib.MGRequest.MGQueryMapFeatures2(this.getSessionID(),
                                                                this._sMapname,
                                                                options.geometry,
                                                                maxFeatures,
                                                                persist,
                                                                options.selectionType || this.selectionType,
                                                                options.filter,
                                                                options.layers,
                                                                layerAttributeFilter,
                                                                reqData,
                                                                this.selectionColor,
                                                                this.selectionImageFormat);
            var callback = (options.extendSelection == true) ? OpenLayers.Function.bind(this.processAndMergeExtendedFeatureInfo, this) : OpenLayers.Function.bind(this.processExtendedFeatureInfo, this);
            Fusion.oBroker.dispatchRequest(r, callback);
        } else {
            var r = new Fusion.Lib.MGRequest.MGQueryMapFeatures(this.getSessionID(),
                                                                this._sMapname,
                                                                options.geometry,
                                                                maxFeatures,
                                                                persist,
                                                                options.selectionType || this.selectionType,
                                                                options.filter,
                                                                options.layers,
                                                                layerAttributeFilter);
            var callback = (options.extendSelection == true) ? OpenLayers.Function.bind(this.processAndMergeFeatureInfo, this) : OpenLayers.Function.bind(this.processFeatureInfo, this);
            Fusion.oBroker.dispatchRequest(r, callback);
            //Uncomment below lines (and comment line above) if QUERYMAPFEATURES does not support JSON output (http://trac.osgeo.org/mapguide/ticket/2090)
            //Fusion.oBroker.dispatchRequest(r, 
            //    OpenLayers.Function.bind(Fusion.xml2json, this, 
            //        callback));
        }
    },

    showLayer: function( layer, noDraw ) {
        this.processLayerEvents(layer, true);
        this.aShowLayers.push(layer.uniqueId);
        //A layer cannot be both hidden and shown, which can be the case if there is a draw
        //delay and the user has toggled on/off the same layer in quick succession before the
        //delay has elapsed
        if (this.drawDelay > 0) {
            this.aHideLayers.erase(layer.uniqueId);
        }
        if (!noDraw) {
            this.drawMap();
        }
    },

    hideLayer: function( layer, noDraw ) {
        this.processLayerEvents(layer, false);
        this.aHideLayers.push(layer.uniqueId);
        //A layer cannot be both hidden and shown, which can be the case if there is a draw
        //delay and the user has toggled on/off the same layer in quick succession before the
        //delay has elapsed
        if (this.drawDelay > 0) {
            this.aShowLayers.erase(layer.uniqueId);
        }
        if (!noDraw) {
            this.drawMap();
        }
    },

    showGroup: function( group, noDraw ) {
        this.processGroupEvents(group, true);
        if (group.groupName == 'layerRoot') {
            this.oLayerOL.setVisibility(true);
            if (this.oLayerOL2) this.oLayerOL2.setVisibility(true);
        } else if (group.isBaseMapGroup) {
            for(var i=0; i<this.oLayersOLTile.length; i++) {
                if(this.oLayersOLTile[i].params.basemaplayergroupname == group.name) {
                    this.oLayersOLTile[i].setVisibility(true);
                }
            }
        } else {
            this.aShowGroups.push(group.uniqueId);
            //A group cannot be both hidden and shown, which can be the case if there is a draw
            //delay and the user has toggled on/off the same group in quick succession before the
            //delay has elapsed
            if (this.drawDelay > 0) {
                this.aHideGroups.erase(group.uniqueId);
            }
            if (!noDraw) {
                this.drawMap();
            }
        }
    },
    hideGroup: function( group, noDraw ) {
        this.processGroupEvents(group, false);
        if (group.groupName == 'layerRoot') {
            this.oLayerOL.setVisibility(false);
            if (this.oLayerOL2) this.oLayerOL2.setVisibility(false);
        } else if (group.isBaseMapGroup) {
            for(var i=0; i<this.oLayersOLTile.length; i++) {
                if(this.oLayersOLTile[i].params.basemaplayergroupname == group.name) {
                    this.oLayersOLTile[i].setVisibility(false);
                }
            }
        } else {
            this.aHideGroups.push(group.uniqueId);
            //A group cannot be both hidden and shown, which can be the case if there is a draw
            //delay and the user has toggled on/off the same group in quick succession before the
            //delay has elapsed
            if (this.drawDelay > 0) {
                this.aShowGroups.erase(group.uniqueId);
            }
            if (!noDraw) {
                this.drawMap();
            }
        }
    },
    refreshLayer: function( layer ) {
        this.aRefreshLayers.push(layer.uniqueId);
        this.drawMap();
    },

    /**
     * called when there is a click on the map holding the CTRL key: query features at that postion.
     **/
    mouseUpCRTLClick: function(evt) {
      if (evt.ctrlKey) {
        var min = this.mapWidget.pixToGeo(evt.xy.x-this.nTolerance, evt.xy.y-this.nTolerance);
        var max = this.mapWidget.pixToGeo(evt.xy.x+this.nTolerance, evt.xy.y+this.nTolerance);
        if (!min) {
          return;
        }
        var sGeometry = 'POLYGON(('+ min.x + ' ' +  min.y + ', ' +  min.x + ' ' +  max.y + ', ' + max.x + ' ' +  max.y + ', ' + max.x + ' ' +  min.y + ', ' + min.x + ' ' +  min.y + '))';
        //var sGeometry = 'POINT('+ min.x + ' ' +  min.y + ')';

        var maxFeatures = 1;
        var persist = 0;
        var selection = 'INTERSECTS';
        var filter = '';
        var layerNames = '';
        var layerAttributeFilter = 3;
        var sep = '';
        for (var i=0; i<this.aLayers.length; ++i) {
          layerNames += sep + this.aLayers[i].layerName;
          sep = ',';
        }
        var r = new Fusion.Lib.MGRequest.MGQueryMapFeatures(this.getSessionID(),
                                                            this._sMapname,
                                                            sGeometry,
                                                            maxFeatures, persist, selection, filter, layerNames,
                                                            layerAttributeFilter);
        var callback = OpenLayers.Function.bind(this.crtlClickDisplay, this);
        Fusion.oBroker.dispatchRequest(r, callback);
      }
    },

    /**
     * open a window if a URL is defined for the feature.
     **/
    crtlClickDisplay: function(xhr) {
        //console.log('ctrlclcik  _display');
        if (xhr.status == 200) {
            var o = Fusion.parseJSON(xhr.responseText);
            var h = o['FeatureInformation']['Hyperlink'];
            if (h) {
                window.open(h[0], "");
            }
        }
    },

    //GETVISIBLEMAPEXTENT must be called for tiled maps whenever the extents
    //are changed so that tooltips will work properly
    mapExtentsChanged: function() {
      if (!this.singleTile) {
          var center = this.mapWidget.oMapOL.getCenter();
          var display = this.mapWidget.oMapOL.getSize();
          
          var r = new Fusion.Lib.MGRequest.MGGetVisibleMapExtent(this.getSessionID(),
                                                              this._sMapname,
                                                              center.lon, center.lat,
                                                              this.mapWidget.oMapOL.getScale(),
                                                              null,
                                                              this._nDpi,
                                                              display.w, display.h);
          Fusion.oBroker.dispatchRequest(r);
      }
    },

    pingServer: function() {
        var s = 'layers/' + this.arch + '/' + Fusion.getScriptLanguage() + "/Common." + Fusion.getScriptLanguage() ;
        var params = {onSuccess: OpenLayers.Function.bind(this.checkPingResponse, this)};
        params.parameters = {'session': this.getSessionID()};
        Fusion.ajaxRequest(s, params);
    },
    
    checkPingResponse: function(xhr) {
        if (xhr.responseText) {
            var o = Fusion.parseJSON(xhr.responseText);
            if (!o.success) {
                clearInterval(this.keepAliveTimer);
                Fusion.reportFatalError(o.message);
            }
        }
    },

    getLinkParams: function() {
      var queryParams = {};
      queryParams.theme = this.sMapResourceId;

      //determine which layers have been toggled
      var showLayers = [];
      var hideLayers = [];
      for (var i=0; i<this.aLayers.length; ++i) {
        var layer = this.aLayers[i];
        if (layer.visible && !layer.initiallyVisible) {  //layer was turned on
          showLayers.push(layer.layerName);
        }
        if (!layer.visible && layer.initiallyVisible) {  //layer was turned off
          hideLayers.push(layer.layerName);
        }
      }
      queryParams.showlayers = showLayers.join(',');
      queryParams.hidelayers = hideLayers.join(',');

      //determine which groups have been toggled
      var showGroups = [];
      var hideGroups = [];
      for (var i=0; i<this.layerRoot.groups.length; ++i) {
        var group = this.layerRoot.groups[i];
        if (group.visible && !group.initiallyVisible) {  //layer was turned on
          showGroups.push(group.groupName);
        }
        if (!group.visible && group.initiallyVisible) {  //layer was turned off
          hideGroups.push(group.groupName);
        }
      }
      queryParams.showgroups = showGroups.join(',');
      queryParams.hidegroups = hideGroups.join(',');

      return queryParams;
    },
    
    getMapTip: function(mapTipWidget) {
      //console.log('showMaptip');
        var oBroker = Fusion.oBroker;
        var x = mapTipWidget.oCurrentPosition.x;
        var y = mapTipWidget.oCurrentPosition.y;
        var min = this.mapWidget.pixToGeo(x-mapTipWidget.nTolerance, y-mapTipWidget.nTolerance);
        var max = this.mapWidget.pixToGeo(x+mapTipWidget.nTolerance, y+mapTipWidget.nTolerance);
        //this can fail if no map is loaded
        if (!min) {
            return;
        }
        var sGeometry = 'POLYGON(('+ min.x + ' ' +  min.y + ', ' +  min.x + ' ' +  max.y + ', ' + max.x + ' ' +  max.y + ', ' + max.x + ' ' +  min.y + ', ' + min.x + ' ' +  min.y + '))';

        //var sGeometry = 'POINT('+ min.x + ' ' +  min.y + ')';

        var maxFeatures = 1;
        var persist = 0;
        var selection = 'INTERSECTS';
        var filter = '';
        // only select visible layers with maptips defined (1+4)
        var layerAttributeFilter = 5;
        //TODO: possibly make the layer names configurable?
        var layerNames = mapTipWidget.aLayers.toString();
        if (this.bUseNativeServices) {
            var reqData = (4 | 8); //Tooltips and hyperlinks
            var r = new Fusion.Lib.MGRequest.MGQueryMapFeatures2(this.getSessionID(),
                                            this._sMapname,
                                            sGeometry,
                                            maxFeatures, persist, selection, filter, layerNames,
                                            layerAttributeFilter,
                                            reqData,
                                            this.selectionColor,
                                            this.selectionImageFormat);
            oBroker.dispatchRequest(r, OpenLayers.Function.bind(this.parseMapTip, this));
        } else {
            var r = new Fusion.Lib.MGRequest.MGQueryMapFeatures(this.getSessionID(),
                                            this._sMapname,
                                            sGeometry,
                                            maxFeatures, persist, selection, filter, layerNames,
                                            layerAttributeFilter);
            oBroker.dispatchRequest(r, OpenLayers.Function.bind(this.parseMapTip, this));
            //Uncomment below lines (and comment line above) if QUERYMAPFEATURES does not support JSON output (http://trac.osgeo.org/mapguide/ticket/2090)
            //oBroker.dispatchRequest(r, 
            //    OpenLayers.Function.bind(Fusion.xml2json, this, 
            //        OpenLayers.Function.bind(this.parseMapTip, this)));
        }
    },
    
    parseMapTip: function(xhr) {
        var o;
        var tooltip = Fusion.parseJSON(xhr.responseText);
        this.oMaptip = {t:"",h:""};
        var t = tooltip['FeatureInformation']['Tooltip'];
        if (t) {
          this.oMaptip.t = t[0].replace(/\\n/g, "<br>");
        }
        var h = tooltip['FeatureInformation']['Hyperlink'];
        if (h) {
          this.oMaptip.h = h[0];
        }
        this.mapWidget.triggerEvent(Fusion.Event.MAP_MAPTIP_REQ_FINISHED, this.oMaptip);
    },
    
    getLegendImageURL: function(fScale, layer, style,defaultIcon) {
        if(layer.layerTypes[0] == 4){
            return defaultIcon;
        }
        else
        {
            if (style.iconOpt && style.iconOpt.url)
            {
                //if (style.iconOpt.url.indexOf("data:image") >= 0)
                //    console.log("Fetching pre-cached icon");
                return style.iconOpt.url;
            }
                
            var origUrl = Fusion.getConfigurationItem('mapguide', 'mapAgentUrl');
            var altUrl = null;
            if (this.oLayerOL && this.oLayerOL.alternateUrls && this.oLayerOL.alternateUrls.length > 0) {
                altUrl = this.oLayerOL.getNextAltURL();
            }
            var url = (altUrl == null) ? origUrl : altUrl;
            
            url += "?OPERATION=GETLEGENDIMAGE&SESSION=" + layer.oMap.getSessionID();
            url += "&VERSION=1.0.0&SCALE=" + fScale;
            op = /\(/g; cp = /\)/g; 
            url += "&LAYERDEFINITION=" + encodeURIComponent(layer.resourceId).replace(op, "%28").replace(cp, "%29");
            url += "&THEMECATEGORY=" + style.categoryIndex;
            url += "&TYPE=" + style.geometryType;
            url += "&CLIENTAGENT=" + encodeURIComponent(this.clientAgent);
            if (this.noCache) {
                url += "&TS=" + (new Date()).getTime();
            }
            return url;
        }
    },
    
    typeNameToValue: function(name) {
        switch(name) { //Values from MgPropertyType
            case "byte":
                return 2;
            case "boolean":
                return 1;
            case "blob":
                return 10;
            case "clob":
                return 11;
            case "datetime":
                return 3;
            case "double":
                return 5;
            case "int16":
                return 6;
            case "int32":
                return 7;
            case "int64":
                return 8;
            case "single":
                return 4;
            case "string":
                return 9;
        }
        return -1;
    },
    
    convertExtendedFeatureInfo: function(efi) {
        var bHasSelection = false;
        var result = {};
        var layerNames = [];
        var featuresByLayer = {};
        if (efi.FeatureInformation.SelectedFeatures) {
            var selLayers = efi.FeatureInformation.SelectedFeatures[0].SelectedLayer;
            bHasSelection = (selLayers.length > 0);
            var box = new OpenLayers.Bounds();
            
            for (var i = 0; i < selLayers.length; i++) {
                var selLayer = selLayers[i];
                var layerName = selLayer["@name"];
                if (!result[layerName]) {
                    if (selLayer.LayerMetadata) {
                        var layerMeta = selLayer.LayerMetadata[0];
                        var pnames = [];
                        var ptypes = [];
                        var pvals = [];
                        for (var j = 0; j < layerMeta.Property.length; j++) {
                            var metaProp = layerMeta.Property[j];
                            pnames.push(metaProp.Name[0]);
                            ptypes.push(metaProp.Type[0]);
                            pvals.push(metaProp.DisplayName[0]);
                        }
                    }
                    result[layerName] = {
                        metadata: [],  //NOTE: Probably a defect, but regular code path is putting blank string arrays here too
                        metadatanames: ["dimension", "bbox", "center", "area", "length"],
                        numelements: selLayer.Feature.length,
                        propertynames: pnames,
                        propertytypes: ptypes,
                        propertyvalues: pvals,
                        values: []
                    };
                    layerNames.push(layerName);
                }
                
                for (var j = 0; j < selLayer.Feature.length; j++) {
                    var feat = selLayer.Feature[j];
                    var featVals = [];
                    if (feat.Property) {
                        for (var k = 0; k < feat.Property.length; k++) {
                            //Fusion represents null as empty string. Don't think that's right but we'll run with whatever
                            //the old code path produces
                            featVals.push(feat.Property[k].Value == null ? "" : feat.Property[k].Value[0]);
                        }
                    }
                    result[layerName].values.push(featVals);
                    //NOTE: Probably a defect, but regular code path is putting blank string arrays here too, so let's do the same
                    result[layerName].metadata.push(["","","","",""]);
                    if (feat.Bounds) {
                        var bounds = feat.Bounds[0].split(" "); //minx miny maxx maxy
                        box.extend(new OpenLayers.LonLat(parseFloat(bounds[0]), parseFloat(bounds[1])));
                        box.extend(new OpenLayers.LonLat(parseFloat(bounds[2]), parseFloat(bounds[3])));
                    }
                }
            }
            return OpenLayers.Util.extend(result, {
                hasSelection: bHasSelection,
                extents: {
                    minx: box.left,
                    miny: box.bottom,
                    maxx: box.right,
                    maxy: box.top
                },
                layers: layerNames
            });
        } else {
            result.hasSelection = false;
            return result;
        }
    },

    processAndMergeExtendedFeatureInfo: function(r) {
        this.processSelectedExtendedFeatureInfo(r, true);
        //this.processSelectedFeatureInfo(r, true);
    },
    
    processExtendedFeatureInfo: function(r) {
        this.processSelectedExtendedFeatureInfo(r, false);
        //this.processSelectedFeatureInfo(r, false);
    },
    
    processSelectedExtendedFeatureInfo: function(r, mergeSelection) {
        var o = Fusion.parseJSON(r.responseText);
        var sel = new Fusion.SimpleSelectionObject(o);
        var attributes = this.convertExtendedFeatureInfo(o);
        var selText = sel.getSelectionXml();
        if (mergeSelection == true)
        {
            sel.merge(this.previousSelection);
        }
        this.previousSelection = sel;
        //Because the QUERYMAPFEATURES 2.6.0 response contains mostly everything we need, we cut down
        //on lots of async request ping-pong. So we can just update the selection image and notify any
        //interested parties of the new selection attributes
        if (selText != "" && selText != null) {
            this.previousAttributes = attributes;
            this.updateMapSelection(selText, false);
            this.processSelectedFeaturePropertiesNode(attributes);
        } else {
            this.previousAttributes = null;
            this.clearSelection();
        }
        this.mapWidget._removeWorker();
    },
    
    processAndMergeFeatureInfo: function (r) {
        this.processSelectedFeatureInfo(r, true);
    },

    processFeatureInfo: function (r) {
        this.processSelectedFeatureInfo(r, false);
    },

    processSelectedFeatureInfo: function (r, mergeSelection) {
        var o = Fusion.parseJSON(r.responseText);

        var newSelection = new Fusion.SimpleSelectionObject(o);
        if(mergeSelection == true)
        {
            newSelection.merge(this.previousSelection);
        }
        this.previousSelection = newSelection;

        var selText = newSelection.getSelectionXml();
        window.selText = selText
        this.setSelection(selText, false);
        this.mapWidget._removeWorker();
    }

});


		
	}
})
})
