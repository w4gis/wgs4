define("wgs4/model/theme", ['dojo/request/xhr', "dojo/DeferredList", "dojo/_base/Deferred"], function(xhr, DeferredList, Deferred){
	function ThemeModel() {
	
	};
	
	ThemeModel.prototype.get = function(callback){
		xhr.post("/map/themes/list", {
			handleAs: "json",	
			data: {}
		}).then(function(result){
			callback(null, result.records);
		}, function(error){
			callback(error);
		});
		
	};
	
	ThemeModel.prototype.add = function(themeName, isThemeShared, isThemeArhive, callback){
		//TODO - запрос не работает
		xhr.post("/map/themes/create", {
			handleAs: "json",	
			data: {
				object: "themes",
				params: JSON.stringify({
					"NAME": themeName,
					"SHARED": isThemeShared,
					"IS_ARCHIVE": isThemeArhive
				})
			}
		}).then(function(result){
			callback(null, result);
		}, function(error){
			callback(error);
		});
		
	};
	
	ThemeModel.prototype.showTheme = function(themeId, callback){
		var sessionId, mapName, map;
		var allMaps = Fusion.getWidgetById('Map').getAllMaps();
		for(var j=0; j<allMaps.length; j++) {
			if (allMaps[j].arch == 'MapGuide') {
				sessionId = allMaps[j].getSessionID();
				mapName = allMaps[j].getMapName();
				map = allMaps[j];
			};	
		};
		xhr.post("/map/themes/displaytheme", {
			handleAs: "json",	
			data:{
				'THEME_ID': themeId, 
				'mapname': mapName, 
				'session': sessionId 
			}
		}).then(function(result){
				//TODO: Сделать без reloadMap
				map.reloadMap()
				callback(null);
			}, function(error){
				callback(error);
			}
		);
	};

	ThemeModel.prototype.deleteTheme = function(themeId, callback){
		xhr.post("/map/themes/delete", {
			handleAs: "json",	
			data:{
				'THEME_ID': themeId
			}
		}).then(function(result){
				callback(null);
				console.log(result);
			}, function(error){
				callback(error);
			}
		);
	};

	ThemeModel.prototype.getThemeLayers = function(themeId, callback){
		xhr.post("/map/themes/themelayerlist", {
			handleAs: "json",	
			data:{
				'themeId': themeId
			}
		}).then(function(result){
				callback(null, result.records);
			}, function(error){
				callback(error);
			}
		);
	};



	ThemeModel.prototype.getThemeLayersParams = function(themeId, callback){
		var d1 = getThemeLayers(themeId);
		var d2 = getFeatureLayers();
		var d3 = getObjectTypes();

		var dl = new DeferredList([d1,d2,d3]);
		dl.then(function(result){
			var themeLayersParams = {};
			themeLayersParams.themeLayers = result[0][1].records;
			themeLayersParams.featureLayers = result[1][1];
			themeLayersParams.objectTypes = result[2][1];
			themeLayersParams.themeId = themeId;
			console.log(themeLayersParams);
			callback(null, themeLayersParams);
		});
	};
	
	function getThemeLayers(themeId) {
		var d = new Deferred();
		xhr.post("/map/themes/themelayerlist", {
				handleAs: "json",	
				data:{
					'themeId': themeId
				}
		}).then(function(result){
			d.resolve(result);
		});
		return d;
	};

	function getFeatureLayers() {
		var d = new Deferred();
		xhr.get("/objectadmin/getfeaturelayerlist", {
			handleAs: "json"
		}).then(function(result){
			d.resolve(result);
		});
		return d;
	};

	function getObjectTypes() {
		var d = new Deferred();
		xhr.get("/objectadmin/getattrtypelist", {
			handleAs: "json"
		}).then(function(result){
			d.resolve(result);
		});
		return d;
	};

	return new ThemeModel();
})

