define("wgs4/model/cardModel", ['dojo/request/xhr', 'dojo/Deferred'], function(xhr, Deferred) {
    "use strict";

    function CardModel() {

    }

    CardModel.prototype.get = function(featureIds) {
        var self = this;
        var deferred = new Deferred();
        this.getNative(featureIds).then(
        //success
        function(cardDescriptions) {
            console.log(cardDescriptions);

            if (featureIds.length > 1) deferred.resolve(self.getParsedMultipleCardDescription(cardDescriptions));
            else deferred.resolve(self.getParsedCardDescription(cardDescriptions));
        },
        //error
        function(error) {
            console.log("Exception: server error. ", error);
            deferred.reject(error);
        });
        return deferred;
    };

    CardModel.prototype.save = function(card) {

    };

    CardModel.prototype.getParsedCardDescription = function(cardDescriptions) {
        var self = this;
        var result = [];
        cardDescriptions[1].forEach(function(description, descriptionId) {
            /*var isChild = false;
            for (var relationIndex = 0; relationIndex < cardDescriptions[3][descriptionId].length; relationIndex++) {
                for (var relationLinkIndex = 0; relationLinkIndex < cardDescriptions[3][descriptionId][relationIndex].length; relationLinkIndex++) {
                    if (description.id == cardDescriptions[3][descriptionId][relationIndex][relationLinkIndex].id) {
                        isChild = true;
                        break;
                    }
                }
            }
            if (isChild) {
                return;
            }*/
            result.push(description);
            var currentCard = result[result.length - 1];
            currentCard.isCurrent = cardDescriptions[0][descriptionId].cardIsCurrent;
            currentCard.propertyGroups = [];
            if (cardDescriptions[2][descriptionId]) {
                cardDescriptions[2][descriptionId].forEach(function(property, propertyId) {
                    self._addPropertyIntoGroup(currentCard, property)
                });
            }
            currentCard.relations = [];
            cardDescriptions[3][descriptionId].forEach(function(relation, relationIndex) {
                currentCard.relations[relation.relationId] = relation;

            });
            /*currentCard.relations = {
                countTypeRelation: cardDescriptions[3][descriptionId].countTypeRelation,
                name: cardDescriptions[3][descriptionId].name,
                relationId: cardDescriptions[3][descriptionId].relationId,
                relationTypeId: cardDescriptions[3][descriptionId].relationTypeId,
                links: []
            };
            var links = result[result.length - 1].relation.links;
            cardDescriptions[4][descriptionId].forEach(function(relation, relationIndex) {
                links[relation.objRelationId] = cardDescriptions[4][descriptionId][relationIndex];
            });*/
        });

        return result;
    };

    CardModel.prototype._addPropertyIntoGroup = function(currentCard, property) {
        if (!currentCard.propertyGroups[property.groupId]) {
            currentCard.propertyGroups[property.groupId] = {
                properties: [],
                name: property.groupName
            };
        }


        var group = currentCard.propertyGroups[property.groupId];
        group.properties[property.id] = {};
        var currentProperty = group.properties[property.id];
        currentProperty.name = property.name;
        currentProperty.valueTypeId = property.valueTypeId;
        currentProperty.value = property.value || "";
        currentProperty.domains = [];
        property.domain.forEach(function(domain, domainIndex) {
            currentProperty.domains[domain.Value_Domain_ID] = {
                value: domain.Value_Domain
            };
        });
    };


    CardModel.prototype.getParsedMultipleCardDescription = function(cardDescriptions) {
        var self = this;
        var result = [];
        cardDescriptions[1].forEach(function(cardBaseProperty, cardBasePropertyIndex) {
            console.log(cardBasePropertyIndex + 1);
            var currentCard = {};
            currentCard = cardBaseProperty;
            console.log(currentCard.name + "; " + cardBaseProperty.name);
            currentCard.featureLayerId = cardDescriptions[0][cardBasePropertyIndex + 1].featureLayerId;
            currentCard.typeName = cardDescriptions[0][cardBasePropertyIndex + 1].label;
            currentCard.objectTypeId = cardDescriptions[0][cardBasePropertyIndex + 1].objectTypeId;
            currentCard.propertyGroups = [];
            cardDescriptions[2][cardBasePropertyIndex].forEach(function(property, propertyIndex) {
                if (!currentCard.propertyGroups[property.groupId]) {
                    currentCard.propertyGroups[property.groupId] = {
                        name: property.groupName,
                        properties: []
                    };
                }
                var currentGroup = currentCard.propertyGroups[property.groupId];
                currentGroup.properties[property.id] = {
                    name: property.name,
                    valueTypeId: property.valueTypeId
                };
                cardDescriptions[3][cardBasePropertyIndex].forEach(function(propertyValue, propertyValueIndex) {
                    if (property.id == propertyValue.propertyId) currentGroup.properties[property.id].value = propertyValue.propertyValue;
                });
            });
            result[cardBasePropertyIndex] = currentCard;
        });
        return result;
    }

    CardModel.prototype.getNative = function(featureIds) {
        return xhr.post("/objectadmin/card/getcardsbyfeatureid", {
            // sync: true,
            handleAs: "json",
            data: {
                'WgsVersion': 4,
                'featureIds': JSON.stringify(featureIds)
            }
        });
    };

    return CardModel;
});