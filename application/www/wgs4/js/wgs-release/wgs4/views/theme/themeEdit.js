 define("wgs4/views/theme/themeEdit", ["dijit/Dialog"], function(Dialog){
  function ThemeEdit() {
    this.themeEditDialog = null;
     
  }
   
  ThemeEdit.prototype.showThemeEditDialog = function(callback) {
    // TODO: Ajax dialog
    if (this.themeEditDialog) {
      this.themeEditDialog.destroyRecursive();
    }
    this.themeEditDialog = new Dialog({
      title: "Отчет",
      content:  "<div> "+
                  "<label> Name" +
                    "<input name='theme-name' placeholder='Name' id='theme-name'/>" +
                  "</label>" +
                  "<button id='theme-edit-submit-button'>ok</button>" +
                "</div>",
      'class': "theme-edit-window"
    });
    this.themeEditDialog.show();
    callback();
  };


  
  ThemeEdit.prototype.hideThemeEditDialog = function() {
    this.themeEditDialog.hide();
  };
  
  return ThemeEdit;
 })
 
 
 