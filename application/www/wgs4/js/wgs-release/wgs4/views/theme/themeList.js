require({cache:{
'url:wgs4/views/theme/theme.ejs':" <div class=\"theme-window-control-pane\">\r\n  <span data-action=\"new\" class=\"theme-list-item\">Добавить новый отчет</span>\r\n\t<span data-action=\"refresh\" class=\"theme-list-item\">Обновить</span>\r\n </div>\r\n <hr>\r\n <div class=\"theme-list-wrapper\">\r\n\t<% for (var i =0; i < themeList.length; i++) {%>\r\n\t\t<div class=\"theme-list-record\">\r\n\t\t\t<span><%= themeList[i].NAME ? themeList[i].NAME : \"Без наименования\"%></span>\r\n      <span data-action=\"show\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"> Отобразить </span>\r\n\t\t\t<span data-action=\"edit\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"> Редактировать </span>\r\n      <span data-action=\"delete\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"> Удалить </span>\r\n      <span data-action=\"setup\" data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-item\"> Настроить </span>\r\n\t\t</div>\r\n\t<% } %>\r\n </div>"}});
 define("wgs4/views/theme/themeList", ["dijit/Dialog", "dojo/text!wgs4/views/theme/theme.ejs"], function(Dialog, themeEjs){
  function ThemeList() {
     
  }
   
  ThemeList.prototype.showThemeListDialog = function(themeList, callback) {
    // TODO: Ajax dialog

    console.log('themeListDialog before destroy ', this.themeListDialog);
    if (this.themeListDialog) {
      this.themeListDialog.destroyRecursive();
      console.log('destroyed');
    }
    console.log('themeListDialog after destroy ', this.themeListDialog);
    this.themeListDialog = new Dialog({
      title: "Тематические отчеты",
      content: window.ejs.render(themeEjs, {themeList: themeList}),
      'class': "theme-list-window"
    });
    console.log('themeListDialog after create new ', this.themeListDialog);

    callback();
    this.themeListDialog.show();

  };


  
  ThemeList.prototype.hideThemeListDialog = function() {
    this.themeListDialog.hide();
  };
  
  return ThemeList;
 })
 
 
 