require({cache:{
'url:wgs4/views/theme/themeSetup.ejs':"<div class=\"theme-layers-control-pane\">\r\n  <span data-action=\"new\" class=\"theme-list-item\">Добавить новый слой</span>\r\n  <span data-action=\"refresh\" data-theme-id=\"<%= themeId %>\" class=\"theme-list-item\">Обновить</span>\r\n</div>\r\n<hr>\r\n<div class=\"theme-layers-list-wrapper\">\r\n  <% for (var i=0; i<themeLayers.length; i++) { %>\r\n    <div class=\"theme-layer-wrapper\">\r\n      <!-- <span><%= themeLayers[i].name %></span> -->\r\n      <input name=\"theme-layer-<%= themeLayers[i].id %>\" value=\"<%= themeLayers[i].name %>\" />\r\n      <select name=\"theme-layer-feature-layer\" data-theme-layer=\"<%= themeLayers[i].id %>\">\r\n        <% for (var j=0; j<featureLayers.length; j++) {%>\r\n          <option <%= (+featureLayers[j].id == + themeLayers[i].featureLayer) ? \"selected\" : \"\" %> > <%= featureLayers[j].name %></option>\r\n        <% } %>\r\n      </select>\r\n      <select name=\"theme-layer-object-type\" data-theme-layer=\"<%= themeLayers[i].id %>\">\r\n        <% for (var j=0; j<objectTypes.length; j++) {%>\r\n          <option <%= (+objectTypes[j].id == + themeLayers[i].featureType) ? \"selected\" : \"\" %> > <%= objectTypes[j].name %></option>\r\n        <% } %>\r\n      </select>\r\n      <select name=\"theme-layer-feature-type\" data-theme-layer=\"<%= themeLayers[i].id %>\">\r\n        <option value=\"1\">Точка</option>\r\n        <option value=\"2\">Линия</option>\r\n        <option value=\"3\" selected>Полигон</option>\r\n      </select>\r\n      <div class=\"theme-layer-control-pane\">\r\n        <span data-action=\"conditions\">Условия</span>\r\n        <span data-action=\"style\">Стили</span>\r\n      </div>\r\n    </div>\r\n  <% } %>\r\n  <span>lorem</span> \r\n</div>"}});
 define("wgs4/views/theme/themeSetup", ["dijit/Dialog", "dojo/text!wgs4/views/theme/themeSetup.ejs"], function(Dialog, themeSetupEjs){
  function ThemeSetup() {
    this.themeSetupDialog = null;
     
  }
   
  ThemeSetup.prototype.showThemeSetupDialog = function(themeLayersParams, callback) {
    // TODO: Ajax dialog
    if (this.themeSetupDialog) {
      this.themeSetupDialog.destroyRecursive();
    }
    this.themeSetupDialog = new Dialog({
      title: "Настройка отчета",
      content: window.ejs.render(themeSetupEjs, {themeLayers: themeLayersParams.themeLayers, featureLayers: themeLayersParams.featureLayers, objectTypes: themeLayersParams.objectTypes, themeId: themeLayersParams.themeId}),
      'class': "theme-setup-window"
    });
    this.themeSetupDialog.show();
    callback();
  };


  
  ThemeSetup.prototype.hideThemeSetupDialog = function() {
    this.themeSetupDialog.hide();
  };
  
  return ThemeSetup;
 })
 
 
 