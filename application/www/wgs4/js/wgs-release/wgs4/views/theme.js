require({cache:{
'url:wgs4/views/theme.ejs':" <div class=\"theme-window-control-pane\">\r\n\t<a href=\"#\" id=\"theme-add-new-button\">Добавить новый отчет</a>\r\n </div>\r\n <div class=\"theme-list-wrapper\">\r\n\t<% for (var i =0; i < themeList.length; i++) {%>\r\n\t\t<div class=\"theme-list-record\">\r\n\t\t\t<span><%= themeList[i].NAME ? themeList[i].NAME : \"Без наименования\"%></span>\r\n\t\t\t<span data-theme-id=\"<%= themeList[i].THEME_ID ? themeList[i].THEME_ID : 0 %>\" class=\"theme-list-show-button\"> Отобразить </span>\r\n\t\t</div>\r\n\t<% } %>\r\n </div>",
'url:wgs4/views/themeEdit.ejs':"<div> \r\n\t<label> Name\r\n\t\t<input name=\"theme-name\" placeholder=\"Name\" id=\"theme-name\"/>\r\n\t</label>\r\n\t<button id=\"theme-edit-submit-button\">ok</button>\r\n</div>"}});
 define("wgs4/views/theme", ["dijit/Dialog", "dojo/text!wgs4/views/theme.ejs", "dojo/text!wgs4/views/themeEdit.ejs"], function(Dialog, themeEjs, themeEditEjs){
	function ThemeView() {
		 
	}
	 
	ThemeView.prototype.showThemeListDialog = function(themeList, callback) {
		// TODO: Ajax dialog
		console.log(themeList, callback);
		this.themeListDialog = new Dialog({
			title: "Тематические отчеты",
			content: window.ejs.render(themeEjs, {themeList: themeList}),
			'class': "theme-list-window"
		});
		this.themeListDialog.show();
		callback();
	};
	
	ThemeView.prototype.showThemeEditDialog = function(callback) {
		// TODO: Ajax dialog
		if (!this.themeEditDialog) {
		this.themeEditDialog = new Dialog({
			title: "Тематический отчет :: Параметры",
			content: window.ejs.render(themeEditEjs),
			'class': "theme-list-window"
		});
		};
		this.themeEditDialog.show();
		callback();
	};
	
	ThemeView.prototype.hideThemeListDialog = function() {
		this.themeListDialog.hide();
	};
	
	ThemeView.prototype.hideThemeEditDialog = function() {
		this.themeEditDialog.hide();
	};
	
	return new ThemeView;
 })
 
 
 