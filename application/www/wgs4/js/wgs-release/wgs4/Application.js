// we use 'define' and not 'require' to workaround Dojo build system
// limitation that prevents from making of this file a layer if it
// using 'require'
define("wgs4/Application", ['wgs4/model/wgs-app'],

function(WgsApp) {
    
    if (!window.WGS) {
        window.WGS = {
            FLAGS: {}
        };
    }

    var app = new WgsApp(window.WGS);

    //AppData fallback for prev. versions of WGS
    if (!window.AppData) {
        window.AppData = app.options;
    }
});