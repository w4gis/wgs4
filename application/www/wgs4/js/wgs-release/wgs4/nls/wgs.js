define("wgs4/nls/wgs", {
	root: {
		coordSystems : 'CS',
		coordSystemsTitle: 'Coordinate systems',
		menu	: 'Menu',
		map		: 'Map',		
		exit	: 'Log out',		
		featureInfo : 'Geometry info',
		legend	: 'Layers',
		print	: 'Print map',	
		redline: 'Draw new features',
		redlineTitle: 'Drawing',
		search	: 'Search',
		titleLayers : 'Layers',
		RelationTitle: 'Связи',
		tasks	: 'Tasks',
		cardB : 'Card',
		taskPaneTitle	: 'Card of the object',
		title : 'WGS4 :: EMP GIS',
		refreshGeometryInfo : 'Get geometry info',
		
		card: 'Card',
		legend: 'Legend',
		tasks: 'Tasks',
		exit: 'Exit',
		close: 'Close',
		settings: 'Settigs'
		
	},
	'ru-ru': true
});
