define("wgs4/nls/taskpane", {
	root: {
		legend	: 'Legend',
		tasks	: 'Tasks',
		map		: 'Map',
		oldMap	: 'WGS3',
		back	: 'Map',
		
		measure	: 'Measurement tools',
		analysys : 'Analysys tools',
		administration : 'Administration',
		editorTitle : 'Edit data',
		desktopOnly : 'desktop',
		projectadmin: 'Project management',
		objectadmin: 'Data management',
		
		featureInfo : 'Geometry info',
		legend	: 'Layers',		
		redline: 'Edit spatial data',
		
		title	: 'Choose task',
		themes	: 'Thematic reports',
		installApplication: 'Install WGS4 as application',
		firefoxOnly : 'Firefox only'
	},
	'ru-ru': true
});
