define("wgs4/nls/ru-ru/wgs", {
    exit	: 'Выход',		
		
	map		: "Карта",
	menu	: "Меню",
	legend	: "Слои",
	search	: 'Поиск',
	tasks	: 'Задачи',
		
	titleLayers : 'Слои',
	title: 'WGS4 :: ГИС ЭГП',
		
	card: 'Карточка',
	legend: 'Слои',
	tasks: 'Задачи',
	exit: 'Выход',
	close: 'Закрыть',
	settings: 'Настройки'
});
