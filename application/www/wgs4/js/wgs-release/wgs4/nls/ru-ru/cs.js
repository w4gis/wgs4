define("wgs4/nls/ru-ru/cs", {
	back	: 'Геоданные',
	legend	: 'Легенда',
	tasks	: 'Инструменты',
	map		: 'Карта',
	selectCs: 'Выбор КС',
	availableCs: 'Координатные системы',
		
	title	: 'Выбор КС',
		
	mercator: 'Pseudo Mercator',
	pz		: 'Параметры Земли - 90',
	pu		: 'Пулково - 42',
	wgs		: 'WGS - 84'
});
