define("wgs4/nls/map", {
	root: {
		title	: "Map",
		map		: "Map",
		legend	: "Layers",
		exit	: "Log out",
		redlineTitle: 'Drawing',		
		redline	: 'Draw new featuers',
		
		clearSelection: 'Clear<br/>selection',
		myLocation: 'My<br/>location',
		initialView: 'Initial<br/>view',
		switchBasemap: 'Select<br/>basemap',
		prevView: 'Previous<br/>view',
		nextView: 'Next<br/>view',
		
		enableOL: 'Enable<br/>OpenLayers',
		disableOL: 'Disable<br/>OpenLayers',
		reload: 'Restart application',
		showMap: 'to map',
		hideMap: 'hide map'
	},
	'ru-ru': true
});
