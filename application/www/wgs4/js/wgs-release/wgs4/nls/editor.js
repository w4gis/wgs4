define("wgs4/nls/editor", {
	root: {
		title	: "Editor",
		map		: "Map",
		legend	: "Layers",
		exit	: "Log out",
		redlineTitle: 'Drawing',		
		redline: 'Draw new features',
		taskPaneTitle	: 'Карточка объекта',
		back		: 'All tasks',
		backToEm	: 'Back',
		layers: 'Editable layers',
		help: 'Select one of availabel layers to start editing',
		helpTitle: 'Edit spatial data',
		
		insertFeature: 'Insert features',
		drawFeature: 'Draw features',
		getLocation : 'Get my location',
		point : 'Add point',
		line : 'Add line',
		drawPoint: 'Draw point',
		drawLine: 'Draw line',
		drawPoly: 'Draw polygone',
		removeFeature: 'Remove features',
		deleteBinded: 'Delete binded cards?',
		deleteSelected: 'Delete selected feature',
		showMap: 'to map',
		hideMap: 'hide map'
	},
	'ru-ru': true
});
