define("wgs4/nls/finfo", {
	root: {
		length	: 'Length',
		length2	: 'Length',
		area	: 'Area',
		coords	: 'Coordinates',
		coordSystem	: 'Coordinate system',	
		objects	: 'Selected objects',
		change	: 'Change',
		
		Line	: 'Lines',
		Polygon	: 'Polygons',
		Point	: 'Points',
		
		back	: 'All tasks',
		backToFm	: 'Back',
		legend	: 'Legend',
		tasks	: 'Tasks',
		map		: 'Map',
		menu	: 'CS',
		title	: 'Geometry info',
		
		selected: 'Selected objects',
		mercator: 'Pseudo Mercator',
		pz		: 'Earth Parameters-90',
		pu		: 'Pulkovo-42',
		wgs		: 'WGS-84',
		showMap: 'to map',
		hideMap: 'hide map',
		featuresSelected1: 'features selected',
		featuresSelected2: 'features selected',
		featuresSelectedMany: 'features selected',
		totalArea: 'Total area',
		totalLength: 'Total length',
		m: 'm',
		help: 'Select objects on map to view their geometric properties',
		helpTitle:'Geometry info',
		emptySelection: 'Select ovjects on map to view their geometry properties'
		
	},
	'ru-ru': true
});
