define("wgs4/nls/settings", {
	root: {
		title	: "Settings",
		back	: "Map",
		language: "Language",
		personal: "Personalization"
	},
	'ru-ru': true
});
