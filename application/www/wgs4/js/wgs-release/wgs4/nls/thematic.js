define("wgs4/nls/thematic", {
	root: {
		title	: "Thematic reports",
		back	: "Tasks",
		map		: "Map",
		legend	: "Layers",
		exit	: "Log out",
		redlineTitle: 'Drawing',		
		redline: 'Draw new featuers'
	},
	'ru-ru': true
});
