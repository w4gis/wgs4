require({cache:{
'url:wgs4/views/relation.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"RelationHeading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\">\r\n\t\t\t\t\r\n\t\t<button data-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n            data-dojo-attach-point=\"backButton\"\r\n            data-dojo-props=\"arrow: 'left', target: 'leftView,card', transitionDir: -1\"\r\n            style=\"float:left\"\r\n            class=\"backButton\">Назад</button>\r\n\t\t\t\r\n\t\t\t<div id = \"RelationTitle\" class=\"mblHeadingDivTitle\" role=\"heading\" aria-level=\"1\">Связи\r\n\t\t\t</div>\r\n\t\t</span>\r\n\t</div>\r\n\t\r\n\t<div style=\"top:2px;left:0;bottom:0px;position:relative;right:0;background-color:white;\"\r\n\t\t data-dojo-type=\"wgs4/wheelScrollableView\"\r\n\t\t data-dojo-attach-point=\"RelationDisplayContainer\" id=\"RelationDisplayContainer\">\r\n\t\t \r\n\t\t \r\n\t</div>\r\n</div>\r\n"}});
define("wgs4/controllers/relation", [
		'dojo/query', 'dojo/data/ObjectStore',
		'dojo/dom-attr', 'dojo/dom',
		'dojo/dom-style', 'dijit/form/Select',
		'dojo/dom-class', 'dojox/mobile/TextBox',
		'dojo/dom-construct', 'dojox/mobile/ListItem',
		'dojo/sniff', 'dojox/mobile/TextArea',
		'dojo/on', 'dojox/mobile/Button',
		'dojo/request/xhr',
		'dojo/_base/array',
		'dijit/registry',
		'dojo/store/Memory','dojo/text!wgs4/views/relation.html'
	], function (query, ObjectStore, domAttr, dom, domStyle, Select, domClass, TextBox, domConstruct, ListItem,
		has, TextArea, on, Button, xhr, arrayUtil, registry, Memory) {
	return {
		//init method called when view created
		//beforeactivate called before transition into the view...
		_hasMenu : false,

		init : function () {
			new window.AppData.ViewBaseClass( this );	
		},
		_beforeActivate : function () {
			var self = this;
			on(dom.byId('BackRelationButton'), 'click', function () {
				self.app.transitionToView(self.domNode, {
					target : 'card'
				})
			})

		},
		afterActivate : function () {

			var self = this;
			var ObjectTypeId;
			var relChildId;
			var ids = [];
			var objTypeRelationIds;
			console.log(this.params.first);
			var relId = this.params.relationId;
			var cardId = this.params.cardId;
			if (this.params.first === '00') {
				xhr.post("/objectadmin/card/GetListChildObjectTypeByRelation", {
					handleAs : "json",
					data : {
						'relationId' : relId
					}
				}).then(function (data0) {
					console.log(data0);
					var dataForDomain = [];

					for (var i = 0; i < data0.length; i++) {
						ids[i] = {
							objTypeId : data0[i].id,
							objTypeRelationId : data0[i].objTypeRelationId
						};
						dataForDomain[i] = {
							id : "relationsChild" + data0[i].id,
							label : data0[i].name
						};

					}
					console.log(dataForDomain);
					domConstruct.create('div', {
						'id' : "divForRelationAdd" + relId,
						'class' : 'relationDiv'
					}, "targetForComboBox");

					var mmry = new Memory({
							data : dataForDomain
						});

					var os = new ObjectStore({
							objectStore : mmry
						});
					var newId = 'divForRelationAdd' + relId;
					var s = new Select({
							store : os,
							'class' : 'RelationComboBox',
						}, newId);
					s.startup();
					s.set("value", "");
					s.on("change", function () {
						console.log("тык " + this.get("value"));
						var relid = this.get("value");
						relChildId = relid.substr(relid.indexOf('d') + 1);
						console.log("relsId " + relChildId);
						for (var i2 = 0; i2 < ids.length; i2++) {
							if (ids[i2].objTypeId == relChildId)
								objTypeRelationIds = ids[i2].objTypeRelationId;
						}
						console.log("rels " + objTypeRelationIds)
					})
				});

				var textIn = 'Если вы уверены, что хотите создать атрибутивный объект и связать его с данным, выберите атрибутивный тип и нажмите "Создать"';
				domConstruct.create('div', {
					'id' : 'targetForText',
					'class' : "relationText"
				}, 'RelationDisplayContainer');
				domConstruct.create('div', {
					'id' : 'targetForComboBox'
				}, 'RelationDisplayContainer');
				dom.byId('targetForText').innerHTML = textIn;

				var CreateButton = new Button({
				 
						'id' : 'CreateRelation',
						'label' : 'Создать',
						'class' : 'ButtonCard',
						'onClick' : function () {
							xhr.post("/objectadmin/card/CreateChildObject", {
								handleAs : "json",
								data : {
									'objTypeRelationId' : objTypeRelationIds,
									'objectTypeId' : relChildId,
									'parentObjectId' : cardId,
								}
							}).then(function (data1) {
								console.log(data1)
								self.app.transitionToView(self.domNode,
								{target : 'card', params : {newCardId: data1.childObjectId}})
									})
							}
						}, domConstruct.create('div', {}, 'RelationDisplayContainer'));

			} else {
				domConstruct.create('div', {
					'id' : 'targetForComboBox'
				}, 'RelationDisplayContainer');
				domConstruct.create('div', {
					'id' : "divForTextBoxProperty",
					'class' : 'cardDiv'
				}, "targetForComboBox");

				var newWidget = new TextBox({
						'id' : 'TextBox',
						'value' : 'Введите название связи',
					}, domConstruct.create('input', {}, "divForTextBoxProperty"));
			}

		},

		afterDeactivate : function () {
			formWidgets = registry.findWidgets(dom.byId('RelationDisplayContainer'));
			if (formWidgets.length > 0) {
				console.log('l: ' + formWidgets.length);
				for (var i = 0; i < formWidgets.length; i++) {
					if (formWidgets[i]) {
						console.log(formWidgets[i].toString() + ' exists');
						formWidgets[i].destroyRecursive(true);
					}
				}
				domConstruct.empty('RelationDisplayContainer');
			}

		}
	}
})
