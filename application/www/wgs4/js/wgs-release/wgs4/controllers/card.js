define("wgs4/controllers/card", ['wgs4/model/cardModel', 'wgs4/controllers/cardView'], function(CardModel, CardView) {
    "use strict";

    function Card(_featureIds) {
        this.featureIds = _featureIds;
        this.model = new CardModel();
    }

    Card.prototype.show = function() {
        this.get().then(
            //succeed
        function(cardDescriptions) {
            console.log(cardDescriptions);
            var view = new CardView(cardDescriptions);
            view.show();
            //this.registerEventHandlers(cardDescriptions);
        },
        //failed
        function(error) {
            alert(error);
        });
    };

    Card.prototype.get = function() {
        return this.model.get(this.featureIds);
    };

    Card.prototype.save = function(card) {

    };

    Card.prototype.registerEventHandlers = function(cardDescriptions) {
        cardDescriptions.forEach (function(card, cardId) {
            card.relations.forEach (function(relation, relationId) {
                relation.linkedCards.forEach (function(childCard, childCardId){

                });
            });
        });
    };

    Card.prototype.hide = function() {

    };

    return Card;
});