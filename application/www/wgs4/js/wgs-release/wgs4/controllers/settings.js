require({cache:{
'url:wgs4/views/settings.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-type=\"wgs4/wheelScrollableView\" data-dojo-props=\"height:'auto'\"\r\n\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\tstyle=\"top:44px;left:0;bottom:0;position:absolute;right:0;\"\r\n\t\t>\r\n\t\t<!--\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'Geometry info'\"></div>\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRect\" data-dojo-props=\"shadow:true\">\r\n\t\t\tSelect objects on map to view their geometric properties\r\n\t\t</div>\r\n\t\t-->\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'${nls.personal}'\"></div>\r\n\t\t<ul data-dojo-type=\"dojox/mobile/RoundRectList\">\r\n\t\t\t<div \r\n\t\t\t\tdata-dojo-attach-point=\"welcome\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\" \r\n\t\t\t\tdata-dojo-props=\"target:'leftView,language', clickable:true, label: '${nls.language}'\"></div>\r\n\t\t</ul>\r\n\t</div>\t\t\t\r\n</div>\r\n"}});
define("wgs4/controllers/settings", [
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',"dojo/on","dojo/dom-style","dojo/query","dojo/_base/array","dojox/mobile/Pane",'dojo/text!wgs4/views/settings.html'], 
	function(domConstruct, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil){
  	return {
		_hasMenu : false,
		//_hasBack : 'map',
		_setupDisplayMode: function(){
			domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
		},
		
		//init is triggered when view is created
		init : function(){
			//
			this._setupDisplayMode()
			new window.AppData.ViewBaseClass( this );	
		},
		
		//triggered before showing view
		
		afterActivate: function(){
			//
		}			
	}
})
