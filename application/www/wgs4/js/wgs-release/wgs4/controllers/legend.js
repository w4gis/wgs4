require({cache:{
'url:wgs4/views/legend.html':"<div  data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"viewContainer\" style=\"top:42px\" data-dojo-props=\"height:'auto'\"\r\n\t\tdata-dojo-type=\"dojox/mobile/ContentPane\">\r\n\t</div>\t\r\n</div>\r\n"}});
define("wgs4/controllers/legend", [
    'dojo/dom-construct', 'dojo/dom',
    'dojo/dom-class',
    'dojo/sniff',
    'dijit/registry',
    'dojo/topic',
    'dojo/dom-attr', "dojo/on", "dojo/dom-style", "dojo/query", "dojo/_base/array", "dojox/mobile/Pane", 'dojo/text!wgs4/views/legend.html'],

function(domConstruct, dom, domClass, has, registry, topic, domAttr, on, domStyle, query, arrayUtil) {
    return {
        _hasMenu: false,
        _viewTitle: 'leftView,legend',

        _setupDisplayMode: function() {
            this.viewContainer.containerNode.insertBefore(document.getElementById("legend"), this.viewContainer.containerNode.firstChild);
            document.getElementById("legend").style.display = "block";

            //domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
        },

        //init is triggered when view is created
        init: function() {
            var self = this;
            new window.AppData.ViewBaseClass(this);

            this._setupDisplayMode();

            console.log('Subscribe to topic')
            topic.subscribe('legendRender', function() {
                self.reloadLegend();
            });
        },


        reloadLegend: function() {
            var checkboxes = query(".jxTreeRoot > .jxTreeContainer > .jxTree .jxTreeContainer .fusionLegendCheckboxContainer > input.fusionLegendCheckboxInput")
            var checkboxesFolders = query(".jxTreeContainer input.fusionLegendCheckboxInput")
            var checkboxeLabels = query(".jxTreeItem.fusionCheckboxItem")
            //<i class="fa fa-folder"></i> fa-folder
            //jxTreeContainer jxTreeBranch jxTreeBranchLastOpen
            var treeParents = query(".jxTreeContainer.jxTreeBranch > .jxTreeItem.fusionCheckboxItem > span.jxTreeLabel")
            arrayUtil.forEach(treeParents, function(parentNode, i) {
                if (!parentNode['data-has-icon']) {
                    domConstruct.create('i', {
                        'class': 'fa folder-icon'
                    }, parentNode, "first")
                    parentNode['data-has-icon'] = true
                }
                //parentNode.innerHTML = '<i class="fa fa-folder-open"></i>' parentNode.innerHTML
            })

            var _styleCheckbox = function(cssClass, box, i) {
                if (!domAttr.get(box, 'data-has-onclick-handler')) {
                    domAttr.set(box, 'data-has-onclick-handler', true)

                    domClass.add(box.parentElement, cssClass + " legend-checkbox")

                    var id = "chk-" + Math.random()
                    domAttr.set(box, 'id', id)

                    var l = domConstruct.create('label', {
                        'for': id

                    }, box, 'after');



                    l.addEventListener('click', function(e) {
                        e.preventDefault();
                        if (!window.AppData.IS_TOUCH) {
                            clickHandler(id);
                        }
                    });

                    l.addEventListener('touchstart', function(e) {
                        e.preventDefault();

                        window.AppData.IS_TOUCH = true;
                        clickHandler(id);
                    })
                }

                function clickHandler(id) {
                    var elem = document.getElementById(id);
                    var elemCoords = elem.getBoundingClientRect();
                    var currentState = elem.checked;

                    elem.dispatchEvent(
                    new MouseEvent('click', {
                        bubbles: true,
                        cancelable: true,
                        clientX: (elemCoords.left + elemCoords.right) / 2,
                        clientY: (elemCoords.top + elemCoords.bottom) / 2
                    }));
                    elem.checked = !currentState;

                    /*setTimeout(function() {
				        
				    }, 500)*/
                }
            }

                function _stylizeLabels() {
                    for (var i = 0; i < checkboxeLabels.length; i++)
                    domClass.add(checkboxeLabels[i], "mblListItem")
                }


                function styleCheckboxFolder(box, i) {
                    _styleCheckbox('slideThree ' + (dojo.config.locale.substr(0, 2)), box, i);
                }

                function styleCheckboxItem(box, i) {
                    _styleCheckbox('squaredFour ' + (dojo.config.locale.substr(0, 2)), box, i);
                }

            arrayUtil.forEach(checkboxes, styleCheckboxItem)
            arrayUtil.forEach(checkboxesFolders, styleCheckboxItem)
            _stylizeLabels()
        },

        //triggered before showing view
        _beforeActivate: function() {
            topic.publish('legendRender')
            if (registry.byId('bottom-legend-button')) registry.byId('bottom-legend-button').set('selected', true)
        },
        afterActivate: function() {
            //
        }
    }
})