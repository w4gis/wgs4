require({cache:{
'url:wgs4/views/editor.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t\t<button data-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n            data-dojo-attach-point=\"backButton\"\r\n            data-dojo-props=\"arrow: 'left', target: 'leftView,taskpane', transitionDir: -1\"\r\n            style=\"float:left\"\r\n            class=\"backButton\">${nls.back}</button>\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-type=\"wgs4/wheelScrollableView\" data-dojo-props=\"height:'auto'\"\r\n\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\tstyle=\"top:44px;left:0;bottom:0;position:absolute;right:0;\"\r\n\t\t>\r\n\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/EdgeToEdgeCategory\" data-dojo-props=\"label:'${nls.helpTitle}'\"></div>\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/Pane\">\r\n\t\t\t${nls.help}\r\n\t\t</div>\r\n\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/EdgeToEdgeCategory\" data-dojo-props=\"label:'${nls.layers}'\"></div>\r\n\t\t<ul data-dojo-type=\"dojox/mobile/EdgeToEdgeList\" data-dojo-attach-point = \"layerList\">\r\n\t\t\t\r\n\t\t</ul>\r\n\t\r\n\t</div>\t\t\t\r\n</div>\r\n"}});
define("wgs4/controllers/editor", [
	'dojo/query','dojo/_base/declare',
	'dojo/dom-attr','dojo/dom',
	'dojo/dom-style','dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on','dojo/hash','dojox/mobile/EdgeToEdgeCategory','dojox/mobile/EdgeToEdgeList','dojox/mobile/ListItem',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/editor.html'
	],function(query, declare, domAttr, dom, domStyle, domClass, domConstruct, has, on, hash, Category, List, ListItem, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			_hasMenu: false,
			_hasBack: "leftView,taskpane",
			
			_featureInfo:[],
			_cs: {
				'pm': {
					'root'	: 'Pseudo Mercator',
					'en-us'	: 'Pseudo Mercator',
					'ru-ru'	: 'Pseudo Mercator'
				},
				'pz':{
					'root'	: 'Earth Parameters-90',
					'en-us'	: 'Earth Parameters-90',
					'ru-ru'	: 'Парметры Земли-90'
				},
				'pu':{
					'root'	: 'Pulkovo-42',
					'en-us'	: 'Pulkovo-42',
					'ru-ru'	: 'Пулково-42'
				},
				'wgs':{
					'root'	: 'WGS-84',
					'en-us'	: 'WGS-84',
					'ru-ru'	: 'WGS-84'
				}
			},
			selectedCs: 'pz',
			
			init : function(){
				var self = this
				new window.AppData.ViewBaseClass( this );	
				var onSuccess = function(result){
					console.log(result)
					for(var i=0; i< result.length; i++)
						(function(layerId, layerName){
							self.layerList.addChild(new ListItem({								
								label: result[i].name,
								target: 'leftView,editormain',
								clickable: true,
								transitionOptions: {
									params: {
										id: layerId										
									}
								}
							}))
						})(result[i].id, result[i].name)
				}
				var onError = function(error){
					console.log('error', error)
				}
				xhr.post("/objectadmin/GetFeatureLayerList", {handleAs: "json",	data:{
					
				}}).then(onSuccess, onError);
			},
			
			_setCs: function(cs){
				this.selectedCs = cs
				this.backButton.transitionOptions = { params : { cs : this.selectedCs, from: 'cs' } }
				if(has("phone"))
					registry.byId("appBack").set("transitionOptions", this.backButton.transitionOptions)
			},
			
			_updateState: function(){
				var cs = this.selectedCs.substr(0,1).toUpperCase() + this.selectedCs.substr(1,this.selectedCs.length-1)
				if(this["is"+cs])
					this["is"+cs].set('checked',true)
			},
			
			_beforeActivate: function(){
				//
				if(this.params && this.params.from && this.params.cs){
					this.selectedCs = this.params.cs
					this.backButton.target = this.params.from
					this._hasBack = this.backButton.target
					this._updateState()
				}
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				
				
				

			}	
		}
	}
)
