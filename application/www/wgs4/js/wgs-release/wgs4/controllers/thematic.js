require(['wgs4/model/theme', 
	'wgs4/views/theme/theme', 
	"dojo/Deferred", 
	'wgs4/views/theme/themeList',
	'wgs4/views/theme/themeEdit',
	'wgs4/views/theme/themeSetup'], function(themeModel, themeView, Deferred, ThemeListView, ThemeEditView, ThemeSetupView){
	//console.log('ok');

	function ThemeController() {
		this.themeListView = new ThemeListView();
		this.themeEditView = new ThemeEditView();
		this.themeSetupView = new ThemeSetupView();
	}

	ThemeController.prototype.showThemeWindow = function() {
		var self = this;
		themeModel.get(function(err, themeList){
			if (err) {
				console.log('Something went wrong! Error is -> '+err); //debug
				return;
			}
			self.themeListView.showThemeListDialog(themeList, self.registryThemeListDialogEvents.bind(self));
		});
	}

	ThemeController.prototype.registryThemeListDialogEvents = function() {
		var self = this;
		var themeWindow = document.getElementsByClassName('theme-list-window')[0];
		console.log(themeWindow);
		themeWindow.addEventListener('click', function(e){
			switch (e.target.getAttribute('data-action')) {
				case "new":
				console.log('new');
				self.themeEditView.showThemeEditDialog(self.registryThemeEditDialogEvents.bind(self));
				break;
				case "show":
				//self.showTheme(+e.target.getAttribute('data-theme-id'));
				console.log('show '+e.target.getAttribute('data-theme-id'));
				break;
				case "edit":
				console.log('edit '+e.target.getAttribute('data-theme-id'));
				break;
				case "delete":
				self.deleteTheme(+e.target.getAttribute('data-theme-id'));
				console.log('delete '+e.target.getAttribute('data-theme-id'));
				break;
				case "setup":
				self.showThemeSetupDialog(+e.target.getAttribute('data-theme-id'));
				/*setupTheme(+e.target.getAttribute('data-theme-id')).then(function(results){
					console.log(results);
				});*/
				console.log('setup '+e.target.getAttribute('data-theme-id'));
				break;
				case "refresh":
				console.log(self);
				self.showThemeWindow();
				//showThemeListDialog();
				break;
			}

		});
	};

	ThemeController.prototype.registryThemeEditDialogEvents = function() {
		var self = this;
		document.getElementById('theme-edit-submit-button').addEventListener('click', function(){
			var themeName = document.getElementById('theme-name');
			if (themeName.value) {
				themeModel.add(themeName.value, 1, 0, function(err, result){
					if (err) {
						console.log('Something went wrong! Error is -> '+err); //debug
						return;
					};
					self.showThemeWindow();
				});
			}
			//добавить при создании формы data-action и если он new, то создаем, если edit то редактируем, а пока новый
			
			self.themeEditView.hideThemeEditDialog();
		});


	};

	ThemeController.prototype.showThemeSetupDialog = function(themeId) {
		var self = this;
		themeModel.getThemeLayersParams(themeId, function(err, themeLayersParams){
			if (err) {
				console.log(err); //debug
				return;
			};
			self.themeSetupView.showThemeSetupDialog(themeLayersParams, self.registryThemeSetupDialogEvents.bind(self));
		});
		
	}


	ThemeController.prototype.registryThemeSetupDialogEvents = function() {
		var self = this;
		var themeSetupWindow = document.getElementsByClassName('theme-setup-window')[0];
		themeSetupWindow.addEventListener('click', function(e){
			switch (e.target.getAttribute('data-action')) {
				case "new":
				console.log('new');
				//self.themeEditView.showThemeEditDialog(self.registryThemeEditDialogEvents.bind(self));
				break;
				case "refresh":
				console.log(self);
				self.showThemeSetupDialog(+e.target.getAttribute('data-theme-id'))
				//showThemeListDialog();
				break;
			}

		});
	}

	ThemeController.prototype.deleteTheme = function(themeId) {
		var self = this;
		themeModel.deleteTheme(themeId, function(err){
			if (err) {
				console.log(err); //debug
				return;
			};
			console.log('delete successfully '+themeId);
			self.showThemeWindow();
		});
	};

	var themeController = new ThemeController();
	themeController.showThemeWindow();


/*
	function showTheme(themeId) {
		themeModel.showTheme(themeId, function(err){
			if (err) {
				console.log(err); //debug
				return;
			};
			themeView.hideThemeListDialog();
		});
	};
*/

})
