require({cache:{
'url:wgs4/views/language.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t\t<button data-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n            data-dojo-attach-point=\"backButton\"\r\n            data-dojo-props=\"arrow: 'left', target: 'leftView,settings', transitionDir: -1\"\r\n            style=\"float:left\"\r\n            class=\"backButton\">${nls.back}</button>\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-type=\"wgs4/wheelScrollableView\" data-dojo-props=\"height:'auto'\"\r\n\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\tstyle=\"top:44px;left:0;bottom:0;position:absolute;right:0;\"\r\n\t\t>\r\n\t\t<!--\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'Geometry info'\"></div>\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRect\" data-dojo-props=\"shadow:true\">\r\n\t\t\tSelect objects on map to view their geometric properties\r\n\t\t</div>\r\n\t\t-->\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'${nls.language}'\"></div>\r\n\t\t<ul data-dojo-type=\"dojox/mobile/RoundRectList\" data-dojo-props=\"select: 'single'\">\r\n\t\t\t<!--<div \r\n\t\t\t\tdata-dojo-attach-point=\"systemDefault\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\" \r\n\t\t\t\tdata-dojo-props=\"label: '${nls.systemDefault}', checked: true\"></div>-->\r\n\t\t\t<div \r\n\t\t\t\tdata-dojo-attach-point=\"en-us\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\" \r\n\t\t\t\tdata-dojo-props=\"label: 'English'\"></div>\r\n\t\t\t<div \r\n\t\t\t\tdata-dojo-attach-point=\"ru-ru\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\" \r\n\t\t\t\tdata-dojo-props=\"label: 'Русский'\"></div>\r\n\t\t\t<div \r\n\t\t\t\tdata-dojo-attach-point=\"uk-ua\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\" \r\n\t\t\t\tdata-dojo-props=\"label: 'Український'\"></div>\r\n\t\t</ul>\r\n\t\r\n\t</div>\t\t\t\r\n</div>\r\n"}});
define("wgs4/controllers/language", [
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',"dojo/on","dojo/dom-style","dojo/query","dojo/_base/array","dojox/mobile/Pane",'dojo/text!wgs4/views/language.html'], 
	function(domConstruct, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil){
  	return {
		_hasMenu : false,
		_hasBack : 'leftView,settings',
		_setupDisplayMode: function(){
			domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
		},
		
		langs: ['en-us','ru-ru','uk-ua'],
		//init is triggered when view is created
		init : function(){
			//
			this._setupDisplayMode()
			new window.AppData.ViewBaseClass( this );	
			var self = this
			for(var i =0; i< this.langs.length ; i++){
			    (function(i){
			        self[self.langs[i]].onClick = function(){
			            localStorage.setItem('wgs_locale', self.langs[i])
			            location.reload()
			        }
			    })(i)
			}
			
			/*this.systemDefault.onClick = function(){
			    localStorage.removeItem('wgs_locale')
			    location.reload()
			}*/
			
		},
		
		//triggered before showing view
		_beforeActivate: function(){
			//this.legendButton.set('selected',true)
			console.log('!');
			for(var i =0; i< this.langs.length ; i++){
			    var locale = localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale') : 'ru-ru';
			    if( locale && locale == this.langs[i])
			        this[this.langs[i]].set('checked', true)
			}
		},
		afterActivate: function(){
			//
		}			
	}
})
