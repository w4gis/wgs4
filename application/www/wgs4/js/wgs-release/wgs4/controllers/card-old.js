require({cache:{
'url:wgs4/views/card.html':"<div>\n    <div data-dojo-type=\"dijit/layout/TabContainer\" style=\"width: 100%; height: 100%;\">\n        <div data-dojo-type=\"dijit/layout/ContentPane\" title=\"Здания\" id=\"card-pane-1\" data-dojo-props=\"\">\n            <form name=\"1\" oninput=\"\n\n\n\n            console.log(event);\n\n\n\n            console.log(event.target.form.attributes[1].value+event.target.name+event.target.value);\">\n                <!-- Основные описания -->\n                <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Основные характеристики', open: true\">\n                    <ul>\n                        <li>\n                            <label> <span>            Наименование        </span>:\n                                <input value=\"Без наименования - Здания\" name=\"name\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n            Краткое наименование\n        </span>:\n                                <input value=\"Здания\" name=\"shortName\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n            Атрибутивное описание\n        </span>:\n                                <input value=\"Здания\" name=\"typeName\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n\n\n\n            Дата изменения\n\n\n\n        </span>:\n                                <input value=\"2015-06-15 11:28:23\" name=\"date\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n\n\n\n            Пользователь\n\n\n\n        </span>:\n                                <input value=\"\" name=\"user\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n\n\n\n            Примечание\n\n\n\n        </span>:\n                                <input value=\"\" name=\"note\" />\n                            </label>\n                        </li>\n                    </ul>\n                </div>\n                <!-- Группы характеристик -->\n                <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Адресные', open: false\">\n                    <ul>\n                        <li>\n                            <label> <span>\n\n\n\n            Улица\n\n\n\n        </span>:\n                                <input value=\"\" name=\"1\" />\n                            </label>\n                        </li>\n                    </ul>\n                </div>\n                <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Конструктивные', open: false\">\n                    <ul>\n                        <li>\n                            <label> <span>\n\n\n\n            Этажность\n\n\n\n        </span>:\n                                <input value=\"\" name=\"2\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n\n\n\n            Район\n\n\n\n        </span>:\n                                <input value=\"\" name=\"3\" />\n                            </label>\n                        </li>\n                        <li>\n                            <label> <span>\n\n\n\n            Номер дома\n\n\n\n        </span>:\n                                <input value=\"\" name=\"4\" />\n                            </label>\n                        </li>\n                    </ul>\n                </div>\n                <!-- Связи -->\n            </form>\n        </div>\n    </div>\n</div>"}});
define("wgs4/controllers/card-old", ['dojo/_base/window',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/dom',
    "dojo/date",

    'dojo/sniff',
    'dijit/registry',
    'dojo/topic', 'dojo/date/locale',
    'dojo/dom-attr', 'dojo/on', 'dojo/dom-style', 'dojo/query', 'dojo/_base/array',
    'dojox/mobile/Pane', 'dojo/store/Memory', 'dijit/form/Select', 'dojo/data/ObjectStore',
    'dojo/request/xhr', 'dojo/json', 'dojox/mobile/Accordion', 'dojox/mobile/ListItem', 'dojox/mobile/ContentPane',
    'dojox/mobile/View', 'dojox/mobile/PageIndicator', 'dojox/mobile/TextBox', 'dojox/mobile/Container',
    'dojo/_base/connect', 'dojox/mobile/DatePicker', 'dojox/mobile/Button', 'dojox/mobile/EdgeToEdgeList', 'dojox/mobile/SimpleDialog', 'wgs4/model/card', 'dgrid/Grid', 'wgs4/controllers/card',
    'dojo/text!wgs4/views/card.html'],

function(win, domConstruct, domClass, dom, ddate, has, registry, topic, datelocale, domAttr, on, domStyle,
query, arrayUtil, Pane, Memory, Select, ObjectStore, xhr, JSON, Accordion, ListItem,
ContentPane, SwapView, PageIndicator, TextBox, Container, connect, DatePicker, Button, List, Dialog, card, Grid, ncard) {

    return {
        //init is triggered when view is created
        _hasMenu: true,
        _hasMenuClass: 'mblGreyButton',
        _hasMenuTitle: '<i class="fa fa-save"></i>',
        _hasMenuCallback: function() {
            var self = this;
            console.log(self);
            self.saveCard();


        },
        _viewTitle: 'card',
        _errors: {
            NO_PARENT_WIDGET: {
                id: 1,
                message: 'Accordeon not found'
            }
        },



        /*
         *	(!) This is a _disableEndDaysOfMonth method to fix datePicker error
         */
        disableEndDaysOfMonth: function() {
            // summary:
            //		Disables the end days of the month to match the specified
            //		number of days of the month. Returns true if the day value is changed.
            // tags:
            //		private
            /*var pat = this.slots[0].pattern + "/" + this.slots[1].pattern,
				v = this.get("values"),
				date = datelocale.parse(v[0] + "/" + v[1], {datePattern:pat, selector:"date"}),
				daysInMonth = ddate.getDaysInMonth(date);*/
            var changedDay = false;

            var DataSlots = this.slots //v = this.get("values"), // [year, month, day]
            DataSlotsValue = [DataSlots[0].value, DataSlots[1].value, DataSlots[2].value]
            DataSlotsPattern = DataSlots[0].pattern + "/" + DataSlots[1].pattern + "/" + DataSlots[2].pattern;

            var dateFallback = datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
                datePattern: DataSlotsPattern,
                selector: "date"
            }) ? datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
                datePattern: DataSlotsPattern,
                selector: "date"
            }) : new Date;

            daysInMonth = ddate.getDaysInMonth(dateFallback);


            if (daysInMonth < DataSlots[2].value) {
                // day value is invalid for this month, change it
                changedDay = true;
                DataSlots[2]._spinToValue(daysInMonth, false /*applyValue*/ );
            }
            this.disableValues(daysInMonth);
            return changedDay;
        },



        /*
         * (!) This is a _getDateAttr method to fix datePicker error		 *
         */
        getDateAttr: function() {
            // summary:
            //		Returns a Date object for the current values.
            // tags:
            //		private
            console.dir(this);
            var DataSlots = this.slots //v = this.get("values"), // [year, month, day]
            DataSlotsValue = [DataSlots[0].value, DataSlots[1].value, DataSlots[2].value]
            DataSlotsPattern = DataSlots[0].pattern + "/" + DataSlots[1].pattern + "/" + DataSlots[2].pattern;

            var dateFallback = datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
                datePattern: DataSlotsPattern,
                selector: "date"
            }) ? datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
                datePattern: DataSlotsPattern,
                selector: "date"
            }) : new Date;

            return dateFallback;
        },

        init: function() {
            //this.selCard = new Select();
            new window.AppData.ViewBaseClass(this);
            //bt = this.SaveCardButton
            this.currentSelected = 0;
            this.frstCardId = 0;
            this.changedCards = new Array();
            var self = this;
            this.initializeSelectionEvents();
            on(dom.byId("SaveCardButton"), "click", function() {
                self.saveCard();
                //self.getCardsOfSelectedFeature(self.currentSelected);
            });
            if (has('phone')) {
                domStyle.set(this.cardDisplayContainer.domNode, 'bottom', '45px')
            }

            //Change combobox with card name after swap view transition
            topic.subscribe('/dojox/mobile/viewChanged', function() {
                if (location.toString().indexOf('#card') > -1) {
                    var newSwapViewId = registry.byId(arguments[0].id).getChildren()[0].id
                    var swValue = newSwapViewId.substr(4, newSwapViewId.length - 1)
                    var cbValue = registry.byId('targetForCmb1').get('value').substr(4, registry.byId('targetForCmb1').get('value').length - 1);
                    if (Number(cbValue) != Number(swValue)) registry.byId('targetForCmb1').set('value', 'ccbi' + swValue);

                }
            })

            connect.subscribe("/dojox/mobile/deleteListItem", function(item) {
                var itemid = item.id.substr(item.id.indexOf('n') + 1);
                console.log('всем булочек и пива');
                item.rightIconNode.style.display = "none";
                if (!item.rightIcon2Node) {
                    item.set("rightIcon2", "mblDomButtonMyRedButton_0");
                    item.rightIcon2Node.firstChild.innerHTML = "Удалить";
                }
                item.rightIcon2Node.style.display = "";
                handler = connect.connect(item.ightIcon2Node, "onclick", function() {
                    console.log('удаляем' + itemid);
                    var relArr = [];
                    relArr[0] = itemid;
                    xhr.post("/objectadmin/card/RemoveObjectRelation", {
                        //sync : true,
                        handleAs: "json",
                        data: {
                            removeMode: 1,
                            'relationsId[]': relArr
                        }
                    }).then(function() {
                        console.log('удалили' + itemid);
                        item.destroy();
                    });
                });
            });
        },

        /*
         * 	Add new property group to card
         * 	@label 		- Group label
         * 	@groupId 	- Group WIDGET id
         * 	@targetId	- Parent widget id
         * 	@isProperty - true if group is NOT for card relations *
         *
         * 	* - relations are placed after all property groups
         *
         */
        addGroup: function(label, groupId, targetId, isProperty) {
            this.createAccordeon(targetId);

            if (!registry.byId('acc' + targetId)) {
                throw (this._errors.NO_PARENT_WIDGET);
            }

            // return if group already exists
            if (registry.byId(groupId)) return;

            var newWidget = new ContentPane({
                'label': label + '<i class="fa redline-opener"></i>',
                'id': groupId
            });

            var panesCount = registry.byId('acc' + targetId).getChildren().length;
            if (isProperty) {
                registry.byId('acc' + targetId).addChild(newWidget, panesCount > 0 ? 1 : 0);
            } else registry.byId('acc' + targetId).addChild(newWidget);

            newWidget.addChild(new List({
                id: groupId + 'list',
            }))

            // place new accordeon panes in correct order (after accordeon pane title)
            var panes = query('#acc' + targetId + ' .mblContentPane.mblAccordionPane');
            for (var i = 0; i < panes.length; i++) {
                domConstruct.place(panes[i], registry.byId(panes[i].id)._at.id, 'after')
            }

        },

        /*
         * 	Shows save card yes/no dialog;
         * 	if yes - saves card;
         * 	clears card, shows cards of given feature.
         *
         * 	Dialog will be created if doesn't exists or shown if it does.
         *
         * 	@featureId - geometry object id
         *
         */
        showSaveCardDialog: function(fid) {
            console.log("test");
            var self = this;
            //domStyle.set("SaveCardButton", "display", 'none');
            registry.byId("SaveCardButton").disabled = true;
            if (this._saveQuestionDialog) {
                this._saveQuestionDialog.show();
                return;
            }

            this._saveQuestionDialog = new Dialog({});
            win.body().appendChild(this._saveQuestionDialog.domNode);
            domConstruct.create('div', {
                innerHTML: 'Закрытие карточки',
                'class': 'mblHeading'
            }, this._saveQuestionDialog.domNode)

            domConstruct.create('div', {
                innerHTML: '<p>Сохранить изменения перед закрытием карточки?</p><br/>' + '<button id="YesSaveCardButton" class="mblBlueButton" >Сохранить</button>&nbsp;' + '<button id="NoSaveCardButton" class="mblRedButton">Отмена</button>',
                'class': 'mblSimpleDialogText',
                style: 'text-align:center'
            }, this._saveQuestionDialog.domNode)

            var funcYes = function() {
                self.saveCard();
                self._saveQuestionDialog.hide();
                if (fid > 0) self.showSelectedCards(fid);
                else {
                    self.currentSelected = 0;
                    self.clearCard();
                    domStyle.set('cardSelectAnything', 'display', 'block');
                }
            }

            var funcNo = function() {
                self._saveQuestionDialog.hide();
                if (fid > 0) self.showSelectedCards(fid);
                else {
                    self.currentSelected = 0;
                    self.clearCard();
                    domStyle.set('cardSelectAnything', 'display', 'block');
                }
            }

            new Button({}, 'YesSaveCardButton')
            new Button({}, 'NoSaveCardButton')
            on(registry.byId('YesSaveCardButton'), 'click', funcYes);
            on(registry.byId('NoSaveCardButton'), 'click', funcNo);

            this._saveQuestionDialog.show();
        },


        /*
         * Adds new relation group to card;
         *
         * @lbl - label for group
         * @id - new object id
         * @idtarget - id of target container
         *
         */
        addRelationGroup: function(lbl, id, idtarget) {
            this.createAccordeon(idtarget);
            if (!(registry.byId('acc' + idtarget)) || registry.byId(id)) return;
            var self = this;

            var newWidget = new ContentPane({
                'id': id,
                'label': lbl + '<i class="fa redline-opener"></i>'
            });
            registry.byId('acc' + idtarget).addChild(newWidget);
            domConstruct.create('table', {
                'id': 'targetForButtons' + id
            }, id);
            domConstruct.create('tr', {
                'id': 'targetTables' + id
            }, 'targetForButtons' + id);
            domConstruct.create('td', {
                'id': 'targetCreateChild' + id
            }, 'targetTables' + id);
            domConstruct.create('td', {
                'id': 'targetUnbindChild' + id
            }, 'targetTables' + id);

            var CreateButton = new Button({
                'id': 'CreateChild' + id,
                'label': 'Создать',
                'class': 'mblBlueButton',
                'onClick': function() {
                    console.log("id reletion " + id.substr(id.indexOf('n') + 1));
                    var relId = id.substr(id.indexOf('n') + 1);
                    self.app.transitionToView(self.domNode, {
                        target: 'leftView,relation',
                        params: {
                            first: '00',
                            relationId: relId,
                            cardId: idtarget
                        }
                    })
                }

            }, domConstruct.create('button', {}, 'targetCreateChild' + id));
            var UnbindButton = new Button({
                'id': 'UnbindChild' + id,
                'label': 'Отвязать',
                'class': 'mblRedButton',
                'onClick': function() {
                    var lst = registry.byId(id + 'list');
                    if (lst.isEditing) lst.endEdit();
                    else lst.startEdit();
                }

            }, domConstruct.create('button', {}, 'targetUnbindChild' + id));

            newWidget.addChild(window.xxx = new List({
                id: id + 'list',
                editable: true
            }))

        },


        /*
         * Adds property into current group, GOT IT?!;
         *
         * @lbl - label of new object
         * @newid - id of new object
         * @id - target object id
         * @editable - true if user can edit text
         *
         */
        addPropertyIntoCurrentGroup: function(lbl, id, newid, editable) {
            if (!(registry.byId(id))) return;
            var self = this;
            console.log(self, self.nls)
            /*if(titleId)
			console.log('#' + titleId + ' div')*/

            domConstruct.create('li', {
                'id': "divForTextBoxProperty" + newid,
                'class': 'cardDiv'

            }, id + 'list');
            //(titleId && query('#x' + titleId + ' div').length) ? query('#x' + titleId + ' div')[0] : id
            var newWidget = new TextBox({
                'id': newid,
                'value': lbl,
                placeHolder: self.nls.na,
                'class': 'textbox',
                'disabled': !editable || !window.WGS.PRIVILEGE.CARD_EDIT,
                'onInput': function(ev) {
                    //alert('!!!');
                    //if(registry.byId('SaveCardButton').get('style') == 'display:none')
                    var fproc = newid.indexOf('-') + 1
                    var cid = newid.substr(fproc, newid.indexOf('-', fproc + 1) - fproc);
                    console.log('id:' + newid + ';cid:' + cid);
                    self.addChangedCard(cid);
                    //domStyle.set('SaveCardButton', 'display', 'inline');
                    //registry.byId("SaveCardButton").disabled = false;
                    //registry.byId('SaveCardButton').set('style', 'display:inline');
                }
            }, domConstruct.create('input', {

            }, "divForTextBoxProperty" + newid));
        },

        /* Add Changed Cards to Array */

        addChangedCard: function(cid) {
            if (!this.changedCards) this.changedCards = new Array();
            registry.byId('SaveCardButton').set('class', 'mblBlueButton');
            //registry.byId('appMenu').set('class', 'mblBlueButton');
            domClass.remove(registry.byId('appMenu').domNode, 'mblGreyButton');
            domClass.add(registry.byId('appMenu').domNode, 'mblBlueButton');
            console.log('ChangeColor');

            var ex = false;
            for (var i = 0; i < this.changedCards; i++) {
                if (this.changedCards[i] == cid) ex = true;
            }
            if (!ex) this.changedCards.push(cid);
            console.log(this.changedCards.join());
        },

        /*
         * Adds list (combobox) of domains to current group;
         *
         * @dataForDomain - array of strings
         * @cid - id of current card
         * @cpgid - group id
         * @cpid - id of new property (this list)
         * @cpvalue - current value of list
         *
         */

        addPropertyDomainIntoCurrentGroup: function(dataForDomain, cid, cpgid, cpid, cpvalue) {
            if (!(registry.byId('groupProp' + cid + cpgid + 'list'))) return;
            var self = this;
            var li = domConstruct.create('li', {
                style: "padding: 5px 10px;"
            }, "propName" + cid + cpid, 'after');
            var mmry = new Memory({
                data: dataForDomain
            });
            var os = new ObjectStore({
                objectStore: mmry
            });

            console.log('value is', cpvalue)
            var s = new Select({
                store: os,
                id: "divForPropertyDomain" + cid + cpid,
                'class': 'comboBoxCardDomain',
                disabled: !window.WGS.PRIVILEGE.CARD_EDIT,
                'value': cpvalue ? ("domainValue" + cid + "prop" + cpid + "domn" + cpvalue) : null,
                //id : "divForPropertyDomain" + cid + cpid,
                'onChange': function() {
                    console.log('fnc from select ' + this.get('value'));
                    //registry.byId('SaveCardButton').set('style', 'display:inline');
                    domStyle.set('SaveCardButton', 'display', 'inline');
                    self.addChangedCard(cid);
                }
            }, domConstruct.create('div', {}, li));

            s.startup();
            if (cpvalue) s.getOptions('').disabled = true;
            //console.log(s.id);
            //s.on("change", fnc);
        },

        /*
         * Adds string that shows property name;
         *
         * @lbl - label
         * @id - target container id
         * @newid - id of new element
         *
         */
        addPropertyIntoCurrentGroupName: function(lbl, id, newid) {
            if (!(registry.byId(id))) return;
            var newWidget = new ListItem({
                'id': newid,
                'label': lbl + ':',
                'class': 'ListItemLabel',
                variableHeight: true
            });
            registry.byId(id + 'list').addChild(newWidget);
            //return newid
        },


        /*
         * Creates new Date Picker (Calendarick);
         *
         * @lbl - label
         * @id - target container id
         * @newid - id of new element
         *
         */

        addDatePicker: function(lbl, id, newid) {
            var self = this
            if (!(registry.byId(id))) return;
            domConstruct.create('li', {
                'id': "divForTextBoxProperty" + newid,
                'class': 'cardDiv'
            }, id + 'list');

            var thisTextBox = new TextBox({
                'id': newid,
                'value': lbl,
                'style': 'display:none',
                'onChange': function(ev) {
                    domStyle.set('SaveCardButton', 'display', 'inline');
                }
            }, domConstruct.create('input', {}, "divForTextBoxProperty" + newid));

            new Button({
                'label': lbl ? lbl : 'Выбрать дату...',
                disabled: !window.WGS.PRIVILEGE.CARD_EDIT,
                'onClick': function(ev) {
                    var thisButton = this;
                    self.dateDialog.show();
                    self.dateSelectButton.onClick = function() {
                        self.dateDialog.hide();
                        thisTextBox.set('value', self.datePicker.get('value'))
                        thisButton.set('label', self.datePicker.get('value'))
                        var fproc = newid.indexOf('-') + 1
                        var cid = newid.substr(fproc, newid.indexOf('-', fproc + 1) - fproc);
                        self.addChangedCard(cid);
                    }
                }
            }, domConstruct.create('button', {}, "divForTextBoxProperty" + newid));
        },

        /*
         * Adds name property of relation;
         *
         * @lbl - label
         * @id - target container id
         * @newid - id of new element
         * @idCard - current card id
         *
         */
        addRelationCardName: function(lbl, id, newid, idCard) {
            if (!(registry.byId(id))) return;

            var newWidget = new ListItem({
                'id': newid,
                'label': lbl,
                'class': 'ListItemLabel',
                'clickable': true
            });

            connect.connect(newWidget, 'onClick', null,

            function() {
                var activeView = registry.byId('swap' + idCard).getParent().getShowingView();
                activeView.goTo(1, registry.byId('swap' + idCard).getParent().id)
            });
            registry.byId(id + 'list').addChild(newWidget);

        },

        /*
         * Creates new Swap View;
         *
         * @cardId - id of current card
         *
         */

        createSwap: function(cardId) {
            if (registry.byId('swap' + cardId)) return;
            var newWidget = new SwapView({
                'label': 'Название'
            }, domConstruct.create('div', {}, "targetForCardSwaps"));
            newWidget.startup();

            var sv = new ContentPane({
                'id': "swap" + cardId
            })


            newWidget.addChild(sv);

            newWidget.domNode.classList.add('scrollable-pane-100-height')
            /*var func = functFiion (moveTo,dir,trans,cont,meth) {
			console.log('test');
			var cid = this.getShowingView().id.substr(4);
			console.log("current swap: ", cid);
			if(registry.byId('targetForCardCombobox'))
			registry.byId('targetForCardCombobox').set('value','ccbi'+cid);
			};
			connect.connect(newWidget, "onBeforeTransitionOut", null, func);*/

            //newWidget.onAfterTransitionIn = func;
            //newWidget.onAnimationEnd = func;
            console.log("Swap " + cardId + " exists");
            //registry.byId("targetForSwaps").addChild( newWidget );
            //console.log(obj.name);

        },

        /*
         * Creates new Accordion;
         *
         * @cardId - id of current card
         *
         */
        createAccordeon: function(cardId) {
            if (registry.byId('acc' + cardId)) return;
            if (!(registry.byId('swap' + cardId))) this.createSwap(cardId);
            var accId = "acc";
            var newAccWidget = new Accordion({
                'id': 'acc' + cardId,
                'label': 'test',
                'singleOpen': true
            }, domConstruct.create('div', {}, registry.byId("swap" + cardId).containerNode));
            newAccWidget.startup();
        },

        /* Saves changed cards */
        saveCard: function() {

            var self = this;
            console.log('save' + this.changedCards.length);
            if (this.changedCards.length > 0) {
                registry.byId('SaveCardButton').set('class', 'mblGreyButton');
                domClass.remove(registry.byId('appMenu').domNode, 'mblBlueButton');
                domClass.add(registry.byId('appMenu').domNode, 'mblGreyButton');
                for (var i2 = 0; i2 < this.changedCards.length; i2++) {
                    console.log('save' + this.changedCards.length);
                    var cid = this.changedCards[i2];
                    var dfr = {};
                    dfr.id = cid;
                    dfr.name = registry.byId("valueNameCard-" + cid + '-').get('value');
                    dfr.shortName = registry.byId("valueShortName-" + cid + '-').get('value');
                    dfr.typeName = registry.byId("valueTypeName-" + cid + '-').get('value');
                    dfr.note = registry.byId("valueNote-" + cid + '-').get('value');
                    if ("ids" in this.objProps[cid.toString()]) {
                        var ids = this.objProps[cid.toString()].ids;
                        for (var i = 0; i < ids.length; i++) {
                            if (ids[i] >= 0) {
                                dfr['property-' + ids[i]] = registry.byId("propValue-" + cid + '-' + ids[i]).get('value');
                            } else {
                                console.log(ids[i]);
                                var iddmn = registry.byId("divForPropertyDomain" + cid + (-ids[i])).get('value');
                                dfr['property-' + (-ids[i])] = iddmn.substr(iddmn.indexOf('domn') + 4);
                            }
                        }

                    }
                    console.log(dfr);
                    xhr.post("/objectadmin/card/SetCard", {
                        handleAs: "json",
                        data: dfr
                    }).then(function() {
                        console.log('Saved!!');
                        domStyle.set('cardChangesSaved', 'display', 'block');
                        setTimeout(function() {
                            domStyle.set('cardChangesSaved', 'display', 'none');
                        }, 3000);
                    });
                    console.log("select not add");
                    self.selCard.updateOption({
                        'value': 'ccbi' + cid,
                        'label': dfr.name ? dfr.name : 'Без наименования' + ' - ' + dfr.typeName
                    });
                    console.log("select add");
                }
                this.changedCards = new Array();
            }

        },

        /* Clears card containers for another ones */

        clearCard: function() {
            // очищаем карточки
            this.changedCards = new Array();
            var formWidgets = registry.findWidgets(dom.byId('targetForCardSwaps'));
            if (formWidgets.length > 0) {
                //console.log('l: ' + formWidgets.length);
                for (var i = 0; i < formWidgets.length; i++) {
                    if (formWidgets[i]) {
                        //console.log(formWidgets[i].toString() + ' exists');
                        formWidgets[i].destroyRecursive(true);
                    }

                }
                //if (dom.byId('targetForPageIndicator')) dom.byId('targetForPageIndicator').destroyRecursive()
                //if (registry.byId('cardPageInd')) registry.byId('cardPageInd').destroyRecursive()
            }
            document.getElementById('targetForCardSwaps').removeChild(document.getElementById('targetForPageIndicator'));
            console.log('cleared tpi');
            // удаляем комбобокс
            formWidgets = registry.findWidgets(dom.byId('targetForCardCombobox'));
            if (formWidgets.length > 0) {
                //console.log('l: ' + formWidgets.length);
                for (var i = 0; i < formWidgets.length; i++) {
                    if (formWidgets[i]) {
                        //console.log(formWidgets[i].toString() + ' exists');
                        formWidgets[i].destroyRecursive(true);
                    }
                }
            }
            document.getElementById('cardPageInd').parentElement.removeChild(document.getElementById('cardPageInd'));
            console.log('cleared tcb');
        },

        /*
         * Gets information about cards of selected object;
         *
         * @fid - feature ID
         *
         */
        showSelectedCards: function(fid) {
            var self = this;
            this.currentSelected = fid;
            var cards = card.getCardsOfSelectedFeature(fid);
            //var cards = card.get();
            console.log(cards);
            this.objProps = {};
            self.clearCard();
            //return;
            var pi = new PageIndicator({
                'id': "cardPageInd",
                refId: domConstruct.create('div', {
                    id: 'targetForPageIndicator'
                }, 'targetForCardSwaps').id
            }, domConstruct.create('div', {}, "targetForCardCombobox", 'after'));
            pi.startup();
            pi.reset();
            var dataForMemory = [];
            console.log('dataaaa');
            if (fid.length > 1) {

                console.log('many selected');
                self.changedCards = {};
                //console.log(data);
                for (var i = 0; i < cards.cards[0].length; i++) {
                    if (cards.cards[0][i].objectTypeId != -1) {
                        self.addGroup("Основные характеристики", "card" + cards.cards[0][i].objectTypeId, cards.cards[0][i].objectTypeId, false);
                        self.addPropertyIntoCurrentGroupName("Наименование", "card" + cards.cards[0][i].objectTypeId, "nameCard" + cards.cards[0][i].objectTypeId);
                        self.addPropertyIntoCurrentGroup(cards.cards[0][i].label, "card" + cards.cards[0][i].objectTypeId, "valueNameCard-" + cards.cards[0][i].objectTypeId + '-', true);

                        self.addPropertyIntoCurrentGroupName("Название", "card" + cards.cards[0][i].objectTypeId, "namesCard" + cards.cards[0][i].objectTypeId);
                        self.addPropertyIntoCurrentGroup(cards.cards[1][i - 1].name, "card" + cards.cards[0][i].objectTypeId, "valueNamesCard-" + cards.cards[0][i].objectTypeId + '-', true);

                        self.addPropertyIntoCurrentGroupName("Краткое название", "card" + cards.cards[0][i].objectTypeId, "shortCard" + cards.cards[0][i].objectTypeId);
                        self.addPropertyIntoCurrentGroup(cards.cards[1][i - 1].shortName, "card" + cards.cards[0][i].objectTypeId, "valueShortCard-" + cards.cards[0][i].objectTypeId + '-', true);

                        self.addPropertyIntoCurrentGroupName("Описание", "card" + cards.cards[0][i].objectTypeId, "noteCard" + cards.cards[0][i].objectTypeId);
                        self.addPropertyIntoCurrentGroup(cards.cards[1][i - 1].note, "card" + cards.cards[0][i].objectTypeId, "valueNoteCard-" + cards.cards[0][i].objectTypeId + '-', true);
                        if (i == 1) self.frstCardId = cards.cards[0][i].objectTypeId;
                        for (j = 0; j < cards.cards[2][i - 1].length; j++) {
                            self.addGroup(cards.cards[2][i - 1][j].groupName, "card" + cards.cards[0][i].objectTypeId + "gr" + cards.cards[2][i - 1][j].groupId, cards.cards[0][i].objectTypeId, false);
                            self.addPropertyIntoCurrentGroupName(cards.cards[2][i - 1][j].Propertyname, "card" + cards.cards[0][i].objectTypeId + "gr" + cards.cards[2][i - 1][j].groupId, "propname" + cards.cards[0][i].objectTypeId + "id" + cards.cards[2][i - 1][j].id);
                            self.addPropertyIntoCurrentGroup(cards.cards[2][i - 1][j].propertyValue, "card" + cards.cards[0][i].objectTypeId + "gr" + cards.cards[2][i - 1][j].groupId, "propvalue" + cards.cards[0][i].objectTypeId + "id" + cards.cards[2][i - 1][j].id, true);
                        }
                    }
                    console.log('dataformemory: ' + cards.cards[0][i].objectTypeId + '; ' + cards.cards[0][i].label);
                    dataForMemory[i] = {
                        id: 'ccbi' + cards.cards[0][i].objectTypeId,
                        value: 'ccbi' + cards.cards[0][i].objectTypeId,
                        label: cards.cards[0][i].label
                    };
                    console.log('many selected finished');
                }
            } else {
                console.log('one selected' + cards[0].length);
                self.changedCards = [];
                for (var i = 0; i < cards[0].length; i++) {
                    console.log("i:" + i);
                    var CardObject = cards[0][i];
                    if (i == 0) self.frstCardId = CardObject.cardId;
                    console.log('dataformemory: ' + CardObject.cardId + '; ' + CardObject.cardName);
                    dataForMemory[i] = {
                        id: 'ccbi' + CardObject.cardId,
                        value: 'ccbi' + CardObject.cardId,
                        label: CardObject.cardName
                    };
                    console.log(CardObject);
                    //self.showNCard(CardObject);
                }
                console.log('one selected finished');

            }

            var os = new ObjectStore({
                objectStore: new Memory({
                    data: dataForMemory
                })
            });
            self.selCard = new Select({
                store: os,
                'class': 'comboBoxCard'
            }, domConstruct.create('div', {
                id: 'targetForCmb1'
            }, "targetForCardCombobox"));
            self.selCard.startup();
            /*self.selCard.on("change", function()
		 	{
		 		var cid = this.get("value").substr(4);
		 		console.log("my value: ", cid);
					//registry.byId("swap"+cid).show();
					//registry.byId("cardPageInd").reset();
					var activeView = registry.byId("swap" + cid).getParent().getShowingView();
					activeView.goTo(1, registry.byId("swap" + cid).getParent().id);
				});*/
            if (fid > 1) self.selCard.set('value', 'ccbi' + cards.cards[0][0].objectTypeId);
            else self.selCard.set('value', 'ccbi' + cards[0][0].cardId);
            //console.log(cards);
            //self.showCardsInDocument(cards);
            //pageInd.reset();
            //return cards;
        },

        /*
         * Gets properties of <cc> card;
         *
         * @cc - card id
         *
         */
        showNCard: function(CardObject) {
            var self = this;
            console.log("props1");
            self.addGroup("Основные характеристики", "card" + CardObject.cardId, CardObject.cardId, false);
            this.objProps[CardObject.cardId.toString()] = {};
            this.objProps[CardObject.cardId.toString()].ids = [];
            var ids = this.objProps[CardObject.cardId.toString()].ids;
            // основное описание карточки

            console.log("props2");
            /*self.selCard.addOption(
				{
					value: 'ccbi' + CardObject.cardId,
					label: (CardObject.name == '' ? 'Без наименования' : CardObject.name) + ' - ' + CardObject.typeName
				});*/
            /*this.cardsMemoryStore.set(
				{
					id : 'ccbi' + CardObject.cardId,
					label : CardObject.name
				});
				this.cardSelectWidget.addOption(
				{
					id : 'ccbi' + CardObject.cardId,
					label : CardObject.name
				});*/

            self.addPropertyIntoCurrentGroupName("Наименование", "card" + CardObject.cardId, "nameCard" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.name, "card" + CardObject.cardId, "valueNameCard-" + CardObject.cardId + '-', true);
            self.addPropertyIntoCurrentGroupName("Краткое наименование", "card" + CardObject.cardId, "shortName" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.shortName, "card" + CardObject.cardId, "valueShortName-" + CardObject.cardId + '-', true);
            self.addPropertyIntoCurrentGroupName("Атрибутивное описание", "card" + CardObject.cardId, "typeName" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.typeName, "card" + CardObject.cardId, "valueTypeName-" + CardObject.cardId + '-', false);
            self.addPropertyIntoCurrentGroupName("Дата последнего изменения", "card" + CardObject.cardId, "createDate" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.createDate, "card" + CardObject.cardId, "valueCreateDate-" + CardObject.cardId + '-', false);
            self.addPropertyIntoCurrentGroupName("Автор", "card" + CardObject.cardId, "editUser" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.editUser, "card" + CardObject.cardId, "valueEditUser-" + CardObject.cardId + '-', false);
            self.addPropertyIntoCurrentGroupName("Примечания", "card" + CardObject.cardId, "note" + CardObject.cardId);
            self.addPropertyIntoCurrentGroup(CardObject.note, "card" + CardObject.cardId, "valueNote-" + CardObject.cardId + '-', true);
            //console.log("feature is added" + CardObject.name + "; " + CardObject.props.length + "; " + CardObject.rels.length);

            // запрос на характеристики
            console.log("CardObject.props.length" + CardObject.props.length);

            for (var i = 0; i < CardObject.props.length; i++) {
                var CardObjectPropetry = CardObject.props[i];
                console.log('There is one more group: ' + CardObjectPropetry.groupName);
                if (CardObjectPropetry.valueTypeId == '5') ids[i] = -CardObjectPropetry.id;
                else ids[i] = CardObjectPropetry.id;
                self.addGroup('Группа: ' + CardObjectPropetry.groupName, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, CardObject.cardId, true);
                self.addPropertyIntoCurrentGroupName(CardObjectPropetry.name, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propName" + CardObject.cardId + CardObjectPropetry.id);
                var queryForDomains = function(CardObject, CardObjectPropetry1) {
                    //console.log('Server responsed domains');
                    //CardObjectPropetry1.domains = [];
                    var dataForDomain = [{
                        id: "",
                        label: "Выберите значение...",
                        disabled: true
                    }];
                    for (var cd in CardObjectPropetry1.domains) {
                        //console.log(cd.id+cd.name);
                        dataForDomain[dataForDomain.length] = {
                            id: "domainValue" + CardObject.cardId + "prop" + CardObjectPropetry1.id + "domn" + cd.id,
                            label: cd.name
                        };
                        //console.log('DOMAIN!!:' + dataForDomain[i3].label);
                    }
                    console.log('!!!!!', dataForDomain)
                    self.addPropertyDomainIntoCurrentGroup(dataForDomain, CardObject.cardId, CardObjectPropetry1.groupId, CardObjectPropetry1.id, CardObjectPropetry1.value);
                }
                // запрос на список значений доменов

                console.log(CardObjectPropetry.valueTypeId)
                switch (Number(CardObjectPropetry.valueTypeId)) {
                    case 5:
                        {
                            queryForDomains(CardObject, CardObjectPropetry);
                            break;
                        };
                    case 2:
                        {
                            self.addDatePicker(CardObjectPropetry.value, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propValue-" + CardObject.cardId + '-' + CardObjectPropetry.id);
                            break;
                        };
                    default:
                        {
                            self.addPropertyIntoCurrentGroup(CardObjectPropetry.value, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propValue-" + CardObject.cardId + '-' + CardObjectPropetry.id, true);
                        }
                }

            }

            // запрос на список отношений
            //CardObject.rels = [];
            for (var CardObjectPropetry in CardObject.rels) {
                var addRelation = function(name, cardId, relationId) {

                    self.addRelationGroup('Связь: ' + name, cardId + 'groupRelation' + relationId, cardId);
                    //console.log("Relation was add " + name);
                    // запрос на список наследников по отношениям
                    //CardObjectPropetry.cards = [];
                    for (var CardObjectRelations in CardObjectPropetry.cards) {
                        self.addRelationCardName(CardObjectRelations.name, cardId + 'groupRelation' + relationId, cardId + relationId + 'groupRelation' + CardObjectRelations.relId, CardObjectRelations.id);
                        //console.log("ChildGroup was add " + CardObjectRelations.name);
                    }
                }
                addRelation(CardObjectPropetry.name, CardObject.cardId, CardObjectPropetry.relationId);
            }
        },

        /* Setup of selection events */
        initializeSelectionEvents: function() {
            var self = this;

            window.AppData.selectionCallbacks.card = {
                'fn': function(selectionData, isSelectionCleared) {
                    var sd;
                    for (var i in selectionData) {
                        sd = selectionData[i]
                        console.log(sd);
                        console.log(isSelectionCleared);
                    }
                    if (!sd.nTotalElements || isSelectionCleared) {
                        //registry.byId('cardSelectAnything').set('style', 'display : none');
                        //if (domStyle.get("SaveCardButton", "display") != "none")
                        if (self.changedCards.length > 0)
                        //console.log("test2");
                        self.showSaveCardDialog(-1);
                        else {
                            self.currentSelected = 0;
                            self.clearCard();
                            domStyle.set('cardSelectAnything', 'display', 'block');
                        }
                    } else {
                        console.log("test25");
                        self.afterActivate();
                        domStyle.set('cardSelectAnything', 'display', 'none');
                    }

                },
                'active': true
            }


        },

        //triggered before showing view
        _beforeActivate: function() {
            if (this.params.newCardId) {
                this.getPropertiesOfNCard({
                    'cardId': this.params.newCardId
                });
                //this.selCard.addOption({'value':'ccbi'+this.params.newCardId,'label':'ccbi'+this.params.newCardId});
                registry.byId('cardPageInd').reset();
                if (registry.byId("swap" + this.frstCardId)) {
                    console.log("swap" + this.frstCardId);
                }

            }

            this.datePicker.placeAt(this.dateDialog.containerNode)
            window.d = this.datePicker
        },
        _afterActivate: function() {
            var self = this;
            var mapName,
            sessionId;
            var allMaps = Fusion.getWidgetById('Map').getAllMaps();
            for (var j = 0; j < allMaps.length; j++) {
                if (allMaps[j].arch == 'MapGuide') {
                    sessionId = allMaps[j].getSessionID()
                    mapName = allMaps[j].getMapName();
                    console.log(mapName);
                }
            }
            if (window.selText) {
                domStyle.set('cardSelectAnything', 'display', 'none');
                xhr.post("/map/lib/getselectedfeatures", {
                    handleAs: "json",
                    data: {
                        'mapname': mapName,
                        'selection': window.selText,
                        'session': sessionId,
                        'version': '4'
                    }
                }).then(function(response) {
                    var featureIds = [];

                    response[0].features.forEach( function(feature, featureIndex){
                        featureIds[featureIndex] = feature.entityId;
                    });
                    var newcard = new ncard(featureIds);
                    newcard.show();
                });

                return;
                //card.test();
                //Fusion.getWidgetById('Map').triggerEvent(Fusion.Event.MAP_BUSY_CHANGED);

                var data = [{
                    first: 'Bob',
                    last: 'Barker',
                    age: 89
                }, {
                    first: 'Vanna',
                    last: 'White',
                    age: 55
                }, {
                    first: 'Pat',
                    last: 'Sajak',
                    age: 65
                }];

                var grid = new Grid({
                    columns: {
                        first: 'First Name',
                        last: 'Last Name',
                        age: 'Age'
                    }
                }, 'grid');
                registry.byId('cardDisplayContainer').addChild(grid);
                grid.renderArray(data);

                return;
                var mapName,
                sessionId;
                var allMaps = Fusion.getWidgetById('Map').getAllMaps();
                for (var j = 0; j < allMaps.length; j++) {
                    if (allMaps[j].arch == 'MapGuide') {
                        sessionId = allMaps[j].getSessionID()
                        mapName = allMaps[j].getMapName();
                        console.log(mapName);
                    }
                }
                if (window.selText) {
                    domStyle.set('cardSelectAnything', 'display', 'none');
                    xhr.post("/map/lib/getselectedfeatures", {
                        handleAs: "json",
                        data: {
                            'mapname': mapName,
                            'selection': window.selText,
                            'session': sessionId,
                            'version': '4'
                        }
                    }).then(function(response) {
                        try {
                            var featureId = [];
                            for (var i = 0; i < response[0].features.length; i++) {
                                featureId[i] = response[0].features[i].entityId;
                                console.log('featureId=' + featureId[i]);
                            }
                            if (featureId != self.currentSelected) {
                                //if (domStyle.get("SaveCardButton", "display") != "none")
                                console.log("test1");
                                if (self.changedCards.length > 0) self.showSaveCardDialog(featureId);
                                else self.showSelectedCards(featureId); // все характеристики выделенного объекта
                            }
                        } catch (e) {}
                    });
                }
                if (this.params.newCardId) {
                    var activeView = registry.byId("swap" + this.frstCardId).getParent().getShowingView();
                    if (activeView) activeView.goTo(1, registry.byId("swap" + this.params.newCardId).getParent().id);
                    //this.param.newCardId = null;
                }
            }
        },

        beforeDeactivate: function() {
            domClass.remove(registry.byId('appMenu').domNode, 'mblGreyButton');
            console.log("help");
        }
    }

})