require({cache:{
'url:wgs4/views/taskpane.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-type=\"dojox/mobile/ContentPane\" \r\n\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\tstyle=\"position: absolute; left: 0px; top: 0; bottom: 0; right: 0px;\">\r\n\t\t<div \r\n\t\t\tdata-dojo-type=\"dojox/mobile/ContentPane\" \r\n\t\t\tdata-dojo-attach-point=\"displayContainer\">\r\n\t\t\t<div\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeCategory\"\r\n\t\t\t\tdata-dojo-props=\"label: '${nls.editorTitle}'\"\r\n\t\t\t></div>\r\n\t\t\t<ul\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeList\">\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"redlineActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, target: 'leftView,editor', label :'<i class=\\'fa margined fa-edit\\'></i> ${nls.redline}'\">\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t\t<div\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeCategory\"\r\n\t\t\t\tdata-dojo-props=\"label: '${nls.analysys}'\"\r\n\t\t\t></div>\r\n\t\t\t<ul\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeList\">\r\n\t\t\t\t\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"fInfoActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, target: 'leftView,fdetails', label :'<i class=\\'fa margined fa-road\\'></i> ${nls.featureInfo}'\">\t\t\t\t\t\r\n\t\t\t\t</li>\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"measurementActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, target: 'leftView,redline', label :'<i class=\\'fa margined fa-arrows-h\\'></i> ${nls.measure}', transitionOptions: {params : {widget: 'me'}}\">\r\n\t\t\t\t</li>\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"themesActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, target: 'leftView,thematic', label :'<i class=\\'fa margined fa-bar-chart-o\\'></i> ${nls.themes}'\">\r\n\t\t\t\t</li>\t\t\t\t\r\n\t\t\t</ul>\r\n\t\t\t<div\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeCategory\"\r\n\t\t\t\tdata-dojo-props=\"label: '${nls.administration}'\"\r\n\t\t\t></div>\r\n\t\t\t<ul\r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/EdgeToEdgeList\">\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"installApplication\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, label :'<i class=\\'fa margined fa-download\\'></i> ${nls.installApplication}'\">\t\t\t\t\t\r\n\t\t\t\t</li>\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"projAdminActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, href: '/projectadmin', hrefTarget: '_blank', label :'<i class=\\'fa margined fa-users\\'></i> ${nls.projectadmin}', rightText: '<i class=\\'fa fa-desktop fa-075\\'></i>'\">\t\t\t\t\t\r\n\t\t\t\t</li>\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"objAdminActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, href: '/objectadmin', hrefTarget: '_blank', label :'<i class=\\'fa margined fa-briefcase\\'></i> ${nls.objectadmin}', rightText: '<i class=\\'fa fa-desktop fa-075\\'></i>'\">\r\n\t\t\t\t</li>\r\n\t\t\t\t<li \r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/ListItem\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"oldMapActivator\"\r\n\t\t\t\t\tdata-dojo-props=\"clickable: true, href: '/map', hrefTarget: '_blank', label :'<i class=\\'fa margined fa-globe\\'></i> ${nls.oldMap}', rightText: '<i class=\\'fa fa-desktop fa-075\\'></i>'\">\r\n\t\t\t\t</li>\t\t\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\t\r\n</div>\r\n"}});
define("wgs4/controllers/taskpane", [
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/taskpane.html'
	],function(query, domAttr, domStyle, domClass, domConstruct, has, on,xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...
			_hasMenu: false,
			_viewTitle: 'task',
			
			init : function(){
				new window.AppData.ViewBaseClass( this );	
				
				domStyle.set("TaskPane", "display", "none")
				domClass.add(this.displayContainer.domNode, 'scrollable-pane-100-height');
				if(!has('phone')){
					domStyle.set(this.displayContainer.domNode,'top','45px')
				}
				var url = 'http://88.204.76.6/manifest.webapp?dynamic';
				var self = this;
				
				if(window.navigator.mozApps){
					
					var	request = window.navigator.mozApps.checkInstalled(url);
					request.onerror = function(e) {
					  alert("Error calling checkInstalled: " + request.error.name);
					};
					request.onsuccess = function(e) {
					  if (request.result) {
						domStyle.set( self.installApplication.domNode, 'display', 'none' )
						console.log("App is installed!");
					  }
					  else {
						domStyle.set( self.installApplication.domNode, 'display', '' )
						console.log("App is not installed!");
					  }
					};
				} else {
					domStyle.set( self.installApplication.domNode, 'display', 'none' )
				}
				
				this.installApplication.onClick = function() {
					
					if(window.navigator.mozApps){
					var request = window.navigator.mozApps.install(url);
						request.onsuccess = function () {
						// Save the App object that is returned
						  var appRecord = this.result;
						  alert('Installation successful!');
						};
						request.onerror = function () {
						  // Display the error information from the DOMError object
						  alert('Install failed, error: ' + this.error.name);
						};
					} else {
						alert('Installation is not supported by your browser.')
					}
					
				}
				
				/*
				 * disable all prohibited actions
				 * 
				 */ 
				if(window.AppData.prohibited['WgsMeasure'])
					this.measurementActivator.destroy();
				if(window.AppData.prohibited['WgsThemes'])
					this.themesActivator.destroy();
				if(!window.WGS.PRIVILEGE.PROJECTOBJECT_ADMIN)
					this.objAdminActivator.destroy();
				if(!window.WGS.PRIVILEGE.PROJECT_ADMIN)
					this.projAdminActivator.destroy();
					
					
			},
			_beforeActivate: function(){
				//
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				
				
			}
			
			
		}
	}
)
