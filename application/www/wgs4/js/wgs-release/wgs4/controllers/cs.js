require({cache:{
'url:wgs4/views/cs.html':"<div data-dojo-type=\"dojox/app/widgets/Container\">\r\n\t<div \r\n\t\tdata-dojo-attach-point=\"heading\"\r\n\t\tdata-dojo-type=\"dojox/mobile/Heading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t\t<button data-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n            data-dojo-attach-point=\"backButton\"\r\n            data-dojo-props=\"arrow: 'left', target: 'leftView,finfo', transitionDir: -1\"\r\n            style=\"float:left\"\r\n            class=\"backButton\">${nls.back}</button>\r\n\t\t\r\n\t</div>\r\n\t<div \r\n\t\tdata-dojo-type=\"wgs4/wheelScrollableView\" data-dojo-props=\"height:'auto'\"\r\n\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\tstyle=\"top:44px;left:0;bottom:0;position:absolute;right:0;\"\r\n\t\t>\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'${nls.selectCs}'\"></div>\t\t\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRect\" data-dojo-props=\"shadow:true\">\r\n\t\t\tSelect coordinate system from the list below\r\n\t\t</div>\r\n\t\t<div data-dojo-attach-point=\"welcome\" data-dojo-type=\"dojox/mobile/Pane\"></div>\r\n\t\t<div data-dojo-type=\"dojox/mobile/RoundRectCategory\" data-dojo-props=\"label:'${nls.availableCs}'\"></div>\r\n\t\t<ul data-dojo-attach-point=\"csList\" data-dojo-type=\"dojox/mobile/RoundRectList\" data-dojo-props=\"select:'single'\">\r\n\t\t\t<li data-cs=\"wgs\" data-dojo-attach-point=\"isWgs\" data-dojo-type=\"dojox/mobile/ListItem\" data-dojo-props = \"label: '${nls.wgs}'\"></li>\r\n\t\t\t<li data-cs=\"pm\" data-dojo-attach-point=\"isPm\" data-dojo-type=\"dojox/mobile/ListItem\" data-dojo-props = \"label: '${nls.mercator}'\"></li>\r\n\t\t\t<li data-cs=\"pu\" data-dojo-attach-point=\"isPu\" data-dojo-type=\"dojox/mobile/ListItem\" data-dojo-props = \"label: '${nls.pu}'\"></li>\r\n\t\t\t<li data-cs=\"pz\" data-dojo-attach-point=\"isPz\" data-dojo-type=\"dojox/mobile/ListItem\" data-dojo-props = \"label: '${nls.pz}'\"></li>\r\n\t\t</ul>\t\t\r\n\t</div>\r\n</div>\r\n"}});
define("wgs4/controllers/cs", [
	'dojo/query','dojo/_base/declare',
	'dojo/dom-attr','dojo/dom',
	'dojo/dom-style','dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on','dojo/hash','dojox/mobile/EdgeToEdgeCategory','dojox/mobile/EdgeToEdgeList','dojox/mobile/ListItem',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/cs.html'
	],function(query, declare, domAttr, dom, domStyle, domClass, domConstruct, has, on, hash, Category, List, ListItem, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			_hasMenu: false,
			_hasBack: "leftView,finfo",
			
			_featureInfo:[],
			_cs: {
				'pm': {
					'root'	: 'Pseudo Mercator',
					'en-us'	: 'Pseudo Mercator',
					'ru-ru'	: 'Pseudo Mercator'
				},
				'pz':{
					'root'	: 'Earth Parameters-90',
					'en-us'	: 'Earth Parameters-90',
					'ru-ru'	: 'Парметры Земли-90'
				},
				'pu':{
					'root'	: 'Pulkovo-42',
					'en-us'	: 'Pulkovo-42',
					'ru-ru'	: 'Пулково-42'
				},
				'wgs':{
					'root'	: 'WGS-84',
					'en-us'	: 'WGS-84',
					'ru-ru'	: 'WGS-84'
				}
			},
			selectedCs: 'pz',
			init : function(){
				new window.AppData.ViewBaseClass( this );	
				
				if(!has('phone')){
					//domStyle.set(this.heading.domNode,'display','none')
					//domStyle.set(this.viewContainer.containerNode,'top','0')
					domClass.add(this.viewContainer.domNode,'heightAuto');
				}
				
				var csListItems = this.csList.getChildren();
				var self = this
				for(var i=0; i<csListItems.length; i++)
					csListItems[i].onClick = function(){
						self._setCs(this['data-cs'])
					}
			},
			_setCs: function(cs){
				this.selectedCs = cs
				this._hasTransitionOptions =  { cs : this.selectedCs, from: 'cs' } 
				//if(has("phone"))
				this.backButton.set("transitionOptions", {params: this._hasTransitionOptions})
				
			},
			_updateState: function(){
				var cs = this.selectedCs.substr(0,1).toUpperCase() + this.selectedCs.substr(1,this.selectedCs.length-1)
				if(this["is"+cs])
					this["is"+cs].set('checked',true)
			},
			_beforeActivate: function(){
				//
				
				if(this.params && this.params.from && this.params.cs){
					this.selectedCs = this.params.cs
					this._hasBack =  'leftView,' + this.params.from
					this._updateState()
					this.backButton.target = this._hasBack;
				}
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				
				//window.AppData.BeforeActivateTitleChangeHandler.call(this)				

			}		
		}
	}
)
