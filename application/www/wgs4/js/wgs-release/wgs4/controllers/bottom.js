require({cache:{
'url:wgs4/views/bottom.html':"<div  data-dojo-type=\"dojox/app/widgets/Container\" class=\"bottom-nav-bar\">\r\n<ul data-dojo-type=\"dojox/mobile/TabBar\" data-dojo-props=\"fixed:'bottom'\" class=\"bottom-bar\" id=\"panes-bar\">\r\n\t\t\t\t<li data-dojo-type=\"dojox/mobile/TabBarButton\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"mapButton\"\r\n\t\t\t\t\tid=\"mapButton\"\r\n\t\t\t\t\tdata-dojo-props='icon:\"/wgs4/icons/grey/map_4.png\",\r\n                       iconPos1: \"0,0,128,128\",\r\n                       target:\"map\",  \"label\": \"${nls.map}\"'></li>\r\n\t\t\t\t<li data-dojo-type=\"dojox/mobile/TabBarButton\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"legendButton\"\r\n\t\t\t\t\tid=\"legendButton\"\r\n\t\t\t\t\tdata-dojo-props='icon:\"/wgs4/icons/grey/layers_4.png\",\r\n                       iconPos1: \"0,0,128,128\",selected:true,\r\n                       target:\"leftView,legend\", \"label\":\"${nls.legend}\"'></li>\r\n\t\t\t\t<li data-dojo-type=\"dojox/mobile/TabBarButton\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"taskButton\"\t\t\t\t\t\r\n\t\t\t\t\tid=\"taskButton\"\t\t\t\t\t\r\n\t\t\t\t\tdata-dojo-props='icon:\"/wgs4/icons/grey/search_3.png\",\r\n                       iconPos1: \"0,0,128,128\",\r\n                       target:\"leftView,taskpane\",label:\"${nls.tasks}\"'></li>\r\n\t\t\t\t<!--<li data-dojo-type=\"dojox/mobile/TabBarButton\"\r\n\t\t\t\t\tid=\"bottom-themes-button\"\t\r\n\t\t\t\t\tdata-dojo-attach-point=\"themeButton\"\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\tdata-dojo-props='icon:\"/wgs4/icons/white/theme_2.png\",\r\n                       iconPos1: \"0,0,128,128\",\r\n                       target:\"thematic\"'>Thematic</li>-->\r\n\n\t\t\t\t<li\tdata-dojo-type=\"dojox/mobile/TabBarButton\"\r\n\t\t\t\t\tdata-dojo-attach-point=\"cardButton\"\r\n\t\t\t\t\tid=\"cardButton\" style=\"border: 0\"\r\n\t\t\t\t\tdata-dojo-props='icon:\"/wgs4/icons/grey/card_3.png\",\r\n                       iconPos1: \"0,0,145,145\",\r\n\t\t\t\t\t   target: \"leftView,card\",label:\"${nls.card}\"'></li>\n\t\t\t</ul>\r\n</div>\r\n"}});
define("wgs4/controllers/bottom", [
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-construct',
	'dojo/sniff',
	
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',"dojox/mobile/TabBar",'dojo/text!wgs4/views/bottom.html'
	],function(query, domAttr, domStyle, domConstruct, has, on,xhr, arrayUtil){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			init : function(){
				//domConstruct.place("TaskPane", this.mainContainer.containerNode, "first");
				//domStyle.set("TaskPane", "display", "block")	
				if(!has('phone')){
					this.mapButton.destroyRecursive()
					//this.taskButton.set('target','taskpane-top')
					
				}
				
				
				if(window.AppData.prohibited['WgsCard'])
					this.cardButton.destroy();
				
			
			},
			
			
			
			beforeActivate: function(){
				//
				//this.taskButton.set('selected',true)
			},
			afterActivate: function(){
				
			}
			
		}
	}
)

