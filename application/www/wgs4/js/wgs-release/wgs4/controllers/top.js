require({cache:{
'url:wgs4/views/top.html':"<div>\r\n\t<div data-dojo-type=\"dojox/mobile/Heading\" id=\"appHeading\" data-dojo-props=\"label:'${nls.title}'\">\r\n\t\t<button \r\n\t\t\tid = \"appMenu\"\r\n\t\t\tdata-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n\t\t\tdata-dojo-attach-point=\"menuButton\" style=\"float:left;\">\r\n\t\t\t<i class=\"fa fa-bars\"></i> ${nls.menu}\t\t\t\r\n\t\t</button>\r\n\t\t<button \r\n\t\t\tdata-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n\t\t\tid = \"appBack\"\r\n\t\t\tdata-dojo-attach-point=\"backButton\" style=\"float:left\"\r\n\t\t\tdata-dojo-props=\"arrow: 'left', label: '<i class=\\'fa fa-sign-out\\'></i> ${nls.exit}'\">\r\n\t\t</button>\r\n\t\t<button \r\n\t\t\tdata-dojo-type=\"dojox/mobile/ToolBarButton\"\r\n\t\t\tid = \"appShowMap\"\r\n\t\t\tdata-dojo-attach-point=\"appShowMap\" style=\"float:right; display:none\"\r\n\t\t\tdata-dojo-props=\"label: 'show map'\">\r\n\t\t</button>\t\t\r\n\t</div>\t\r\n\t<div id=\"dlg1\" data-dojo-type=\"dojox/mobile/SimpleDialog\" data-dojo-props='modal:false'>\r\n\t\t<div id=\"menu\" style=\"width:274px;height:210px;\"></div>\r\n\t</div>\r\n</div>\r\n"}});
define("wgs4/controllers/top", [
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-construct',
	
	"dojox/mobile/IconMenu",
    "dojox/mobile/IconMenuItem",
	
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array', 'dijit/registry',
	'dojox/mobile/SimpleDialog','dojo/text!wgs4/views/top.html'
	],function(query, domAttr, domStyle, domConstruct, Menu, MenuItem, on,xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			
			menuCols : 3,
			
			_createMenuItem: function(faClass, nlsLabel, href, onClick, requiredPrivilege, closeOnAction){
				var requiredPrivilege = requiredPrivilege || null;
				if(!requiredPrivilege || ( requiredPrivilege && WGS.PRIVILEGE[requiredPrivilege] )){
					var itemHref = href || null;
					var itemHrefTarget = itemHref ? "_blank" : null;
					var closeOnClick = ((closeOnAction !== undefined) ? closeOnAction : (itemHref ? true : false));
					
					var item = new MenuItem({
						label: '<i class="fa fa-3 fa-' + faClass + '"></i><br/>'+nlsLabel,
						href: itemHref,
						hrefTarget: itemHrefTarget,
						closeOnAction: closeOnClick
					});								
					
					if(onClick) {
						item.onClick = onClick;
					}
					this.menu.addChild(item);
				}				
			},
			
			_createMenu: function(){
				this.menu = new Menu({
					cols: this.menuCols,
				}, "menu");
				this.menu.startup();
				var thisView = this
				var olEventHandler = function(){
					thisView._activeMap = Fusion.getWidgetById('MapMenu').getMap().mapGroup.mapId;
					var newMapId = (thisView._activeMap == "noOpenLayers") ? "openLayers" : "noOpenLayers";
						
					Fusion.getWidgetById('BasemapSwitcher').setBasemap('None');
					window.AppData.MapGroupSwitcher[newMapId]();						
						
					var self = this
					setTimeout(function(){
						self.set('selected',
						   (thisView._activeMap == "noOpenLayers"));
						self.set('label',(thisView._activeMap == "openLayers") ? 
						   '<i class="fa fa-3 fa-file-o"></i><br/>Enable<br/>OpenLayers' : 
						   '<i class="fa fa-3 fa-copy"></i><br/>Disable<br/>OpenLayers');
					},500);
				};				
				
				var doMeasure = function(){
					Fusion.getWidgetById('Measure').activate();
				};
				
				var doSelectRadius = function(){
					Fusion.getWidgetById('SelectRadius').activate();
				};
				var doSelectPolygon = function(){
					Fusion.getWidgetById('SelectPolygon').activate();
				};
				
				var doPrint = function(){
					console.log(thisView)
					thisView.app.transitionToView(thisView.domNode, {target: "leftView,redline", params : {widget : 'pr'}})
				};
				
				var doSettings = function(){
				    thisView.app.transitionToView(thisView.domNode, {target: "leftView,settings"})
				};
				var doLegend = function(){
				    thisView.app.transitionToView(thisView.domNode, {target: "leftView,legend"})
				};
				
				var doTasks = function(){
				    thisView.app.transitionToView(thisView.domNode, {target: "leftView,taskpane"})
				};
				var doCard = function(){
				    thisView.app.transitionToView(thisView.domNode, {target: "leftView,card"})
				};
				//this._createMenuItem("file-o", "Enable<br/>OpenLayers", null, olEventHandler, null, true )								
				//this._createMenuItem("arrows-h", "Measure<br/>&nbsp;", null, doMeasure, null, true )								
				this._createMenuItem("th-list", this.nls.legend, null, doLegend, null, true);
				this._createMenuItem("tasks", this.nls.tasks, null, doTasks, null, true);
				
				if(!window.AppData.prohibited['WgsCard']) {
					this._createMenuItem("list-alt", this.nls.card, null, doCard, null, true);
				}
				//this._createMenuItem("print", "Print<br/>map", null, doPrint, null, true)
				//this._createMenuItem("square-o", "Select<br/>by polygon", null, doSelectPolygon, null, true )								
				//this._createMenuItem("circle-o", "Select<br/>by circle", null, doSelectRadius, null, true )								
				//this._createMenuItem("desktop", "Go to<br/>WGS3", "/map", null, null)
				this._createMenuItem("gear", this.nls.settings,	null, doSettings, null, true);		
				this._createMenuItem("sign-out", this.nls.exit, null, window.AppData.onExit, null, true );
				//this._createMenuItem("user", "Manage<br/>project", "/projectadmin", null, "PROJECT_ADMIN")
				//this._createMenuItem("briefcase", "Manage<br/>data", "/objectadmin", null, "PROJECTOBJECT_ADMIN")
				
				var item = new MenuItem({
					label: '<i class="fa fa-3 fa-times"></i><br/>'+this.nls.close+'<br/>'+ this.nls.menu ,
					closeOnAction: true
				});
				
				this.menu.addChild(item);				
			},
			
			init : function(){
				//domConstruct.place("TaskPane", this.mainContainer.containerNode, "first");
				//domStyle.set("TaskPane", "display", "block")	
				window.AppData.onExit = function(){
					var onSuccess = function(){
						location.assign('/info');
					};
					var onError = function(e){
						console.log(e);
					};
					xhr.get("/authorization/logout", {
						handleAs: "text"
					}).then(onSuccess, onError);

				};
				
				//this.backButton.onClick
				//this.appMenu.set('label','!')
				this._createMenu();
				//var self = this;
				this.menuButton.onClick = function(){
					try{
						registry.byId('dlg1').show('wgs4_leftView');
					}catch(e){
						console.log(e);
					}
				}; 
			},
			beforeActivate: function(){
				//
				//this.taskButton.set('selected',true)
			},
			afterActivate: function(){
				//
			}
			
		};
	}
);
