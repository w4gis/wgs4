require({cache:{
'url:wgs4/views/map.html':"<div id=\"map\" data-dojo-type=\"dojox/mobile/View\">\r\n\t\t<div \r\n\t\t\tdata-dojo-attach-point=\"viewContainer\"\r\n\t\t\tdata-dojo-type=\"dojox/mobile/Pane\" class=\"map-view\">\r\n\t\t\t<div style=\"position:absolute;top:0;left:0;right:0;bottom:0;overflow:hidden\">\r\n\t\t\t\t<div id=\"Map\" style=\"position:absolute;height:120%;width:120%;left:-10%;top:-10%\"></div>\r\n\t\t\t</div>\r\n\t\t\t\r\n\t\t\t<div \r\n\t\t\t\tid = \"WgsProgressIndicator\"\r\n\t\t\t\tdata-dojo-attach-point=\"WgsProgressIndicator\"\r\n\t\t\t\tdata-dojo-props=\"size:32, center: false\" \r\n\t\t\t\tdata-dojo-type=\"dojox/mobile/ProgressIndicator\">\r\n\t\t\t</div>\r\n\t\t\t\t\r\n\t\t\t<div id=\"Toolbar\" >\r\n\t\t\t\t<div class = \"toolbar-container mobile-bar\" id=\"ToolbarVertical\"></div>\r\n\t\t\t\t<!--<div id=\"Geolocation\"></div>-->\r\n\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t\t<div style=\"display:none\" id=\"Topbar\" class=\"toolbar-container\"></div>\t\t\t\r\n\t\t\t\t\r\n\t\t\t\r\n\t\t\t<div class=\"right-bar mobile-bar\">\r\n\t\t\t\t<div class = \"toolbar-button\" id=\"ZoomInFixed\"></div>\r\n\t\t\t\t<div class = \"toolbar-button\" id=\"ZoomOut\"></div>\r\n\t\t\t</div>\r\n\t\t\t<div id = \"toolbar-parent\" class=\"full\">\r\n\t\t\t\t<div class=\"map-button-bar mobile-bar hidden\">\r\n\t\t\t\t\t<div id=\"MoreToolsContainer\" alt=\"Показать/Скрыть панель инструментов\" class=\"css3Button\" >\r\n\t\t\t\t\t\t<button id=\"MoreTools\"><i class=\"fa\"></i></button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\r\n\t\t\t\t\t<div id=\"NextView\"></div>\r\n\t\t\t\t\t<div id=\"vertInitialMapView\"></div>\r\n\t\t\t\t\t<div id=\"PreviousView\"></div>\r\n\t\t\t\t\t<div class=\"splitter\">&nbsp;</div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t<div id=\"Geolocation\"></div>\r\n\t\t\t\t\t<div id=\"MapMenu\"></div>\r\n\t\t\t\t\t<div id=\"ClearSelection\"></div>\r\n\t\t\t\t\t<div class=\"splitter\">&nbsp;</div>\r\n\t\t\t\t\r\n\t\t\t\t\t<div id=\"BasemapSwitcher\"></div>\t\t\t\t\r\n\t\t\t\t\t<div id=\"OpenLayersEnabler\" style=\"display: none\" alt=\"\" class=\"css3Button\" ><button id=\"EnableOpenLayers\"><i class=\"fa\"></i></button></div>\r\n\t\t\t\t\r\n\t\t\t\t\t<!--<div id=\"SelectRadiusValue\"></div>-->\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div style=\"display:none\">\r\n\t\t\t\t<div id=\"QuickPlot\"></div>\r\n\t\t\t\t<div id=\"SelectRadius\"></div>\r\n\t\t\t\t<div id=\"SelectPolygon\"></div>\r\n\t\t\t\t<div id=\"Measure\"></div>\r\n\t\t\t\t<div id=\"Redline\"></div>\r\n\t\t\t\t<div id=\"FeatureInfo\"></div>\r\n\t\t\t</div>\r\n\t\t\t<div id=\"TaskPane\" style=\"width:100%;height:100%;display:none\"></div>\r\n\t\t\t<!-- invisible elements-->\r\n\t\t\t<div id=\"legend\"></div>\r\n\t\t\t\t<div class=\"bottom-panel\">\r\n\t\t\t\t\t<div class=\"statusbar\"  style=\"\">\r\n\t\t\t\t\t\r\n\t\t\t\t\t\t<div id=\"StatusCoords\"></div>\r\n\t\t\t\t\t\t<div id=\"StatusSelection\"></div>\r\n\t\t\t\t\t\t<!--<div id=\"StatusViewSize\"></div>\t\t\t\t\t-->\r\n\t\t\t\t\t\t<div id=\"StatusScale\"></div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t\r\n\t\t\t\t<button \r\n\t\t\t\t\tstyle=\"position: absolute;right: 8px;bottom: 1.5rem\"\r\n\t\t\t\t\tdata-dojo-type=\"dojox/mobile/Button\" \r\n\t\t\t\t\ttitle=\"${nls.reload}\"\r\n\t\t\t\t\tdata-dojo-props=\"onClick: function(){location.reload();}, label: '<i class=\\'fa fa-refresh\\'></i>'\"></button>\r\n\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t<span class=\"copyright\">\r\n\t\t\t\t\t<span class=\"name\">wgs4</span> \r\n\t\t\t\t\t<span id=\"wgs-copyright-version\" \r\n\t\t\t\t\t\tonclick = \"window.AppData.maybeDebug();\" class=\"version\">\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\t\t</div>\t\t\r\n\t\t\r\n\t</div>\r\n"}});
define("wgs4/controllers/map", [
	'dojo/query',
	'dojo/dom-attr','dojo/dom-class',
	'dojo/dom-style', 'dojo/dom',
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dojo/sniff',
	'dijit/registry',
	'wgs4/model/wgs-fusion','dojox/mobile/Icon','dojo/dom-construct', 'dojo/text!wgs4/views/map.html'
	],function( query, domAttr, domClass, domStyle, dom, on, xhr, arrayUtil, has, registry, WgsFusion, Icon, domConstruct ){
		return {
			
			_hasMenu 		: true, 	// whether to display menu button on The view header or not
			_hideTitle		: true,
			_viewTitle		: 'map',	// used to automatically select tabbar button (sync selected view with buttons)
										// should be the same as view name in app-config.json
										
			_minToolbarWidth: 750,		// min width for main map toolbar (If window width is not enough, smaller buttons will be used)
			_activeMap		: '', 		// as of versions from 0.2 to current can be "openLayers" or "noOpenLayers", initialised at _initializeOpenLayersEnabler
										// and used to turn on/off Open Layers Basemaps
										
			_isOpenLayersEnabled : true, //whether there is open layers enable button or not
			_isFlexibleMainMapBarEnabled : true, //auto expand/collapse map buttons bar
			_moreToolsButtonId : 'MoreTools', //expandable toolbar's button,
			_mapWidgetId	: 'Map', 	//Fusion MapWidget id
			
			
			/*
			 * 	PUBLIC METHODS
			 */			
			//init method called when view created
			init : function(){				
				new window.AppData.ViewBaseClass( this );
				
				domAttr.set ('wgs-copyright-version', 'innerHTML', 
					window.AppData.getVersion());
				
				this._startFusionApplication();			
				
			},		
			
			
			
			
			/*
			 * 	PRIVATE METHODS
			 */				 
			 
			 // resize map when view is activated to prevent map visualization errors
			_afterActivate: function(){
				if(Fusion && Fusion.getWidgetById(this._mapWidgetId)) {
					Fusion.getWidgetById(this._mapWidgetId).resize();
				}
			},
			
			/*
			 * once Fusion is loaded we can setup UI and events
			 * 
			 */ 
			_configureFusionApplication : function(){
				
				//Fusion is loaded, we can listen to this var by setInterval or smth like this
				window.AppData.LOADED = true; 
				
				//setup Fusion events and buttons
				this._initializeMapButtons();
				this._initializeMapBusyEvents();
				this._initializeSelectionEvents();
				if(this._isOpenLayersEnabled) {
					this._initializeOpenLayersEnabler();
				}
				
				//Map is loaded and available for quering or anth else
				Fusion.getWidgetById(this._mapWidgetId).registerForEvent(Fusion.Event.MAP_LOADED, function(){
					window.AppData.MAP_LOADED = true;
				});
				
				//task pane (Some rich fusion widgets are loaded)
				Fusion.registerForEvent(Fusion.Event.TASK_PANE_LOADED, function(){
					window.AppData.TASKS_LOADED = true;
				});
				
				//setup show/hide toolbar button
				on(dom.byId(this._moreToolsButtonId), 'click', function(){
					var bb = query('.map-button-bar')[0];
					domClass[ ( (domClass.contains(bb, 'hidden')) ? 'remove' : 'add' ) ] (bb, 'hidden');
				});	
										
			},
			
			/*
			 * Hack Fusion classes;
			 * 
			 * Get localized ApplicationDefinition and startup everything.
			 * 
			 */ 
			_startFusionApplication: function(){
				var self = this;
				
				// redefine Fusion widgets to add some Dojo (TM).
				new WgsFusion();
				
				//get localized Application Definition
				var locale = localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale').substr(0,2) : '';				
				var options = {
					//sessionId: getCookie('PHPSESSID'),
					applicationDefinitionURL : "/map/fusion/getApplicationDefinition?lang=" + locale
				};
				
				//if ANYTHING wrong happens
				function onFusionError(e){
				   alert("Error occured, see console for details");
					console.error('Fusion error: ', e);
				}
				
				window.Fusion.registerForEvent(window.Fusion.Event.FUSION_INITIALIZED, function(){ 
				   self._configureFusionApplication();
				});
				window.Fusion.registerForEvent( window.Fusion.Event.FUSION_ERROR, onFusionError );
				window.Fusion.initialize(options);
				
			},
			
			/*
			 * 	Restyle Fusion widget's buttons
			 */
			_initializeMapButtons: function(){
				var zoomIn  = query("#ZoomInFixed .jxButtonLabel")[0];
				var	zoomOut = query("#ZoomOut .jxButtonLabel")[0];
				var	geo 	= query("#Geolocation .jxButtonLabel")[0];
				var	basemap = query("#BasemapSwitcher .jxButtonLabel")[0];
				var	toolBarItems 	= query(".toolbar-container .jxToolItem .jxButtonLabel");
				var	clearSelection 	= query("#ClearSelection .jxButtonLabel")[0];
				var	initialView 	= query("#vertInitialMapView .jxButtonLabel")[0];
				var	nextView 		= query("#NextView .jxButtonLabel")[0];
				var	previousView 	= query("#PreviousView .jxButtonLabel")[0];
				var	self 	= this;
				
				var basemapWrapper = domConstruct.create('span', {}, basemap, 'first');
				/*
				var baseMapIcon = new Icon({
					icon:"/wgs4/icons/grey/layers_4.png",
					iconPos1: "0,0,128,128"
				}, basemapWrapper);
				*/
				domClass.add(basemapWrapper, 'mblSpriteIconWrapper');
				
				domAttr.set(toolBarItems[0]	, 'innerHTML', '<i class="fa fa-arrows"></i>');
				domAttr.set(toolBarItems[1]	, 'innerHTML', '<i class="fa fa-hand-o-up"></i>');
				domAttr.set(toolBarItems[2]	, 'innerHTML', '<i class="fa fa-search-plus"></i>');
				domAttr.set(zoomIn			, 'innerHTML', '<i class="fa fa-plus"></i>');
				domAttr.set(zoomOut			, 'innerHTML', '<i class="fa fa-minus"></i>');
				
				domAttr.set(clearSelection	, 'innerHTML', 
					'<i class="fa fa-square-o"></i><br/><span class="icon-title">' + this.nls.clearSelection + '</span>');
				domAttr.set(geo				, 'innerHTML', 
					'<i class="fa fa-location-arrow"></i><br/><span class="icon-title">' + this.nls.myLocation + '</span>');
				domAttr.set(basemap			, 'innerHTML', 
					'<sup><b>+</b></sup><br/><span class="icon-title">' + this.nls.switchBasemap + '</span>');
				domAttr.set(initialView		, 'innerHTML', 
					'<i class="fa fa-arrows-alt"></i></sup><br/><span class="icon-title">' + this.nls.initialView + '</span>');
				domAttr.set(previousView	, 'innerHTML', 
					'<i class="fa fa-mail-reply"></i></sup><br/><span class="icon-title">' + this.nls.prevView + '</span>');
				domAttr.set(nextView		, 'innerHTML', 
					'<i class="fa fa-mail-forward"></i></sup><br/><span class="icon-title">' + this.nls.nextView + '</span>');
				
				
				
				window.onresize = function(){
					self._resizeBars();
				};
				
				// resize main toolbar to fit viewport width
				self._resizeBars();
			},
			
			/*
			 * set callbacks for selection on/ off events
			 * window.AppData should be initialized
			 */
			_initializeSelectionEvents: function(){
				
				var mapWidget = Fusion.getWidgetById ( this._mapWidgetId );
				
				// call each ACTIVE selection callbacks
				// @selection - selection object from Fusion
				// @isSelectionCleared - true when nothing selected
				function doActiveCallbacksWithSelection(selection, isSelectionCleared) {
					
					//display the selection to the user in some way ...
					var c = window.AppData.selectionCallbacks;
					for(var i in c){
						if(c[i].active) {
							c[i].fn(selection, isSelectionCleared);
						}
					}
					
				}
				
				function doSelectionOff(selection) {
					doActiveCallbacksWithSelection (selection, true);
				}
				
				function doSelectionOn(selection) {
					doActiveCallbacksWithSelection (selection, false);
				}
								
				//clear the selection results					
				function selectionOffEventHandler() {
					mapWidget.getSelection( doSelectionOff );
					console.log('selection cleared');
				}
				
				//a new selection has been made, request it
				function selectionOnEventHandler() {					
					mapWidget.getSelection( doSelectionOn );
				}
				
				//setup selection events callbacks
				mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_ON, selectionOnEventHandler);
				mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_OFF, selectionOffEventHandler);

			},
			
			/*
			 *  show progress if map is loading
			 *  hide progress if not
			 *  (this function add FusionEvent listener)
			 */ 
			_initializeMapBusyEvents: function(){
				var mapWidget = Fusion.getWidgetById( this._mapWidgetId );
				var self = this;
				
				//
				function mapBusyEventHandler(){
					window.AppData.WGS_BUSY = !window.AppData.WGS_BUSY;
					if(self.WgsProgressIndicator){
						if(window.AppData.WGS_BUSY)
							self.viewContainer.containerNode.appendChild( self.WgsProgressIndicator.domNode );
						self.WgsProgressIndicator[ ( window.AppData.WGS_BUSY ? "start" : "stop" ) ]();
					}
				}
				mapWidget.registerForEvent( window.Fusion.Event.MAP_BUSY_CHANGED, mapBusyEventHandler );				
			},
			
			
			/*
			 * setup openlayers button behaviour, 
			 * @buttonId - OpenLayers button id (just on/off button)
			 * @basemapSwitcherId - Basemap Switcher button id (dropdown to select active basemep)
			 */ 
			_initializeOpenLayersEnabler: function(buttonId, basemapSwitcherId){
				var buttonId = buttonId || 'EnableOpenLayers';
				var basemapSwitcherId = basemapSwitcherId || 'BasemapSwitcher';
				var self = this;
				//console.log(dom.byId(buttonId), dom.byId(basemapSwitcherId))
				if( !dom.byId(buttonId) || !dom.byId(basemapSwitcherId) ) return;
					
				domStyle.set( dom.byId(buttonId).parentNode, 'display', '' );
				/*
				 * if isInitial is true, just set basemap switcher labels,
				 * else: change map to enable/disable OpenLayers basemaps 
				 */
				
				var handler = function(isInitial){
					var isInitial = isInitial || false;
					self._activeMap = Fusion.getWidgetById('MapMenu').getMap().mapGroup.mapId;
					var newMapId = self._activeMap;
							
					if(!isInitial){
						newMapId = (self._activeMap == "noOpenLayers") ? "openLayers" : "noOpenLayers";
						
						if(window.AppData.MapGroupSwitcher[newMapId]){
							Fusion.getWidgetById(basemapSwitcherId).setBasemap( 'None' );
							window.AppData.MapGroupSwitcher[newMapId]();							
						}
					}
						
					setTimeout(function(){
						//self.set('selected',(thisView._activeMap == "noOpenLayers"))
						domAttr.set(dom.byId(buttonId),'innerHTML',
							(newMapId == "openLayers") ? 
								( '<span class="icon-title icon-title-show-always">' + self.nls.disableOL + '</span>' ) : 
								( '<span class="icon-title icon-title-show-always">' + self.nls.enableOL + '</span>' )
						);
						domClass[ ( (newMapId == "openLayers") ? 'remove' : 'add' ) ]( 
							dom.byId(basemapSwitcherId), 'jxDisabled'
						);
					},500);					
				}
				
				handler(true);
				
				on(dom.byId(buttonId), 'click', function(){
					handler();
				});
								
			},
			
			
			/*
			 * 	Show/hide toolbar buttons labels if width is enough/not enough
			 */ 
			_resizeBars: function(){
				if(!this._isFlexibleMainMapBarEnabled) return;
				
				var toolBar = query('.map-button-bar.mobile-bar');
				//console.log(toolBar)
				if(toolBar[0] && dom.byId('toolbar-parent') && dom.byId('wgs4_map')){
					var toolBarWidth = domStyle.get("wgs4_map", 'width');
					var minWidth = this._minToolbarWidth;
				//	console.log(toolBarWidth)
					try {
						domClass[ ( toolBarWidth > minWidth ) ? 'remove' : 'add' ]( 'toolbar-parent', 'small' );
						domClass[ ( toolBarWidth > minWidth ) ? 'add' : 'remove' ]( 'toolbar-parent', 'full' );
					} catch (e){
				// WTF ->	console.log('please clear/update cache')
					}
					if( toolBarWidth > minWidth ) {
						domClass.remove( toolBar[0], 'hidden' );
					}
				}
				
			}			
				
		};
	}
);
