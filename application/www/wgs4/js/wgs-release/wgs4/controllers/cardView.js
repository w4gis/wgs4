require({cache:{
'url:wgs4/views/card.ejs':"<% function renderProperty (propertyName, propertyValue, propertyId){ %>\r\n<li>\r\n    <label>\r\n        <span>\r\n            <%=propertyName %>\r\n        </span>:\r\n        <input value=\"<%=propertyValue %>\" name=\"<%=propertyId%>\" />\r\n    </label>\r\n</li>\r\n\r\n<% } %>\r\n\r\n<% function renderDomainProperty (propertyName, propertyValue, propertyValues, propertyId){ %>\r\n<li>\r\n    <label>\r\n        <span>\r\n            <%=propertyName %>\r\n        </span>:\r\n            <select name=\"<%=propertyId%>\" data-dojo-type=\"dijit/form/Select\">\r\n            <% if (propertyValue == \"\") %>\r\n                <option value=\"selectAnotherVariantValue\" selected=\"selected\" disabled=\"disabled\">Выберите значение..</option>\r\n            <% if(propertyValues) propertyValues.forEach (function(domain, domainId) { %>\r\n                <option value=\"<%= domainId %>\" <% if (domainId == propertyValue)%> selected=\"selected\"><%= domain.value %></option>\r\n            <% }); %>\r\n            </select>\r\n    </label>\r\n</li>\r\n\r\n<% } %>\r\n\r\n\r\n    <div data-dojo-type=\"dijit/layout/TabContainer\" style=\"width: 100px; height: 100px;\">\r\n        <div \r\n            data-dojo-type=\"dijit/layout/ContentPane\" \r\n            title=\"ddfsdf\" \r\n            id=\"card-pane-ddsf\"\r\n            data-dojo-props=\"\"></div>\r\n        <% for (var i=0; i < cards.length; i++) { %>\r\n        <div \r\n            data-dojo-type=\"dijit/layout/ContentPane\" \r\n            title=\"<%= cards[i].typeName %>\" \r\n            id=\"card-pane-<%= cards[i].id || cards[i].objectTypeId %>\"\r\n            data-dojo-props=\"\">\r\n            <form name=\"<%= cards[i].id || cards[i].objectTypeId %>\" oninput=\"console.log(event);console.log(event.target.form.attributes[1].value+event.target.name+event.target.value);\">\r\n                \r\n                <!-- Основные описания -->\r\n               <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Основные характеристики', open: true\">\r\n                    <ul>\r\n                        <%=renderProperty(\"Наименование\", ( cards[i].name || \"Без наименования\" ) + \" - \" + cards[i].typeName,'name') %>\r\n                        <%=renderProperty(\"Краткое наименование\", cards[i].shortName || cards[i].typeName,'shortName') %>\r\n                        <%=renderProperty(\"Атрибутивное описание\", cards[i].typeName,'typeName') %>\r\n                        <% if(cards[i].createDate != undefined)\r\n                            renderProperty(\"Дата изменения\", cards[i].createDate, 'date') %>\r\n                        <% if(cards[i].edit_user != undefined)\r\n                            renderProperty(\"Пользователь\", cards[i].edit_user, 'user') %>\r\n                        <% if(cards[i].note != undefined)\r\n                            renderProperty(\"Примечание\", cards[i].note, 'note') %>\r\n                        \r\n                    </ul>\r\n                </div>\r\n                \r\n                <!-- Группы характеристик -->\r\n                <% cards[i].propertyGroups.forEach (function(group, groupId ) { %>\r\n                    <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: '<%= group.name %>', open: false\">\r\n                        <ul>\r\n                            <% group.properties.forEach(function (property, id ) {\r\n                                if(property.valueTypeId == 5)\r\n                                    renderDomainProperty(property.name, property.value, property.domains, id);\r\n                                else renderProperty(property.name, property.value, id);\r\n                            }) %>\r\n                        </ul> \r\n                    </div>\r\n                <% }) %>\r\n                \r\n                <!-- Связи -->\r\n                <% if(cards[i].relations) cards[i].relations.forEach (function(relation, relationId) { %>\r\n                    <div data-dojo-type=\"dijit/TitlePane\" data-dojo-props=\"title: 'Связь: <%=relation.name %>', open:  false\">\r\n                        <ul>\r\n                        <% relation.linkedCards.forEach (function(childCard, childCardIndex) { %>\r\n                            <li>\r\n                                <button data-card-id = \"<%=childCard.id%>\" onclick=\"console.log(event.target.dataset.cardId);\r\n                                require(['wgs4/controllers/card'],function(Card){\r\n                                    new Card([event.target.dataset.cardId]).show();\r\n                                });\r\n                                \">\r\n                                    <%=childCard.name||childCard.typeName %>\r\n                                </button>\r\n                            </li>\r\n                            <% }); %>\r\n                        </ul>\r\n                    </div>\r\n                <% }); %>\r\n            </form>\r\n        </div>\r\n        <% } %>\r\n    </div>\r\n"}});
define("wgs4/controllers/cardView", ['dojo/text!wgs4/views/card.ejs', 'dojox/layout/FloatingPane', 'dijit/TitlePane', "dojo/parser", "dijit/layout/TabContainer", "dijit/layout/ContentPane"],
function(template, FloatingPane, TitlePane, parser) {
    "use strict";

    function CardView(CardDescriptions) {
        this.cardDescriptions = CardDescriptions;
    }

    CardView.prototype.show = function() {
        var htmlCard = window.ejs.render(template, {
            cards: this.cardDescriptions
        });
        var cardDialogContainerElem = document.createElement("div");
        document.body.appendChild(cardDialogContainerElem);
        var cardDialog = new FloatingPane({
            title: 'Карточка объекта',
            resizable: true,
            dockable: true,
            content: htmlCard,
            'class': 'card-dialog',
            style: 'left: 123px'
        }, cardDialogContainerElem);
        cardDialog.startup();

        //parser.parse(cardDialog.containerNode);

    };

    CardView.prototype.childCardButtonClick = function(){
        alert("I don't know");
    }


    return CardView;
});