var profile = {



    "action": "release",

    "releaseDir": "wgs-release",

    "stripConsole": "normal",

    "copyTests": false,

    "cssOptimize": "comments.keepLines",

    "mini": true,

    "webkitMobile": true,

    'selectorEngine': "acme",



    localeList: "en-us,ru-ru",

    staticHasFeatures: {

        "dojo-v1x-i18n-Api": 1,

    },


    layers: {

        "dojo/dojo": {

            includeLocales: ['en-us', 'ru-ru'],

            customBase: true,

            boot: true,

            include: [

                "dojox/app/controllers/Load",
                "dojox/app/controllers/Transition",
                "dojox/app/controllers/Layout",
                "dojo/_base/fx",
                "dojo/_base/window",
                "dojo/json",
                "dojox/mobile",


                "dojox/mobile/Heading",
                "dojox/mobile/ScrollableView", "dojox/mobile/ScrollablePane",
                "dojox/mobile/ToolBarButton",
                "dijit/_WidgetBase", "dijit/_WidgetsInTemplateMixin", "dijit/registry",
                "dijit/_TemplatedMixin", "dojox/mobile/Opener", "dijit/form/Select",
                "dojox/mobile/FormLayout", "dojox/mobile/TabBar", "dojox/mobile/TabBarButton",
                "dojox/mobile/GridLayout", "dojox/mobile/TextBox", "dojox/mobile/SpinWheelDatePicker",
                "dojox/mobile/TextArea", "dojox/mobile/Button", "dojox/mobile/ContentPane", "dojox/mobile/RoundRectStoreList",
                "dojox/app/widgets/Container", "dojox/mobile/EdgeToEdgeCategory", "dojox/mobile/RoundRectCategory",
                "dojox/mobile/EdgeToEdgeList", "dojox/mobile/EdgeToEdgeStoreList", "dojox/mobile/RoundRect", "dojox/mobile/ExpandingTextArea",
                "dojox/app/widgets/Container", "dojox/mobile/TextBox", "dojox/mobile/TextBox",
                "dojox/mobile/Tooltip", "dojox/mobile/Switch"

            ]

        },

        "wgs4/gis-app": {

            includeLocales: ['en-us', 'ru-ru'],
            include: [

                "wgs4/Application",
                "wgs4/model/wgs-fusion",
                
                "wgs4/controllers/bottom",
                "wgs4/controllers/top",

                "wgs4/controllers/cs",

                "wgs4/nls/cs",
                "wgs4/controllers/editor",
                "wgs4/nls/editor",

                "wgs4/controllers/fdetails",
                "wgs4/controllers/finfo",
                "wgs4/nls/finfo",



                "wgs4/controllers/language",
                "wgs4/nls/language",



                "wgs4/controllers/legend",
                "wgs4/nls/legend",


                "wgs4/controllers/map",
                "wgs4/nls/map",


                "wgs4/controllers/redline",
                "wgs4/controllers/settings",
                "wgs4/nls/settings",

                "wgs4/nls/taskpane",

                "wgs4/controllers/taskpane",

                "wgs4/nls/card",
                "wgs4/controllers/card",

                "wgs4/nls/thematic",

                "wgs4/controllers/thematic"]



        }

    },



    packages: [

    {
        name: "dojo",
        location: "./dojo"
    },

    {
        name: "dijit",
        location: "./dijit"
    },

    {
        name: "dojox",
        location: "./dojox"
    },

    {
        name: "wgs4",
        location: "./wgs"
    }

    ]

};