 define(["dijit/Dialog", "dojo/text!wgs45/views/theme/themeSetup.ejs"], function(Dialog, themeSetupEjs){
  function ThemeSetup() {
    this.themeSetupDialog = null;
     
  }
   
  ThemeSetup.prototype.showThemeSetupDialog = function(themeLayersParams, callback) {
    // TODO: Ajax dialog
    if (this.themeSetupDialog) {
      this.themeSetupDialog.destroyRecursive();
    }
    this.themeSetupDialog = new Dialog({
      title: "Настройка отчета",
      content: window.ejs.render(themeSetupEjs, {themeLayersParams: themeLayersParams}),
      'class': "theme-setup-window"
    });
    this.themeSetupDialog.onHide = function() {
      this.destroyRecursive();
    }
    this.themeSetupDialog.show();
    callback(themeLayersParams);
  };


  
  ThemeSetup.prototype.hideThemeSetupDialog = function() {
    this.themeSetupDialog.hide();
  };
  
  return ThemeSetup;
 })
 
 
 