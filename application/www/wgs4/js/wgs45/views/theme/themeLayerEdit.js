 define(["dijit/Dialog", 
      "dojo/text!wgs45/views/theme/themeLayerEdit.ejs", 
      "dijit/ColorPalette",
      "dijit/form/ComboBox",

      "dijit/form/DropDownButton",
      "dijit/TooltipDialog",
      "dijit/layout/ContentPane"], 
function(Dialog, 
        themeLayerEditEjs, 
        ColorPalette,
        ComboBox ){


  function ThemeLayerEdit() {
    this.themeLayerEditDialog = null;
     
  }
   
  ThemeLayerEdit.prototype.showThemeLayerEditDialog = function(themeLayersParams, callback) {
    // TODO: Ajax dialog
    if (this.themeLayerEditDialog) {
      this.themeLayerEditDialog.destroyRecursive();
    }
    this.themeLayerEditDialog = new Dialog({
      title: "Тематический слой",
      content: window.ejs.render(themeLayerEditEjs, {themeLayersParams: themeLayersParams}),
      'class': "theme-layer-edit-window"
    });
    this.themeLayerEditDialog.onHide = function() {
      this.destroyRecursive();
    }
    new ComboBox({
        name: "select0",
        style: "width: 300px"
    }, 'featurelayer-select').startup();
    new ComboBox({
        name: "select1",
        style: "width: 150px"
    }, 'objecttype-select').startup();
    new ComboBox({
        name: "select2",
        style: "width: 150px"
    }, 'geometry-select').startup();

    this.themeLayerEditDialog.show();
    this.addColorPallete();
    callback(themeLayersParams);
    
  };

  ThemeLayerEdit.prototype.addColorPallete = function() {
    var stylePreview = document.getElementById('style-preview');
    var foregroundColor = new ColorPalette({
      palette: "7x10",
      onChange: function(val){ 
        console.log(val);
        stylePreview.setAttribute('data-foreground-color', val);
        stylePreview.style.backgroundColor = val;
        //stylePreview.innerHTML = val; 
      }
    }, "foreground-color").startup();
  };

  
  ThemeLayerEdit.prototype.hideThemeLayerEditDialog = function() {
    this.themeLayerEditDialog.hide();
  };

  
  return ThemeLayerEdit;
 })
 
 
 