 define(["dijit/Dialog", "dojo/text!wgs45/views/theme/theme.ejs", "dijit/layout/TabContainer", "dijit/layout/ContentPane"], function(Dialog, themeEjs){
  function ThemeList() {
    this.themeListDialog = null;

  }

  ThemeList.prototype.showThemeListDialog = function(themeList, callback) {
   // console.log('themeListDialog before destroy ', this.themeListDialog);
    if (this.themeListDialog) {
      this.themeListDialog.destroyRecursive();
      //console.log('destroyed');
    }
   // console.log('themeListDialog after destroy ', this.themeListDialog);
    this.themeListDialog = new Dialog({
      title: "Тематические отчеты",
      content: window.ejs.render(themeEjs, {themeList: themeList}),
      'class': "theme-list-window"
    });
  // console.log('themeListDialog after create new ', this.themeListDialog);

    this.themeListDialog.onHide = function() {
      this.destroyRecursive();
    }

    this.themeListDialog.show();
    callback(themeList);
  };



  ThemeList.prototype.hideThemeListDialog = function() {
    this.themeListDialog.hide();
  };

  return ThemeList;
 })
