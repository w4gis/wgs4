// we use 'define' and not 'require' to workaround Dojo build system
// limitation that prevents from making of this file a layer if it
// using 'require'
define(['wgs45/controllers/wgs-app'], function(WgsApp) {

    if (!window.WGS) {
        window.WGS = {
            FLAGS: {},
            MODULES: {},
            PRIVILEGE: {}
        };
    }
    /*console.log("window.WGS", window.WGS);*/
    window.WGS_APP = new WgsApp(window.WGS);
});
