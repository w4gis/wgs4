define(['wgs45/model/cardModel',
	'wgs45/controllers/cardView',
	'dojo/request/xhr',
	'dojo/Deferred',
	'dijit/registry',
	'dojo/json',
	'wgs45/controllers/helper',
	'wgs45/controllers/cardActionDialog'
], function (CardModel, CardView, xhr, Deferred, registry, json, Helper, CardActionDialog) {

	"use strict";

	function Card(_featureIds, windowsDock, isDirectIds, parentCard, geomParams, layerId) {
		this.featureIds = _featureIds;
		this._dock = windowsDock;
		this._noFeatureIds = isDirectIds || false;
		this._parentCard = parentCard;
		this.model = new CardModel(this._noFeatureIds);
		this.geomParams = geomParams;
		this._layer = layerId;
		this._isEmpty = false;

		console.log(this._layer);
	}

	Card.prototype.maximize = function () {
		this.view.cardDialog.show();
	}
	Card.prototype.show = function () {
		var self = this;
		this.get().then(function (cardDescriptions) {
			Helper.hide();
			console.log('card ', cardDescriptions);
			if (cardDescriptions.length === 0) {
				cardDescriptions.push({
					typeName: 'Нет описаний',
					propertyGroups: []
				});
				self._isEmpty = true;
			}
			if (self.geomParams && cardDescriptions[0]) {
				cardDescriptions[0].propertyGroups.push(self.geomParams);
			}
			self.view = new CardView(cardDescriptions, self._dock, self._isEmpty, self.featureIds.length > 1);
			self.view.ids = JSON.stringify(self.featureIds);
			self.view.show();
			self.registerEventHandlers();
			//this.registerEventHandlers(cardDescriptions);
		}, function (error) {
			Helper.warning(error.toString());
		});
	};

	Card.prototype.get = function () {
		Helper.info('Загрузка...');
		return this.model.get(this.featureIds);
	};

	Card.prototype.save = function (form) {
		if (WGS.PRIVILEGE.CARD_EDIT) {
			Helper.info('Сохранение изменений...');
			var modifiedCardDescription = this._getCardDesctiption(form);
			return this.model.save(modifiedCardDescription);
		} else {
			var d = new Deferred;
			setTimeout(function () {
				d.resolve();
			}, 100);
			return d;
		};

	};

	Card.prototype.ok = function (form) {
		if (WGS.PRIVILEGE.CARD_EDIT) {
			Helper.info('Сохранение изменений...');
			var modifiedCardDescription = this._getCardDesctiption(form);
			return this.model.save(modifiedCardDescription);
		} else {
			var d = new Deferred;
			setTimeout(function () {
				d.resolve();
			}, 100);
			return d
		};

	};

	Card.prototype.removeCard = function (objectId, widget) {
		return this._getRemoveConfirmDialog(objectId).show(widget);
	};

	Card.prototype.printCard = function (form) {
		var modifiedCardDescription = this._getCardDesctiption(form);
		return this.model.printCardOne(modifiedCardDescription.id);
	};

	Card.prototype._openNestedCard = function (id) {
		var self = this;

		require(['wgs45/controllers/card'], function (Card) {
			new Card([id], self._dock, true, self).show();
		});
	};

	Card.prototype._getRemoveConfirmDialog = function (objectId) {
		if (!this.removeConfirmDialog) {
			this.removeConfirmDialog = new CardActionDialog(objectId, this.model);
		}
		return this.removeConfirmDialog;
	};

	Card.prototype.registerEventHandlers = function (cardDescriptions) {
		// cardDescriptions.forEach(function(card, cardId) {
		//     card.relations.forEach(function(relation, relationId) {
		//         relation.linkedCards.forEach(function(childCard, childCardId) {

		//         });
		//     });
		// });

		var self = this;
		var saveButtons = this.view.cardDialog.domNode.getElementsByClassName('card__buttons__save');
		var okButtons = this.view.cardDialog.domNode.getElementsByClassName('card__buttons__ok');
		var printButtons = this.view.cardDialog.domNode.getElementsByClassName('card__buttons__print');
		var removeButtons = this.view.cardDialog.domNode.getElementsByClassName('card__buttons__remove');
		[].forEach.call(saveButtons, function (saveButton) {
			registry.byId(saveButton.getAttribute('widgetid')).onClick = function () {
				console.log('saving...'); //debug
				var saveEventHandler = self.saveEventHandler.bind(self);

				var tabContainerNode = button.closest('.dijitBorderContainer').firstElementChild.firstElementChild;
				var tabContainerId = tabContainerNode.getAttribute('widgetid');
				var tabContainer = registry.byId(tabContainerId);
				tabContainer.getChildren().forEach(function (child) {
					var form = child.domNode.getElementsByTagName('form')[0];
					self.save(form).then(saveEventHandler, self.saveEventHandler.bind(self));
				});

				return false;
			};
		});

		[].forEach.call(okButtons, function (button) {
			registry.byId(button.getAttribute('widgetid')).onClick = function () {
				console.log('save & close...'); //debug

				if (!self._isEmpty) {
					var saveEventHandler = function () {
						self.saveEventHandler();
					};

					var tabContainerNode = button.closest('.dijitBorderContainer').firstElementChild.firstElementChild;
					var tabContainerId = tabContainerNode.getAttribute('widgetid');
					var tabContainer = registry.byId(tabContainerId);
					tabContainer.getChildren().forEach(function (child) {
						var form = child.domNode.getElementsByTagName('form')[0];
						self.save(form).then(saveEventHandler, self.saveEventHandler.bind(self));
					});
					self.view.close();
				} else {
					self.view.close();
				}

				return false;
			};
		});

		[].forEach.call(printButtons, function (printButton) {
			registry.byId(printButton.getAttribute('widgetid')).onClick = function () {
				console.log('print...'); //debug
				var printEventHandler = function () {
					self.printEventHandler();
				};

				var tabContainerId = printButton.closest('.dijitBorderContainer').firstElementChild.firstElementChild.getAttribute('widgetid');
				var form = registry.byId(tabContainerId).selectedChildWidget.domNode.getElementsByTagName('form')[0];
				self.printCard(form);
				//self.printCard(form).then(printEventHandler, self.printEventHandler.bind(self));
				return false;
			};
		});

		[].forEach.call(removeButtons, function (removeButton) {
			registry.byId(removeButton.getAttribute('widgetid')).onClick = function () {
				console.log('remove...');

				var removeEventHandler = function (message) {
					console.log('succ ', message);
					self.updateParent();
					self.view.close();
					if (!self._noFeatureIds) {
						require(['wgs45/controllers/card'], function (Card) {
							new Card(self.featureIds, self._dock, self._noFeatureIds, self._parentCard, self.geomParams, self._layer).show();
						});
					}
				};

				var removeErrorEventHandler = function (err) {
					console.log('err ', err);
				};

				var tabContainerId = removeButton.closest('.dijitBorderContainer').firstElementChild.firstElementChild.getAttribute('widgetid');
				var form = registry.byId(tabContainerId).selectedChildWidget.domNode.getElementsByTagName('form')[0];
				self.removeCard(form.dataset.cardId, removeButton).then(removeEventHandler, removeErrorEventHandler);
				return false;
			};
		});

		this._registerEventHandlersForNestedCards();

		this.view.cardDialog.domNode.addEventListener('click', function (e) {
			console.log('clicked!');
			var actionButtonElem = e.target.classList.contains('card-action') ?
				e.target :
				e.target.closest('.card-action');

			if (!actionButtonElem) {
				return;
			}

			var actionButtonDataElem = actionButtonElem.dataset.action ? actionButtonElem : actionButtonElem.getElementsByClassName('card-action__data-elem')[0];

			console.log(actionButtonElem, actionButtonDataElem);

			if (!actionButtonDataElem) {
				return;
			}

			var relationId = actionButtonDataElem.dataset.relationId;

			switch (actionButtonDataElem.dataset.action) {
			case 'card-unbind':
				{
					var objRelationId = actionButtonDataElem.dataset.objRelationId;
					self.model.removeObjectRelation(objRelationId).then(function (resp) {
						self._updateRelation(relationId);
					});
					break;
				}

			case 'card-bind':
				{
					var parentId = actionButtonDataElem.dataset.cardId;
					var filteringSelect = actionButtonDataElem.closest('.card__relation').getElementsByClassName('dijitComboBox')[0];
					console.log(filteringSelect);
					var selectWidget = registry.byId(filteringSelect.getAttribute('widgetid'));
					var childId = selectWidget.get('value');
					console.log(relationId, parentId, childId);
					if (relationId && parentId && childId) {
						self.model.bindChildObject(parentId, childId, relationId).then(function (resp) {
							self._updateRelation(relationId);
						});
						selectWidget.set('value', null);
					}
					break;
				}
			case 'card-create-child':
				{
					var parentId = actionButtonDataElem.dataset.cardId;
					var selectNode = actionButtonDataElem.closest('.card__relation').getElementsByClassName('card__relation__actions__create-select')[0];
					var objTypeRelationId = registry.byId(selectNode.get('widgetid')).value;
					console.log(parentId, objTypeRelationId, relationId);

					if (parentId && relationId && objTypeRelationId) {
						self.model.createChildObject(parentId, objTypeRelationId, relationId).then(function (resp) {
							self._updateRelation(relationId);
						});
					}
				}
			case 'card-create-new':
				{
					var actDialog = new CardActionDialog(self.featureIds[0], null, false, 0, self._layer);
					actDialog.initPromise.then(function (cardtypes) {
						actDialog.show(actionButtonDataElem).then(function (success) {
							Helper.success('Карточка создана');
							self.view.close();
							require(['wgs45/controllers/card'], function (Card) {
								new Card(self.featureIds, self._dock, self._noFeatureIds, self._parentCard, self.geomParams, self._layer).show();
							});
						}, function (error) {
							console.log(error);
						});
					});
				}
			}

		});
	};

	Card.prototype._updateRelation = function (relationId) {
		var self = this;
		self.get().then(function (cardDescriptions) {
			Helper.hide();
			self.view.updateNestedCards(cardDescriptions, relationId);
			self._registerEventHandlersForNestedCards();
			self.updateParent();
		});
	};

	Card.prototype.updateByChildCard = function () {
		console.log('update');
		var self = this;
		self.get().then(function (cardDescriptions) {
			console.log('card ', cardDescriptions);
			cardDescriptions.forEach(function (card) {
				for (var relationId in card.relations) {
					console.log('relid ', relationId);
					if (!card.relations.hasOwnProperty(relationId)) {
						continue;
					}
					self.view.updateNestedCards(cardDescriptions, relationId);
				}
				self._registerEventHandlersForNestedCards();
				//Helper.success('Обновлено');
			});
		}, function (err) {
			Helper.error(err);
		});
		self.updateParent();
	};

	Card.prototype.updateParent = function () {
		console.log('updateParent');
		if (this._parentCard && this._parentCard.updateByChildCard) {
			this._parentCard.updateByChildCard();
		}
	};

	Card.prototype._registerEventHandlersForNestedCards = function () {
		var self = this;
		var nestedCardLinks = this.view.cardDialog.domNode.getElementsByClassName('card__nested-card');
		[].forEach.call(nestedCardLinks, function (link) {
			link.addEventListener('click', function (e) {
				e.preventDefault();
				self._openNestedCard(link.dataset.cardId);
			});
		});
	};

	Card.prototype._getCardDesctiption = function (form) {
		var resultArray = {};
		var noPropertyStringInputs = ['name', 'shortName', 'note', 'id'];
		var forbiddenProperties = ['user', 'date', 'typeName'];

		var formInputs = form.getElementsByTagName('input');
		var domainWidgetClassName = 'dijitSelect';
		var domainWidgetElems = form.getElementsByClassName(domainWidgetClassName);

		[].forEach.call(domainWidgetElems, function (selectElem) {
			//TODO: ???
			if (selectElem.closest('label') && selectElem.closest('label').dataset.propertyId) {
				var propId = selectElem.closest('label').dataset.propertyId;
				resultArray['property-' + propId] = registry.byId(selectElem.getAttribute('widgetid')).get('value');
			}
		});

		for (var i in formInputs) {
			//console.log('frminput ', formInputs[i].parentNode.parentNode, formInputs[i].name);
			var inputName = formInputs[i].name;
			if (inputName && ~inputName.indexOf('property')) {

				var isDate = false;
				if (formInputs[i].parentNode &&
					formInputs[i].parentNode.parentNode &&
					formInputs[i].parentNode.parentNode.classList.contains("dijitDateTextBox")) {
					console.log('Дата есть ', formInputs[i]);
					isDate = true;
				}

				var isStripedProperty = false;
				var isForbidden = false;

				noPropertyStringInputs.forEach(function (noPropInputName) {
					if (~inputName.indexOf(noPropInputName)) {
						isStripedProperty = true;
					}
				});

				forbiddenProperties.forEach(function (noPropInputName) {
					if (~inputName.indexOf(noPropInputName)) {
						isForbidden = true;
					}
				});

				if (isForbidden) {
					continue;
				}

				if (isStripedProperty) {
					resultArray[inputName.slice(9, inputName.length)] = formInputs[i].value;
				} else {
					var val = formInputs[i].value;
					if (isDate && val == "") {
						val = "<...>";
					}
					resultArray[inputName] = val;
				}
			}
		}
		resultArray.id = form.dataset.cardId;
		if (this.featureIds.length > 1) {
			resultArray.featureIds = json.stringify(this.featureIds);
		}
		return resultArray;
	};

	Card.prototype.saveEventHandler = function (response) {
		Helper.success('Изменения успешно сохранены');
		this.updateParent();
		// console.log(response);
	};

	Card.prototype.hide = function () {
		this.view.cardDialog.hide();
	};

	return Card;
});
