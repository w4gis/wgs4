define(['wgs45/model/tusurFlatStats', 'wgs45/controllers/card'], function (Model, Card) {

	function TusurFlatStatsController() {
		var self = this;
		this.model = Model;
		this.mapName = null;

		this.CARD = {
			ROOM_CARD_TYPE: 4,
			ROOM_STUDENT_RELATION_TYPE: 1
		};

		window.WGS_APP.appState.selectionCallbacks.tusurFlatStats = {
			active: true,
			fn: function (selection, isSelectionCleared) {
				self._getInfoAboutRoom(selection, isSelectionCleared);
			}
		};

	}

	TusurFlatStatsController.prototype.refreshData = function () {
		var self = this;
		console.log('=======================================================');
		var featureLayerId;

		this.mapName = window.WGS_APP.getCurrentMapProps().map.getMapTitle();
		this.container = document.getElementById('stats-Map-' + self.mapName + '__container');

		try {
			featureLayerId = window.WGS_APP.getCurrentLayerId(self.mapName);
			var allMapLayers = window.WGS_APP.getAllLayerIds(self.mapName);
		} catch (E) {

			console.log(E);
			return;
		}

		var mapStats = {
			currentPlaces: 0,
			freePlaces: 0,
			maxByDocsPlaces: 0,
			maxPlaces: 0,
			gender: {
				boys: 0,
				girls: 0
			}
		}

		allMapLayers.forEach(function (layerId) {
			this.model.getStats(layerId, function (stats, error) {
				if (stats) {
					mapStats.currentPlaces += +stats.currentPlaces;
					mapStats.freePlaces += +stats.freePlaces;
					mapStats.maxByDocsPlaces += +stats.maxByDocsPlaces;
					mapStats.maxPlaces += +stats.maxPlaces;


					if (stats.sexOfStudents) {
						stats.sexOfStudents.forEach(function (currentData) {
							if (currentData.label == "М") {
								mapStats.gender.boys += +currentData.value;
							}

							if (currentData.label == "Ж") {
								mapStats.gender.girls += +currentData.value;
							}
						});
					}

				}
			});
		}, this);


		//console.log('hm', featureLayerId);
		this.model.getStats(featureLayerId, function (stats, error) {
			if (stats) {
				self._fillInfoBlock(stats, mapStats);
				if (stats.currentPlaces) {
					if (self.container) {
						self.container.style.display = 'inline-block';
					}
				} else {
					if (self.container) {
						self.container.style.display = 'none';
					}
				}
			}
		});

	}


	TusurFlatStatsController.prototype._fillInfoBlock = function (stats, mapStats) {
		var self = this;

		for (var key in stats) {
			var currentFillDOM = document.getElementById(key + '-Map-' + self.mapName);
			if (currentFillDOM) {
				//console.log(key, currentFillDOM, stats, stats[key]);
				if (key == 'sexOfStudents') {
					self._fillGenderInfoBlock(currentFillDOM, stats[key], mapStats.gender);
				} else {
					currentFillDOM.innerHTML = "<b>" + stats[key] + "</b> (" + mapStats[key] + ")";
				}
			}
		}

	}


	TusurFlatStatsController.prototype._fillGenderInfoBlock = function (dom, data, mapStats) {
		if (!data) {
			dom.innerHTML = '';
			return;
		}

		var gender = {
			boys: 0,
			girls: 0
		};

		data.forEach(function (currentData) {
			if (currentData.label == "М") {
				gender.boys = currentData.value;
			}

			if (currentData.label == "Ж") {
				gender.girls = currentData.value;
			}
		});

		var string = "М - <b>" + gender.boys + "</b> (" + mapStats.boys + ")" + ", Ж - <b>" + gender.girls + "</b> (" + mapStats.girls + ")";
		dom.innerHTML = string;

	}

	TusurFlatStatsController.prototype._getInfoAboutRoom = function (selection, isSelectionCleared) {
		var self = this;
		window.WGS_APP.getSelection().then(function (result) {
			var roomInfoContainer = document.getElementById('room-stats-Map-' + self.mapName + '__container');
			roomInfoContainer.innerHTML = '';
			var currentCard = new Card(result[0].featureIds);
			currentCard.get().then(function (data) {
				data.forEach(function (card) {
					// console.log('tusurFlatStats card', data);
					if (card.cardCount && card.cardCount > 1 || card.objectTypeId && card.objectTypeId != self.CARD.ROOM_CARD_TYPE) {
						// display none
						return;
					}

					for (var key in card.relations) {
						if (card.relations[key].linkedCards && card.relations[key].relationId == self.CARD.ROOM_STUDENT_RELATION_TYPE) {
							var studentRoomHeaderElem = document.createElement('H3');
							studentRoomHeaderElem.innerHTML = 'Жильцы к. ' + card.name;
							var studentListElem = document.createElement('UL');
							card.relations[key].linkedCards.forEach(function (student) {
								var studentListItemElem = document.createElement('LI');
								studentListItemElem.className = "student-list__item";
								studentListItemElem.innerHTML = '<a href="#" data-action="card-open-direct" data-card-id="' + student.id + '">' + student.name + '</a>';
								studentListElem.appendChild(studentListItemElem);
							});
							console.log('studentList UL', studentListElem);

							if (!roomInfoContainer) {
								return;
							}
							roomInfoContainer.appendChild(studentRoomHeaderElem);
							roomInfoContainer.appendChild(studentListElem);
						}
					}
				});





			});
			console.log('tusurFlatStats result', result);
		}, function (error) {
			console.log('tusurFlatStats error', error);
			// body...
		});
		//console.log('tusurFlatStats',selection, isSelectionCleared);
	}

	return TusurFlatStatsController;
})
