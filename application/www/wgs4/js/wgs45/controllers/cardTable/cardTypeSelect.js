define(['dojo/text!wgs45/views/cardTypeSelect.ejs', 'dojo/Deferred', 'dojo/request/xhr', 'dojo/json'], function(template, Deferred, xhr, json) {
    "use strict";
    
    function CardTypeSelect() {
    }

    

    CardTypeSelect.prototype.renderHtml = function(cardTypes, id){
        return window.ejs.render(template, {cardTypes : cardTypes, selectId: id});
    };

    return CardTypeSelect;
});