define(['dojox/layout/FloatingPane',
        'dojo/text!wgs45/views/settingsView.ejs',
        'dijit/registry'],
function(FloatingPane, template, registry) {
	"use strict";

	function SettingsView(windowsDock) {
        this._dock = windowsDock;

	}

		SettingsView.prototype.show = function() {
			var settingsDialogContainerElem = document.createElement("div");
    	    var self = this;

    	    var settingsKeys = [{
    	    	name: 'Главные настройки',
    	    	params: ['ряз', 'дьва', 'восемьнадцать']
    	    },{
    	    	name: 'Неглавные настройки',
    	    	params: ['ряз', 'восемьнадцать']
    	    }];
	
    	    var dialogHtml = window.ejs.render(template, {tabs: settingsKeys});
	
    	    document.body.appendChild(settingsDialogContainerElem);
    	    this.settingsDialog = new FloatingPane({
    	        title: 'Настройки интерфейса',
    	        resizable: false,
    	        dockable: true,
    	        dockTo: this._dock,
    	        content: dialogHtml,
    	        'class': 'card-dialog cart-table',
    	        onShow: function() {
    	            console.log('SHOW settings');
    	            registry.byId('appLayout').resize();
    	        }
    	    }, settingsDialogContainerElem);
	
    	    this.settingsDialog.domNode.getElementsByClassName('dojoxFloatingMinimizeIcon')[0].addEventListener('click', function() {
    	        setTimeout(function() {
    	            console.log('HIDE');
    	            registry.byId('appLayout').resize();
    	        }, 1000);
    	    });
	
    	    var resizeTimeout;
    	    window.addEventListener('resize', function() {
    	        // prevent calling event handler too often
    	        if (!resizeTimeout) {
    	            resizeTimeout = setTimeout(function() {
    	                self.resize();
    	                resizeTimeout = null;
    	            }, 500);
    	        }
    	    });
	
    	    this.settingsDialog.startup();
		};

	//resizes settings dialog
        SettingsView.prototype.resize = function() {
            var windowOffset = 0;

            var bodyWidth = document.body.clientWidth - windowOffset;
            var bodyHeight = document.body.clientHeight - windowOffset;
            var resizeParams = {
                w: bodyWidth,
                h: bodyHeight
            };

            this.settingsDialog.resize(resizeParams);

        };

    return SettingsView;
});