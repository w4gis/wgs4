define([
    'dijit/registry',
    'dojo/text!wgs45/views/card.ejs',
    'dojo/text!wgs45/views/card-nested-cards.ejs',
    'dojox/layout/FloatingPane',
    'dijit/TitlePane',
    "dojo/parser",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane"],

function(registry, template, relationTemplate, FloatingPane, TitlePane, parser) {
    "use strict";

    function CardView(CardDescriptions, windowsDock, isEmpty, isMultiple) {
        this.cardDescriptions = CardDescriptions;
        this._dock = windowsDock;
        this._isEmpty = isEmpty;
        this._isMultiple = isMultiple;
    }

    CardView.prototype.show = function() {

        var self = this;
        var htmlCard = window.ejs.render(template, {
            cards: this.cardDescriptions,
            isEmpty: self._isEmpty,
            isMultiple: self._isMultiple
        });

        var cardDialogContainerElem = document.createElement("div");
        document.body.appendChild(cardDialogContainerElem);
        var bodyWidth = document.body.clientWidth;

        this.cardDialog = new FloatingPane({
            'title': this.getTitle(),
            'class': 'card-dialog',

            'resizable': true,
            'dockable': true,

            'content': htmlCard,
            'dockTo': this._dock,

            style: 'left: ' + (bodyWidth / 2 - 350) + 'px',

            onShow: function() {
                console.log('SHOW');
                registry.byId('appLayout').resize();
            }
        }, cardDialogContainerElem);

        this.cardDialog.startup();
        this.cardDialog.domNode.getElementsByClassName('dojoxFloatingCloseIcon')[0].addEventListener('click', function() {
            self._onClose();
        });
        
        this.cardDialog.bringToTop();
        this._registerEventHandlers();
        
        this.resize();

        //registry.byId('card-tab').selectChild(registry.byId('card-tab').getChildren()[0]);
        // parser.parse(cardDialog.containerNode);

    };
    
    CardView.prototype._onClose = function() {
        if(window.WGS_APP && window.WGS_APP.cards[this.ids]) {
            window.WGS_APP.cards[this.ids] = null;
        }
    }

    CardView.prototype.updateNestedCards = function(cardDescriptions, relationTypeId) {
        console.log('carddesc', cardDescriptions, relationTypeId);
        var relationUlElems = this.cardDialog.domNode.getElementsByClassName('card__relation__list');
        console.log('relul ', relationUlElems);
        var targetElems = [];
        [].forEach.call(relationUlElems, function(ul) {
            console.log('reltypeids ', ul.dataset.relationTypeId, ' ', relationTypeId);
            if (ul.dataset.relationTypeId == relationTypeId) {
                targetElems.push(ul);
            }
        });

        if (targetElems) {

            cardDescriptions.forEach(function(card, index) {
                
                for (var relationId in card.relations) {
                    if(!card.relations.hasOwnProperty(relationId))
                        continue;
                    var relation = card.relations[relationId];
                    if (relationId == relationTypeId) {
                        var htmlRelations = window.ejs.render(relationTemplate, {
                            relation: relation
                        });
                        [].forEach.call(targetElems, function(targetElem) {
                            //replace old relation cards with new ones

                            //while there are <tr> for child cards, remove them
                            var nestedRowElems = targetElem.getElementsByClassName('card__relation__nested-record');
                            while (nestedRowElems.length > 0) {
                                var nestedRowElem = nestedRowElems[0];
                                nestedRowElem.parentElement.removeChild(nestedRowElem);

                                nestedRowElems = targetElem.getElementsByClassName('card__relation__nested-record');
                            }

                            //add trs for current child cards
                            var nestedRowTableElem = targetElem.getElementsByTagName('tr')[0].parentElement;
                            nestedRowTableElem.innerHTML += htmlRelations;

                            //parser unbind dojo buttons
                            parser.parse(nestedRowTableElem);


                            //update relation count
                            var relationsCount = 0;
                            relation.linkedCards.forEach(function() {
                                relationsCount++;
                            });
                            registry.byId(targetElem.closest('.dijitTitlePane').getAttribute('widgetid')).set('title', relation.name + ": " + relationsCount);
                        });
                    }
                }
            });

        }

    };

    CardView.prototype.createRelationSelectWidget = function(elem) {
        require([
            "dojo/store/Memory", "dijit/form/FilteringSelect", 'dojo/store/JsonRest'], function(Memory, FilteringSelect, JsonRestStore) {

            var relationButtons = elem.closest('.dijitTitlePane').getElementsByClassName('card-action__data-elem'); //[0].dataset.relationId;
            var relationId;

            [].forEach.call(relationButtons, function(buttonElem) {
                if (buttonElem.dataset.action == "card-bind") {
                    relationId = buttonElem.dataset.relationId;
                }
            });

            console.log(relationId);
            var stateStore = new JsonRestStore({
                target: '/objectadmin/card/getchildcardsbyrelationidandname/?relationId=' + relationId
            });

            var filteringSelect = new FilteringSelect({
                name: "name",
                placeHolder: "Карточка...",
                store: stateStore,
                searchAttr: "name",
                searchDelay: 500
            }, elem).startup();
        });
    };
    CardView.prototype.getTitle = function() {
        var title;
        if (this.cardDescriptions[0] && this.cardDescriptions[0].name) {
            title = this.cardDescriptions[0].name;
        } else {
            title = 'Без наименования';
        }

        title += " :: Карточка объекта";
        return title;
    };

    CardView.prototype.close = function() {
        this.cardDialog.close();
        this._onClose();
    };

    CardView.prototype.resize = function() {
        if(!this.cardDialog) {
            return;
        }
        var self = this;
        var windowOffset = 20;
        
        var thisWidth = self.cardDialog.domNode.clientWidth;
        var thisHeight = self.cardDialog.domNode.clientHeight;
        var bodyWidth = document.body.clientWidth - windowOffset;
        var bodyHeight = document.body.clientHeight - windowOffset;
        var resizeFlag = false;
        var resizeParams = {};

        if (bodyWidth < thisWidth) {
            thisWidth = bodyWidth;
            resizeFlag = true;
            resizeParams.w = thisWidth;
            resizeParams.l = windowOffset / 2;
        }

        if (bodyHeight < thisHeight) {
            thisHeight = bodyHeight;
            resizeFlag = true;
            resizeParams.h = thisHeight;
            resizeParams.t = windowOffset / 2;
        }

        if (resizeFlag) {
            self.cardDialog.resize(resizeParams);
        }
    };

    CardView.prototype._registerEventHandlers = function() {
        var self = this;
        console.log('REGISTER EVENTS!');
        var resizeTimeout;
        //RESIZE WINDOW ON BODY RESIZE
        window.addEventListener('resize', function() {
            // prevent calling event handler too often
            if (!resizeTimeout) {
                resizeTimeout = setTimeout(function() {
                    self.resize();
                    resizeTimeout = null;
                }, 500);
            }
        });

        //MINIMIZE WINDOW
        this.cardDialog.domNode.getElementsByClassName('dojoxFloatingMinimizeIcon')[0].addEventListener('click', function() {
            setTimeout(function() {
                registry.byId('appLayout').resize();
            }, 1000);
        });

        //OPEN NESTED CARDS
        [].forEach.call(this.cardDialog.domNode.getElementsByClassName('card__relation'), function(cardRelationPaneElem) {
            var selectElem = cardRelationPaneElem.getElementsByClassName('card__relation__actions__bind-select')[0];
            console.log(selectElem);
            self.createRelationSelectWidget(selectElem);
        });
    };
    return CardView;
});