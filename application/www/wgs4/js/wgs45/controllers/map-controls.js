//Fusion.getWidgetById('Map').loadMapGroup(Fusion.applicationDefinition.mapGroups['openLayers-fet']);

define([
    "dojo/text!wgs45/config/toolbar-buttons.json",
    "dijit/Toolbar",
    "dijit/ToolbarSeparator",
    "dijit/form/Button",
    "dijit/form/ToggleButton",
    "dijit/form/DropDownButton",
    'dijit/registry',
    "dijit/MenuBar",
    "dijit/DropDownMenu",
    "dijit/CheckedMenuItem",
    "dijit/PopupMenuBarItem",
    "dijit/MenuItem",
    "dijit/MenuSeparator"], function(buttonsConfig, Toolbar, ToolbarSeparator, Button, ToggleButton,DropDownButton, registry, MenuBar, DropDownMenu, CheckedMenuItem, PopupMenuBarItem, MenuItem,  MenuSeparator) {

    function MapControls(options) {
        var self = this;
        var buttons = JSON.parse(buttonsConfig);
        //console.log(buttons);
        this._mapName = options.mapName || 'tusur';
        this._mapWidgetIdBase = options.widgetId || 'Map';
        this.buttons = [];
        buttons.forEach(function(buttonDescription) {
            if (buttonDescription.active !== false) {
                self.buttons.push(buttonDescription);
            }
        })

        this.separateButtons = [{
            title: "+",
            widgetName: "ZoomInFixed",
            labelClass: "fa-plus",
            noText: true
        }, {
            title: "-",
            widgetName: "ZoomOutFixed",
            labelClass: "fa-minus",
            noText: true
        }];
    }

    MapControls.prototype.createControls = function() {
        var self = this;
        var Fusion = window.Fusion;
        this.toolbar = new Toolbar({}, "toolbar-" + this._mapName);
        this.buttons.forEach(function(buttonDescription) {
            var button;            
            switch (buttonDescription.type) {
                case "mapMode":
                    {
                        button = self._createMapModeButton(buttonDescription);
                        break;
                    }
                case "select":
                    {   
                        button = self._addMenu(buttonDescription);
                        break;
                    }
                case "button":
                    {
                        button = self._createSimpleButton(buttonDescription);
                        break;
                    }
                case "separator":
                    {
                        button = new ToolbarSeparator();
                    }
            }

            if (button) {
                self.toolbar.addChild(button);
            }
        });

        this.separateButtons.forEach(function(buttonDescription) {
            new Button({
                label: (
                buttonDescription.labelClass ?
                    '<i class="fa ' + buttonDescription.labelClass + '"></i> ' : "") + (buttonDescription.noText ? "" : buttonDescription.title),
                showLabel: true,
                iconClass: buttonDescription.iconClass,
                onClick: function() {
                    self.callWidget(buttonDescription);
                },
                'class': 'map__button'
            }, self._getWidgetId(buttonDescription.widgetName) + "-button");
        });

        Fusion.getWidgetById(this._getWidgetId('Map')).registerForEvent(Fusion.Event.MAP_BUSY_CHANGED, function() {
            self._setupWidgets();
        });

        this._setupWidgets();

        this.buttons.forEach(function(button) {
            
            if(self._mapName == 'tusur') {
                if (button.checkedOnMain) {
                    self.activateWidget(button);
                }
            } else {
                if (button.checked) {
                    self.activateWidget(button);
                }
            }
        });
    };

    MapControls.prototype._addMenu = function(buttonDescription){
        var self = this;
        console.log(buttonDescription);
         function getTitle() {
            return (this.labelClass ? "<i class=\"" + this.labelClass + "\"></i> " : "") + this.title;
        }

        var pMenuBar = new DropDownButton({label: buttonDescription.title});
        // var menuItems = buttonDescription;
        // var parentItem = pMenuBar;
         function addSubmenu(parentItem, menuItem) {
           // menuItems.forEach(function(menuItem) {
                var menuItemWidget = new DropDownMenu({});
                //parentItem.addChild(menuItemWidget);
                if (menuItem.children) {
                    var childPermissionCounter = menuItem.children.length; //reduce for every permission and separator
                    menuItem.children.forEach(function(childMenuItem) {
                        if(childMenuItem.active === false) {
                            childPermissionCounter--;
                            return;
                        }
                        //check for user privileges/ If NO, menu not shows
                        if(childMenuItem.privilege && !window.WGS.PRIVILEGE[childMenuItem.privilege]) {
                            childPermissionCounter--;
                            return;
                        }
                        var menuButton;
                        switch (childMenuItem.type) {
                            case "checked":
                                {
                                    menuItemWidget.addChild(menuButton = new CheckedMenuItem({
                                        label: getTitle.call(childMenuItem)
                                    }));
                                    break;
                                }

                            case "separator":
                                {
                                    childPermissionCounter--;
                                    menuItemWidget.addChild(menuButton = new MenuSeparator({}));
                                    break;
                                }
                            default:
                                {
                                    menuItemWidget.addChild(menuButton = new MenuItem({
                                        label: getTitle.call(childMenuItem),
                                        item: childMenuItem,
                                        onClick: function() {
                                            self.dispatchClickEvent(this)
                                        }
                                    }));
                                }
                        }
                        menuButton.domNode.dataset.menuAction = childMenuItem.action;
                        menuButton.domNode.dataset.menuActionParam = childMenuItem.id;
                        menuButton.domNode.classList.add('has-wgs-action')

                    });
                    if (!childPermissionCounter) {
                        //if nothing to show
                        menuItemWidget.destroy();
                        return;
                    }
                }

                parentItem.dropDown = menuItemWidget;

                // parentItem.addChild(new PopupMenuBarItem({
                //     label: getTitle.call(menuItem),
                //     dropDown: menuItemWidget
                // }));
            //});
        }
        addSubmenu(pMenuBar, buttonDescription);

        //registry.byId('toolbar-tusur').addChild(pMenuBar);
        
        pMenuBar.startup();
        return pMenuBar;
    }


    MapControls.prototype.callWidget = function(buttonDescription) {
        this._activateWidget(buttonDescription);
    };

    MapControls.prototype.activateWidget = function(buttonDescription) {
        this._deactivateAllWidgets(buttonDescription);
        this._activateWidget(buttonDescription);
    };

    MapControls.prototype._getWidgetId = function(widgetName) {
        return widgetName + "-" + this._mapName;
    };

    MapControls.prototype._deactivateAllWidgets = function(ignoredButtonDescription) {
        var Fusion = window.Fusion;
        var self = this;


        this.buttons.forEach(function(buttonDescription) {
            if ((buttonDescription.type !== "mapMode") || (buttonDescription.widgetName == ignoredButtonDescription.widgetName)) {
                return;
            }
            var correspondingWidgetId = self._getWidgetId(buttonDescription.widgetName);
            var widgetButton = registry.byId(self._getWidgetId(buttonDescription.widgetName) + "-button");
            var buttonIsActive = widgetButton.get('checked');

            registry.byId(self._getWidgetId(buttonDescription.widgetName) + "-button").set('checked', false);
            if (buttonIsActive) {
                Fusion.getWidgetById(correspondingWidgetId).deactivate();
            }
        });
    };

    MapControls.prototype._activateWidget = function(buttonDescription) {
        var Fusion = window.Fusion;
        var correspondingWidgetId = this._getWidgetId(buttonDescription.widgetName);

        Fusion.getWidgetById(correspondingWidgetId).activate();
        /*if (buttonDescription.type == "mapMode") {
            registry.byId(this._getWidgetId(buttonDescription.widgetName) + "-button").set('checked', true);
        }
*/
        this._setupWidgets();
    };

    MapControls.prototype._setupWidgets = function() {
        var self = this;
        var Fusion = window.Fusion;
        this.buttons.forEach(function(buttonDescription) {
            if (buttonDescription.type == "separator") {
                return;
            }
            var correspondingWidgetId = self._getWidgetId(buttonDescription.widgetName);
            var widgetButton = registry.byId(correspondingWidgetId + "-button");

            if (widgetButton) {
                widgetButton.set('disabled', !Fusion.getWidgetById(correspondingWidgetId).isEnabled());
            }
        });

    };

    MapControls.prototype._createSimpleButton = function(buttonDescription) {
        var self = this;
        var button = new Button({
            label: (
            buttonDescription.labelClass ?
                '<i class="fa ' + buttonDescription.labelClass + '"></i> ' : "") + (buttonDescription.noText ? "" : buttonDescription.title),
            showLabel: true,
            
            iconClass: buttonDescription.iconClass,

            onClick: function() {
                if (buttonDescription.widgetName) {
                    return self.callWidget(buttonDescription);
                }

                if (buttonDescription.actionName) {

                    var widgetEvent = new CustomEvent("menu-select", {
                        bubbles: true,
                        detail: {
                            action: buttonDescription.actionName,
                            param: buttonDescription.actionParam
                        }
                    });
                    this.domNode.dispatchEvent(widgetEvent);
                }
            },
            id: self._getWidgetId(buttonDescription.widgetName || buttonDescription.actionName) + "-button",
            'class': 'map__toolbar__button ' + (buttonDescription.min ? 'map__toolbar__button__min-mode' : '')
        });

        var mapWidgetId = self._mapWidgetIdBase + '-' + self._mapName;
        var Fusion = window.Fusion;
        button.set('disabled', !Fusion.getWidgetById(mapWidgetId).hasSelection());

        if (buttonDescription.actionName) {
            document.getElementById(mapWidgetId).addEventListener('map-selection', function(evt) {
                console.log('map selection event fired');
                console.log(evt.detail.numSelected);
                console.log(evt.detail.hasSelection);
                if(buttonDescription.actionName === 'card-add') {
                    button.set('disabled', !evt.detail.hasSelection || evt.detail.numSelected != 1);
                }
                else {
                    button.set('disabled', !evt.detail.hasSelection);
                }
            });
        }

        return button;
    };

    MapControls.prototype._createMapModeButton = function(buttonDescription) {
        var self = this;
        return new ToggleButton({
            label: (
            buttonDescription.labelClass ?
                '<i class="fa ' + buttonDescription.labelClass + '"></i> ' : "") + (buttonDescription.noText ? "" : buttonDescription.title),
            showLabel: true,
            iconClass: buttonDescription.iconClass,

            checked: buttonDescription.checked,
            onClick: function() {
                self.activateWidget(buttonDescription);
            },
            id: self._getWidgetId(buttonDescription.widgetName) + "-button",
            'class': 'map__toolbar__button ' + (buttonDescription.min ? 'map__toolbar__button__min-mode' : '')
        });
    };

    MapControls.prototype.dispatchClickEvent = function(menuItem) {
        var self = this;
        console.log(menuItem);
        if(menuItem.item.type == 'button') {
            console.log('button');
            if (menuItem.item.widgetName) {
                    return self.callWidget(menuItem.item);
                }

            if (menuItem.item.actionName) {
                var widgetEvent = new CustomEvent("menu-select", {
                    bubbles: true,
                    detail: {
                        action: menuItem.item.actionName,
                        param: menuItem.item.actionParam
                    }
                });
                menuItem.domNode.dispatchEvent(widgetEvent);
            }
        }
        if(menuItem.item.type == 'mapMode') {
            console.log('mapMode');
            self.activateWidget(menuItem.item);
        }
        /*var widgetEvent = new CustomEvent("menu-select", {
            bubbles: true,
            detail: {
                action: menuItem.domNode.dataset.menuAction,
                param: menuItem.domNode.dataset.menuActionParam
            }

        });
        menuItem.domNode.dispatchEvent(widgetEvent);*/

    };

    return MapControls;
});