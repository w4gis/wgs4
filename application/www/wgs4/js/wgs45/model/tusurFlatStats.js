define(['dojo/request/xhr', "dojo/DeferredList", "dojo/_base/Deferred"], function(xhr, DeferredList, Deferred) {
    function TusurFlatStatsModel() {
        
    };

    TusurFlatStatsModel.prototype.getStats = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetStats", {
            handleAs: "json",
            data: {'data': featureLayerId}
        }).then(
        function(stats){
            callback(stats, null);
        }, function(error){
            callback(null, error);
        });
    }

    /*TusurFlatStatsModel.prototype._getCurrentPlacesByFeatureLayerId = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetCurrentPlacesByFeatureLayerId", {
            handleAs: "json",
            data: featureLayerId
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };

    TusurFlatStatsModel.prototype._getFreePlacesByFeatureLayerId = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetFreePlacesByFeatureLayerId", {
            handleAs: "json",
            data: featureLayerId
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };

    TusurFlatStatsModel.prototype._getMaxByDocsPlacesByFeatureLayerId = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetMaxByDocsPlacesByFeatureLayerId", {
            handleAs: "json",
            data: featureLayerId
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };

    TusurFlatStatsModel.prototype._getMaxPlacesByFeatureLayerId = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetMaxPlacesByFeatureLayerId", {
            handleAs: "json",
            data: featureLayerId
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };

    TusurFlatStatsModel.prototype._getSexOfStudentsByFeatureLayerId = function(featureLayerId, callback) {
        xhr.post("/map/TusurFlatStats/GetSexOfStudentsByFeatureLayerId", {
            handleAs: "json",
            data: featureLayerId
        }).then(function(result) {
            callback(null, result.records);
        }, function(error) {
            callback(error);
        });

    };
*/





    return new TusurFlatStatsModel();
})