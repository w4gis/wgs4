define([
    "dojo/Deferred",
    "dojo/DeferredList",
    "dojo/request/xhr",
    "dojo/text!wgs45/package.json"], function(Deferred, DeferredList, xhr, packageInfo) {

    function WgsApp(wgsConfig) {
        var appInfo = JSON.parse(packageInfo);

        this._prohibitedCommands = {};
        this._version = appInfo.version;
        this._versionFlags = {
            'debug': isDebug(),
            'next': wgsConfig.FLAGS.NO_DRIVERS > 0
        };
        this._maps = [];
        this._authenticated = false;

        var authInfoDeferred = this._setupAuthInfo();
        var getMapsDeferred = this._getAllMaps();


        this.initDeferred = new DeferredList([authInfoDeferred, getMapsDeferred]);

    }

    WgsApp.prototype.getSelectionXml = function(mapName) {
        return window.WGS_APP.selText[mapName];

    };

    WgsApp.prototype.getVersion = function() {
        var flagsString = "";
        for (var i in this._versionFlags) {
            flagsString += (this._versionFlags[i] ? (i + ", ") : "");
        }

        flagsString = flagsString.slice(0, -2);
        
        return this._version +  (flagsString ? (" (" + flagsString + ")") : '');
    };

    WgsApp.prototype.getMaps = function() {
        return this._maps;
    };

    WgsApp.prototype.getAuthStatus = function() {
        return this._authenticated;
    };

    WgsApp.prototype.getProhibitedCommands = function() {
        return this._prohibitedCommands;
    }
    /**
     *  PRIVATE METHODS
     */
    WgsApp.prototype._setupAuthInfo = function() {
        var authInfoDeferred = new Deferred();
        var self = this;

        xhr.get("/authorization/status", {
            handleAs: "json"
        }).then(function(state) {
            for (var i in state) {
                if (i != "auth") {
                    self._prohibitedCommands[i] = true;
                }
            }
            self._authenticated = !! state.auth;

            authInfoDeferred.resolve({
                isAuthenticated: !! state.auth
            });
        }, function() {
            self._authenticated = false;

            authInfoDeferred.resolve({
                isAuthenticated: false
            });
        });

        return authInfoDeferred;
    }


    WgsApp.prototype._getAllMaps = function() {
        var self = this;
        var getMapsDeferred = new Deferred();

        xhr.get('/map/all', {
            handleAs: 'json'
        }).then(function(maps) {
            self._maps = maps;
            getMapsDeferred.resolve(maps);
        }, function() {
            getMapsDeferred.reject();
        });

        return getMapsDeferred;
    };


    return WgsApp;


    function isDebug() {
        return location.toString().indexOf('debug') > -1 || location.toString().indexOf('/tusur') > -1;
    }
});