dojoConfig = {
	async: true,
	has: {
		"dojo-firebug": false
	},
	baseUrl: "/wgs4/js/",
	parseOnLoad: false,
	locale: localStorage ? (localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale') : 'ru-ru' ) : 'ru-ru',
	packages: [
		{ name: "dojo", location: "wgs-release/dojo" },
		{ name: "dijit", location: "wgs-release/dijit" },
		{ name: "dojox", location: "wgs-release/dojox" },
		{ name: "wgs4", location: "wgs", main: "Application" },
		{ name: "wgs45", location: "wgs45", main: "application" },
		{ name: "dgrid", location: "dgrid" },
		{ name: "dstore", location: "dstore" }
	]
};
