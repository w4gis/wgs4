define({
    root:{
	    title	: "Locale & language",
	    back	: "Settings",
	    language: 'Select language',
	    systemDefault: 'System default',
	    apply: 'Apply immediately'
    },
    'ru-ru' : true
});
