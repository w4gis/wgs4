define({
		title	: "Редактирование объектов",
		map		: "Map",
		legend	: "Layers",
		exit	: "Log out",
		redlineTitle: 'Drawing',		
		redline: 'Draw new features',
		taskPaneTitle	: 'Карточка объекта',
		back		: 'Все задачи',
		backToEm	: 'Назад',
		layers: 'Слои для редактирования',
		help: 'Выберите один из доступных для редактирования слоёв из списка ниже',
		helpTitle: 'Редактирование геоданных',
		
		insertFeature: 'Добавить объект',
		drawFeature: 'Нарисовать объект',
		getLocation : 'Моё местоположение',
		point : 'Добавить точку',
		line : 'Добавить линию',
		drawPoint: 'Нарисовать точку',
		drawLine: 'Нарисовать линию',
		drawPoly: 'Нарисовать полигон',
		removeFeature: 'Удалить объект',
		deleteBinded: 'Удалить привязанные карточки?',
		deleteSelected: 'Удалить выбранный объект',
		showMap: 'на карту',
		hideMap: 'скрыть карту'
	
});
