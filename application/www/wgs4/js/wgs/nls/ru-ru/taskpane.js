define({
	legend	: 'Слои',
	tasks	: 'Задачи',
	map		: 'Карта',
	themes: 'Тематические отчеты',
	back	: 'Карта',
	measure	: 'Расстояние и площадь',
		
	analysys : 'Инструменты анализа',
	administration : 'Администрирование',
	editorTitle : 'Редактирование данных',		
	desktopOnly : 'ПК',
	projectadmin: 'Управление проектом',
	objectadmin: 'Управление данными',
	
	featureInfo : 'Геометрический анализ',	
	redline: 'Редактирование геоданных',
	title	: 'Выберите задачу',
	installApplication: 'Установить WGS4 как приложение',
	firefoxOnly : 'только Firefox'
});
