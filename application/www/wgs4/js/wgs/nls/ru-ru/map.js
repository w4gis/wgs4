define({
		title	: "Карта",
		map		: "Карта",
		legend	: "Слои",
		exit	: "Выход",
		
		clearSelection: 'Очистить выделение',
		myLocation: 'Место-положение',
		initialView: 'Показать всю карту',
		switchBasemap: 'Выбрать подложку',
		prevView: 'Предыдущий<br/>вид',
		nextView: 'Следующий<br/>вид',
		
		enableOL: 'Включить<br/>OpenLayers',
		disableOL: 'Отключить<br/>OpenLayers',
	reload: 'Перезапустить приложение',
		showMap: 'на карту',
		hideMap: 'скрыть карту'
		
});
