define({
	root: {
		
		back	: 'Geom',
		legend	: 'Legend',
		tasks	: 'Tasks',
		map		: 'Map',
		selectCs: 'Select CS',
		availableCs: 'Coordinate systems',
		title	: 'Coordinate systems',
		
		mercator: 'Pseudo Mercator',
		pz		: 'Earth Parameters - 90',
		pu		: 'Pulkovo - 42',
		wgs		: 'WGS - 84'
	},
	'ru-ru': true
});
