define([
	'dojo/dom-construct','dojo/dom',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',"dojo/on","dojo/dom-style","dojo/query","dojo/_base/array","dojox/mobile/Pane",'dojo/text!wgs4/views/legend.html'], 
	function(domConstruct, dom, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil){
  	return {
		_hasMenu : false,
		_viewTitle: 'leftView,legend',
		
		_setupDisplayMode: function(){
			
			domConstruct.place("legend", this.viewContainer.containerNode, "first");
			domStyle.set("legend", "display", "block")
			//domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
		},
		
		//init is triggered when view is created
		init : function(){
			//
			new window.AppData.ViewBaseClass( this );				
			this._setupDisplayMode()
			
			var self = this
			console.log('Subscribe to topic')
			topic.subscribe('legendRender', function(){
				self.reloadLegend()
			})
		},
		

		reloadLegend :function(){
			var checkboxes = query(".jxTreeRoot > .jxTreeContainer > .jxTree .jxTreeContainer .fusionLegendCheckboxContainer > input.fusionLegendCheckboxInput")
			var checkboxesFolders = query(".jxTreeContainer input.fusionLegendCheckboxInput")
			var checkboxeLabels = query(".jxTreeItem.fusionCheckboxItem")
			//<i class="fa fa-folder"></i> fa-folder
			//jxTreeContainer jxTreeBranch jxTreeBranchLastOpen
			
			var treeParents = query(".jxTreeContainer.jxTreeBranch > .jxTreeItem.fusionCheckboxItem > span.jxTreeLabel")
			arrayUtil.forEach(treeParents, function(parentNode, i){
				if(!parentNode['data-has-icon']){
					domConstruct.create('i',{
						'class':'fa folder-icon'						
					},parentNode, "first")
					parentNode['data-has-icon'] = true
				}
				//parentNode.innerHTML = '<i class="fa fa-folder-open"></i>' parentNode.innerHTML
			})
			
			var _styleCheckbox = function(cssClass, box, i){
				if(!domAttr.get(box, 'data-has-onclick-handler')){
					domAttr.set(box, 'data-has-onclick-handler',true)
					domAttr.set(box, 'data-checked', (box.checked ? 1 : 0) )
					domClass.add(box.parentNode, cssClass + " legend-checkbox")
					
					var id = "chk-"+Math.random()
					domAttr.set(box, 'id', id)
					
					var l = domConstruct.create('label',{
						'for': id,
						onClick: function(e){
							e.preventDefault();
							e.stopPropagation();						
						}						
					},box,'after')					
					
					var changeLayerChecked = function(e){
						e.preventDefault();
						e.stopPropagation();
						if(domAttr.get(box, 'data-on-change')){
							console.log(domAttr.get(box, 'data-checked') == 1)
							domAttr.set(box, 'data-checked', Math.abs(domAttr.get(box, 'data-checked') - 1 ))
							box.checked = (domAttr.get(box, 'data-checked') == 1)
							
							box['data-on-change']();
							
							setTimeout(function(){
								box.checked = (domAttr.get(box, 'data-checked') == 1)
								//alert(box.checked)
							}, 300)
						}
					}
					on(dom.byId(id), 'click' , function(e){
							e.preventDefault();
							e.stopPropagation();
							console.log(e)
							console.log(window.IS_TOUCH, window.IS_MOUSE)						
							if(!window.IS_MOUSE && !window.IS_TOUCH){
								///alert('clicked')
								console.log('clicked')						
								window.IS_MOUSE_CLICK = true
								changeLayerChecked(e)
							}
						})
					//})(box);
					
					on(l,'mousedown',function(e){
						e.preventDefault();
						e.stopPropagation();
						if(!window.IS_TOUCH){
							console.log('mdown')
							window.IS_MOUSE = true
							changeLayerChecked(e)
						}
					})
					on(l,'touchstart',function(e){
						e.preventDefault();
						e.stopPropagation();
						window.IS_TOUCH = true
						console.log('touched')
						//alert('touched')
						changeLayerChecked(e)
						
					})
					on(l,'click',function(e){
						
						
					})
					
				}				
			}
			function _stylizeLabels () {
				for(var i=0; i< checkboxeLabels.length; i++)
					domClass.add(checkboxeLabels[i], "mblListItem")					
			}
		
			
			function styleCheckboxFolder (box, i) {
				_styleCheckbox('slideThree ' + ( dojo.config.locale.substr(0,2)), box, i);				
			}
			
			function styleCheckboxItem (box, i) {
				_styleCheckbox('squaredFour ' + ( dojo.config.locale.substr(0,2)), box, i);				
			}
			
			arrayUtil.forEach(checkboxes, styleCheckboxItem)
			arrayUtil.forEach(checkboxesFolders, styleCheckboxFolder)
			_stylizeLabels ()
		},
		
		//triggered before showing view
		_beforeActivate: function(){
			topic.publish('legendRender')
			if(registry.byId('bottom-legend-button'))
				registry.byId('bottom-legend-button').set('selected',true)			
		},
		afterActivate: function(){
			//
		}			
	}
})
