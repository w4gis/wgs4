define([
	'dojo/query','dojo/_base/declare',
	'dojo/dom-attr','dojo/dom',
	'dojo/dom-style','dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on','dojo/hash','dojox/mobile/EdgeToEdgeCategory','dojox/mobile/EdgeToEdgeList','dojox/mobile/ListItem',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/cs.html'
	],function(query, declare, domAttr, dom, domStyle, domClass, domConstruct, has, on, hash, Category, List, ListItem, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			_hasMenu: false,
			_hasBack: "leftView,finfo",
			
			_featureInfo:[],
			_cs: {
				'pm': {
					'root'	: 'Pseudo Mercator',
					'en-us'	: 'Pseudo Mercator',
					'ru-ru'	: 'Pseudo Mercator'
				},
				'pz':{
					'root'	: 'Earth Parameters-90',
					'en-us'	: 'Earth Parameters-90',
					'ru-ru'	: 'Парметры Земли-90'
				},
				'pu':{
					'root'	: 'Pulkovo-42',
					'en-us'	: 'Pulkovo-42',
					'ru-ru'	: 'Пулково-42'
				},
				'wgs':{
					'root'	: 'WGS-84',
					'en-us'	: 'WGS-84',
					'ru-ru'	: 'WGS-84'
				}
			},
			selectedCs: 'pz',
			init : function(){
				new window.AppData.ViewBaseClass( this );	
				
				if(!has('phone')){
					//domStyle.set(this.heading.domNode,'display','none')
					//domStyle.set(this.viewContainer.containerNode,'top','0')
					domClass.add(this.viewContainer.domNode,'heightAuto');
				}
				
				var csListItems = this.csList.getChildren();
				var self = this
				for(var i=0; i<csListItems.length; i++)
					csListItems[i].onClick = function(){
						self._setCs(this['data-cs'])
					}
			},
			_setCs: function(cs){
				this.selectedCs = cs
				this._hasTransitionOptions =  { cs : this.selectedCs, from: 'cs' } 
				//if(has("phone"))
				this.backButton.set("transitionOptions", {params: this._hasTransitionOptions})
				
			},
			_updateState: function(){
				var cs = this.selectedCs.substr(0,1).toUpperCase() + this.selectedCs.substr(1,this.selectedCs.length-1)
				if(this["is"+cs])
					this["is"+cs].set('checked',true)
			},
			_beforeActivate: function(){
				//
				
				if(this.params && this.params.from && this.params.cs){
					this.selectedCs = this.params.cs
					this._hasBack =  'leftView,' + this.params.from
					this._updateState()
					this.backButton.target = this._hasBack;
				}
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				
				//window.AppData.BeforeActivateTitleChangeHandler.call(this)				

			}		
		}
	}
)
