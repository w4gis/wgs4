define([
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/taskpane.html'
	],function(query, domAttr, domStyle, domClass, domConstruct, has, on,xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...
			_hasMenu: false,
			_viewTitle: 'task',
			
			init : function(){
				new window.AppData.ViewBaseClass( this );	
				
				domStyle.set("TaskPane", "display", "none")
				domClass.add(this.displayContainer.domNode, 'scrollable-pane-100-height');
				if(!has('phone')){
					domStyle.set(this.displayContainer.domNode,'top','45px')
				}
				var url = 'http://88.204.76.6/manifest.webapp?dynamic';
				var self = this;
				
				if(window.navigator.mozApps){
					
					var	request = window.navigator.mozApps.checkInstalled(url);
					request.onerror = function(e) {
					  alert("Error calling checkInstalled: " + request.error.name);
					};
					request.onsuccess = function(e) {
					  if (request.result) {
						domStyle.set( self.installApplication.domNode, 'display', 'none' )
						console.log("App is installed!");
					  }
					  else {
						domStyle.set( self.installApplication.domNode, 'display', '' )
						console.log("App is not installed!");
					  }
					};
				} else {
					domStyle.set( self.installApplication.domNode, 'display', 'none' )
				}
				
				this.installApplication.onClick = function() {
					
					if(window.navigator.mozApps){
					var request = window.navigator.mozApps.install(url);
						request.onsuccess = function () {
						// Save the App object that is returned
						  var appRecord = this.result;
						  alert('Installation successful!');
						};
						request.onerror = function () {
						  // Display the error information from the DOMError object
						  alert('Install failed, error: ' + this.error.name);
						};
					} else {
						alert('Installation is not supported by your browser.')
					}
					
				}
				
				/*
				 * disable all prohibited actions
				 * 
				 */ 
				if(window.AppData.prohibited['WgsMeasure'])
					this.measurementActivator.destroy();
				if(window.AppData.prohibited['WgsThemes'])
					this.themesActivator.destroy();
				if(!window.WGS.PRIVILEGE.PROJECTOBJECT_ADMIN)
					this.objAdminActivator.destroy();
				if(!window.WGS.PRIVILEGE.PROJECT_ADMIN)
					this.projAdminActivator.destroy();
					
					
			},
			_beforeActivate: function(){
				//
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				
				
			}
			
			
		}
	}
)
