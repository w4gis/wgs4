define([
	'dojo/query',
	'dojo/dom-attr','dojo/dom-class',
	'dojo/dom-style', 'dojo/dom',
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dojo/sniff',
	'dijit/registry',
	'wgs4/wgs-fusion','dojox/mobile/Icon','dojo/dom-construct', 'dojox/mobile/Accordion','dojo/text!wgs4/views/map.html'
	],function(query, domAttr, domClass, domStyle, dom, on, xhr, arrayUtil,has,registry, WgsFusion, Icon, domConstruct, Accordion){
		return {

			_hasMenu 		: true, 	// whether to display menu button on The view header or not
			_hideTitle		: true,
			_viewTitle		: 'map',	// used to automatically select tabbar button (sync selected view with buttons)
										// should be the same as view name in app-config.json

			_minToolbarWidth: 750,		// min width for main map toolbar (If window width is not enough, smaller button will be used)
			_activeMap		: '', 		// as of version 0.2 can be "openLayers" or "noOpenLayers", initialised at _initializeOpenLayersEnabler
										// and used to turn on/off Open Layers Basemaps

			_isOpenLayersEnabled : true, //whether there is open layers enable button or not
			_isFlexibleMainMapBarEnabled : true, //auto expand/collapse map buttons bar
			_moreToolsButtonId : 'MoreTools', //expandable toolbar's button,
			_mapWidgetId	: 'Map', 	//Fusion MapWidget id
			/*
			 * 	PUBLIC METHODS
			 */

			//init method called when view created
			init : function(){
				new window.AppData.ViewBaseClass( this );
				domAttr.set ('wgs-copyright-version', 'innerHTML',
					window.AppData.getVersion());

				this._startFusionApplication();

			},

			// resize map when view is activated to prevent map visualization errors
			_afterActivate: function(){
				if(Fusion && Fusion.getWidgetById(this._mapWidgetId))
					Fusion.getWidgetById(this._mapWidgetId).resize()
			},

			/*
			 * 	PRIVATE METHODS
			 */

			 /*
			  * once Fusion is loaded we can setup UI and events
			  *
			  */
			 _configureFusionApplication : function(){

				//Fusion is loaded, we can listen to this var by setInterval or smth like this
				window.AppData.LOADED = true

				//setup Fusion events and buttons
				this._initializeMapButtons();
				this._initializeMapBusyEvents();
				this._initializeSelectionEvents();
				if(this._isOpenLayersEnabled)
					this._initializeOpenLayersEnabler();

				//Map is loaded and available for quering or anth else
				Fusion.getWidgetById(this._mapWidgetId).registerForEvent(Fusion.Event.MAP_LOADED, function(){
					window.AppData.MAP_LOADED = true
				});

				//task pane (Some rich fusion widgets are loaded)
				Fusion.registerForEvent(Fusion.Event.TASK_PANE_LOADED, function(){
					window.AppData.TASKS_LOADED = true
				});

				//setup show/hide toolbar button
				on(dom.byId(this._moreToolsButtonId), 'click', function(){
					var bb = query('.map-button-bar')[0]
					domClass[ ( (domClass.contains(bb, 'hidden')) ? 'remove' : 'add' ) ] (bb, 'hidden')
				})

			},

			/*
			 * Hack Fusion classes;
			 * Check if session is valid;
			 * Get localized ApplicationDefinition and startup everything.
			 *
			 */
			_startFusionApplication: function(){
				var self = this

				// redefine Fusion widgets to add some Dojo (TM).
				new WgsFusion();

				//get localized Application Definition
				var locale = localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale').substr(0,2) : '';
				var options = {
					//sessionId: getCookie('PHPSESSID'),
					applicationDefinitionURL : "/map/fusion/getApplicationDefinitionM?lang=" + locale
				}

				//if ANYTHING wrong happens
				function onFusionError(e){
					console.error('Fusion error: ',e)
				}


				window.Fusion.registerForEvent(window.Fusion.Event.FUSION_INITIALIZED, function(){ self._configureFusionApplication() });
				window.Fusion.registerForEvent(window.Fusion.Event.FUSION_ERROR, onFusionError);
				window.Fusion.initialize(options);

			},

			/*
			 * 	Restyle Fusion widget's buttons
			 */
			_initializeMapButtons: function(){
				var zoomIn  = query("#ZoomInFixed .jxButtonLabel")[0],
					zoomOut = query("#ZoomOut .jxButtonLabel")[0],
					geo 	= query("#Geolocation .jxButtonLabel")[0],
					basemap = query("#BasemapSwitcher .jxButtonLabel")[0],
					toolBarItems 	= query(".toolbar-container .jxToolItem .jxButtonLabel"),
					clearSelection 	= query("#ClearSelection .jxButtonLabel")[0],
					initialView 	= query("#vertInitialMapView .jxButtonLabel")[0],
					nextView 		= query("#NextView .jxButtonLabel")[0],
					previousView 	= query("#PreviousView .jxButtonLabel")[0],
					self 	= this;

				domAttr.set(toolBarItems[0]	, 'innerHTML', '<i class="fa fa-arrows"></i>')
				domAttr.set(toolBarItems[1]	, 'innerHTML', '<i class="fa fa-hand-o-up"></i>')
				domAttr.set(toolBarItems[2]	, 'innerHTML', '<i class="fa fa-search-plus"></i>')
				domAttr.set(zoomIn			, 'innerHTML', '<i class="fa fa-plus"></i>')
				domAttr.set(zoomOut			, 'innerHTML', '<i class="fa fa-minus"></i>')

				domAttr.set(clearSelection	, 'innerHTML', '<i class="fa fa-square-o"></i><br/><span class="icon-title">' + this.nls.clearSelection + '</span>')
				domAttr.set(geo				, 'innerHTML', '<i class="fa fa-location-arrow"></i><br/><span class="icon-title">' + this.nls.myLocation + '</span>')
				domAttr.set(basemap			, 'innerHTML', '<sup><b>+</b></sup><br/><span class="icon-title">' + this.nls.switchBasemap + '</span>')
				domAttr.set(initialView		, 'innerHTML', '<i class="fa fa-arrows-alt"></i></sup><br/><span class="icon-title">' + this.nls.initialView + '</span>')
				domAttr.set(previousView	, 'innerHTML', '<i class="fa fa-mail-reply"></i></sup><br/><span class="icon-title">' + this.nls.prevView + '</span>')
				domAttr.set(nextView		, 'innerHTML', '<i class="fa fa-mail-forward"></i></sup><br/><span class="icon-title">' + this.nls.nextView + '</span>')

				var basemapWrapper = domConstruct.create('span', {}, basemap, 'first')
				var baseMapIcon = new Icon({
					icon:"/wgs4/icons/grey/layers_4.png",
					iconPos1: "0,0,128,128"
				}, basemapWrapper)
				domClass.add(basemapWrapper, 'mblSpriteIconWrapper')

				window.onresize = function(){
					self._resizeBars();
				}
				// resize main toolbar to fit viewport width
				self._resizeBars()
			},

			/*
			 * set callbacks for selection on/ off events
			 * window.AppData should be initialized
			 */
			_initializeSelectionEvents: function(){

				var mapWidget = Fusion.getWidgetById ( this._mapWidgetId );

				// call each ACTIVE selection callbacks
				// @selection - selection object from Fusion
				// @isSelectionCleared - true when nothing selected
				function doActiveCallbacksWithSelection(selection, isSelectionCleared) {
					//display the selection to the user in some way ...
					var c = window.AppData.selectionCallbacks
					for(var i in c){
						if(c[i].active)
							c[i].fn(selection, isSelectionCleared)
					}

				}
				var doSelectionOff = function(selection){
					doActiveCallbacksWithSelection (selection, true)
				}

				var doSelectionOn = function(selection){
					doActiveCallbacksWithSelection (selection, false)
				}


				//clear the selection results
				function selectionOffEventHandler() {
					mapWidget.getSelection( doSelectionOff );
					console.log('selection cleared');
					//TODO: separate callback for card button
					//document.getElementById("cardButton").disabled = true;
				}

				//a new selection has been made, request it
				function selectionOnEventHandler() {
					mapWidget.getSelection( doSelectionOn );
				}

				//setup selection events callbacks
				mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_ON, selectionOnEventHandler);
				mapWidget.registerForEvent(window.Fusion.Event.MAP_SELECTION_OFF, selectionOffEventHandler);

			},

			/*
			 *  show progress if map is loading
			 *  hide progress if not
			 *  (this function add FusionEvent listener)
			 */
			_initializeMapBusyEvents: function(){
				var mapWidget = Fusion.getWidgetById( this._mapWidgetId );
				var self = this;

				//
				function mapBusyEventHandler(){
					window.AppData.WGS_BUSY = !window.AppData.WGS_BUSY
					//console.log(window.AppData.WGS_BUSY, self.WgsProgressIndicator)
					if(self.WgsProgressIndicator){
						if(window.AppData.WGS_BUSY)
							self.viewContainer.containerNode.appendChild( self.WgsProgressIndicator.domNode );
						self.WgsProgressIndicator[ ( window.AppData.WGS_BUSY ? "start" : "stop" ) ]()
					}
				}
				mapWidget.registerForEvent( window.Fusion.Event.MAP_BUSY_CHANGED, mapBusyEventHandler );
			},


			/*
			 * setup openlayers button behaviour,
			 * @buttonId - OpenLayers button id (just on/off button)
			 * @basemapSwitcherId - Basemap Switcher button id (dropdown to select active basemep)
			 */
			_initializeOpenLayersEnabler: function(buttonId, basemapSwitcherId){
				var buttonId = buttonId || 'EnableOpenLayers',
					basemapSwitcherId = basemapSwitcherId || 'BasemapSwitcher',
					self = this;
				//console.log(dom.byId(buttonId), dom.byId(basemapSwitcherId))
				if( dom.byId(buttonId) && dom.byId(basemapSwitcherId) ){

					domStyle.set( dom.byId(buttonId).parentNode, 'display', '' );
					/*
					 * if isInitial is true, just set basemap switcher labels,
					 * else: change map to enable/disable OpenLayers basemaps
					 */
					var handler = function(isInitial){
						var isInitial = isInitial || false
						self._activeMap = Fusion.getWidgetById('MapMenu').getMap().mapGroup.mapId;
						var newMapId = self._activeMap
						if(!isInitial)
							newMapId = (self._activeMap == "noOpenLayers") ? "openLayers" : "noOpenLayers";
						if(!isInitial){
							if(window.AppData.MapGroupSwitcher[newMapId]){
								Fusion.getWidgetById(basemapSwitcherId).setBasemap( 'None' );
								window.AppData.MapGroupSwitcher[newMapId]();
							}
						}

						setTimeout(function(){
							//self.set('selected',(thisView._activeMap == "noOpenLayers"))
							domAttr.set(dom.byId(buttonId),'innerHTML',
								(newMapId == "openLayers") ? '<span class="icon-title icon-title-show-always">' + self.nls.disableOL + '</span>' :
								'<span class="icon-title icon-title-show-always">' + self.nls.enableOL + '</span>'
							)
							domClass[ ( (newMapId == "openLayers") ? 'remove' : 'add' ) ]( dom.byId(basemapSwitcherId), 'jxDisabled')
						},500)
					}
					handler(true);

					on(dom.byId(buttonId), 'click', function(){
						handler()
					})
				}
			},


			/*
			 * 	Show/hide toolbar buttons labels if width is enough/not enough
			 */
			_resizeBars: function(){
				if(this._isFlexibleMainMapBarEnabled) {
					var toolBar = query('.map-button-bar.mobile-bar')
					//console.log(toolBar)
					if(toolBar[0] && dom.byId('toolbar-parent') && dom.byId('wgs4_map')){
						var toolBarWidth = domStyle.get("wgs4_map", 'width')
						var minWidth = this._minToolbarWidth;
						console.log(toolBarWidth)
						try {
							domClass[ ( toolBarWidth > minWidth ) ? 'remove' : 'add' ]( 'toolbar-parent', 'small' )
							domClass[ ( toolBarWidth > minWidth ) ? 'add' : 'remove' ]( 'toolbar-parent', 'full' )
						} catch (e){
							console.log('please clear/update cache')
						}
						if( toolBarWidth > minWidth )
							domClass[ 'remove' ]( toolBar[0], 'hidden' )
					}
				}
			}

		}
	}
)
