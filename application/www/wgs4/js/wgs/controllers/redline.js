define([
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-construct',
	'dojo/sniff',
	'dojo/on','dojo/hash',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/redline.html'
	],function(query, domAttr, domStyle, domConstruct, has, on, hash, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			_hasBack: "leftView,taskpane",
			
			task : null,
			setupTask: function(){
				var h = this.params.widget
				if(h == "re") this.task = "Redline"
				if(h == "fi") this.task = "FeatureInfo"
				if(h == "me") this.task = "Measure"
				if(h == "pr") this.task = "QuickPlot"
					
			},			
			init : function(){
				window.AppData.taskPaneTitle = this.heading
				window.AppData.taskPaneBackButton = this.backButton
				new window.AppData.ViewBaseClass( this );	
				if(!has('phone')){
					//domStyle.set(this.heading.domNode,'display','none')
					//domStyle.set(this.bottomBar.domNode,'display','none')
					//domStyle.set(this.viewContainer.containerNode,'top','0')
					domStyle.set(this.viewContainer.containerNode,'bottom','0')
				}
				this.setupTaskPane()				
			},
			setupTaskPane: function(){
				var self = this
				this.setupTask()				
				if(!query('#'+this.viewContainer.containerNode.id+' #TaskPane').length)
					domConstruct.place("TaskPane", this.viewContainer.containerNode, "first");
				domStyle.set("TaskPane", "display", "block")
				
				var loadTaskIfNotAlreadyLoaded = function(){
					var ctask = Fusion.getWidgetById('TaskPane').nCurrentTask
					console.log(ctask, Fusion.getWidgetById('TaskPane').aExecutedTasks[ctask], Fusion.getWidgetById('TaskPane').aExecutedTasks[ctask].indexOf(self.task))
					if(!ctask || !Fusion.getWidgetById('TaskPane').aExecutedTasks[ctask] ||
						!(Fusion.getWidgetById('TaskPane').aExecutedTasks[ctask].indexOf(self.task) > 0)){
						console.log('DoActivate!')
						Fusion.getWidgetById(self.task).activate()
					}
				}
				
				if(!window.AppData.TASKS_LOADED){
					var i = setInterval(function(){
						//console.log('interval')
						
						if(window.AppData.TASKS_LOADED){
							clearInterval(i)
							loadTaskIfNotAlreadyLoaded()
						}
					},200)
				}else {
					//console.log('already loaded')
					//console.log(Fusion.getWidgetById(self.task))
					loadTaskIfNotAlreadyLoaded()				
				}
			},
			_beforeActivate: function(){
				//
				//this.taskButton.set('selected',true)
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)
				this.setupTaskPane()
				
			},
			afterActivate: function(){
				//
			}
			
		}
	}
)
