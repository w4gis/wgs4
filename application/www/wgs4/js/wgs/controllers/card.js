define(['dojo/_base/window',
		'dojo/dom-construct',
		'dojo/dom-class',
		'dojo/dom',
		"dojo/date",
	
		'dojo/sniff',
		'dijit/registry',
		'dojo/topic', 'dojo/date/locale',
		'dojo/dom-attr', 'dojo/on', 'dojo/dom-style', 'dojo/query', 'dojo/_base/array',
		'dojox/mobile/Pane', 'dojo/store/Memory', 'dijit/form/Select', 'dojo/data/ObjectStore',
		'dojo/request/xhr', 'dojo/json', 'dojox/mobile/Accordion', 'dojox/mobile/ListItem', 'dojox/mobile/ContentPane',
		'dojox/mobile/SwapView', 'dojox/mobile/PageIndicator', 'dojox/mobile/TextBox', 'dojox/mobile/ExpandingTextArea', 'dojox/mobile/Container',
		'dojo/_base/connect', 'dojox/mobile/DatePicker', 'dojox/mobile/Button', 'dojox/mobile/EdgeToEdgeList', 'dojox/mobile/SimpleDialog', 
		'wgs4/wheelScrollableView', 'dojox/gesture/tap', 'dojox/mobile/SpinWheelDatePicker', 'dojox/mobile/SimpleDialog','dojo/text!wgs4/views/card.html'],
	function (win, domConstruct, domClass, dom, ddate, has, registry, topic, datelocale, domAttr, on, domStyle,
		query, arrayUtil, Pane, Memory, Select, ObjectStore, xhr, JSON, Accordion, ListItem,
		ContentPane, SwapView, PageIndicator, TextBox, Textarea, Container, connect, DatePicker, Button, List, Dialog, ScrollableView, EditableList) {

	return {
		//init is triggered when view is created
		_hasMenu : true,
		_hasMenuClass : 'mblGreyButton',
		_hasMenuTitle : '<i class="fa fa-save"></i>',
		_hasMenuCallback : function(){
			self.saveCard;
		},
		_viewTitle : 'card',
		_errors : {
			NO_PARENT_WIDGET : {
				id : 1,
				message : 'Accordeon not found'
			}
		},



		/*
		*	(!) This is a _disableEndDaysOfMonth method to fix datePicker error
		*/
		disableEndDaysOfMonth: function(){
			// summary:
			//		Disables the end days of the month to match the specified
			//		number of days of the month. Returns true if the day value is changed.
			// tags:
			//		private
			/*var pat = this.slots[0].pattern + "/" + this.slots[1].pattern,
				v = this.get("values"),
				date = datelocale.parse(v[0] + "/" + v[1], {datePattern:pat, selector:"date"}),
				daysInMonth = ddate.getDaysInMonth(date);*/
			var changedDay = false;

			var DataSlots = this.slots //v = this.get("values"), // [year, month, day]
				DataSlotsValue = [DataSlots[0].value, DataSlots[1].value, DataSlots[2].value]
				DataSlotsPattern = DataSlots[0].pattern + "/" + DataSlots[1].pattern + "/" + DataSlots[2].pattern;

			var dateFallback = datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
				datePattern : DataSlotsPattern,
				selector : "date"
			}) ? datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
				datePattern : DataSlotsPattern,
				selector : "date"
			}) : new Date;

			daysInMonth = ddate.getDaysInMonth( dateFallback );


			if(daysInMonth <  DataSlots[2].value){
				// day value is invalid for this month, change it
				changedDay = true;
				DataSlots[2]._spinToValue(daysInMonth, false/*applyValue*/);
			}
			this.disableValues(daysInMonth);
			return changedDay;
		},



		/*
		 * (!) This is a _getDateAttr method to fix datePicker error		 *
		 */
		getDateAttr : function () {
			// summary:
			//		Returns a Date object for the current values.
			// tags:
			//		private
			//console.log(window.w = this);
			var DataSlots = this.slots //v = this.get("values"), // [year, month, day]
				DataSlotsValue = [DataSlots[0].value, DataSlots[1].value, DataSlots[2].value]
				DataSlotsPattern = DataSlots[0].pattern + "/" + DataSlots[1].pattern + "/" + DataSlots[2].pattern;

			var dateFallback = datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
				datePattern : DataSlotsPattern,
				selector : "date"
			}) ? datelocale.parse(DataSlotsValue[0] + "/" + DataSlotsValue[1] + "/" + DataSlotsValue[2], {
				datePattern : DataSlotsPattern,
				selector : "date"
			}) : new Date;

			return dateFallback;
		},

		init : function () {
			//this.selCard = new Select();
			new window.AppData.ViewBaseClass( this );	
			//bt = this.SaveCardButton
			this.currentSelected = 0;
			this.frstCardId = 0;
			this.changedCards = new Array();
			var self = this;
			this.initializeSelectionEvents();
			on(dom.byId("SaveCardButton"), "click", function () {
				self.saveCard();
				//self.getCardsOfSelectedFeature(self.currentSelected);
			});
			if(has('phone')){
				domStyle.set(this.cardDisplayContainer.domNode, 'bottom', '45px')
			}

			//Change combobox with card name after swap view transition
			topic.subscribe('/dojox/mobile/viewChanged', function () {
				if (location.toString().indexOf('#card') > -1) {
					var newSwapViewId = registry.byId(arguments[0].id).getChildren()[0].id
						var swValue = newSwapViewId.substr(4, newSwapViewId.length - 1)
						var cbValue = registry.byId('targetForCmb1').get('value').substr(4, registry.byId('targetForCmb1').get('value').length - 1);
					if (Number(cbValue) != Number(swValue))
						registry.byId('targetForCmb1').set('value', 'ccbi' + swValue);

				}
			})

			connect.subscribe("/dojox/mobile/deleteListItem", function (item) {
				var itemid = item.id.substr(item.id.indexOf('n') + 1);
				console.log('всем булочек и пива');
				item.rightIconNode.style.display = "none";
				if (!item.rightIcon2Node) {
					item.set("rightIcon2", "mblDomButtonMyRedButton_0");
					item.rightIcon2Node.firstChild.innerHTML = "Удалить";
				}
				item.rightIcon2Node.style.display = "";
				handler = connect.connect(item.ightIcon2Node, "onclick", function () {
						console.log('удаляем' + itemid);
						var relArr=[];
						relArr[0]=itemid;
						xhr.post("/objectadmin/card/RemoveObjectRelation", {
							//sync : true,
							handleAs : "json",
							data: {
								removeMode : 1,
								'relationsId[]': relArr
							}
						}).then(function () {
							console.log('удалили' + itemid);
							item.destroy();
						});
					});
			});
		},

		/*
		 * 	Add new property group to card
		 * 	@label 		- Group label
		 * 	@groupId 	- Group WIDGET id
		 * 	@targetId	- Parent widget id
		 * 	@isProperty - true if group is NOT for card relations *
		 *
		 * 	* - relations are placed after all property groups
		 *
		 */
		addGroup : function (label, groupId, targetId, isProperty) {
			this.createAccordeon(targetId);

			if (!registry.byId('acc' + targetId)) {
				throw(this._errors.NO_PARENT_WIDGET);
			}

			// return if group already exists
			if (registry.byId(groupId))
				return;

			var newWidget = new ContentPane({
					'label' : label + '<i class="fa redline-opener"></i>',
					'id' : groupId
				});

			var panesCount = registry.byId('acc' + targetId).getChildren().length;
			if (isProperty) {
				registry.byId('acc' + targetId).addChild(newWidget, panesCount > 0 ? 1 : 0);
			} else
				registry.byId('acc' + targetId).addChild(newWidget);

			newWidget.addChild(new List({
					id : groupId + 'list',
				}))

			// place new accordeon panes in correct order (after accordeon pane title)
			var panes = query('#acc' + targetId + ' .mblContentPane.mblAccordionPane');
			for (var i = 0; i < panes.length; i++) {
				domConstruct.place(panes[i], registry.byId(panes[i].id)._at.id, 'after')
			}

		},

		/*
		 * 	Shows save card yes/no dialog;
		 * 	if yes - saves card;
		 * 	clears card, shows cards of given feature.
		 *
		 * 	Dialog will be created if doesn't exists or shown if it does.
		 *
		 * 	@featureId - geometry object id
		 *
		 */
		showSaveCardDialog : function (fid) {
		console.log("test");
			var self = this;
			//domStyle.set("SaveCardButton", "display", 'none');
			registry.byId("SaveCardButton").disabled = true;
			if (this._saveQuestionDialog) {
				this._saveQuestionDialog.show();
				return;
			}

			this._saveQuestionDialog = new Dialog({});
			win.body().appendChild(this._saveQuestionDialog.domNode);
			domConstruct.create('div', {
				innerHTML : 'Закрытие карточки',
				'class' : 'mblHeading'
			}, this._saveQuestionDialog.domNode)

			domConstruct.create('div', {
				innerHTML : '<p>Сохранить изменения перед закрытием карточки?</p><br/>' +
				'<button id="YesSaveCardButton" class="mblBlueButton" >Сохранить</button>&nbsp;' +
				'<button id="NoSaveCardButton" class="mblRedButton">Отмена</button>',
				'class' : 'mblSimpleDialogText',
				style : 'text-align:center'
			}, this._saveQuestionDialog.domNode)

			var funcYes = function () {
				self.saveCard();
				self._saveQuestionDialog.hide();
				if (fid > 0)
					self.getCardsOfSelectedFeature(fid);
				else {
					self.currentSelected = 0;
					self.clearCard();
					domStyle.set('cardSelectAnything', 'display', 'block');
				}
			}

			var funcNo = function () {
				self._saveQuestionDialog.hide();
				if (fid > 0)
					self.getCardsOfSelectedFeature(fid);
				else {
					self.currentSelected = 0;
					self.clearCard();
					domStyle.set('cardSelectAnything', 'display', 'block');
				}
			}

			new Button({}, 'YesSaveCardButton')
			new Button({}, 'NoSaveCardButton')
			on(registry.byId('YesSaveCardButton'), 'click', funcYes);
			on(registry.byId('NoSaveCardButton'), 'click', funcNo);

			this._saveQuestionDialog.show();
		},

		
		/*
		* Adds new relation group to card;
		*
		* @lbl - label for group
		* @id - new object id
		* @idtarget - id of target container
		*
		*/
		addRelationGroup : function (lbl, id, idtarget) {
			this.createAccordeon(idtarget);
			if (!(registry.byId('acc' + idtarget)) || registry.byId(id))
				return;
			var self = this;

			var newWidget = new ContentPane({
					'id' : id,
					'label' : lbl + '<i class="fa redline-opener"></i>'
				});
			registry.byId('acc' + idtarget).addChild(newWidget);
			domConstruct.create('table', {
				'id' : 'targetForButtons' + id
			}, id);
			domConstruct.create('tr', {
				'id' : 'targetTables' + id
			}, 'targetForButtons' + id);
			domConstruct.create('td', {
				'id' : 'targetCreateChild' + id
			}, 'targetTables' + id);
			domConstruct.create('td', {
				'id' : 'targetUnbindChild' + id
			}, 'targetTables' + id);

			var CreateButton = new Button({
					'id' : 'CreateChild' + id,
					'label' : 'Создать',
					'class' : 'mblBlueButton',
					'onClick' : function () {
						console.log("id reletion " + id.substr(id.indexOf('n') + 1));
						var relId = id.substr(id.indexOf('n') + 1);
						self.app.transitionToView(self.domNode, {
							target : 'leftView,relation',
							params : {
								first : '00',
								relationId : relId,
								cardId : idtarget
							}
						})
					}

				}, domConstruct.create('button', {}, 'targetCreateChild' + id));
			var UnbindButton = new Button({
					'id' : 'UnbindChild' + id,
					'label' : 'Отвязать',
					'class' : 'mblRedButton',
					'onClick' : function () {
						var lst = registry.byId(id + 'list');
						if (lst.isEditing)
							lst.endEdit();
						else
							lst.startEdit();
					}

				}, domConstruct.create('button', {}, 'targetUnbindChild' + id));

			newWidget.addChild(window.xxx=new List({
					id : id + 'list',
					editable : true
				}))

		},


		/*
		* Adds property into current group, GOT IT?!;
		*
		* @lbl - label of new object
		* @newid - id of new object
		* @id - target object id
		* @editable - true if user can edit text
		*
		*/
		addPropertyIntoCurrentGroup : function (lbl, id, newid, editable, isTextArea) {
			if (!(registry.byId(id)))
				return;
			var self = this;
			/*if(titleId)
			console.log('#' + titleId + ' div')*/

			domConstruct.create('li', {
				'id' : "divForTextBoxProperty" + newid,
				'class' : 'cardDiv'

			}, id + 'list');
			//(titleId && query('#x' + titleId + ' div').length) ? query('#x' + titleId + ' div')[0] : id
			var className = isTextArea ? Textarea : TextBox;
			var newWidget = new className({
					'id' : newid,
					'value' : lbl,
					placeHolder: self.nls.na,
					'class' : 'textbox',
					'disabled' : !editable || !window.WGS.PRIVILEGE.CARD_EDIT,
					'onInput' : function (ev) {
						//alert('!!!');
						//if(registry.byId('SaveCardButton').get('style') == 'display:none')
						var fproc = newid.indexOf('-') + 1
							var cid = newid.substr(fproc, newid.indexOf('-', fproc + 1) - fproc);
						console.log('id:' + newid + ';cid:' + cid);
						self.addChangedCard(cid);
						//domStyle.set('SaveCardButton', 'display', 'inline');
						//registry.byId("SaveCardButton").disabled = false;
						//registry.byId('SaveCardButton').set('style', 'display:inline');
					}
				}, domConstruct.create(isTextArea ? 'textarea' : 'input', {
					
				}, "divForTextBoxProperty" + newid));
		},

		/* Add Changed Cards to Array */

		addChangedCard : function (cid) {
			if (!this.changedCards)
				this.changedCards = new Array();
				registry.byId('SaveCardButton').set('class', 'mblBlueButton');
			var ex = false;
			for (var i = 0; i < this.changedCards; i++) {
				if (this.changedCards[i] == cid)
					ex = true;
			}
			if (!ex)
				this.changedCards.push(cid);
			console.log(this.changedCards.join());
		},

		/*
		* Adds list (combobox) of domains to current group;
		*
		* @dataForDomain - array of strings
		* @cid - id of current card
		* @cpgid - group id
		* @cpid - id of new property (this list)
		* @cpvalue - current value of list
		*
		*/		

		addPropertyDomainIntoCurrentGroup : function (dataForDomain, cid, cpgid, cpid, cpvalue) {
			if (!(registry.byId('groupProp' + cid + cpgid + 'list')))
				return;
			var self = this;
			var li = domConstruct.create('li', {
					style : "padding: 5px 10px;"
				}, "propName" + cid + cpid, 'after');
			var mmry = new Memory({
					data : dataForDomain
				});
			var os = new ObjectStore({
					objectStore : mmry
				});

			console.log('value is', cpvalue)
			var s = new Select({
					store : os,
					id : "divForPropertyDomain" + cid + cpid,
					'class' : 'comboBoxCardDomain',
					disabled: !window.WGS.PRIVILEGE.CARD_EDIT,
					'value' : cpvalue ? ( "domainValue" + cid + "prop" + cpid + "domn" + cpvalue) : null,
					//id : "divForPropertyDomain" + cid + cpid,
					'onChange' : function () {
						console.log('fnc from select ' + this.get('value'));
						//registry.byId('SaveCardButton').set('style', 'display:inline');
						domStyle.set('SaveCardButton', 'display', 'inline');
						self.addChangedCard(cid);
					}
				}, domConstruct.create('div', {}, li));

			s.startup();
			if(cpvalue)
				s.getOptions('').disabled = true;
			//console.log(s.id);
			//s.on("change", fnc);
		},

		/*
		* Adds string that shows property name;
		*
		* @lbl - label
		* @id - target container id
		* @newid - id of new element
		*
		*/
		addPropertyIntoCurrentGroupName : function (lbl, id, newid) {
			if (!(registry.byId(id)))
				return;
			var newWidget = new ListItem({
					'id' : newid,
					'label' : lbl + ':',
					'class' : 'ListItemLabel',
					variableHeight : true
				});
			registry.byId(id + 'list').addChild(newWidget);
			//return newid
		},


		/*
		* Creates new Date Picker (Calendarick);
		*
		* @lbl - label
		* @id - target container id
		* @newid - id of new element
		*
		*/

		addDatePicker : function (lbl, id, newid) {
			var self = this
				if (!(registry.byId(id)))
					return;
				domConstruct.create('li', {
					'id' : "divForTextBoxProperty" + newid,
					'class' : 'cardDiv'
				}, id + 'list');

			var thisTextBox = new TextBox({
					'id' : newid,
					'value' : lbl,
					'style' : 'display:none',
					'onChange' : function (ev) {
						domStyle.set('SaveCardButton', 'display', 'inline');
					}
				}, domConstruct.create('input', {}, "divForTextBoxProperty" + newid));

			new Button({
				'label' : lbl ? lbl : 'Выбрать дату...',
				disabled: !window.WGS.PRIVILEGE.CARD_EDIT,
				'onClick' : function (ev) {
					var thisButton = this;
					self.dateDialog.show();
					self.dateSelectButton.onClick = function () {
						self.dateDialog.hide();
						thisTextBox.set('value', self.datePicker.get('value'))
						thisButton.set('label', self.datePicker.get('value'))
						var fproc = newid.indexOf('-') + 1
							var cid = newid.substr(fproc, newid.indexOf('-', fproc + 1) - fproc);
						self.addChangedCard(cid);
					}
				}
			}, domConstruct.create('button', {}, "divForTextBoxProperty" + newid));
		},

		/*
		* Adds name property of relation;
		*
		* @lbl - label
		* @id - target container id
		* @newid - id of new element
		* @idCard - current card id
		*
		*/
		addRelationCardName : function (lbl, id, newid, idCard) {
			if (!(registry.byId(id)))
				return;

			var newWidget = new ListItem({
					'id' : newid,
					'label' : lbl,
					'class' : 'ListItemLabel',
					'clickable' : true
				});

			connect.connect(newWidget, 'onClick', null,
				function () {
				var activeView = registry.byId('swap' + idCard).getParent().getShowingView();
				activeView.goTo(1, registry.byId('swap' + idCard).getParent().id)
			});
			registry.byId(id + 'list').addChild(newWidget);

		},

		/*
		* Creates new Swap View;
		*
		* @cardId - id of current card
		*
		*/

		createSwap : function (cardId) {
			if (registry.byId('swap' + cardId))
				return;
			var newWidget = new SwapView({
					'label' : 'Название'
				}, domConstruct.create('div', {}, "targetForCardSwaps"));
			newWidget.startup();
			newWidget.addChild(new ScrollableView({
					'id' : "swap" + cardId,
					'class' : 'scrollable-pane-100-height'
				}))
			/*var func = function (moveTo,dir,trans,cont,meth) {
			console.log('test');
			var cid = this.getShowingView().id.substr(4);
			console.log("current swap: ", cid);
			if(registry.byId('targetForCardCombobox'))
			registry.byId('targetForCardCombobox').set('value','ccbi'+cid);
			};
			connect.connect(newWidget, "onBeforeTransitionOut", null, func);*/

			//newWidget.onAfterTransitionIn = func;
			//newWidget.onAnimationEnd = func;
			console.log("Swap " + cardId + " exists");
			//registry.byId("targetForSwaps").addChild( newWidget );
			//console.log(obj.name);

		},

		/*
		* Creates new Accordion;
		*
		* @cardId - id of current card
		*
		*/
		createAccordeon : function (cardId) {
			if (registry.byId('acc' + cardId))
				return;
			if (!(registry.byId('swap' + cardId)))
				this.createSwap(cardId);
			var accId = "acc";
			var newAccWidget = new Accordion({
					'id' : 'acc' + cardId,
					'label' : 'test',
					//'class' : 'mblAccordionTitle',
					'singleOpen' : true
				}, domConstruct.create('div', {}, registry.byId("swap" + cardId).containerNode));
			newAccWidget.startup();
			//.addChild(newAccWidget);
			//.addChild(newAccWidget);
		},

		/* Saves changed cards */
		saveCard : function () {

			var self = this;
			console.log('save' + this.changedCards.length);
			if (this.changedCards.length > 0) {
				registry.byId('SaveCardButton').set('class', 'mblGreyButton');
				for (var i2 = 0; i2 < this.changedCards.length; i2++) {
					console.log('save' + this.changedCards.length);
					//registry.byId('SaveCardButton').set('style', 'display:none');
					//domStyle.set('SaveCardButton', 'display', 'none');
					//registry.byId('SaveCardButton').disabled = "disabled";
					//window.t = registry.byId('swap' + this.frstCardId);
					var cid = this.changedCards[i2]; //registry.byId('swap' + this.frstCardId).getParent().getShowingView().getChildren()[0].id.substr(4);
					var dfr = {};
					dfr.id = cid;
					/*var td=new Date();
					var tdate=td.getDate();
					var tmn=td.getMonth()+1;
					var th=td.getHours();
					var tmin=td.getMinutes();
					var tsec=td.getSeconds();
					dfr.createDate=td.getFullYear() + '-' + (tmn<10?'0'+tmn:tmn) + '-' + (tdate<10?'0'+tdate:tdate) + ' ' + (th<10?'0'+th:th) + ':' + (tmin<10?'0'+tmin:tmin) + ':' + (tsec<10?'0'+tsec:tsec);*/
					dfr.name = registry.byId("valueNameCard-" + cid + '-').get('value');
					dfr.shortName = registry.byId("valueShortName-" + cid + '-').get('value');
					dfr.typeName = registry.byId("valueTypeName-" + cid + '-').get('value');
					dfr.note = registry.byId("valueNote-" + cid + '-').get('value');
					if ("ids" in this.objProps[cid.toString()]) {
						var ids = this.objProps[cid.toString()].ids;
						for (var i = 0; i < ids.length; i++) {
							if (ids[i] >= 0) {
								dfr['property-' + ids[i]] = registry.byId("propValue-" + cid + '-' + ids[i]).get('value');
							} else {
								console.log(ids[i]);
								var iddmn = registry.byId("divForPropertyDomain" + cid + (-ids[i])).get('value');
								dfr['property-' + (-ids[i])] = iddmn.substr(iddmn.indexOf('domn') + 4);
								//dfr['grid-'+(-ids[i])] = registry.byId("divForPropertyDomain" + cid + (-ids[i])).name;
								//console.log(dfr['property-'+(-ids[i])]);
							}
						}

					}
					console.log(dfr);
					xhr.post("/objectadmin/card/SetCard", {
						//sync : true,
						handleAs : "json",
						data : dfr
					}).then(function () {
						console.log('Saved!!');
						domStyle.set('cardChangesSaved', 'display', 'block');
						setTimeout(function () {
							domStyle.set('cardChangesSaved', 'display', 'none');
						}, 3000);
						//self.getCardsOfSelectedFeature(self.currentSelected);
					});
					console.log("select not add");
					self.selCard.updateOption({
						'value' : 'ccbi' + cid,
						'label' : dfr.name ? dfr.name : 'Без наименования' + ' - ' + dfr.typeName
					});
					console.log("select add");
				}
				this.changedCards = new Array();
			}

		},

		/* Clears card containers for another ones */

		clearCard : function () {
			// очищаем карточки
			this.changedCards = new Array();
			var formWidgets = registry.findWidgets(dom.byId('targetForCardSwaps'));
			if (formWidgets.length > 0) {
				//console.log('l: ' + formWidgets.length);
				for (var i = 0; i < formWidgets.length; i++) {
					if (formWidgets[i]) {
						//console.log(formWidgets[i].toString() + ' exists');
						formWidgets[i].destroyRecursive(true);
					}
				}
				domConstruct.empty('targetForCardSwaps');
				if (registry.byId('cardPageInd'))
					registry.byId('cardPageInd').destroyRecursive()
			}
			// удаляем комбобокс
			formWidgets = registry.findWidgets(dom.byId('targetForCardCombobox'));
			if (formWidgets.length > 0) {
				//console.log('l: ' + formWidgets.length);
				for (var i = 0; i < formWidgets.length; i++) {
					if (formWidgets[i]) {
						//console.log(formWidgets[i].toString() + ' exists');
						formWidgets[i].destroyRecursive(true);
					}
				}
				domConstruct.empty('targetForCardCombobox');
			}
		},

		/*
		* Gets information about cards of selected object;
		*
		* @fid - feature ID
		*
		*/
		getCardsOfSelectedFeature : function (fid) {
			this.currentSelected = fid;
			var self = this;
			var cards = {};
			this.objProps = {};
			this.changedCards = new Array();
			// запрос на список карточек объекта
			xhr.post("/objectadmin/card/getcardsbyfeatureid", {
				//sync : true,
				handleAs : "json",
				data : {
					'featureId' : fid
				}
			}).then(function (data) {
				cards.cards = [];
				self.clearCard();

				var pi = new PageIndicator({
						'id' : "cardPageInd",
						refId : domConstruct.create('span', {
							id : 'targetForPageIndicator'
						}, 'targetForCardSwaps').id
					}, domConstruct.create('div', {}, "targetForCardCombobox", 'after'));
				pi.startup();
				pi.reset();
				var dataForMemory = [];
				for (var i = 0; i < data.length; i++) {

					cards.cards[i] = {};
					var CardObject = cards.cards[i];
					CardObject.cardId = data[i].cardId;
					if (i == 0)
						self.frstCardId = CardObject.cardId;
					dataForMemory[data.length - i - 1] = {
						id : 'ccbi' + data[i].cardId,
						value : 'ccbi' + data[i].cardId,
						label : data[i].cardName
					};
					CardObject.isCurrent = data[i].cardIsCurrent === '1';
					CardObject.relationName = data[i].cardRelationName;
					CardObject.cardLabel = data[i].cardLabel;
					self.getPropertiesOfNCard(CardObject);

					//while (!CardObject.note || !CardObject.props || !(CardObject.props[CardObject.props.length - 1].value)) {}
					// || !CardObject.rels || !(CardObject.rels[CardObject.rels.length-1].countTypeRelation)){}
				}
				//dataForMemory.reverse();

				/*var mmry = new Memory({
				data : []//dataForMemory
				});*/
				var os = new ObjectStore({
						objectStore : new Memory()
					});
				self.selCard = new Select({
						store : os,
						'class' : 'comboBoxCard'
					}, domConstruct.create('div', {
							id : 'targetForCmb1'
						}, "targetForCardCombobox"));
				self.selCard.startup();
				self.selCard.on("change", function () {
					var cid = this.get("value").substr(4);
					console.log("my value: ", cid);
					//registry.byId("swap"+cid).show();
					//registry.byId("cardPageInd").reset();
					var activeView = registry.byId("swap" + cid).getParent().getShowingView();
					activeView.goTo(1, registry.byId("swap" + cid).getParent().id);
				});
				self.selCard.set('value', 'ccbi' + cards.cards[0].cardId);
				console.log(cards);
				//self.showCardsInDocument(cards);
			}, function (err) {
				domConstruct.place("<p>error: <p>" + err.response.text + "</p></p>", "targetForCardSwaps");
			});
			//pageInd.reset();
			//return cards;
		},

		/*
		* Gets properties of <cc> card;
		*
		* @cc - card id
		*
		*/
		getPropertiesOfNCard : function (CardObject) {
			var self = this;
			self.addGroup("Основные характеристики", "card" + CardObject.cardId, CardObject.cardId, false);
			this.objProps[CardObject.cardId.toString()] = {};
			this.objProps[CardObject.cardId.toString()].ids = [];
			var ids = this.objProps[CardObject.cardId.toString()].ids;
			// запрос на основное описание карточки
			xhr.post("/objectadmin/card/getvaluebaseproperties", {
				//sync : true,
				handleAs : "json",
				data : {
					'objectId' : CardObject.cardId
				}
			}).then(function (data2) {
				//for (var i2=0; i2<data2.length;i2++){
				//CardObject.id = data2[0].id;
				CardObject.name = data2[0].name;
				CardObject.shortName = data2[0].shortName;
				CardObject.typeName = data2[0].typeName;
				CardObject.createDate = data2[0].createDate;
				CardObject.editUser = data2[0].edit_user;
				CardObject.note = data2[0].note;
				self.selCard.addOption({
					value : 'ccbi' + CardObject.cardId,
					label : (CardObject.name == '' ? 'Без наименования' : CardObject.name) + ' - ' + CardObject.typeName
				});
				/*this.cardsMemoryStore.set({
				id : 'ccbi' + CardObject.cardId,
				label : CardObject.name
				});
				this.cardSelectWidget.addOption({
				id : 'ccbi' + CardObject.cardId,
				label : CardObject.name
				});*/

				self.addPropertyIntoCurrentGroupName("Наименование", "card" + CardObject.cardId, "nameCard" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.name, "card" + CardObject.cardId, "valueNameCard-" + CardObject.cardId + '-', true);
				self.addPropertyIntoCurrentGroupName("Краткое наименование", "card" + CardObject.cardId, "shortName" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.shortName, "card" + CardObject.cardId, "valueShortName-" + CardObject.cardId + '-', true);
				self.addPropertyIntoCurrentGroupName("Атрибутивное описание", "card" + CardObject.cardId, "typeName" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.typeName, "card" + CardObject.cardId, "valueTypeName-" + CardObject.cardId + '-', false);
				self.addPropertyIntoCurrentGroupName("Дата последнего изменения", "card" + CardObject.cardId, "createDate" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.createDate, "card" + CardObject.cardId, "valueCreateDate-" + CardObject.cardId + '-', false);
				self.addPropertyIntoCurrentGroupName("Автор", "card" + CardObject.cardId, "editUser" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.editUser, "card" + CardObject.cardId, "valueEditUser-" + CardObject.cardId + '-', false);
				self.addPropertyIntoCurrentGroupName("Примечания", "card" + CardObject.cardId, "note" + CardObject.cardId);
				self.addPropertyIntoCurrentGroup(CardObject.note, "card" + CardObject.cardId, "valueNote-" + CardObject.cardId + '-', true);
				//console.log("feature is added" + CardObject.name + "; " + CardObject.props.length + "; " + CardObject.rels.length);
			});
			// запрос на характеристики
			xhr.post("/objectadmin/card/GetValueProperties", {
				//sync : true,
				handleAs : "json",
				data : {
					'objectId' : CardObject.cardId
				}
			}).then(function (data2) {
				var ic2 = 0;
				CardObject.props = [];
				console.log(data2);
				try{data2.sort(function (a, b) {
					return (a.groupName > b.groupName) ? -1 : 1
				})}catch(err) {}
				for (var i2 = 0; i2 < data2.length; i2++) {
					CardObject.props[i2] = {};
					var CardObjectPropetry = CardObject.props[i2];
					CardObjectPropetry.id = data2[i2].id;
					CardObjectPropetry.name = data2[i2].name;
					CardObjectPropetry.valueTypeId = data2[i2].vulueTypeId; //
					CardObjectPropetry.groupId = data2[i2].groupId;
					CardObjectPropetry.groupName = data2[i2].groupName;
					CardObjectPropetry.value = data2[i2].value;
					//console.log('There is one more group: ' + CardObjectPropetry.groupName);
					if (CardObjectPropetry.valueTypeId == '5')
						ids[i2] = -CardObjectPropetry.id;
					else
						ids[i2] = CardObjectPropetry.id;
					self.addGroup('Группа: ' + CardObjectPropetry.groupName, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, CardObject.cardId, true);
					self.addPropertyIntoCurrentGroupName(CardObjectPropetry.name, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propName" + CardObject.cardId + CardObjectPropetry.id);
					var queryForDomains = function (CardObject, CardObjectPropetry1) {
						xhr.post("/objectadmin/card/GetValueDomainList", {
							//sync : true,
							handleAs : "json",
							data : {
								'propId' : CardObjectPropetry1.id
							}
						}).then(function (data3) {
							//console.log('Server responsed domains');
							CardObjectPropetry1.domains = [];
							var dataForDomain = [ {id: "", label: "Выберите значение...", disabled: true} ];
							for (var i3 = 0; i3 < data3.length; i3++) {
								CardObjectPropetry1.domains[i3] = {};
								var cd = CardObjectPropetry1.domains[i3];
								cd.id = data3[i3].id;
								cd.name = data3[i3].name;
								//console.log(cd.id+cd.name);
								dataForDomain[ dataForDomain.length ] = {
									id : "domainValue" + CardObject.cardId + "prop" + CardObjectPropetry1.id + "domn" + cd.id,
									label : cd.name
								};
								//console.log('DOMAIN!!:' + dataForDomain[i3].label);
							}
							self.addPropertyDomainIntoCurrentGroup(dataForDomain, CardObject.cardId, CardObjectPropetry1.groupId, CardObjectPropetry1.id, CardObjectPropetry1.value);
						});
					}
					// запрос на список значений доменов

					console.log(CardObjectPropetry.valueTypeId)
					switch (Number(CardObjectPropetry.valueTypeId)) {
					case 5: {
							queryForDomains(CardObject, CardObjectPropetry);
							break;
						};
					case 2: {
							self.addDatePicker(CardObjectPropetry.value, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propValue-" + CardObject.cardId + '-' + CardObjectPropetry.id);
							break;
						};
					default: {
							self.addPropertyIntoCurrentGroup(CardObjectPropetry.value, 'groupProp' + CardObject.cardId + CardObjectPropetry.groupId, "propValue-" + CardObject.cardId + '-' + CardObjectPropetry.id, true, CardObjectPropetry.valueTypeId == 7);
						}
					}

				}
			});
			// запрос на список отношений
			xhr.post("/objectadmin/card/GetListRelByObject", {
				//sync : true,
				handleAs : "json",
				data : {
					'objectId' : CardObject.cardId
				}
			}).then(function (data2) {
				CardObject.rels = [];
				for (var i2 = 0; i2 < data2.length; i2++) {
					CardObject.rels[i2] = {};
					var CardObjectPropetry = CardObject.rels[i2];
					CardObjectPropetry.relationId = data2[i2].relationId;
					CardObjectPropetry.name = data2[i2].name;
					CardObjectPropetry.relationTypeId = data2[i2].relationTypeId;
					CardObjectPropetry.countTypeRelation = data2[i2].countTypeRelation;

					var addRelation = function (name, cardId, relationId) {

						self.addRelationGroup('Связь: ' + name, cardId + 'groupRelation' + relationId, cardId);
						//console.log("Relation was add " + name);
						// запрос на список наследников по отношениям
						xhr.post("/objectadmin/card/GetListChildObjectByRelation", {
							handleAs : "json",
							data : {
								'objectId' : cardId,
								'relationId' : relationId
							}
						}).then(function (data3) {
							CardObjectPropetry.cards = [];
							for (var i3 = 0; i3 < data3.length; i3++) {
								CardObjectPropetry.cards[i3] = {};
								var CardObjectRelations = CardObjectPropetry.cards[i3];
								CardObjectRelations.id = data3[i3].id;
								CardObjectRelations.name = (data3[i3].name ? data3[i3].name : 'Без наименования') + ' (' + data3[i3].typeName + ')';
								CardObjectRelations.relId = data3[i3].objRelationId;
								console.log(CardObjectRelations, data3[i3])
								self.addRelationCardName(CardObjectRelations.name, cardId + 'groupRelation' + relationId, cardId + relationId + 'groupRelation' + CardObjectRelations.relId, CardObjectRelations.id);
								//console.log("ChildGroup was add " + CardObjectRelations.name);
							}

						});
					}
					addRelation(CardObjectPropetry.name, CardObject.cardId, CardObjectPropetry.relationId);
				}
			});
		},

		/* Setup of selection events */
		initializeSelectionEvents : function () {
			var self = this;

			window.AppData.selectionCallbacks.card = {
				'fn' : function (selectionData, isSelectionCleared) {
					var sd;
					for (var i in selectionData) {
						sd = selectionData[i]
						console.log(sd);
						console.log(isSelectionCleared);
					}
					if(!sd.nTotalElements || isSelectionCleared){
						//registry.byId('cardSelectAnything').set('style', 'display : none');
						//if (domStyle.get("SaveCardButton", "display") != "none")
						if (self.changedCards.length > 0)
							//console.log("test2");
							self.showSaveCardDialog(-1);
						else {
							self.currentSelected = 0;
							self.clearCard();
							domStyle.set('cardSelectAnything', 'display', 'block');
						}
					} else {
					console.log("test25");
						self.afterActivate();
						domStyle.set('cardSelectAnything', 'display', 'none');
					}	
					
				},
				'active' : true
			}			
			

		},

		//triggered before showing view
		_beforeActivate : function () {
			if (this.params.newCardId) {
				this.getPropertiesOfNCard({
					'cardId' : this.params.newCardId
				});
				//this.selCard.addOption({'value':'ccbi'+this.params.newCardId,'label':'ccbi'+this.params.newCardId});
				registry.byId('cardPageInd').reset();
				if (registry.byId("swap" + this.frstCardId)) {
					console.log("swap" + this.frstCardId);
				}

			}

			this.datePicker.placeAt(this.dateDialog.containerNode)
			window.d = this.datePicker
		},
		_afterActivate : function () {
			var self = this;
			//Fusion.getWidgetById('Map').triggerEvent(Fusion.Event.MAP_BUSY_CHANGED);
			var mapName,
			sessionId;
			var allMaps = Fusion.getWidgetById('Map').getAllMaps();
			for (var j = 0; j < allMaps.length; j++) {
				if (allMaps[j].arch == 'MapGuide') {
					sessionId = allMaps[j].getSessionID()
						mapName = allMaps[j].getMapName();
					console.log(mapName);
				}
			}
			if (window.selText) {
				domStyle.set('cardSelectAnything', 'display', 'none');				
				xhr.post("/map/lib/getselectedfeatures", {
					handleAs : "json",
					data : {
						'mapname' : mapName,
						'selection' : window.selText,
						'session' : sessionId,
						'version' : '4'
					}
				}).then(function (response) {
					try {
						var featureId = response[0].features[0].entityId;
						console.log(featureId + ' ' + self.currentSelected);
						if (featureId != self.currentSelected) {
							//if (domStyle.get("SaveCardButton", "display") != "none")
							console.log("test1");
							if (self.changedCards.length > 0)
								self.showSaveCardDialog(featureId);
							else
								self.getCardsOfSelectedFeature(featureId); // все характеристики выделенного объекта
						}
					} catch (e) {}
				});
			}
			if (this.params.newCardId) {
				var activeView = registry.byId("swap" + this.frstCardId).getParent().getShowingView();
				if (activeView)
					activeView.goTo(1, registry.byId("swap" + this.params.newCardId).getParent().id);
				//this.param.newCardId = null;
			}
		}
	}

})
