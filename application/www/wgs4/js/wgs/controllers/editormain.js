define([
	'dojo/query','dojo/_base/declare', 'dojo/Deferred',
	'dojo/dom-attr','dojo/dom',
	'dojo/dom-style','dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff','dojox/mobile/Button',
	'dojo/on','dojo/hash','dojox/mobile/RoundRectCategory','dojox/mobile/EdgeToEdgeList','dojox/mobile/ListItem',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojox/mobile/CheckBox',"dojox/mobile/TextBox",'dojo/text!wgs4/views/editormain.html'
	],function(query, declare, Deferred, domAttr, dom, domStyle, domClass, domConstruct, has, Button, on, hash, Category, List, ListItem, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			_hasMenu: false,
			_hasBack: "leftView,editor",
			_viewTitle: "leftView,editormain",
			_backNls: "backToEm",
			
			_featureInfo:[],
			
			FEATURE_TYPES: {
				POINT: 1,
				LINE : 2,
				POLYGON: 3
			},
			
			_isLayerSelectionClear: function(sd /* map selection */){
				return false;
				var isSelectionCleared = true
				if ( sd )
				for(i = 0; i < sd.aLayers.length; i++) {
					console.log(sd.aLayers[i].name.indexOf ("_" + this.params.id + "_"))
					if( sd.aLayers[i].name.indexOf ("_" + this.params.id + "_") == -1 ) {
						isSelectionCleared = true; break;
					} else isSelectionCleared = false;
				}
				
				return isSelectionCleared;
				
						
			},
			
			init : function(){
				var self = this
				new window.AppData.ViewBaseClass( this );	
				this._setLineDigitizer()
				this._setPointDigitizer();
				this._setPolyDigitizer();
				
				this._setDeleteCallback();
				
				//TODO : selectionOff callback;
				window.AppData.selectionCallbacks.editormain = {
					'fn' : function (selectionData, isSelectionCleared) {
						var sd;
						for (var i in selectionData) {
							sd = selectionData[i]
						}
						console.log('!', isSelectionCleared, sd, self._isLayerSelectionClear( sd ))
						self.deleteBtn.set( 'disabled', isSelectionCleared || self._isLayerSelectionClear( sd ) )	
						//self._isLayerSelectionClear()
						
						if(sd && sd.fMinX == sd.fMaxX && sd.fMinY == sd.fMaxY) {
							
							xhr.post('/map/geometry/transformCoords', { handleAs: 'json', data: {
								x: sd.fMinX,
								y: sd.fMinY,
								reverse: 1
							}}).then ( function( coords ) {							
								registry.byId( "add-point-x" ).set( 'value', coords.x );
								registry.byId( "add-point-y" ).set( 'value', coords.y );
								this._setLineButtonActiveIfValidInput();
								this._setButtonActiveIfValidInput();						
							})
						}
					},
					'active' : true
				}	
				
			},
			
			/*
			 * disable all feature types UI control elements
			 */ 
			_disableAllFeatureTypeButtons : function(){
				this._displayFeatureTypeButtons( false, false, false );
			},
			
			/*
			 * Show/hide UI controls for different featuetypes
			 * 
			 * @point - if true show UI controls for POINTs
			 * @poly - if true show UI controls for POLYGONs
			 * @line - if true show UI controls for LINEs
			 */ 
			_displayFeatureTypeButtons: function(point, poly, line){
				var elems ,
					point2 = point ? '' : 'none',
					poly2 = poly ? '' : 'none',
					line2 = line ? '' : 'none';
				console.log('LPP', line !== undefined, point !== undefined, poly !== undefined)
				if( line !== undefined) {
					elems = query('.line-actions');
					for(i=0; i<elems.length; i++)
						domStyle.set(elems[i],'display', line2)
				}
				
				if( point !== undefined ){
					elems = query('.point-actions')				
					for(var i=0; i<elems.length; i++)
						domStyle.set(elems[i],'display', point2)
				}
				
				if( poly !== undefined ) {
					elems = query('.poly-actions')
					for(i=0; i<elems.length; i++)				
						domStyle.set(elems[i],'display', poly2)
				}
			},
			
			/*
			 * Enable UI elements for chosen layer's feature type
			 */		 
			_setupFeatureEditButtons: function(){
				var self = this, i ;
				this._disableAllFeatureTypeButtons();				
					
				this._getFeatureTypeByLayer().then ( function(result){
					switch (result.error){
						case 0 : {
							switch (result.gt) {
								case self.FEATURE_TYPES.POINT: {
									self._displayFeatureTypeButtons( true )									
									break;
								}
								case self.FEATURE_TYPES.LINE: {
									self._displayFeatureTypeButtons( undefined, undefined, true )
									break;
								}
								case self.FEATURE_TYPES.POLYGON: {
									self._displayFeatureTypeButtons( undefined, true, undefined )										
									break;
								}
							}
							break;
						}
						default: {
							self._displayFeatureTypeButtons( true, true, true );			
						}
					}
				})				
			},
			
			/*
			 * Return deferred object with feature type of layer with id= id || this.params.id
			 */ 
			_getFeatureTypeByLayer: function( id ) {
				var self = this
				var deferredResult = new Deferred;
											
				var i = setInterval( function() {
					
					if(window.AppData.MAP_LOADED) {
						clearInterval ( i );
						var mapParams = self._getMapParams() , byCoords = byCoords || false;
						var onSuccess = function(result){
							//TODO: Сделать по-нормальному))
							deferredResult.resolve (result)							
						}
						var onError = function(error){
							console.log('error')
						}
						var postData ={
							'mapname': mapParams.mapName, 
							'session': mapParams.sessionId, 
							'layerId': id || self.params.id				
						}
						
						xhr.post( "/map/geometry/GetFeatureTypeByLayer", {handleAs: 'json', data : postData } ).then( onSuccess, onError );
							
					}
				}, 400 );
				
				return deferredResult;
				
			},
			
			/*
			 * Insert feature into DB
			 * 
			 * @featureObject - array of points {x, y} OR Object {x, y}
			 * @featureType - Point (1)/Line (2)/Polygon (3)
			 * @byCoords - if true ignore featureObject & get coords from HTML input
			 * 
			 */ 
			_insertFeature: function(featureObject, featureType, byCoords) {
				var mapParams = this._getMapParams(), 
					self = this, 
					byCoords = byCoords || false;
					
				var onSuccess = function(result){
					//TODO: Сделать по-нормальному)) хотя мож и так ок...
					mapParams.map.reloadMap()
					mapParams.map.mapWidget.clearSelection();
							
				}
				
				var onError = function(error){
					console.log('error')
				}
					
				var coords = [];
				
				var postData ={
					'mapname': mapParams.mapName, 
					'session': mapParams.sessionId, 
					'layerId': self.params.id,
					'feature_type': featureType
				}
				
				if( self.FEATURE_TYPES.POINT != featureType) {	
					if( byCoords ){
						coords.push({
							x: registry.byId( "add-point-x" ).get( 'value' ),
							y: registry.byId( "add-point-y" ).get( 'value' )
						})
						coords.push({
							x: registry.byId( "add-point-x2" ).get( 'value' ),
							y: registry.byId( "add-point-y2" ).get( 'value' )
						})
						postData.reverse = 1;
					} else
					for(var i=0; i<featureObject.points.length; i++)
						coords.push({
							x: featureObject.points[i].X,
							y: featureObject.points[i].Y
						})					
					postData.coords = JSON.stringify(coords)
				} else { 
					
					if( byCoords ) {
						postData.x = registry.byId( "add-point-x" ).get( 'value' );
						postData.y = registry.byId( "add-point-y" ).get( 'value' );
						postData.reverse = 1;
					} else {
						postData.x = featureObject.X
						postData.y = featureObject.Y
					}
					
				}
				
				//self.FEATURE_TYPES.POLYGON
				xhr.post( "/map/geometry/insertpoint", { data : postData } ).then( onSuccess, onError );
				
			},
			
			_setDeleteCallback: function() {
				var  self = this;
				var callback = function() {
					var mapParams = self._getMapParams();
					var _deleteFeatureById = function(featureId) {
						var onSuccess = function(result){
							//TODO: Сделать по-нормальному))
							mapParams.map.reloadMap();
							mapParams.map.mapWidget.clearSelection();
							
							console.log('ну типа все ок')
						}
						var onError = function(error){
							console.log('error')
						}
						
						xhr.post("/map/geometry/removeFeature", {
							data:{
								'mapname': mapParams.mapName, 
								'session': mapParams.sessionId, 
								'layerId': self.params.id,
								'feature_id': featureId
							}}
						).then(onSuccess, onError);
					}
					
					if (window.selText) {
						console.log( mapParams.mapName )
						xhr.post("/map/lib/getselectedfeatures", {
							handleAs : "json",
							data : {
								'mapname' : mapParams.mapName,
								'selection' : window.selText,
								'session' : mapParams.sessionId,
								'version' : '4'
							}
						}).then(function (response) {
							try {
								var featureId = response[0].features[0].entityId;
								_deleteFeatureById(featureId);								
							} catch (e) {
								console.log('Error deleting feature on client');
							}
						});
					}				
				}
				
				this.deleteBtn.onClick = function(){
					 callback.call(self)
				}
			},
			
			/*
			 * return object with map params
			 */ 
			_getMapParams: function() {
				var sessionId, mapName, map;
				var allMaps = Fusion.getWidgetById('Map').getAllMaps()
					
				for(var j=0; j<allMaps.length; j++){
					if (allMaps[j].arch == 'MapGuide') {
						sessionId = allMaps[j].getSessionID()
						mapName = allMaps[j].getMapName()
						map = allMaps[j]
						//console.log(allMaps[j].getMapTitle())
					}	
				}
				return {
					'sessionId'		: sessionId,
					'mapName'		: mapName,
					'map'			: map
				}
				
			},
			
			_setButtonActiveIfValidInput: function(){
				var valueX = registry.byId( "add-point-x" ).get('value');
				var valueY = registry.byId( "add-point-y" ).get('value');
				var result = valueX == '' || valueY == '' || isNaN( Number( valueX ) ) || isNaN( Number( valueY ) );
				registry.byId( "coordPointBtn" ).set( 'disabled', result );
				return result;				
			},
			
			_setLineButtonActiveIfValidInput: function(){
				var num = 2,valueX,valueY, result
				for( var i = 1; i<= num; i++) {
					valueX = registry.byId( "add-point-x" + (i == 1 ? '' :i) ).get('value');
					valueY = registry.byId( "add-point-y" + (i == 1 ? '' :i) ).get('value');				
					result = valueX == '' || valueY == '' || isNaN( Number( valueX ) ) || isNaN( Number( valueY ) );
					if(result){
						registry.byId( "coordLineBtn" ).set( 'disabled', result );
						return result;
					}
				}
				
				registry.byId( "coordLineBtn" ).set( 'disabled', result );
				return result;				
			},
			
			_setPointDigitizer: function(){
				var self = this;
				
				on( registry.byId( "add-point-x" ), 'change,keyup', this._setButtonActiveIfValidInput);
				on( registry.byId( "add-point-y" ), 'change,keyup', this._setButtonActiveIfValidInput);
				
				on( registry.byId( "add-point-x" ), 'change,keyup', this._setLineButtonActiveIfValidInput);
				on( registry.byId( "add-point-y" ), 'change,keyup', this._setLineButtonActiveIfValidInput);				
				on( registry.byId( "add-point-x2" ), 'change,keyup', this._setLineButtonActiveIfValidInput);
				on( registry.byId( "add-point-y2" ), 'change,keyup', this._setLineButtonActiveIfValidInput);			
				
				
				this._setButtonActiveIfValidInput();
				this._setLineButtonActiveIfValidInput();
				
				var OnPointDigitized = function(point){
					console.log(point);
					self._insertFeature( point, self.FEATURE_TYPES.POINT );	
				};
				this.pointBtn.onClick = function(){
					 DigitizePoint(OnPointDigitized);
					 return false;
				}
				
				this.coordPointBtn.onClick = function(){
					 self._insertFeature(null, self.FEATURE_TYPES.POINT, true );
					 return false;
				}
				
				this.coordLineBtn.onClick = function(){
					 self._insertFeature(null, self.FEATURE_TYPES.LINE, true );
					 return false;
				}
				
				
				var getCoord = function( number ){
					var number = number || '';
					
					var showPosition = function (position) {						
						registry.byId( "add-point-x" + number ).set( 'value', position.coords.longitude);
						registry.byId( "add-point-y" + number ).set( 'value', position.coords.latitude);						
						self._setButtonActiveIfValidInput();
						self._setLineButtonActiveIfValidInput();
					}
					
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(showPosition);
					} else {
						alert( "Geolocation is not supported by this browser." );
					}					
				}
				
				this.getCoordPointBtn.onClick = function() { getCoord() }
				this.getCoordPointBtn2.onClick = function() { getCoord( 2 ) }
			},
			
			_setLineDigitizer: function(){
				var self = this
				var OnLineDigitized = function(line){
					self._insertFeature( line, self.FEATURE_TYPES.LINE );				
				};
				this.lineBtn.onClick = function(){
					 DigitizeLine(OnLineDigitized);
					 return false;
				}
			},
			
			
			_setPolyDigitizer: function(){
				var self = this
				var OnPolyDigitized = function(polygon){
					self._insertFeature( polygon, self.FEATURE_TYPES.POLYGON );					
				};
				this.polygonBtn.onClick = function(){
					 DigitizePolygon(OnPolyDigitized);
					 return false;
				}				
			},
			
			
			
			_beforeActivate: function(){
				//
				var self = this
				this._setupFeatureEditButtons();
				
				Fusion.getWidgetById('Map').getSelection( function(selectionData){
					var sd;
					for (var i in selectionData) {
						sd = selectionData[i]
					}					
					console.log(Fusion.getWidgetById('Map').hasSelection() , self._isLayerSelectionClear( sd ))
					self.deleteBtn.set( 'disabled', !Fusion.getWidgetById('Map').hasSelection() || self._isLayerSelectionClear( sd ) )						
				})
			}		
		}
	}
)
