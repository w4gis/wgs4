define([
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',"dojo/on","dojo/dom-style","dojo/query","dojo/_base/array","dojox/mobile/Pane",'dojo/text!wgs4/views/settings.html'], 
	function(domConstruct, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil){
  	return {
		_hasMenu : false,
		//_hasBack : 'map',
		_setupDisplayMode: function(){
			domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
		},
		
		//init is triggered when view is created
		init : function(){
			//
			this._setupDisplayMode()
			new window.AppData.ViewBaseClass( this );	
		},
		
		//triggered before showing view
		
		afterActivate: function(){
			//
		}			
	}
})
