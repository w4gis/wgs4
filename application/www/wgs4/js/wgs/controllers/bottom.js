define([
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-style',
	'dojo/dom-construct',
	'dojo/sniff',
	
	'dojo/on',
	'dojo/request/xhr',
	'dojo/_base/array',"dojox/mobile/TabBar",'dojo/text!wgs4/views/bottom.html'
	],function(query, domAttr, domStyle, domConstruct, has, on,xhr, arrayUtil){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			init : function(){
				//domConstruct.place("TaskPane", this.mainContainer.containerNode, "first");
				//domStyle.set("TaskPane", "display", "block")	
				if(!has('phone')){
					this.mapButton.destroyRecursive()
					//this.taskButton.set('target','taskpane-top')
					
				}
				
				
				if(window.AppData.prohibited['WgsCard'])
					this.cardButton.destroy();
				
			
			},
			
			
			
			beforeActivate: function(){
				//
				//this.taskButton.set('selected',true)
			},
			afterActivate: function(){
				
			}
			
		}
	}
)

