define([
	'dojo/query','dojo/_base/declare',
	'dojo/dom-attr','dojo/dom',
	'dojo/dom-style','dojo/dom-class',
	'dojo/dom-construct',
	'dojo/sniff', "dojox/mobile/Badge",
	'dojo/on','dojo/hash','dojox/mobile/EdgeToEdgeCategory','dojox/mobile/EdgeToEdgeList','dojox/mobile/ListItem','dojox/mobile/ContentPane',
	'dojo/request/xhr',
	'dojo/_base/array',
	'dijit/registry','dojo/text!wgs4/views/finfo.html'
	],function(query, declare, domAttr, dom, domStyle, domClass, domConstruct, has, Badge, on, hash, Category, List, ListItem, ContentPane, xhr, arrayUtil, registry){
		return {
			//init method called when view created
			//beforeactivate called before transition into the view...						
			
			//_hasMenu: "leftView,cs",			
			_hasMenu: false,
			_hasBack: "leftView,taskpane",
			
			_featureInfo:[],
			_selectedType : 'Polygon',
			_itemParent: "finfo-accordion",
			
			selectedCs: 'pz',
			
			/*
			 * 	Set styles (select tab for THIS view) of the bottom bar buttons
			 */ 
			_setSelectedTabBarButton: function(){
				if(registry.byId('bottom-tasks-button'))
					registry.byId('bottom-tasks-button').set('selected',true)				
			},
			
			init : function(){
				window.AppData.finfo = this
				this._cs = {
					'pm': this.nls.mercator,
					'pz': this.nls.pz,
					'pu': this.nls.pu,
					'wgs':this.nls.wgs
				}
				
				new window.AppData.ViewBaseClass( this );	
				
				this.registerOnSelectEventHandler()
				
				//initialize empty array of selected features
				this._clearPresentedFeatures();
				
				//initialize zeros in Lines/Polygones/Point list items
				this.updateBadges();
				
				
				
			},
			
			/*
			 * Can be called from outside of view.
			 * Sets selectedCs to params.cs
			 */ 
			updateCs: function(params){
				var params = params || this.params
				if(params && params.from == "cs" && params.cs){
					this.selectedCs = params.cs
					this.updateCsHeading()
				}				
			},
			
			beforeDeactivate: function(){
				this.deactivateSelectionCallback()
				
			},
			
			activateSelectionCallback: function(){
				window.AppData.selectionCallbacks.finfo.active = true
				Fusion.getWidgetById('Map').getSelection(
					function(selection){
						window.AppData.selectionCallbacks.finfo.fn(selection)
					}
				)
			},
			
			deactivateSelectionCallback: function(){
				window.AppData.selectionCallbacks.finfo.active = false
			},
			
			_beforeActivate: function(){
				console.log('before activate')
				
				this.activateSelectionCallback();
				this._setSelectedTabBarButton();
						
				this.updateCs();
				//this.menuButton.transitionOptions = { params : { cs : this.selectedCs, from: 'finfo' } }
				//this._hasMenuOptions = this.menuButton.transitionOptions
				this.welcome.transitionOptions = { params : { cs : this.selectedCs, from: 'finfo' } }
				
				
			},
			
			afterActivate: function(){
				//
			},
			
			/*
			 * 	Destroy all widgets inside _itemParent (i.e. all featue info widgets)
			 * 	
			 */ 
			_destroyExistingEntries: function(){
				this._selectedPane = undefined
				var existedEntries = registry.byId(this["_itemParent"]).getChildren()
				var lastopened = registry.byId(this["_itemParent"]).getSelectedPanes().length ? registry.byId(this["_itemParent"]).getSelectedPanes()[0] : null
				for(var i=0; i<existedEntries.length; i++){
					if(existedEntries[i]){
						existedEntries[i].destroyRecursive()							
					}
				}					
				domConstruct.empty(this["_itemParent"])
				this.lastOpenedPane = lastopened ? lastopened.label : null
			},
			
			/*
			 * 	Set this.presented to defaults
			 */ 
			_clearPresentedFeatures: function(){
				this.presented = {
					'Line': { layers:[], count:0 },
					'Polygon': { layers:[], count:0 },
					'Point': {layers:[], count:0}
				}
			},
			
			/*
			 * add layer of object o to this.presented if it is not already added
			 * render layer title
			 * 
			 * o - feature info object
			 */ 
			_addMissingLayers: function(o){
				var alreadyPresentedLayer = false, listId = 0, newFeatureItem;
				for(var j = 0; j < this.presented[o['type']].layers.length; j++)
					if( this.presented[o['type']].layers[j].layerName == o.layer) {
						alreadyPresentedLayer = true; listId = this.presented[o['type']].layers[j].id; 
						break;
					}
				if(!alreadyPresentedLayer){
					listId = 'gl-'+Math.random() //(G)eometry (L)ist + random number
					this.presented[o['type']].layers.push( { layerName : o.layer, 'id': listId, length: 0, area: 0, count: 0 } );
						
					newFeatureItem = new ContentPane({
						'id': listId,
						'class': 'variable-height',
						'label': o.layer + '<i class="fa redline-opener"></i>',
						selected : (this.lastOpenedPane == o.layer) || (!this.lastOpenedPane)
					},domConstruct.create('div',{id:listId}))
					
					registry.byId(this["_itemParent"]).addChild(newFeatureItem)			
						
				}
				return listId
			},
			
			/*
			 * 	Returns layout with feature info
			 * 
			 *  o - feature info object
			 */ 
			_getFeatureObjectInfoContent: function(o){
				var content = '<table class="resultTable">'
				var length = ( o.length ? ('<tr><td>'+this.nls.length+': </td><td><span class="number">' + o.length.toFixed(2) + "</span>"+ this.nls.m +"</td></tr>") : "" )
				var area = ( o.area ? ('<tr><td>'+this.nls.area+': </td><td><span class="number">' + o.area.toFixed(2) + "</span>"+ this.nls.m +"<sup>2</sup></td></tr>") : "" )
				content += length
				content += area
				content += "</table><p style='padding: 2px 5px'>"+this.nls.coords+":</p>"
				content += '<table class="coordsTable"><tr><th>№</th><th>X</th><th>Y</th></tr>', cs = this.selectedCs
						
				for(var k in o.coords[cs])
					if(o.coords[cs][k].x)
						content += ("<tr><td>"+(parseInt(k)+1)+"</td><td><span class='number'>"+ o.coords[cs][k].x.toString()+ "</span></td><td><span class='number'>"+o.coords[cs][k].y.toString()+"</span></td></tr>")
				content+="</table>"
				return content
			},
			
			/*
			 * 	NOT USED
			 * 	Reopen selected accordion pane
			 * 
			 */ 
			
						
			
			//updates CS title (gets info from this.selectedCs)
			updateCsHeading: function(){
				var locale = dojo.config.locale //Global object? //'ru-ru' for example
				var header = '' + this._cs[this.selectedCs] + ''
				this.welcome.set('label',header)
			},
			
			/*
			 * Add length & area of given object o 
			 * from layer with id layerId 
			 * to this.presented...
			 * 
			 * o - feature info object
			 * layerId - random id given to o's layer in _addMissingLayers
			 */ 
			_addFeatureInfoToLayerInfo: function(o, layerId){
				for(var j=0; j<this.presented[o['type']].layers.length; j++){
					if(this.presented[o['type']].layers[j].id == layerId){
						this.presented[o['type']].layers[j].length += o.length ? o.length : 0;
						this.presented[o['type']].layers[j].area += o.area ? o.area :0;
						this.presented[o['type']].layers[j].count ++;
						break;
					}
				}
			},
			
			/*
			 * 1 Clear all existing feature info entries
			 * 2 Fill in this.presented
			 * 3 Render all records of type this._selectedType
			 * 4 Render summary info for each layer of this._selectedType
			 * 
			 */ 
			renderFeatureInfo: function(){
				var parent = this._itemParent
				var ftype = this._selectedType
				var result = this._featureInfo
				if(parent && dom.byId(parent))
					this._destroyExistingEntries();
				//console.log('LAST is: ', last)
				this._clearPresentedFeatures();
				
				var self = this				
				for(var i in result){
					if(result[i].type){
						var o = result[i]
						this.presented[o['type']].count++				
							
						if(parent && dom.byId(parent) && ftype === o['type'])
							var layerId = this._addMissingLayers(result[i])
						
						this._addFeatureInfoToLayerInfo(o, layerId)
						
						if(parent && dom.byId(parent) && ftype === o['type']){
							var content = this._getFeatureObjectInfoContent(result[i])
							var newChild = new ContentPane({
								id: 'ge'+i,//(G)eometry (E)ntity + geom.entityId
								content: '<div id="'+ ('ge'+i+'exp') +'" class="expander"><i id="'+ ('ge'+i+'icon') +'" class="fa opener"></i> '+ result[i].label + 
									'</div><div class="listItemContent" id="'+('ge'+i+'coords')+'">'+ content +'</div>'
							})
							registry.byId(layerId).addChild(newChild);
							
							// wrapped with function to get current (and not last) newChild
							(function(nc){
								on(dom.byId('ge'+i+'exp'),'click',function(){
									domClass[ domClass.contains(nc.domNode,'opened') ? "remove" : "add" ](nc.domNode, "opened")
								})
							})(newChild)
							
							var layerCount = registry.byId(layerId).domNode.children.length
							if(registry.byId(layerId).domNode.children[ layerCount -2 ])
								registry.byId(layerId).domNode.children[ layerCount -2 ].children[0].classList.remove( 'last-child' )
							if(registry.byId(layerId).domNode.children[ layerCount -1 ])
							registry.byId(layerId).domNode.children[ layerCount -1 ].children[0].classList.add( 'last-child' )
							
							/*
							var allObjects = query('#'+ layerId +' .expander')
							for(var j = 0; j< allObjects.length; j++ ){
								domClass.remove(allObjects[j], 'last-child');
							}
							domClass.add(allObjects [allObjects.length-1], 'last-child');*/
						}					
					}
				}
				
					
				
				
				this.renderObjectTypesInfo();
				this.updateBadges();	
				
					
			},
			
			/*
			 * Add total area & length layout of selected features by layers
			 */ 
			renderObjectTypesInfo: function(){
				var ftype = this._selectedType
				if(parent && dom.byId(parent)){
					for(var j=0; j<this.presented[ftype].layers.length; j++){
						var layer = this.presented[ftype].layers[j]
						console.log('LC', String( layer.count )[ String( layer.count ).length - 1 ]);
						var lcs = String( layer.count ), lcsLength = lcs.length;
						var summary = new ContentPane({
							content: '<div class="finfo-layer-summary"><div><span class="number">'+layer.count+'</span> ' + 
									(
										(
											lcs[ lcsLength - 1 ] == 1 
										) && 
										(
											lcsLength == 1 || lcs[ lcsLength - 2 ] != 1  
										) ? this.nls.featuresSelected1 : (
											(
												lcs[ lcsLength - 1 ] == 2 ||
												lcs[ lcsLength - 1 ] == 3 ||
												lcs[ lcsLength - 1 ] == 4 
											) && (
												lcsLength == 1 || lcs[ lcsLength - 2 ] != 1  
											) ? this.nls.featuresSelected2 : this.nls.featuresSelectedMany 
										)
									) + 
								'</div>'+
								(layer.area ? (
									'<div>'+ 
										this.nls.totalArea +
										': <span class="number">'+
											layer.area.toFixed(2)+
										'</span>'+
										this.nls.m +
										'<sup>2</sup></div>') : 
									""
								) +
								( layer.length ? (
									'<div>'+ 
										this.nls.totalLength +
										': <span class="number">'+
											layer.length.toFixed(2)+
										'</span>'+
										this.nls.m+
									'</div></div>') : "</div>")
						})
						registry.byId(layer.id).addChild(summary,'first');
					}
				}
				
			},
			
			/*
			 * 	Set amount of selected Lines/Polygons/Points
			 */ 
			updateBadges: function(){
				for(var i in this.presented){
					var badge = this["is" + i]
					badge.set('rightText',this.presented[i].count)
				}			
			},
			
			/*
			 * 	register for fusion SELECTION_ON event by
			 * 	adding callback to window.AppData.selectionCallbacks
			 * 
			 */ 
			registerOnSelectEventHandler: function(){
				var self = this
				this.updateCsHeading();
				var onSuccess = function(result){
					self._featureInfo = result
					self.renderFeatureInfo()					
				}
				var onError = function(error){
					console.log(error)
				}
				window.AppData.selectionCallbacks.finfo = {
					fn:	function(sd){
							for(var i in sd){						
								var mapName = i
								var allMaps = Fusion.getWidgetById('Map').getAllMaps()
								for(var j=0; j<allMaps.length;j++){
									console.log(allMaps[j].getMapName(), mapName)
									if(allMaps[j].getMapName() == mapName){
										var sessionId = allMaps[j].getSessionID()
										if(mapName){									
											xhr.post("/map/geometry/GetSelectedFeatures", {
												handleAs: "json",
												data:{'mapname': mapName, 'session': sessionId, 'isXml':1, 'selection': window.selText, version: 4}
											}).then(onSuccess, onError);
										}
									}
								}								
							}
						},
					active : false
				}
			}
			
		}
	}
)
