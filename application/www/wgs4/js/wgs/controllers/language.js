define([
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',"dojo/on","dojo/dom-style","dojo/query","dojo/_base/array","dojox/mobile/Pane",'dojo/text!wgs4/views/language.html'], 
	function(domConstruct, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil){
  	return {
		_hasMenu : false,
		_hasBack : 'leftView,settings',
		_setupDisplayMode: function(){
			domClass.add(this.viewContainer.domNode, 'scrollable-pane-100-height');
		},
		
		langs: ['en-us','ru-ru','uk-ua'],
		//init is triggered when view is created
		init : function(){
			//
			this._setupDisplayMode()
			new window.AppData.ViewBaseClass( this );	
			var self = this
			for(var i =0; i< this.langs.length ; i++){
			    (function(i){
			        self[self.langs[i]].onClick = function(){
			            localStorage.setItem('wgs_locale', self.langs[i])
			            location.reload()
			        }
			    })(i)
			}
			
			/*this.systemDefault.onClick = function(){
			    localStorage.removeItem('wgs_locale')
			    location.reload()
			}*/
			
		},
		
		//triggered before showing view
		_beforeActivate: function(){
			//this.legendButton.set('selected',true)
			console.log('!');
			for(var i =0; i< this.langs.length ; i++){
			    var locale = localStorage.getItem('wgs_locale') ? localStorage.getItem('wgs_locale') : 'ru-ru';
			    if( locale && locale == this.langs[i])
			        this[this.langs[i]].set('checked', true)
			}
		},
		afterActivate: function(){
			//
		}			
	}
})
