define([
	'dojo',
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/sniff',
	'dijit/registry',
	'dojo/topic',
	'dojo/dom-attr',
	'dojo/on',
	'dojo/dom-style',
	'dojo/query',
	'dojo/_base/array',
	'dojox/mobile/Pane','dojox/mobile/Button',
	'dojo/request/xhr',
	'dojox/mobile/ContentPane',
	'dojox/mobile/ListItem',
	'dojox/gesture/tap', 
	'dojo/_base/event', 'dojo/text!wgs4/views/thematic.html'], 
	function(dojo, domConstruct, domClass, has, registry, topic, domAttr, on, domStyle,query, arrayUtil, pane, Button, xhr, ContentPane, ListItem, tap, event){
  	return {
		//init is triggered when view is created
		
		
		_themeList:[],
		_hasBack: "leftView,taskpane",
		_itemParent: "themes_list",
		//=================================================================
		init : function(){
		
							
			new window.AppData.ViewBaseClass( this );	
			this.postThemeRequest();
			
		},
		//=================================================================	
		beforeActivate: function(){
			window.AppData.BeforeActivateTitleChangeHandler.call(this)
		},
		//=================================================================
		afterActivate: function(){
			//
		},			
		//=================================================================
	
		//=================================================================	
		postThemeRequest: function(){
			var self = this
			var onSuccess = function(result){
					_themeList = result
					self.displayThemesList()
				}
			var onError = function(error){
					console.log('error')
				}
			xhr.post("/map/themes/list", {handleAs: "json",	data:{}}).then(onSuccess, onError);
		},		
		//=================================================================	
		
		//TODO: причесать код!!
		//TODO: Комментарии
		//TODO: Переход на легенду и в телефоне на карту при отображении тематики
		
		displayThemesList: function(){
			var self = this
			for(var i=0; i < _themeList.records.length; i++){
				(function(theme_id){
					var listItemParams = {
						'id': 'themeId_'+i,
						'label': _themeList.records[i].NAME,
						'rightText' : '<button id="editButton_'+i+'"></button>'+'<i class="fa fa-angle-right left-padding-icon"></i>',
					}
				
					newFeatureItem = new ListItem( listItemParams, domConstruct.create('div',{id:'themeId_'+i}) )
					registry.byId(self["_itemParent"]).addChild(newFeatureItem);
					
					new Button({
						label: '<i class="fa fa-pencil-square-o list-item-centered-icon double-em-icon"></i>', //TODO: Локализация
						onClick: function(e){
							e.stopPropagation(); 
							console.log('Edit button clicked');
						},
						'class' : 'without-border'
					}, document.getElementById("editButton_"+i));
					
					on(dojo.byId('themeId_'+i), 'click', function(){
						self.onClickDisplayTheme(theme_id)
					})
					
					
				})(_themeList.records[i].THEME_ID)
				
				
			}
			
			this.viewContainer.resize();
		},		
		//=================================================================	
		onClickDisplayTheme: function(theme_id){
				var sessionId, mapName, map;
				var allMaps = Fusion.getWidgetById('Map').getAllMaps()
				for(var j=0; j<allMaps.length;j++){
						if (allMaps[j].arch == 'MapGuide') {
							sessionId = allMaps[j].getSessionID()
							mapName = allMaps[j].getMapName()
							map = allMaps[j]
							//console.log(allMaps[j].getMapTitle())
						}	
				}

		
				var self = this
				var onSuccess = function(result){
					console.log('ok')
					//TODO: Сделать по-нормальному))
					map.reloadMap()
					console.log('ну типа все ок')
				}
				var onError = function(error){
					console.log('error')
				}
				xhr.post("/map/themes/displaytheme", {handleAs: "json",	data:{
					'THEME_ID': theme_id, 'mapname': mapName, 'session': sessionId, 
				}}).then(onSuccess, onError);

				
		}
	}
})
