// we use 'define' and not 'require' to workaround Dojo build system
// limitation that prevents from making of this file a layer if it
// using 'require'
define(["dojo/text!/wgs4/js/etc/config/app-config.json", "dojo/hash", "dojo/json", "dojo/request/xhr", "dojox/app/main", "dojo/sniff", "dojo/dom-class", "dojo/hash", 'dijit/registry', 'dojo/dom-style','wgs4/view-base'],
	function(appConf, hash, json, xhr, Application, has, domClass, hash, registry, domStyle, ViewBase){
		
		//AppData is used for application settings
		if(!window.AppData) 
			window.AppData = {
                //Версия WGS4
                version: {
                    major: 0, minor: 3, patch: 6, 
                    state: 
						location.toString().indexOf('debug') >0 ? (
							'(dev' + ( WGS.FLAGS.NO_DRIVERS > 0 ? ", next)" : ")" )
						) : ( WGS.FLAGS.NO_DRIVERS > 0 ? " next" : "")
						
						
                },
                
                //Счетчик, использующийся для перехода в debug версию по нажатию на номер версии в приложении
                _dCounter: 0,
				

				//list of prohibited user commands
				// received in map.js
				prohibited		: {},
				
				//Версия приложения строкой
				getVersion: function(){
                    return this.version.major + '.' + this.version.minor + '.' + this.version.patch + ' '+ this.version.state +''
				},
				MapGroupSwitcher: {},
				
				//переход в debug версию из production по нажатию на какую-нибудь кнопку/div/... два раза
				//Используется счетчик _dCounter
				maybeDebug: function(){
					window.AppData._dCounter += 1;
					if( window.AppData._dCounter > 1 && location.toString().indexOf('/debug') == -1 ) {
						location.assign('/mobile/debug');
					}
				},
				
				_hasOpenLayers: false,
				hasOpenLayersChangeHandler: function(){
					this._hasOpenLayers = !this._hasOpenLayers;
					//domAttr.set()
				},
				
				/* 
				 * Все функции, перечисленные в selectionCallbacks выполняются по очереди при выделении объекта на
				 * карте (в качестве параметра получают список выделенных объектов в формате Fusion 2.6). Флаг active 
				 * используется для активации/деактивации обработчика. Сама функция передается в параметре fn.
				 */
				selectionCallbacks : {
					'console': {
						'fn' : function(selectionData){
							console.log(selectionData);
							/*
								var str="";
								for(var o in selectionData){
									str+=o.toString()+"\n";
								}
								alert(str);
							*/
						}, 
						'active': ( location.toString().indexOf('debug') > -1 )
					}
				},
				
				WGS_BUSY : false //by default, map is not busy, becomes true when loading map
				
			}
		
		//start application from defaultView
		hash("") 
		try{
			
			//Start aplication only if SESSION IS NOT EXPIRED, else redirect to login page
			//(if you try to start app without session checking => then there be errors if session is invalid)
			//if ok - load map & register fusion events
				
			var onSuccess = function( state ){
				console.log(state)
				if( state.auth == 0 ){
					location.assign('/info');
					return;
				} else {
					
					for(var i in state)
						if(i != "auth")
							window.AppData.prohibited[ i ] = true;
							
					console.log( "WGS4 v" + window.AppData.getVersion() )
					//we are using different events for mouse- and touch devices
					window.IS_MOUSE = false
					window.IS_TOUCH = false
					appConf = json.parse( appConf );
					
					window.AppData.ViewBaseClass = ViewBase
					/*
					 * If device 'css width' is < 600 use phonemod.
					 * Else use tablet mode
					 * */
					var width = window.innerWidth || document.documentElement.clientWidth;
					if(width <= 600){
						has.add("phone", true);
						domClass.add('body','phone')
						for(var i in appConf.views)
							if(appConf.views[i].constraint == "left") appConf.views[i].constraint = "center"
					}else domClass.add('body','tablet')
					
					//view history management is different in ie9orLess and web browsers
					has.add("ie9orLess", has("ie") && (has("ie") <= 9));		
					
					//run Applications
					Application(appConf);
				}
			}
					
			var onError = function(){
				location.assign('/info');
				return;
			}
					
			xhr.get("/authorization/status", {
				handleAs: "json"
			}).then(onSuccess, onError);	
		    
		    
		}catch(e){
			console.log(e)
		}
		
		
		
	}
);
