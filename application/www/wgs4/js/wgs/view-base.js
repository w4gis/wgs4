define([
	"dojo/_base/declare", 
	'dojo/date/locale',
	'dijit/registry',
	'dojo/sniff',
	'dojo/dom-class',
	'dojo/dom-style',
	'dojox/mobile/ToolBarButton'
], function(declare, locale, registry, has, domClass, domStyle, Button){
	return declare(null, {
		_menuButtonId: "appMenu",
		_backButtonId: "appBack",
		_tabBarId : 'panes-bar',
		showMapButtonId: 'appShowMap',
		uniteTitleAndBack: true, //if true, hide title of left views on tablets (unite title and back button)
		
		/*
		* Substitute ${var} in template with data[var]
		* Substitute ${!var} with capitalized version of data[var]
		*/
		substitute: function(template, data) {
			var self = this
			return template.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g, function(match, key){
			if(data[key]!=undefined || (key.substr(0,1) == "!" && data[key.substr(1)]))
				return (key.substr(0,1)=="!" ? self.capitalize(data[key.substr(1)]):data[key])
			return "";	
			});
		},
		
		/*
		* make str[0] bigger :-)
		*/
		
		capitalize: function(str){
			return str.substr(0,1).toUpperCase() + str.substr(1,str.length-1)//.toLowerCase()
		},
		
		BeforeActivateTitleChangeHandler: function(){
			var baseView = this._baseView;
			var self = this			
			console.log('Initialising "' + this._viewTitle + '" view', baseView);
			
			//if there is a tabbar button for this view select it when view activated
			//else - deselect all buttons
			if(this._viewTitle && registry.byId(this._viewTitle + "Button")) {
				registry.byId(this._viewTitle + "Button").set('selected', true)						
			}
			else {
				if(registry.byId( baseView._tabBarId )) 
					for( var i = 0; i< registry.byId( baseView._tabBarId ).getChildren().length ; i++)
						registry.byId( baseView._tabBarId ).getChildren()[i].set ( 'selected', false )
			}
			
			/*
			 * Setup app MENU button
			 * Setup app Back/Exit button
			 * Set view title / hide view title on phone
			 * 
			 */ 
			if(has('phone')){
				
				var _setupShowMapButton = function() {
					var hideTo = 'map';
					var backTo = self._viewTitle;
					var backParams = self.params;
					var appName = 'wgs4';
					var availableBackTo = ['leftView,editormain', 'leftView,fdetails']
					
					console.log('will go back to ', backTo)
					if(registry.byId( baseView.showMapButtonId ) && self._viewTitle != hideTo){
						var showMapButton = registry.byId( baseView.showMapButtonId );
						if( availableBackTo.indexOf( self._viewTitle ) > -1) {
							showMapButton.set('label', self.nls.showMap)
							domStyle.set( showMapButton.domNode, 'display', '' )
							showMapButton.onClick = function(){
								window[ appName ].transitionToView( self.domNode, {
									target: hideTo,
									params: {
										backTo: backTo,
										backParams: backParams
									}
								})
							};
						} else {
							domStyle.set( showMapButton.domNode, 'display', 'none' )
						}
					}
					if(registry.byId( baseView.showMapButtonId ) && self._viewTitle == hideTo){
						var showMapButton = registry.byId( baseView.showMapButtonId );
						if(self.params.backTo) {
							domStyle.set( showMapButton.domNode, 'display', '' )
							
							showMapButton.set('label',  self.nls.hideMap)
							showMapButton.onClick = function(){
								window[ appName ].transitionToView( self.domNode, {
									target: self.params.backTo,
									params: self.params.backParams
								})
							};
						}else {
							domStyle.set( showMapButton.domNode, 'display', 'none' )
						}
					}
				}
				
				/*
				 *  view._hasMenu : can be 	
				 * 		TRUE - display standard menu;
				 * 		STRING - go to view named STRING;
				 * 		false/undefined - hide menu button.
				 * 
				 *  view._hasMenuOptions: can be an 
				 * 			OBJECT with TransitionOptions (a query params for URL string);
				 * 			undefined - empty query.
				 * 
				 */ 
				var _setupMenuButton = function() {
					var hasBack = ( self._hasBack || self._viewTitle != 'map' );
					
					domStyle.set( baseView._menuButtonId, 'display', ( self._hasMenu ? '' : 'none' ) )
					domStyle.set( baseView._menuButtonId, 'float', ( hasBack ? 'right' : 'left' ) )
					
					registry.byId( baseView._menuButtonId ).set( "target", 
						( self._hasMenu && self._hasMenu !== true) ? self._hasMenu : null
					)
					
					registry.byId( baseView._menuButtonId ).set( "transitionOptions" , 
						self._hasMenuOptions ? self._hasMenuOptions : {}
					)
				
					registry.byId( baseView._menuButtonId ).set("onClick", 
						(self._hasMenu && self._hasMenu !== true) ? 
							function(){ void (0) }	: (
								self._hasMenuCallback ? 
								self._hasMenuCallback 	:							
							function(){ 
								registry.byId('dlg1').show() 
							})
					)
					
					if(self._hasMenuClass)
						domClass.add (registry.byId( baseView._menuButtonId ).domNode, self._hasMenuClass)
					else
						domClass.remove (registry.byId( baseView._menuButtonId ).domNode, "mblBlueButton mblRedButton")
					registry.byId( baseView._menuButtonId ).set("label", 
						self._hasMenuTitle ? self._hasMenuTitle :
						self.nls.menu ? ( '<i class=\'fa fa-bars\'></i> ' + self.nls.menu ) : '<i class=\'fa fa-bars\'></i>' 
					);
				
				}
				
				var _setupBackButton = function() {
					var hasBack = ( self._hasBack || self._viewTitle != 'map' );
					registry.byId( baseView._backButtonId ).set("label", 
						hasBack ? (self._backNls ? self.nls[ self._backNls ] : self.nls.back) : self.nls.exit)
					//registry.byId( baseView._backButtonId ).transitionOptions = {target : self._hasBack ? self._hasBack : 'map'}
					registry.byId( baseView._backButtonId ).set("onClick", 
						hasBack ? function(){ 
							console.log(self, self._hasTransitionOptions)
							self.app.transitionToView(self.domNode, { 
								target: self._hasBack ? self._hasBack : 'map',
								params: self._hasTransitionOptions || {}
							});
						} : function(){ window.AppData.onExit() }
					);
					domStyle.set(registry.byId("appBack").domNode, 'display', 
						hasBack ? '' : 'none')
				}
				
				_setupMenuButton();
				_setupBackButton();
				_setupShowMapButton();
				registry.byId("appHeading").set("label", this._title ? this.nls[this._title] : this.nls.title);
				if(this.heading)
					domStyle.set(this.heading.domNode,'display','none')
				if(this.viewContainer){
					domStyle.set(this.viewContainer.domNode,'top','0')	
				}
							
			}else{
				domStyle.set(registry.byId("appBack").domNode, 'display', 'none')
				if( self._hasBack ) {
					if(self._baseView.uniteTitleAndBack ) {
						domClass.add( self.domNode, 'uniteTitleAndBack ')
						self.backButton.set('label', self.nls.title)
					}
					domClass.add(self.domNode, 'hasBack')
				}
			}		
			if(this.viewContainer) {
				domClass.add(this.viewContainer.domNode,'scrollable-pane-100-height')	
				if( this._hideTitle ){
					domStyle.set(this.viewContainer.domNode,'top','0')	
				}
			}
				
		},
		
		//beforeactivate called before transition into the view
		beforeActivate: function(){
			this.BeforeActivateTitleChangeHandler();			
			
			if(this._beforeActivate)
				this._beforeActivate();
		},
		afterActivate: function(){
			if(this._afterActivate)
				this._afterActivate();
		},
			
		constructor: function( parent , overwriteMode /* undefined || 0 - overwrite, 1 - do not overwrite */){
			if( !parent || parent._isInheritedFromBaseView ) return;
			
			var predefined = ['capitalize','substitute','BeforeActivateTitleChangeHandler','beforeActivate','afterActivate']
			parent._isInheritedFromBaseView = true;
			parent._baseView = this;
			for(var i = 0; i < predefined.length; i ++) {
				if(!parent[ predefined[i] ] || !overwriteMode || overwriteMode == 0) {
					parent[ predefined[i] ] = this [ predefined[i] ]
				}
			}	
		}
	});
});
