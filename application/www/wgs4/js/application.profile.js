var profile = {

    "action": "release",
    "releaseDir": "wgs-release",
    "stripConsole": "normal",
    "cssOptimize": "comments.keepLines",
    'selectorEngine': "acme",

    localeList: "en-us,ru-ru",

   
        
    staticHasFeatures: {
            "dojo-v1x-i18n-Api": 1,
            // The trace & log APIs are used for debugging the loader,
            // so we don’t need them in the build
            'dojo-trace-api': 0,
            'dojo-log-api': 0,
            // This causes normally private loader data to be exposed for debugging,
            // so we don’t need that either
            'dojo-publish-privates': 0,
            // We’re fully async, so get rid of the legacy loader
            'dojo-sync-loader': 0,
            // We aren’t loading tests in production
            'dojo-test-sniff': 0
        
    },
    
    layers: {
        "dojo/dojo": {
            includeLocales: ['en-us', 'ru-ru'],
            customBase: true,
            boot: true,
            include: [
                "dojo/request/xhr",
                "dijit/registry",
                
                "dojo/Deferred",
                "dojo/DeferredList",
                
                // Menu
                "dijit/MenuBar",
                "dijit/PopupMenuBarItem",
                "dijit/CheckedMenuItem",
                "dijit/MenuSeparator",
                "dijit/Menu",
                "dijit/MenuItem",
                "dijit/DropDownMenu",
                
                // Form Elemenets
                "dijit/form/Button",
                "dijit/form/ToggleButton",
                "dijit/Toolbar",
                "dijit/ToolbarSeparator",
                
                // Layout widgets
                "dijit/layout/BorderContainer", 
                "dijit/layout/AccordionContainer", 
                "dijit/layout/TabContainer",
                "dijit/layout/ContentPane",
                
                "dojox/layout/Dock",
                "dojox/layout/FloatingPane"
                
            ]

        },

        "wgs45/application": {
            includeLocales: ['en-us', 'ru-ru'],
            include: [
                "wgs45/controllers/wgs-app",
                "wgs45/controllers/app-menu",
                
                "wgs45/controllers/card",
                "wgs45/controllers/cardView",
                "wgs45/controllers/legend",
                "wgs45/controllers/map-controls",
                "wgs45/controllers/theme",
                "wgs45/controllers/map",
                
                "wgs45/controllers/cardTable/cardTable",
                "wgs45/controllers/cardTable/cardGrid",
                "wgs45/controllers/cardTable/cardPanes",
                "wgs45/controllers/cardTable/cardTableView",
                "wgs45/controllers/cardTable/cardTypeSelect",
                
            ]

        },
        
        'dgrid/dgrid': {
            includeLocales: ['en-us', 'ru-ru'],
            
            include: ["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", 
                "dgrid/Keyboard", "dgrid/test/data/perf"]
        }
    },

    basePath: "./",


    packages: [{
        name: "dojo",
        location: "./dojo"
    }, {
        name: "dijit",
        location: "./dijit"
    }, {
        name: "dojox",
        location: "./dojox"
    }, {
        name: "wgs4",
        location: "./wgs"
    }, {
        name: "wgs45",
        location: "./wgs45"
    }, {
        name: "dgrid",
        location: "./dgrid"
    }, {
        name: "dstore",
        location: "./dstore"
    }]
};