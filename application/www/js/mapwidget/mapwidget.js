var MT_ESTABLISH_CONNECTION = 80;
var MT_COORDS_POLYGON = 100;
var MT_COORDS_CIRCLE = 101;
var MT_GET_AREA = 81;
var MT_GET_SELECTION = 82;
var MT_SET_VIEW = 90;
var MT_GET_VIEW = 91;
var MT_ENABLE_OK= 70;
function log(arg){
	/*require(['dojo/dom-attr'],function(attr){
		attr.set('result1','innerHTML',arg)
	});*/
	console.log(arg)
}

function parseMessage(m,origin,callback){
	var callback = callback || false;
	var message = false;
	
 		message = Ext.util.JSON.decode(m);
		//console.log(message)
		if(message)
			if(message.type){
				switch(message.type){
					case MT_GET_AREA: if(callback){callback(message)} break;
					case MT_ENABLE_OK: if(Ext.getCmp('okSetArea'))Ext.getCmp('okSetArea').setDisabled(false); break;
					case MT_GET_SELECTION: log("Context: <br/>" + message.content.context+'<br/><br/>Id: '+message.content.id); break;
					default: log(message.content); break; 		
				} 	
			}
			else log('undefined operation');
		else log('cant parse message')
	
}
