Ext.namespace('Ext.ux');

Ext.ux.SwfUploadPanel = function(config){

    this.storeDocGrid = config.storeDocGrid;
    this.parent = config.parent; 
    // ��������� �� ��������� 
    //	if (!config.title) config.title = 'File Upload';
	if (!config.single_select) config.single_select = false;
	if (!config.file_types) config.file_types = "*.*";
	if (!config.file_types_description) config.file_types_description = "All Files";
	if (!config.file_size_limit) config.file_size_limit = "102400"; // 100MB
	if (!config.file_upload_limit) config.file_upload_limit = "0";
	if (!config.file_post_name) config.file_post_name = "Filedata"; // This is the "name" of the file item that the server-side script will receive. Setting this doesn't work in the Linux Flash Player
	if (!config.flash_url) config.flash_url = "swfupload_f9.swf";
	if (!config.debug) config.debug = false;
		
	this.rec = Ext.data.Record.create ([
    	{name: 'name'},
    	{name: 'size'},
    	{name: 'id'},
    	{name: 'type'},
    	{name: 'creationdate', type: 'date', dateFormat: 'm/d/Y'},
    	{name: 'status'}
	]);
		
	var store = new Ext.data.Store({
	   reader: new Ext.data.JsonReader({ id: 'id' }, this.rec)
	});

	this.suo = new SWFUpload({
    	upload_url: config.upload_url,
    	post_params: config.post_params,
    	file_post_name: config.file_post_name,	
    	file_size_limit : config.file_size_limit,
    	file_types : config.file_types,
    	file_types_description : config.file_types_description,
    	file_upload_limit : config.file_upload_limit,
    	flash_url : config.flash_url,	

        // Event Handler Settings
		file_queued_handler : this.fileQueue.createDelegate(this),
		file_queue_error_handler : this.fileQueueError.createDelegate(this),
		file_dialog_complete_handler : this.fileDialogComplete.createDelegate(this),
		
		upload_error_handler : this.uploadError.createDelegate(this), 
		upload_progress_handler : this.uploadProgress.createDelegate(this),
		upload_complete_handler : this.uploadComplete.createDelegate(this),

		upload_success_handler : this.fileComplete.createDelegate(this),
		upload_error_handler : this.fileCancelled.createDelegate(this),

		swfupload_loaded_handler : this.swfUploadLoaded.createDelegate(this),

		debug: config.debug

	});

	/*this.progress_bar = new Ext.ProgressBar({
	    text:'Progress Bar'
    });*/ 
    
    this.AddButton = new Ext.Button({
        text : '��������',
        iconCls: 'icon-add',
        handler : this.addFilesToDialog,
        config: config,
        scope : this
    });
     
    this.RemoveButton = new Ext.Button({
        text : '�������',
        iconCls: 'icon-delete',
        handler : this.removeFileFromDialog,
        scope : this,
        disabled: true
    });  
    
    this.ClearButton = new Ext.Button({
        text : '��������',
        iconCls: 'icon-clear',
        handler : this.clearDialog,
        scope : this,
        disabled: true
    });  
     
    this.UploadButton = new Ext.Button({
        text : '���������',
        iconCls: 'icon-upload',
        handler : this.uploadFiles,
        scope : this,
        disabled: true,
        hidden: false
    }); 
     
    this.CancelButton = new Ext.Button({
        text : '��������',
        //iconCls: 'SwfUploadPanel_iconCancel',
        handler : this.cancelUploadFiles,
        scope : this,
        hidden: true
    });
    
    this.CloseButton = new Ext.Button({
        text : '�������',
        iconCls: 'icon-close',
        handler : function () {config.parent.close()},
        scope : this,
        disabled: false
    });
    
    var cm = new Ext.grid.ColumnModel([
        {id:'status', header: "������", width: 25, dataIndex: 'status', renderer: this.pictureStatus },
        {id:'name', header: "��� �����", dataIndex: 'name'},
		{id:'size', header: "������", width: 40, dataIndex: 'size', renderer: this.formatBytes },
		{id:'status', header: "���������", width: 70, dataIndex: 'status', renderer: this.formatStatus }
	]);
																		 
	config = config || {};
	config = Ext.apply(config || {}, {
	    store: store,
		cm: cm,
        viewConfig: { forceFit: true },
		autoExpandColumn: 'name',
		enableColumnMove: false,
		sm: new Ext.grid.RowSelectionModel({singleSelect: config.single_select}),
		tbar: [ this.AddButton, this.RemoveButton, this.ClearButton, '-', this.UploadButton,  /*this.CancelButton,*/ '->', this.CloseButton ],
		//bbar: [ this.progress_bar ],
		listeners: {
            'rowmousedown': function ()
            {
                this.RemoveButton.enable(); 
            }
        }
    });

	cm.defaultSortable = false;
    
	Ext.ux.SwfUploadPanel.superclass.constructor.apply(this, arguments);

	/*try {
	   this.progress_bar.setWidth(this.bbar.getWidth() - 5);
	   Ext.fly(this.progress_bar.el.dom.firstChild.firstChild).applyStyles("height: 16px");
	} catch (e) {}
	
	this.on('resize', function() { 
	   this.progress_bar.setWidth(this.bbar.getWidth() - 5);	
	   Ext.fly(this.progress_bar.el.dom.firstChild.firstChild).applyStyles("height: 16px");
	});*/

};

Ext.extend(Ext.ux.SwfUploadPanel, Ext.grid.GridPanel, {
        
    addFilesToDialog: function (config)
    {
        if (config.single_file_select)
            this.suo.selectFile();
        else
            this.suo.selectFiles();
    },
       
    removeFileFromDialog: function ()
    {
        var selRecords = this.getSelections();
        for (var i=0; i < selRecords.length; i++) {
            if (selRecords[i].data.status != 1) {
                this.suo.cancelUpload(selRecords[i].id);
                this.store.remove(selRecords[i]);
            }
        }
        this.RemoveButton.disable();
        if (this.suo.getStats().files_queued == 0) {
            this.ClearButton.disable();
            this.UploadButton.disable();    
        }
    },
       
    clearDialog: function ()
    {
        this.getSelectionModel().selectAll();
        var selRecords = this.getSelections();
        for (var i=0; i < selRecords.length; i++) {
            if (selRecords[i].data.status != 1) {
                this.suo.cancelUpload(selRecords[i].id);
                this.store.remove(selRecords[i]);
            }
        }
        this.RemoveButton.disable();
        if (this.suo.getStats().files_queued == 0) {
            this.ClearButton.disable();
            this.UploadButton.disable();  
        }
    },
   
    uploadFiles: function ()
    {
        this.suo.startUpload();
        //this.UploadButton.setVisible(false);
        this.UploadButton.disable();
        //this.CancelButton.setVisible(true);
        this.CloseButton.disable();
        this.AddButton.disable();
        this.getSelectionModel().clearSelections();
        this.RemoveButton.disable();
        this.ClearButton.disable();
    },
   
    cancelUploadFiles: function ()
    {
        this.suo.stopUpload();
        this.getStore().each(function() {
            if (this.data.status == 1) {
                this.set('status', 0);
                this.commit();
            }
        });
        
        //this.CancelButton.setVisible(false);
        //this.UploadButton.setVisible(true);
        this.UploadButton.enable();
        this.CloseButton.enable();
        this.AddButton.enable();
        this.ClearButton.enable();
        this.storeDocGrid.reload();
        this.parent.show();
        /*this.progress_bar.reset();
        this.progress_bar.updateText('������ ��������� ��������');*/
    },
    
    formatStatus: function(status) {
	    switch(status) {
    	    case 0: return("��������� � �������");
    	    case 1: return("��������...");
    	    case 2: return("���� ������� ��������");
    	    case 3: return("������ ��������");
       	    case 4: return("�������� �������� �������������");
	    }
    },   
    
    pictureStatus: function(status) {
        
        this.state_tpl = new Ext.Template(
            "<div class='ext-ux-uploaddialogswf-state ext-ux-uploaddialogswf-state-{state}'>&#160;</div>"
        ).compile();
        
        switch(status) {
            case 0: return this.state_tpl.apply({state: status}); 
            case 1: return this.state_tpl.apply({state: status});
            case 2: return this.state_tpl.apply({state: status});
            case 3: return this.state_tpl.apply({state: status});
            case 4: return this.state_tpl.apply({state: status});
        }
    },
    
	/**
	 * Formats raw bytes into kB/mB/GB/TB
	 * @param {Integer} bytes
	 * @return {String}
	 */
    formatBytes: function(bytes) {
        if(isNaN(bytes)) {	return (''); }
        var unit, val;
        
        if(bytes < 999)	{
        	unit = 'B';
        val = (!bytes && this.progressRequestCount >= 1) ? '~' : bytes;
        }	else if(bytes < 999999)	{
        	unit = 'kB';
        	val = Math.round(bytes/1000);
        }	else if(bytes < 999999999)	{
        	unit = 'MB';
        	val = Math.round(bytes/100000) / 10;
        }	else if(bytes < 999999999999)	{
        	unit = 'GB';
        	val = Math.round(bytes/100000000) / 10;
        }	else	{
        	unit = 'TB';
        	val = Math.round(bytes/100000000000) / 10;
        }

        return (val + ' ' + unit);
    },

	/**
	 * Show notice when error occurs
	 * @param {Object, Integer, Integer}
	 * @return {}
	 */
    uploadError: function(file, error, code) {
        switch (error) {
		    case -200: 	Ext.MessageBox.alert('Error','File not found 404.');
			    break;
			case -230: 	Ext.MessageBox.alert('Error','Security Error. Not allowed to post to different url.');
			    break;
		}
	},

	/**
	 * Add file to store / grid
	 * @param {file}
	 * @return {}
	 */
	fileQueue: function(file) {
		file.status = 0;
		r = new this.rec(file);
		r.id = file.id;
		this.store.add(r);
		this.fireEvent('fileQueued', this, file);			
	},

	/**
	 * Error when file queue error occurs
	 */
	fileQueueError: function(a, code, queue_remaining) {
		switch (code) {
			case -100:
            Ext.WgsMsg.show({
                    title: '��������������',
                    msg: '���������� ��������� ���������� �� ������ ��������� ' + queue_remaining + '. ',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
            });
    		break;
		}
	},

	fileComplete: function(file) {
	   if (this.suo.getStats().files_queued > 0) {
	       this.uploadFiles();
	   } else {
           this.CancelButton.setVisible(false);
           this.UploadButton.setVisible(true);
           this.UploadButton.disable();
           this.CloseButton.enable();
           this.AddButton.enable();
           this.ClearButton.enable();
	       this.fireEvent('queueUploadComplete', this);
	   }
    },
    
    fileDialogComplete: function(file_count) {
        if (this.suo.getStats().files_queued > 0) {
            this.ClearButton.enable(); 
            this.UploadButton.enable();
        }
	},

	uploadProgress: function(file, current_size, total_size) {
	   this.store.getById(file.id).set('status', 1);		
	   this.store.getById(file.id).commit();
	   //this.progress_bar.updateProgress(current_size/total_size, '����������� ����: ' + file.name + ' (' + this.formatBytes(current_size) + ' �� ' + this.formatBytes(total_size) + ')');
	},

	uploadComplete: function(file, result) {
	    if (this.cancelled) {
		    this.cancelled = false;
		} else {
		    var o = Ext.decode(result);	

			this.store.getById(file.id).set('status', 2);
			this.store.getById(file.id).commit();
			/*this.progress_bar.reset();
			this.progress_bar.updateText('������ ��������� ��������');*/
			
			if (this.remove_completed) {
				this.store.remove(this.store.getById(file.id));
			}
			
			this.fireEvent('fileUploadComplete', this, file, o);
		}
    },
	
	addPostParam: function( name, value ) {
	    this.suo.settings.post_params[name] = value;
		this.suo.setPostParams( this.suo.settings.post_params );
	},
		
	fileCancelled: function(file, code, b, c, d) {
		if (code == -280) return;
		
		this.store.getById(file.id).set('status', 4);
		this.store.getById(file.id).commit();
		/*this.progress_bar.reset();
		this.progress_bar.updateText('������ ��������� ��������');*/
		this.cancelled = true;
	},
		
	swfUploadLoaded: function() {
	    this.fireEvent('swfUploadLoaded', this);
	}
		
});