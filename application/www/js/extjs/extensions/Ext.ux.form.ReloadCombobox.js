Ext.ns('Ext.ux.form');

Ext.ux.form.ReloadCombobox = Ext.extend(Ext.form.ComboBox, {
	initComponent:function(){
		//from twintrigger
		this.triggerConfig = {
			tag:'span', cls:'x-form-twin-triggers', cn:[
				{tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class},
				{tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger2Class}
			]
		};

		Ext.ux.form.ReloadCombobox.superclass.initComponent.call(this);
	},

	getTrigger:Ext.form.TwinTriggerField.prototype.getTrigger,

	initTrigger:Ext.form.TwinTriggerField.prototype.initTrigger,

	trigger2Class:'x-form-reload-trigger',
	trigger1Class:'x-form-arrow-trigger',

	onTrigger1Click:function(){
		this.onTriggerClick();
	},

	onTrigger2Click:function(){
		this.reloadData();
	},
	
	reloadData: function()
	{
	}
});