﻿Ext.namespace('Projectadmin');

/* Application */

Projectadmin = function()
{
    var conn = new Ext.data.WgsConnection();
    
    var projectObjectManagementPanel = new ProjectObjectManagementPanel();
    var projectRoleManagementPanel = new ProjectRoleManagementPanel();
    var projectUserManagementPanel = new ProjectUserManagementPanel();
        
    return {
        init: function()
        {
            var modules = [];

            if (WGS.PRIVILEGE.PROJECTOBJECT_ADMIN) {
                modules[modules.length] = projectObjectManagementPanel;
            }
            if (WGS.PRIVILEGE.PROJECTUSER_ADMIN) {
                modules[modules.length] = projectUserManagementPanel;
            }
            if (WGS.PRIVILEGE.PROJECTROLE_ADMIN) {
                modules[modules.length] = projectRoleManagementPanel;
            }
            
            if (modules.length) {
                var tabPanel = new Ext.TabPanel({
                    deferredRender: false,
                    activeTab: 0,
                    style: 'padding-top: 49px',
                    listeners:
                                {
                                    tabchange: function(tabPanel, tab)
                                    {
                                        switch(tab.title)
                                        {
                                        case 'Роли':
                                          Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleStore.reload();
                                          break;    
                                        case 'Пользователи':
                                          Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();
                                          break;
                                        default:
                                          break;
                                        }
                                    }
                                },
                    items: modules
                });
                
                new Ext.Viewport({
                    layout: 'fit',
                    items: tabPanel
                    }).doLayout();
            }
        },
        conn: conn,
        projectObjectManagementPanel: projectObjectManagementPanel,
        projectRoleManagementPanel: projectRoleManagementPanel,
        projectUserManagementPanel: projectUserManagementPanel,
        getUnsavedGrid: function()
        {   
            return (this.projectObjectManagementPanel.permissionPanel.objectPermissionsGrid &&
                    this.projectObjectManagementPanel.permissionPanel.objectPermissionsGrid.changed ?
                    this.projectObjectManagementPanel.permissionPanel.objectPermissionsGrid:
                   (this.projectObjectManagementPanel.permissionPanel.rolePermissionsGrid &&
                    this.projectObjectManagementPanel.permissionPanel.rolePermissionsGrid.changed ?
                    this.projectObjectManagementPanel.permissionPanel.rolePermissionsGrid: false));    
        }        
    }
}();