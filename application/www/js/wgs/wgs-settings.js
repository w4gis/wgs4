Ext.ns('WGS');

/**
 * WGS constants
 */
WGS.constants = {
	/**
	 * WGS.constants.Core
	 */
	Core: {
		DEFAULT_LANG		:	'ru',
		DEBUG_MODE			:	false,
		PROJECT_NAME		:	'TradeFort'
	},

	Favicon: {
		IMG					:	'tf/resources/img/tf.png'
	},

	/**
	 * WGS.constants.Navigator
	 */
	Menu: {
		ITEMS_URL			:	'menu/get'
	},

	Session: {
		ID					:	'PHPSESSID'
	},
	
	/**
	 * WGS.constants.Login
	 */
	Login: {
		LOGO_URL			:	'tf/resources/img/tf2.png',
		REGISTER_URL		:	'auth/login',
		LOGOUT_URL			:	'auth/logout'
	},

	/**
	 * WGS.constants.Events
	 */
	Events: {
		CHECK_INTERVAL		:	60000,
		CHECK_URL			:	'auth/notouch'
	},

	Table: {
		LIST_URL			:	'data/list',
		PROPERTY_URL		:	'data/list',
		CREATE_URL			:	'data/create',
		UPDATE_URL			:	'data/update',
		REMOVE_URL			:	'data/remove',
		DELETE_URL			:	'data/delete',
		RESTORE_URL			:	'data/restore',
		LOCK_URL			:	'data/lock',
		UNLOCK_URL			:	'data/unlock'
	},

	Date: {
		GridTraceFormat		:	'Y-m-d H:i:s.u',
		GridFormat			:	'Y-m-d H:i:s',
		RecordManagerFormat	:	'Y-m-d H:i:s',
		TableFormat			:	'U'
	},
	
	Permission: {
		SAVE_URL			:	'permission/save'
	},
	
	SystemUser: {
		CHANGE_PASSWORD_URL	:	'data/changepwd'
	}
};