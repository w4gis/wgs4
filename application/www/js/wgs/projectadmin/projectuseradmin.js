﻿/* Форма основных характеристик роли */
ProjectUserForm = function()
{
	var self = this;
	Ext.namespace('Ext.projectUserLock');
    Ext.projectUserLock.states = [
        ['1','Заблокирован'],
        ['0','Разблокирован']
        ];

    this.projectUserLockStates = new Ext.data.SimpleStore({
        fields: ['id', 'state'],
        data: Ext.projectUserLock.states
    });

    this.projectUserLockStates.loadData(Ext.projectUserLock.states);

    this.userLockStatesCombobox = new Ext.form.ComboBox({
        name:'userLocked',
        store: this.projectUserLockStates,
        displayField:'state',
        valueField: 'id',
        fieldLabel: 'Состояние',
        typeAhead: true,
        mode: 'local',
        anchor:'100%',
        triggerAction: 'all',
        editable:false,
        selectOnFocus:true
    });

    this.oldPasswordField = new Ext.form.Field(
            {
	        xtype:'field',
	        fieldLabel: 'Старый пароль',
	        name: 'oldPassword',
	        anchor:'100%',
	        inputType: 'password',
	        listeners:
	        {
	        	change: function()
	        	{
	        		self.passwordField.enable();
	        		self.confirmPasswordField.enable();
	        		self.passwordField.focus();
	        	}
	        }
	        });

    this.passwordField = new Ext.form.Field(
            {
	        xtype:'field',
	        fieldLabel: 'Пароль',
	        name: 'password',
	        anchor:'100%',
	        inputType: 'password'
		    });


    this.confirmPasswordField = new Ext.form.Field(
			{
	        xtype:'field',
	        fieldLabel: 'Подтверждение',
	        name: 'confirmPassword',
	        anchor:'100%',
	        inputType: 'password'
		    });

	ProjectUserForm.superclass.constructor.call(this,{
		id: 'sysUserForm',
        method: 'POST',
        title: 'Общие свойства',
        region: 'north',
        height:228,
        collapsible: false,
        frame:true,
        deferredRender: false,
        buttonAlign: 'right',
        bodyStyle:'padding:3px 3px 0',
        defaults: {width: 230},
        items: [
                {
                xtype:'hidden',
                name: 'userId',
                allowBlank:true,
                anchor:'100%'
                },
                {
                xtype:'textfield',
                fieldLabel: 'Название',
                name: 'userName',
                msgTarget: 'under',
                msgFx: 'normal',
                invalidText : 'Недопустимое значение',
                allowBlank:false,
                blankText: 'Данное поле не может быть пустым',
                regex: /^([\wА-Яа-я_][ ]?)+$/,
                maskRe: /[A-Za-zА-Яа-я_ ]+/,
                maxLengthText: "Число символов не должно превышать 255",
		        anchor:'100%'
                },

                {
                xtype:'textarea',
                fieldLabel: 'Описание',
                name: 'userDescription',
                anchor:'100%',
                autoHeight: true
                },
                {
                xtype:'hidden',
                name: 'userLocked',
                anchor:'100%'
                },
                {
                xtype:'hidden',
                name: 'userPassword',
                anchor:'100%'
                },this.userLockStatesCombobox



        ]
	});
}
Ext.extend(ProjectUserForm, Ext.FormPanel, {});

/* Назначение пользователей и дочерних ролей */
ProjectRoleTabPanel = function()
{

/*  Дерево доступных ролей */
	this.availableRolesTree = new Ext.tree.TreePanel({
		id: 'availableSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
              text: 'Доступные',
              draggable: false,
              id: 'root',
              expanded: true			  
              }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        //loader: availableSysRoleLoader,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Дерево дочерних ролей */
	this.assignRolesTree = new Ext.tree.TreePanel({
        id: 'childrenSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
            text: 'Назначенные',
            draggable: false,
            id: 'root',
            expanded: true			
        }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Панель доступных ролей */
    this.availableRolesPanel = new Ext.Panel({
    	region: 'west',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
		autoScroll: true,
        anchor: '100%',
        items : [ this.availableRolesTree ]
    });

/*  Панель назначенных ролей */
    this.assignRolesPanel = new Ext.Panel({
        region: 'east',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
		autoScroll: true,
        anchor: '100%',
        items : [ this.assignRolesTree ]
    });

/*  Роли */
	this.rolesTab = new Ext.Panel({
		title : 'Назначение ролей',
        layout : 'table',
        height:186,
        items : [ this.availableRolesPanel, this.assignRolesPanel]
    });

/*  Конструктор */
	ProjectRoleTabPanel.superclass.constructor.call(this,{
		region: 'center',
        deferredRender: false,
        activeTab: 0,
        defaults:{autoScroll:true},
        items : [ this.rolesTab ]
	});
}
Ext.extend(ProjectRoleTabPanel, Ext.TabPanel, {});

/* Диалог добавления/изменения системной роли */
ProjectUserWindow = function()
{
	this.projectUserForm = new ProjectUserForm();
	this.projectRoleTabPanel = new ProjectRoleTabPanel();

	this.setMarkInvalidOldPassword = function (element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Необходимо ввести верный парль');
    	element.focus(true);
	}
	this.setMarkInvalid = function(element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Данное поле не может быть пустым');
    	element.focus();
	}
	this.setMarkInvalidShortPswd = function(element)
	{
		element.msgTarget = 'under';
		element.markInvalid('В целях обеспечения безопасности доступа длина пароля не может быть менее 6 символов.');
    	element.focus();
	}
	

	this.warningMessage = function (msg, focusedElement)
	{
		Ext.Msg.show({
	    title : 'Предупреждение',
     	msg : msg,
    	buttons : Ext.Msg.OK,
    	fn: function(btn)
    		{
    			if(btn == 'ok')
    	  		{
    			   focusedElement.focus();
    	  		}
    		},
    	icon : Ext.MessageBox.WARNING });
	}

	ProjectUserWindow.superclass.constructor.call(this,{
		width : 430,
		height : 510,
		closeAction : 'close',
		openAction : 'hide',
		plain : true,
		autoScroll : true,
		resizable : false,
		modal: true,
		items : [this.projectUserForm, this.projectRoleTabPanel]
    });
    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'ОК',
        handler : function()
        {
            this.fireEvent('ok')
        },
        scope : this,
        disabled: true
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function()
        {
            this.fireEvent('cancel')
        },
        scope : this,
        disabled: true
    });

    }
Ext.extend(ProjectUserWindow, Ext.Window, {
	initComponent : function() {

		  var self = this;
		  this.addEvents({ ok: true, cancel: true});

		  /* ok */
		  this.on('ok', function()
		  {

		  	var assignRoleNodes = [];
            var assignRoleIds = [];
            assignRoleNodes = this.projectRoleTabPanel.assignRolesTree.getRootNode().childNodes;
            for(var i = 0; i < assignRoleNodes.length; i++)
                assignRoleIds[i] = assignRoleNodes[i].id;

            var assignNotRoleNodes = [];
            var assignNotRoleIds = [];
            assignNotRoleNodes = this.projectRoleTabPanel.availableRolesTree.getRootNode().childNodes;
            for(var i = 0; i < assignNotRoleNodes.length; i++)
                assignNotRoleIds[i] = assignNotRoleNodes[i].id;

            var  projectUserFormValues = this.projectUserForm.getForm().getValues();

            if(projectUserFormValues.userName != '') {
	            if(projectUserFormValues.userId == '') {
	             	if(projectUserFormValues.password != '' && projectUserFormValues.password.length>=6) {
			             if (projectUserFormValues.password == projectUserFormValues.confirmPassword) {
			             	Projectadmin.conn.request({
			                    url: '/projectadmin/user/createuser',
			                    method: 'POST',
			                    //form: sysUserForm.id,
			                    params: {
							            'id':			projectUserFormValues.userId,
							            'name':			projectUserFormValues.userName,
							            'description':	projectUserFormValues.userDescription,
							            'password':		projectUserFormValues.password,
							            'locked': 		this.projectUserForm.userLockStatesCombobox.getValue(),
							            'assignRoleIds[]': 		assignRoleIds,
							            'assignNotRoleIds[]': 	assignNotRoleIds
							            },
			                    success: function() {
			                    Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();
			                    self.close();
			                	},
			                	failure: function()
		                        {
		                        	 self.projectUserForm.getForm().findField('userName').focus(true);
		                        }
			            	});
			             } else {
			             	this.warningMessage('Пароли не совпадают! Повторите ввод пароля.',this.projectUserForm.getForm().findField('password'));
			             	this.projectUserForm.getForm().setValues(
			             	{
			             		password: '',
			             		confirmPassword: ''
			             	});
			             }
	             	} else if(projectUserFormValues.password ==''){
	             		this.setMarkInvalid(this.projectUserForm.getForm().findField('password'));
	             	} else
						this.setMarkInvalidShortPswd(this.projectUserForm.getForm().findField('password'));
            	}

	            else
	            {

	                if(projectUserFormValues.password != '' && projectUserFormValues.password.length >= 6) {
		                if(projectUserFormValues.password == projectUserFormValues.confirmPassword) {
		                Projectadmin.conn.request({
		                    url: '/projectadmin/user/edituser',
		                    method: 'POST',
		                    params: {
						            'id':          	projectUserFormValues.userId,
						            'name':        	projectUserFormValues.userName,
						            'description': 	projectUserFormValues.userDescription,
						            'password':		projectUserFormValues.password,
						            'locked': 		this.projectUserForm.userLockStatesCombobox.getValue(),
						            'assignRoleIds[]': 	    assignRoleIds,
						            'assignNotRoleIds[]': 	assignNotRoleIds
						            },
		                    success: function() {
		                    Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();
		                    self.close();
		                    },
			                	failure: function()
		                        {
		                        	 self.projectUserForm.getForm().findField('userName').focus(true);
		                        }
			            });
	                	} else {
	                		this.warningMessage('Пароли не совпадают! Повторите ввод пароля.',this.projectUserForm.getForm().findField('password'));
			             	this.projectUserForm.getForm().setValues(
			             	{
			             		password: '',
			             		confirmPassword: ''
			             	});
	                	}
	                }else if(projectUserFormValues.password!=''){
	                		this.warningMessage('В целях обеспечения безопасности доступа к системе длина пароля не может быть менее 6 символов.',this.projectUserForm.getForm().findField('password'));
			             	this.projectUserForm.getForm().setValues(
			             	{
			             		password: '',
			             		confirmPassword: ''
			             	});
	                	} else {
	                   	Projectadmin.conn.request({
	                    url: '/projectadmin/user/edituser',
	                    method: 'POST',
	                    params: {
					            'id':          	projectUserFormValues.userId,
					            'name':        	projectUserFormValues.userName,
					            'description': 	projectUserFormValues.userDescription,
					            'password':		projectUserFormValues.userPassword,
					            'locked': 		this.projectUserForm.userLockStatesCombobox.getValue(),
					            'assignRoleIds[]': 	    assignRoleIds,
					            'assignNotRoleIds[]': 	assignNotRoleIds
					            },
	                    		success: function() {
	                    			Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();
                    	       		self.close();
                    	       		},
			                	failure: function()
		                        {
		                        	 self.projectUserForm.getForm().findField('userName').focus(true);
		                        }
	            		});
	                }
	            }
            } else {
            	this.setMarkInvalid(this.projectUserForm.getForm().findField('userName'));
            }
		  });

		  /* cancel */
		  this.on('cancel', function()
		  {
		  	this.close();
		  });
		ProjectUserWindow.superclass.initComponent.apply(this, arguments);
	}
});

/* Таблица пользователей */
ProjectUsersGrid = function()
{
	/* buttons */
	this.createButton = new Ext.Button({
		text : 'Создать',
		iconCls: 'icon-add',
        handler : function()
		{
			this.fireEvent('create')
		},
		scope : this
	});

	this.editButton = new Ext.Button({
		text : 'Свойства',
		iconCls: 'icon-properties',
        handler : function()
		{
			this.fireEvent('edit')
		},
		scope : this
	});

	this.removeButton = new Ext.Button({
		text : 'Удалить',
        iconCls: 'icon-delete',
		handler : function()
		{
			this.fireEvent('remove')
		},
		scope : this
	});
	/* constructor */

	this.userSelectionModel = new Ext.grid.CheckboxSelectionModel();

	this.userStore = new Ext.data.WgsJsonStore({
		id: 'sysUserStore',
        url: '/projectadmin/user/getuserlist',
        baseParams: {id: 0},
        autoLoad: true,
        fields: ['userId', 'userName', 'description','assignedRoles']

	});

	ProjectUsersGrid.superclass.constructor.call(this, {

		title : 'Управление пользователями проекта',
		store : this.userStore,
		cm : new Ext.grid.ColumnModel([
			{id: 'userId',header: "Идентификатор пользователя", hidden: true, hideable: false},
            {id: 'userName',header: "Имя пользователя", sortable: true, dataIndex: 'userName', width:240},
            {header: "Описание", sortable: true, dataIndex: 'description',width:500},
            {header: "Назначенные роли", dataIndex: 'assignedRoles',width:500}

		]),
		sm : this.userSelectionModel,
		viewConfig:
	       {
	           forceFit: true
	       },
		autoScroll : true,
		tbar : [ this.createButton, '-', this.editButton, '-', this.removeButton ],
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : this.userStore,
			displayInfo : true
		})

	});
}

Ext.extend(ProjectUsersGrid, Ext.grid.EditorGridPanel, {
	initComponent : function() {

	  this.addEvents({ create: true, edit: true, remove: true });
	  /* create */
	  this.on('create', function()
	  {
	  	var projectUserWindow = new ProjectUserWindow();

	  	projectUserWindow.projectRoleTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/user/getallroles',
	  		baseParams:{id:0}
	  		});

	  	projectUserWindow.projectRoleTabPanel.assignRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/user/getemptytree',
	  		baseParams:{id:0},
	  		listeners:
	  		{
	  			load: function()
	  			{
	  				projectUserWindow.cancelButton.enable();
	  				projectUserWindow.okButton.enable();
	  			}
	  		}
	  		});
	  	projectUserWindow.projectUserForm.userLockStatesCombobox.setValue(0);
	  	projectUserWindow.projectUserForm.add(projectUserWindow.projectUserForm.passwordField);
	  	projectUserWindow.projectUserForm.add(projectUserWindow.projectUserForm.confirmPasswordField);

	  	projectUserWindow.setTitle('Создание нового пользователя');
	  	projectUserWindow.show();


	  });

	  /* edit */
	  this.on('edit', function()
	  {
	  	var projectUserWindow = new ProjectUserWindow();

	  	var reader = new Ext.data.JsonReader({},[
                       {name: 'userId'},
                       {name: 'userName'},
                       {name: 'userDescription'},
                       {name: 'userLocked'},
                       {name: 'userPassword'}

                    ]);

        var userForChange = Projectadmin.projectUserManagementPanel.projectUsersGrid.userSelectionModel.getSelections();


                   if(userForChange.length != 0)
                   {
                       if(userForChange.length == 1)
                       {
                       var msgWait = Ext.Msg.wait('Получение данных о пользователе...', 'Подождите', {interval:32});
	                       Projectadmin.conn.request(
	                        {
	                            url: '/projectadmin/user/getuserproperties',
	                            method: 'POST',
	                            params: {'name':userForChange[0].data.userName},
	                            success: function(response) {
	                              projectUserWindow.projectUserForm.userLockStatesCombobox.setValue(reader.read(response).records[0].data.userLocked);
	                              projectUserWindow.projectUserForm.getForm().loadRecord(reader.read(response).records[0]);
	                              projectUserWindow.setTitle('Изменение свойств пользователя : '+userForChange[0].data.userName);
	                              //projectUserWindow.projectUserForm.getForm().findField('password').disable();
	                              //projectUserWindow.projectUserForm.getForm().findField('confirmPassword').disable();
	                              //alert(systemUserWindow.systemUserForm.getForm().getValues().userPassword);
	                              projectUserWindow.show();
	                              msgWait.hide();
	                            }
	                        });

	                      	projectUserWindow.projectRoleTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
						  		{
						  		dataUrl:'/projectadmin/user/getavialableroles',
						  		baseParams:{name:userForChange[0].data.userName}
						  		});

						  	projectUserWindow.projectRoleTabPanel.assignRolesTree.loader = new Ext.tree.WgsTreeLoader(
						  		{
						  		dataUrl:'/projectadmin/user/getparentroles',
						  		baseParams:{name:userForChange[0].data.userName},
						  		listeners:
						  		{
						  			load: function()
						  			{
						  				projectUserWindow.cancelButton.enable();
						  				projectUserWindow.okButton.enable();
						  			}
						  		}
						  		});
	                        projectUserWindow.projectUserForm.passwordField.fieldLabel = 'Новый пароль';
						  	//projectUserWindow.projectUserForm.add(projectUserWindow.projectUserForm.oldPasswordField);
						  	projectUserWindow.projectUserForm.add(projectUserWindow.projectUserForm.passwordField);
						  	projectUserWindow.projectUserForm.add(projectUserWindow.projectUserForm.confirmPasswordField);
						  	projectUserWindow.setTitle('Изменение пользователя: ');
						  	projectUserWindow.show();
                       }
                       else
                       Ext.Msg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать одного пользователя для изменения.',
							buttons : Ext.Msg.OK,
							icon : Ext.MessageBox.WARNING
						});
                   }
                   else
                   Ext.Msg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать одного пользователя для изменения.',
							buttons : Ext.Msg.OK,
							icon : Ext.MessageBox.WARNING
						});
	  });

	  /* remove */
	  this.on('remove', function()
	  {
		  	var usersForRemoveIds = [];
		    var usersForRemove = Projectadmin.projectUserManagementPanel.projectUsersGrid.userSelectionModel.getSelections();
		    var message = '';
		    if(usersForRemove.length != 0)
		    {
		    	if(usersForRemove.length == 1)
				message = 'Вы действительно желаете удалить выбранного пользователя?';
				else
				message = 'Вы действительно желаете удалить выбранных пользователей?';

		        Ext.MessageBox.confirm('', message,function(btn)
		        {
		          if (btn == 'yes')
		          {
		              for(var i = 0; i < usersForRemove.length; i++)
		              usersForRemoveIds[i] = usersForRemove[i].data.userId;

		            Ext.Ajax.request({
		            url: '/projectadmin/user/removeuser',
		            method: 'POST',

		            params: {'usersForRemoveIds[]': usersForRemoveIds},
		            success: function() {

						Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();
						//Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleStore.reload();
		                }
		            });
		          }

		        });

		    }
		    else
		    Ext.Msg.show({
				title : 'Предупреждение',
				msg : 'Необходимо выбрать пользователя для удаления.',
				buttons : Ext.Msg.OK,
				icon : Ext.MessageBox.WARNING
			});

	  });

	ProjectUsersGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* Администратор системных пользователей */
ProjectUserManagementPanel = function() {
	this.projectUsersGrid = new ProjectUsersGrid();

	ProjectUserManagementPanel.superclass.constructor.call(this, {
		title : 'Пользователи',
		closable : false,
		autoScroll : true,
		layout : 'fit',
		items : [this.projectUsersGrid]
	});
}

Ext.extend(ProjectUserManagementPanel, Ext.Panel, {});