﻿/* ObjectPermissionsGrid */

ObjectPermissionsGrid = function(data, objectId, objectName, msgWait)
{
    /* public */
    var self = this;

    /* privates */
    this.objectId = objectId;
    this.changed = false;
    this.data = data;

    /* methods */
    this.getPrivileges = function(data, storeFields)
    {
        var privileges = [];
        if (typeof(data)=='object')
        {
            data.each(function(record){
                $H(record).each(function(subrecord){
                    if (subrecord.key == 'privileges')
                        subrecord.value.each(function(privilege)
                        {
                            storeFields[storeFields.length] = privilege.privilegeId;
                            privileges[privileges.length] = {name: privilege.privilegeName, id: privilege.privilegeId};
                        });
                });
                throw $break;
            });
        }
        return privileges;
    }

    this.getGridData = function(data)
    {
        var storeData = [];
        data.each(function(record){
            var storeDataRow = [];
            $H(record).each(function(subrecord){
                if (subrecord.key == 'privileges')
                    subrecord.value.each(function(privilege)
                    {
                        storeDataRow[storeDataRow.length] = privilege.permission;
                    });
                else
                    storeDataRow[storeDataRow.length] = subrecord.value;
            });
            storeData[storeData.length] = storeDataRow;
        });
        return storeData;
    }

    this.getFilledColumns = function(privileges, gridColumns)
    {
        var checkColumns = [];
        privileges.each(function(privilege){
            var checkColumn = new Ext.grid.CheckColumnThree({
                header: privilege.name,
                align: 'center',
                dataIndex: privilege.id
            });
            checkColumns[checkColumns.length] = checkColumn;
            gridColumns[gridColumns.length] = checkColumn;
        });
        return {
            grid: gridColumns,
            check: checkColumns
        }
    }

    this.initGrid = function()
    {
        var storeFields = ['roleId', 'roleName', 'roleDescription', 'parentRoles', 'childRoles'];
        var gridColumns = [
            {id: 'roleId', hidden: true, hideable: false},
            {id: "roleName", header: "Имя роли", sortable: true, dataIndex: 'roleName'},
            {header: "Описание", sortable: true, dataIndex: 'roleDescription'},
            {header: "Родительские роли", sortable: true, dataIndex: 'parentRoles', width: 150},
            {header: "Дочерние роли", sortable: true, dataIndex: 'childRoles', width: 150}
        ];

        var privileges = this.getPrivileges(this.data, storeFields);
        var columns = this.getFilledColumns(privileges, gridColumns);
        this.columns = columns.grid;
        this.plugins = columns.check;
        this.store = new Ext.data.SimpleStore({
            fields: storeFields,
            listeners: {
                update: function()
                {
                    self.saveButton.enable();
                    self.cancelButton.enable();
                    self.changed = true;
                }
            }
        });
        //alert(this.getGridData(this.data)[0]);
        this.store.loadData(this.getGridData(this.data));
    }

    /* buttons */
    this.saveButton = new Ext.Button({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        disabled: true,
        handler: function() {this.fireEvent('save')},
        scope: this
    });

    this.cancelButton = new Ext.Button({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        disabled: true,
        handler: function() {this.fireEvent('cancel')},
        scope: this
    });

    this.selectAllButton = new Ext.Button({
        text: 'Выделить все',
        handler: function() {this.fireEvent('setAll', 1)},
        scope: this
    });

    this.inheritAllButton = new Ext.Button({
        text: 'Наследовать все',
        handler: function() {this.fireEvent('setAll', 2)},
        scope: this
    });

    this.deselectAllButton = new Ext.Button({
        text: 'Снять все',
        handler: function() {this.fireEvent('setAll', 0)},
        scope: this
    });
	
	
    /* constructor */
    ObjectPermissionsGrid.superclass.constructor.call(this, {
        clicksToEdit: 1,
        stripeRows: true,
        collapsible: false,
        autoExpandColumn: 'roleName',
        anchor: '100% 100%',
        region: 'center',
        title: 'Параметры доступа: ' + objectName,
        listeners: {
            render: function()
            {
                msgWait.hide();
            }
        },
        viewConfig: {forceFit: true},
        tbar: [this.saveButton, this.cancelButton, this.selectAllButton, this.inheritAllButton, this.deselectAllButton]
    });
}

Ext.extend(ObjectPermissionsGrid, Ext.grid.EditorGridPanel, {
    initComponent: function()
    {
        this.initGrid();

        this.addEvents({
            save: true,
            cancel: true,
            setAll: true
        });

        this.on('cancel', this.onCancel);
        this.on('save', this.onSave);
        this.on('setAll', this.onSetAll);

        ObjectPermissionsGrid.superclass.initComponent.apply(this, arguments);
    },

    onCancel: function()
    {
        this.store.rejectChanges();
        this.saveButton.disable();
        this.cancelButton.disable();
        this.changed = false;
    },

    onSave: function()
    {
        var modifiedRecords = this.store.getModifiedRecords();
        var params = [];
        Ext.each(modifiedRecords, function(record){
            params[params.length] = record.data;
        });
        Projectadmin.conn.request(
        {
            url: '/projectadmin/object/setobjectpermissions',
            method: 'POST',
            params: {permissions: Ext.util.JSON.encode(params), objectid: this.objectId},
            success: function()
            {
                this.store.commitChanges();
                this.saveButton.disable();
                this.cancelButton.disable();
                this.changed = false;
            },
            scope: this
        });
    },

    onSetAll: function(state)
    {
        this.store.each(function(record){
            // 5 - фиксировано! Шестой элемент - CheckColumn
            record.set(Object.keys(record.data)[5], state);
        });
    }
});

/* RolePermissionsGrid */

RolePermissionsGrid = function(data, roleId, roleName, msgWait)
{
    /* public */
    var self = this;

    /* privates */
    this.roleId = roleId;
    this.changed = false;
    this.data = data;

    this.getGridData = function(data)
    {
        var storeData = [];
        data.each(function(record){
            var storeDataRow = [];
            $H(record).each(function(subrecord){
                    storeDataRow[storeDataRow.length] = subrecord.value;
            });
            storeData[storeData.length] = storeDataRow;
        });
        return storeData;
    }

    this.initGrid = function()
    {
        var storeFields = ['id', 'text', 'permission'];

        var column = new Ext.grid.CheckColumnThree({
            header: 'Доступ',
            align: 'center',
            dataIndex: 'permission',
            width: 100
        });

        this.columns = [
            {id: 'id', hidden: true, hideable: false},
            {header: "Наименование объект", dataIndex: "text", width: 400},
            column
        ];

        this.plugins =  column;
        this.store = new Ext.data.SimpleStore({
            fields: storeFields,
            listeners: {
                update: function()
                {
                    self.saveButton.enable();
                    self.cancelButton.enable();
                    self.changed = true;
                }
            }
        });

        this.store.loadData(this.getGridData(this.data));
    }

    /* buttons */
    this.saveButton = new Ext.Button({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        disabled: true,
        handler: function() {this.fireEvent('save')},
        scope: this
    });

    this.cancelButton = new Ext.Button({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        disabled: true,
        handler: function() {this.fireEvent('cancel')},
        scope: this
    });

    this.selectAllButton = new Ext.Button({
        text: 'Выделить все',
        handler: function() {this.fireEvent('setAll', 1)},
        scope: this
    });

    this.inheritAllButton = new Ext.Button({
        text: 'Наследовать все',
        handler: function() {this.fireEvent('setAll', 2)},
        scope: this
    });

    this.deselectAllButton = new Ext.Button({
        text: 'Снять все',
        handler: function() {this.fireEvent('setAll', 0)},
        scope: this
    });
this.setAreaButton = new Ext.Button({
        text: 'Задать доступную область',
        handler: function() {this.fireEvent('setArea', 0)},
        scope: this
    });
    /* constructor */
    RolePermissionsGrid.superclass.constructor.call(this, {
        clicksToEdit: 1,
        stripeRows: true,
        collapsible: false,
        anchor: '100% 100%',
        region: 'center',
        title: 'Параметры доступа: ' + roleName,
        cm: this.columns,
        listeners: {
            render: function()
            {
                msgWait.hide();
            }
        },
        tbar: [this.saveButton, this.cancelButton, this.selectAllButton, this.inheritAllButton, this.deselectAllButton,'-',this.setAreaButton]
    });
}

Ext.extend(RolePermissionsGrid, Ext.grid.EditorGridPanel, {
    initComponent: function()
    {
        this.initGrid();

        this.addEvents({
            save: true,
            cancel: true,
            setAll: true,
			setArea:true
        });

        this.on('cancel', this.onCancel);
        this.on('save', this.onSave);
        this.on('setAll', this.onSetAll);
this.on('setArea', this.onSetArea);
        RolePermissionsGrid.superclass.initComponent.apply(this, arguments);
    },

    onSetAll: function(state)
    {
        this.store.each(function(record){
            if (record.get('permission') != null)
                record.set('permission', state);
        });
    },

    onCancel: function()
    {
        this.store.rejectChanges();
        this.saveButton.disable();
        this.cancelButton.disable();
        this.changed = false;
    },
onSetArea: function()
    {
	var win
	var ri = this.roleId
		function setAvArea(message){
			Projectadmin.conn.request(
                        {
                            url: '/projectadmin/role/setArea',
							method: 'POST',
                            params: {m: 1,r:ri,l:message.content.ftf,d:message.content.context,s:message.content.sdo},
                            success: function()
                            {
                                //win.destroy();
                                //win = null;
								//alert('successful')
								Ext.Msg.show({
                   title:'Изменения сохранены',
                   msg: 'Ограничения по области территории генплана установлены',
                   buttons: Ext.Msg.OK
                   
                });
                                win.close();
                            }
                        });
		}
		function unsetAvArea(){
			Projectadmin.conn.request(
                        {
                            url: '/projectadmin/role/setArea',
							method: 'POST',
                            params: {m: 1,r:ri,l:'',d:''},
                            success: function()
                            {
                                //win.destroy();
                                //win = null;
								//alert('successful')
								Ext.Msg.show({
                   title:'Изменения сохранены',
                   msg: 'Ограничения по области территории генплана удалены',
                   buttons: Ext.Msg.OK
                   
                });
                                win.close();
                            }
                        });
		}
		Projectadmin.conn.request(
                        {
                            url: '/projectadmin/role/getArea',
							method: 'POST',
                            params: {m: 1,r:ri},
                            success: function(data)
                            {
                                
                           
        win = new Ext.Window({
                layout:'border',
                title: 'Выбор доступной области территории',
                modal: true,
                width: 800,
                height: 500,
                plain: true,
                resizable: true,
                items: [
					{
						region: 'center',
						cmargins: '5 5 5 5',
						html:'<div id="container" style="width:100%;height:100%"></div>'
					}
				],
                buttons: [{
                    iconCls: 'icon-accept-tick',
                    text:'Установить ограничения',
					id: 'okSetArea',
					disabled: true,
                    handler: function() {
                        socketObj.getArea(setAvArea);
						/*
						var id = layoutComboBox.getValue();
                        Projectadmin.conn.request(
                        {
                            url: '/projectadmin/object/setdefaultlayout',
                            params: {id: id},
                            success: function()
                            {
                                //win.destroy();
                                //win = null;
                                win.close();
                            }
                        });*/
                    }
                },{
                    iconCls: 'icon-delete',
                    text: 'Снять ограничения',
                    handler: function(){
                        unsetAvArea();
						win.close();
                        //win.destroy();
                        //win = null;
                    }
                },{
                    iconCls: 'icon-close-cross',
                    text: 'Закрыть',
                    handler: function(){
                        win.close();
                        //win.destroy();
                        //win = null;
                    }
                }]
            });
            win.show();
			
			var d = Ext.util.JSON.decode(data.responseText)
			var hash = ''
			if(d.record[0])
				hash = d.record[0].display ? ('#'+d.record[0].display) : '';
					
			var socketObj = {
    				//запрос на получение выделенной области и контекста
					getArea: function(callback){
						var self = this; 
						self.callback = callback; 
						message = Ext.util.JSON.encode({type: MT_GET_AREA});
 						self.socket.postMessage(message)										
					},					
					//запрос на получение выделенного объекта и контекста
					//...временно только одного выделенного объекта					
					getSelection: function(){
						var self = this; 
						message = message = Ext.util.JSON.encode({type: MT_GET_SELECTION});
 						self.socket.postMessage(message)
						
					},
					//запрос на установку контекста					
					setView: function(params){
						var self = this; 
						message = message = Ext.util.JSON.encode({type: MT_SET_VIEW, content: params});
 						self.socket.postMessage(message)
						
					},
					//запрос на получение контекста				
					getView: function(){
						var self = this; 
						message = message = Ext.util.JSON.encode({type: MT_GET_VIEW});
 						self.socket.postMessage(message)
						
					},
					//Это лишнее
					establishConnection: function(){
						var self = this;
						message = message = Ext.util.JSON.encode({type: MT_ESTABLISH_CONNECTION});
 						self.socket.postMessage(message)
						
					},
					//Собственно сокет
					socket : new easyXDM.Socket({
						remote: "/mapwidget"+hash,
						container: document.getElementById("container"),
    					
    					//разбор ответов
						onMessage: function(message, origin){
							if(socketObj.callback)
								parseMessage(message, origin, socketObj.callback)
							else
								parseMessage(message, origin)
						}						
					})			
				}
    			
    			
				/*
        	//Элементы интерфейса. Их создание
        		var myButton = new Button({
            		label: "Контекст и выделенная область",
            		onClick: function(){
                		// Do something:
                		socketObj.getArea();                		
            		}
        		}, "btnGetArea");
        		var myButton = new Button({
            		label: "Контекст и выделенные объекты",
            		onClick: function(){
                		// Do something:
                		socketObj.getSelection();                		
            		}
        		}, "btnGetSelection");
        		var btnSetMapParams = new Button({
            		label: "Установить контекст",
            		onClick: function(){
                		// Do something:
                		socketObj.setView(dijit.byId('input').get('value'))
                		               			
            		}
        		}, "btnSetMap");
        		var btnGetMapParams = new Button({
            		label: "Контекст",
            		onClick: function(){
                		// Do something:
                		socketObj.getView()	
            		}
        		}, "btnGetMap");
        		var inputTextarea = new TextArea({
            		innerHTML: 'Сюда можно скопировать контекст, полученный выше. Можно скопировать контекст и выделенную область... и нажать "Установить..."'
        		}, "input");
			
			*/
			 }
        });
    },
    onSave: function()
    {
        var modifiedRecords = this.store.getModifiedRecords();
        var params = [];
        Ext.each(modifiedRecords, function(record){
            var rec = new Object();
            rec.id = record.data.id;
            rec.permission = record.data.permission;
            params[params.length] = rec;
        });
        Projectadmin.conn.request(
        {
            url: '/projectadmin/object/setrolepermissions',
            method: 'POST',
            params: {permissions: Ext.util.JSON.encode(params), roleid: this.roleId},
            success: function()
            {
                this.store.commitChanges();
                this.saveButton.disable();
                this.cancelButton.disable();
                this.changed = false;
            },
            scope: this
        });
    }
});

/* PermissionPanel */

PermissionPanel = function()
{
    /* methods */
    this.addRolePermissionsGrid = function(grid)
    {
        this.rolePermissionsGrid = grid;
        this.add(this.rolePermissionsGrid);
        this.doLayout();
    }

    this.addObjectPermissionsGrid = function(grid)
    {
        this.objectPermissionsGrid = grid;
        this.add(this.objectPermissionsGrid);
        this.doLayout();
    }

    this.clear = function()
    {
        if (this.rolePermissionsGrid)
        {
            this.remove(this.rolePermissionsGrid);
            this.rolePermissionsGrid = null;
        }
        if (this.objectPermissionsGrid)
        {
            this.remove(this.objectPermissionsGrid);
            this.objectPermissionsGrid = null;
        }
    }

    /* buttons */
    this.overObjectsButton = new Ext.Button({
        text: 'Назначение прав через объекты',
        pressed: true,
        handler: function() {this.fireEvent('overObjects', this.overObjectsButton)},
        scope: this
    });

    this.overRolesButton = new Ext.Button({
        text: 'Назначение прав через роли',
        handler: function() {this.fireEvent('overRoles', this.overRolesButton)},
        scope: this
    });

    this.defaultLayoutButton = new Ext.Button({
        text: 'Выбрать карту по умолчанию',
        handler: function() {this.fireEvent('setDefaultLayout', this.defaultLayoutButton)},
        scope: this
    });

    this.overviewLayoutButton = new Ext.Button({
        text: 'Выбрать карту для общего вида',
        handler: function() {this.fireEvent('setOverviewLayout', this.overviewLayoutButton)},
        scope: this
    });

    /* constructor */
    ObjectPermissionsGrid.superclass.constructor.call(this, {
        region: 'center',
        border: true,
        collapsible: false,
        split: true,
        autoScroll: true,
        margins: '0 0 0 0',
        layout: 'anchor',
        tbar: [this.overObjectsButton, this.overRolesButton, '-', this.defaultLayoutButton, this.overviewLayoutButton]
    });
}

Ext.extend(PermissionPanel, Ext.Panel, {
    initComponent: function() {

        this.addEvents({
            overObjects: true,
            overRoles: true,
            setDefaultLayout: true,
            setOverviewLayout: true
        });

        /* overObjects */
        this.on('overObjects', function(button){
            button.toggle(true);
            Projectadmin.projectObjectManagementPanel.permissionTreePanel.overObjectsPanel.expand();
            Projectadmin.projectObjectManagementPanel.permissionPanel.overRolesButton.toggle(false);
        });

        /* overRoles */
        this.on('overRoles', function(button){
            button.toggle(true);
            Projectadmin.projectObjectManagementPanel.permissionTreePanel.overRolesPanel.expand();
            Projectadmin.projectObjectManagementPanel.permissionPanel.overObjectsButton.toggle(false);
        });

        /* setDefaultLayout */
        this.on('setDefaultLayout', function(){
            var store = new Ext.data.WgsJsonStore({
                autoLoad: true,
                url: '/projectadmin/object/getdefaultlayout',
                fields: ['id', 'name', 'bydefault']
            });

            var layoutComboBox = new Ext.form.ComboBox({
                store: store,
                editable: false,
                displayField: 'name',
                valueField: 'id',
                loadingText: 'Загрузка...',
                margins: '2 2 2 2',
                triggerAction: 'all'
            });

            store.on('load', function(ds,records,o) {
                records.each(function(e) {
                    if (e.data.bydefault == '1')
                    {
                        layoutComboBox.setValue(e.data.id);
                        throw $break;
                    }
                });
            });

            var win = new Ext.Window({
                layout:'fit',
                title: 'Карта по умолчанию',
                modal: true,
                width: 500,
                height: 92,
                plain: true,
                resizable: false,
                items: layoutComboBox,
                buttons: [{
                    iconCls: 'icon-accept-tick',
                    text:'OK',
                    handler: function() {
                        var id = layoutComboBox.getValue();
                        Projectadmin.conn.request(
                        {
                            url: '/projectadmin/object/setdefaultlayout',
                            params: {id: id},
                            success: function()
                            {
                                //win.destroy();
                                //win = null;
                                win.close();
                            }
                        });
                    }
                },{
                    iconCls: 'icon-close-cross',
                    text: 'Отмена',
                    handler: function(){
                        win.close();
                        //win.destroy();
                        //win = null;
                    }
                }]
            });
            win.show();
        });

        /* setOverviewLayout */
        this.on('setOverviewLayout', function(){
            var store = new Ext.data.WgsJsonStore({
                autoLoad: true,
                url: '/projectadmin/object/getoverviewlayout',
                fields: ['id', 'name', 'overview']
            });

            var layoutComboBox = new Ext.form.ComboBox({
                store: store,
                editable: false,
                displayField: 'name',
                valueField: 'id',
                loadingText: 'Загрузка...',
                margins: '2 2 2 2',
                triggerAction: 'all'
            });

            store.on('load', function(ds,records,o) {
                records.each(function(e) {
                    if (e.data.overview == '1')
                    {
                        layoutComboBox.setValue(e.data.id);
                        throw $break;
                    }
                });
            });

            var win = new Ext.Window({
                layout:'fit',
                title: 'Карта для общего вида',
                modal: true,
                width: 500,
                height: 92,
                plain: true,
                resizable: false,
                items: layoutComboBox,
                buttons: [{
                    iconCls: 'icon-accept-tick',
                    text:'OK',
                    handler: function() {
                        var id = layoutComboBox.getValue();
                        Projectadmin.conn.request(
                        {
                            url: '/projectadmin/object/setoverviewlayout',
                            params: {id: id},
                            success: function()
                            {
                                win.close();
                                //win.destroy();
                                //win = null;
                            }
                        });
                    }
                },{
                	iconCls: 'icon-close-cross',
                    text: 'Отмена',
                    handler: function(){
                        win.close();
                        //win.destroy();
                        //win = null;
                    }
                }]
            });
            win.show();
        });

        PermissionPanel.superclass.initComponent.apply(this, arguments);
    }
});

/* OverObjectsPanel */

OverObjectsPanel = function()
{
    this.objectTree = new Ext.tree.TreePanel({
        rootVisible: false,
        root: new Ext.tree.AsyncTreeNode({
            text: 'Слои',
            loader: new Ext.tree.WgsTreeLoader({dataUrl:'/projectadmin/object/getobjecttree'}),
            children:
            [{
                text:'Слои с данными',
                expanded: false,
                id: 'fl-root'
            },
            {
                text:'Подложки',
                expanded: false,
                id: 'bl-root'
            },
            {
                text:'Функциональность и представление',
                expanded: false,
                id: 'cmd-root'
            }]
        }),
        animate: true,
        singleExpand: false,
        autoScroll: false,
        border: false,
        enableDD: false,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

    OverObjectsPanel.superclass.constructor.call(this, {
        title: 'через объекты',
        border: false,
        autoScroll: true,
        iconCls: 'nav',
        items: this.objectTree,
        tools: [{
            id: 'left',
            on: {
                click: function(){this.objectTree.root.collapse(true, true)},
                scope: this
            }
            },{
            id: 'right',
            on: {
                click: function(){this.objectTree.root.expand(true, true)},
                scope: this
            }
            },{
            id:'refresh',
            on:{
                click: function(){this.objectTree.root.reload()},
                scope: this
            }
        }]
    });
}

Ext.extend(OverObjectsPanel, Ext.Panel, {
    initComponent: function()
    {
        /* expand */
        this.on('expand', function(){
            Projectadmin.projectObjectManagementPanel.permissionPanel.overRolesButton.toggle(false);
            Projectadmin.projectObjectManagementPanel.permissionPanel.overObjectsButton.toggle(true);
        });

        /* objectTree@click */
        this.objectTree.on('click', function(node){
            var fnUpdatePermissionGrid = function(result) {
                var updatePermissionGrid = function()
                {
                    if (node.leaf)
                    {
                        var msgWait = Ext.Msg.wait('Генерация списка прав...', 'Подождите', {interval:32});
                        Projectadmin.conn.request(
                        {
                            url: '/projectadmin/object/getobjectpermissions',
                            params: {id: node.id},
                            success: function(response)
                            {
                                Projectadmin.projectObjectManagementPanel.permissionPanel.clear();
                                try {
                                    var jsonData = Ext.util.JSON.decode(response.responseText)
                                } catch(e) {}
                                if (jsonData)
                                    Projectadmin.projectObjectManagementPanel.permissionPanel.addObjectPermissionsGrid(
                                        new ObjectPermissionsGrid(jsonData, node.id, node.text, msgWait)
                                    );
                                else
                                    msgWait.hide();
                            }
                        });
                    }
                    else
                        Projectadmin.projectObjectManagementPanel.permissionPanel.clear();
                }
                if (result == 'yes')
                   Projectadmin.getUnsavedGrid().fireEvent('save');
                if (result != 'cancel')
                    updatePermissionGrid();
            };
            if (Projectadmin.getUnsavedGrid())
                Ext.Msg.show({
                   title:'Сохранить изменения?',
                   msg: 'Вы пытаетесь получить список прав, не сохранив изменения. Сохранить изменения?',
                   buttons: Ext.Msg.YESNOCANCEL,
                   fn: fnUpdatePermissionGrid,
                   icon: Ext.MessageBox.QUESTION
                });
            else
                fnUpdatePermissionGrid();
        });

        OverObjectsPanel.superclass.initComponent.apply(this, arguments);
    }
});

/* OverRolesPanel */

OverRolesPanel = function()
{
    this.roleTree = new Ext.tree.TreePanel({
        root: new Ext.tree.AsyncTreeNode({
            text: 'Роли проекта',
            loader: new Ext.tree.WgsTreeLoader({dataUrl:'/projectadmin/object/getroletree'}),
            id: 'root',
            expanded: true
        }),
        animate: true,
        singleExpand: false,
        autoScroll: false,
        border: false,
        enableDD: false,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

    OverRolesPanel.superclass.constructor.call(this, {
        title: 'через роли',
        border: false,
        autoScroll: true,
        iconCls: 'settings',
        items: this.roleTree,
        tools: [{
            id:'refresh',
            on:{
                click: function(){this.roleTree.root.reload()},
                scope: this
            }
        }]
    });
}

Ext.extend(OverRolesPanel, Ext.Panel, {
    initComponent: function()
    {
        /* expand */
        this.on('expand', function(){
            Projectadmin.projectObjectManagementPanel.permissionPanel.overRolesButton.toggle(true);
            Projectadmin.projectObjectManagementPanel.permissionPanel.overObjectsButton.toggle(false);
        });

        /* roleTree@click */
        this.roleTree.on('click', function(node){
            var fnUpdatePermissionGrid = function(result) {
                var updatePermissionGrid = function()
                {
                    if (node.leaf)
                    {
                        var msgWait = Ext.Msg.wait('Генерация списка прав...', 'Подождите', {interval:32});
                        Projectadmin.conn.request(
                        {
                            url: '/projectadmin/object/getrolepermissions',
                            params: {id: node.id},
                            success: function(response)
                            {
                                Projectadmin.projectObjectManagementPanel.permissionPanel.clear();
                                try {
                                    var jsonData = Ext.util.JSON.decode(response.responseText)
                                } catch(e) {}
                                if (jsonData)
                                    Projectadmin.projectObjectManagementPanel.permissionPanel.addRolePermissionsGrid(
                                        new RolePermissionsGrid(jsonData, node.id, node.text, msgWait)
                                    );
                                else
                                    msgWait.hide();
                            }
                        });
                    }
                    else
                        Projectadmin.projectObjectManagementPanel.permissionPanel.clear();
                }
                if (result == 'yes')
                   Projectadmin.getUnsavedGrid().fireEvent('save');
                if (result != 'cancel')
                    updatePermissionGrid();
            };
            if (Projectadmin.getUnsavedGrid())
                Ext.Msg.show({
                   title:'Сохранить изменения?',
                   msg: 'Вы пытаетесь получить список прав, не сохранив изменения. Сохранить изменения?',
                   buttons: Ext.Msg.YESNOCANCEL,
                   fn: fnUpdatePermissionGrid,
                   icon: Ext.MessageBox.QUESTION
                });
            else
                fnUpdatePermissionGrid();
        });

        OverRolesPanel.superclass.initComponent.apply(this, arguments);
    }
});

/* PermissionTreePanel */

PermissionTreePanel = function()
{
    this.overObjectsPanel = new OverObjectsPanel();
    this.overRolesPanel = new OverRolesPanel();

    PermissionTreePanel.superclass.constructor.call(this, {
        region: 'west',
        title: 'Назначение прав',
        split: true,
        width: 300,
        minSize: 175,
        maxSize: 400,
        collapsible: true,
        collapseMode: 'mini',
        margins:'0 0 0 0',
        layout: 'accordion',
        layoutConfig: {animate: true},
        items: [this.overObjectsPanel, this.overRolesPanel]
    });
}

Ext.extend(PermissionTreePanel, Ext.Panel);

/* ProjectObjectManagementPanel */

ProjectObjectManagementPanel = function()
{
    this.permissionPanel = new PermissionPanel();
    this.permissionTreePanel = new PermissionTreePanel();

    ProjectObjectManagementPanel.superclass.constructor.call(this, {
        title: 'Параметры доступа',
        closable: false,
        autoScroll: false,
        layout: 'border',
        items: [this.permissionPanel, this.permissionTreePanel]
    });
}

Ext.extend(ProjectObjectManagementPanel, Ext.Panel);