﻿/* Форма основных характеристик роли */
ProjectRoleForm = function()
{
	Ext.namespace('Ext.projectRoleLock');
    Ext.projectRoleLock.states = [
        ['1','Заблокирована'],
        ['0','Разблокирована']
        ];

        this.projectRoleLockStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: Ext.projectRoleLock.states
        });

        this.projectRoleLockStates.loadData(Ext.projectRoleLock.states);

        this.roleLockStatesCombobox = new Ext.form.ComboBox({
                                    name:'roleLocked',
                                    store: this.projectRoleLockStates,
                                    displayField:'state',
                                    valueField: 'id',
                                    fieldLabel: 'Состояние',
                                    typeAhead: true,
                                    mode: 'local',
                                    anchor:'100%',
                                    triggerAction: 'all',
                                    editable:false,
                                    selectOnFocus:true
                                    });



	ProjectRoleForm.superclass.constructor.call(this,{
		id: 'sysRoleForm',
		method: 'POST',
		title: 'Общие свойства',
		region: 'north',
		//width: 200,
		height:160,
		collapsible: false,
		frame:true,
		bodyStyle:'padding:5px 5px 0',
		defaults: {width: 230},
		items: [
		    	{
		        xtype:'hidden',
		        name: 'roleId',
		        allowBlank:true,
		        anchor:'100%'
		        },
		        {
		        xtype:'textfield',
		        fieldLabel: 'Название',
		        name: 'roleName',
		        msgTarget: 'under',
                msgFx: 'normal',
                invalidText : 'Недопустимое значение',
                allowBlank:false,
                blankText: 'Данное поле не может быть пустым',
		        regex: /^([\wА-Яа-я_][ ]?)+$/,
		        maskRe: /[A-Za-zА-Яа-я_ ]+/,
		        maxLength: 255,
		        maxLengthText: "Число символов не должно превышать 255",
		        anchor:'100%'
		    	},
		    	{
		        xtype:'textarea',
		        fieldLabel: 'Описание',
		        name: 'roleDescription',
		        anchor:'100%'
		    	},
		    	this.roleLockStatesCombobox
		       ]
	});
}
Ext.extend(ProjectRoleForm, Ext.FormPanel, {});

/* Назначение пользователей и дочерних ролей */
ProjectRoleUserTabPanel = function()
{
/*  Дерево доступных пользователей  */
	this.availableUsersTree = new Ext.tree.TreePanel({
        id : 'availableSysUserTree',
        root : new Ext.tree.AsyncTreeNode({
              text : 'Доступные',
              draggable : false, // disable root node dragging
              id : 'root',
              expanded : true,
			  autoScroll: true		
              }),
        selModel: new Ext.tree.MultiSelectionModel({}),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Дерево назначенных пользователей  */
	this.membersUsersTree = new Ext.tree.TreePanel({
		id: 'memberSysUserTree',
        root: new Ext.tree.AsyncTreeNode({
            text: 'Назначенные',
            draggable: false,
            id: 'root',
            expanded: true,
			autoScroll: true
		
        }),
        selModel: new Ext.tree.MultiSelectionModel({}),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Панель доступных пользователей  */
	this.avalableUsersPanel = new Ext.Panel({
		region: 'west',
		split: true,
		width: 206,
		height:186,
		collapsible: false,
		autoScroll: true,
		anchor: '100%',
    	items: [ this.availableUsersTree ]
  	});

/*  Панель назначенных пользователей  */
    this.membersUsersPanel = new Ext.Panel({
    	region: 'east',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
		autoScroll: true,
        anchor: '100%',
        items:[ this.membersUsersTree ]
    });

/*  Пользователи */
    this.usersTab = new Ext.Panel({
    	title: 'Пользователи',
		layout: 'table',
	    height:186,
	    items:[this.avalableUsersPanel, this.membersUsersPanel]
  	});

/*  Дерево доступных ролей */
	this.availableRolesTree = new Ext.tree.TreePanel({
		id: 'availableSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
              text: 'Доступные',
              draggable: false,
              id: 'root',
              expanded: true
              }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Дерево дочерних ролей */
	this.childrenRolesTree = new Ext.tree.TreePanel({
        id: 'childrenSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
            text: 'Назначенные',
            draggable: false,
            id: 'root',
            expanded: true
        }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Панель доступных ролей */
    this.availableRolesPanel = new Ext.Panel({
    	region: 'west',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
        anchor: '100%',
        items : [ this.availableRolesTree ]
    });

/*  Панель доступных ролей */
    this.childrenRolesPanel = new Ext.Panel({
        region: 'east',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
        anchor: '100%',
        items : [ this.childrenRolesTree ]
    });

/*  Роли */
	this.rolesTab = new Ext.Panel({
		title : 'Дочерние роли',
        layout : 'table',
        items : [ this.availableRolesPanel, this.childrenRolesPanel]
    });

/*  Конструктор */
	ProjectRoleUserTabPanel.superclass.constructor.call(this,{
		region: 'center',
        deferredRender: false,
        activeTab: 0,
        defaults:{autoScroll:true},
        items : [ this.usersTab, this.rolesTab ]
	});
}
Ext.extend(ProjectRoleUserTabPanel, Ext.TabPanel, {});

/* Диалог добавления/изменения системной роли */
ProjectRoleWindow = function()
{
	var self = this;
	this.projectRoleForm = new ProjectRoleForm();
	this.projectRoleUserTabPanel = new ProjectRoleUserTabPanel();

	this.setMarkInvalid = function(element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Данное поле не может быть пустым');
    	element.focus();
	}

	ProjectRoleWindow.superclass.constructor.call(this,{
		width : 430,
		height : 450,
		closeAction : 'close',
		openAction : 'hide',
		plain : true,
		autoScroll : true,
		resizable : false,
		modal: true,
		items : [this.projectRoleForm, this.projectRoleUserTabPanel],
        scope:this
    });
    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'ОК',
        handler : function()
        {
            this.fireEvent('ok')
        },
        scope : this,
        disabled: true
    });

    this.cancelButton = this.addButton({
    	iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function()
        {
            this.fireEvent('cancel')
        },
        scope : this,
        disabled: true
    });
}
Ext.extend(ProjectRoleWindow, Ext.Window, {
	initComponent : function() {

		  var self = this;
		  this.addEvents({ ok: true, cancel: true});

		  /* ok */
		  this.on('ok', function()
		  {
		  	var self = this;
		  	var memberUserNodes = [];
            var memberUserIds = [];
            memberUserNodes = this.projectRoleUserTabPanel.membersUsersTree.getRootNode().childNodes;
            for(var i = 0; i < memberUserNodes.length; i++)
                memberUserIds[i] = memberUserNodes[i].id;

            var childrenRoleNodes = [];
            var childrenRoleIds = [];
            childrenRoleNodes = this.projectRoleUserTabPanel.childrenRolesTree.getRootNode().childNodes;
            for(var i = 0; i < childrenRoleNodes.length; i++)
                childrenRoleIds[i] = childrenRoleNodes[i].id;


            var memberNotUserNodes = [];
            var memberNotUserIds = [];
            memberNotUserNodes = this.projectRoleUserTabPanel.availableUsersTree.getRootNode().childNodes;
            for(var i = 0; i < memberNotUserNodes.length; i++)
                memberNotUserIds[i] = memberNotUserNodes[i].id;

            var childrenNotRoleNodes = [];
            var childrenNotRoleIds = [];
            childrenNotRoleNodes = this.projectRoleUserTabPanel.availableRolesTree.getRootNode().childNodes;
            for(var i = 0; i < childrenNotRoleNodes.length; i++)
                childrenNotRoleIds[i] = childrenNotRoleNodes[i].id;

            var  projectRoleForm = this.projectRoleForm.getForm();
            var  roleLocked = this.projectRoleForm.roleLockStatesCombobox.getValue();
            if(projectRoleForm.getValues().roleName != '')
		    {
	            if(this.projectRoleForm.getForm().getValues().roleId == '')
	            {
	            Projectadmin.conn.request({
	                    url: '/projectadmin/role/createrole',
	                    method: 'POST',
	                    params: {
	                    	'id':projectRoleForm.getValues().roleId,
	                    	'name':projectRoleForm.getValues().roleName,
	                    	'description': projectRoleForm.getValues().roleDescription,
	                    	'locked':roleLocked,
	                    	'memberUserIds[]': memberUserIds,
	            			'childrenRoleIds[]': childrenRoleIds,
	            			'memberNotUserIds[]': memberNotUserIds,
	            			'childrenNotRoleIds[]': childrenNotRoleIds
	                    	},
	                    success: function()
	                    	{
		                        Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleStore.reload();
	                        	self.close();
	                        },
	                        failure: function()
	                        {
	                        	 self.projectRoleForm.getForm().findField('roleName').focus(true);
	                        }
	            });
	            }
	            else
	            {
	            Projectadmin.conn.request({
	                    url: '/projectadmin/role/editrole',
	                    method: 'POST',
	                    params: {
	                    	'id':projectRoleForm.getValues().roleId,
	                    	'name':projectRoleForm.getValues().roleName,
	                    	'description': projectRoleForm.getValues().roleDescription,
	                    	'locked':roleLocked,
	                    	'memberUserIds[]': memberUserIds,
	            			'childrenRoleIds[]': childrenRoleIds,
	            			'memberNotUserIds[]': memberNotUserIds,
	            			'childrenNotRoleIds[]': childrenNotRoleIds
	                    	},
	                    success: function()
		                    {
			                    Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleStore.reload();
		                    	self.close();
		                    },
	                        failure: function()
	                        {
	                        	 self.projectRoleForm.getForm().findField('roleName').focus(true);
	                        }
	            });

            	}

            }
			else
			{
				this.setMarkInvalid(this.projectRoleForm.getForm().findField('roleName'));
			}


		  });

		  /* cancel */
		  this.on('cancel', function()
		  {
		  	this.close();
		  });

		  /*this.on('close', function()
		  {
		  	if(this.cancelButton.disabled != true)
		  		this.close();

		  });*/

		ProjectRoleWindow.superclass.initComponent.apply(this, arguments);
	}
});

/* Таблица ролей */
ProjectRolesGrid = function()
{
	/* public */
	var self = this;

	/* buttons */
	this.createButton = new Ext.Button({
		text : 'Создать',
        iconCls: 'icon-add',
		handler : function()
		{
			this.fireEvent('create')
		},
		scope : this
	});

	this.editButton = new Ext.Button({
		text : 'Свойства',
		iconCls: 'icon-properties',
        handler : function()
		{
			this.fireEvent('edit')
		},
		scope : this
	});

	this.removeButton = new Ext.Button({
		text : 'Удалить',
        iconCls: 'icon-delete',
		handler : function()
		{
			this.fireEvent('remove')
		},
		scope : this
	});
	/* constructor */

	this.roleSelectionModel = new Ext.grid.CheckboxSelectionModel();
	this.roleStore = new Ext.data.WgsJsonStore({
		id : 'sysRoleStore',
		url : '/projectadmin/role/getrolelist',
		baseParams : {id : 0},
		autoLoad : true,
		fields : ['roleId', 'roleName', 'description', 'roleUserMembers', 'childrenRoles']

	});

	ProjectRolesGrid.superclass.constructor.call(this, {

		title : 'Управление ролями проекта',
		store : this.roleStore,
		cm : new Ext.grid.ColumnModel([//this.roleSelectionModel,
			{ id : 'roleId', header : "Идентификатор роли", hidden : true, hideable : false },
			{ id : 'roleName', header : "Имя роли", sortable : true, dataIndex : 'roleName', width : 220 },
			{ header : "Описание", sortable : true, dataIndex : 'description', width : 420 },
			{ header : "Пользователи", dataIndex : 'roleUserMembers', width : 300 },
			{ header : "Дочерние роли", dataIndex : 'childrenRoles', width : 300 }

		]),
		sm : this.roleSelectionModel,
		viewConfig:
	       {
	           forceFit: true
	       },
		autoScroll : true,
		tbar : [ this.createButton, '-', this.editButton, '-', this.removeButton ],
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : this.roleStore,
			displayInfo : true
		})

	});
}
Ext.extend(ProjectRolesGrid, Ext.grid.EditorGridPanel, {
	initComponent : function() {

	  this.addEvents({ create: true, edit: true, remove: true });

	  /* create */
	  this.on('create', function()
	  {
	  	var projectRoleWindow = new ProjectRoleWindow;

	  	projectRoleWindow.projectRoleUserTabPanel.availableUsersTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/role/getavialableusers',
	  		baseParams:{id:0}
	  		});
	  	projectRoleWindow.projectRoleUserTabPanel.membersUsersTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/role/getemptytree',
	  		baseParams:{id:0}
	  		});
	  	projectRoleWindow.projectRoleUserTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/role/getavialableroles',
	  		baseParams:{id:0}
	  		});
	  	projectRoleWindow.projectRoleUserTabPanel.childrenRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/projectadmin/role/getemptytree',
	  		baseParams:{id:0},
	  		listeners:
	  		{
	  			load: function()
	  			{
	  				projectRoleWindow.cancelButton.enable();
	  				projectRoleWindow.okButton.enable();
	  			}
	  		}
	  		});

	  	projectRoleWindow.setTitle('Создание новой роли');
	  	projectRoleWindow.projectRoleForm.roleLockStatesCombobox.setValue(0);
	  	projectRoleWindow.show();


	  });

	  /* edit */
	  this.on('edit', function()
	  {
	  	var projectRoleWindow = new ProjectRoleWindow;

	  	var reader = new Ext.data.JsonReader({},[
                       {name: 'roleId'},
                       {name: 'roleName'},
                       {name: 'roleDescription'},
                       {name: 'roleLocked'}
                    ]);

        var roleForChange = Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleSelectionModel.getSelections();


                   if(roleForChange.length != 0)
                   {
                       if(roleForChange.length == 1)
                       {
                       var msgWait = Ext.Msg.wait('Получение данных о роли...', 'Подождите', {interval:32});
                       Projectadmin.conn.request(
                        {
                            url: '/projectadmin/role/getroleproperties',
                            method: 'POST',
                            params: {'id':roleForChange[0].data.roleId},
                            success: function(response) {
                              projectRoleWindow.show();
                              projectRoleWindow.projectRoleForm.getForm().loadRecord(reader.read(response).records[0]);
                              projectRoleWindow.setTitle('Изменение свойств роли : '+roleForChange[0].data.roleName);
                              projectRoleWindow.projectRoleForm.roleLockStatesCombobox.setValue(reader.read(response).records[0].data.roleLocked)
                              msgWait.hide();
                            }
                        });

                       	projectRoleWindow.projectRoleUserTabPanel.availableUsersTree.loader = new Ext.tree.WgsTreeLoader(
					  		{
					  		dataUrl:'/projectadmin/role/getavialableusers',
					  		baseParams:{id:roleForChange[0].data.roleId}
					  		});
					  	projectRoleWindow.projectRoleUserTabPanel.membersUsersTree.loader = new Ext.tree.WgsTreeLoader(
					  		{
					  		dataUrl:'/projectadmin/role/getmemberusers',
					  		baseParams:{id:roleForChange[0].data.roleId}
					  		});
					  	projectRoleWindow.projectRoleUserTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
					  		{
					  		dataUrl:'/projectadmin/role/getavialableroles',
					  		baseParams:{id:roleForChange[0].data.roleId}
					  		});
					  	projectRoleWindow.projectRoleUserTabPanel.childrenRolesTree.loader = new Ext.tree.WgsTreeLoader(
					  		{
					  		dataUrl:'/projectadmin/role/getchildrenroles',
					  		baseParams:{id:roleForChange[0].data.roleId},
					  		listeners:
	  						{
					  			load: function()
					  			{
					  				projectRoleWindow.cancelButton.enable();
					  				projectRoleWindow.okButton.enable();
					  			}
					  		}
					  		});

                       }
                       else
	                       Ext.Msg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать одну роль для изменения.',
							buttons : Ext.Msg.OK,
							icon : Ext.MessageBox.WARNING
						});
                   }
                   else
	                   Ext.Msg.show({
						title : 'Предупреждение',
						msg : 'Необходимо выбрать одну роль для изменения.',
						buttons : Ext.Msg.OK,
						icon : Ext.MessageBox.WARNING
					});

	  });

	  /* remove */
	  this.on('remove', function()
	  {
	  	 var rolesForRemoveIds = [];
         var rolesForRemove = Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleSelectionModel.getSelections();
         var message = '';
	        if(rolesForRemove.length != 0)
	        {
	        	if(rolesForRemove.length == 1)
				message = 'Вы действительно желаете удалить выбранную роль?';
				else
				message = 'Вы действительно желаете удалить выбранные роли?';
	            Ext.MessageBox.confirm('', message,function(btn)
	            {
	              if (btn == 'yes')
	              {
	                  for(var i = 0; i < rolesForRemove.length; i++)
	                  rolesForRemoveIds[i] = rolesForRemove[i].data.roleId;

	                Ext.Ajax.request({
	                url: '/projectadmin/role/removerole',
	                method: 'POST',
	                params: {'rolesForRemoveIds[]': rolesForRemoveIds},
	                success: function() {
	                        //Ext.Msg.alert('Статус','Удаление успешно завершено');
	                        Projectadmin.projectRoleManagementPanel.projectRolesGrid.roleStore.reload();
	                        //Projectadmin.projectUserManagementPanel.projectUsersGrid.userStore.reload();

	                    }
	                });
	              }

	            });

	        }
	        else
	            Ext.Msg.show({
				title : 'Предупреждение',
				msg : 'Необходимо выбрать роль для удаления.',
				buttons : Ext.Msg.OK,
				icon : Ext.MessageBox.WARNING
			});
	});
	ProjectRolesGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* Администратор системных ролей */
ProjectRoleManagementPanel = function() {
	this.projectRolesGrid = new ProjectRolesGrid();

	ProjectRoleManagementPanel.superclass.constructor.call(this, {
		title : 'Роли',
		closable : false,
		autoScroll : true,
		layout : 'fit',
		items : [this.projectRolesGrid]
	});
}

Ext.extend(ProjectRoleManagementPanel, Ext.Panel, {});