window.onresize = function() {
   Information.SketchResize();
}
        
ViewList = function () {
    
    this.xd = Ext.data;

    this.store = new Ext.data.JsonStore({
        url: '/map/favoriteview/getgeometryproperty',
        root: 'view',
        autoLoad: true,
		fields: ['name', 'number', 'url', 'centerX', 'centerY', 'scale', 'width', 'height', 'layerview']
    });
    
    this.tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap" id="{name}">',
            '<div style="width: {width}px; height: {height}px" class="thumb"><img src="{url}" title="{name}" style="width: {width}px; height: {height}px"></div>',
            '<div id="div_{number}" align="center"><span class="x-editable">{shortName}</span></div></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );

    ViewList.superclass.constructor.call(this, {
        store: this.store,
        tpl: this.tpl,
        autoHeight: true,
        autoWidth: true,
        border: true,
        singleSelect: true,
        autoScroll : true,
        overClass:'x-view-over',
        itemSelector:'div.thumb-wrap',
        prepareData: function(data){
            data.shortName = Ext.util.Format.ellipsis(data.name, data.width*0.175); // 0.175 = 45/257 (45 - ������ ������, 257 - ������ ������)
            return data;
        }
    });
}
Ext.extend(ViewList, Ext.DataView, {
    initComponent : function() {
        
        this.on('click', function() {
            this.getSelectedRecords().each(function (record){
                this.clearSelections();
                if (record.data.scale) {
                    //window.location.href = '/map/'+record.data.number;
                	var url = '/map#'+record.data.centerX+','+record.data.centerY+'|'+record.data.scale;
                	if (record.data.layerview) {
                		url = url + '|'+record.data.layerview;
                	}
                	window.location.href = url;
                }    
            }, this)        
        }, this);
        
        //var msgWaitLoad;

        this.store.on('beforeload', function() {
           //msgWaitLoad = Ext.WgsMsg.wait('��������� �������...', '���������', {interval: 200}); 
        })
        
        this.store.on('load', function (){
            Information.SketchResize();
            //msgWaitLoad.hide();
        }, this)
        
        ViewList.superclass.initComponent.apply(this, arguments);
    }
});

FavoriteView = function() {
    this.viewList = new ViewList()
    
    FavoriteView.superclass.constructor.call(this, {
        id:'favorite-view',
        collapsible : false,
        layout : 'fit',
        autoScroll : true,
        border: false,
        style: 'padding-top: 3%',
        items: [this.viewList]
    });
}
Ext.extend(FavoriteView, Ext.Panel, {})

Information = function() {
    var conn = new Ext.data.WgsConnection();

    var favoriteView = new FavoriteView();
    return {
        init : function() {
            new Ext.Viewport({
                layout : 'column',
                style: 'padding-top: 48px;',
                items : [
                    {
                        columnWidth: 0.2,
                        border: false,
                        layout: 'fit',
                        items: new Ext.Panel({border: false, html: '&nbsp;'})
                    },
                    {
                        columnWidth: 0.65,
                        border: false,
                        items: new Ext.Panel({border: false, items: favoriteView})
                    },
                    {
                        columnWidth: 0.15,
                        border: false,
                        layout: 'fit',
                        items: new Ext.Panel({border:false, html: '&nbsp;'})
                    }
                ] 
            }).doLayout();
        },
        
        getClientWidth: function ()
        {
          return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
        },
        
        getClientHeight: function ()
        {
          return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
        },
        
        SketchResize: function () {
            var count = Information.favoriteView.viewList.store.getCount();
            if (count > 0) {
                var panelHeight = (Information.getClientHeight() - 46) * 0.85;
                var panelWidth = Information.getClientWidth() * 0.65;
                var sketchHeight = (panelHeight/3.5);
                var sketchWidth = (panelWidth/3.5);
                Information.favoriteView.viewList.store.each(function(record) {
                    record.data.width = sketchWidth;
                    record.data.height = sketchHeight;
                });
                Information.favoriteView.viewList.refresh();
            }
        },
        
        conn : conn,
        favoriteView : favoriteView
    }
}();