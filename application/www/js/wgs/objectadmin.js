Ext.override(Ext.data.Connection, {
    timeout: 360000
})
//Ext.override(Ext.data.proxy.Ajax, { timeout:360000 });
function checkFill() {
    var objectColumn = Ext.getCmp('comboImportId').getValue();
    var excelColumn = Ext.getCmp('comboImportColumn').getValue();
    var propertyId = Ext.getCmp('comboImportProperty').getValue();
    var selection = Ext.getCmp('card-0_grid').selModel.selections
    var cond = objectColumn && excelColumn && propertyId && selection.items[0]
    var state = true
    if (cond) state = false
    return state

}

function setEnabled() {
    var btn = 'importOk'
    Ext.getCmp(btn).setDisabled(checkFill())
}
Ext.override(Ext.data.Store, {
    addField: function(field) {
        field = new Ext.data.Field(field);
        this.recordType.prototype.fields.replace(field);
        if (typeof field.defaultValue != 'undefined') {
            this.each(function(r) {
                if (typeof r.data[field.name] == 'undefined') {
                    r.data[field.name] = field.defaultValue;
                }
            });
        }
    },
    removeField: function(name) {
        this.recordType.prototype.fields.removeKey(name);
        this.each(function(r) {
            delete r.data[name];
            if (r.modified) {
                delete r.modified[name];
            }
        });
    }
});
Ext.override(Ext.grid.ColumnModel, {
    addColumn: function(column, colIndex) {
        if (typeof column == 'string') {
            column = {
                header: column,
                dataIndex: column
            };
        }
        var config = this.config;
        this.config = [];
        if (typeof colIndex == 'number') {
            config.splice(colIndex, 0, column);
        } else {
            colIndex = config.push(column);
        }
        this.setConfig(config);
        return colIndex;
    },
    removeColumn: function(colIndex) {
        var config = this.config;
        this.config = [config[colIndex]];
        config.splice(colIndex, 1);
        this.setConfig(config);
    }
});
Ext.override(Ext.grid.GridPanel, {
    addColumn: function(field, column, colIndex) {
        if (!column) {
            if (field.dataIndex) {
                column = field;
                field = field.dataIndex;
            } else {
                column = field.name || field;
            }
        }
        this.store.addField(field);
        return this.colModel.addColumn(column, colIndex);
    },
    removeColumn: function(name, colIndex) {
        this.store.removeField(name);
        if (typeof colIndex != 'number') {
            colIndex = this.colModel.findColumnIndex(name);
        }
        if (colIndex >= 0) {
            this.colModel.removeColumn(colIndex);
        }
    }
});

function cardNavI(incr) {

}

function cardNav(incr) {
    var l = Ext.getCmp('exportWizard').getLayout();
    var i = l.activeItem.id.split('card-')[1];
    var next = parseInt(i, 10) + incr;
    if (next != 2) l.setActiveItem(next);
    //alert(next)

    if (window.extraCols) for (var i = 0; i < window.extraCols.length; i++)
    Ext.getCmp('card-1').removeColumn(window.extraCols[i].dataIndex)

    if (next === 1) {
        window.extraCols = [{
            header: 'objectId',
            dataIndex: 'objectId',
            hidden: false
        }]

        var selection = Ext.getCmp('card-0').selModel.getSelections()
        var propIds = []
        for (var i = 0; i < selection.length; i++) {
            propIds[propIds.length] = selection[i].data.id;
            window.extraCols[window.extraCols.length] = {
                header: selection[i].data.name,
                dataIndex: i == 0 ? 'pv' : ('pv' + (i + 1))
            }
        }
        loadMask = new Ext.LoadMask(Ext.getCmp('card-1').getEl(), {
            msg: 'Загрузка...'
        });
        loadMask.show();
        Ext.Ajax.request({
            url: 'objectadmin/search/getresult',
            success: function(response, options) {
                var objects = Ext.util.JSON.decode(response.responseText)
                var objIds = [];
                for (var i = 0; i < objects.rows.length; i++)
                if (objects.rows[i].objectId) objIds[objIds.length] = objects.rows[i].objectId
                //console.log('Length is: ', objIds.length)

                Ext.getCmp('card-1').store.baseParams = {
                    'objectIds[]': objIds,
                    'propIds[]': propIds,
                    limit: 36
                }
                window.OBJ_IDS = objIds
                window.PROP_IDS = propIds
                for (var i = 0; i < window.extraCols.length; i++)
                Ext.getCmp('card-1').addColumn({
                    name: window.extraCols[i].dataIndex
                }, window.extraCols[i])

                delete Ext.getCmp('card-1').getStore().reader.ef;
                Ext.getCmp('card-1').getStore().reader.buildExtractors();
                Ext.getCmp('card-1').getStore().load();
                Ext.getCmp('card-next').setText('Экспорт в формат <b>MS Excel</b>')
                //if(Ext.getCmp('card-next'))
                /*Ext.getCmp('exportWizard').getBottomToolbar().remove(Ext.getCmp('card-next'))					
					
					if(!Ext.getCmp('exportButton')){
						var exportButton = new Ext.ux.Exporter.Button({ 
							component: Ext.getCmp('card-1'), 
							id		 : 'exportButton',
							text     : "<span style='margin-top:5px; color:black; text-decoration:none'>Экспорт в формат <b>MS Excel</b></span>" 
						}); 
						Ext.getCmp('exportWizard').getBottomToolbar().add(exportButton);					
						Ext.getCmp('exportWizard').getBottomToolbar().fireEvent('resize')
						
					}else
					Ext.getCmp('exportButton').setDisabled(false);									*/
            },
            failure: function() {
                // Сообщение об ошибке
                loadMask.hide()
                Ext.MessageBox.alert('', 'Не удалось получить данные для экспорта. Обратитесь к администратору системы');
            },
            params: { // параметры передаем
                'featureTypes[]': window.type_id,
                mode: 1,
                limit: -1,
                docSearch: false
            },
            method: 'POST'
        });
    } else if (next == 2) {
        var names = [];
        loadMask = new Ext.LoadMask(Ext.getCmp('card-1').getEl(), {
            msg: 'Пожалуйста, подождите. Выполняется экспорт данных...'
        });
        loadMask.show();

        for (var i in window.extraCols)
        if (window.extraCols[i].header) names[names.length] = window.extraCols[i].header
        Ext.Ajax.request({
            url: '/map/multipleFilling/GetExcelValues',
            method: 'POST',
            params: {
                'objectIds[]': window.OBJ_IDS,
                'propIds[]': window.PROP_IDS,
                'names[]': names
            },
            success: function(response) {
                loadMask.hide();
                window.open("/map/report/GetExportFile/" + response.responseText, "excel");
                Ext.getCmp('exportWizard').close();
            },
            failure: function() {
                loadMask.hide();
                Ext.MessageBox.alert('', 'Экспорт не может быть выполнен (ошибка формирования документа Excel).');
            }
        })
    } else {
        Ext.getCmp('card-next').setText('Далее &raquo;')

        /*Ext.getCmp('exportWizard').getBottomToolbar().add(new Ext.Button({
					id: 'card-next',
					text: 'Далее &raquo;',
					handler: function(){cardNav(1)}
    			})
			)					
		if(Ext.getCmp('exportButton')){
			Ext.getCmp('exportButton').setDisabled(true);
			Ext.getCmp('exportWizard').getBottomToolbar().fireEvent('resize')
		}*/

    }
    Ext.getCmp('card-prev').setDisabled(next === 0);

}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ОКНО ПРИВЯЗКИ ХАРАКТЕРИСТИКИ К СВЯЗИ*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BindPropByRelatFormWindow = function(owner, relation_Id) {
    this.owner = owner;
    this.relation_Id = relation_Id;

    var self = this;

    this.projectSelectionModel = new Ext.grid.CheckboxSelectionModel({
        singleSelect: false
    });
    this.storeJS = new Ext.data.GroupingStore({
        autoLoad: true,
        url: '/objectadmin/GetListPropertyNotByRelation',
        reader: new Ext.data.JsonReader({}, [{
            name: 'id'
        }, {
            name: 'name'
        }, {
            name: 'groupid'
        }, {
            name: 'groupname'
        }, {
            name: 'valuetypeid'
        }, {
            name: 'valuetypename'
        }, {
            name: 'placdate'
        }]),
        sortInfo: {
            field: 'groupname',
            direction: "ASC"
        },
        groupField: 'groupname',
        baseParams: {
            relationId: self.relation_Id
        }
    });

    BindPropByRelatFormWindow.superclass.constructor.call(this, {
        title: 'Привязка характеристик',
        layout: 'fit',
        resizable: true,
        height: 600,
        width: 600,
        modal: true,
        items: [new Ext.Panel({
            frame: true,
            layout: 'fit',
            items: [self.PropFildSet = new Ext.form.FieldSet({
                title: 'Список характеристик',
                layout: 'fit',
                items: [
                self.PropGrid = new Ext.grid.GridPanel({
                    sm: this.projectSelectionModel,
                    stripeRows: true,
                    view: new Ext.grid.GroupingView({
                        groupTextTpl: '{text} ( Количество: {[values.rs.length]} )',
                        enableGroupingMenu: false,
                        forceFit: true
                    }),
                    store: self.storeJS,
                    autoScroll: true,
                    columns: [
                    this.projectSelectionModel, {
                        id: 'propId',
                        dataIndex: 'id',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'groupId',
                        dataIndex: 'groupid',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'valueTypeId',
                        dataIndex: 'valuetypeid',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'propName',
                        dataIndex: 'name',
                        header: "Наименование",
                        sortable: true
                    }, {
                        id: 'groupName',
                        dataIndex: 'groupname',
                        header: "Наименование группы",
                        sortable: true
                    }, {
                        id: 'valueTypeName',
                        dataIndex: 'valuetypename',
                        header: "Тип значения",
                        sortable: true
                    }, {
                        id: 'placDate',
                        dataIndex: 'placdate',
                        header: "Дата создания",
                        sortable: true
                    }]
                })]
            })]
        })]
    });
    this.okButton = this.addButton({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Закрыть',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(BindPropByRelatFormWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            self.close();
        }, this);
        this.on('ok', function() {
            var selectedProperies = self.PropGrid.getSelectionModel().getSelections();
            if (selectedProperies.length != 0) {
                var propertiesForBind = [];
                for (var i = 0; i < selectedProperies.length; i++) {
                    propertiesForBind[propertiesForBind.length] = selectedProperies[i].data.id;
                }
                var msgWait = Ext.Msg.wait('Привязка характеристик к связи...', 'Подождите', {
                    interval: 50
                });
                Objectadmin.conn.request({
                    url: '/objectadmin/AddPropetyByRelation',
                    method: 'POST',
                    params: {
                        relationId: self.relation_Id,
                        'propertiesId[]': propertiesForBind
                    },
                    success: function() {
                        if (self.owner.PropGrid) {
                            self.owner.propStoreJS.reload();
                            msgWait.hide();
                            self.storeJS.reload();
                            Objectadmin.objectTreePanel.refreshNode();
                        }
                    }
                });
            }
        }, this);
        BindPropByRelatFormWindow.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-ФОРМА/ОКНО РЕДАКТИРОВАНИЕ СВЯЗЕЙ АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
RelationAttrFormWindow = function(owner, type_Id, relation_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.relation_Id = relation_Id

    this.selectedRecords = new Array();
    this.loadAttrStoreJS = false;
    this.loadSelectStoreJS = false;
    var self = this;
    this.bindPropButton = new Ext.Button({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('bindProp')
        },
        scope: this
    });
    this.unBindPropButton = new Ext.Button({
        text: 'Отвязать',
        handler: function() {
            this.fireEvent('unBindProp')
        },
        scope: this
    });
    this.projectSelectionModel = new Ext.grid.CheckboxSelectionModel({
        singleSelect: true
    });
    this.selectAttrtype = function() {
        var attr_id = 0;
        var i = 0;
        while (self.selectStoreJS.getAt(i) != null) {
            attr_id = self.selectStoreJS.getAt(i).data.childObjectTypeId;
            self.selectedRecords[i] = self.attrStoreJS.getAt(self.attrStoreJS.findBy(function(record, id) {
                if (record.data.id == attr_id) {
                    return true;
                }
            }));
            i = i + 1;
        }
    };
    this.attrStoreJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'shortname', 'adddate', 'note', 'label'],
        url: '/objectadmin/GetAttrTypeList',
        baseParams: {
            typeId: self.type_Id
        }
    });
    this.propStoreJS = new Ext.data.GroupingStore({
        autoLoad: true,
        reader: new Ext.data.JsonReader({}, [{
            name: 'id'
        }, {
            name: 'name'
        }, {
            name: 'groupid'
        }, {
            name: 'groupname'
        }, {
            name: 'valuetypeid'
        }, {
            name: 'valuetypename'
        }, {
            name: 'placdate'
        }, {
            name: 'priority'
        }]),
        sortInfo: {
            field: 'groupname',
            direction: "ASC"
        },
        groupField: 'groupname',
        remoteGroup: true,
        url: '/objectadmin/GetListPropertyByRelation',
        baseParams: {
            relationId: self.relation_Id
        }
    });
    this.selectStoreJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['objTypeRelationId', 'parentObjectTypeId', 'childObjectTypeId'],
        url: '/objectadmin/GetObjTypeRelationByRelation',
        baseParams: {
            relationId: self.relation_Id
        }
    });
    self.PropGrid = new Ext.grid.GridPanel({
        view: new Ext.grid.GroupingView({
            groupTextTpl: '{text} ( Количество: {[values.rs.length]} )',
            enableGroupingMenu: false,
            forceFit: true
        }),
        frame: true,
        store: this.propStoreJS,
        animCollapse: false,
        iconCls: 'icon-grid',
        autoScroll: true,
        enableHdMenu: false,
        columns: [{
            id: 'priority',
            dataIndex: 'priority',
            hidden: true
        }, {
            id: 'propId',
            dataIndex: 'id',
            hidden: true
        }, {
            id: 'groupId',
            dataIndex: 'groupid',
            hidden: true
        }, {
            id: 'valueTypeId',
            dataIndex: 'valuetypeid',
            hidden: true
        }, {
            id: 'propName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }, {
            id: 'groupName',
            dataIndex: 'groupname',
            header: "Наименование группы",
            sortable: true
        }, {
            id: 'valueTypeName',
            dataIndex: 'valuetypename',
            header: "Тип значения",
            sortable: true
        }, {
            id: 'placDate',
            dataIndex: 'placdate',
            hidden: true
        }],
        tbar: [this.bindPropButton, this.unBindPropButton]
    });
    self.AttrGrid = new Ext.grid.GridPanel({
        sm: this.projectSelectionModel,
        stripeRows: true,
        viewConfig: {
            forceFit: true
        },
        store: self.attrStoreJS,
        frame: true,
        autoScroll: true,
        enableHdMenu: false,
        columns: [
        this.projectSelectionModel, {
            id: 'attrTypeId',
            dataIndex: 'id',
            hidden: true
        }, {
            id: 'attrTypeName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }, {
            id: 'addDate',
            dataIndex: 'adddate',
            hidden: true
        }, {
            id: 'attrTypeShortName',
            dataIndex: 'note',
            hidden: true
        }, {
            id: 'label',
            dataIndex: 'label',
            hidden: true
        }]
    });

    if (self.relation_Id == -1) {
        this.bindPropButton.disable();
        this.unBindPropButton.disable();
    }

    RelationAttrFormWindow.superclass.constructor.call(this, {
        title: 'Привязка описания к описанию',
        plain: true,
        layout: 'fit',
        resizable: false,
        height: 600,
        width: 800,
        modal: true,
        items: [new Ext.Panel({
            frame: true,
            closable: true,
            anchor: '100%',
            layout: 'border',
            items: [
            self.AttrFildSet = new Ext.form.FieldSet({
                title: 'Описания',
                autoScroll: true,
                height: 200,
                margins: '0 5 0 0',
                region: 'west',
                width: 220,
                layout: 'fit',
                items: [self.AttrGrid]
            }), {
                layout: 'border',
                region: 'center',
                items: [
                self.formRelation = new Ext.form.WgsFormPanel({
                    border: false,
                    region: 'north',
                    height: 120,
                    labelWidth: 130,
                    items: [new Ext.form.FieldSet({
                        title: 'Настройки связи',
                        autoHeight: true,
                        items: [
                        this.RelationId = new Ext.form.Hidden({
                            name: 'relationId',
                            hidden: true,
                            labelSeparator: ''
                        }),
                        this.RelationName = new Ext.form.TextField({
                            name: 'relationName',
                            fieldLabel: 'Наименование связи',
                            anchor: '100%',
                            allowBlank: false
                        }),
                        /*this.RelationType = new Ext.ux.RadioGroup(
                                                    {
                                                        xtype:'ux-radiogroup',
                                                        fieldLabel:'Тип связи',
                                                        name:'relationType',
                                                        radios:[
                                                        {
                                                            xtype: 'radio',
                                                            value:1,
                                                            name:'relationType',
                                                            boxLabel:'Один к одному',
                                                            checked:true,
                                                            disabled: false
                                                        },{
                                                            xtype: 'radio',
                                                            value:2,
                                                            name:'relationType',
                                                            boxLabel:'Один ко многим',
                                                            disabled: false
                                                        }]
                                                    })*/

                        this.RelationType = new Ext.form.RadioGroup({
                            fieldLabel: 'Тип связи',
                            //name:'relationType',
                            columns: 1,
                            items: [{
                                inputValue: 1,
                                name: 'relationType',
                                boxLabel: 'Один к одному',
                                checked: true,
                                listeners: {
                                    'check': function(Checkbox, checked) {
                                        if (checked) {
                                            self.AttrGrid.getSelectionModel().clearSelections();
                                            self.AttrGrid.getSelectionModel().singleSelect = true;
                                        }
                                    }
                                }
                            }, {
                                inputValue: 2,
                                name: 'relationType',
                                boxLabel: 'Один ко многим',
                                listeners: {
                                    'check': function(Checkbox, checked) {
                                        if (checked) {
                                            self.AttrGrid.getSelectionModel().clearSelections();
                                            self.AttrGrid.getSelectionModel().singleSelect = false;
                                        }
                                    }
                                }
                            }]
                        })]
                    })]
                }), new Ext.form.FieldSet({
                    title: 'Характеристики связи',
                    region: 'center',
                    layout: 'fit',
                    items: [self.PropGrid]
                })]
            }]
        })]
    });

    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        disabled: true,
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Закрыть',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(RelationAttrFormWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        var AddRemoveAttrType = function() {
            var relationsForAdd = [];
            var relationsForRemove = [];
            var msgWait = Ext.Msg.wait('Сохранение изменений...', 'Подождите', {
                interval: 50
            });
            self.attrStoreJS.each(function(record) {
                if (self.AttrGrid.getSelectionModel().isSelected(record)) {
                    relationsForAdd[relationsForAdd.length] = record.data.id;
                } else {
                    relationsForRemove[relationsForRemove.length] = record.data.id;
                }
            });

            var params = {
                parentObjectTypeId: self.type_Id,
                relationId: self.relation_Id
            };
            if (relationsForAdd.length) {
                params['addChildObjectTypeId[]'] = relationsForAdd;
            }
            if (relationsForRemove.length) {
                params['removeChildObjectTypeId[]'] = relationsForRemove;
            }
            Objectadmin.conn.request({
                url: '/objectadmin/AddRemoveObjTypeRelationByData',
                method: 'POST',
                params: params
                /*,
                success: function()
                {
					if(Objectadmin.objectTreePanel) {
                        Objectadmin.objectTreePanel.refreshNode();
                    }
            	}*/
,
                success: function() {
                    self.owner.attrTypeRelationListGrid.storeJS.reload();
                }
            });
            msgWait.hide();
            //self.owner.attrTypeRelationListGrid.storeJS.reload();
        };
        var CreateRelation = function() {
            var msgWait = Ext.Msg.wait('Создание связи...', 'Подождите', {
                interval: 50
            });
            self.formRelation.getForm().submit({
                failure: function(form, action) {
                    if (action.response) {
                        if (action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.relation_Id = action.response.responseText;
                                self.formRelation.getForm().setValues({
                                    relationId: self.relation_Id
                                });
                                self.formRelation.getForm().url = '/objectadmin/SetRelation';
                                self.setTitle('Редактирование связи "' + self.formRelation.getForm().getValues(false).relationName + '" между атрибутивными описаниями');
                                self.propStoreJS.baseParams.relationId = self.relation_Id;
                                AddRemoveAttrType();
                                self.selectStoreJS.baseParams.relationId = self.relation_Id;
                                self.bindPropButton.enable();
                                self.unBindPropButton.enable();
                            } else {
                                Ext.Msg.show({
                                    title: 'Ошибка',
                                    msg: 'Создать связь не удалось.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        }
                    }
                    self.RelationType.setDisabled(true);
                    self.okButton.setText('Применить');
                    msgWait.hide();
                }
            });
        };
        var EditRelation = function() {
            var msgWait = Ext.Msg.wait('Сохранение параметров...', 'Подождите', {
                interval: 50
            });
            self.formRelation.getForm().submit({
                success: function(form, action) {
                    self.setTitle('Редактирование связи "' + self.formRelation.getForm().getValues(false).relationName + '" между атрибутивными описаниями');
                    AddRemoveAttrType();
                    Objectadmin.objectTreePanel.refreshNode();
                },
                failure: function(form, action) {
                    msgWait.hide();
                    Ext.Msg.show({
                        title: 'Ошибка',
                        msg: 'Сохранить не удалось.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
            });
        };
        this.on('cancel', function() {
            self.close();
        }, this);

        this.on('ok', function() {
            if (self.relation_Id == -1) {
                if (self.AttrGrid.getSelectionModel().getSelections().length == 0) {
                    Ext.Msg.show({
                        title: 'Ошибка',
                        msg: 'Хотя бы одно атрибутивное описание должно быть связано данной связью.\n' + 'Иначе создать связь невозможно!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                } else {
                    CreateRelation();
                }
            } else {
                if (self.AttrGrid.getSelectionModel().getSelections().length == 0) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Хотя бы одно атрибутивное описание должно быть связано данной связью,\n' + 'иначе связь будет удалена!\n' + 'Вы уверены, что хотите продолжить?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                EditRelation();
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.QUESTION
                    });
                } else {
                    EditRelation();
                }
            }
        }, this);
        this.attrStoreJS.on('load', function(Store, records, options) {
            this.okButton.enable();
            self.loadAttrStoreJS = true;
            if ((self.loadSelectStoreJS) && (self.loadAttrStoreJS)) {
                self.selectAttrtype();
                self.AttrGrid.getSelectionModel().selectRecords(self.selectedRecords);
            }
        }, this);
        this.selectStoreJS.on('load', function(Store, records, options) {
            self.loadSelectStoreJS = true;
            if ((self.loadSelectStoreJS) && (self.loadAttrStoreJS)) {
                self.selectAttrtype();
                self.AttrGrid.getSelectionModel().selectRecords(self.selectedRecords);
            }
        });

        this.on('bindProp', function() {
            self.bindPropFormWindow = new BindPropByRelatFormWindow(self, self.relation_Id);
            self.bindPropFormWindow.setTitle('Привязка характеристик к связи');
            self.bindPropFormWindow.show();
            self.bindPropFormWindow.PropFildSet.doLayout();
        });

        this.on('unBindProp', function() {
            var listSelectedProperties = self.PropGrid.getSelectionModel().getSelections();
            if (listSelectedProperties.length != 0) {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'У некоторых объектов связи данная характеристика может быть заполнена.\n' + 'Ее отвязка может привети к потере данных!\n' + 'Вы уверены, что хотите продолжить отвязку?',
                    fn: function(btn, text) {
                        if (btn == 'yes') {
                            var msgWait = Ext.Msg.wait('Отвязка характеристик от связи...', 'Подождите', {
                                interval: 50
                            });
                            var propertiesForBind = [];
                            for (var i = 0; i < listSelectedProperties.length; i++) {
                                propertiesForBind[propertiesForBind.length] = listSelectedProperties[i].data.id;
                            }

                            Objectadmin.conn.request({
                                url: '/objectadmin/RemovePropetyByRelation',
                                method: 'POST',
                                params: {
                                    relationId: self.relation_Id,
                                    'propertiesId[]': propertiesForBind
                                },
                                success: function() {
                                    self.propStoreJS.removeAll();
                                    self.propStoreJS.reload();
                                    msgWait.hide();
                                    Objectadmin.objectTreePanel.refreshNode();
                                },
                                failure: function() {
                                    self.propStoreJS.removeAll();
                                    self.propStoreJS.reload();
                                    Ext.Msg.show({
                                        title: 'Ошибка',
                                        msg: 'Отвязка характеристик не удалась.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                    msgWait.hide();
                                }
                            });

                        }
                    },
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.MessageBox.QUESTION
                });
            }
        });
        RelationAttrFormWindow.superclass.initComponent.apply(this, arguments);
    }

});

//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ФОРМА/ОКНО ОТВЯЗКИ АТРИБУТИВНОГО ОПИСАНИЯ ОТ СЛОЯ-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
UnBindAttrTypeFeatureLayerWindow = function(owner, type_id, layer_Id) {
    this.owner = owner;
    this.type_id = type_id;
    this.layer_Id = layer_Id;

    UnBindAttrTypeFeatureLayerWindow.superclass.constructor.call(this, {
        title: 'Отвязка описания',
        layout: 'fit',
        resizable: false,
        height: 150,
        width: 400,
        modal: true,
        items: [
        new Ext.form.WgsFormPanel({
            frame: true,
            items: [{
                xtype: 'textfield',
                hidden: true,
                labelSeparator: ''
            },
            this.removeObjectCheck = new Ext.form.Checkbox({
                boxLabel: 'Удалить все привязанные атрибутивные объекты',
                hideLabel: true
            })]
        })]
    });
    this.okButton = this.addButton({
        text: 'Отвязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(UnBindAttrTypeFeatureLayerWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            self.destroy();
        }, this);
        this.on('ok', function() {
            var msgWait = Ext.Msg.wait('Отвязка атрибутивного описания...', 'Подождите', {
                interval: 600
            });
            Objectadmin.conn.request({
                url: '/objectadmin/RemoveRelationAtt',
                method: 'POST',
                params: {
                    layerid: self.layer_Id,
                    typeid: self.type_id,
                    removeObject: self.removeObjectCheck.getValue()
                },
                success: function() {
                    self.owner.featureLayerForAttrTypeListGrid.storeJS.reload();
                    msgWait.hide();
                    self.close();
                },
                failure: function() {
                    self.owner.featureLayerForAttrTypeListGrid.storeJS.reload();
                    msgWait.hide();
                    self.close();
                }
            });

        }, this);
        UnBindAttrTypeFeatureLayerWindow.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ФОРМА/ОКНО ПРИВЯЗКИ АТРИБУТИВНОГО ОПИСАНИЯ К СЛОЯМ-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BindAttrTypeFeatureLayerWindow = function(owner, typeId, layerId) {
    this.owner = owner;
    this.typeId = typeId;
    this.layerId = layerId;
    var self = this;
    BindAttrTypeFeatureLayerWindow.superclass.constructor.call(this, {
        title: 'Привязка атрибутивного описания к слоям',
        layout: 'fit',
        resizable: true,
        autoHeight: true,
        width: 520,
        modal: true,
        items: [
        new Ext.form.FormPanel({
            autoScroll: true,
            autoHeight: true,
            frame: true,
            items: [
            this.isCurrentCheck = new Ext.form.Checkbox({
                boxLabel: 'Назначить данное описание основным',
                hideLabel: true,
                labelSeparator: ''
            }),
            this.bindOldObjectCheck = new Ext.form.Checkbox({
                boxLabel: 'Привязать имеющиеся атрибутивные объеты к подходящим геометрическим',
                hideLabel: true,
                labelSeparator: ''
            }),
            this.removeOldObjectCheck = new Ext.form.Checkbox({
                boxLabel: 'Удалить все атрибутивные объекты не привязанные к геометрическим',
                hideLabel: true,
                labelSeparator: ''
            })]
        })]
    });
    this.okButton = this.addButton({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(BindAttrTypeFeatureLayerWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            this.close();
        }, this);
        this.on('ok', function() {
            self.BindAttrType = function(unBindDate) {
                var msgWait = Ext.Msg.wait('Привязка атрибутивного описания...', 'Подождите', {
                    interval: 800
                });
                Objectadmin.conn.request({
                    url: '/objectadmin/AddRelationAtt',
                    method: 'POST',
                    params: {
                        layerid: self.layerId,
                        typeid: self.typeId,
                        iscurrent: self.isCurrentCheck.getValue(),
                        unBindDate: unBindDate,
                        bindOldObject: self.bindOldObjectCheck.getValue(),
                        removeOldObject: self.removeOldObjectCheck.getValue()
                    },
                    success: function() {
                        self.owner.featureLayerForAttrTypeListGrid.storeJS.reload();
                        msgWait.hide();
                        self.close();
                    },
                    scope: this
                });
            };
            if (self.bindOldObjectCheck.getValue()) {
                self.unBindDateStoreJS = new Ext.data.WgsJsonStore({
                    fields: ['unBindDate'],
                    url: '/objectadmin/GetListUnBindObjectDateByType',
                    baseParams: {
                        layerid: self.layerId,
                        typeid: self.typeId
                    }
                });

                self.unBindDateStoreJS.load({

                    callback: function(records) {
                        if (!self.choiseUnBindDateWindowShow) {
                            if (records.length > 0) {
                                self.choiseUnBindDateWindowShow = true;
                                self.choiseUnBindDateWindow = new ChoiseUnBindDateWindow(self, self.typeId, self.layerId);
                                self.choiseUnBindDateWindow.show();
                            } else {
                                self.BindAttrType(null);
                            }
                        }
                    }
                });
            } else {
                self.BindAttrType(null);
            }
        }, this);
        BindAttrTypeFeatureLayerWindow.superclass.initComponent.apply(this, arguments);
    }
});



//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-GRID СПИСКА СЛОЕВ ДЛЯ АТРИБУТИВНОГО ОПИСАНИЯ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerForAttrTypeListGrid = function(owner, parentType_Id) {
    this.owner = owner;
    this.parentType_Id = parentType_Id;

    //--------Массив со списком атрибутивных описаний----
    var self = this;
    this.storeJS = new Ext.data.GroupingStore({
        autoLoad: true,
        reader: new Ext.data.JsonReader({}, [{
            name: 'id'
        }, {
            name: 'name'
        }, {
            name: 'groupid'
        }, {
            name: 'groupname'
        }, {
            name: 'checked'
        }]),
        sortInfo: {
            field: 'groupname',
            direction: "ASC"
        },
        groupField: 'groupname',
        remoteGroup: true,
        url: '/objectadmin/getfeaturelayerlistforattrtype',
        baseParams: {
            parentObjTypeId: self.parentType_Id
        }
    });

    this.rowActions = new Ext.ux.grid.RowActions({
        actions: [{
            iconCls: 'icon-add',
            tooltip: 'Показать документ'
        }]
    });

    this.checkColumn = new Ext.grid.CheckColumn({
        header: 'Привязан',
        align: 'center',
        dataIndex: 'checked'
    });

    this.setParentTypeId = function(_parentType_Id) {
        self.parentType_Id = _parentType_Id;
    }
    this.featureLayerCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel({});
    this.rowSelectionModel = new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    //-------Конструктор грида-----------
    FeatureLayerForAttrTypeListGrid.superclass.constructor.call(this, {
        view: new Ext.grid.GroupingView({
            groupTextTpl: '{text}',
            enableGroupingMenu: false,
            forceFit: true
        }),
        sm: this.rowSelectionModel,
        store: this.storeJS,
        autoScroll: true,
        stripeRows: true,
        columns: [{
            id: 'id',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'name',
            header: "Наименование",
            sortable: true,
            dataIndex: 'name'
        },
        this.checkColumn, {
            id: 'groupid',
            dataIndex: 'groupid',
            hideable: false,
            hidden: true
        }, {
            id: 'groupname',
            header: "Группа слоев",
            dataIndex: 'groupname',
            hideable: false,
            hidden: true
        }],
        plugins: [ /*this.checkColumn, */ this.rowActions]
    });
}
Ext.extend(FeatureLayerForAttrTypeListGrid, Ext.grid.EditorGridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);

        this.storeJS.on('update', function() {
            this.owner.bindButton.enable();
            this.owner.rejectChangesButton.enable();
        }, this);

        this.rowSelectionModel.on('rowselect', function(sm, index, record) {
            if (this.parentType_Id != -1) {
                if (record.get('checked') == 1) {
                    this.owner.bindButton.disable();
                    this.owner.unbindButton.enable();
                } else {
                    this.owner.bindButton.enable();
                    this.owner.unbindButton.disable();
                }
            }
        }, this);

        FeatureLayerForAttrTypeListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ПАНЕЛЬ СПИСКА СЛОЕВ ДЛЯ АТРИБУТИВНОГО ОПИСАНИЯ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerForAttrTypeListPanel = function(owner, parentType_Id) {
    this.owner = owner;
    this.parentType_Id = parentType_Id;

    this.featureLayerForAttrTypeListGrid = new FeatureLayerForAttrTypeListGrid(this, this.parentType_Id);
    //this.featureLayerForAttrTypeListGrid.show();
    //--------Кнопки ToolBar'а----------

    this.bindButton = new Ext.Button({
        text: 'Привязать',
        disabled: true,
        handler: function() {
            this.fireEvent('createRelation')
        },
        scope: this
    });
    this.unbindButton = new Ext.Button({
        text: 'Отвязать',
        disabled: true,
        handler: function() {
            this.fireEvent('removeRelation')
        },
        scope: this
    });

    //--------Панель----------
    FeatureLayerForAttrTypeListPanel.superclass.constructor.call(this, {
        title: 'Список атрибутивных описаний',
        layout: 'fit',
        tbar: [this.bindButton, ' ', '-', ' ', this.unbindButton /*,' ','-',' ',this.selectAllButton,' ','-',' ',this.deselectAllButton,' ','-',' ',this.rejectChangesButton */ ],
        items: [this.featureLayerForAttrTypeListGrid]
    });
}
Ext.extend(FeatureLayerForAttrTypeListPanel, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            createRelation: true,
            removeRelation: true
        });
        var self = this;

        this.on('createRelation', function() {
            var record = this.featureLayerForAttrTypeListGrid.rowSelectionModel.getSelected();
            var bindWindow = new BindAttrTypeFeatureLayerWindow(this, this.parentType_Id, record.get('id'));
            bindWindow.show();

        }, this);
        this.on('removeRelation', function() {
            var record = this.featureLayerForAttrTypeListGrid.rowSelectionModel.getSelected();
            var unbindWindow = new UnBindAttrTypeFeatureLayerWindow(this, this.parentType_Id, record.get('id'));
            unbindWindow.show();

        });
        FeatureLayerForAttrTypeListPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************


//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-GRID СПИСКА СВЯЗЕЙ АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
AttrTypeRelationListGrid = function(owner, parentType_Id) {
    this.owner = owner;
    this.parentType_Id = parentType_Id;

    //--------Массив со списком атрибутивных описаний----
    var self = this;
    this.storeJS = new Ext.data.GroupingStore({
        autoLoad: true,
        reader: new Ext.data.JsonReader({}, [{
            name: 'objTypeRelationId'
        }, {
            name: 'parentObjectTypeId'
        }, {
            name: 'childObjectTypeId'
        }, {
            name: 'relationId'
        }, {
            name: 'parentName'
        }, {
            name: 'valuetypeid'
        }, {
            name: 'childName'
        }, {
            name: 'relationName'
        }, {
            name: 'relationTypeName'
        }, {
            name: 'relationType'
        }]),
        sortInfo: {
            field: 'relationName',
            direction: "ASC"
        },
        groupField: 'relationName',
        remoteGroup: true,
        url: '/objectadmin/GetObjTypeRelation',
        baseParams: {
            parentObjTypeId: self.parentType_Id
        }
    });

    this.setParentTypeId = function(_parentType_Id) {
        self.parentType_Id = _parentType_Id;
    }
    //-------Конструктор грида-----------
    AttrTypeRelationListGrid.superclass.constructor.call(this, {
        view: new Ext.grid.GroupingView({
            groupTextTpl: '{text} ( Количество: {[values.rs.length]} )',
            enableGroupingMenu: true,
            forceFit: true
        }),
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'objTypeRelationId',
            dataIndex: 'objTypeRelationId',
            hideable: false,
            hidden: true
        }, {
            id: 'parentObjectTypeId',
            dataIndex: 'parentObjectTypeId',
            hideable: false,
            hidden: true
        }, {
            id: 'childObjectTypeId',
            dataIndex: 'childObjectTypeId',
            hideable: false,
            hidden: true
        }, {
            id: 'relationId',
            dataIndex: 'relationId',
            hideable: false,
            hidden: true
        }, {
            id: 'relationType',
            dataIndex: 'relationType',
            hideable: false,
            hidden: true
        }, {
            id: 'parentName',
            dataIndex: 'parentName',
            hideable: false,
            hidden: true
        }, {
            id: 'relationName',
            dataIndex: 'relationName',
            header: "Наименование связи",
            sortable: true
        }, {
            id: 'relationTypeName',
            dataIndex: 'relationTypeName',
            header: "Тип связи",
            sortable: true
        }, {
            id: 'childName',
            dataIndex: 'childName',
            header: "Наименование дочернего типа",
            sortable: true
        }]
    });
}
Ext.extend(AttrTypeRelationListGrid, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);
        AttrTypeRelationListGrid.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ПАНЕЛЬ СПИСКА СВЯЗЕЙ АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
AttrTypeRelationListPanel = function(owner, parentType_Id) {
    this.owner = owner;
    this.parentType_Id = parentType_Id;

    this.attrTypeRelationListGrid = new AttrTypeRelationListGrid(this, this.parentType_Id);
    //--------Кнопки ToolBar'а----------
    this.createButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('createRelation')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('editRelation')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('removeRelation')
        },
        scope: this
    });

    //--------Панель----------
    AttrTypeRelationListPanel.superclass.constructor.call(this, {
        title: 'Список атрибутивных описаний',
        layout: 'fit',
        tbar: [this.createButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.attrTypeRelationListGrid]
    });
}
Ext.extend(AttrTypeRelationListPanel, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            createRelation: true,
            editRelation: true,
            removeRelation: true
        });
        var self = this;

        this.on('beforeshow ', function() {
            if (self.layer_Id == null) {
                self.CurrentButton.disable();
                self.UnCurrentButton.disable();
                self.BindButton.disable();
                self.unBindButton.disable();
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showAttrTyperListPanel = false;
        }, this);
        this.on('createRelation', function() {
            this.relationAttrFormWindow = new RelationAttrFormWindow(self, self.parentType_Id, -1);
            self.relationAttrFormWindow.setTitle('Создание связи между атрибутивными описаниями');
            self.relationAttrFormWindow.formRelation.getForm().url = '/objectadmin/AddRelation';
            self.relationAttrFormWindow.show();
        }, this);
        this.on('editRelation', function() {
            var relationForEdit = this.attrTypeRelationListGrid.getSelectionModel().getSelections();
            if (relationForEdit.length == 1) {
                this.relationAttrFormWindow = new RelationAttrFormWindow(self, self.parentType_Id, relationForEdit[0].data.relationId);
                self.relationAttrFormWindow.setTitle('Редактирование связи "' + relationForEdit[0].data.relationName + '" между атрибутивными описаниями');
                self.relationAttrFormWindow.show();
                self.relationAttrFormWindow.okButton.setText('Применить');
                self.relationAttrFormWindow.formRelation.getForm().url = '/objectadmin/SetRelation';
                self.relationAttrFormWindow.formRelation.getForm().setValues({
                    relationName: relationForEdit[0].data.relationName,
                    relationType: relationForEdit[0].data.relationType,
                    relationId: relationForEdit[0].data.relationId
                });
                self.relationAttrFormWindow.RelationType.setDisabled(true);
            } else {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо выделить одну связь.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            }
        }, this);
        this.on('removeRelation', function() {
            Ext.Msg.show({
                title: 'Предупреждение',
                msg: 'Вы уверены, что хотите удалить выбранные связи?',
                fn: function(btn, text) {
                    if (btn == 'yes') {
                        var objTypeRelationForRemove = [];
                        var recordsRelationForRemove = self.attrTypeRelationListGrid.getSelectionModel().getSelections();
                        if (recordsRelationForRemove.length != 0) {
                            var msgWait = Ext.Msg.wait('Удаление связи(ей)...', 'Подождите', {
                                interval: 50
                            });
                            for (var i = 0; i < recordsRelationForRemove.length; i++) {
                                objTypeRelationForRemove[objTypeRelationForRemove.length] = recordsRelationForRemove[i].data.objTypeRelationId;
                            }
                            Objectadmin.conn.request({
                                url: '/objectadmin/RemoveObjTypeRelation',
                                method: 'POST',
                                params: {
                                    'objTypeRelationId[]': objTypeRelationForRemove
                                },
                                success: function() {
                                    Objectadmin.objectTreePanel.refreshNode();
                                }
                            });
                            msgWait.hide();
                            self.attrTypeRelationListGrid.storeJS.reload();
                        }
                    }
                },
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.QUESTION
            });
        });
        AttrTypeRelationListPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-GRID СПИСКА АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
AttrTypeListGrid = function(owner, layer_Id) {
    this.owner = owner;
    this.layer_Id = layer_Id;

    //--------Массив со списком атрибутивных описаний----
    var self = this;
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'nameTag', 'shortname', 'adddate', 'note', 'label'],
        url: '/objectadmin/GetAttrTypeList',
        baseParams: {
            layerId: self.layer_Id
        }
    });

    this.setLayerId = function(_layer_Id) {
        self.layer_Id = _layer_Id;
    }
    //-------Конструктор грида-----------
    AttrTypeListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        frame: true,
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'attrTypeId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'attrTypeName',
            dataIndex: 'name',
            hideable: false,
            hidden: true
        }, {
            id: 'label',
            dataIndex: 'label',
            hideable: false,
            hidden: true
        }, {
            id: 'attrTypeNameTag',
            dataIndex: 'nameTag',
            header: "Полное наименование",
            sortable: true
        }, {
            id: 'attrTypeShortName',
            dataIndex: 'shortname',
            header: "Краткое наименование",
            sortable: true
        }, {
            id: 'addDate',
            dataIndex: 'adddate',
            header: "Дата создания",
            sortable: true
        }, {
            id: 'attrTypeNote',
            dataIndex: 'note',
            header: "Примечание",
            sortable: true
        }]
    });
}
Ext.extend(AttrTypeListGrid, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);

        this.storeJS.on('load', function(store, records, option) {
            if (records.length == 0) {
                this.owner.unBindButton.disable();
            } else {
                this.getSelectionModel().selectFirstRow();
                this.owner.unBindButton.enable();
            }
        }, this);

        AttrTypeListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
AttrTypeListPanel = function(owner, layer_Id) {
    this.owner = owner;
    this.layer_Id = layer_Id;

    this.attrTypeListGrid = new AttrTypeListGrid(this, this.layer_Id);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        iconCls: 'icon-add',
        text: 'Добавить',
        handler: function() {
            this.fireEvent('AddAttrTyper')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        iconCls: 'icon-properties',
        text: 'Свойства',
        handler: function() {
            this.fireEvent('EditAttrType')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        iconCls: 'icon-delete',
        text: 'Удалить',
        handler: function() {
            this.fireEvent('RemoveAttrType')
        },
        scope: this
    });
    this.unBindButton = new Ext.Button({
        text: 'Отвязать',
        disabled: true,
        handler: function() {
            this.fireEvent('UnBindAttrType')
        },
        scope: this
    });
    this.BindButton = new Ext.Button({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('BindAttrType')
        },
        scope: this
    });
    this.CurrentButton = new Ext.Button({
        text: 'Назначить основным',
        handler: function() {
            this.fireEvent('CurrentAttrType')
        },
        scope: this
    });
    this.UnCurrentButton = new Ext.Button({
        text: 'Назначить дополнительным',
        handler: function() {
            this.fireEvent('UnCurrentAttrType')
        },
        scope: this
    });

    //--------Панель----------
    AttrTypeListPanel.superclass.constructor.call(this, {
        title: 'Список атрибутивных описаний',
        layout: 'fit',
        region: 'center',
        tbar: [this.BindButton, this.unBindButton, this.CurrentButton /*,this.UnCurrentButton*/ ],
        items: [this.attrTypeListGrid]
    });
}
Ext.extend(AttrTypeListPanel, Ext.form.FieldSet, {
    initComponent: function() {
        this.addEvents({
            AddAttrType: true,
            EditAttrType: true,
            RemoveAttrType: true,
            UnBindAttrType: true,
            BindAttrType: true,
            CurrentAttrType: true,
            UnCurrentAttrType: true
        });
        var self = this;

        this.on('beforeshow ', function() {
            if (self.layer_Id == null) {
                self.CurrentButton.disable();
                self.UnCurrentButton.disable();
                self.BindButton.disable();
                self.unBindButton.disable();
            }
        }, this);

        this.on('UnBindAttrType', function() {
            var attrTypeForUnBind = this.attrTypeListGrid.getSelectionModel().getSelections();
            if (attrTypeForUnBind.length != 0) {
                for (var i = 0; i < attrTypeForUnBind.length; i++) {
                    self.unBindAttrFormWindow = new UnBindAttrFormWindow(self, attrTypeForUnBind[i].data.id, this.layer_Id);
                    self.unBindAttrFormWindow.setTitle('Отвязка атрибутивного описания: ' + attrTypeForUnBind[i].data.name); //+ attrTypeForUnBind[i].data.name
                    self.unBindAttrFormWindow.show();
                }
            }
        }, this);

        this.on('BindAttrType', function() {
            self.BindAttrFormWindow = new BindAttrFormWindow(self, self.layer_Id);
            self.BindAttrFormWindow.setTitle('Привязка атрибутивных описаний');
            self.BindAttrFormWindow.show();
            self.BindAttrFormWindow.AttrFildSet.doLayout();
        }, this);

        this.on('CurrentAttrType', function() {
            var attrTypeForCurrent = this.attrTypeListGrid.getSelectionModel().getSelections();
            if (attrTypeForCurrent.length == 1) {
                var msgWait = Ext.Msg.wait('Назначение атрибутивного описания основным...', 'Подождите', {
                    interval: 50
                });
                Objectadmin.conn.request({
                    url: '/objectadmin/SetRelationAtt',
                    method: 'POST',
                    params: {
                        layerid: self.layer_Id,
                        typeid: attrTypeForCurrent[0].data.id,
                        iscurrent: true
                    },
                    success: function() {
                        self.attrTypeListGrid.storeJS.reload();
                        msgWait.hide();
                    }
                });
            }
        }, this);

        this.on('UnCurrentAttrType', function() {
            var attrTypeForCurrent = this.attrTypeListGrid.getSelectionModel().getSelections();
            if (attrTypeForCurrent.length == 1) {
                var msgWait = Ext.Msg.wait('Назначение атрибутивного описания дополнительным...', 'Подождите', {
                    interval: 50
                });
                Objectadmin.conn.request({
                    url: '/objectadmin/SetRelationAtt',
                    method: 'POST',
                    params: {
                        layerid: self.layer_Id,
                        typeid: attrTypeForCurrent[0].data.id,
                        iscurrent: false
                    },
                    success: function() {
                        self.attrTypeListGrid.storeJS.reload();
                        msgWait.hide();
                    }
                });
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showAttrTyperListPanel = false;
        }, this);

        AttrTypeListPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-                                          -*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

UnbindLayerTypePanel = function() {
    if (WGS.PRIVILEGE.CARD_BIND) {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'bindcard',
                tooltip: 'Привязать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'putcard',
                tooltip: 'Вложить карточку'
            }]
        });
    } else {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }]
        });
    }

    this.rowActions.on({
        action: function(grid, record, action, row, col) {
            if (action == 'viewcard') {
                if (record.data.objectId) {
                    Map.Card.Instance(this.mapViewer).open({
                        objectId: record.data.objectId,
                        callback: function() {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            } else if (action == 'bindcard') {
                var objectId = record.data.objectId;
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Привязка карточки'
                });
                var mapViewer = Map.Viewer.Create({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        api.Map.ObserveOnMapLoaded(function() {
                            lib.getSelectedFeatures(function(featureLayers) {
                                if (featureLayers != -1) {
                                    featureId = featureLayers[0].features[0].featureId;
                                    if (featureLayers[0].features.length > 1 || featureLayers.length > 1) Ext.WgsMsg.show({
                                        title: 'Предупреждение',
                                        msg: 'Необходимо выбрать один объект',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.WARNING,
                                        fn: function() {
                                            api.Map.ClearSelection();
                                        }
                                    });
                                    else Ext.WgsMsg.show({
                                        title: 'Подтверждение',
                                        msg: 'Вы уверены, что хотите привязать выбранный объект?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: function(buttonId) {
                                            if (buttonId == 'yes') {
                                                new Ext.data.WgsConnection().request({
                                                    url: '/map/bind/bindfeature',
                                                    method: 'POST',
                                                    params: {
                                                        objectId: objectId,
                                                        featureId: featureId
                                                    },
                                                    success: function() {
                                                        this.unbindedObjStore.reload();
                                                        win.close();
                                                    },
                                                    failure: function() {
                                                        api.Map.ClearSelection();
                                                    },
                                                    scope: this
                                                });
                                            } else {
                                                api.Map.ClearSelection();
                                            }
                                        },
                                        icon: Ext.MessageBox.QUESTION,
                                        scope: this
                                    });
                                }
                            }, this);
                        }, this);
                    },
                    scope: this
                });
            } else if (action == 'putcard') {
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Вложение карточки'
                });
                var mapViewer = new Map.Viewer({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        win.maximize()
                        api.Map.ObserveOnMapLoaded(function() {
                            var bindSearch = Map.Bind.Instance(mapViewer);
                            var featureTypeId = this.featureTypeComboBox.getValue();
                            var featureTypeName = this.featureTypeStore.getAt(this.featureTypeStore.find('featureTypeId', featureTypeId)).data.featureTypeName
                            bindSearch.open({
                                mode: Map.Bind.CARD_OBJECT_MODE,
                                cardId: record.data.objectId,
                                cardName: record.data.name,
                                featureTypeName: featureTypeName,
                                featureTypeId: featureTypeId
                            });
                        }, this, true);
                    },
                    scope: this
                });
            }
        },
        scope: this
    });

    this.bindButton = new Ext.Button({
        text: 'Привязать все',
        disabled: true
    });

    this.removeAllButton = new Ext.Button({
        text: 'Удалить все',
        disabled: true
    });

    this.removeButton = new Ext.Button({
        iconCls: 'icon-delete',
        text: 'Удалить',
        disabled: true
    });

    this.featureTypeStore = new Ext.data.WgsJsonStore({
        autoLoad: true,
        url: '/objectadmin/unbindedobj/getlayertypefeaturetypes',
        fields: ['featureTypeId', 'featureTypeName']
    });

    this.unbindDateStore = new Ext.data.WgsJsonStore({
        url: '/objectadmin/unbindedobj/getlayertypeunbinddates',
        fields: ['unbindedDate']
    });

    this.unbindDateStore.on('load', function(ds, records, o) {
        if (records.length > 0) {
            this.unbindDateComboBox.setValue(records[0].data.unbindedDate);
            this.unbindedObjStore.baseParams = {
                featureTypeId: this.featureTypeComboBox.getValue(),
                unbindedDate: records[0].data.unbindedDate
            };
            this.unbindedObjStore.reload();
        }
    }, this);

    this.featureTypeStore.on('load', function(ds, records, o) {
        if (records.length > 0) {
            this.featureTypeComboBox.setValue(records[0].data.featureTypeId);
            this.unbindDateStore.baseParams = {
                featureTypeId: records[0].data.featureTypeId
            };
            this.unbindDateStore.load();
        }
    }, this);

    this.featureTypeComboBox = new Ext.form.ComboBox({
        editable: false,
        width: 200,
        maxHeight: 400,
        emptyText: 'Выбрать...',
        loadingText: 'Загрузка...',
        store: this.featureTypeStore,
        displayField: 'featureTypeName',
        valueField: 'featureTypeId',
        triggerAction: 'all',
        anchor: '100%'
    });

    this.unbindDateComboBox = new Ext.form.ComboBox({
        editable: false,
        width: 200,
        maxHeight: 400,
        emptyText: 'Выбрать...',
        loadingText: 'Загрузка...',
        store: this.unbindDateStore,
        displayField: 'unbindedDate',
        valueField: 'unbindedDate',
        triggerAction: 'all',
        anchor: '100%'
    });

    this.featureTypeComboBox.on('select', function(combo, record) {
        this.unbindDateStore.baseParams = {
            featureTypeId: record.data.featureTypeId
        };
        this.unbindDateStore.load();
    }, this);

    this.unbindDateComboBox.on('select', function(combo, record) {
        var featureTypeId = this.featureTypeComboBox.getValue();
        this.unbindedObjStore.baseParams = {
            featureTypeId: featureTypeId,
            unbindedDate: record.data.unbindedDate
        };
        this.unbindedObjStore.removeAll();
        this.unbindedObjStore.load();
    }, this);

    this.unbindedObjStore = new Ext.data.WgsGroupingStore({
        reader: new Ext.data.JsonReader({
            totalProperty: 'totalCount',
            root: 'rows'
        }, [{
            name: 'objectId'
        }, {
            name: 'name'
        }, {
            name: 'hide'
        }]),
        url: '/objectadmin/unbindedobj/getlayertypeobjects',
        fields: ['objectId', 'name', 'hide'],
        sortInfo: {
            field: 'name',
            direction: "ASC"
        }
    });

    this.unbindedObjStore.on('load', function(ds, records, o) {
        if (records.length > 0) {
            this.removeAllButton.enable();
        } else {
            this.removeAllButton.disable();
        }
        this.removeButton.disable();
    }, this);

    this.unbindedObjCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

    this.unbindedObjCheckboxSelectionModel.on('selectionchange', function(SelectionModel) {
        if (SelectionModel.getSelections().length > 0) {
            this.removeButton.enable();
            this.removeAllButton.disable();
        } else {
            this.removeButton.disable();
            this.removeAllButton.enable();
        }
    }, this);

    this.removeButton.on('click', function() {
        var selections = this.unbindedObjCheckboxSelectionModel.getSelections();
        var cardIds = [];
        Ext.each(selections, function(item, index) {
            cardIds.push(item.get('objectId'));
        });

        Ext.WgsMsg.show({
            title: 'Подтверждение',
            msg: 'Вы уверены, что хотите удалить выбранные карточки?',
            buttons: Ext.Msg.YESNO,
            fn: function(buttonId) {
                if (buttonId == 'yes') {
                    Objectadmin.conn.request({
                        url: '/map/bind/removecard',
                        method: 'POST',
                        params: {
                            'cardIds[]': cardIds
                        },
                        success: function(response) {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }, this);

    this.removeAllButton.on('click', function() {
        var featureTypeId = this.featureTypeComboBox.getValue();
        var unbindedDate = this.unbindDateComboBox.getValue();
        Ext.WgsMsg.show({
            title: 'Подтверждение',
            msg: 'Вы уверены, что хотите удалить все карточки на указанную дату?',
            buttons: Ext.Msg.YESNO,
            fn: function(buttonId) {
                if (buttonId == 'yes') {
                    var loadMask = new Ext.LoadMask(this.unbindedObjGrid.getEl(), {
                        msg: 'Удаление...'
                    });
                    loadMask.show();
                    Objectadmin.conn.request({
                        url: '/objectadmin/unbindedobj/removeall',
                        method: 'POST',
                        params: {
                            featureTypeId: featureTypeId,
                            unbindedDate: unbindedDate
                        },
                        success: function(response) {
                            this.unbindedObjStore.removeAll();
                            this.featureTypeComboBox.clearValue();
                            this.unbindDateStore.removeAll();
                            this.unbindDateComboBox.clearValue();
                            this.featureTypeStore.removeAll();
                            this.featureTypeStore.load();
                            this.unbindedObjStore.load();
                            loadMask.hide();
                        },
                        scope: this
                    });
                }
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }, this);

    this.unbindedObjGrid = new Ext.grid.GridPanel({
        store: this.unbindedObjStore,
        columns: [{
            id: 'objectId',
            dataIndex: 'objectId',
            hidden: true,
            hideable: false
        }, {
            id: 'name',
            header: "Наименование",
            sortable: true,
            dataIndex: 'name',
            width: 150
        },
        this.rowActions,
        this.unbindedObjCheckboxSelectionModel],
        sm: this.unbindedObjCheckboxSelectionModel,
        plugins: [this.rowActions],
        viewConfig: {
            forceFit: true
        },
        bbar: new Ext.PagingToolbar({
            store: this.unbindedObjStore,
            displayInfo: true,
            pageSize: 30
        }),
        stripeRows: true
    });

    UnbindLayerTypePanel.superclass.constructor.call(this, {
        layout: 'fit',
        title: 'Слой - Тип',
        tbar: [
            '', {
            xtype: 'tbtext',
            text: 'Тип:'
        },
            '',
        this.featureTypeComboBox,
            '', {
            xtype: 'tbtext',
            text: 'Дата отвязки:'
        },
            '',
        this.unbindDateComboBox,
            '->',
        this.removeAllButton,
        this.removeButton],
        items: [
        this.unbindedObjGrid]
    });
}
Ext.extend(UnbindLayerTypePanel, Ext.Panel);

UnbindObjectCardPanel = function() {
    if (WGS.PRIVILEGE.CARD_BIND) {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'bindcard',
                tooltip: 'Привязать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'putcard',
                tooltip: 'Вложить карточку'
            }]
        });
    } else {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }]
        });
    }

    this.rowActions.on({
        action: function(grid, record, action, row, col) {
            if (action == 'viewcard') {
                if (record.data.objectId) {
                    Map.Card.Instance(this.mapViewer).open({
                        objectId: record.data.objectId,
                        callback: function() {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            } else if (action == 'bindcard') {
                var objectId = record.data.objectId;
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Привязка карточки'
                });
                var mapViewer = Map.Viewer.Create({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        api.Map.ObserveOnMapLoaded(function() {
                            lib.getSelectedFeatures(function(featureLayers) {
                                if (featureLayers != -1) {
                                    featureId = featureLayers[0].features[0].featureId;
                                    if (featureLayers[0].features.length > 1 || featureLayers.length > 1) Ext.WgsMsg.show({
                                        title: 'Предупреждение',
                                        msg: 'Необходимо выбрать один объект',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.WARNING,
                                        fn: function() {
                                            api.Map.ClearSelection();
                                        }
                                    });
                                    else Ext.WgsMsg.show({
                                        title: 'Подтверждение',
                                        msg: 'Вы уверены, что хотите привязать выбранный объект?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: function(buttonId) {
                                            if (buttonId == 'yes') {
                                                new Ext.data.WgsConnection().request({
                                                    url: '/map/bind/bindfeature',
                                                    method: 'POST',
                                                    params: {
                                                        objectId: objectId,
                                                        featureId: featureId
                                                    },
                                                    success: function() {
                                                        this.unbindedObjStore.reload();
                                                        win.close();
                                                    },
                                                    failure: function() {
                                                        api.Map.ClearSelection();
                                                    },
                                                    scope: this
                                                });
                                            } else {
                                                api.Map.ClearSelection();
                                            }
                                        },
                                        icon: Ext.MessageBox.QUESTION,
                                        scope: this
                                    });
                                }
                            }, this);
                        }, this);
                    },
                    scope: this
                });
            } else if (action == 'putcard') {
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Вложение карточки'
                });
                var mapViewer = new Map.Viewer({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        win.maximize()
                        api.Map.ObserveOnMapLoaded(function() {
                            var bindSearch = Map.Bind.Instance(mapViewer);
                            bindSearch.open({
                                mode: Map.Bind.CARD_OBJECT_MODE,
                                cardId: record.data.objectId,
                                cardName: record.data.objectName,
                                featureTypeName: record.data.objectTypeName,
                                featureTypeId: record.data.objectTypeId
                            });
                        }, this, true);
                    },
                    scope: this
                });
            }
        },
        scope: this
    });

    this.removeButton = new Ext.Button({
        iconCls: 'icon-delete',
        text: 'Удалить',
        disabled: true
    });

    this.unbindedObjStore = new Ext.data.WgsGroupingStore({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            totalProperty: 'totalCount',
            root: 'rows'
        }, [{
            name: 'objectId'
        }, {
            name: 'objectName'
        }, {
            name: 'objectTypeName'
        }, {
            name: 'objectTypeId'
        }, {
            name: 'unbindedDate'
        }, {
            name: 'hide'
        }]),
        url: '/objectadmin/unbindedobj/getobjectcardobjects',
        fields: ['objectId', 'objectName', 'objectTypeName', 'objectTypeId', 'unbindedDate', 'hide'],
        sortInfo: {
            field: 'objectName',
            direction: "ASC"
        },
        remoteGroup: true
    });

    this.unbindedObjStore.on('load', function(ds, records, o) {
        this.removeButton.disable();
    }, this);

    this.unbindedObjCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

    this.unbindedObjCheckboxSelectionModel.on('selectionchange', function(SelectionModel) {
        if (SelectionModel.getSelections().length > 0) {
            this.removeButton.enable();
        } else {
            this.removeButton.disable();
        }
    }, this);

    this.removeButton.on('click', function() {
        var selections = this.unbindedObjCheckboxSelectionModel.getSelections();
        var cardIds = [];
        Ext.each(selections, function(item, index) {
            cardIds.push(item.get('objectId'));
        });

        Ext.WgsMsg.show({
            title: 'Подтверждение',
            msg: 'Вы уверены, что хотите удалить выбранные карточки?',
            buttons: Ext.Msg.YESNO,
            fn: function(buttonId) {
                if (buttonId == 'yes') {
                    Objectadmin.conn.request({
                        url: '/map/bind/removecard',
                        method: 'POST',
                        params: {
                            'cardIds[]': cardIds
                        },
                        success: function(response) {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }, this);

    this.unbindedObjGrid = new Ext.grid.GridPanel({
        store: this.unbindedObjStore,
        columns: [{
            id: 'objectId',
            dataIndex: 'objectId',
            hidden: true,
            hideable: false
        }, {
            id: 'objectName',
            header: "Наименование",
            sortable: true,
            dataIndex: 'objectName',
            width: 150
        }, {
            id: 'objectTypeName',
            header: "Тип",
            sortable: true,
            dataIndex: 'objectTypeName'
        }, {
            id: 'unbindedDate',
            header: "Дата отвязки",
            sortable: true,
            dataIndex: 'unbindedDate'
        },
        this.rowActions,
        this.unbindedObjCheckboxSelectionModel],
        sm: this.unbindedObjCheckboxSelectionModel,
        plugins: [this.rowActions],
        view: new Ext.grid.GroupingView({
            forceFit: true,
            groupTextTpl: '{gvalue}'
        }),
        bbar: new Ext.PagingToolbar({
            store: this.unbindedObjStore,
            displayInfo: true,
            pageSize: 30
        }),
        stripeRows: true
    });
    UnbindObjectCardPanel.superclass.constructor.call(this, {
        layout: 'fit',
        title: 'Объект - Карточка',
        tbar: [
            '->',
        this.removeButton],
        items: [
        this.unbindedObjGrid]
    });
}

Ext.extend(UnbindObjectCardPanel, Ext.Panel);

UnbindCardCardPanel = function() {
    if (WGS.PRIVILEGE.CARD_BIND) {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'bindcard',
                tooltip: 'Привязать карточку',
                hideIndex: 'hide'
            }, {
                iconCls: 'putcard',
                tooltip: 'Вложить карточку'
            }]
        });
    } else {
        this.rowActions = new Ext.ux.grid.RowActions({
            actions: [{
                iconCls: 'viewcard',
                tooltip: 'Показать карточку',
                hideIndex: 'hide'
            }]
        });
    }

    this.rowActions.on({
        action: function(grid, record, action, row, col) {
            if (action == 'viewcard') {
                if (record.data.objectId) {
                    Map.Card.Instance(this.mapViewer).open({
                        objectId: record.data.objectId,
                        callback: function() {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            } else if (action == 'bindcard') {
                var objectId = record.data.objectId;
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Привязка карточки'
                });
                var mapViewer = Map.Viewer.Create({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        api.Map.ObserveOnMapLoaded(function() {
                            lib.getSelectedFeatures(function(featureLayers) {
                                if (featureLayers != -1) {
                                    featureId = featureLayers[0].features[0].featureId;
                                    if (featureLayers[0].features.length > 1 || featureLayers.length > 1) Ext.WgsMsg.show({
                                        title: 'Предупреждение',
                                        msg: 'Необходимо выбрать один объект',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.WARNING,
                                        fn: function() {
                                            api.Map.ClearSelection();
                                        }
                                    });
                                    else Ext.WgsMsg.show({
                                        title: 'Подтверждение',
                                        msg: 'Вы уверены, что хотите привязать выбранный объект?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: function(buttonId) {
                                            if (buttonId == 'yes') {
                                                new Ext.data.WgsConnection().request({
                                                    url: '/map/bind/bindfeature',
                                                    method: 'POST',
                                                    params: {
                                                        objectId: objectId,
                                                        featureId: featureId
                                                    },
                                                    success: function() {
                                                        this.unbindedObjStore.reload();
                                                        win.close();
                                                    },
                                                    failure: function() {
                                                        api.Map.ClearSelection();
                                                    },
                                                    scope: this
                                                });
                                            } else {
                                                api.Map.ClearSelection();
                                            }
                                        },
                                        icon: Ext.MessageBox.QUESTION,
                                        scope: this
                                    });
                                }
                            }, this);
                        }, this);
                    },
                    scope: this
                });
            } else if (action == 'putcard') {
                var win = new Ext.WgsShimWindow({
                    width: 750,
                    maximizable: true,
                    resizable: true,
                    layout: 'fit',
                    title: 'Вложение карточки'
                });
                var mapViewer = new Map.Viewer({
                    viewerType: 'ajax',
                    simpleMode: true,
                    render: function(viewerPanel) {
                        viewerPanel.setWidth(740);
                        viewerPanel.setHeight(480);
                        win.add(viewerPanel);
                        win.show();
                    },
                    load: function(api, lib, commands) {
                        win.maximize()
                        api.Map.ObserveOnMapLoaded(function() {
                            var bindSearch = Map.Bind.Instance(mapViewer);
                            bindSearch.open({
                                mode: Map.Bind.CARD_OBJECT_MODE,
                                cardId: record.data.objectId,
                                cardName: record.data.objectName,
                                featureTypeName: record.data.objectTypeName,
                                featureTypeId: record.data.objectTypeId
                            });
                        }, null, true);
                    },
                    scope: this
                });
            }
        },
        scope: this
    });

    this.removeButton = new Ext.Button({
        iconCls: 'icon-delete',
        text: 'Удалить',
        disabled: true
    });

    this.unbindedObjStore = new Ext.data.WgsGroupingStore({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            totalProperty: 'totalCount',
            root: 'rows'
        }, [{
            name: 'objectId'
        }, {
            name: 'objectName'
        }, {
            name: 'objectTypeName'
        }, {
            name: 'objectTypeId'
        }, {
            name: 'hide'
        }]),
        url: '/objectadmin/unbindedobj/getcardcardobjects',
        fields: ['objectId', 'objectName', 'objectTypeName', 'objectTypeId', 'hide'],
        sortInfo: {
            field: 'objectName',
            direction: "ASC"
        },
        remoteGroup: true
    });

    this.unbindedObjStore.on('load', function(ds, records, o) {
        this.removeButton.disable();
    }, this);

    this.unbindedObjCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

    this.unbindedObjCheckboxSelectionModel.on('selectionchange', function(SelectionModel) {
        if (SelectionModel.getSelections().length > 0) {
            this.removeButton.enable();
        } else {
            this.removeButton.disable();
        }
    }, this);

    this.removeButton.on('click', function() {
        var selections = this.unbindedObjCheckboxSelectionModel.getSelections();
        var cardIds = [];
        Ext.each(selections, function(item, index) {
            cardIds.push(item.get('objectId'));
        });

        Ext.WgsMsg.show({
            title: 'Подтверждение',
            msg: 'Вы уверены, что хотите удалить выбранные карточки?',
            buttons: Ext.Msg.YESNO,
            fn: function(buttonId) {
                if (buttonId == 'yes') {
                    Objectadmin.conn.request({
                        url: '/map/bind/removecard',
                        method: 'POST',
                        params: {
                            'cardIds[]': cardIds
                        },
                        success: function(response) {
                            this.unbindedObjStore.reload();
                        },
                        scope: this
                    });
                }
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }, this);

    this.unbindedObjGrid = new Ext.grid.GridPanel({
        store: this.unbindedObjStore,
        columns: [{
            id: 'objectId',
            dataIndex: 'objectId',
            hidden: true,
            hideable: false
        }, {
            id: 'objectName',
            header: "Наименование",
            sortable: true,
            dataIndex: 'objectName',
            width: 150
        }, {
            id: 'objectTypeName',
            header: "Тип",
            sortable: true,
            dataIndex: 'objectTypeName'
        },
        this.rowActions,
        this.unbindedObjCheckboxSelectionModel],
        sm: this.unbindedObjCheckboxSelectionModel,
        plugins: [this.rowActions],
        view: new Ext.grid.GroupingView({
            forceFit: true,
            groupTextTpl: '{gvalue}'
        }),
        bbar: new Ext.PagingToolbar({
            store: this.unbindedObjStore,
            displayInfo: true,
            pageSize: 30
        }),
        stripeRows: true
    });

    UnbindObjectCardPanel.superclass.constructor.call(this, {
        layout: 'fit',
        title: 'Карточка - Карточка',
        tbar: [
            '->',
        this.removeButton],
        items: [
        this.unbindedObjGrid]
    });
}

Ext.extend(UnbindCardCardPanel, Ext.Panel);

UnbindObjPanel = function() {
    this.layerTypePanel = new UnbindLayerTypePanel();
    this.objectCardPanel = new UnbindObjectCardPanel();
    this.objectObjectPanel = new UnbindCardCardPanel();

    UnbindObjPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-unbindedobj',
        closable: true,
        layout: 'fit',
        items: [
        new Ext.TabPanel({
            activeTab: 0,
            items: [
            this.layerTypePanel,
            this.objectCardPanel,
            this.objectObjectPanel]
        })]
    });
}

Ext.extend(UnbindObjPanel, Ext.Panel);

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-ФОРМА РЕДАКТИРОВАНИЯ АТРИБУТИВНЫХ ОПИСАНИЙ-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
AttrTypeEditFormPanel = function(owner, type_Id) {
    this.owner = owner;
    this.type_Id = type_Id;

    this.typeName = '';
    this.typeShortName = '';
    this.typeAddDate = '';
    this.typeNote = '';

    this.loadLabelProp = false;
    this.labelProp = null;

    var self = this;

    if (this.type_Id == null) {
        this.propListPanelAttrType = new PropListPanelAttrType(this, -1, null);
        this.propListPanelAttrType.bindButton.disable();
        this.propListPanelAttrType.unBindButton.disable();
        this.propListPanelAttrType.currentButton.disable();
        this.propListPanelAttrType.unCurrentButton.disable();

        this.attrTypeRelationListPanel = new AttrTypeRelationListPanel(this, -1);
        this.attrTypeRelationListPanel.createButton.disable();
        this.attrTypeRelationListPanel.editButton.disable();
        this.attrTypeRelationListPanel.removeButton.disable();

        this.featureLayerForAttrTypeListPanel = new FeatureLayerForAttrTypeListPanel(this, -1);
        this.featureLayerForAttrTypeListPanel.bindButton.disable();
        this.featureLayerForAttrTypeListPanel.unbindButton.disable();
    } else {
        this.propListPanelAttrType = new PropListPanelAttrType(this, this.type_Id, null);
        this.attrTypeRelationListPanel = new AttrTypeRelationListPanel(this, this.type_Id);
        this.featureLayerForAttrTypeListPanel = new FeatureLayerForAttrTypeListPanel(this, this.type_Id);
    }
    this.propListPanelAttrType.setTitle('Список характеристик атрибутивного описания');
    this.attrTypeRelationListPanel.setTitle('Список дочерних атрибутивных описаний');
    this.featureLayerForAttrTypeListPanel.setTitle('Список привязанных слоев');

    self.PropComboBox = new Ext.form.ComboBox({
        fieldLabel: 'Характеристика',
        name: 'propnamelabel',
        anchor: '100%',
        disabled: true,
        store: self.propStoreJS = new Ext.data.WgsJsonStore({
            autoLoad: true,
            fields: ['id', 'name'],
            url: '/objectadmin/GetPropList',
            baseParams: {
                typeId: this.type_Id,
                aCadView: 1
            }
        }),
        displayField: 'name',
        valueField: 'id',
        hiddenName: 'propnamelabel',
        typeAhead: true,
        loadingText: 'Загрузка характеристик...',
        triggerAction: 'all',
        emptyText: 'Выберите характеристику...',
        selectOnFocus: true,
        allowBlank: false,
        editable: false
    })

    AttrTypeEditFormPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-atttype',
        closable: true,
        labelWidth: 150,
        anchor: '100% 100%',
        items: [{
            anchor: '100% 100%',
            layout: 'border',
            items: [{
                region: 'north',
                height: 335,
                autoScroll: true,
                items: [
                this.GeneralParam = new Ext.form.FieldSet({
                    title: 'Основные параметры',
                    autoHeight: true,
                    defaultType: 'textfield',
                    items: [{
                        fieldLabel: 'Полное наименование',
                        anchor: '100%',
                        name: 'name',
                        allowBlank: false
                    }, {
                        fieldLabel: 'Краткое наименование',
                        anchor: '100%',
                        name: 'shortname'
                    },
                    new Ext.form.DateField({
                        fieldLabel: 'Дата создания',
                        anchor: '100%',
                        //altFormats : 'd.m.Y|d.m.y',
                        format: 'Y-m-d H:i:s',
                        name: 'adddate',
                        allowBlank: false,
                        disabled: true
                    }), new Ext.form.TextArea({
                        fieldLabel: 'Примечание',
                        anchor: '100%',
                        name: 'note'

                    }), {
                        xtype: 'hidden',
                        name: 'id',
                        hidden: true,
                        labelSeparator: ''
                    },

                    new Ext.form.FieldSet({
                        autoHeight: true,
                        labelWidth: 137,
                        title: 'Подпись к объектам на карте',
                        items: [{
                            xtype: 'ux-radiogroup',
                            fieldLabel: '',
                            labelSeparator: '',
                            name: 'objectLabel',
                            horizontal: true,
                            radios: [{
                                value: -1,
                                boxLabel: 'Полное наименование',
                                checked: true,
                                listeners: {
                                    'check': function(r, c) {
                                        if (c) {
                                            self.PropComboBox.disable();
                                            self.PropComboBox.clearValue();
                                        } else {
                                            //self.PropComboBox.enable();
                                        }

                                    }
                                }
                            }, {
                                value: -2,
                                boxLabel: 'Краткое наименование',
                                listeners: {
                                    'check': function(r, c) {
                                        if (c) {
                                            self.PropComboBox.disable();
                                            self.PropComboBox.clearValue();
                                        } else {
                                            //self.PropComboBox.enable();
                                        }

                                    }
                                }
                            }, {
                                value: -3,
                                boxLabel: 'Примечание',
                                listeners: {
                                    'check': function(r, c) {
                                        if (c) {
                                            self.PropComboBox.disable();
                                            self.PropComboBox.clearValue();
                                        } else {
                                            //self.PropComboBox.enable();
                                        }

                                    }
                                }
                            }, {
                                value: -4,
                                boxLabel: 'Характеристика',
                                listeners: {
                                    'check': function(r, c) {
                                        if (c) {
                                            self.PropComboBox.enable();
                                        } else {
                                            self.PropComboBox.disable();
                                            self.PropComboBox.clearValue();
                                        }

                                    }
                                }
                            }]
                        },
                        /*this.PropRadioGroup = new Ext.form.RadioGroup({
                                                            fieldLabel:'',
                                                            labelSeparator: '',
                                                            columns: 4,
                                                            items:
                                                            [
                                                                {
                                                                inputValue:-1,
                                                                boxLabel:'Наименование полное',
                                                                checked:true,
                                                                name:'objectLabel',
                                                                listeners:
                                                                {
                                                                    'check':function(r,c)
                                                                    {
                                                                        if (c) {
                                                                            self.PropComboBox.disable();
                                                                            self.PropComboBox.clearValue();
                                                                        }
                                                                    }
                                                                }
                                                            },{
                                                                inputValue:-2,
                                                                boxLabel:'Краткое наименование',
                                                                name:'objectLabel',
                                                                listeners:
                                                                {
                                                                    'check':function(r,c)
                                                                    {
                                                                        if (c) {
                                                                            self.PropComboBox.disable();
                                                                            self.PropComboBox.clearValue();
                                                                        } else {
                                                                           //self.PropComboBox.enable();
                                                                        }

                                                                    }
                                                                }
                                                            },{
                                                                inputValue:-3,
                                                                boxLabel:'Примечание',
                                                                name:'objectLabel',
                                                                listeners:
                                                                {
                                                                    'check':function(r,c)
                                                                    {
                                                                        if (c) {
                                                                            self.PropComboBox.disable();
                                                                            self.PropComboBox.clearValue();
                                                                        } else {
                                                                           //self.PropComboBox.enable();
                                                                        }

                                                                    }
                                                                }
                                                            },{
                                                                inputValue:-4,
                                                                boxLabel:'Характеристика',
                                                                name:'objectLabel',
                                                                listeners:
                                                                {
                                                                    'check':function(r,c)
                                                                    {
                                                                        if (c) {
                                                                            self.PropComboBox.enable();
                                                                        } else {
                                                                            self.PropComboBox.disable();
                                                                            self.PropComboBox.clearValue();
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                            ]
                                                        })*/
                        //,
                        self.PropComboBox]
                    })]
                })]
            }, {
                region: 'center',
                autoScroll: true,
                layout: 'fit',
                items: [
                this.tabpanel = new Ext.TabPanel({
                    activeTab: 0,
                    enableTabScroll: true,
                    items: [this.propListPanelAttrType, this.attrTypeRelationListPanel, this.featureLayerForAttrTypeListPanel]
                })]
            }]
        }

        ]
    });
    this.saveButton = this.GeneralParam.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.GeneralParam.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(AttrTypeEditFormPanel, Ext.form.WgsFormPanel, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;

        self.propStoreJS.on('load', function() {
            if (self.loadLabelProp == false) {
                self.loadLabelProp = true;
                if (self.labelProp != null) {
                    //self.labelProp = null;
                    self.PropComboBox.enable();
                    self.getForm().setValues({
                        objectLabel: -4,
                        propnamelabel: self.labelProp
                    });
                }
            }
        });

        this.on('cancel', function() {
            if (this.type_Id) {
                Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showAttrType[this.type_Id], true);
                delete Objectadmin.showAttrType[this.type_Id];
            } else {
                for (var i = 0; i < Objectadmin.showAttrType.length; i++) {
                    if (Objectadmin.showAttrType[i]) {
                        if (Objectadmin.showAttrType[i].title == 'Создание атрибутивного описания') {
                            Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showAttrType[i], true);
                            delete Objectadmin.showAttrType[i];
                        }
                    }
                }
            }
        }, this);
        //Сохранить
        this.on('save', function() {
            var msgWait = Ext.Msg.wait('Сохранение...', 'Подождите', {
                interval: 200
            });
            this.submit({
                success: function(form, action) {
                    if (self.owner.attrTypeListGrid) {
                        self.owner.attrTypeListGrid.storeJS.reload();
                    }
                    Objectadmin.objectTreePanel.refreshNode();
                    self.setTitle(self.getForm().getValues().name);
                    self.saveButton.setText('Применить');
                    self.propListPanelAttrType.propListGrid.storeJS.reload();
                    self.attrTypeRelationListPanel.attrTypeRelationListGrid.storeJS.reload();
                    self.featureLayerForAttrTypeListPanel.featureLayerForAttrTypeListGrid.storeJS.reload();
                    msgWait.hide();
                },
                failure: function(form, action) {
                    if (self.type_Id == null) {
                        if (action.response && action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.type_Id = action.response.responseText;
                                self.getForm().setValues({
                                    id: self.type_Id
                                });
                                self.getForm().url = '/objectadmin/EditAttrType';
                                self.setTitle(self.getForm().getValues(false).name);
                                if (self.owner.attrTypeListGrid) {
                                    self.owner.attrTypeListGrid.storeJS.reload();
                                }
                                self.propListPanelAttrType.type_Id = self.type_Id;
                                self.propListPanelAttrType.propListGrid.type_Id = self.type_Id;
                                self.propListPanelAttrType.propListGrid.storeJS.baseParams.typeId = self.type_Id;
                                self.propListPanelAttrType.propListGrid.storeJS.reload();
                                self.propListPanelAttrType.bindButton.enable();
                                self.propListPanelAttrType.unBindButton.enable();
                                self.propListPanelAttrType.currentButton.enable();
                                self.propListPanelAttrType.unCurrentButton.enable();

                                self.attrTypeRelationListPanel.parentType_Id = self.type_Id;
                                self.attrTypeRelationListPanel.attrTypeRelationListGrid.parentType_Id = self.type_Id;
                                self.attrTypeRelationListPanel.attrTypeRelationListGrid.storeJS.baseParams.parentObjTypeId = self.type_Id;
                                self.attrTypeRelationListPanel.attrTypeRelationListGrid.storeJS.reload();
                                self.attrTypeRelationListPanel.createButton.enable();
                                self.attrTypeRelationListPanel.editButton.enable();
                                self.attrTypeRelationListPanel.removeButton.enable();

                                self.featureLayerForAttrTypeListPanel.parentType_Id = self.type_Id;
                                self.featureLayerForAttrTypeListPanel.featureLayerForAttrTypeListGrid.parentType_Id = self.type_Id;
                                self.featureLayerForAttrTypeListPanel.featureLayerForAttrTypeListGrid.storeJS.baseParams.parentObjTypeId = self.type_Id;
                                self.featureLayerForAttrTypeListPanel.featureLayerForAttrTypeListGrid.storeJS.reload();

                                Objectadmin.showAttrType[self.type_Id] = Objectadmin.showAttrType[0];
                                delete Objectadmin.showAttrType[0];
                                Objectadmin.objectTreePanel.refreshNode();
                                self.saveButton.setText('Применить');
                            }
                        }
                    }
                    msgWait.hide();
                }
            });
        }, this);
        this.on('destroy', function() {
            this.owner.showAttrTypeEditFormPanel = false;
            if (this.type_Id == null) {
                delete Objectadmin.showAttrType[0];
            } else {
                delete Objectadmin.showAttrType[this.type_Id];
            }

        }, this);
        AttrTypeEditFormPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ФОРМА/ОКНО ОТВЯЗКИ АТРИБУТИВНОГО ОПИСАНИЯ ОТ СЛОЯ-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
UnBindAttrFormWindow = function(owner, type_id, layer_Id) {
    this.owner = owner;
    this.type_id = type_id;
    this.layer_Id = layer_Id;

    UnBindAttrFormWindow.superclass.constructor.call(this, {
        title: 'Отвязка описания',
        layout: 'fit',
        resizable: false,
        height: 150,
        width: 400,
        modal: true,
        items: [
        new Ext.form.WgsFormPanel({
            frame: true,
            items: [{
                xtype: 'textfield',
                hidden: true,
                labelSeparator: ''
            },
            this.removeObjectCheck = new Ext.form.Checkbox({
                boxLabel: 'Удалить все привязанные атрибутивные объекты',
                hideLabel: true
            })]
        })]
    });
    this.okButton = this.addButton({
        text: 'Отвязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(UnBindAttrFormWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            self.destroy();
        }, this);
        this.on('ok', function() {
            var msgWait = Ext.Msg.wait('Отвязка атрибутивного описания...', 'Подождите', {
                interval: 100
            });
            Objectadmin.conn.request({
                url: '/objectadmin/RemoveRelationAtt',
                method: 'POST',
                params: {
                    layerid: self.layer_Id,
                    typeid: self.type_id,
                    removeObject: self.removeObjectCheck.getValue()
                },
                success: function() {
                    self.owner.attrTypeListGrid.storeJS.reload();
                    msgWait.hide();
                    self.owner.attrTypeListGrid.storeJS.reload();
                    Objectadmin.objectTreePanel.refreshNode();
                    self.destroy();
                },
                failure: function() {
                    self.owner.attrTypeListGrid.storeJS.reload();
                    msgWait.hide();
                    self.owner.attrTypeListGrid.storeJS.reload();
                    Objectadmin.objectTreePanel.refreshNode();
                    self.destroy();
                }
            });

        }, this);
        UnBindAttrFormWindow.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ФОРМА/ОКНО ПРИВЯЗКИ АТРИБУТИВНОГО ОПИСАНИЯ К СЛОЮ-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BindAttrFormWindow = function(owner, layer_Id) {
    this.owner = owner;
    this.layer_Id = layer_Id;

    var self = this;

    this.projectSelectionModel = new Ext.grid.CheckboxSelectionModel({
        singleSelect: true
    });

    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'shortname', 'adddate', 'note', 'label'],
        url: '/objectadmin/GetAttrTypeListNotByLayer',
        baseParams: {
            layerId: self.layer_Id
        }
    });

    BindAttrFormWindow.superclass.constructor.call(this, {
        title: 'Привязка описания',
        layout: 'border',
        resizable: true,
        height: 500,
        width: 520,
        modal: true,
        items: [{
            region: 'center',
            frame: true,
            layout: 'fit',
            items: [
            self.AttrFildSet = new Ext.form.FieldSet({
                title: 'Список атрибутивных описаний',
                region: 'center',
                autoScroll: true,
                layout: 'fit',
                items: [
                self.AttrGrid = new Ext.grid.GridPanel({
                    sm: this.projectSelectionModel,
                    stripeRows: true,
                    viewConfig: {
                        forceFit: true
                    },
                    store: self.storeJS,
                    frame: true,
                    autoScroll: true,
                    columns: [
                    this.projectSelectionModel, {
                        id: 'attrTypeId',
                        dataIndex: 'id',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'label',
                        dataIndex: 'label',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'attrTypeName',
                        dataIndex: 'name',
                        header: "Наименование",
                        sortable: true
                    }, {
                        id: 'addDate',
                        dataIndex: 'adddate',
                        header: "Дата создания",
                        sortable: true
                    }, {
                        id: 'attrTypeShortName',
                        dataIndex: 'note',
                        header: "Примечание",
                        sortable: true
                    }]
                })]
            })]
        }, {
            region: 'south',
            height: 120,
            frame: true,
            items: [
            new Ext.form.FieldSet({
                title: 'Настройки привязки',
                autoScroll: true,
                autoHeight: true,
                items: [
                this.isCurrentCheck = new Ext.form.Checkbox({
                    boxLabel: 'Назначить данное описание основным',
                    hideLabel: true,
                    labelSeparator: ''
                }),
                this.bindOldObjectCheck = new Ext.form.Checkbox({
                    boxLabel: 'Привязать имеющиеся атрибутивные объеты к подходящим геометрическим',
                    hideLabel: true,
                    labelSeparator: ''
                }),
                this.removeOldObjectCheck = new Ext.form.Checkbox({
                    boxLabel: 'Удалить все атрибутивные объекты не привязанные к геометрическим',
                    hideLabel: true,
                    labelSeparator: ''
                })]
            })]
        }]
    });
    this.okButton = this.addButton({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(BindAttrFormWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            self.destroy();
        }, this);
        this.on('ok', function() {
            var attrTypeForBind = self.AttrGrid.getSelectionModel().getSelections();
            if (attrTypeForBind.length != 0) {
                self.BindAttrType = function(unBindDate) {
                    var msgWait = Ext.Msg.wait('Привязка атрибутивного описания...', 'Подождите', {
                        interval: 600
                    });
                    Objectadmin.conn.request({
                        url: '/objectadmin/AddRelationAtt',
                        method: 'POST',
                        params: {
                            layerid: self.layer_Id,
                            typeid: attrTypeForBind[0].data.id,
                            iscurrent: self.isCurrentCheck.getValue(),
                            unBindDate: unBindDate,
                            bindOldObject: self.bindOldObjectCheck.getValue(),
                            removeOldObject: self.removeOldObjectCheck.getValue()
                        },
                        success: function() {
                            self.owner.attrTypeListGrid.storeJS.reload();
                            msgWait.hide();
                            Objectadmin.objectTreePanel.refreshNode();
                            self.destroy();
                        }
                    });
                };
                if (self.bindOldObjectCheck.getValue()) {
                    self.unBindDateStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['unBindDate'],
                        url: '/objectadmin/GetListUnBindObjectDateByType',
                        baseParams: {
                            layerid: self.layer_Id,
                            typeid: attrTypeForBind[0].data.id
                        }
                    });

                    self.unBindDateStoreJS.load({

                        callback: function(records) {
                            if (!self.choiseUnBindDateWindowShow) {
                                if (records.length > 0) {
                                    self.choiseUnBindDateWindowShow = true;
                                    self.choiseUnBindDateWindow = new ChoiseUnBindDateWindow(self, attrTypeForBind[0].data.id, self.layer_Id);
                                    self.choiseUnBindDateWindow.show();
                                } else {
                                    self.BindAttrType(null);
                                }
                            }
                        }
                    });
                } else {
                    self.BindAttrType(null);
                }

            }
        }, this);
        BindAttrFormWindow.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-ОКНО ВЫБОРА ДАТЫ ОТВЯЗКИ ОБЪЕКТОВ-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ChoiseUnBindDateWindow = function(owner, type_id, layer_id) {
    this.owner = owner;
    this.type_id = type_id;
    this.layer_id = layer_id;

    var self = this;

    this.unBindDateComboBox = new Ext.form.ComboBox({
        anchor: '100%',
        hideLabel: true,
        labelSeparator: '',
        store: self.owner.unBindDateStoreJS,
        displayField: 'unBindDate',
        valueField: 'unBindDate',
        loadingText: 'Список загружается...',
        triggerAction: 'all',
        emptyText: 'Выберите дату...',
        allowBlank: true,
        editable: false
    });

    ChoiseUnBindDateWindow.superclass.constructor.call(this, {
        title: 'Выбор даты отвязки',
        layout: 'fit',
        height: 180,
        width: 375,
        modal: true,
        autoScrol: true,
        resizable: false,
        items: [
        new Ext.form.FormPanel({
            frame: true,
            items: [
            new Ext.Panel({
                frame: true,
                height: 60,
                layout: 'fit',
                style: 'margin-bottom: 5px',
                html: '<p>Если вы уверены, что хотите привязать старые атрибутивные ' + 'объекты то, выберите дату их отвязки от ланного слоя и нажмите' + '"Привязать", иначе "Отмена"</p>'
            }),
            this.unBindDateComboBox]
        })]
    });
    this.okButton = this.addButton({
        text: 'Привязать',
        disabled: true,
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });

    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(ChoiseUnBindDateWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;

        self.unBindDateComboBox.on('select', function() {
            self.okButton.enable();
        });

        this.on('cancel', function() {
            self.owner.choiseUnBindDateWindowShow = false;
            self.destroy();
        }, this);

        this.on('ok', function() {
            self.owner.choiseUnBindDateWindowShow = false;
            self.owner.BindAttrType(self.unBindDateComboBox.getValue());
            self.destroy();
        });
        ChoiseUnBindDateWindow.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-GRID СПИСКА СЛОЕВ-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerListGrid = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    //--------Массив со списком групп слоев----
    var self = this;
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'groupid', 'groupname'],
        url: '/objectadmin/GetFeatureLayerList',
        baseParams: {
            typeId: self.type_Id,
            groupId: self.group_Id
        }
    });
    //-------Конструктор грида-----------
    FeatureLayerListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        frame: true,
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'featureLayerId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'featureLayerGroupId',
            dataIndex: 'groupid',
            hideable: false,
            hidden: true
        }, {
            id: 'featureLayerName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }, {
            id: 'featureLayerGroupName',
            dataIndex: 'groupname',
            header: "Наименование группы",
            sortable: true
        }]
    });
}
Ext.extend(FeatureLayerListGrid, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);
        FeatureLayerListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА СЛОЕВ-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerListPanel = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    this.featureLayerListGrid = new FeatureLayerListGrid(this, this.type_Id, this.group_Id);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('AddFeatureLayer')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('EditFeatureLayer')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('RemoveFeatureLayer')
        },
        scope: this
    });

    //--------Панель----------
    FeatureLayerListPanel.superclass.constructor.call(this, {
        title: 'Список слоев',
        layout: 'fit',
        region: 'center',
        tbar: [this.addButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.featureLayerListGrid]
    });
}
Ext.extend(FeatureLayerListPanel, Ext.form.FieldSet, {
    initComponent: function() {
        this.addEvents({
            AddFeatureLayer: true,
            EditFeatureLayer: true,
            RemoveFeatureLayer: true
        });
        var self = this;
        this.on('RemoveFeatureLayer', function() {
            var self = this;
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите удалить слой?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId) {
                    if (buttonId == 'yes') {
                        var msgWait = Ext.Msg.wait('Удаление слоя...', 'Подождите', {
                            interval: 50
                        });
                        Objectadmin.conn.request({
                            url: '/objectadmin/RemoveFeatureLayer',
                            method: 'POST',
                            params: {
                                id: self.featureLayerListGrid.getSelectionModel().getSelected().data.id
                            },
                            success: function() {
                                msgWait.hide();
                                self.featureLayerListGrid.storeJS.reload();
                                Objectadmin.objectTreePanel.refreshNode();
                            },
                            failure: function() {
                                msgWait.hide();
                                Ext.MessageBox.alert('', 'Слой удалить не удалось!');
                            }
                        });
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }, this);

        this.on('AddFeatureLayer', function() {
            if (0 in Objectadmin.showLayer) {
                Ext.MessageBox.alert('', 'Необходимо закончить создание одного слоя, чтобы преступить к новому!');
            } else {
                Objectadmin.showLayer[0] = new FeatureLayerEditFormPanel(this, null);
                Objectadmin.showLayer[0].setTitle('Создание слоя');
                Objectadmin.showLayer[0].getForm().url = '/objectadmin/AddFeatureLayer';
                Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayer[0]));
                Objectadmin.showLayer[0].doLayout();
                Objectadmin.showLayer[0].layerName = '';
                Objectadmin.showLayer[0].layerGroup = self.group_Id;
                Objectadmin.showLayer[0].getForm().setValues({
                    groupid: self.group_Id
                });
                Objectadmin.showLayer[0].storeJS.load({
                    callback: function() {
                        if (Objectadmin.showLayer[0]) {
                            Objectadmin.showLayer[0].groupComboBox.setValue(self.group_Id);
                        }
                    }
                });
            }
        }, this);

        this.on('EditFeatureLayer', function() {
            var featureLayerForChange = this.featureLayerListGrid.getSelectionModel().getSelections();
            if (featureLayerForChange.length != 0) {
                if (featureLayerForChange.length == 1) {
                    if (!(featureLayerForChange[0].data.id in Objectadmin.showLayer)) {
                        Objectadmin.showLayer[featureLayerForChange[0].data.id] = new FeatureLayerEditFormPanel(this, featureLayerForChange[0].data.id);
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].setTitle(featureLayerForChange[0].data.name);
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().url = '/objectadmin/EditFeatureLayer';
                        Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayer[featureLayerForChange[0].data.id]));
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].doLayout();

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].layerName = featureLayerForChange[0].data.name;
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].layerGroup = featureLayerForChange[0].data.groupid;

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().loadRecord(featureLayerForChange[0]);

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().setValues({
                            groupid: featureLayerForChange[0].data.groupname
                        });

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].storeJS.load({
                            callback: function() {
                                if (Objectadmin.showLayer[featureLayerForChange[0].data.id]) {
                                    Objectadmin.showLayer[featureLayerForChange[0].data.id].groupComboBox.setValue(featureLayerForChange[0].data.groupid);
                                }
                            }
                        });
                    } else {
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].setTitle(featureLayerForChange[0].data.name);
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().url = '/objectadmin/EditFeatureLayer';
                        Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showLayer[featureLayerForChange[0].data.id]);
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].doLayout();

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].layerName = featureLayerForChange[0].data.name;
                        Objectadmin.showLayer[featureLayerForChange[0].data.id].layerGroup = featureLayerForChange[0].data.groupid;

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().loadRecord(featureLayerForChange[0]);

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].getForm().setValues({
                            groupid: featureLayerForChange[0].data.groupname
                        });

                        Objectadmin.showLayer[featureLayerForChange[0].data.id].storeJS.load({
                            callback: function() {
                                if (Objectadmin.showLayer[featureLayerForChange[0].data.id]) {
                                    Objectadmin.showLayer[featureLayerForChange[0].data.id].groupComboBox.setValue(featureLayerForChange[0].data.groupid);
                                }
                            }
                        });
                    }
                }
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showFeatureLayerListPanel = false;
        }, this);

        FeatureLayerListPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-ФОРМА РЕДАКТИРОВАНИЯ СЛОЯ-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerEditFormPanel = function(owner, layer_Id) {
    this.owner = owner;
    this.layer_Id = layer_Id;

    this.layerName = '';
    this.layerGroup = 0;

    //--------Список групп слоев дли ComboBox'а------------------
    this.storeJS = new Ext.data.WgsJsonStore({
        fields: ['id', 'name'],
        url: '/objectadmin/GetLayerGroupList'
    });

    var self = this;

    if (this.layer_Id == null) {
        this.attrTypeListPanel = new AttrTypeListPanel(self, -1);
        this.attrTypeListPanel.CurrentButton.disable();
        this.attrTypeListPanel.UnCurrentButton.disable();
        this.attrTypeListPanel.BindButton.disable();
        this.attrTypeListPanel.unBindButton.disable();
    } else {
        this.attrTypeListPanel = new AttrTypeListPanel(self, self.layer_Id);
    }
    this.attrTypeListPanel.setTitle('Привязанные атрибутивные описания');

    FeatureLayerEditFormPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-layer',
        closable: true,
        labelWidth: 150,
        anchor: '100% 100%',
        items: [{
            anchor: '100% 100%',
            layout: 'border',
            items: [{
                region: 'north',
                height: 180,
                items: [
                this.GeneralParam = new Ext.form.FieldSet({
                    title: 'Основные параметры',
                    autoHeight: true,
                    defaultType: 'textfield',
                    items: [{
                        fieldLabel: 'Наименование',
                        anchor: '100%',
                        name: 'name',
                        allowBlank: false
                    }, {
                        fieldLabel: 'Координатна система (ESPG)',
                        anchor: '100%',
                        name: 'cs',
                        value: '4326',
                        allowBlank: false
                    },
                    this.groupComboBox = new Ext.form.ComboBox({
                        fieldLabel: 'Группа',
                        name: 'groupname',
                        store: this.storeJS,
                        anchor: '100%',
                        displayField: 'name',
                        valueField: 'id',
                        hiddenName: 'groupid',
                        typeAhead: true,
                        loadingText: 'Список загружается...',
                        triggerAction: 'all',
                        emptyText: 'Выберите группу...',
                        selectOnFocus: true,
                        editable: false
                    }), {
                        xtype: 'hidden',
                        name: 'id',
                        hidden: true,
                        labelSeparator: ''
                    }]
                })]
            },
            this.attrTypeListPanel]
        }]
    });
    this.saveButton = this.GeneralParam.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.GeneralParam.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(FeatureLayerEditFormPanel, Ext.form.WgsFormPanel, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;
        //Отмена

        this.on('cancel', function() {
            if (this.layer_Id) {
                Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayer[this.layer_Id], true);
                delete Objectadmin.showLayer[this.layer_Id];
            } else {
                for (var i = 0; i < Objectadmin.showLayer.length; i++) {
                    if (Objectadmin.showLayer[i]) {
                        if (Objectadmin.showLayer[i].title == 'Создание слоя') {
                            Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayer[i], true);
                            delete Objectadmin.showLayer[i];
                        }
                    }
                }
            }
        }, this);
        //Сохранить
        this.on('save', function() {
            var msgWait = Ext.Msg.wait('Сохранение...', 'Подождите', {
                interval: 2000
            });
            this.submit({
                success: function(form, action) {
                    if (self.owner.featureLayerListGrid) {
                        self.owner.featureLayerListGrid.storeJS.reload();
                    }
                    Objectadmin.objectTreePanel.refreshNode();
                    self.setTitle(self.getForm().getValues().name);
                    self.attrTypeListPanel.attrTypeListGrid.storeJS.reload();
                    self.saveButton.setText('Применить');
                    msgWait.hide();
                },
                failure: function(form, action) {
                    if (self.layer_Id == null) {
                        if (action.response && action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.layer_Id = action.response.responseText;
                                self.getForm().setValues({
                                    id: self.layer_Id
                                });
                                self.attrTypeListPanel.CurrentButton.enable();
                                self.attrTypeListPanel.UnCurrentButton.enable();
                                self.attrTypeListPanel.BindButton.enable();
                                self.attrTypeListPanel.unBindButton.enable();
                                self.attrTypeListPanel.layer_Id = self.layer_Id;
                                self.attrTypeListPanel.attrTypeListGrid.setLayerId(self.layer_Id);
                                self.attrTypeListPanel.attrTypeListGrid.storeJS.baseParams.layerId = self.layer_Id;
                                self.attrTypeListPanel.attrTypeListGrid.storeJS.reload();
                                self.getForm().url = '/objectadmin/EditFeatureLayer';
                                self.setTitle(self.getForm().getValues(false).name);
                                if (self.owner.featureLayerListGrid) {
                                    self.owner.featureLayerListGrid.storeJS.reload();
                                }
                                Objectadmin.showLayer[self.layer_Id] = Objectadmin.showLayer[0];
                                delete Objectadmin.showLayer[0];
                                Objectadmin.objectTreePanel.refreshNode();
                                self.saveButton.setText('Применить');
                            }
                        }
                    }
                    msgWait.hide();
                }
            });
        }, this);
        this.on('destroy', function() {
            if (this.layer_Id == null) {
                delete Objectadmin.showLayer[0];
            } else {
                delete Objectadmin.showLayer[this.layer_Id];
            }
        }, this);
        FeatureLayerEditFormPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-ОКНО РЕДАКТИРОВАНИЯ СЛОЯ-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
FeatureLayerEditWindow = function(owner) {
    this.owner = owner;
    this.featureLayerEditFormPanel = new FeatureLayerEditFormPanel(this, 1);
    FeatureLayerEditWindow.superclass.constructor.call(this, {
        title: '',
        layout: 'fit',
        resizable: false,
        height: 300,
        width: 300,
        modal: true,
        items: [this.featureLayerEditFormPanel]
    });
}
Ext.extend(FeatureLayerEditWindow, Ext.Window, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-GRID СПИСКА ГРУПП СЛОЕВ-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
LayerGroupListGrid = function(owner) {
    this.owner = owner;
    //--------Массив со списком групп слоев----
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name'],
        url: '/objectadmin/GetLayerGroupList'
    });
    //-------Конструктор грида-----------
    LayerGroupListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'layerGroupId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'layerGroupName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }]
    });
}
Ext.extend(LayerGroupListGrid, Ext.grid.GridPanel, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА ГРУПП СЛОЕВ-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
LayerGroupListPanel = function(owner) {
    this.owner = owner;
    this.layerGroupListGrid = new LayerGroupListGrid(this);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('AddLayerGroup')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('EditLayerGroup')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('RemoveLayerGroup')
        },
        scope: this
    });

    //--------Панель----------
    LayerGroupListPanel.superclass.constructor.call(this, {
        title: 'Список групп слоев',
        closable: true,
        autoScroll: true,
        layout: 'fit',
        frame: true,
        tbar: [this.addButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.layerGroupListGrid]
    });
}
Ext.extend(LayerGroupListPanel, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            AddLayerGroup: true,
            EditLayerGroup: true,
            RemoveLayerGroup: true
        });
        this.on('RemoveLayerGroup', function() {
            var self = this;
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите удалить группу слоев?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId) {
                    if (buttonId == 'yes') {
                        var msgWait = Ext.Msg.wait('Удаление группы слоев...', 'Подождите', {
                            interval: 50
                        });
                        Objectadmin.conn.request({
                            url: '/objectadmin/RemoveLayerGroup',
                            method: 'POST',
                            params: {
                                objectid: self.layerGroupListGrid.getSelectionModel().getSelected().data.id
                            },
                            success: function() {
                                msgWait.hide();
                                Objectadmin.objectTreePanel.refreshNode();
                            },
                            failure: function() {
                                msgWait.hide();
                                Ext.MessageBox.alert('', 'Группу слоев удалить не удалось!');
                            }
                        });
                        this.layerGroupListGrid.storeJS.reload();
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }, this);

        this.on('AddLayerGroup', function() {
            this.layerGroupEditWindow = new LayerGroupEditWindow(this);
            this.layerGroupEditWindow.setTitle('Создание группы слоев');
            this.layerGroupEditWindow.layerGroupEditFormPanel.getForm().url = '/objectadmin/AddLayerGroup';
            this.layerGroupEditWindow.show();
        }, this);

        this.on('EditLayerGroup', function() {
            var layerGroupForChange = this.layerGroupListGrid.getSelectionModel().getSelections();
            if (layerGroupForChange.length != 0) {
                if (layerGroupForChange.length == 1) {
                    this.layerGroupEditFormPanel = new LayerGroupEditFormPanel(this, layerGroupForChange[0].data.id);
                    this.layerGroupEditFormPanel.setTitle(layerGroupForChange[0].data.name);
                    this.layerGroupEditFormPanel.getForm().url = '/objectadmin/EditLayerGroup';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(this.layerGroupEditFormPanel));
                    this.layerGroupEditFormPanel.doLayout();
                    this.layerGroupEditFormPanel.getForm().loadRecord(layerGroupForChange[0]);

                }
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showLayerGroupListPanel = false;
        }, this);

        LayerGroupListPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ФОРМА РЕДАКТИРОВАНИЯ ГРУППЫ СЛОЕВ-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
LayerGroupEditFormPanel = function(owner, group_Id) {
    this.owner = owner;
    this.group_Id = group_Id;
    this.group_Name = '';

    if (this.group_Id == null) {
        this.featureLayerListPanel = new FeatureLayerListPanel(this, null, -1);
        this.featureLayerListPanel.addButton.disable();
        this.featureLayerListPanel.editButton.disable();
        this.featureLayerListPanel.removeButton.disable();
    } else {
        this.featureLayerListPanel = new FeatureLayerListPanel(this, null, group_Id);
    }

    this.featureLayerListPanel.setTitle('Слои группы');

    LayerGroupEditFormPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-layer-group',
        closable: true,
        labelWidth: 150,
        anchor: '100% 100%',
        items: [{
            anchor: '100% 100%',
            layout: 'border',
            items: [{
                region: 'north',
                height: 105,
                items: [
                this.GeneralParam = new Ext.form.FieldSet({
                    title: 'Основные параметры',
                    autoHeight: true,
                    defaultType: 'textfield',
                    items: [{
                        fieldLabel: 'Наименование',
                        name: 'name',
                        anchor: '100%',
                        allowBlank: false
                    }, {
                        xtype: 'hidden',
                        name: 'id',
                        hidden: true,
                        labelSeparator: ''
                    }]
                })]
            },
            this.featureLayerListPanel]
        }]
    });
    this.saveButton = this.GeneralParam.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.GeneralParam.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(LayerGroupEditFormPanel, Ext.form.WgsFormPanel, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;
        //Отмена
        this.on('cancel', function() {
            if (this.group_Id) {
                Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayerGroup[this.group_Id], true);
                delete Objectadmin.showLayerGroup[this.group_Id];
            } else {
                for (var i = 0; i < Objectadmin.showLayerGroup.length; i++) {
                    if (Objectadmin.showLayerGroup[i]) {
                        if (Objectadmin.showLayerGroup[i].title == 'Создание группы слоев') {
                            Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayerGroup[i], true);
                            delete Objectadmin.showLayerGroup[i];
                        }
                    }
                }
            }
        }, this);
        //Сохранить
        this.on('save', function() {
            this.submit({
                success: function(form, action) {
                    Objectadmin.objectTreePanel.refreshNode();
                    self.setTitle(self.getForm().getValues().name);
                    self.featureLayerListPanel.featureLayerListGrid.storeJS.reload();
                    self.saveButton.setText('Применить');
                },
                failure: function(form, action) {
                    if (self.group_Id == null) {
                        if (action.response && action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.group_Id = action.response.responseText;
                                self.getForm().setValues({
                                    id: self.group_Id
                                });
                                self.featureLayerListPanel.addButton.enable();
                                self.featureLayerListPanel.editButton.enable();
                                self.featureLayerListPanel.removeButton.enable();

                                self.featureLayerListPanel.group_Id = self.group_Id;
                                self.featureLayerListPanel.featureLayerListGrid.group_Id = self.group_Id;
                                self.featureLayerListPanel.featureLayerListGrid.storeJS.baseParams.groupId = self.group_Id;
                                self.featureLayerListPanel.featureLayerListGrid.storeJS.reload();
                                self.getForm().url = '/objectadmin/EditLayerGroup';
                                self.setTitle(self.getForm().getValues(false).name);
                                Objectadmin.showLayerGroup[self.group_Id] = Objectadmin.showLayerGroup[0];
                                delete Objectadmin.showLayerGroup[0];
                                Objectadmin.objectTreePanel.refreshNode();
                                self.saveButton.setText('Применить');
                            }
                        }
                    }
                }
            });
        }, this);

        this.on('destroy', function() {
            if (self.group_Id == null) {
                delete Objectadmin.showLayerGroup[0];
            } else {
                delete Objectadmin.showLayerGroup[this.group_Id];
            }
        }, this);
        LayerGroupEditFormPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ОКНО РЕДАКТИРОВАНИЯ ГРУППЫ СЛОЕВ-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
LayerGroupEditWindow = function(owner) {
    this.owner = owner;
    this.layerGroupEditFormPanel = new LayerGroupEditFormPanel(this, null);
    LayerGroupEditWindow.superclass.constructor.call(this, {
        title: '',
        layout: 'fit',
        resizable: false,
        height: 120,
        width: 300,
        modal: true,
        items: [this.layerGroupEditFormPanel]
    });
}
Ext.extend(LayerGroupEditWindow, Ext.Window, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-GRID СПИСКА ГРУПП ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropGroupListGrid = function(owner) {
    this.owner = owner;
    //--------Массив со списком групп слоев----
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name'],
        url: '/objectadmin/GetPropGroupList'
    });
    //-------Конструктор грида-----------
    PropGroupListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'propGroupId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'propGroupName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }]
    });
}
Ext.extend(PropGroupListGrid, Ext.grid.GridPanel, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА ГРУПП ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropGroupListPanel = function(owner) {
    this.owner = owner;
    this.propGroupListGrid = new PropGroupListGrid(this);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('AddPropGroup')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('EditPropGroup')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('RemovePropGroup')
        },
        scope: this
    });

    //--------Панель----------
    PropGroupListPanel.superclass.constructor.call(this, {
        title: 'Список групп характеристик',
        closable: true,
        autoScroll: true,
        layout: 'fit',
        frame: true,
        tbar: [this.addButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.propGroupListGrid]
    });
}
Ext.extend(PropGroupListPanel, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            AddPropGroup: true,
            EditPropGroup: true,
            RemovePropGroup: true
        });
        this.on('RemovePropGroup', function() {
            var self = this;
            Objectadmin.conn.request({
                url: '/objectadmin/RemovePropGroup',
                method: 'POST',
                params: {
                    id: self.propGroupListGrid.getSelectionModel().getSelected().data.id
                },
                success: function() {
                    Objectadmin.objectTreePanel.refreshNode();
                }

            });
            this.propGroupListGrid.storeJS.reload();
        }, this);

        this.on('AddPropGroup', function() {
            this.propGroupEditWindow = new PropGroupEditWindow(this);
            this.propGroupEditWindow.setTitle('Создание группы характеристик');
            this.propGroupEditWindow.propGroupEditFormPanel.getForm().url = '/objectadmin/AddPropGroup';
            this.propGroupEditWindow.show();
        }, this);

        this.on('EditPropGroup', function() {
            var propGroupForChange = this.propGroupListGrid.getSelectionModel().getSelections();
            if (propGroupForChange.length != 0) {
                if (propGroupForChange.length == 1) {
                    this.propGroupEditWindow = new PropGroupEditWindow(this);
                    this.propGroupEditWindow.setTitle(propGroupForChange[0].data.name);
                    this.propGroupEditWindow.propGroupEditFormPanel.getForm().url = '/objectadmin/EditPropGroup';
                    this.propGroupEditWindow.show();
                    this.propGroupEditWindow.propGroupEditFormPanel.getForm().loadRecord(propGroupForChange[0]);
                    this.propGroupEditWindow.propGroupEditFormPanel.getForm().url = '/objectadmin/EditPropGroup';
                }
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showPropGroupListPanel = false;
        }, this);

        PropGroupListPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-ФОРМА РЕДАКТИРОВАНИЯ ГРУППЫ ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropGroupEditFormPanel = function(owner, group_Id) {
    this.owner = owner;
    this.group_Id = group_Id;
    this.group_Name = '';

    if (this.group_Id == null) {
        this.propListPanel = new PropListPanel(this, null, -1);
        this.propListPanel.addButton.disable();
        this.propListPanel.editButton.disable();
        this.propListPanel.removeButton.disable();
    } else {
        this.propListPanel = new PropListPanel(this, null, group_Id);
    }


    this.propListPanel.setTitle('Характеристики группы');

    PropGroupEditFormPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-property-group',
        closable: true,
        labelWidth: 150,
        anchor: '100% 100%',
        items: [{
            anchor: '100% 100%',
            layout: 'border',
            items: [{
                region: 'north',
                height: 110,
                items: [
                this.GeneralParam = new Ext.form.FieldSet({
                    title: 'Основные параметры',
                    autoHeight: true,
                    defaultType: 'textfield',
                    items: [{
                        fieldLabel: 'Наименование',
                        anchor: '100%',
                        name: 'name',
                        allowBlank: false
                    }, {
                        xtype: 'hidden',
                        name: 'id',
                        hidden: true,
                        labelSeparator: ''
                    }]
                })]
            },
            this.propListPanel]
        }]
    });
    this.saveButton = this.GeneralParam.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.GeneralParam.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(PropGroupEditFormPanel, Ext.form.WgsFormPanel, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;
        //Отмена
        this.on('cancel', function() {
            if (this.group_Id) {
                Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showPropGroup[this.group_Id], true);
                delete Objectadmin.showPropGroup[this.group_Id];
            } else {
                for (var i = 0; i < Objectadmin.showPropGroup.length; i++) {
                    if (Objectadmin.showPropGroup[i]) {
                        if (Objectadmin.showPropGroup[i].title == 'Создание группы характеристик') {
                            Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showPropGroup[i], true);
                            delete Objectadmin.showPropGroup[i];
                        }
                    }
                }
            }
        }, this);
        //Сохранить
        this.on('save', function() {
            this.submit({
                success: function(form, action) {
                    Objectadmin.objectTreePanel.refreshNode();
                    self.setTitle(self.getForm().getValues().name);
                    self.propListPanel.propListGrid.storeJS.reload();
                    self.saveButton.setText('Применить');
                },
                failure: function(form, action) {
                    if (self.group_Id == null) {
                        if (action.response && action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.group_Id = action.response.responseText;
                                self.getForm().setValues({
                                    id: self.group_Id
                                });
                                self.propListPanel.addButton.enable();
                                self.propListPanel.editButton.enable();
                                self.propListPanel.removeButton.enable();

                                self.propListPanel.group_Id = self.group_Id;
                                self.propListPanel.propListGrid.group_Id = self.group_Id;
                                self.propListPanel.propListGrid.storeJS.baseParams.groupId = self.group_Id;
                                self.propListPanel.propListGrid.storeJS.reload();
                                self.getForm().url = '/objectadmin/EditPropGroup';
                                self.setTitle(self.getForm().getValues(false).name);
                                Objectadmin.showPropGroup[self.group_Id] = Objectadmin.showPropGroup[0];
                                delete Objectadmin.showPropGroup[0];
                                Objectadmin.objectTreePanel.refreshNode();
                                self.saveButton.setText('Применить');
                            }
                        }
                    }
                }
            });
        }, this);
        this.on('destroy', function() {
            if (this.group_Id == null) {
                delete Objectadmin.showPropGroup[0];
            } else {
                delete Objectadmin.showPropGroup[this.group_Id];
            }

        }, this);
        PropGroupEditFormPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-ОКНО РЕДАКТИРОВАНИЯ ГРУППЫ ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropGroupEditWindow = function(owner) {
    this.owner = owner;
    this.propGroupEditFormPanel = new PropGroupEditFormPanel(this);
    PropGroupEditWindow.superclass.constructor.call(this, {
        title: '',
        layout: 'fit',
        resizable: false,
        height: 120,
        width: 300,
        modal: true,
        items: [this.propGroupEditFormPanel]
    });
}
Ext.extend(PropGroupEditWindow, Ext.Window, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-GRID СПИСКА ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropListGrid = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    //--------Массив со списком групп слоев----
    var self = this;
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'autoCADname', 'tagname', 'groupid', 'groupname', 'valuetypeid', 'valuetypename', 'placdate', 'acadview'],
        url: '/objectadmin/GetPropList',
        baseParams: {
            typeId: self.type_Id,
            groupId: self.group_Id
        }
    });
    //-------Конструктор грида-----------
    PropListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        store: this.storeJS,
        autoScroll: true,
        frame: true,
        columns: [{
            id: 'propId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'groupId',
            dataIndex: 'groupid',
            hideable: false,
            hidden: true
        }, {
            id: 'acadview',
            dataIndex: 'acadview',
            hideable: false,
            hidden: true
        }, {
            id: 'valueTypeId',
            dataIndex: 'valuetypeid',
            hideable: false,
            hidden: true
        }, {
            id: 'propName',
            dataIndex: 'name',
            hideable: false,
            hidden: true
        }, {
            id: 'autoCADname',
            dataIndex: 'autoCADname',
            hideable: false,
            hidden: true
        }, {
            id: 'propTagName',
            dataIndex: 'tagname',
            header: "Наименование",
            sortable: true
        }, {
            id: 'groupName',
            dataIndex: 'groupname',
            header: "Наименование группы",
            sortable: true
        }, {
            id: 'valueTypeName',
            dataIndex: 'valuetypename',
            header: "Тип значения",
            sortable: true
        }, {
            id: 'placDate',
            dataIndex: 'placdate',
            header: "Дата создания",
            sortable: true
        }]
    });
}
Ext.extend(PropListGrid, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);

        PropListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropListPanel = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    this.propListGrid = new PropListGrid(this, this.type_Id, this.group_Id);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('AddProp')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('EditProp')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('RemoveProp')
        },
        scope: this
    });

    //--------Панель----------
    PropListPanel.superclass.constructor.call(this, {
        title: 'Список характеристик',
        region: 'center',
        layout: 'fit',
        tbar: [this.addButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.propListGrid]
    });
}
Ext.extend(PropListPanel, Ext.form.FieldSet, {
    initComponent: function() {
        this.addEvents({
            AddProp: true,
            EditProp: true,
            RemoveProp: true
        });
        var self = this;
        this.on('RemoveProp', function() {
            Ext.Msg.show({
                title: 'Предупреждение',
                msg: 'При удалении характеристики вы можете потерять данные,\n' + 'если она привязана к одному из атрибутивных описаний!' + 'Вы уверены что хотите удалить характеристику?',
                fn: function(btn, text) {
                    var msgWait = Ext.Msg.wait('Удаление характеристики...', 'Подождите', {
                        interval: 50
                    });
                    Objectadmin.conn.request({
                        url: '/objectadmin/RemoveProp',
                        method: 'POST',
                        params: {
                            id: self.propListGrid.getSelectionModel().getSelected().data.id
                        },
                        success: function() {
                            msgWait.hide();
                            self.propListGrid.storeJS.reload();
                            Objectadmin.objectTreePanel.refreshNode();
                        },
                        failure: function() {
                            msgWait.hide();
                            Ext.Msg.show({
                                title: 'Ошибка',
                                msg: 'Удалить характеристику не удалось.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    });
                },
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.WARNING
            });
        }, this);

        this.on('AddProp', function() {
            if (0 in Objectadmin.showProp) {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо закончить создание одной характеристики, чтобы преступить к новому!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            } else {
                Objectadmin.showProp[0] = new PropEditFormPanel(this, null);
                Objectadmin.showProp[0].setTitle('Создание характеристики');
                Objectadmin.showProp[0].getForm().url = '/objectadmin/AddProp';
                Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showProp[0]));
                Objectadmin.showProp[0].doLayout();
                Objectadmin.showProp[0].propName = '';
                Objectadmin.showProp[0].propGroup = self.group_Id;
                Objectadmin.showProp[0].getForm().setValues({
                    placdate: new Date()
                });
                Objectadmin.showProp[0].getForm().setValues({
                    groupid: self.group_Id
                });
                Objectadmin.showProp[0].groupStoreJS.load({
                    callback: function() {
                        if (Objectadmin.showProp[0]) {
                            Objectadmin.showProp[0].groupComboBox.setValue(self.group_Id);
                        }
                    }
                });

                Objectadmin.showProp[0].valueTypeStoreJS.load({
                    callback: function(records) {
                        if (records) {
                            if (records.length > 0) {
                                Objectadmin.showProp[0].valueTypeComboBox.setValue(records[0].data.id);
                            }
                        }
                    }
                });
            }
        }, this);

        this.on('EditProp', function() {
            var propForChange = this.propListGrid.getSelectionModel().getSelections();
            if (propForChange.length != 0) {
                if (propForChange.length == 1) {
                    if (propForChange[0].data.id in Objectadmin.showProp) {
                        Objectadmin.showProp[propForChange[0].data.id].setTitle(propForChange[0].data.name);
                        Objectadmin.showProp[propForChange[0].data.id].getForm().url = '/objectadmin/EditProp';
                        Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showProp[propForChange[0].data.id]);
                        Objectadmin.showProp[propForChange[0].data.id].doLayout();

                        Objectadmin.showProp[propForChange[0].data.id].propName = propForChange[0].data.name;
                        Objectadmin.showProp[propForChange[0].data.id].propCADName = propForChange[0].data.autoCADname;
                        Objectadmin.showProp[propForChange[0].data.id].propGroup = propForChange[0].data.groupid;
                        Objectadmin.showProp[propForChange[0].data.id].propValueType = propForChange[0].data.valuetypeid;

                        Objectadmin.showProp[propForChange[0].data.id].getForm().loadRecord(propForChange[0]);

                        Objectadmin.showProp[propForChange[0].data.id].getForm().setValues({
                            groupid: propForChange[0].data.groupname,
                            valuetypeid: propForChange[0].data.valuetypename,
                            autoCADname: propForChange[0].data.autoCADname
                        });

                        Objectadmin.showProp[propForChange[0].data.id].groupStoreJS.load({
                            callback: function() {
                                if (Objectadmin.showProp[propForChange[0].data.id]) {
                                    Objectadmin.showProp[propForChange[0].data.id].groupComboBox.setValue(propForChange[0].data.groupid);
                                }
                            }
                        });
                        Objectadmin.showProp[propForChange[0].data.id].valueTypeStoreJS.load({
                            callback: function() {
                                if (Objectadmin.showProp[propForChange[0].data.id]) {
                                    Objectadmin.showProp[propForChange[0].data.id].valueTypeComboBox.setValue(propForChange[0].data.valuetypeid);
                                }
                            }
                        });
                    } else {
                        Objectadmin.showProp[propForChange[0].data.id] = new PropEditFormPanel(this, propForChange[0].data.id);
                        Objectadmin.showProp[propForChange[0].data.id].setTitle(propForChange[0].data.name);
                        Objectadmin.showProp[propForChange[0].data.id].getForm().url = '/objectadmin/EditProp';
                        Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showProp[propForChange[0].data.id]));
                        Objectadmin.showProp[propForChange[0].data.id].doLayout();

                        Objectadmin.showProp[propForChange[0].data.id].propName = propForChange[0].data.name;
                        Objectadmin.showProp[propForChange[0].data.id].propCADName = propForChange[0].data.autoCADname;
                        Objectadmin.showProp[propForChange[0].data.id].propGroup = propForChange[0].data.groupid;
                        Objectadmin.showProp[propForChange[0].data.id].propValueType = propForChange[0].data.valuetypeid;
                        if (propForChange[0].data.valuetypeid == 5) {
                            Objectadmin.showProp[propForChange[0].data.id].valuePropListPanel.show();
                        }

                        Objectadmin.showProp[propForChange[0].data.id].getForm().loadRecord(propForChange[0]);

                        Objectadmin.showProp[propForChange[0].data.id].getForm().setValues({
                            groupid: propForChange[0].data.groupname,
                            valuetypeid: propForChange[0].data.valuetypename,
                            autoCADname: propForChange[0].data.autoCADname
                        });

                        Objectadmin.showProp[propForChange[0].data.id].groupStoreJS.load({
                            callback: function() {
                                if (Objectadmin.showProp[propForChange[0].data.id]) {
                                    Objectadmin.showProp[propForChange[0].data.id].groupComboBox.setValue(propForChange[0].data.groupid);
                                }
                            }
                        });
                        Objectadmin.showProp[propForChange[0].data.id].valueTypeStoreJS.load({
                            callback: function() {
                                if (Objectadmin.showProp[propForChange[0].data.id]) {
                                    Objectadmin.showProp[propForChange[0].data.id].valueTypeComboBox.setValue(propForChange[0].data.valuetypeid);
                                }
                            }
                        });
                    }
                }
            }
        }, this);

        this.on('destroy', function() {
            this.owner.showPropListPanel = false;
        }, this);

        PropListPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-GRID СПИСКА ХАРАКТЕРИСТИК ДЛЯ АТРИБУТИВНОГО ТИПА-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropListGridAttrType = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    //--------Массив со списком групп слоев----
    var self = this;
    this.storeJS = new Ext.data.GroupingStore({
        autoLoad: true,
        url: '/objectadmin/GetPropList',
        reader: new Ext.data.JsonReader({}, [{
            name: 'id'
        }, {
            name: 'name'
        }, {
            autoCADname: 'autoCADname'
        }, {
            name: 'tagname'
        }, {
            name: 'groupid'
        }, {
            name: 'groupname'
        }, {
            name: 'valuetypeid'
        }, {
            name: 'valuetypename'
        }, {
            name: 'placdate'
        }, {
            name: 'acadview'
        }, {
            name: 'grouppriority'
        }, {
            name: 'priority'
        }

        ]),
        sortInfo: {
            field: 'grouppriority',
            direction: "ASC"
        },
        groupField: 'groupname',
        remoteGroup: true,
        baseParams: {
            typeId: self.type_Id,
            groupId: self.group_Id
        }
    });
    //-------Конструктор грида-----------
    PropListGridAttrType.superclass.constructor.call(this, {
        view: new Ext.grid.GroupingView({
            groupTextTpl: '{text} ( Количество: {[values.rs.length]} )',
            enableGroupingMenu: false,
            forceFit: true
        }),
        store: this.storeJS,
        iconCls: 'icon-grid',
        autoScroll: true,
        enableHdMenu: false,
        columns: [{
            id: 'priority',
            dataIndex: 'priority',
            hideable: false,
            hidden: true
        }, {
            id: 'grouppriority',
            dataIndex: 'grouppriority',
            hideable: false,
            hidden: true
        }, {
            id: 'propId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'groupId',
            dataIndex: 'groupid',
            hideable: false,
            hidden: true
        }, {
            id: 'acadview',
            dataIndex: 'acadview',
            hideable: false,
            hidden: true
        }, {
            id: 'valueTypeId',
            dataIndex: 'valuetypeid',
            hideable: false,
            hidden: true
        }, {
            id: 'propName',
            dataIndex: 'name',
            hideable: false,
            hidden: true
        }, {
            id: 'autoCADname',
            dataIndex: 'autoCADname',
            hideable: false,
            hidden: true
        }, {
            id: 'propTagName',
            dataIndex: 'tagname',
            header: "Наименование",
            sortable: true
        }, {
            id: 'groupName',
            dataIndex: 'groupname',
            header: "Наименование группы",
            sortable: true
        }, {
            id: 'valueTypeName',
            dataIndex: 'valuetypename',
            header: "Тип значения",
            sortable: true
        }, {
            id: 'placDate',
            dataIndex: 'placdate',
            header: "Дата создания",
            sortable: true
        }]
    });
}
Ext.extend(PropListGridAttrType, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);

        this.storeJS.on('load', function(store, records, options) {
            if (records.length > 0) {
                this.getSelectionModel().selectFirstRow();
            }
        }, this);

        PropListGridAttrType.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-ПАНЕЛЬ СПИСКА ХАРАКТЕРИСТИК ДЛЯ АТРИБУТИВНОГО ТИПА-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropListPanelAttrType = function(owner, type_Id, group_Id) {
    this.owner = owner;
    this.type_Id = type_Id;
    this.group_Id = group_Id;
    this.propListGrid = new PropListGridAttrType(this, this.type_Id, this.group_Id);
    //--------Кнопки ToolBar'а----------
    this.bindButton = new Ext.Button({
        text: 'Привязать характеристику',
        handler: function() {
            this.fireEvent('bindProp')
        },
        scope: this
    });
    this.unBindButton = new Ext.Button({
        text: 'Отвязать характеристику',
        handler: function() {
            this.fireEvent('unBindProp')
        },
        scope: this
    });
    this.currentButton = new Ext.Button({
        text: 'Добавить к объектным данным',
        handler: function() {
            this.fireEvent('currentProp')
        },
        scope: this
    });
    this.unCurrentButton = new Ext.Button({
        text: 'Убрать из объектных данных',
        handler: function() {
            this.fireEvent('unCurrentProp')
        },
        scope: this
    });
    //--------Панель----------
    this.exportButton = new Ext.Button({
        text: 'Экспорт данных',
        handler: function() {
            this.fireEvent('exportData')
        },
        scope: this
    });
    this.importButton = new Ext.Button({
        text: 'Импорт данных',
        handler: function() {
            this.fireEvent('importData')
        },
        scope: this
    });
    //--------Панель----------
    PropListPanelAttrType.superclass.constructor.call(this, {
        layout: 'fit',
        frame: false,
        tbar: [this.bindButton, this.unBindButton, this.currentButton, this.unCurrentButton, '-', this.exportButton, this.importButton], //,
        items: [this.propListGrid]
    });
}
Ext.extend(PropListPanelAttrType, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            bindProp: true,
            unBindProp: true,
            currentProp: true,
            unCurrentProp: true,
            exportData: true,
            importData: true
        });
        var self = this;
        this.on('currentProp', function() {
            var listSelectedProperties = self.propListGrid.getSelectionModel().getSelections();
            if (listSelectedProperties.length != 0) {
                for (var i = 0; i < listSelectedProperties.length; i++) {
                    var propName = listSelectedProperties[i].data.name;
                    if (listSelectedProperties[i].data.acadview == 0) {
                        if (
                            (listSelectedProperties[i].data.valuetypeid == 1) || 
                            (listSelectedProperties[i].data.valuetypeid == 3) || 
                            (listSelectedProperties[i].data.valuetypeid == 4) || 
                            (listSelectedProperties[i].data.valuetypeid == 5) || 
                            (listSelectedProperties[i].data.valuetypeid == 9)) {
                            var msgWait = Ext.Msg.wait('Добавление характеристики "' + propName + '" к объектным данным...', 'Подождите', {
                                interval: 50
                            });
                            Objectadmin.conn.request({
                                url: '/objectadmin/SetPropertyByObjectType',
                                method: 'POST',
                                params: {
                                    typeid: self.type_Id,
                                    propertyid: listSelectedProperties[i].data.id,
                                    aCadView: 1
                                },
                                success: function() {
                                    self.propListGrid.storeJS.reload();
                                    msgWait.hide();
                                    Objectadmin.objectTreePanel.refreshNode();
                                }
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Ошибка',
                                msg: 'Характеристика "' + propName + '" имеет не подходящий тип значения \n ' + 'для добавления ее к объектным данным! \n' + 'Необходим тип значения: строка, число целое, число вещественное, дата-время',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка',
                            msg: 'Характеристика "' + propName + '" уже добавлена к объектным данным.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            }
        });
        this.on('unCurrentProp', function() {
            var listSelectedProperties = self.propListGrid.getSelectionModel().getSelections();
            if (listSelectedProperties.length != 0) {
                for (var i = 0; i < listSelectedProperties.length; i++) {
                    var propName = listSelectedProperties[i].data.name;
                    if (listSelectedProperties[i].data.acadview == 1) {
                        var msgWait = Ext.Msg.wait('Удаление характеристики "' + propName + '" из объектных данных...', 'Подождите', {
                            interval: 50
                        });
                        Objectadmin.conn.request({
                            url: '/objectadmin/SetPropertyByObjectType',
                            method: 'POST',
                            params: {
                                typeid: self.type_Id,
                                propertyid: listSelectedProperties[i].data.id,
                                aCadView: 0
                            },
                            success: function() {
                                self.propListGrid.storeJS.reload();
                                msgWait.hide();
                                Objectadmin.objectTreePanel.refreshNode();
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка',
                            msg: 'Характеристика "' + propName + '" не входит в объектные данные.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            }
        });
        this.on('bindProp', function() {
            self.bindPropFormWindow = new BindPropFormWindow(self, self.type_Id);
            self.bindPropFormWindow.setTitle('Привязка характеристик');
            self.bindPropFormWindow.show();
        });
        this.on('unBindProp', function() {
            var listSelectedProperties = self.propListGrid.getSelectionModel().getSelections();
            if (listSelectedProperties.length != 0) {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'У некоторых объектов данная характеристика может быть заполнена.\n' + 'Ее отвязка может привети к потере данных!\n' + 'Вы уверены, что хотите продолжить отвязку?',
                    fn: function(btn, text) {
                        if (btn == 'yes') {
                            var propertiesForUnBind = [];
                            var propertiesForUnBindACAD = [];
                            for (var i = 0; i < listSelectedProperties.length; i++) {
                                propertiesForUnBind[propertiesForUnBind.length] = listSelectedProperties[i].data.id;
                                propertiesForUnBindACAD[propertiesForUnBindACAD.length] = listSelectedProperties[i].data.acadview;
                            }
                            var msgWait = Ext.Msg.wait('Отвязка характеристики от атрибутивного описания...', 'Подождите', {
                                interval: 50
                            });
                            Objectadmin.conn.request({
                                url: '/objectadmin/RemovePropertyByObjectType',
                                method: 'POST',
                                params: {
                                    typeid: self.type_Id,
                                    'propertiesId[]': propertiesForUnBind,
                                    'aCadViews[]': propertiesForUnBindACAD
                                },
                                success: function() {
                                    self.propListGrid.storeJS.removeAll();
                                    self.propListGrid.storeJS.reload();
                                    msgWait.hide();
                                    Objectadmin.objectTreePanel.refreshNode();
                                }
                            });
                        }
                    },
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.MessageBox.QUESTION
                });
            }
        });
        this.on('importData', function() {
            UploadDialog = function() {
                window.propList = self.propListGrid.storeJS
                UploadDialog.superclass.constructor.call(this, {
                    url: '/map/featuredoc/importfile',
                    base_params: { /*'entityId': Ext.util.JSON.encode(Map.Featuredoc.entityId), 'doc_id' : this.doc_id*/},
                    reset_on_hide: true,
                    title: 'Шаг 1. Загрузка файла с данными',
                    allow_close_on_upload: false,
                    upload_autostart: false,
                    permitted_extensions: ['xls', 'xlsx'],
                    modal: true,
                    id: 'uploader'
                });
            }

            Ext.extend(UploadDialog, Ext.ux.UploadDialog.Dialog, {
                initComponent: function() {
                    this.on('fileuploadstart', function(dialog, filename, filecount) {});

                    this.on('uploadsuccess', function(dialog, filename) {});

                    this.on('uploadcomplete', function(response) {
                        var sm = new Ext.grid.CheckboxSelectionModel({
                            singleSelect: false,
                            listeners: {
                                rowselect: function(smObj, rowIndex, record) {
                                    setEnabled();
                                },
                                rowdeselect: function(smObj, rowIndex, record) {
                                    setEnabled();
                                }
                            }
                        })

                        var fields = [],
                            cols = [sm],
                            titles = []
                        if (response.RESP[2]) {
                            for (var i in response.RESP[2]) {
                                fields[fields.length] = i
                                cols[cols.length] = {
                                    header: response.RESP[2][i],
                                    dataIndex: i,
                                    hidden: false
                                }
                                titles[titles.length] = []
                                titles[titles.length - 1][0] = i
                                titles[titles.length - 1][1] = response.RESP[2][i]
                            }
                            var data = []
                            for (var i in response.RESP) {
                                if (i > 2) {
                                    data[data.length] = [];
                                    for (var j in response.RESP[i])
                                    data[data.length - 1][data[data.length - 1].length] = response.RESP[i][j];
                                }
                            }
                            //console.log(data)
                            var store = new Ext.data.SimpleStore({
                                fields: fields,
                                data: data,
                                autoLoad: true
                            });
                            var store4combo = new Ext.data.SimpleStore({
                                fields: ['myId', 'myText'],
                                data: titles,
                                autoLoad: false
                            });
                            var pl = []
                            for (var i = 0; i < window.propList.data.items.length; i++) {
                                pl[pl.length] = []
                                pl[pl.length - 1][0] = window.propList.data.items[i].data.id
                                pl[pl.length - 1][1] = window.propList.data.items[i].data.name
                            }
                            var store4property = new Ext.data.SimpleStore({
                                fields: ['myId', 'myText'],
                                data: pl,
                                autoLoad: false
                            });

                            var win = new Ext.Window({
                                layout: 'border',
                                id: 'importWizard',
                                title: 'Импорт данных из файла Microsoft Excel',
                                modal: true,
                                activeItem: 0,
                                width: 800,
                                height: 550,
                                plain: true,
                                resizable: true,

                                items: [
                                new Ext.form.FormPanel({
                                    id: 'card-0',
                                    region: 'center',
                                    layout: 'border',
                                    bodyStyle: 'border: none',
                                    items: [{
                                        xtype: 'fieldset',
                                        region: 'center',
                                        title: 'Шаг 2',
                                        layout: 'border',
                                        items: [new Ext.form.Label({
                                            region: 'north',
                                            html: '<span style="width:100%; text-align:center; padding-bottom:10px;">Выберите строки таблицы, которые необходимо восстановить:</span><p>&nbsp;</p>'
                                        }),
                                        new Ext.grid.GridPanel({
                                            id: 'card-0_grid',
                                            layout: 'fit',
                                            region: 'center',
                                            store: store, // хранилище. Важно! Required!
                                            cm: new Ext.grid.ColumnModel({ // Модель колонок
                                                columns: cols,
                                                defaultSortable: true
                                            }),
                                            viewConfig: {
                                                forceFit: true
                                            },
                                            sm: sm,
                                            title: 'Восстановленные данные'
                                        })]
                                    }, {
                                        xtype: 'fieldset',
                                        region: 'south',
                                        height: 145,
                                        title: 'Шаг 3',
                                        fieldDefaults: {
                                            msgTarget: 'side',
                                            labelWidth: 275
                                        },
                                        items: [
                                        new Ext.form.Label({
                                            html: '<span style="width:100%; text-align:center; padding-bottom:10px;">Выберите столбец в таблице, который содержит данные для восстановления, и восстанавливаемую характеристику:</span><p>&nbsp;</p>'
                                        }), {
                                            xtype: 'combo',
                                            fieldLabel: 'Столбец идентификаторов карточек',
                                            name: 'comboImportId',
                                            id: 'comboImportId',
                                            labelStyle: 'width: 275px;',
                                            typeAhead: true,
                                            forceSelection: true,
                                            mode: 'local',
                                            triggerAction: 'all',
                                            selectOnFocus: true,
                                            width: 200,
                                            store: store4combo,
                                            valueField: 'myId',
                                            displayField: 'myText',
                                            emptyText: 'Выберите столбец...',
                                            listeners: {
                                                scope: window,
                                                'select': setEnabled
                                            }
                                        }, {
                                            xtype: 'combo',
                                            fieldLabel: 'Столбец для восстановления данных',
                                            name: 'comboImportColumn',
                                            id: 'comboImportColumn',
                                            labelStyle: 'width: 275px;',
                                            typeAhead: true,
                                            forceSelection: true,
                                            mode: 'local',
                                            triggerAction: 'all',
                                            selectOnFocus: true,
                                            width: 200,
                                            store: store4combo,
                                            valueField: 'myId',
                                            displayField: 'myText',
                                            emptyText: 'Выберите столбец...',
                                            listeners: {
                                                scope: window,
                                                'select': setEnabled
                                            }
                                        }, {
                                            xtype: 'combo',
                                            fieldLabel: 'Восстанавливаемая характеристика',
                                            name: 'comboImportProperty',
                                            id: 'comboImportProperty',
                                            labelStyle: 'width: 275px;',
                                            width: 200,
                                            typeAhead: true,
                                            forceSelection: true,
                                            mode: 'local',
                                            triggerAction: 'all',
                                            selectOnFocus: true,
                                            store: store4property,
                                            valueField: 'myId',
                                            displayField: 'myText',
                                            emptyText: 'Выберите характеристику...',
                                            listeners: {
                                                scope: window,
                                                'select': setEnabled
                                            }
                                        }]
                                    }]
                                })],
                                buttons: [{
                                    iconCls: 'icon-accept-tick',
                                    text: 'Ок',
                                    id: 'importOk',
                                    disabled: true,
                                    handler: function() {
                                        Ext.Msg.show({
                                            title: 'Подтверждение импорта',
                                            msg: 'Вы уверены, что хотите перезаписать атрибутивные описания?',
                                            buttons: Ext.Msg.YESNO,
                                            fn: function(btn) {
                                                if (btn == 'yes') {
                                                    var objectColumn = Ext.getCmp('comboImportId').getValue();
                                                    var excelColumn = Ext.getCmp('comboImportColumn').getValue();
                                                    var propertyId = Ext.getCmp('comboImportProperty').getValue();
                                                    var selection = Ext.getCmp('card-0_grid').selModel.selections
                                                    var importData = []
                                                    for (var i in selection.items)
                                                    if (selection.items[i].data) {
                                                        var key = 'property-' + propertyId
                                                        importData[importData.length] = {
                                                            id: selection.items[i].data[objectColumn]
                                                        }
                                                        importData[importData.length - 1][key] = selection.items[i].data[excelColumn]
                                                    }
                                                    loadMask = new Ext.LoadMask(Ext.getCmp('importWizard').getEl(), {
                                                        msg: 'Загрузка...'
                                                    });
                                                    loadMask.show();

                                                    Ext.Ajax.request({
                                                        url: 'objectadmin/card/setCards',
                                                        success: function(response, options) {
                                                            Ext.MessageBox.alert('', 'Восстановление данных выполнено успешно'); //loadMask.hide()	
                                                            loadMask.hide();
                                                            Ext.getCmp('importWizard').close();
                                                            Ext.getCmp('uploader').close();
                                                        },
                                                        failure: function() {
                                                            // Сообщение об ошибке
                                                            //loadMask.hide()
                                                            Ext.MessageBox.alert('', 'Не удалось получить данные для экспорта. Обратитесь к администратору системы');
                                                            loadMask.hide();
                                                        },
                                                        params: { // параметры'', передаем
                                                            data: Ext.util.JSON.encode(importData)
                                                        },
                                                        method: 'POST'
                                                    });


                                                }
                                            }
                                        })

                                    }
                                }, {
                                    iconCls: 'icon-close-cross',
                                    text: 'Закрыть',
                                    handler: function() {
                                        win.close();
                                    }
                                }]
                            });

                            win.show();

                        } else Ext.MessageBox.alert('Ошибка импорта данных', 'Не удалось выполнить разбор документа Excel. Проверьте формат представления данных.');

                    }, this);

                    UploadDialog.superclass.initComponent.apply(this, arguments);
                }
            });

            var ud = new UploadDialog();
            ud.show();
        })

        this.on('exportData', function() {
            var propStoreJS = new Ext.data.GroupingStore({
                autoLoad: true,
                url: '/objectadmin/GetPropList',
                reader: new Ext.data.JsonReader({}, [{
                    name: 'id'
                }, {
                    name: 'name'
                }, {
                    autoCADname: 'autoCADname'
                }, {
                    name: 'tagname'
                }, {
                    name: 'groupid'
                }, {
                    name: 'groupname'
                }, {
                    name: 'valuetypeid'
                }, {
                    name: 'valuetypename'
                }, {
                    name: 'placdate'
                }, {
                    name: 'acadview'
                }, {
                    name: 'grouppriority'
                }, {
                    name: 'priority'
                }

                ]),
                sortInfo: {
                    field: 'groupname',
                    direction: "ASC"
                },
                groupField: 'groupname',
                remoteGroup: true,
                baseParams: {
                    typeId: self.type_Id
                }
            });

            propStoreJS.load();
            var sm = new Ext.grid.CheckboxSelectionModel({
                singleSelect: false
            })
            var grid = new Ext.grid.GridPanel({
                region: 'center',
                id: 'card-0',
                title: 'Параметры атрибутивного описания',
                store: propStoreJS,
                cm: new Ext.grid.ColumnModel({ // Модель колонок
                    columns: [sm, // Колонки
                    {
                        header: 'ID',
                        dataIndex: 'Id',
                        hidden: true
                    }, {
                        header: 'Наименование',
                        dataIndex: 'name',
                        hidden: false,
                        width: 100
                    }, {
                        header: 'Тип',
                        dataIndex: 'valuetypename',
                        hidden: false,
                        width: 100
                    }, {
                        header: 'Группа характеристик',
                        dataIndex: 'groupname',
                        hidden: true
                    }],
                    defaultSortable: true
                }),
                selModel: sm,
                view: new Ext.grid.GroupingView({
                    groupTextTpl: '{[values.rs[0].data["groupname"]]} ( Количество: {[values.rs.length]} )',
                    enableGroupingMenu: false,
                    forceFit: true
                })
            })

            var panel = new Ext.Window({
                title: 'Формирование отчетности',
                layout: 'fit',
                id: 'card-1_99',
                items: []
            })
            window.type_id = self.type_Id;
            window.win = new Ext.Window({
                layout: 'card',
                id: 'exportWizard',
                title: 'Экспорт данных в формате Microsoft Excel',
                modal: true,
                activeItem: 0, // index or id
                bbar: ['->', {
                    id: 'card-prev',
                    text: '&laquo; Назад',
                    handler: function() {
                        cardNav(-1)
                    },
                    disabled: true
                }, {
                    id: 'card-next',
                    text: 'Далее &raquo;',
                    handler: function() {
                        cardNav(1)
                    }
                }],
                width: 800,
                height: 550,
                plain: true,
                resizable: true,
                buttons: [{
                    iconCls: 'icon-close-cross',
                    text: 'Закрыть',
                    handler: function() {
                        win.close();
                        //win.destroy();
                        //win = null;
                    }
                }]
            });


            var propStoreJS2 = new Ext.data.WgsJsonStore({
                url: '/map/multipleFilling/getValues',
                root: 'record',
                fields: [
                    'objectId'],
                timeout: 45000,
                listeners: {
                    'load': function() {
                        loadMask.hide()
                    }

                },
                baseParams: {
                    //		'objectIds[]': objIds,
                    //		'propIds[]': propIds
                },
                totalproperty: 'total'
            });

            var grid2 = new Ext.grid.GridPanel({
                region: 'center',
                id: 'card-1',
                viewConfig: {
                    forceFit: true
                },

                title: 'Таблица наблюдений',
                store: propStoreJS2, // хранилище. Важно! Required!
                cm: new Ext.grid.ColumnModel({ // Модель колонок
                    columns: [ // Колонки						
                    ],
                    defaultSortable: true
                }),
                bbar: new Ext.PagingToolbar({
                    store: propStoreJS2,
                    id: 'pb',
                    pageSize: 36,
                    displayInfo: true,
                    displayMsg: 'Записи {0} - {1} из {2}',
                    emptyMsg: "Нет записей, удовлетворяющих условиям",

                })
            })

            win.add(grid);
            win.add(grid2);
            win.show();
        })
        PropListPanelAttrType.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ФОРМА РЕДАКТИРОВАНИЯ ХАРАКТЕРИСТИКИ-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
PropEditFormPanel = function(owner, prop_Id) {
    this.owner = owner;
    this.prop_Id = prop_Id;
    this.propName = '';
    this.autoCADname = '';
    this.propGroup = 0;
    this.propValueType = 0;

    this.groupStoreJS = new Ext.data.WgsJsonStore({
        fields: ['id', 'name'],
        url: '/objectadmin/GetPropGroupList'
    });
    this.valueTypeStoreJS = new Ext.data.WgsJsonStore({
        fields: ['id', 'name'],
        url: '/objectadmin/GetValueTypeList'
    });
    if (this.prop_Id == null) {
        this.valuePropListPanel = new ValuePropListPanel(this, -1);
        this.valuePropListPanel.addButton.disable();
        this.valuePropListPanel.editButton.disable();
        this.valuePropListPanel.removeButton.disable();
    } else {
        this.valuePropListPanel = new ValuePropListPanel(this, this.prop_Id);
    }
    this.valuePropListPanel.setTitle('Список значений');

    //---Проверка на выбранный тип значений---
    this.checkValueType = function(value) {
        if (value == 5) {
            this.valuePropListPanel.show();
        } else {
            this.valuePropListPanel.hide();
        }
    };

    this.checkValueType(this.propValueType);

    PropEditFormPanel.superclass.constructor.call(this, {
        frame: true,
        iconCls: 'icon-property',
        closable: true,
        labelWidth: 160,
        anchor: '100% 100%',
        items: [{
            anchor: '100% 100%',
            layout: 'border',
            items: [{
                region: 'north',
                height: 210,
                items: [
                this.GeneralParam = new Ext.form.FieldSet({
                    title: 'Основные параметры',
                    autoHeight: true,
                    defaultType: 'textfield',
                    items: [{
                        fieldLabel: 'Наименование',
                        name: 'name',
                        anchor: '100%',
                        allowBlank: false
                    }, {
                        fieldLabel: 'Наименование в AutoCAD',
                        name: 'autoCADname',
                        anchor: '100%',
                        allowBlank: true
                    },
                    this.groupComboBox = new Ext.form.ComboBox({
                        fieldLabel: 'Группа',
                        name: 'groupname',
                        anchor: '100%',
                        store: this.groupStoreJS,
                        displayField: 'name',
                        valueField: 'id',
                        hiddenName: 'groupid',
                        typeAhead: true,
                        loadingText: 'Загрузка групп ...',
                        triggerAction: 'all',
                        emptyText: 'Выберите группу...',
                        selectOnFocus: true,
                        editable: false
                    }), this.valueTypeComboBox = new Ext.form.ComboBox({
                        fieldLabel: 'Тип значения',
                        name: 'valuetypename',
                        anchor: '100%',
                        store: this.valueTypeStoreJS,
                        displayField: 'name',
                        valueField: 'id',
                        hiddenName: 'valuetypeid',
                        typeAhead: true,
                        loadingText: 'Загрузка типов...',
                        triggerAction: 'all',
                        emptyText: 'Выберите тип...',
                        selectOnFocus: true,
                        editable: false
                    }), this.createDateField = new Ext.form.DateField({
                        fieldLabel: 'Дата создания',
                        name: 'placdate',
                        anchor: '100%',
                        //altFormats : 'd.m.y|d.m.Y',
                        format: 'Y-m-d H:i:s',
                        allowBlank: true,
                        readOnly: true
                    }), {
                        xtype: 'hidden',
                        name: 'id',
                        hidden: true,
                        labelSeparator: ''
                    }]
                })]
            },
            this.valuePropListPanel]
        }]
    });
    this.saveButton = this.GeneralParam.addButton({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.GeneralParam.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(PropEditFormPanel, Ext.form.WgsFormPanel, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;

        this.valueTypeComboBox.on('select', function(combo, record, index) {
            self.checkValueType(record.data.id);
        });

        //Отмена
        this.on('cancel', function() {
            if (this.prop_Id) {
                Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showProp[this.prop_Id], true);
                delete Objectadmin.showProp[this.group_Id];
            } else {
                for (var i = 0; i < Objectadmin.showProp.length; i++) {
                    if (Objectadmin.showProp[i]) {
                        if (Objectadmin.showProp[i].title == 'Создание характеристики') {
                            Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showProp[i], true);
                            delete Objectadmin.showProp[i];
                        }
                    }
                }
            }
        }, this);
        //Сохранить
        this.on('save', function() {
            this.submit({
                success: function(form, action) {
                    if (self.owner.propListGrid) {
                        self.owner.propListGrid.storeJS.reload();
                    }
                    Objectadmin.objectTreePanel.refreshNode();
                    self.setTitle(self.getForm().getValues().name);
                    self.saveButton.setText('Применить');
                },
                failure: function(form, action) {
                    if (self.prop_Id == null) {
                        if (action.response && action.response.responseText) {
                            if (action.response.responseText.length <= 10) {
                                self.prop_Id = action.response.responseText;
                                self.getForm().setValues({
                                    id: self.prop_Id
                                });

                                self.valuePropListPanel.addButton.enable();
                                self.valuePropListPanel.editButton.enable();
                                self.valuePropListPanel.removeButton.enable();
                                self.valueTypeComboBox.disable();

                                self.valuePropListPanel.prop_Id = self.prop_Id;
                                self.valuePropListPanel.valuePropListGrid.prop_Id = self.prop_Id;
                                self.valuePropListPanel.valuePropListGrid.storeJS.baseParams.propId = self.prop_Id;
                                self.valuePropListPanel.valuePropListGrid.storeJS.reload();
                                self.getForm().url = '/objectadmin/EditProp';
                                if (self.owner.propListGrid) {
                                    self.owner.propListGrid.storeJS.reload();
                                }
                                self.setTitle(self.getForm().getValues(false).name);
                                Objectadmin.showProp[self.prop_Id] = Objectadmin.showProp[0];
                                delete Objectadmin.showProp[0];
                                Objectadmin.objectTreePanel.refreshNode();
                                self.saveButton.setText('Применить');
                            }
                        }
                    }
                }
            });
        }, this);
        this.on('destroy', function() {
            if (this.prop_Id == null) {
                delete Objectadmin.showProp[0];
            } else {
                delete Objectadmin.showProp[this.prop_Id];
            }
        }, this);
        this.on('show', function() {
            this.createDateField.disable();
            if (self.prop_Id == null) {
                self.valueTypeComboBox.enable();
            } else {
                self.valueTypeComboBox.disable();
            }
        }, this);
        PropEditFormPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-GRID СПИСКА ЗНАЧЕНИЙ ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ValuePropListGrid = function(owner, prop_Id) {
    this.owner = owner;
    this.prop_Id = prop_Id;
    //--------Массив со списком значений----
    var self = this;
    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name'],
        url: '/objectadmin/GetValueDomainList',
        baseParams: {
            propId: self.prop_Id
        }
    });
    //-------Конструктор грида-----------
    ValuePropListGrid.superclass.constructor.call(this, {
        viewConfig: {
            forceFit: true
        },
        frame: true,
        store: this.storeJS,
        autoScroll: true,
        columns: [{
            id: 'valueId',
            dataIndex: 'id',
            hideable: false,
            hidden: true
        }, {
            id: 'valueName',
            dataIndex: 'name',
            header: "Наименование",
            sortable: true
        }]
    });
}
Ext.extend(ValuePropListGrid, Ext.grid.GridPanel, {
    initComponent: function() {
        var self = this;
        this.storeJS.on('beforeload', function() {
            self.storeJS.removeAll();
        }, this);

        ValuePropListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ПАНЕЛЬ СПИСКА ЗНАЧЕНИЙ ХАРАКТЕРИСТИК-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ValuePropListPanel = function(owner, prop_Id) {
    this.owner = owner;
    this.prop_Id = prop_Id;
    this.valuePropListGrid = new ValuePropListGrid(this, this.prop_Id);
    //--------Кнопки ToolBar'а----------
    this.addButton = new Ext.Button({
        text: 'Добавить',
        iconCls: 'icon-add',
        handler: function() {
            this.fireEvent('addValue')
        },
        scope: this
    });
    this.editButton = new Ext.Button({
        text: '  Свойства',
        iconCls: 'icon-properties',
        handler: function() {
            this.fireEvent('editValue')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: '  Удалить',
        iconCls: 'icon-delete',
        handler: function() {
            this.fireEvent('removeValue')
        },
        scope: this
    });
    //--------Панель----------
    ValuePropListPanel.superclass.constructor.call(this, {
        title: 'Список значений',
        region: 'center',
        layout: 'fit',
        tbar: [this.addButton, ' ', '-', ' ', this.editButton, ' ', '-', ' ', this.removeButton],
        items: [this.valuePropListGrid]
    });
}
Ext.extend(ValuePropListPanel, Ext.form.FieldSet, {
    initComponent: function() {
        this.addEvents({
            addValue: true,
            editValue: true,
            removeValue: true
        });
        var self = this;
        this.on('addValue', function() {
            self.valuePropEditWindow = new ValuePropEditWindow(self, self.prop_Id, null);

            self.valuePropEditWindow.setTitle('Новое значение');
            self.valuePropEditWindow.formValue.getForm().url = '/objectadmin/AddValueDomain';
            self.valuePropEditWindow.show();
            self.valuePropEditWindow.formValue.getForm().setValues({
                propId: self.prop_Id
            });

        });
        this.on('editValue', function() {
            var listSelectedValues = self.valuePropListGrid.getSelectionModel().getSelections();
            if (listSelectedValues.length == 1) {
                self.valuePropEditWindow = new ValuePropEditWindow(self, self.prop_Id, listSelectedValues[0].data.id);
                self.valuePropEditWindow.setTitle('Значение: "' + self.valuePropListGrid.getSelectionModel().getSelected().data.name + '"');
                self.valuePropEditWindow.formValue.getForm().url = '/objectadmin/SetValueDomain';
                self.valuePropEditWindow.show();
                self.valuePropEditWindow.formValue.getForm().setValues({
                    valueDomain: listSelectedValues[0].data.name,
                    valueDomainId: listSelectedValues[0].data.id,
                    propId: self.prop_Id
                });
            } else {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо выбрать одно значение.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
        this.on('removeValue', function() {
            var listSelectedValues = self.valuePropListGrid.getSelectionModel().getSelections();
            if (listSelectedValues.length == 1) {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'Некоторые объекты могут иметь данную характеристику с удаляемым значением.\n' + 'Это может привети к потере данных!\n' + 'Вы уверены, что хотите продолжить удаление?',
                    fn: function(btn, text) {
                        if (btn == 'yes') {
                            var msgWait = Ext.Msg.wait('Удаление значения...', 'Подождите', {
                                interval: 50
                            });
                            Objectadmin.conn.request({
                                url: '/objectadmin/RemoveValueDomain',
                                method: 'POST',
                                params: {
                                    valueDomainId: listSelectedValues[0].data.id
                                },
                                success: function() {
                                    msgWait.hide();
                                    self.valuePropListGrid.storeJS.reload();
                                },
                                failure: function() {
                                    msgWait.hide();
                                    Ext.Msg.show({
                                        title: 'Ошибка',
                                        msg: 'Удалить значение не удалось.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }
                            });
                        }
                    },
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.MessageBox.QUESTION
                });
            } else {
                Ext.Msg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо выбрать одно значение.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
        ValuePropListPanel.superclass.initComponent.apply(this, arguments);
    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-ОКНО РЕДАКТИРОВАНИЯ ЗНАЧЕНИЯ ХАРАКТЕРИСТИКИ-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ValuePropEditWindow = function(owner, prop_Id, value_id) {
    this.owner = owner;
    this.prop_Id = prop_Id;
    this.value_id = value_id;

    ValuePropEditWindow.superclass.constructor.call(this, {
        title: '',
        layout: 'fit',
        resizable: false,
        height: 150,
        width: 400,
        modal: true,
        items: [this.formValue = new Ext.form.FormPanel({
            frame: true,
            closable: true,
            defaultType: 'textfield',
            items: [{
                name: 'propId',
                hidden: true,
                labelSeparator: ''
            }, {
                fieldLabel: 'Значение',
                name: 'valueDomain',
                anchor: '100%',
                allowBlank: true
            }, {
                name: 'valueDomainId',
                hidden: true,
                labelSeparator: ''
            }]
        })]
    });
    this.saveButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text: 'ОК',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(ValuePropEditWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });
        var self = this;
        this.on('show', function() {
            this.formValue.getForm().findField('valueDomain').focus(true);
        }, this);
        this.on('cancel', function() {
            self.close();
        });
        this.on('save', function() {
            var msgWait = Ext.Msg.wait('Сохранение изменений...', 'Подождите', {
                interval: 50
            });
            self.formValue.getForm().submit({
                success: function(form, action) {
                    if (self.owner.valuePropListGrid) {
                        self.owner.valuePropListGrid.storeJS.reload();
                    }
                    msgWait.hide();
                    self.close();
                },
                failure: function(form, action) {
                    if (action.response) {
                        if (action.response.responseText) {
                            if (action.response.responseText.length < 10) {
                                if (self.value_id == null) {
                                    self.value_id = action.response.responseText;
                                    self.formValue.getForm().url = '/objectadmin/SetValueDomain';
                                    if (self.owner.valuePropListGrid) {
                                        self.owner.valuePropListGrid.storeJS.reload();
                                    }
                                    self.setTitle('Значение: ' + self.formValue.getForm().getValues(false).valueDomain);
                                    msgWait.hide();
                                    self.close();
                                }
                            } else {
                                msgWait.hide();
                                Ext.Msg.show({
                                    title: 'Ошибка',
                                    msg: 'Внести изменения не удалось.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        } else {
                            msgWait.hide();
                            Ext.Msg.show({
                                title: 'Ошибка',
                                msg: 'Внести изменения не удалось.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    }
                }
            });
        });
        ValuePropEditWindow.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-ОКНО ПРИВЯЗКИ ХАРАКТЕРИСТИКИ К АТРИБУТИВНОМУ ТИПУ*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BindPropFormWindow = function(owner, type_Id) {
    this.owner = owner;
    this.type_Id = type_Id;

    var self = this;

    this.projectSelectionModel = new Ext.grid.CheckboxSelectionModel({
        singleSelect: false
    });
    this.storeJS = new Ext.data.GroupingStore({
        autoLoad: true,
        //fields: ['id', 'name','tagname','groupid','groupname','valuetypeid','valuetypename','placdate','acadview'],
        url: '/objectadmin/GetPropListNotByType',
        reader: new Ext.data.JsonReader({}, [{
            name: 'id'
        }, {
            name: 'name'
        }, {
            name: 'groupid'
        }, {
            name: 'groupname'
        }, {
            name: 'valuetypeid'
        }, {
            name: 'valuetypename'
        }, {
            name: 'placdate'
        }]),
        sortInfo: {
            field: 'groupname',
            direction: "ASC"
        },
        groupField: 'groupname',
        //remoteGroup: true,
        baseParams: {
            typeId: self.type_Id
        }
    });

    BindPropFormWindow.superclass.constructor.call(this, {
        layout: 'fit',
        resizable: false,
        height: 450,
        width: 600,
        modal: true,
        items: [new Ext.form.WgsFormPanel({
            frame: true,
            items: [self.PropFildSet = new Ext.form.FieldSet({
                title: 'Список характеристик',
                autoHeight: true,
                items: [
                self.PropGrid = new Ext.grid.GridPanel({
                    sm: this.projectSelectionModel,
                    stripeRows: true,
                    view: new Ext.grid.GroupingView({
                        groupTextTpl: '{text} ( Количество: {[values.rs.length]} )',
                        enableGroupingMenu: false,
                        forceFit: true
                    }),
                    store: self.storeJS,
                    frame: true,
                    height: 270,
                    autoScroll: true,
                    columns: [
                    this.projectSelectionModel, {
                        id: 'propId',
                        dataIndex: 'id',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'groupId',
                        dataIndex: 'groupid',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'valueTypeId',
                        dataIndex: 'valuetypeid',
                        hideable: false,
                        hidden: true
                    }, {
                        id: 'propName',
                        dataIndex: 'name',
                        header: "Наименование",
                        sortable: true
                    }, {
                        id: 'groupName',
                        dataIndex: 'groupname',
                        header: "Наименование группы",
                        sortable: true
                    }, {
                        id: 'valueTypeName',
                        dataIndex: 'valuetypename',
                        header: "Тип значения",
                        sortable: true
                    }, {
                        id: 'placDate',
                        dataIndex: 'placdate',
                        header: "Дата создания",
                        sortable: true
                    }]
                })]
            }),
            new Ext.form.FieldSet({
                title: 'Настройки привязки',
                autoHeight: true,
                items: [
                this.isCurrentCheck = new Ext.form.Checkbox({
                    boxLabel: 'Добавить выбранные характеристики к объектным данным',
                    hideLabel: true,
                    labelSeparator: ''
                })]
            })]
        })]
    });

    this.okButton = this.addButton({
        text: 'Привязать',
        handler: function() {
            this.fireEvent('ok')
        },
        scope: this
    });
    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text: 'Закрыть',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
}
Ext.extend(BindPropFormWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            ok: true,
            cancel: true
        });
        var self = this;
        this.on('cancel', function() {
            self.close();
        }, this);
        this.on('ok', function() {
            var selectedProperies = self.PropGrid.getSelectionModel().getSelections();
            if (selectedProperies.length != 0) {
                var propertiesForBind = [];
                var isError = false;
                for (var i = 0; i < selectedProperies.length; i++) {
                    propertiesForBind[propertiesForBind.length] = selectedProperies[i].data.id;
                    if ((
                        (selectedProperies[i].data.valuetypeid == 6) || 
                        (selectedProperies[i].data.valuetypeid == 7) || 
                        (selectedProperies[i].data.valuetypeid == 8)) && 
                        (self.isCurrentCheck.getValue() == true)) {
                        isError = true;
                    }
                }
                if (isError == false) {
                    var msgWait = Ext.Msg.wait('Привязка характеристик...', 'Подождите', {
                        interval: 50
                    });
                    Objectadmin.conn.request({
                        url: '/objectadmin/AddPropertyByObjectType',
                        method: 'POST',
                        params: {
                            typeid: self.type_Id,
                            'propertiesId[]': propertiesForBind,
                            aCadView: self.isCurrentCheck.getValue()
                        },
                        success: function() {
                            if (self.owner.propListGrid) {
                                self.owner.propListGrid.storeJS.reload();
                                self.storeJS.reload();
                            }
                            Objectadmin.objectTreePanel.refreshNode();
                            msgWait.hide();
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Ошибка',
                        msg: 'Выбраны характеристики имеющие не подходящий тип значения \n ' + 'для добавления их к объектным данным! \n' + 'Необходим тип значения: строка, число целое, число вещественное, дата-время',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
            }
        }, this);
        BindPropFormWindow.superclass.initComponent.apply(this, arguments);
    }
});



//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-ОСНОВНАЯ ТАБ ПАНЕЛЬ-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ObjectAdminWorkspaceTabPanel = function() {
    ObjectAdminWorkspaceTabPanel.superclass.constructor.call(this, {
        region: 'center',
        deferredRender: false,
        autoScrol: true,
        enableTabScroll: true,
        activeTab: 0,
        items: [{
            title: 'Начало работы',
            autoScroll: true

        }]
    });
}
Ext.extend(ObjectAdminWorkspaceTabPanel, Ext.WgsServiceTabPanel, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-ПАНЕЛЬ ДЕРЕВА-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
ObjectTreePanel = function() {
    this.showLayerGroupListPanel = false;
    this.showPropGroupListPanel = false;
    this.showPropListPanel = false;
    this.showAttrTypeEditFormPanel = false;
    //this.Objectadmin.showAttrType = new Array();
    //this.Objectadmin.showLayerGroup = new Array();

    //---------Кнопка "Показать карточки выбранных атрибутивных описаний"--------------------
    this.attrObjectButton = new Ext.Button({
        text: 'Карточки',
        iconCls: 'icon-objects',
        disabled: true,
        handler: function() {
            this.fireEvent('attrObject', this.getSelectionModel().getSelectedNodes())
        },
        scope: this,
        tooltip: 'Список карточек'
    });
    //---------Кнопка "Выгрузить атрибутивное описание"--------------------
    this.attrSaveButton = new Ext.Button({
        text: 'Выгрузить описание',
        disabled: true,
        handler: function() {
            this.fireEvent('attrSave', this.getSelectionModel().getSelectedNodes())
        },
        scope: this
    });
    //---------Кнопка "Обновить дерево"--------------------
    this.refreshButton = new Ext.Button({
        text: 'Обновить',
        iconCls: 'icon-refresh',
        handler: function() {
            this.fireEvent('refresh')
        },
        scope: this,
        tooltip: 'Обновить дерево'
    });
    //---------Кнопка "Добавить элемент в под коталог"--------------------
    this.ObjectType = new Object;
    this.ObjectType.attributes = new Object;
    this.addButton = new Ext.Toolbar.SplitButton({
        text: 'Добавить',
        handler: function() {
            this.fireEvent('add', this.getSelectionModel().getSelectedNodes()[0])
        },
        tooltip: 'Добавить',
        iconCls: 'icon-add',
        scope: this,
        menu: {
            items: [{
                text: 'Атрибутивное описание',
                iconCls: 'icon-atttype',
                handler: function() {
                    this.ObjectType.attributes.id = 'AttrType-root';
                    this.fireEvent('add', this.ObjectType);
                    this.ObjectType.attributes.id = '';
                },
                scope: this
            }, {
                text: 'Группу слоев',
                iconCls: 'icon-layer-group',
                handler: function() {
                    this.ObjectType.attributes.id = 'LayerGroup-root'
                    this.fireEvent('add', this.ObjectType);
                    this.ObjectType.attributes.id = '';
                },
                scope: this
            }, {
                text: 'Слой',
                iconCls: 'icon-layer',
                handler: function() {
                    this.ObjectType.attributes.Type = 'LayerGroup'
                    this.fireEvent('add', this.ObjectType);
                    this.ObjectType.attributes.Type = '';
                },
                scope: this
            }, {
                text: 'Группу характеристик',
                iconCls: 'icon-property-group',
                handler: function() {
                    this.ObjectType.attributes.id = 'PropGroup-root';
                    this.fireEvent('add', this.ObjectType);
                    this.ObjectType.attributes.id = '';
                },
                scope: this
            }, {
                text: 'Характеристику',
                iconCls: 'icon-property',
                handler: function() {
                    this.ObjectType.attributes.Type = 'PropGroup';
                    this.fireEvent('add', this.ObjectType);
                    this.ObjectType.attributes.Type = '';
                },
                scope: this
            }]
        }
    });
    //---------Кнопка "Свойства элемента"--------------------
    this.editButton = new Ext.Button({
        text: 'Свойства',
        iconCls: 'icon-properties',
        disabled: true,
        handler: function() {
            this.fireEvent('edit', this.getSelectionModel().getSelectedNodes()[0])
        },
        scope: this,
        tooltip: 'Свойства'
    });

    //---------Кнопка "Удалить элемент"--------------------
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        iconCls: 'icon-delete',
        disabled: true,
        handler: function() {
            this.fireEvent('removeObj', this.getSelectionModel().getSelectedNodes()[0])
        },
        scope: this,
        tooltip: 'Удалить'
    });

    //---------Кнопка "Отвязанные карточки"--------------------
    this.unbindedObjButton = new Ext.Button({
        text: 'Отвязанные карточки',
        iconCls: 'icon-unbindedobj',
        handler: function() {
            this.fireEvent('unbindedObj')
        },
        scope: this,
        tooltip: 'Отвязанные карточки'
    });

    //---------Конструктор дерева--------------
    ObjectTreePanel.superclass.constructor.call(this, {
        selModel: new Ext.tree.MultiSelectionModel(),
        region: 'west',
        //split:true,
        width: 280,
        minSize: 175,
        maxSize: 400,
        layoutConfig: {
            animate: true
        },
        layout: 'fit',
        //tbar: [
        //this.addButton,
        //  ],
        /*bbar: [
			'-',this.attrSaveButton,'-'
			  ],*/
        id: 'TreeObjects',
        autoScroll: true,
        root: new Ext.tree.AsyncTreeNode({
            text: 'Объекты',
            loader: this.myTreeLoader = new Ext.tree.WgsTreeLoader({
                dataUrl: '/objectadmin/getobjecttree'
            }),
            id: 'root',
            children: [{
                text: 'Атрибутивные описания',
                expanded: false,
                id: 'AttrType-root',
                Type: 'root',
                OwnerId: 'null',
                OwnerType: 'null',
                MyId: '1'
            }, {
                text: 'Группы слоев',
                expanded: false,
                id: 'LayerGroup-root',
                Type: 'root',
                OwnerId: 'null',
                OwnerType: 'null',
                MyId: '2'
            }, {
                text: 'Группы характеристик',
                expanded: false,
                id: 'PropGroup-root',
                Type: 'root',
                OwnerId: 'null',
                OwnerType: 'null',
                MyId: '3'
            }]
        }),
        rootVisible: false
    });

}
Ext.extend(ObjectTreePanel, Ext.tree.TreePanel, {
    initComponent: function() {
        this.addEvents({
            refresh: true,
            add: true,
            edit: true,
            removeObj: true,
            attrSave: true
        });

        var self = this;

        //----------Передача параметров в Loader ------------------
        this.myTreeLoader.on("beforeload", function(treeLoader, node) {
            this.myTreeLoader.baseParams.MyId = node.attributes.MyId;
            this.myTreeLoader.baseParams.Type = node.attributes.Type;
            this.myTreeLoader.baseParams.OwnerId = node.attributes.OwnerId;
            this.myTreeLoader.baseParams.OwnerType = node.attributes.OwnerType;
        }, this);

        this.on('removeObj', function(node) {
            if (node.attributes.Type == 'AttrType') {
                if (node.attributes.OwnerType == 'root') {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'При удалении атрибутивного описания вы можете потерять данные.\n' + 'Вы уверены что хотите удалить выбранное атрибутивное описание?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                if (node.attributes.MyId in Objectadmin.showAttrType) {
                                    Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showAttrType[node.attributes.MyId], true);
                                    delete Objectadmin.showAttrType[node.attributes.MyId];
                                }
                                var msgWait = Ext.Msg.wait('Удаление атрибутивного описания...', 'Подождите', {
                                    interval: 50
                                });
                                Objectadmin.conn.request({
                                    url: '/objectadmin/RemoveAttrType',
                                    method: 'POST',
                                    params: {
                                        id: node.attributes.MyId
                                    },
                                    success: function() {
                                        msgWait.hide();
                                        Objectadmin.objectTreePanel.refreshNode();
                                    },
                                    failure: function() {
                                        msgWait.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка',
                                            msg: 'Удалить атрибутивное описание не удалось.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }

            if (node.attributes.Type == 'LayerGroup') {
                if (node.attributes.OwnerType == 'root') {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Вы уверены что хотите удалить выбранную группу слоев?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                if (node.attributes.MyId in Objectadmin.showLayerGroup) {
                                    Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayerGroup[node.attributes.MyId], true);
                                    delete Objectadmin.showLayerGroup[node.attributes.MyId];
                                }
                                var msgWait = Ext.Msg.wait('Удаление группы слоев...', 'Подождите', {
                                    interval: 50
                                });
                                Objectadmin.conn.request({
                                    url: '/objectadmin/RemoveLayerGroup',
                                    method: 'POST',
                                    params: {
                                        objectid: node.attributes.MyId
                                    },
                                    success: function() {
                                        msgWait.hide();
                                        Objectadmin.objectTreePanel.refreshNode();
                                    },
                                    failure: function() {
                                        msgWait.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка',
                                            msg: 'Удалить группу слоев не удалось.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }

            if (node.attributes.Type == 'PropGroup') {
                if (node.attributes.OwnerType == 'root') {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Вы уверены что хотите удалить выбранную группу характеристик?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                if (node.attributes.MyId in Objectadmin.showPropGroup) {
                                    Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showPropGroup[node.attributes.MyId], true);
                                    delete Objectadmin.showPropGroup[node.attributes.MyId];
                                }
                                var msgWait = Ext.Msg.wait('Удаление группы характеристик...', 'Подождите', {
                                    interval: 50
                                });
                                Objectadmin.conn.request({
                                    url: '/objectadmin/RemovePropGroup',
                                    method: 'POST',
                                    params: {
                                        id: node.attributes.MyId
                                    },
                                    success: function() {
                                        msgWait.hide();
                                        Objectadmin.objectTreePanel.refreshNode();
                                    },
                                    failure: function() {
                                        msgWait.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка',
                                            msg: 'Удалить группу характеристик не удалось.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }
            if (node.attributes.Type == 'Layer') {
                if (node.attributes.OwnerType == 'LayerGroup') {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'При удалении слоя вы потеряете все геометрические объекты.\n' + 'Вы уверены что хотите удалить выбранный слой?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                if (node.attributes.MyId in Objectadmin.showLayer) {
                                    Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showLayer[node.attributes.MyId], true);
                                    delete Objectadmin.showLayer[node.attributes.MyId];
                                }
                                var msgWait = Ext.Msg.wait('Удаление слоя...', 'Подождите', {
                                    interval: 250
                                });
                                Objectadmin.conn.request({
                                    url: '/objectadmin/RemoveFeatureLayer',
                                    method: 'POST',
                                    params: {
                                        id: node.attributes.MyId
                                    },
                                    success: function() {
                                        msgWait.hide();
                                        Objectadmin.objectTreePanel.refreshNode();
                                    },
                                    failure: function() {
                                        msgWait.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка',
                                            msg: 'Удалить слой не удалось.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }
            if (node.attributes.Type == 'Prop') {
                if (node.attributes.OwnerType == 'PropGroup') {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'При удалении характеристики вы можете потерятьданные,\n' + 'если она привязана к одному из атрибутивных описаний!' + 'Вы уверены что хотите удалить характеристику?',
                        fn: function(btn, text) {
                            if (btn == 'yes') {
                                if (node.attributes.MyId in Objectadmin.showProp) {
                                    Objectadmin.objectAdminWorkspaceTabPanel.remove(Objectadmin.showProp[node.attributes.MyId], true);
                                    delete Objectadmin.showProp[node.attributes.MyId];
                                }
                                var msgWait = Ext.Msg.wait('Удаление характеристики...', 'Подождите', {
                                    interval: 50
                                });
                                Objectadmin.conn.request({
                                    url: '/objectadmin/RemoveProp',
                                    method: 'POST',
                                    params: {
                                        id: node.attributes.MyId
                                    },
                                    success: function() {
                                        msgWait.hide();
                                        Objectadmin.objectTreePanel.refreshNode();
                                    },
                                    failure: function() {
                                        msgWait.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка',
                                            msg: 'Удалить характеристику не удалось.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            }
                        },
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING
                    });

                }
            }
        });

        this.on('add', function(node) {
            if (node) if (node.attributes.Type == 'LayerGroup') {
                if (0 in Objectadmin.showLayer) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо закончить создание одного слоя, прежде чем приступать к новому!',
                        fn: function() {
                            Objectadmin.objectAdminWorkspaceTabPanel.items.each(function(panel) {
                                if (panel.title == 'Создание слоя') Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(panel);
                            });
                        },
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    Objectadmin.showLayer[0] = new FeatureLayerEditFormPanel(this, null);
                    Objectadmin.showLayer[0].setTitle('Создание слоя');
                    Objectadmin.showLayer[0].getForm().url = '/objectadmin/AddFeatureLayer';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayer[0]));
                    Objectadmin.showLayer[0].doLayout();
                    Objectadmin.showLayer[0].layerName = '';
                    Objectadmin.showLayer[0].layerGroup = node.attributes.MyId;
                    Objectadmin.showLayer[0].getForm().setValues({
                        groupid: node.attributes.MyId
                    });
                    Objectadmin.showLayer[0].storeJS.load({
                        callback: function(records) {
                            if (Objectadmin.showLayer[0]) {
                                if (node.attributes.MyId) {
                                    Objectadmin.showLayer[0].groupComboBox.setValue(node.attributes.MyId);
                                } else {
                                    if (records) {
                                        if (records.length > 0) {
                                            Objectadmin.showLayer[0].groupComboBox.setValue(records[0].data.id);
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            } else if (node.attributes.id == 'LayerGroup-root') {
                if (0 in Objectadmin.showLayerGroup) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо закончить создание одной группы слоев, прежде чем приступать к новой!',
                        fn: function() {
                            Objectadmin.objectAdminWorkspaceTabPanel.items.each(function(panel) {
                                if (panel.title == 'Создание группы слоев') Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(panel);
                            });
                        },
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    Objectadmin.showLayerGroup[0] = new LayerGroupEditFormPanel(this, null);
                    Objectadmin.showLayerGroup[0].setTitle('Создание группы слоев');
                    Objectadmin.showLayerGroup[0].getForm().url = '/objectadmin/AddLayerGroup';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayerGroup[0]));
                    Objectadmin.showLayerGroup[0].doLayout();
                    Objectadmin.showLayerGroup[0].group_Name = '';
                }
            } else if (node.attributes.Type == 'PropGroup') {
                if (0 in Objectadmin.showProp) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо закончить создание одной характеристики, прежде чем приступать к новой!',
                        fn: function() {
                            Objectadmin.objectAdminWorkspaceTabPanel.items.each(function(panel) {
                                if (panel.title == 'Создание характеристики') Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(panel);
                            });
                        },
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {

                    Objectadmin.showProp[0] = new PropEditFormPanel(this, null);
                    Objectadmin.showProp[0].setTitle('Создание характеристики');
                    Objectadmin.showProp[0].getForm().url = '/objectadmin/AddProp';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showProp[0]));
                    //var loadMask = new Ext.LoadMask(Objectadmin.showProp[0].getEl(),{msg: 'Загрузка...'});
                    Objectadmin.showProp[0].doLayout();
                    Objectadmin.showProp[0].propName = '';
                    Objectadmin.showProp[0].propGroup = node.attributes.MyId;
                    Objectadmin.showProp[0].propValueType = 1;
                    Objectadmin.showProp[0].getForm().setValues({
                        groupid: node.attributes.MyId,
                        placdate: new Date()
                    });
                    Objectadmin.showProp[0].groupStoreJS.load({
                        callback: function(records) {
                            if (Objectadmin.showProp[0]) {
                                if (node.attributes.MyId) {
                                    Objectadmin.showProp[0].groupComboBox.setValue(node.attributes.MyId);
                                } else {
                                    if (records) {
                                        if (records.length > 0) {
                                            Objectadmin.showProp[0].groupComboBox.setValue(records[0].data.id);
                                        }
                                    }
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[0].valueTypeStoreJS.load({
                        callback: function(records) {
                            if (records) {
                                if (records.length > 0) {
                                    Objectadmin.showProp[0].valueTypeComboBox.setValue(records[0].data.id);
                                }
                            }
                        }
                    });
                    //alert(1);

                }
            } else if (node.attributes.id == 'PropGroup-root') {
                if (0 in Objectadmin.showPropGroup) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо закончить создание одной группы характеристик, прежде чем приступать к новой!',
                        fn: function() {
                            Objectadmin.objectAdminWorkspaceTabPanel.items.each(function(panel) {
                                if (panel.title == 'Создание группы характеристик') Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(panel);
                            });
                        },
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    Objectadmin.showPropGroup[0] = new PropGroupEditFormPanel(this, null);
                    Objectadmin.showPropGroup[0].setTitle('Создание группы характеристик');
                    Objectadmin.showPropGroup[0].getForm().url = '/objectadmin/AddPropGroup';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showPropGroup[0]));
                    Objectadmin.showPropGroup[0].doLayout();
                    Objectadmin.showPropGroup[0].group_Name = '';
                }
            } else if (node.attributes.id == 'AttrType-root') {
                if (0 in Objectadmin.showAttrType) {
                    Ext.Msg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо закончить создание одного атрибутивного описания, прежде чем приступать к новому!',
                        fn: function() {
                            Objectadmin.objectAdminWorkspaceTabPanel.items.each(function(panel) {
                                if (panel.title == 'Создание атрибутивного описания') Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(panel);
                            });
                        },
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    Objectadmin.showAttrType[0] = new AttrTypeEditFormPanel(this, null);;
                    Objectadmin.showAttrType[0].setTitle('Создание атрибутивного описания');
                    Objectadmin.showAttrType[0].getForm().url = '/objectadmin/AddAttrType';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showAttrType[0]));
                    Objectadmin.showAttrType[0].doLayout();
                    Objectadmin.showAttrType[0].getForm().setValues({
                        adddate: new Date()
                    });
                }
            }
        });
        this.on('edit', function(node) {
            if (node.attributes.Type == 'AttrType') {
                if (node.attributes.MyId in Objectadmin.showAttrType) {
                    this.attrTypeStoreJS.reload();
                    var self = this;
                    this.attrTypeStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showAttrType[node.attributes.MyId]) {
                                Objectadmin.showAttrType[node.attributes.MyId].getForm().loadRecord(self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })));
                            }
                        }
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().reset();
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showAttrType[node.attributes.MyId]);
                    Objectadmin.showAttrType[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().url = '/objectadmin/EditAttrType';
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].doLayout();
                } else {
                    this.attrTypeStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'nameTag', 'shortname', 'adddate', 'note', 'label'],
                        url: '/objectadmin/GetAttrTypeList'
                    });
                    this.attrTypeStoreJS.reload();
                    Objectadmin.showAttrType[node.attributes.MyId] = new AttrTypeEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.showAttrType[node.attributes.MyId].saveButton.setText('Применить');
                    var self = this;
                    this.attrTypeStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showAttrType[node.attributes.MyId]) {
                                Objectadmin.showAttrType[node.attributes.MyId].getForm().loadRecord(self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })));
                                //Objectadmin.showAttrType[node.attributes.MyId].getForm().loadRecord(self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.find('id',node.attributes.MyId)));
                                var objectLabel = self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })).data.label;
                                //alert(objectLabel);
                                if ((objectLabel == -1) || (objectLabel == -2) || (objectLabel == -3)) {
                                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                                        objectLabel: objectLabel
                                    });
                                } else if (objectLabel > 0) {
                                    /*Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({objectLabel: -4, propnamelabel:objectLabel});
    									Objectadmin.showAttrType[node.attributes.MyId].PropComboBox.enable()*/
                                    Objectadmin.showAttrType[node.attributes.MyId].labelProp = objectLabel;
                                }
                            }
                        }
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().url = '/objectadmin/EditAttrType';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showAttrType[node.attributes.MyId]));
                    Objectadmin.showAttrType[node.attributes.MyId].doLayout();
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                }
            }
            if (node.attributes.Type == 'LayerGroup') {
                if (node.attributes.MyId in Objectadmin.showLayerGroup) {
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showLayerGroup[node.attributes.MyId]);
                    Objectadmin.showLayerGroup[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayerGroup[node.attributes.MyId].getForm().setValues({
                        name: node.attributes.text
                    });
                    Objectadmin.showLayerGroup[node.attributes.MyId].doLayout();
                    Objectadmin.showLayerGroup[node.attributes.MyId].featureLayerListPanel.featureLayerListGrid.storeJS.baseParams.groupId = node.attributes.MyId;
                    Objectadmin.showLayerGroup[node.attributes.MyId].featureLayerListPanel.featureLayerListGrid.storeJS.baseParams.typeId = null;
                    Objectadmin.showLayerGroup[node.attributes.MyId].featureLayerListPanel.featureLayerListGrid.storeJS.reload();
                    Objectadmin.showLayerGroup[node.attributes.MyId].group_Name = node.attributes.text;
                } else {
                    Objectadmin.showLayerGroup[node.attributes.MyId] = new LayerGroupEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.showLayerGroup[node.attributes.MyId].saveButton.setText('Применить');
                    Objectadmin.showLayerGroup[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayerGroup[node.attributes.MyId].getForm().url = '/objectadmin/EditLayerGroup';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayerGroup[node.attributes.MyId]));
                    Objectadmin.showLayerGroup[node.attributes.MyId].doLayout();
                    Objectadmin.showLayerGroup[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.showLayerGroup[node.attributes.MyId].group_Name = node.attributes.text;
                }
            }
            if (node.attributes.Type == 'PropGroup') {
                if (node.attributes.MyId in Objectadmin.showPropGroup) {
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showPropGroup[node.attributes.MyId]);
                    Objectadmin.showPropGroup[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showPropGroup[node.attributes.MyId].getForm().setValues({
                        name: node.attributes.text
                    });
                    Objectadmin.showPropGroup[node.attributes.MyId].doLayout();
                    Objectadmin.showPropGroup[node.attributes.MyId].propListPanel.propListGrid.storeJS.baseParams.groupId = node.attributes.MyId;
                    Objectadmin.showPropGroup[node.attributes.MyId].propListPanel.propListGrid.storeJS.baseParams.typeId = null;
                    Objectadmin.showPropGroup[node.attributes.MyId].propListPanel.propListGrid.storeJS.reload();
                    Objectadmin.showPropGroup[node.attributes.MyId].group_Name = node.attributes.text;
                } else {
                    Objectadmin.showPropGroup[node.attributes.MyId] = new PropGroupEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.showPropGroup[node.attributes.MyId].saveButton.setText('Применить');
                    Objectadmin.showPropGroup[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showPropGroup[node.attributes.MyId].getForm().url = '/objectadmin/EditPropGroup';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showPropGroup[node.attributes.MyId]));
                    Objectadmin.showPropGroup[node.attributes.MyId].doLayout();
                    Objectadmin.showPropGroup[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.showPropGroup[node.attributes.MyId].group_Name = node.attributes.text;
                }
            }
            if (node.attributes.Type == 'Layer') {
                if (node.attributes.MyId in Objectadmin.showLayer) {
                    Objectadmin.loadLayerStoreJS = false;
                    Objectadmin.loadGroupLayerStoreJS = false;
                    this.layerStoreJS.reload();
                    var self = this;
                    this.layerStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showLayer[node.attributes.MyId]) {
                                this.myRecord = self.layerStoreJS.getAt(self.layerStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showLayer[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showLayer[node.attributes.MyId].layerGroup = this.myRecord.data.groupid;
                                Objectadmin.loadLayerStoreJS = true;
                                if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].storeJS.load({
                        callback: function() {
                            Objectadmin.loadGroupLayerStoreJS = true;
                            if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                if (Objectadmin.showLayer[node.attributes.MyId]) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showLayer[node.attributes.MyId].layerGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].getForm().reset();
                    Objectadmin.showLayer[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showLayer[node.attributes.MyId]);
                    Objectadmin.showLayer[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayer[node.attributes.MyId].layerName = node.attributes.text;

                } else {
                    this.layerStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'groupid', 'groupname'],
                        url: '/objectadmin/GetFeatureLayerList'
                    });
                    this.layerStoreJS.reload();
                    Objectadmin.showLayer[node.attributes.MyId] = new FeatureLayerEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.showLayer[node.attributes.MyId].saveButton.setText('Применить');
                    Objectadmin.loadLayerStoreJS = false;
                    Objectadmin.loadGroupLayerStoreJS = false;
                    var self = this;
                    this.layerStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showLayer[node.attributes.MyId]) {
                                this.myRecord = self.layerStoreJS.getAt(self.layerStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showLayer[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showLayer[node.attributes.MyId].layerGroup = this.myRecord.data.groupid;
                                Objectadmin.loadLayerStoreJS = true;
                                if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                    if (Objectadmin.showLayer[node.attributes.MyId]) {
                                        Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                    }
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayer[node.attributes.MyId].getForm().url = '/objectadmin/EditFeatureLayer';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayer[node.attributes.MyId]));
                    Objectadmin.showLayer[node.attributes.MyId].doLayout();
                    Objectadmin.showLayer[node.attributes.MyId].layerName = node.attributes.text;
                    Objectadmin.showLayer[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });

                    Objectadmin.showLayer[node.attributes.MyId].storeJS.load({
                        callback: function() {
                            Objectadmin.loadGroupLayerStoreJS = true;
                            if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                if (Objectadmin.showLayer[node.attributes.MyId]) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showLayer[node.attributes.MyId].layerGroup);
                                }
                            }
                        }
                    });
                }
            }
            if (node.attributes.Type == 'Prop') {
                if (node.attributes.MyId in Objectadmin.showProp) {
                    Objectadmin.loadPropStoreJS = false;
                    Objectadmin.loadGroupPropStoreJS = false;
                    Objectadmin.loadValueTypePropStoreJS = false;
                    this.propStoreJS.reload();
                    var self = this;
                    this.propStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showProp[node.attributes.MyId]) {
                                this.myRecord = self.propStoreJS.getAt(self.propStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));

                                Objectadmin.showProp[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showProp[node.attributes.MyId].propGroup = this.myRecord.data.groupid;
                                Objectadmin.showProp[node.attributes.MyId].propValueType = this.myRecord.data.valuetypeid;
                                Objectadmin.loadPropStoreJS = true;
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                    if (Objectadmin.showProp[node.attributes.MyId]) {
                                        Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                    }
                                }
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                    if (Objectadmin.showProp[node.attributes.MyId]) {
                                        Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(this.myRecord.data.valuetypeid);
                                    }
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].groupStoreJS.load({
                        callback: function() {
                            Objectadmin.loadGroupPropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].valueTypeStoreJS.load({
                        callback: function() {
                            Objectadmin.loadValueTypePropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propValueType);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].getForm().reset();
                    Objectadmin.showProp[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showProp[node.attributes.MyId]);
                    Objectadmin.showProp[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showProp[node.attributes.MyId].propName = node.attributes.text;

                } else {
                    this.propStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'autoCADname', 'groupid', 'groupname', 'valuetypeid', 'valuetypename', 'placdate'],
                        url: '/objectadmin/GetPropList'
                    });
                    this.propStoreJS.reload();
                    Objectadmin.showProp[node.attributes.MyId] = new PropEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.showProp[node.attributes.MyId].saveButton.setText('Применить');
                    Objectadmin.loadPropStoreJS = false;
                    Objectadmin.loadGroupPropStoreJS = false;
                    Objectadmin.loadValueTypePropStoreJS = false;
                    findF = function(record, id) {
                        if (record.data.id == 1) {
                            return true;
                        }
                    }
                    var self = this;
                    this.propStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showProp[node.attributes.MyId]) {
                                //this.myRecord = self.propStoreJS.getAt(self.propStoreJS.find('id',node.attributes.MyId));
                                this.myRecord = self.propStoreJS.getAt(self.propStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                //alert(1);
                                //alert(this.myRecord.data.id);
                                Objectadmin.showProp[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showProp[node.attributes.MyId].propGroup = this.myRecord.data.groupid;
                                Objectadmin.showProp[node.attributes.MyId].propValueType = this.myRecord.data.valuetypeid;
                                if (this.myRecord.data.valuetypeid == 5) {
                                    Objectadmin.showProp[node.attributes.MyId].valuePropListPanel.show();
                                }
                                Objectadmin.loadPropStoreJS = true;
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                }
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(this.myRecord.data.valuetypeid);
                                }
                            }
                            //valueTypeComboBox
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showProp[node.attributes.MyId].getForm().url = '/objectadmin/EditProp';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showProp[node.attributes.MyId]));
                    Objectadmin.showProp[node.attributes.MyId].doLayout();
                    Objectadmin.showProp[node.attributes.MyId].propName = node.attributes.text;
                    Objectadmin.showProp[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });

                    Objectadmin.showProp[node.attributes.MyId].groupStoreJS.load({
                        callback: function() {
                            Objectadmin.loadGroupPropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].valueTypeStoreJS.load({
                        callback: function() {
                            Objectadmin.loadValueTypePropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propValueType);
                                }
                            }
                        }
                    });
                }
            }
        }, this);

        this.on('click', function(node) {
            Objectadmin.objectTreePanel.attrObjectButton.disable();
            switch (node.attributes.Type) {
                case 'LayerGroup':
                case 'PropGroup':
                    Objectadmin.objectTreePanel.addButton.enable();
                    Objectadmin.objectTreePanel.editButton.enable();
                    Objectadmin.objectTreePanel.removeButton.enable();
                    break;
                case 'root':
                    Objectadmin.objectTreePanel.addButton.enable();
                    Objectadmin.objectTreePanel.editButton.disable();
                    Objectadmin.objectTreePanel.removeButton.disable();
                    break;
                case 'AttrType':
                    if (node.attributes.leaf == undefined) {
                        Objectadmin.objectTreePanel.addButton.disable();
                        Objectadmin.objectTreePanel.editButton.enable();
                        Objectadmin.objectTreePanel.removeButton.enable();
                    } else {
                        Objectadmin.objectTreePanel.editButton.enable();
                    }
                    Objectadmin.objectTreePanel.attrObjectButton.enable();
                    break;
                case 'Layer':
                    if (node.attributes.leaf == undefined) {
                        Objectadmin.objectTreePanel.addButton.disable();
                        Objectadmin.objectTreePanel.editButton.enable();
                        Objectadmin.objectTreePanel.removeButton.enable();
                    } else {
                        Objectadmin.objectTreePanel.editButton.enable();
                        Objectadmin.objectTreePanel.removeButton.disable();
                    }
                    Objectadmin.objectTreePanel.attrObjectButton.enable();
                    break;
                case 'Prop':
                    if (node.attributes.leaf == undefined) {
                        Objectadmin.objectTreePanel.addButton.disable();
                        Objectadmin.objectTreePanel.editButton.enable();
                        Objectadmin.objectTreePanel.removeButton.enable();
                    } else {
                        Objectadmin.objectTreePanel.editButton.enable();
                        if (node.attributes.OwnerType == 'AttrType') Objectadmin.objectTreePanel.removeButton.disable();
                        else Objectadmin.objectTreePanel.removeButton.enable();
                    }
                    break;
                default:
                    Objectadmin.objectTreePanel.addButton.disable();
                    Objectadmin.objectTreePanel.editButton.disable();
                    Objectadmin.objectTreePanel.removeButton.disable();
                    break;
            }
        });

        this.on('dblclick', function(node) {
            if (node.attributes.Type == 'AttrType' && node.attributes.leaf) //----ИЗМЕНЕНА И ДОБАВЛЕНА В КНОПКУ EDIT
            {
                if (node.attributes.MyId in Objectadmin.showAttrType) {
                    this.attrTypeStoreJS.reload();
                    var self = this;
                    this.attrTypeStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showAttrType[node.attributes.MyId]) {
                                Objectadmin.showAttrType[node.attributes.MyId].getForm().loadRecord(self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })));
                            }
                        }
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().reset();
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showAttrType[node.attributes.MyId]);
                    Objectadmin.showAttrType[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().url = '/objectadmin/EditAttrType';
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].doLayout();
                } else {
                    this.attrTypeStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'nameTag', 'shortname', 'adddate', 'note', 'label'],
                        url: '/objectadmin/GetAttrTypeList'
                    });
                    this.attrTypeStoreJS.reload();
                    Objectadmin.showAttrType[node.attributes.MyId] = new AttrTypeEditFormPanel(this, node.attributes.MyId);
                    var self = this;
                    this.attrTypeStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showAttrType[node.attributes.MyId]) {
                                var objectLabel = self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })).data.label;
                                if ((objectLabel == -1) && (objectLabel == -2) && (objectLabel == -3)) {
                                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                                        objectLabel: objectLabel
                                    });
                                } else if (objectLabel > 1) {
                                    /*Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({objectLabel: -4, propnamelabel:objectLabel});
    									Objectadmin.showAttrType[node.attributes.MyId].PropComboBox.enable();*/
                                    Objectadmin.showAttrType[node.attributes.MyId].labelProp = objectLabel;
                                }
                                Objectadmin.showAttrType[node.attributes.MyId].getForm().loadRecord(self.attrTypeStoreJS.getAt(self.attrTypeStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                })));
                            }
                        }
                    });
                    Objectadmin.showAttrType[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().url = '/objectadmin/EditAttrType';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showAttrType[node.attributes.MyId]));
                    Objectadmin.showAttrType[node.attributes.MyId].doLayout();
                    Objectadmin.showAttrType[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                }
            }
            if (node.attributes.Type == 'Layer' && node.attributes.leaf) //----ИЗМЕНЕНА И ДОБАВЛЕНА В КНОПКУ EDIT
            {
                if (node.attributes.MyId in Objectadmin.showLayer) {
                    Objectadmin.loadLayerStoreJS = false;
                    Objectadmin.loadGroupLayerStoreJS = false;
                    this.layerStoreJS.reload();
                    var self = this;
                    this.layerStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showLayer[node.attributes.MyId]) {
                                this.myRecord = self.layerStoreJS.getAt(self.layerStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showLayer[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showLayer[node.attributes.MyId].layerGroup = this.myRecord.data.groupid;
                                Objectadmin.loadLayerStoreJS = true;
                                if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                    if (Objectadmin.showLayer[node.attributes.MyId]) {
                                        Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                    }
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].storeJS.load({
                        callback: function() {
                            Objectadmin.loadGroupLayerStoreJS = true;
                            if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                if (Objectadmin.showLayer[node.attributes.MyId]) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showLayer[node.attributes.MyId].layerGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].getForm().reset();
                    Objectadmin.showLayer[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showLayer[node.attributes.MyId]);
                    Objectadmin.showLayer[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayer[node.attributes.MyId].layerName = node.attributes.text;

                } else {
                    this.layerStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'groupid', 'groupname'],
                        url: '/objectadmin/GetFeatureLayerList'
                    });
                    this.layerStoreJS.reload();
                    Objectadmin.showLayer[node.attributes.MyId] = new FeatureLayerEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.loadLayerStoreJS = false;
                    Objectadmin.loadGroupLayerStoreJS = false;
                    var self = this;
                    this.layerStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showLayer[node.attributes.MyId]) {
                                this.myRecord = self.layerStoreJS.getAt(self.layerStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showLayer[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showLayer[node.attributes.MyId].layerGroup = this.myRecord.data.groupid;
                                Objectadmin.loadLayerStoreJS = true;
                                if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                }
                            }
                        }
                    });
                    Objectadmin.showLayer[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showLayer[node.attributes.MyId].getForm().url = '/objectadmin/EditFeatureLayer';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showLayer[node.attributes.MyId]));
                    Objectadmin.showLayer[node.attributes.MyId].doLayout();
                    Objectadmin.showLayer[node.attributes.MyId].layerName = node.attributes.text;
                    Objectadmin.showLayer[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });

                    Objectadmin.showLayer[node.attributes.MyId].storeJS.load({
                        callback: function() {
                            Objectadmin.loadGroupLayerStoreJS = true;
                            if ((Objectadmin.loadLayerStoreJS == true) && (Objectadmin.loadGroupLayerStoreJS == true)) {
                                if (Objectadmin.showLayer[node.attributes.MyId]) {
                                    Objectadmin.showLayer[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showLayer[node.attributes.MyId].layerGroup);
                                }
                            }
                        }
                    });
                }
            }
            if (node.attributes.Type == 'Prop') //----ИЗМЕНЕНА И ДОБАВЛЕНА В КНОПКУ EDIT
            {
                if (node.attributes.MyId in Objectadmin.showProp) {
                    Objectadmin.loadPropStoreJS = false;
                    Objectadmin.loadGroupPropStoreJS = false;
                    Objectadmin.loadValueTypePropStoreJS = false;
                    this.propStoreJS.reload();
                    var self = this;
                    this.propStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showProp[node.attributes.MyId]) {
                                this.myRecord = self.propStoreJS.getAt(self.propStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showProp[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showProp[node.attributes.MyId].propGroup = this.myRecord.data.groupid;
                                Objectadmin.showProp[node.attributes.MyId].propValueType = this.myRecord.data.valuetypeid;
                                Objectadmin.loadPropStoreJS = true;
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                }
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(this.myRecord.data.valuetypeid);
                                }
                            }
                            //valueTypeComboBox
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].groupStoreJS.load({
                        callback: function() {
                            Objectadmin.loadGroupPropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].valueTypeStoreJS.load({
                        callback: function() {
                            Objectadmin.loadValueTypePropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propValueType);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].getForm().reset();
                    Objectadmin.showProp[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.showProp[node.attributes.MyId]);
                    Objectadmin.showProp[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showProp[node.attributes.MyId].propName = node.attributes.text;

                } else {
                    this.propStoreJS = new Ext.data.WgsJsonStore({
                        fields: ['id', 'name', 'autoCADname', 'groupid', 'groupname', 'valuetypeid', 'valuetypename', 'placdate'],
                        url: '/objectadmin/GetPropList'
                    });
                    this.propStoreJS.reload();
                    Objectadmin.showProp[node.attributes.MyId] = new PropEditFormPanel(this, node.attributes.MyId);
                    Objectadmin.loadPropStoreJS = false;
                    Objectadmin.loadGroupPropStoreJS = false;
                    Objectadmin.loadValueTypePropStoreJS = false;
                    var self = this;
                    this.propStoreJS.load({
                        callback: function() {
                            if (Objectadmin.showProp[node.attributes.MyId]) {
                                this.myRecord = self.propStoreJS.getAt(self.propStoreJS.findBy(function(record, id) {
                                    if (record.data.id == node.attributes.MyId) {
                                        return true;
                                    }
                                }));
                                Objectadmin.showProp[node.attributes.MyId].getForm().loadRecord(this.myRecord);
                                Objectadmin.showProp[node.attributes.MyId].propGroup = this.myRecord.data.groupid;
                                Objectadmin.showProp[node.attributes.MyId].propValueType = this.myRecord.data.valuetypeid;
                                if (this.myRecord.data.valuetypeid == 5) {
                                    Objectadmin.showProp[node.attributes.MyId].valuePropListPanel.show();
                                }
                                Objectadmin.loadPropStoreJS = true;
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(this.myRecord.data.groupid);
                                }
                                if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(this.myRecord.data.valuetypeid);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].setTitle(node.attributes.text);
                    Objectadmin.showProp[node.attributes.MyId].getForm().url = '/objectadmin/EditProp';
                    Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.showProp[node.attributes.MyId]));
                    Objectadmin.showProp[node.attributes.MyId].doLayout();
                    Objectadmin.showProp[node.attributes.MyId].propName = node.attributes.text;
                    Objectadmin.showProp[node.attributes.MyId].getForm().setValues({
                        id: node.attributes.MyId,
                        name: node.attributes.text
                    });

                    Objectadmin.showProp[node.attributes.MyId].groupStoreJS.load({
                        callback: function() {
                            Objectadmin.loadGroupPropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadGroupPropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].groupComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propGroup);
                                }
                            }
                        }
                    });
                    Objectadmin.showProp[node.attributes.MyId].valueTypeStoreJS.load({
                        callback: function() {
                            Objectadmin.loadValueTypePropStoreJS = true;
                            if ((Objectadmin.loadPropStoreJS == true) && (Objectadmin.loadValueTypePropStoreJS == true)) {
                                if (Objectadmin.showProp[node.attributes.MyId]) {
                                    Objectadmin.showProp[node.attributes.MyId].valueTypeComboBox.setValue(Objectadmin.showProp[node.attributes.MyId].propValueType);
                                }
                            }
                        }
                    });
                }
            }
        }, this);

        this.on('unbindedObj', function(node) {
            if (!Objectadmin.unbindObjPanel) {
                Objectadmin.unbindObjPanel = new UnbindObjPanel();
                Objectadmin.unbindObjPanel.on('destroy', function() {
                    Objectadmin.unbindObjPanel = null;
                });
                Objectadmin.unbindObjPanel.setTitle('Отвязанные карточки');
                Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.objectAdminWorkspaceTabPanel.add(Objectadmin.unbindObjPanel));
            } else {
                Objectadmin.objectAdminWorkspaceTabPanel.setActiveTab(Objectadmin.unbindObjPanel);
            }
        });

        this.refreshNode = function() {
            var rootNode = {
                childNodes: [],
                typeNode: 'root',
                idNode: 'root'
            };

            treeNodeExpandedSave = function(parentTreeNode, parentObjNode) {
                if (!parentTreeNode.leaf) {
                    if (parentTreeNode.isLoaded()) {
                        var nodeObj = {
                            childNodes: [],
                            typeNode: parentTreeNode.attributes.Type,
                            idNode: parentTreeNode.attributes.MyId,
                            isExpand: parentTreeNode.isExpanded(),
                            isSelect: parentTreeNode.isSelected()
                        };
                        parentObjNode.childNodes.push(nodeObj);
                        parentTreeNode.childNodes.each(function(nodeTree) {
                            treeNodeExpandedSave(nodeTree, nodeObj)
                        });
                    }
                }
            }
            var self = this;
            this.collapseF = function(node) {
                node.collapse();
                node.un('expand', self.collapseF);
            }

            treeNodeExpandedLoad = function(parentTreeNode, parentObjNode) {
                parentObjNode.childNodes.each(function(nodeObj) {
                    if ((nodeObj.typeNode == parentTreeNode.attributes.Type) && (nodeObj.idNode == parentTreeNode.attributes.MyId)) {
                        parentTreeNode.reload();
                        parentTreeNode.on('load', function(node) {
                            if (parentTreeNode.childNodes.size() > 0) {
                                parentTreeNode.childNodes.each(function(nodeTree) {
                                    treeNodeExpandedLoad(nodeTree, nodeObj);
                                });
                            }
                        });
                        if (!nodeObj.isExpand) {
                            parentTreeNode.on('expand', self.collapseF);
                        }
                    }
                });
            }

            this.getRootNode().childNodes.each(function(nodeTree) {
                treeNodeExpandedSave(nodeTree, rootNode);
            });

            this.getRootNode().reload();

            this.getRootNode().childNodes.each(function(nodeTree) {
                treeNodeExpandedLoad(nodeTree, rootNode);
            });

        };

        this.on('refresh', function() {
            this.refreshNode();
        }, this);

        this.on('attrObject', function(nodes) {
            if (this.getSelectionModel().getSelectedNodes()[0]) {
                var node = this.getSelectionModel().getSelectedNodes()[0];
                var panel = Map.Search.Create(Objectadmin.objectAdminWorkspaceTabPanel);
                panel.open();
                if (node.attributes.Type == 'Layer') panel.searchCardsByLayer(node.attributes.MyId)
                else panel.searchCardsByType(node.attributes.MyId)
            }
        }, this);

        this.on('attrSave', function(nodes) {
            var typesId = [];
            var self = this;
            nodes.each(function(node) {
                if (node.attributes.Type == 'AttrType') {
                    typesId[typesId.length] = node.attributes.MyId;
                }
            });
            if (typesId.length > 0) {
                Objectadmin.conn.request({
                    url: '/objectadmin/SaveAttrTypeToXML',
                    method: 'POST',
                    params: {
                        'typesId[]': typesId
                    },
                    success: function(response) {
                        var FileName = response.responseText;
                        Ext.WgsMsg.show({
                            title: 'Сообщение',
                            msg: 'Атрибутивное описание выгружено, перейдите по ссылке для того чтобы сохранить его в файл' +
                            // '<A href="/favoriteview_cache/'+FileName+'">Сохранить</A>',
                            '<A href="/doccache/Привет.txt">Сохранить</A>',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                });
            }
        });
        ObjectTreePanel.superclass.initComponent.apply(this, arguments);
    }
});

Ext.namespace('Objectadmin');

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-ПРОЕКТ-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
Objectadmin = function() {
    var conn = new Ext.data.WgsConnection();
    var objectTreePanel = new ObjectTreePanel();
    var objectAdminWorkspaceTabPanel = new ObjectAdminWorkspaceTabPanel();
    var setReadonly = function(f, bReadOnly) {
        if (typeof f._readOnly == 'undefined') {
            if (f.initEvents) f.initEvents = f.initEvents.createSequence(self.initEventsSequence)
        }
        f._readOnly = bReadOnly;
        f.getEl().dom.readOnly = bReadOnly;
        if (f instanceof Ext.form.TriggerField) {
            if (bReadOnly) {
                f.trigger.un('click', f.onTriggerClick, f);
                if (f.keyNav) f.keyNav.disable();
            } else {
                f.trigger.on('click', f.onTriggerClick, f, {
                    preventDefault: true
                });
                if (f.keyNav) f.keyNav.enable();
            }
        }
    };

    return {
        init: function() {
            new Ext.Viewport({
                layout: 'fit',
                items: new Ext.Panel({
                    layout: 'border',
                    deferredRender: false,
                    activeTab: 0,
                    style: 'padding-top: 48px',
                    items: [
                    objectAdminWorkspaceTabPanel,
                    objectTreePanel],
                    tbar: [
                    objectTreePanel.addButton,
                    objectTreePanel.editButton,
                    objectTreePanel.removeButton,
                    //'->',
                    '-',
                    objectTreePanel.refreshButton,
                    objectTreePanel.attrObjectButton,
                        '-',
                    objectTreePanel.unbindedObjButton]
                })
            }).doLayout();
            Ext.QuickTips.init();
        },
        conn: conn,
        objectTreePanel: objectTreePanel,
        objectAdminWorkspaceTabPanel: objectAdminWorkspaceTabPanel,
        setReadonly: setReadonly
    }
}();

Objectadmin.showAttrType = new Array();
Objectadmin.showLayer = new Array();
Objectadmin.showLayerGroup = new Array();
Objectadmin.showPropGroup = new Array();
Objectadmin.showProp = new Array();

Ext.onReady(Objectadmin.init, Objectadmin);