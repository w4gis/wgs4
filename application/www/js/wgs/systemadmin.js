﻿/* Application */

Systemadmin = function() {
	var conn = new Ext.data.WgsConnection();

	var objectManagementPanel = new ObjectManagementPanel();
	var systemRoleManagementPanel = new SystemRoleManagementPanel();
	var systemUserManagementPanel = new SystemUserManagementPanel();
	
	var createModuleWindow = new CreateModuleWindow();
	var createPrivilegeWindow = new CreatePrivilegeWindow();
	var createProjectWindow = new CreateProjectWindow();

	var tabPanel = new Ext.TabPanel({
		deferredRender : false,
		activeTab : 0,
		style: 'padding-top: 49px',
		listeners:
        	{
        		tabchange: function(tabPanel, tab)
        		{
        			switch(tab.title)
					{
					case 'Роли':
					  Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
	                  break;    
					case 'Пользователи':
					  Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
					  break;
					default:
					  break;
					}
        		}
        	},
		items : [objectManagementPanel, systemRoleManagementPanel, systemUserManagementPanel]
	});
	
	return {
		createModuleWindow : createModuleWindow,
		createPrivilegeWindow : createPrivilegeWindow, 
		createProjectWindow : createProjectWindow,

		setPermissionChangeAction : function(value) {
			this.permissionChangeAction = value
		},
		
		init : function() {
			new Ext.Viewport({
				layout : 'fit',
				items : tabPanel
			}).doLayout();
		},
		conn : conn,
		objectManagementPanel : objectManagementPanel,
		systemRoleManagementPanel:  systemRoleManagementPanel,
		systemUserManagementPanel:  systemUserManagementPanel
	}
}();