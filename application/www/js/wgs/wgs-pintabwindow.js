Ext.ns('WGS.sys');

WGS.sys.PinTabWindow = Ext.extend(Ext.Window, {
	
	pin: function() {
		this.pp = this.getPinablePanel();
		
		this.pinPanel = this.createPinPanel({
			title: this.title,
			iconCls: this.iconCls,
			closable: true,
			pinablePanel: this.pp,
			pinToolTabPanel: this.pttp
		});
	
		this.pttp.add(this.pinPanel);
		this.pp.ptContainer = this.pinPanel;
		this.hide();
	
		var ai = this.pttp.items.length-1;
	
		this.pttp.setActiveTab(ai);
	
		this.pp.fireEvent('pinned');
	
		Ext.fly(this.pttp.getTabEl(ai)).on('dblclick', this.unpin, this);
	},
	
	unpin: function(){
		this.pp.fireEvent('unpinned');
		this.add(this.pp);
		this.pp.ptContainer = this;
		this.restore();
		this.show();
		this.pttp.remove(this.pinPanel);
	},
	
	getPinTool: function(pinToolTabPanel) {
		this.pttp = pinToolTabPanel;
		return {
			id: 'pin',
			handler: this.pin,
			scope: this
		};
	},

	constructor: function(config) {
		Ext.apply(this, config);

		if (this.usePinTool && this.pinToolTabPanel) {
			this.tools = this.tools || [];
			this.tools.unshift(this.getPinTool(this.pinToolTabPanel));
		}
		if (this.useHelpTool) {
			this.tools = this.tools || [];
			this.titleText = this.titleText ? this.titleText : this.title ? this.title :'Unknown title';
			/*
			this.tools.unshift(WGS.cmp.Help.Tool({
				code: this.titleText
			}));
			*/
		}

		WGS.sys.PinTabWindow.superclass.constructor.call(this);
	}
});