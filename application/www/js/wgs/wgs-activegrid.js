﻿Ext.ns('WGS.sys');

/**
 * Utility to work with events.
 *
 */

WGS.sys._createActiveGrid = function(Extendable){
	var IsTabPanel = Extendable == WGS.sys.ClosableTabPanel;

	var	ActiveGrid = Ext.extend(Extendable, {
		name: 'Active grid',
		version: '1.0.0',
		version: '1.0.0',
		revision: '$Revision:$',
		revdate: '$Date:$',
		module: 'Core',

		/**
		 *
		 */
		closeWindows: function()
		{
			if (this.rmAddWin && !this.rmAddWin.hidden) {
				this.rmAddWin.close();
			}
			if (this.rmEditWin && !this.rmEditWin.hidden) {
				this.rmEditWin.close();
			}
			if (this.rmViewWin && !this.rmViewWin.hidden) {
				this.rmViewWin.close();
			}
			if (this.rmMetaWin && !this.rmMetaWin.hidden) {
				this.rmMetaWin.close();
			}
		},

		/**
		 * @protected
		 * @param {Array} cols Array of columns
		 *
		 * Supported column types:
		 * * string
		 * * date
		 * * link
		 * * select
		 * * linkselect
		 * * iconselect
		 * * int
		 * * checkbox
		 */
		prepareColumns: function(cols)
		{
			var fields = this.table.getFields();
			for (var i = 0, field; field = fields[i]; i++) {
				var col = {
					name: field.name,
					header: field.title? field.title: field.name,
					hidden: field.visible == undefined? false: !field.visible,
					width: field.width,
					dataIndex: field.name,
					sortable: field.sortable,
					groupable: field.groupable,
					hideable: field.hideable,
					renderer: field.renderer,
					tooltip: field.tooltip,
					editor: field.editor,
					summaryType: field.summaryType,
					align: field.align
				};

				switch (field.type) {
					case 'date':
						col.xtype = 'datecolumn';
						col.format = field.format ? field.format : WGS.constants.Date.GridFormat;
						break;
					case 'password':
						col.renderer = function(){
							return '**********';
						};
						break;
					case 'link':
						col = new Ext.ux.LinkColumn({
							name: col.name,
							sortable: col.sortable,
				        	dataIndex: col.dataIndex,
				        	header: col.header
				        });
				        this.gridPlugins.push(col);
				        break;
					case 'linkselect':
						col = new Ext.ux.LinkColumn({
							name: col.name,
							sortable: col.sortable,
				        	dataIndex: col.dataIndex,
				        	header: col.header,
				        	renderer: function(value, metaData, record, rowIndex, colIndex, store, field) {
								if (field.store && field.store.records) {
									return '<a href="tf" onclick="return false;" class="linkcolumn-link lc-'+field.name+'">'+field.store.records[value]+'</a>';
								}
								return value;
							}.createDelegate(this, [field], true)
				        });
				        this.gridPlugins.push(col);
				        break;
					case 'select':
						if (!field.renderer) {
							col.renderer = function(value, metaData, record, rowIndex, colIndex, store, field) {
								if (field.store && field.store.records) {
									return field.store.records[value];
								}
								return value;
							}.createDelegate(this, [field], true);
						} else {
							col.renderer = field.renderer;
						}
						break;
					case 'checkbox':
						col = new Ext.ux.grid.CheckColumn({
							name: col.name,
							sortable: col.sortable,
				        	dataIndex: col.dataIndex,
				        	header: col.header,
				        	editable: false,
				        	hidden: col.hidden,
				        	width: col.width
				        });
				        this.gridPlugins.push(col);
						break;
					case 'iconselect':
						col = new Ext.ux.grid.CellActions({
							width: 26,
							fixed: true,
							dataIndex: col.dataIndex,
							align: "left",
							hidden: col.hidden,
							hideable: col.hideable,
							cellActions: [{
								iconIndex: col.dataIndex,
								qtipIndex: col.dataIndex
							}]
					    });
					    if (!this.cellActionsAdded) {
							this.gridPlugins.push(col);
							this.cellActionsAdded = true;
					    }
						break;
				}

				cols.push(col);
			}
		},

		/**
		 * Initialize grid actions
		 * @protected
		 */
		initActions: function()
		{
			Ext.applyIf(this.actions, {
				'add': new Ext.Action({
					text: 'Добавить',
					tooltip: 'Добавить',
					handler: function(){
						this.doAddRecord();
					},
					iconCls: 'icon-add',
					scope: this,
					hotKeys: [{
			    		key: 45,
			    		shift: false,
			    		fn: function(){
							this.doAddRecord();
						},
						scope: this
					},{
			    		key: 45,
			    		shift: true,
			    		fn: function(){
							this.doAddRecordByTemplate();
						},
						scope: this
					}]
				}),
				'edit': new Ext.Action({
					text: 'Редактировать',
					tooltip: 'Редактировать',
					handler: function(){
						this.doEditRecord();
					},
					iconCls: 'icon-edit',
					disabled: true,
					scope: this,
					hotKeys: [{
						ctrl: true,
			    		key: [10,13],
			    		fn: function(){
							this.doEditRecord();
						},
						scope: this
					}]
				}),
				'view': new Ext.Action({
					text: this.viewText,
					tooltip: this.viewText,
					handler: function(){
						this.doViewRecord();
					},
					iconCls: 'tf-view',
					disabled: true,
					scope: this,
					hotKeys: [{
			        	key: [10,13],
			        	ctrl: false,
			        	fn: function(){
							this.doViewRecord();
						},
			    		scope: this
					}]
				}),
				'remove': new Ext.Action({
					text: 'Удалить',
					tooltip: 'Удалить',
					handler: function(){
						this.doRemoveRecord();
					},
					iconCls: 'icon-delete',
					disabled: true,
					scope: this,
					hotKeys: [{
			    		key: 46,
			    		fn: function(){
							this.doRemoveRecord();
						},
						scope: this
					}]
				})/*,
				'export': new WGS.Exporter.Action({
					title: this.exportText,
					tooltip: this.exportText,
					component: function(){
						return this.grid;
					}.createDelegate(this),
					columns: this.cols,
					visibleColumns: true,
					text: this.exportText
				}),
				'help': new WGS.cmp.Help.Action({
					code: this.titleText,
					pinToolTabPanel: this
				})*/
			});
		},

		/**
		 * Create grid toolbar
		 * @protected
		 */
		createToolbar: function()
		{
			this.toolbar = [];
			for (var i=0, action; action=this.actionsConfig[i]; i++) {
				if (this.actions[action]) {
					this.toolbar.push(this.actions[action]);
				}
				if (action in {'-':true, '->':true, ' ':true}) {
					this.toolbar.push(action);
				}
			}
		},

		/**
		 * Initialize hot keys
		 * @protected
		 */
		initKeyMap: function()
		{
			this.keyMap = [];
			for (var i=0, action; action=this.actionsHotKeysConfig[i]; i++) {
				if (this.actions[action] && this.actionsConfig.indexOf(action) != -1) {
					var conf = this.actions[action].initialConfig;
					if (conf && conf.hotKeys) {
						this.keyMap = this.keyMap.concat(conf.hotKeys);
					}
				}
			}
		},

		/**
		 * Create grid components
		 * @protected
		 */
		createGrid: function()
		{
			// getting table store
			this.gridStore = this.table.getStore();

			// selection model

			this.csm = !this.sm ? new Ext.grid.RowSelectionModel({singleSelect: true}) : this.sm;

			// column model
			//if (!this.initialConfig.sm) {
			//	this.cols = [ this.csm ];
			//} else {
			this.cols = [ ];
			//}

			// plugins
			this.gridPlugins = this.gridPlugins||[];

			if (this.table.isObject) {
				this.objInfoColumn = new Ext.ux.grid.RowActions({
					widthSlope: 22,
					hideable: false,
					actions:[{
		                iconCls: 'tf-info',
		                style: 'margin-left: 3px;',
		                tooltip: this.showMetadataTip
		            },{
		                iconCls: 'tf-lock',
		                hideIndex: 'isunlocked',
		                style: 'cursor: default;',
		                tooltip: this.recordLockedTip
		            }]
				});

				this.objInfoColumn.on('action', function(grid, record, action) {
	                if (action == 'tf-info') {
	                	if (record) {
	                		this.doShowMetadata(record);
	                	}
	                }
	            }, this);

				this.cols.push(this.objInfoColumn);
				this.gridPlugins.push(this.objInfoColumn);
			}

			this.rowActions = this.rowActions||null;
			if (this.rowActions) {
				this.cols.push(this.rowActions);
				this.gridPlugins.push(this.rowActions);
			}

			this.cellActions = this.cellActions||null;
			if (this.cellActions) {
				this.cols.push(this.cellActions);
				this.gridPlugins.push(this.cellActions);
			}

			// preparing columns
			this.prepareColumns(this.cols);

			// prepare toolbar
			this.createToolbar();

			this.grid = new Ext.grid.EditorGridPanel({
				title: IsTabPanel? this.title: undefined,
				iconCls: IsTabPanel? this.iconCls: undefined,
				////
				stripeRows: this.stripeRows,
				columnLines: this.columnLines,
				enableDrag: this.enableDrag,
				enableDrop: this.enableDrop,
				enableDragDrop: this.enableDragDrop,
				enableHdMenu: this.enableHdMenu == undefined ? true : this.enableHdMenu,
				clicksToEdit: this.clicksToEdit,
				ddGroup: this.ddGroup,
				//viewConfig: this.viewConfig,
				////
				store: this.gridStore,
				columns: this.cols,
				sm: this.csm,
				view: new Ext.grid.GroupingView({
	            	getRowClass: this.getGridViewRowClass,
	            	forceFit:true
	        	}),
	        	plugins: this.gridPlugins,
		        border: false,
		        tbar: this.toolbar.length? this.toolbar: undefined,
				bbar: new Ext.PagingToolbar({
			        store: this.gridStore,
			        displayInfo: true,
			        pageSize: this.table.pageSize,
			        prependButtons: true
			    }),
			    stripeRows : true,
			    listeners: {
			    	//afterrender: this.onGridAfterRender,
			    	scope: this
			    }
			});

			this.grid.on('afterrender', function(){
				this.grid.loadMask = new Ext.LoadMask(this.grid.getEl());
				this.loadMask.el = this.grid.loadMask.el;
			}, this);

			this.loadMask = new Ext.LoadMask(this.grid.getEl() || this.getEl() || Ext.getBody(), {
				msg: this.waitMsg,
				store: this.gridStore
			});

			if (this.actionsConfig.indexOf('edit') != -1) {
				this.grid.on('rowdblclick', function(grid, rowIndex, e){
					this.actions.edit.execute();
				}, this);
			}

			this.csm.on('selectionchange', function(selModel){
				var selected = selModel.getSelections().length,
					disabled = (selected == 0) || (selected > 1);
				this.actions.edit.setDisabled(selected < 1);
				this.actions.view.setDisabled(disabled);
				this.actions.remove.setDisabled(selected < 1);
			}, this);


			this.gridStore.on('beforeload', function(store, options){
				if (options && !options.deferred) {
					this.table.prepareFields();
					this.table.onReady(function(){
						options.deferred = true;
						this.gridStore.load(options);
					}, this);
					return false;
				}
			}, this);

			if (this.storeAutoLoad) {
				this.gridStore.load({
					callback: function(){
						this.add(this.grid);
						if (IsTabPanel) {
							this.setActiveTab(0);
						}
						this.doLayout();
						this.fireEvent('firstloaded');
					},
					scope: this,
					deferred: true
				});
			} else {
				this.table.prepareFields();
				(function(){
					this.table.onReady(function(){
						this.add(this.grid);
						if (IsTabPanel) {
							this.setActiveTab(0);
						}
						this.doLayout();
						this.fireEvent('firstloaded');
					}, this);
				}).defer(100, this);
			}

			this.grid.on('close', function(){
				if (IsTabPanel) {
					this.remove(this.grid);
					for (var i=0, item; item=this.items.get(i); i++){
						item.fireEvent('close');
					}
					(function(){
						this.removeAll();
					}).defer(500, this);
				}
				this.fireEvent('close');
			}, this);
		},

		/**
		 * @private
		 */
		onGridAfterRender: function(grid)
		{
			//total width of the columns
			//grid.columnsTotalWidth = grid.getColumnModel().getTotalWidth();

			/*
			 * Reset width of the columns
			 */
			/*function resetColumnsWidth() {
				for(var i = 0, cc; cc = grid.colModel.config[i]; i++){
		        	for(var j = 0, dc; dc = grid.columnsWidth[j]; j++){
		            	if (cc.id && cc.width) {
							if (cc.id == dc.id) {
								grid.getColumnModel().setColumnWidth(j, dc.width);
							}
						}
		        	}
		        }
			}

			/*
			 * Set values for columns width
			 */
			function setColumnsWidth() {
				grid.columnsWidth = [];
				for(var i = 0, c; c = grid.colModel.config[i]; i++){
		            if (c.id && c.width) {
						grid.columnsWidth.push({
							id: c.id,
							width: c.width
						});
					}
		        }
			}

			setColumnsWidth();

			/*
			 * Resize event handler
			 */
			/*grid.on('resize', function(grid, adjWidth, adjHeight, rawWidth, rawHeight) {
				// if not autoWidth & new total width of the columns more than old value
				// than set forceFit to false and set first width for each column
				if (this.getView().forceFit) {
					if (this.getColumnModel().getTotalWidth() <= this.columnsTotalWidth) {
						this.getView().forceFit = false;
						resetColumnsWidth();
					}
				// else if grid width more than old value of total width set forceFit to false
				} else {
					var scrollOffset = this.getView().scrollOffset || 20;
					if (adjWidth-scrollOffset > this.columnsTotalWidth) {
						//this.getView().forceFit = true;
					}
				}
				this.getView().refresh();
			}, grid);

			/*
			 * Colunm resize handler
			 */
			/*grid.on('columnresize', function(columnIndex, newWidth) {
				// if not autoWidth set total width and remember values
				if (!this.getView().forceFit) {
					this.columnsTotalWidth = this.getColumnModel().getTotalWidth();
					setColumnsWidth();
				}
			}, grid);

			var scrollOffset = grid.getView().scrollOffset || 20;

			// set autoWidth if grid width less than total width
			if (grid.getWidth() - scrollOffset >= grid.columnsTotalWidth) {
				grid.getView().forceFit = true;
				grid.getView().refresh();
				grid.getView().forceFit = false;
			}


			this.fireEvent('gridafterrender', grid);*/
		},

		/**
		 * @private
		 */
		doShowMetadata: function(record)
		{
			if (record) {
				(new WGS.table.Object).onReady(function(table){
					var queryRecord = table.createRecord({'sid': record.get('sid')});

					Ext.MessageBox.wait(WGS.sys.ActiveTable.prototype.waitMsg, '');

					table.loadRecord(queryRecord, function(record){
						if (record) {
							this.rmMetaWin = new WGS.sys.RecordManagerWindow({
								title: this.showMetadataTitle,
								fields: table.getFields(),
								mode: 'view',
								autoClose: true,
								usePinTool: IsTabPanel,
								pinToolTabPanel: this
							});

							var rm = this.rmMetaWin.getRecordManager();

							this.rmMetaWin.on('show', function(win) {
								rm.importRecord(record);
								Ext.MessageBox.hide();
								this.rmMetaWin = win;
							}, this);

							this.rmMetaWin.show();
						}
					}, this);
				}, this);
			}
		},

		/**
		 * @private
		 */

		onAddSuccess: function(response, options, record, recordIndex) {
			if (response.record) {

				if (this.table.convertValues) {
					this.table.convertValues(response.record);
				}
				for (var j=0, f; f = record.fields.item(j); j++) {
					if (f.type.type == 'date' && response.record[f.name]) {
						record.set(f.name, Date.parseDate(response.record[f.name], f.dateFormat));
					}
					else {
						record.set(f.name, response.record[f.name]);
					}
				}
				record.commit();
				this.rmAddWin.loadMask.hide();
				this.rmAddWin.close();
				//WGS.util.Msg(this.infoTitle, this.createResultMsg);
			}

			this.gridStore.on('add', function(store, records, index){
				this.grid.getSelectionModel().selectRecords(records);
				this.grid.getView().focusRow(index);
			}, this, {single: true});

			this.gridStore.addSorted(record);
		},

		doAddRecord: function(record, flag, oldThemeId)
		{
			flag = flag || 0;
			oldThemeId = oldThemeId || 0;
			
			var newRecord = Ext.isObject(record)? record: this.table.createRecord();
			var grid = this.table;
			
			newRecord.data.POSITION = grid.tableStore.data.length;
			
			this.fireEvent('beforeaddrecord', newRecord);

			this.rmAddWin = new WGS.sys.RecordManagerWindow({
				iconCls: 'icon-add',
				fields: this.table.getFields(),
				mode: 'add',
				usePinTool: IsTabPanel,
				pinToolTabPanel: this
			});

			var rm = this.rmAddWin.getRecordManager();

			this.rmAddWin.on('show', function(win) {
				rm.importRecord(newRecord);
				if (Ext.isObject(record)) {
					rm.recordChanged = true;
				}
				this.rmAddWin = win;
			}, this);

			rm.on('cancelchanges', function() {
				rm.ptContainer.close();
			}, this);
			
						
			rm.on('changesapplied', function() {
				this.rmAddWin.loadMask.show();
				var t = this.table;
				this.table.save(newRecord, {
					responseSuccess: function(response, options, record, recordIndex){
						this.onAddSuccess(response, options, record, recordIndex);
						var dataToAdd = new Object();
						
						dataToAdd.object = "themes";
						dataToAdd.params = newRecord.data;
						dataToAdd.params.POSITION = grid.tableStore.data.length;
						
						Ext.Ajax.request({
								url        : 'map/themes/update',
								success    : function(response, options) {
												t.tableStore.reload();
											},
								failure    : function() {
													// Сообщение об ошибке
													Ext.MessageBox.alert('Не пришел ответ от сервера');
											},
								params    : { // параметры передаем
													data: Ext.util.JSON.encode(dataToAdd)
											},
								method	: 'POST'
							});
						/*После установки ID копируем слои тематики*/
						if (flag == 1){
						
							Ext.Ajax.request({
								url        : 'map/themes/themelayerlist',
								success    : function(response, options) {
													example = Ext.util.JSON.decode(response.responseText);												
													for(var i = 0; i< example.records.length; i++){
														example.records[i].themeId = record.data.THEME_ID;
													}
													
													requestParam = Ext.util.JSON.encode(example.records)
											/* запрос на добавление слоев*/
											
													Ext.Ajax.request({
														url : 'map/themes/addthemelayer',
														success : function(response, options){
																		t.tableStore.reload();
														},
														failure: function(){
																		Ext.MessageBox.alert('Не пришел ответ от сервера (запрос на добавление)');
														},
														params 		: {
																		themeId 	: record.data.THEME_ID,
																		records		: requestParam
														}
													});
											},
								failure    : function() {
													// Сообщение об ошибке
													Ext.MessageBox.alert('Не пришел ответ от сервера');
											},
								params    : { // параметры передаем
													themeId: oldThemeId
											},
								method	: 'POST'
							});
							
			////////////
							}
									
					},
					failure : function () {
						this.rmAddWin.loadMask.hide();
					},
					responseFailure: function(response, options, record, recordIndex){
						//////////////
						this.rmAddWin.loadMask.hide();
						rm.recordChanged = true;
						var msg = response.msg ? response.msg: this.failureMsgText;
						Ext.Msg.show({
							title : this.failureMsgTitle,
							msg: msg,
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
						//////////////
					},
					scope: this
				});
			}, this);

			
			
			//grid.getview().refresh();
			////console.log('refreshed')
			this.rmAddWin.show();
		},

		/**
		 * @private
		 */
		doAddRecordByTemplate: function()
		{
			////console.log('addRecordByTemplate')
			var sm = this.grid.getSelectionModel(),
				record = sm.getSelected();

			if (record) {
				var newRecord = this.table.copyRecord(record);
				this.doAddRecord(newRecord);
			}
		},

		onEditSuccess: function(response, options, record, recordIndex, grid) {
			if (response.record) {
				if (this.table.convertValues) {
					this.table.convertValues(response.record);
				}
				for (var j=0, f; f = record.fields.item(j); j++) {
					if (f.type.type == 'date' && response.record[f.name]) {
						record.set(f.name, Date.parseDate(response.record[f.name], f.dateFormat));
					}
					else {
						record.set(f.name, response.record[f.name]);
					}
				}
				record.commit();
				//WGS.util.Msg(this.infoTitle, this.updateResultMsg);
			}
		},

		/**
		 * @private
		 */
		doEditRecord: function(records, rm)
		{
			var sm = this.grid.getSelectionModel(),
				records = records||sm.getSelections(),
				MULTIEDITMODE = records.length != 1;

			if (!rm) {
				this.rmEditWin = new WGS.sys.RecordManagerWindow({
					iconCls: 'icon-edit',
					fields: this.table.getFields(),
					mode: records.length > 1? 'multiedit': 'edit',
					arrows: true,
					autoClose: false,
					usePinTool: IsTabPanel,
					pinToolTabPanel: this
				});

				this.rmEditWin.on('show', function(win) {
					this.rmEditWin = win;
				}, this);

				var rm = this.rmEditWin.getRecordManager();

				rm.on('close', function(){
					this.rmEditWin = null;
				}, this);

				var fCloseWin = false;

				rm.on('applychanges', function() {
					fCloseWin = true;
				});

				rm.on('cancelchanges', function(rec, recs) {
					rm.ptContainer.close();
				}, this);

				rm.on('changesapplied', function(rec, recs) {
					rm.ptContainer.loadMask.show();
					this.table.save(recs||rec, {
						responseSuccess: function(response, options, record, recordIndex, recordsTotal){
							if (recordsTotal-1 == recordIndex && rm.ptContainer) {
								rm.ptContainer.loadMask.hide();
								if (fCloseWin) {
									rm.ptContainer.close();
								}
							}
							this.onEditSuccess(response, options, record, recordIndex, this.grid);
						},
						responseFailure: function(response, options, record, recordIndex){
							if (rm.ptContainer) {
								rm.ptContainer.loadMask.hide();
							}
							var msg = response.msg ? response.msg: this.failureMsgText;
							Ext.Msg.show({
								title : this.failureMsgTitle,
								msg: msg,
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						failure: function () {
							if (rm.ptContainer) {
								rm.ptContainer.loadMask.hide();
							}
							this.table.getStore().rejectChanges();
						},

						scope: this
					});
				}, this);

				rm.on('tonextrecord', function() {
					rm.ptContainer.loadMask.show();
					fCloseWin = false;
					sm.selectNext();
					var nextRecord = sm.getSelected();
					this.doEditRecord([nextRecord], rm);
				}, this);

				rm.on('topreviousrecord', function() {
					rm.ptContainer.loadMask.show();
					fCloseWin = false;
					sm.selectPrevious();
					var previousRecord = sm.getSelected();
					this.doEditRecord([previousRecord], rm);
				}, this);

				rm.on('importrecord', function(pbtn, nbtn) {
					nbtn.setDisabled(!sm.hasNext());
					pbtn.setDisabled(!sm.hasPrevious());
				}, this);
			}

			if (!MULTIEDITMODE) {
				//simple edit mode
				var record = records[0];

				var queryData = {};
					queryData[this.table.getIndexField()] = record.get(this.table.getIndexField());
				var queryRecord = this.table.createRecord(queryData);

				this.fireEvent('beforequeryrecord', queryRecord, record);

				if (!rm.ptContainer.rendered) {
					this.loadMask.show();
				} else {
					rm.ptContainer.loadMask.show();
				}

				this.table.loadRecord(queryRecord, function(loadedRecord){
					if (this.rmEditWin) {
						if (rm.ptContainer) {
							if (!rm.ptContainer.rendered) {
								this.loadMask.hide();
							} else {
								rm.ptContainer.loadMask.hide();
							}
						}
						record.data = loadedRecord.data;
						record.commit();
						rm.panel.enable();
						rm.ptContainer.onReady(function() {
							rm.importRecord(record);
						}, this);
					}
				}, this);
			}
			else {
				//multi edit mode
				rm.ptContainer.onReady(function() {
					var newRecord = this.table.createRecord();
					rm.importRecord(newRecord, records);
				}, this);
			}
		},

		/**
		 * @private
		 */
		doViewRecord: function()
		{
			var sm = this.grid.getSelectionModel(),
				record = sm.getSelected();

			if (record) {
				this.rmViewWin = new WGS.sys.RecordManagerWindow({
					fields: this.table.getFields(),
					mode: 'view',
					arrows: true,
					usePinTool: IsTabPanel,
					pinToolTabPanel: this
				});

				var rm = this.rmViewWin.getRecordManager();

				rm.on('cancelchanges', function() {
					rm.ptContainer.close();
				}, this);

				this.rmViewWin.on('show', function(win) {
					rm.importRecord(record);
					this.rmViewWin = win;
				}, this);

				rm.on('tonextrecord', function() {
					sm.selectNext();
					var record = sm.getSelected();
					rm.importRecord(record);
				}, this);

				rm.on('topreviousrecord', function() {
					sm.selectPrevious();
					var record = sm.getSelected();
					rm.importRecord(record);
				}, this);

				rm.on('importrecord', function(pbtn, nbtn) {
					nbtn.setDisabled(!sm.hasNext());
					pbtn.setDisabled(!sm.hasPrevious());
				}, this);

				this.rmViewWin.show();
			}
		},

		/**
		 * @private
		 */
		doRemoveRecord: function()
		{
			var sm = this.grid.getSelectionModel(),
				records = sm.getSelections();
			if (records.length > 0) {
				//TODO: locks
				Ext.Msg.show({
					title: 'Удаление объектов',
					msg: 'Вы действительно хотите удалить выбранные объекты?',
					buttons: Ext.Msg.YESNO,
					fn: function(btn){
						if (btn == 'yes') {
							this.loadMask.show();
							this.table.remove(records, {
								responseSuccess: function(response, options, record, recordIndex){
									this.loadMask.hide();
									this.gridStore.remove(record);
									//WGS.util.Msg(this.infoTitle, this.removeResultMsg);
								},
								responseFailure: function(response, options, record, recordIndex){
									this.loadMask.hide();
									var msg = response.msg ? response.msg: this.failureMsgText;
									Ext.Msg.show({
										title : this.failureMsgTitle,
										msg: msg,
										buttons: Ext.Msg.OK,
										icon: Ext.MessageBox.ERROR
									});
								},
								scope: this
							});
						}
					},
					icon: Ext.MessageBox.QUESTION,
					scope: this
				});
			}
		},

		getColumn: function(column)
		{
			for (var i=0, c; c = this.cols[i]; i++) {
				if (c.name == column) {
					return c;
				}
			}
		},

		getStore: function()
		{
			return this.grid.getStore();
		},

		getView: function()
		{
			return this.grid.getView();
		},

		getColumnModel: function()
		{
			return this.grid.getColumnModel();
		},

		getSelectionModel: function()
		{
			return this.grid.getSelectionModel();
		},

		/**
		 * Initialization
		 *
		 * @return void
		 */
		initComponent: function()
		{
			Ext.apply(this, {
				anchor: '100% 100%'
			});

			if (IsTabPanel) {
				Ext.apply(this, {
					activeTab: 0,
					enableTabScroll: true
				});
			}
			else {
				Ext.apply(this, {
					layout: 'fit'
				});
			}

			this.addEvents('firstloaded');

			// init actions
			this.initActions();

			// init key map
			this.initKeyMap();

			this.table.onReady(this.createGrid, this);

			ActiveGrid.superclass.initComponent.call(this);
		},

		afterRender: function()
		{
			new Ext.KeyMap(this.getEl(), this.keyMap);

			ActiveGrid.superclass.afterRender.call(this);
		},

		constructor: function(config)
		{
			Ext.apply(this, config);
			Ext.applyIf(this, {
				actions: {},
				actionsConfig: [],
				actionsHotKeysConfig: [],
				border: false,
				//TODO: move to active table
				storeAutoLoad: true
			});

			ActiveGrid.superclass.constructor.call(this, config);
		}
	});

	return ActiveGrid;
};

WGS.sys.ActiveGrid = WGS.sys._createActiveGrid(WGS.sys.ClosablePanel);
WGS.sys.ActiveTabGrid = WGS.sys._createActiveGrid(WGS.sys.ClosableTabPanel);