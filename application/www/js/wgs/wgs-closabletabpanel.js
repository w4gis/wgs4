Ext.ns('WGS.sys');

WGS.sys.ClosableTabPanel = Ext.extend(Ext.TabPanel, {
	constructor: function(config)
	{
		config = config||{};
		var tools = config.tools||[];
		
		var plugins = [];
		
		if (config.closable==undefined?true:config.closable) {
			tools.push({
				id: 'close',
				toolscope: ['*'],
				handler: function(){
					this.hide();
					this.fireEvent('close');
				},
				scope: this
			});
			var stripTools = new Ext.ux.tabs.StripTools({
				striptools: tools
			});
			plugins.push(stripTools);
		}
		
		Ext.apply(this, {
			plugins: plugins
		});
		
		WGS.sys.ClosableTabPanel.superclass.constructor.call(this, config);
	}
});