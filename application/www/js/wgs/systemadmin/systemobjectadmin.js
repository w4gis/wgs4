﻿// PermissionsGrid - назначение прав на системные объекты
PermissionsGrid = function(data, roleId, roleName, msgWait, storeFields, gridColumns)
{
    // public
    var self = this;

    // privates
    this.roleId = roleId;
    this.changed = false;
    this.data = data;

    Systemadmin.setPermissionChangeAction(false);
    // methods
    this.getPrivileges = function(data, storeFields)
    {
        var privileges = [];
        data.each(function(record){
            $H(record).each(function(subrecord){
                if (subrecord.key == 'privileges')
                    subrecord.value.each(function(privilege)
                    {
                        storeFields[storeFields.length] = privilege.privilegesId;
                        privileges[privileges.length] = {name: privilege.privilegesName, id: privilege.privilegesId};
                    });
            });
            throw $break;
        });
        return privileges;
    }

    this.getGridData = function(data)
    {
        var storeData = [];
        data.each(function(record){
            var storeDataRow = [];
            $H(record).each(function(subrecord){
                if (subrecord.key == 'privileges')
                    subrecord.value.each(function(privilege)
                    {
                        storeDataRow[storeDataRow.length] = privilege.privilegesAllow;
                    });
                else
                    storeDataRow[storeDataRow.length] = subrecord.value;
            });
            storeData[storeData.length] = storeDataRow;
        });
        return storeData;
    }

    this.getFilledColumns = function(privileges, gridColumns)
    {
        var checkColumns = [];
        privileges.each(function(privilege){
            var checkColumn = new Ext.grid.CheckColumnThree({
                header: privilege.name,
                align: 'center',
                dataIndex: privilege.id
            });
            checkColumns[checkColumns.length] = checkColumn;
            gridColumns[gridColumns.length] = checkColumn;
        });
        return {
            grid: gridColumns,
            check: checkColumns
        }
    }

    this.initGrid = function()
    {
        var privileges = this.getPrivileges(this.data, storeFields);
        var columns = this.getFilledColumns(privileges, gridColumns);
        this.columns = columns.grid;
        this.plugins = columns.check;
        this.store = new Ext.data.SimpleStore({
            fields: storeFields,
            listeners: {
                update: function()
                {
                    self.saveButton.enable();
                    self.cancelButton.enable();
                    self.changed = true;
                    Systemadmin.setPermissionChangeAction(true);
                }
            }
        });

        this.store.loadData(this.getGridData(this.data));
    }

    /* buttons */
    this.saveButton = new Ext.Button({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        disabled: true,
        handler: function() {this.fireEvent('save')},
        scope: this
    });

    this.cancelButton = new Ext.Button({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        disabled: true,
        handler: function() {this.fireEvent('cancel')},
        scope: this
    });

    this.selectAllButton = new Ext.Button({
        text: 'Выделить все',
        handler: function() {this.fireEvent('setAll', 1, storeFields)},
        scope: this
    });

    this.inheritAllButton = new Ext.Button({
        text: 'Наследовать все',
        handler: function() {this.fireEvent('setAll', 2, storeFields)},
        scope: this
    });

    this.deselectAllButton = new Ext.Button({
        text: 'Снять все',
        handler: function() {this.fireEvent('setAll', 0, storeFields)},
        scope: this
    });

    /* constructor */
    PermissionsGrid.superclass.constructor.call(this, {
    	viewConfig: { forceFit: true },
        clicksToEdit: 1,
        stripeRows: true,
        collapsible: false,
        autoExpandColumn: 'name',
        anchor: '100% 100%',
        region: 'center',
        title: 'Назначение прав:  ' + roleName,
        listeners: {
            render: function()
            {
                msgWait.hide();
            }
        },
        tbar: [this.saveButton, this.cancelButton, '-', this.selectAllButton, this.inheritAllButton, this.deselectAllButton]
    });
}

Ext.extend(PermissionsGrid, Ext.grid.GridPanel, {
	initComponent : function() {

		var self = this;

	 	this.initGrid();

	 	this.addEvents({
			save : true,
			cancel : true
		});

		this.on('cancel', function(){
            this.store.rejectChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
            this.changed = false;
            Systemadmin.setPermissionChangeAction(false);
        });

		this.on('save', function(){
			var msgWait = Ext.Msg.wait('Сохранение параметров...', 'Подождите', {interval: 32});
            var modifiedRecords = this.store.getModifiedRecords();
            var params = [];
            Ext.each(modifiedRecords, function(record){
                params[params.length] = record.data;
            });
            Systemadmin.conn.request(
            {
                url: '/systemadmin/setsysobjectpermissions',
                method: 'POST',
                params: {privileges: Ext.util.JSON.encode(params), roleid: this.roleId},
                success: function(){
               	    msgWait.hide();
                }
            });
            this.store.commitChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
            this.changed = false;
            Systemadmin.setPermissionChangeAction(false);
        });

        this.on('setAll', function(state, storeFields)
    	{
	        this.store.each(function(record){
	            record.set(Object.keys(record.data)[storeFields.length-1], state);
	        });
    	});

		PermissionsGrid.superclass.initComponent.apply(this, arguments);
	}
});

// PermissionsPanel - назначение привилегий на объекты
PermissionsPanel = function() {

	// methods
	this.addPermissionsGrid = function(grid)
    {
        this.permissionsGrid = grid;
        this.add(this.permissionsGrid);
        this.doLayout();
    }

    this.clear = function()
    {
        if (this.permissionsGrid)
        {
            this.remove(this.permissionsGrid);
            this.permissionsGrid = null;
        }
    }

    this.generateGrid = function (id, text, url, storeFields, gridColumns) {
        var msgWait = Ext.Msg.wait('Генерация списка прав...', 'Подождите', {interval:32});
        Systemadmin.conn.request(
        {
            url: url,
            params: {id: id},
            success: function(response)
            {
                Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
                Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.addPermissionsGrid(
                    new PermissionsGrid(Ext.util.JSON.decode(response.responseText), id, text, msgWait, storeFields, gridColumns)//,
                );
            }
        });
    }

    this.overModulesButton = new Ext.Button({
        text: 'Модули',
        handler: function() {this.fireEvent('overModules', this.overModulesButton)},
        scope: this
    });

    this.overPrivilegesButton = new Ext.Button({
        text: 'Системные привилегии',
        handler: function() {this.fireEvent('overPrivileges', this.overPrivilegesButton)},
        scope: this
    });

	PermissionsPanel.superclass.constructor.call(this, {
		collapsible : false,
		margins : '0 0 0 0',
		layout : 'fit',
		title: 'Параметры доступа',
		tbar: [this.overModulesButton, '-', this.overPrivilegesButton]
	});
}
Ext.extend(PermissionsPanel, Ext.Panel, {
	initComponent : function() {

		var self = this;

	 	this.addEvents({
			//overProjects : true,
			overModules : true,
			overPrivileges : true
		});

			/*this.on('overProjects', function(){
			var roleId = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().id;
            var roleName = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().text;
            var url = '/systemadmin/getstoreprojectlist';
            var storeFields = ['id', 'name', 'domain', 'description', 'database', 'title', 'schemewdb', 'schemesdb', 'schemeadb', 'schemeuser', 'privileges'];

        	var gridColumns = [
            { id : 'id', header : "Идентификатор проекта", hidden : true, hideable : false },
            { header : "Проект", dataIndex : 'name' },
            { header : "Описание", dataIndex : 'description' }];

            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.generateGrid(roleId, roleName, url, storeFields, gridColumns);
        });*/

        this.on('overModules', function(){
        	if (Systemadmin.permissionChangeAction == false)
        	{
	            if (Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode() && Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().id != 'root')
	            {
	            	self.overPrivilegesButton.toggle(false);
	            	self.overModulesButton.toggle(true);
	            	var roleId = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().id;
		            var roleName = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().text;
		            var url = '/systemadmin/getmemberships/';
		            var storeFields = ['name', 'id', 'parentId', 'label', 'urlstring', 'description', 'priority', 'memberships', 'objectid'];

		        	var gridColumns = [
		            { id : 'id', header : "Идентификатор модуля", hidden : true, hideable : false },
		            { header : "Название модуля", dataIndex : 'name' },
		            { header : "Описание модуля", dataIndex : 'description' }];

		            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.generateGrid(roleId, roleName, url, storeFields, gridColumns);
	            }
	            else {
	            	Ext.MessageBox.show({
			         	title: 'Предупреждение',
				     	msg: 'Необходимо выбрать роль.',
				     	buttons: Ext.MessageBox.OK,
				     	icon: Ext.MessageBox.WARNING
			    	});
			    	Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
			    	self.overPrivilegesButton.toggle(false);
	            	self.overModulesButton.toggle(false);
	            }
        	}
        	else {
        		   Ext.Msg.show({
                   title:'Сохранить изменения?',
                   msg: 'Вы пытаетесь получить список прав, не сохранив изменения. Сохранить изменения?',
                   buttons: Ext.Msg.YESNOCANCEL,
                   fn:  function (btn, text) {
							if (btn == 'yes') {
								Systemadmin.setPermissionChangeAction(false);
								Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.permissionsGrid.fireEvent('save');
								self.fireEvent('overModules');
							}
							if (btn == 'no') {
								Systemadmin.setPermissionChangeAction(false);
								self.fireEvent('overModules');
							}
                   },
                   icon: Ext.MessageBox.QUESTION
           		});
        	}

        });

        this.on('overPrivileges', function(){
        	if (Systemadmin.permissionChangeAction == false)
        	{
	            if (Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode() && Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().id != 'root')
	            {
	            	self.overModulesButton.toggle(false);
	            	self.overPrivilegesButton.toggle(true);

	            	var roleId = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().id;
		            var roleName = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree.getSelectionModel().getSelectedNode().text;
		            var url = '/systemadmin/getstoreprivilegelist/';
		            var storeFields = ['id', 'name', 'nameHtml','description', 'objectid', 'locked'];

		        	var gridColumns = [
		            { id : 'id', header : "Идентификатор привилегии", hidden : true, hideable : false },
		            { header : "Привилегия", dataIndex : 'nameHtml' },
		            { header : "Описание привилегии", dataIndex : 'description' }];

		            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.generateGrid(roleId, roleName, url, storeFields, gridColumns);
	            }
	            else {
	            	Ext.MessageBox.show({
			         	title: 'Предупреждение',
				     	msg: 'Необходимо выбрать роль.',
				     	buttons: Ext.MessageBox.OK,
				     	icon: Ext.MessageBox.WARNING
			    	});
			    	Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
			    	self.overPrivilegesButton.toggle(false);
	            	self.overModulesButton.toggle(false);
	            }
        	}
        	else {
        		   Ext.Msg.show({
                   title:'Сохранить изменения?',
                   msg: 'Вы пытаетесь получить список прав, не сохранив изменения. Сохранить изменения?',
                   buttons: Ext.Msg.YESNOCANCEL,
                   fn:  function (btn, text) {
							if (btn == 'yes') {
								Systemadmin.setPermissionChangeAction(false);
								Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.permissionsGrid.fireEvent('save');
								self.fireEvent('overPrivileges');
							}
							if (btn == 'no') {
								Systemadmin.setPermissionChangeAction(false);
								Systemadmin.setPermissionChangeAction(false);
							}
                   },
                   icon: Ext.MessageBox.QUESTION
           		});
        	}
        });

		PermissionsPanel.superclass.initComponent.apply(this, arguments);
	}
});

ModuleListMemberShipsGrid = function(data, projectId, projectName, msgWait)
{
    /* public */
    var self = this;

    /* privates */
    this.projectId = projectId;
    this.changed = false;
    this.data = data;
	this.checkColumn;

    /* methods */
    this.getMemberships = function(data, storeFields)
    {
        var memberships = [];
        data.each(function(record){
            $H(record).each(function(subrecord){
                if (subrecord.key == 'memberships')
                    subrecord.value.each(function(membership)
                    {
                        storeFields[storeFields.length] = membership.membershipsId;
                        memberships[memberships.length] = {name: membership.membershipsName, id: membership.membershipsId};
                    });
            });
            throw $break;
        });
        return memberships;
    }

    this.getGridData = function(data)
    {
        var storeData = [];
        data.each(function(record){
            var storeDataRow = [];
            $H(record).each(function(subrecord){
                if (subrecord.key == 'memberships')
                    subrecord.value.each(function(membership)
                    {
                        storeDataRow[storeDataRow.length] = membership.membershipsAllow;
                    });
                else
                    storeDataRow[storeDataRow.length] = subrecord.value;
            });
            storeData[storeData.length] = storeDataRow;
        });
        return storeData;
    }

    this.getFilledColumns = function(memberships, gridColumns)
    {
        var checkColumns = [];
        memberships.each(function(membership){
            self.checkColumn = new Ext.grid.CheckColumn({
                header: membership.name,
                align: 'center',
                dataIndex: membership.id
            });
            checkColumns[checkColumns.length] = self.checkColumn;
            gridColumns[gridColumns.length] = self.checkColumn;
        });
        return {
            grid: gridColumns,
            check: checkColumns
        }
    }

    this.initGrid = function()
    {
        var storeFields = ['name', 'id', 'parentId', 'label', 'urlstring', 'description', 'priority'];
        var gridColumns = [
            { id : 'id', header : "Идентификатор модуля", hidden : true, hideable : false },
            { header : "Название модуля", dataIndex : 'name' },
            { header : "Описание модуля", dataIndex : 'description' }
        ];

        var memberships = this.getMemberships(this.data, storeFields);
        var columns = this.getFilledColumns(memberships, gridColumns);
        self.columns = columns.grid;
        this.plugins = columns.check;
        this.store = new Ext.data.SimpleStore({
            fields: storeFields,
            listeners: {
                update: function()
                {
                    self.saveButton.enable();
                    self.cancelButton.enable();
                    self.changed = true;
                }
            }
        });

        this.store.loadData(this.getGridData(this.data));
    }

    /* buttons */
    this.saveButton = new Ext.Button({
        iconCls: 'icon-accept-tick',
        text: 'Сохранить',
        disabled: true,
        handler: function() {this.fireEvent('save')},
        scope: this
    });

    this.cancelButton = new Ext.Button({
        iconCls: 'icon-close-cross',
        text: 'Отмена',
        disabled: true,
        handler: function() {this.fireEvent('cancel')},
        scope: this
    });

    this.selectAllButton = new Ext.Button({
        text: 'Выделить все',
        handler: function() {this.fireEvent('selectAll')},
        scope: this
    });

    this.deselectAllButton = new Ext.Button({
        text: 'Снять все',
        handler: function() {this.fireEvent('deselectAll')},
        scope: this
    });

    /* constructor */
    ModuleListMemberShipsGrid.superclass.constructor.call(this, {
    	viewConfig: { forceFit: true },
        clicksToEdit: 1,
        stripeRows: true,
        collapsible: false,
        autoExpandColumn: 'name',
        anchor: '100% 100%',
        region: 'center',
        title: 'Назначение системных модулей проекту:  ' + projectName,
        listeners: {
            render: function()
            {
                msgWait.hide();
            }
        },
        tbar: [this.saveButton, this.cancelButton, '-', this.selectAllButton, this.deselectAllButton]
    });
}

Ext.extend(ModuleListMemberShipsGrid, Ext.grid.GridPanel, {
	initComponent : function() {

		var self = this;

	 	this.initGrid();

	 	this.addEvents({
			save : true,
			cancel : true
		});

		this.on('cancel', function(){
            this.store.rejectChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
            this.changed = false;
        });

		this.on('save', function(){
			var msgWait = Ext.Msg.wait('Сохранение параметров...', 'Подождите', {interval: 32});
            var modifiedRecords = this.store.getModifiedRecords();
            var params = [];
            Ext.each(modifiedRecords, function(record){
                params[params.length] = record.data;
            });
            Systemadmin.conn.request(
            {
                url: '/systemadmin/setmemberships',
                method: 'POST',
                params: {memberships: Ext.util.JSON.encode(params), projectid: this.projectId},
                success: function(){
                	var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
                   	projecttree.getRootNode().reload(function () {
                   		var node = projecttree.getNodeById(self.projectId);
					    projecttree.expandPath(node.getPath());
					    projecttree.selectPath(node.getPath());
					    msgWait.hide();
                   	});
                }
            });
            this.store.commitChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
            this.changed = false;
        });

        this.on('selectAll', function(){
        	this.store.each(function(record){
            	record.set(Object.keys(record.data)[7], 1);
        	});
        });

        this.on('deselectAll', function(){
        	this.store.each(function(record){
            	record.set(Object.keys(record.data)[7], '');
        	});
        });

		ModuleListMemberShipsGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* CreateProjectForm - форма создания нового модуля*/
CreateProjectForm = function() {

	CreateProjectForm.superclass.constructor.call(this, {
		method : 'POST',
		frame : true,
		bodyStyle : 'padding:5px 10px 0',
		width : 490,
		items : [{
			xtype : 'fieldset',
			title : 'Основные параметры',
			autoHeight : true,
			width : 457,
			defaultType : 'textfield',
			labelWidth : 90,
			items : [{
               xtype:'hidden',
               name: 'id',
               allowBlank:true,
               anchor:'100%'
               }, this.project_name = new Ext.form.TextField({
				fieldLabel : 'Название',
				name : 'name',
                enableKeyEvents: true,
				width : 340,
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\w][-_ ]?)+$/,
				maskRe : /[a-zA-Z0-9-_ ]/
			}), {
				fieldLabel : 'Домен',
				name : 'domain',
				width : 340,
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\w][-_./]?)+$/,
				maskRe : /[a-zA-Z0-9-_./]/
			},  new Ext.form.TextArea({
				fieldLabel : 'Описание',
				name : 'description',
				width : 340
			}), this.database = new Ext.form.TextField ({
				fieldLabel : 'База данных',
				name : 'database',
				width : 340,
				value: 'GIS',
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\w][-_ ]?)+$/,
				maskRe : /[a-zA-Z0-9-_ ]/
			}), new Ext.form.TextField({
				fieldLabel : 'Легенда',
				name : 'title',
				msgTarget: 'under',
                allowBlank:false,
				width : 340,
				regex : /^([\wА-Яа-я][-_ ]?)+$/,
				maskRe : /[a-zA-ZА-Яа-я0-9-_ ]/
			})]
		},{},{
			xtype : 'fieldset',
			title : 'Схемы базы данных',
			autoHeight : true,
			width : 457,
			labelWidth : 90,
			items : [	this.scheme_wdb = new Ext.form.TextField({
						fieldLabel : 'Схема WDB',
						name : 'schemewdb',
						msgTarget: 'under',
                        readOnly: true,
						width : 340,
						regex : /^([\w][-_]?)+$/,
						maskRe : /[a-zA-Z0-9-_]/
					}),
					this.scheme_sdb = new Ext.form.TextField({
						fieldLabel : 'Схема SDB',
						name : 'schemesdb',
						msgTarget: 'under',
                        readOnly: true,
						width : 340,
						regex : /^([\w][-_]?)+$/,
						maskRe : /[a-zA-Z0-9-_]/
					}),
					this.scheme_adb = new Ext.form.TextField({
						fieldLabel : 'Схема ADB',
						name : 'schemeadb',
						msgTarget: 'under',
                        readOnly: true,
						width : 340,
						regex : /^([\w][-_]?)+$/,
						maskRe : /[a-zA-Z0-9-_]/
					}),
					this.scheme_user = new Ext.form.TextField({
						fieldLabel : 'Схема USER',
						name : 'schemeuser',
						msgTarget: 'under',
                        readOnly: true,
						width : 340,
						regex : /^([\w][-_]?)+$/,
						maskRe : /[a-zA-Z0-9-_]/
					})]
			}, {}, {
				xtype : 'fieldset',
				title : 'Дополнительные параметры',
				autoHeight : true,
				width : 457,
				items : [
					new Ext.form.Checkbox({
					name : 'locked',
					anchor : false,
					checked : false,
					hideLabel : true,
					boxLabel : 'Заблокировать проект'
					})]
			}],
        buttonAlign: 'right'
	});
    /* buttons */
    this.saveButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'Сохранить',
        handler : function() {
            this.fireEvent('save')
        },
        scope : this
    });

    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function() {
            this.fireEvent('cancel')
        },
        scope : this
    });

}
Ext.extend(CreateProjectForm, Ext.form.WgsFormPanel, {
	initComponent : function() {

		this.addEvents({
			save : true,
			cancel : true
		});

		var self = this;

        this.project_name.on('keyup', function() {
            if (this.project_name.getValue())
            {
                this.scheme_wdb.setValue((this.project_name.getValue() + "_WDB").toUpperCase());
                this.scheme_sdb.setValue((this.project_name.getValue() + "_SDB").toUpperCase());
                this.scheme_adb.setValue((this.project_name.getValue() + "_ADB").toUpperCase());
                this.scheme_user.setValue((this.project_name.getValue() + "_USER").toUpperCase());
            } else {
                this.scheme_wdb.setValue('');
                this.scheme_sdb.setValue('');
                this.scheme_adb.setValue('');
                this.scheme_user.setValue('');
            }
        }, this);

		// cancel
		this.on('cancel', function() {
			Systemadmin.createProjectWindow.hide();
		});

		// save
		this.on('save', function() {
			Systemadmin.createProjectWindow.hide();
		});

		// save
		this.on('save', function() {

			var msgWait = Ext.Msg.wait('Операция выполняется...', 'Подождите', {interval: 50});
			self.submit({
				success : function(form, action) {
					var nodeName = self.getForm().getValues().name;
					Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid.storeProjectList.reload();
					Systemadmin.createProjectWindow.hide();
					var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
					projecttree.getRootNode().reload(function () {
					    projecttree.collapseAll();
					    var node = projecttree.getRootNode().firstChild.findChild('name', nodeName);
					    if (node) {
					    	projecttree.expandPath(node.getPath());
					    	projecttree.selectPath(node.getPath());
					    }
						projecttree.fireEvent('click', node);
					    msgWait.hide();
					});
				},
			failure:function(form, action) {
				Systemadmin.createProjectWindow.show();
				msgWait.hide();
			}
			});
		});

		CreateProjectForm.superclass.initComponent.apply(this, arguments);
	}
});
/* CreateProjectWindow - окно создания нового модуля */
CreateProjectWindow = function() {

	this.createProjectForm = new CreateProjectForm();
	CreateProjectWindow.superclass.constructor.call(this, {
		title : 'Создание нового проекта',
		closable : true,
		width : 502,
		autoHeight : true,
		border : false,
		resizable : false,
		modal : true,
		plain : true,
		closeAction : 'hide',
		items : this.createProjectForm
	});
}
Ext.extend(CreateProjectWindow, Ext.Window, {});

// ProjectListGrid - список проектов
ProjectListGrid = function()
{
     // public
     var self = this;

     // storeProjectList
	 this.storeProjectList = new Ext.data.JsonStore({
	 	 autoLoad : true,
	 	 url : '/systemadmin/GetStoreProjectList',
	 	 fields : ['id', 'name', 'nameHtml', 'domain', 'description', 'database', 'title', 'schemewdb', 'schemesdb', 'schemeadb', 'schemeuser', 'locked']
	 });

     // buttons
     this.createButton = new Ext.Button({
          text : 'Создать',
          iconCls: 'icon-add',
          handler : function()
          {
               this.fireEvent('create')
          },
          scope : this
     });

     this.editButton = new Ext.Button({
          text : 'Свойства',
          iconCls: 'icon-properties',
          handler : function()
          {
               this.fireEvent('edit')
          },
          scope : this
     });

     this.removeButton = new Ext.Button({
          text : 'Удалить',
          iconCls: 'icon-delete',
          handler : function()
          {
               this.fireEvent('remove')
          },
          scope : this
     });

     this.projectSelectionModel = new Ext.grid.CheckboxSelectionModel({
     singleSelect: true
     });
     // constructor
     ProjectListGrid.superclass.constructor.call(this, {

          store : this.storeProjectList,
          title: 'Менеджер проектов',
          collapsible: false,
          viewConfig: { forceFit: true },
          cm : new Ext.grid.ColumnModel([//this.projectSelectionModel,
               { id : 'id', header : "Идентификатор проекта", hidden : true, hideable : false },
               { header : "Проект", sortable : true, dataIndex : 'nameHtml' },
               { header : "Домен", dataIndex : 'domain' },
               { header : "Описание", dataIndex : 'description' },
               { header : "База данных", dataIndex : 'database' },
               { header : "Легенда", dataIndex : 'title' },
               { header : "Схема WDB", dataIndex : 'schemewdb', hidden: true },
               { header : "Схема SDB", dataIndex : 'schemesdb', hidden: true},
               { header : "Схема ADB", dataIndex : 'schemeadb', hidden: true },
               { header : "Схема USER", dataIndex : 'schemeuser', hidden: true }

          ]),
          sm : this.projectSelectionModel,
          autoScroll : true,
		  border: false,
		  stripeRows: true,
          tbar : [ this.createButton, '-', this.editButton/*, '-', this.removeButton*/ ]

     });
}

Ext.extend(ProjectListGrid, Ext.grid.GridPanel, {
	initComponent : function() {

		this.addEvents({
			create : true,
			edit : true,
			remove : true
		});

		// create module
		this.on('create', function() {
			Systemadmin.createProjectWindow.setTitle('Создание нового проекта');

			Systemadmin.createProjectWindow.show();
			Systemadmin.createProjectWindow.createProjectForm.getForm().url = '/systemadmin/createsysproject/';
			Systemadmin.createProjectWindow.createProjectForm.getForm().reset();

            Systemadmin.createProjectWindow.createProjectForm.project_name.getEl().dom.readOnly = false;
            Systemadmin.createProjectWindow.createProjectForm.database.getEl().dom.readOnly = false;

		});

		// edit module
		this.on('edit', function() {
			var records = this.projectSelectionModel.getSelections();
			if (records[0])
			{
					Systemadmin.createProjectWindow.setTitle('Редактирование проекта: '+records[0].data.name);
                    Systemadmin.createProjectWindow.show();
                    Systemadmin.createProjectWindow.createProjectForm.getForm().url = '/systemadmin/editsysproject/';
                    Systemadmin.createProjectWindow.createProjectForm.project_name.getEl().dom.readOnly = true;
                    Systemadmin.createProjectWindow.createProjectForm.database.getEl().dom.readOnly = true;
                    Systemadmin.createProjectWindow.createProjectForm.getForm().loadRecord(records[0]);
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать проект для редактирования.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		// remove module
		this.on('remove', function() {
			var records = this.projectSelectionModel.getSelections();
			if (records[0])
			{
				Ext.MessageBox.confirm('Подтверждение удаления', 'Вы действительно хотите удалить проект "'+records[0].data.name+'" ?',
				function(btn) {
					if (btn == 'yes')
			    	{
			       	  var msgWait = Ext.Msg.wait('Удаление проекта...', 'Подождите', {interval: 50});
			    	  	Systemadmin.conn.request({
						   url: 'systemadmin/removesysproject',
						   params: { id: records[0].data.id},
						   success: function(response){
								Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid.storeProjectList.reload();
								Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
								var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
								projecttree.getRootNode().reload( function (){
									if (projecttree.getRootNode().firstChild) {
										projecttree.expandPath(projecttree.getRootNode().firstChild.getPath());
										projecttree.selectPath(projecttree.getRootNode().firstChild.getPath());
									}
									msgWait.hide();
								});
						   }
						});
			    	}
				});
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать проект для удаления.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		this.on('rowclick', function() {
			var records = this.projectSelectionModel.getSelections();
			var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
			if (records.length != 0)
			{
				var node = projecttree.getNodeById(records[0].data.id);
					    projecttree.expandPath(node.getPath());
					    projecttree.selectPath(node.getPath());
				Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.generateGrid(records[0].data.id, records[0].data.name);
			}
			else {
				var node = projecttree.getRootNode().firstChild;
					    projecttree.expandPath(node.getPath());
					    projecttree.selectPath(node.getPath());
				Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
			}
		});

		ProjectListGrid.superclass.initComponent.apply(this, arguments);
	}
});

// ProjectMemberShipsPanel - назначение проектам модулей
ProjectMemberShipsPanel = function() {

	this.addmoduleListMemberShipsGrid = function(grid)
    {
        this.moduleListMemberShipsGrid = grid;
        this.add(this.moduleListMemberShipsGrid);
        this.doLayout();
    }

    this.clear = function()
    {
        if (this.moduleListMemberShipsGrid)
        {
            this.remove(this.moduleListMemberShipsGrid);
            this.moduleListMemberShipsGrid = null;
        }
    }

    this.generateGrid = function (id, text) {
    	Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel);
        var msgWait = Ext.Msg.wait('Генерация списка доступных модулей...', 'Подождите', {interval:32});
        Systemadmin.conn.request(
        {
            url: '/systemadmin/getmemberships',
            params: {id: id},
            success: function(response)
            {
                Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
                Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.addmoduleListMemberShipsGrid(
                    new ModuleListMemberShipsGrid(Ext.util.JSON.decode(response.responseText), id, text, msgWait)
                );
            }
        });
    }

	ProjectMemberShipsPanel.superclass.constructor.call(this, {
		region: 'center',
		border: false,
		collapsible : false,
		margins : '0 0 0 0',
		layout : 'fit',
		split: true
	});
}
Ext.extend(ProjectMemberShipsPanel, Ext.Panel, {});

// ProjectInformationPanel - информация о проектах системы
ProjectInformationPanel = function() {
	this.projectListGrid = new ProjectListGrid();
	ProjectInformationPanel.superclass.constructor.call(this, {
		region: 'north',
		border: false,
		collapsible : false,
		split: true,
		height: 250,
		minSize : 170,
		maxSize : 350,
		margins : '0 0 0 0',
		layout : 'fit',
		items: [this.projectListGrid]
	});
}
Ext.extend(ProjectInformationPanel, Ext.Panel, {});

// ProjectControlPanel - управление проектами системы
ProjectControlPanel = function() {
	this.projectInformationPanel = new ProjectInformationPanel();
	this.projectMemberShipsPanel = new ProjectMemberShipsPanel();
	ProjectControlPanel.superclass.constructor.call(this, {
		collapsible : false,
		margins : '0 0 0 0',
		layout : 'border',
		title: 'Управление системными проектами',
		items: [this.projectInformationPanel, this.projectMemberShipsPanel]
	});
}
Ext.extend(ProjectControlPanel, Ext.Panel, {});

// PrivilegeListGrid
PrivilegeListGrid = function()
{
     /* public */
     var self = this;

     // storePrivilegeGrid
	 this.storePrivilegeGrid = new Ext.data.JsonStore({
	 	 autoLoad : true,
		 url : '/systemadmin/getstoreprivilegelist/',
		 fields : ['id', 'name', 'nameHtml', 'description', 'objectid', 'locked']
	 });

     /* buttons */
     this.createButton = new Ext.Button({
          text : 'Создать',
          iconCls: 'icon-add',
          handler : function()
          {
               this.fireEvent('create')
          },
          scope : this
     });

     this.editButton = new Ext.Button({
          text : 'Свойства',
          iconCls: 'icon-properties',
          handler : function()
          {
               this.fireEvent('edit')
          },
          scope : this
     });

     this.removeButton = new Ext.Button({
          text : 'Удалить',
          iconCls: 'icon-delete',
          handler : function()
          {
               this.fireEvent('remove')
          },
          scope : this
     });

     this.privilegeSelectionModel = new Ext.grid.CheckboxSelectionModel({
     //singleSelect: true
     });
     // constructor
     PrivilegeListGrid.superclass.constructor.call(this, {

          store : this.storePrivilegeGrid,
          viewConfig: { forceFit: true },
          cm : new Ext.grid.ColumnModel([//this.privilegeSelectionModel,
               { id : 'id', header : "Идентификатор привилегии", hidden : true, hideable : false },
	           { header : "Название привилегии", sortable : true, dataIndex : 'nameHtml' },
	           { header : "Описание привилегии", dataIndex : 'description' }

          ]),
          sm : this.privilegeSelectionModel,
          autoScroll : true,
		  stripeRows: true,
          tbar : [ this.createButton, '-', this.editButton, '-', this.removeButton ]
     });
}

Ext.extend(PrivilegeListGrid, Ext.grid.GridPanel, {
	initComponent : function() {

		var self = this;

		this.addEvents({
			create : true,
			edit : true,
			remove : true
		});

		this.storePrivilegeGrid.on('load', function() {
			var countRecord = 0;
			if (Systemadmin.createPrivilegeWindow.createPrivilegeForm.msgWait)
			Systemadmin.createPrivilegeWindow.createPrivilegeForm.msgWait.hide();

			Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();


			var roletree = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree;
			if (roletree.getRootNode().firstChild) {
			roletree.expandPath(roletree.getRootNode().firstChild.getPath());
			roletree.selectPath(roletree.getRootNode().firstChild.getPath());
			}
			Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.toggle(false);
            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overModulesButton.toggle(false);

			Systemadmin.objectManagementPanel.objectTabPanel.privilegeControlPanel.privilegeListGrid.storePrivilegeGrid.each(function(record) {
				if (Systemadmin.createPrivilegeWindow.createPrivilegeForm.name == record.data.name){
					Systemadmin.objectManagementPanel.objectTabPanel.privilegeControlPanel.privilegeListGrid.privilegeSelectionModel.selectRow(countRecord);
				}
				countRecord++;
			});
		}, this);

		// create module
		this.on('create', function() {
				Systemadmin.createPrivilegeWindow.setTitle('Создание новой системной привилегии');
				Systemadmin.createPrivilegeWindow.show();
				Systemadmin.createPrivilegeWindow.createPrivilegeForm.getForm().url = '/systemadmin/createsysprivilege/';
				Systemadmin.createPrivilegeWindow.createPrivilegeForm.getForm().reset();
		});

		// edit module
		this.on('edit', function() {
			var records = this.privilegeSelectionModel.getSelections();
			if (records[0])
			{
					Systemadmin.createPrivilegeWindow.setTitle('Редактирование системной привилегии: '+records[0].data.name);
                    Systemadmin.createPrivilegeWindow.show();
                    Systemadmin.createPrivilegeWindow.createPrivilegeForm.getForm().url = '/systemadmin/editsysprivilege/';
                    Systemadmin.createPrivilegeWindow.createPrivilegeForm.getForm().loadRecord(records[0]);
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать привилегию для редактирования.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		// remove module
		this.on('remove', function() {
			var records = this.privilegeSelectionModel.getSelections(); var params = '';

			if (records[0])
			{
				for (i = 0; i < records.length; i++)
					params = params + "_" + records[i].data.id;
				Ext.MessageBox.confirm('Подтверждение удаления', 'Вы действительно хотите удалить выбранные привилегии?"',
				function(btn) {
					if (btn == 'yes')
			    	{
			       	  var msgWait = Ext.Msg.wait('Удаление привилегий...', 'Подождите', {interval: 50});
			    	  	Systemadmin.conn.request({
						   url: 'systemadmin/removesysprivilege',
						   params: { id: params},
						   success: function(response){
						   		Systemadmin.setPermissionChangeAction(false);
								self.storePrivilegeGrid.reload();
								Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
								var roletree = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree;
								if (roletree.getRootNode().firstChild) {
									roletree.expandPath(roletree.getRootNode().firstChild.getPath());
									roletree.selectPath(roletree.getRootNode().firstChild.getPath());
								}
								Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.toggle(false);
								Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overModulesButton.toggle(false);
								msgWait.hide();
						   }
						});
			    	}
				});
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать привилегию для удаления.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		PrivilegeListGrid.superclass.initComponent.apply(this, arguments);
	}
});

// PrivilegeControlPanel
PrivilegeControlPanel = function() {
	this.privilegeListGrid = new PrivilegeListGrid();
	PrivilegeControlPanel.superclass.constructor.call(this, {
		collapsible : false,
		margins : '0 0 0 0',
		layout : 'fit',
		title: 'Управление системными привилегиями',
		items: [this.privilegeListGrid]
	});
}
Ext.extend(PrivilegeControlPanel, Ext.Panel, {});

/* CreatePrivilegeForm - форма создания новой привилегии*/
CreatePrivilegeForm = function() {

	CreatePrivilegeForm.superclass.constructor.call(this, {
		method : 'POST',
		frame : true,
		bodyStyle : 'padding:5px 10px 0',
		width : 490,
		items : [
			{
			xtype : 'fieldset',
			title : 'Основные параметры',
			autoHeight : true,
			width : 457,
			defaultType : 'textfield',
			labelWidth : 90,
			items : [{
               xtype:'hidden',
               name: 'id',
               allowBlank:true,
               anchor:'100%'
               },{
				fieldLabel : 'Название',
				name : 'name',
				width : 340,
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\w][_]?)+$/,
				maskRe : /[A-Z_]/
			   }, new Ext.form.TextArea({
				fieldLabel : 'Описание',
				name : 'description',
				width : 340
			   })
			]},{}, {
				xtype : 'fieldset',
				title : 'Дополнительные параметры',
				autoHeight : true,
				width : 457,
				items : [
					new Ext.form.Checkbox({
					name : 'locked',
					anchor : false,
					checked : false,
					hideLabel : true,
					boxLabel : 'Заблокировать привилегию'
					})]
			}],
        buttonAlign: 'right'
	});
    this.saveButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'Сохранить',
        handler : function() {
            this.fireEvent('save')
        },
        scope : this
    });

    this.cancelButton = this.addButton({
    	iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function() {
            this.fireEvent('cancel')
        },
        scope : this
    });
}
Ext.extend(CreatePrivilegeForm, Ext.form.WgsFormPanel, {
	initComponent : function() {

		this.addEvents({
			save : true,
			cancel : true
		});

		// cancel
		this.on('cancel', function() {
			Systemadmin.createPrivilegeWindow.hide();
		});

		var self = this;
		this.msgWait = false;
		this.name = false;

		// save
		this.on('save', function() {
			self.msgWait = Ext.Msg.wait('Операция выполняется...', 'Подождите', {interval: 50});
			self.submit({

				success : function(form, action) {
					self.name = self.getForm().getValues().name;
					Systemadmin.createPrivilegeWindow.hide();
					Systemadmin.objectManagementPanel.objectTabPanel.privilegeControlPanel.privilegeListGrid.storePrivilegeGrid.reload();
					Systemadmin.setPermissionChangeAction(false);
				},

				failure:function(form, action) {
					Systemadmin.createPrivilegeWindow.show();
					self.msgWait.hide();
				}
			});
		});

		CreatePrivilegeForm.superclass.initComponent.apply(this, arguments);
	}
});

/* CreatePrivilegeWindow - окно создания новой привилегии */
CreatePrivilegeWindow = function() {

	this.createPrivilegeForm = new CreatePrivilegeForm();
	CreatePrivilegeWindow.superclass.constructor.call(this, {
		closable : true,
		width : 502,
		autoHeight : true,
		border : false,
		resizable : false,
		modal : true,
		plain : true,
		closeAction : 'hide',
		items : this.createPrivilegeForm
	});
}

Ext.extend(CreatePrivilegeWindow, Ext.Window, {});

// ModuleListGrid
ModuleListGrid = function()
{
     /* public */
     var self = this;

     // storeModuleGrid
	 this.storeModuleGrid = new Ext.data.JsonStore({
		 autoLoad : true,
		 url : '/systemadmin/GetModuleListTree',
		 fields : ['id', 'name', 'nameHTML', 'label', 'urlstring', 'description', 'priority', 'parentId', 'container', 'inmenu', 'locked']
	 });

     /* buttons */
     this.createButton = new Ext.Button({
          text : 'Создать',
          iconCls: 'icon-add',
          handler : function()
          {
               this.fireEvent('create')
          },
          scope : this
     });

     this.editButton = new Ext.Button({
          text : 'Свойства',
          iconCls: 'icon-properties',
          handler : function()
          {
               this.fireEvent('edit')
          },
          scope : this
     });

     this.removeButton = new Ext.Button({
          text : 'Удалить',
          iconCls: 'icon-delete',
          handler : function()
          {
               this.fireEvent('remove')
          },
          scope : this
     });


     this.moduleSelectionModel = new Ext.grid.CheckboxSelectionModel({
     singleSelect: true
     });
     // constructor
     ModuleListGrid.superclass.constructor.call(this, {

          store : this.storeModuleGrid,
          viewConfig: { forceFit: true },
          cm : new Ext.grid.ColumnModel([//this.moduleSelectionModel,
               { id : 'id', header : "Идентификатор модуля", hidden : true, hideable : false },
               { dataIndex : 'name', hidden : true, hideable : false },
               { dataIndex : 'name', hidden : true, hideable : false },
               { dataIndex : 'container', hidden : true, hideable : false },
               { dataIndex : 'inmenu', hidden : true, hideable : false },
               { header : "Название модуля", dataIndex : 'label' },
               { header : "URL-адрес", dataIndex : 'urlstring' },
               { header : "Описание модуля", dataIndex : 'description' },
               { header : "Приоритет модуля", dataIndex : 'priority' }

          ]),
          sm : this.moduleSelectionModel,
          autoScroll : true,
		  stripeRows: true,
          tbar : [ this.createButton, '-', this.editButton, '-', this.removeButton ]
     });
}

Ext.extend(ModuleListGrid, Ext.grid.GridPanel, {
	initComponent : function() {

		var self = this;

		this.addEvents({
			create : true,
			edit : true,
			remove : true
		});

		// create module
		this.on('create', function() {
				Systemadmin.createModuleWindow.setTitle('Создание нового модуля');
				Systemadmin.createModuleWindow.show();
				Systemadmin.createModuleWindow.createModuleForm.getForm().url = '/systemadmin/CreateSysModule/';
				Systemadmin.createModuleWindow.createModuleForm.getForm().reset();

		});

		// edit module
		this.on('edit', function() {
			var records = this.moduleSelectionModel.getSelections();
			if (records[0])
			{
					Systemadmin.createModuleWindow.setTitle('Редактирование модуля: '+records[0].data.label);
                    Systemadmin.createModuleWindow.show();
                    Systemadmin.createModuleWindow.createModuleForm.getForm().url = '/systemadmin/EditSysModule/';
                    Systemadmin.createModuleWindow.createModuleForm.getForm().loadRecord(records[0]);

                    var parent = 0;
                    var count = 0;
                    var value;
					self.storeModuleGrid.each(function(record) {
						if (count == 0) value = record.data.label;
						if (records[0].data.parentId == record.data.id) {
							Systemadmin.createModuleWindow.createModuleForm.parentField.expand();
							Systemadmin.createModuleWindow.createModuleForm.moduleCombobox.setValue(record.data.label);
							parent++;
						}
						count++;
					});
					if (parent == 0) {
						Systemadmin.createModuleWindow.createModuleForm.parentField.collapse();
					}
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать модуль для редактирования.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		// remove module
		this.on('remove', function() {
			var records = this.moduleSelectionModel.getSelections();
			if (records[0])
			{
				Ext.MessageBox.confirm('Подтверждение удаления', 'Вы действительно хотите удалить модуль "'+records[0].data.label+'" ?',
				function(btn) {
					if (btn == 'yes')
			    	{
			    	  Systemadmin.setPermissionChangeAction(false);
			       	  var msgWait = Ext.Msg.wait('Удаление модуля...', 'Подождите', {interval: 50});
			       	  var moduletree = Systemadmin.objectManagementPanel.objectsTreePanel.overModulesPanel.moduleTree;
			       	  var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
			    	  	Systemadmin.conn.request({
						   url: 'systemadmin/RemoveSysModule',
						   success: function(response){
						   		Systemadmin.createModuleWindow.createModuleForm.storeModuleList.reload();
						   		self.storeModuleGrid.reload();
						   		Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
		                    	moduletree.getRootNode().reload(function () {
		                        var node = moduletree.getNodeById('root');
		                        moduletree.selectPath(node.getPath());
		                        Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel.moduleListGrid.moduleSelectionModel.clearSelections();
		                        Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
		                        Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid.projectSelectionModel.clearSelections();
		                        var node = projecttree.getRootNode().firstChild;
								projecttree.expandPath(node.getPath());
								projecttree.selectPath(node.getPath());
		                        msgWait.hide();
		                    	});
		                    // очищаем панель назначения прав доступа
							var roletree = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree;
							if (roletree.getRootNode().firstChild) {
							roletree.expandPath(roletree.getRootNode().firstChild.getPath());
							roletree.selectPath(roletree.getRootNode().firstChild.getPath());
							}
							Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.toggle(false);
				            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overModulesButton.toggle(false);
						   },
						   params: { id: moduletree.getSelectionModel().getSelectedNode().id }
						});
			    	}
				});
			}
			else Ext.MessageBox.show({
		         	title: 'Предупреждение',
			     	msg: 'Необходимо выбрать модуль для удаления.',
			     	buttons: Ext.MessageBox.OK,
			     	icon: Ext.MessageBox.WARNING
		   });
		});

		this.on('rowclick', function() {
			var records = this.moduleSelectionModel.getSelections();
			var moduletree = Systemadmin.objectManagementPanel.objectsTreePanel.overModulesPanel.moduleTree;
			if (records.length != 0)
			{
				var node = moduletree.getNodeById(records[0].data.id);
					    moduletree.expandPath(node.getPath());
					    moduletree.selectPath(node.getPath());
			}
			else {
				var node = moduletree.getRootNode().firstChild;
					    moduletree.expandPath(node.getPath());
					    moduletree.selectPath(node.getPath());
			}
		});

		ModuleListGrid.superclass.initComponent.apply(this, arguments);
	}
});

// ModuleControlPanel
ModuleControlPanel = function() {
	this.moduleListGrid = new ModuleListGrid();
	ModuleControlPanel.superclass.constructor.call(this, {
		collapsible : false,
		margins : '0 0 0 0',
		layout : 'fit',
		title: 'Управление системными модулями',
		items: [this.moduleListGrid]
	});
}
Ext.extend(ModuleControlPanel, Ext.Panel, {});

/* CreateModuleForm - форма создания нового модуля*/
CreateModuleForm = function() {

	var self = this;

	this.storeModuleList = new Ext.data.JsonStore({
		autoLoad : true,
		url : '/systemadmin/GetModuleListTree',
		fields : ['id', 'name', 'nameHTML', 'label', 'urlstring', 'description', 'priority', 'parentId', 'container', 'inmenu', 'locked']
	});

	this.moduleCombobox = new Ext.form.ComboBox({
		name : 'parentId',
		width : 430,
		store : self.storeModuleList,
		loadingText: 'Загрузка...',
		emptyText: 'Выберите модуль...',
		triggerAction : 'all',
		onSelect : function(record, index) {
			if (self.getForm().getValues().label != record.data.label)
			this.setValue(record.data.label);
			else this.setValue(null);
			this.collapse();
		},
		displayField : 'nameHTML',
		hiddenName : 'parentId',
		labelSeparator: '',
		editable : false
	});

	CreateModuleForm.superclass.constructor.call(this, {
		method : 'POST',
		frame : true,
		bodyStyle : 'padding:5px 10px 0',
		width : 490,
		items : [{
			xtype : 'fieldset',
			title : 'Основные параметры',
			autoHeight : true,
			width : 457,
			defaultType : 'textfield',
			labelWidth : 90,
			items : [{
               xtype:'hidden',
               name: 'id',
               allowBlank:true,
               anchor:'100%'
               },this.parentModule = new Ext.form.Hidden ({
                 name: 'parentModule'
               }),{
				fieldLabel : 'Название',
				name : 'name',
				width : 340,
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\w][-_ ]?)+$/,
				maskRe : /[a-zA-Z0-9-_ ]/
			}, {
				fieldLabel : 'Легенда',
				name : 'label',
				width : 340,
				msgTarget: 'under',
                allowBlank:false,
				regex : /^([\wА-Яа-я][-_ ]?)+$/,
				maskRe : /[a-zA-ZА-Яа-я0-9-_ ]/
			}, this.urlstring = new Ext.form.TextField ({
				fieldLabel : 'URL-адрес',
				name : 'urlstring',
				width : 340,
				msgTarget: 'under',
				regex : /^([\w/][-_]?)+$/,
				maskRe : /[a-zA-Z0-9-_/]/
			}), new Ext.form.TextArea({
				fieldLabel : 'Описание',
				name : 'description',
				width : 340
			}), new Ext.form.NumberField({
				fieldLabel : 'Приоритет',
				name : 'priority',
				value : '0',
				msgTarget: 'under',
                allowBlank:false,
				width : 340,
				maskRe : /[0-9]/
			})]
		}, {}, this.parentField = new Ext.form.FieldSet({
				xtype:'fieldset',
	            checkboxToggle:true,
	            title: 'Родительский модуль',
	            autoHeight:true,
	            labelWidth : 1,
	            collapsed: false,
	            items :[this.moduleCombobox]
			}),{
			xtype : 'fieldset',
			title : 'Дополнительные параметры',
			autoHeight : true,
			width : 457,
			items : [this.container = new Ext.form.Checkbox({
				name : 'container',
				anchor : false,
				checked : false,
				hideLabel : true,
				boxLabel : 'Отображать url - адрес в системном меню'
			}), new Ext.form.Checkbox({
				name : 'inmenu',
				anchor : false,
				checked : false,
				hideLabel : true,
				boxLabel : 'Отображать модуль в системном меню'
			}), new Ext.form.Checkbox({
				name : 'locked',
				anchor : false,
				checked : false,
				hideLabel : true,
				boxLabel : 'Заблокировать модуль'
			})]
		}],
        buttonAlign: 'right'
	});
    this.saveButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'Сохранить',
        handler : function() {
            this.fireEvent('save')
        },
        scope : this
    });

    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function() {
            this.fireEvent('cancel')
        },
        scope : this
    });
}
Ext.extend(CreateModuleForm, Ext.form.WgsFormPanel, {
	initComponent : function() {

		this.addEvents({
			save : true,
			cancel : true
		});

        var self = this;

        this.parentField.on('collapse', function(){
            this.parentModule.setValue('off');
        }, this);

        this.parentField.on('expand', function(){
            this.parentModule.setValue('on');
        }, this);

		// cancel
		this.on('cancel', function() {
			Systemadmin.createModuleWindow.hide();
		});

		// save
		this.on('save', function() {

			var msgWait = Ext.Msg.wait('Операция выполняется...', 'Подождите', {interval: 50});
			this.storeModuleList.each (function(record) {
				if (record.data.label == this.moduleCombobox.getValue())
					this.moduleCombobox.setValue(record.data.id);
			}, this);

			self.submit({
				success : function(form, action) {
					Systemadmin.setPermissionChangeAction(false);
					var nodeName = self.getForm().getValues().name;
					var nodeId = self.moduleCombobox.getValue();
					self.storeModuleList.reload();
					Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel.moduleListGrid.storeModuleGrid.reload();
					Systemadmin.createModuleWindow.hide();
					var moduletree = Systemadmin.objectManagementPanel.objectsTreePanel.overModulesPanel.moduleTree;
					moduletree.getRootNode().reload(function () {
					    moduletree.collapseAll();
					    if (nodeId)
					    {
						    var node = moduletree.getNodeById(nodeId).findChild('name', nodeName);
						    if (!node) node = moduletree.getRootNode().firstChild.findChild('name', nodeName);
						    if (!node) node = moduletree.getNodeById(self.getForm().getValues().id);
						    if (node) {
						    	moduletree.expandPath(node.getPath());
						    	moduletree.selectPath(node.getPath());
						    }
					    }
					    else
					    {
					    	node = moduletree.getNodeById(self.getForm().getValues().id);
					    	if (!node) node = moduletree.getRootNode().firstChild.findChild('name', nodeName);
						    if (node) {
						    	moduletree.expandPath(node.getPath());
						    	moduletree.selectPath(node.getPath());
						    }
					    }
					    msgWait.hide();

						var count = 0;
						self.storeModuleList.each (function(record) {
							if (node && node.id == record.data.id)
								Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel.moduleListGrid.moduleSelectionModel.selectRow(count);
							count++;
						});

				        Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
			            Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid.projectSelectionModel.clearSelections();
			            var projecttree = Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.projectTree;
			            projecttree.getRootNode().reload(function (){
			            	var projectnode = projecttree.getRootNode().firstChild;
							projecttree.expandPath(projectnode.getPath());
							projecttree.selectPath(projectnode.getPath());
						});
						Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();

						// очищаем панель назначения прав доступа
						var roletree = Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.roleTree;
						if (roletree.getRootNode().firstChild) {
						roletree.expandPath(roletree.getRootNode().firstChild.getPath());
						roletree.selectPath(roletree.getRootNode().firstChild.getPath());
						}
						Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.toggle(false);
			            Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overModulesButton.toggle(false);
					});
				},
			failure:function(form, action) {
				self.storeModuleList.each (function(record) {
					if (record.data.id == self.moduleCombobox.getValue())
						self.moduleCombobox.setValue(record.data.label);
				});
				Systemadmin.createModuleWindow.show();
				msgWait.hide();
			}
			});
		});

		CreateModuleForm.superclass.initComponent.apply(this, arguments);
	}
});

/* CreateModuleWindow - окно создания нового модуля */
CreateModuleWindow = function() {

	this.createModuleForm = new CreateModuleForm();
	CreateModuleWindow.superclass.constructor.call(this, {
		title : 'Создание нового модуля',
		closable : true,
		width : 502,
		autoHeight : true,
		border : false,
		resizable : false,
		modal : true,
		plain : true,
		closeAction : 'hide',
		items : this.createModuleForm
	});
}

Ext.extend(CreateModuleWindow, Ext.Window, {});

/* OverProjectsPanel */
OverProjectsPanel = function() {
	this.projectTree = new Ext.tree.TreePanel({
		rootVisible : false,
		root : new Ext.tree.AsyncTreeNode({
			text : 'Список проектов',
			draggable : false
		}),
		animate : true,
		singleExpand : false,
		autoScroll : true,
		loader : new Ext.tree.TreeLoader({
			dataUrl : '/systemadmin/GetProjectList'
		}),
		enableDD : false,
		border : false,
		containerScroll : true,
		dropConfig : {appendOnly : true}
	});

	OverProjectsPanel.superclass.constructor.call(this, {
		title : 'Системные проекты',
		border : false,
		autoScroll : true,
		iconCls : 'nav',
		items : this.projectTree
	});
}
Ext.extend(OverProjectsPanel, Ext.Panel, {
	initComponent : function() {

		var self = this;

		this.on('expand', function() {
			if (Systemadmin.objectManagementPanel.objectTabPanel.getActiveTab() != Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel)
			Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel);
		});

		this.on('collapse', function() {
			if (Systemadmin.objectManagementPanel.objectTabPanel.getActiveTab() != Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel)
			Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.getActiveTab());
		});

		this.projectTree.on('load', function() {
			var node = self.projectTree.getRootNode().firstChild;
			self.projectTree.expandPath(node.getPath());
			self.projectTree.selectPath(node.getPath());
		});

		this.projectTree.on('click', function(node) {
			var projectlistGrid = Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid;
			var checked = 0;
			var count = 0;
			Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectInformationPanel.projectListGrid.storeProjectList.each(function(record) {
				if (node.id == record.data.id) {
					projectlistGrid.projectSelectionModel.selectRow(count);
					checked++;
				}
				count++;
			});
			if (checked == 0) {
				projectlistGrid.projectSelectionModel.clearSelections();
				Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.clear();
			}
			if (node.attributes.type == 1)
            {
            	Systemadmin.objectManagementPanel.objectTabPanel.projectControlPanel.projectMemberShipsPanel.generateGrid(node.id, node.text);
            }
		});

		OverProjectsPanel.superclass.initComponent.apply(this, arguments);
	}
});

/* OverModulesPanel */
OverModulesPanel = function() {
	this.moduleTree = new Ext.tree.TreePanel({
		rootVisible : false,
		root : new Ext.tree.AsyncTreeNode({
			text : 'Список модулей',
			draggable : false
		}),
		animate : true,
		singleExpand : false,
		autoScroll : true,
		loader : new Ext.tree.TreeLoader({
			dataUrl : '/systemadmin/GetModuleList'
		}),
		enableDD : false,
		border : false,
		containerScroll : true,
		dropConfig : {appendOnly : true}
	});

	OverModulesPanel.superclass.constructor.call(this, {
		title : 'Системные модули',
		border : false,
		autoScroll : true,
		iconCls : 'nav',
		items : this.moduleTree
	});
}
Ext.extend(OverModulesPanel, Ext.Panel, {
	initComponent : function() {

		var self = this;

		this.on('expand', function() {
			Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel);
		});

		this.on('collapse', function() {
			if (Systemadmin.objectManagementPanel.objectTabPanel.getActiveTab() != Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel)
			Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.getActiveTab());
		});

		this.moduleTree.on('load', function() {
			var node = self.moduleTree.getRootNode().firstChild;
			self.moduleTree.expandPath(node.getPath());
			self.moduleTree.selectPath(node.getPath());
		});

		this.moduleTree.on('click', function(node) {
			var modulelistGrid = Systemadmin.objectManagementPanel.objectTabPanel.moduleControlPanel.moduleListGrid;
			var checked = 0;
			var count = 0;
			Systemadmin.createModuleWindow.createModuleForm.storeModuleList.each (function(record) {
				if (record.data.id == node.id) {
					modulelistGrid.moduleSelectionModel.selectRow(count);
					checked++;
				}
				count++;
			});
			if (checked == 0) modulelistGrid.moduleSelectionModel.clearSelections();
		});

		OverModulesPanel.superclass.initComponent.apply(this, arguments);
	}
});

/* OverRolesPanel */
OverRolesPanel = function() {
	this.roleTree = new Ext.tree.TreePanel({
		rootVisible : false,
		root : new Ext.tree.AsyncTreeNode({
			text : 'Список ролей',
			draggable : false
		}),
		animate : true,
		singleExpand : false,
		autoScroll : true,
		loader : new Ext.tree.TreeLoader({
			dataUrl : '/systemadmin/GetRoleList'
		}),
		enableDD : false,
		border : false,
		containerScroll : true,
		dropConfig : {appendOnly : true}
	});

	OverRolesPanel.superclass.constructor.call(this, {
		title : 'Системные роли',
		border : false,
		autoScroll : true,
		iconCls : 'nav',
		items : this.roleTree,
		tools: [{
            id:'refresh',
            on:{
                click: function(){
                	this.roleTree.getRootNode().reload()
                	Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.clear();
                	Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.toggle(false);
                	Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overModulesButton.toggle(false);
                },
                scope: this
            }
        }]
	});
}
Ext.extend(OverRolesPanel, Ext.Panel, {
	initComponent : function() {

		var self = this;

		this.on('expand', function() {
			Systemadmin.objectManagementPanel.objectTabPanel.setActiveTab(Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel);
		});

		this.roleTree.on('load', function() {
		var node = self.roleTree.getRootNode().firstChild;
			self.roleTree.expandPath(node.getPath());
			self.roleTree.selectPath(node.getPath());
			Systemadmin.setPermissionChangeAction(false);
		});

		this.roleTree.on('click', function(node) {
			self.roleTree.expandPath(node.getPath());
			self.roleTree.selectPath(node.getPath());
			if (Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.overPrivilegesButton.pressed == true)
			Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.fireEvent('overPrivileges');
			else Systemadmin.objectManagementPanel.objectTabPanel.permissionsPanel.fireEvent('overModules');
		});

		OverRolesPanel.superclass.initComponent.apply(this, arguments);
	}
});

/* ObjectsTreePanel */
ObjectsTreePanel = function() {
	this.overModulesPanel = new OverModulesPanel();
	this.overProjectsPanel = new OverProjectsPanel();
	this.overRolesPanel = new OverRolesPanel();

	ObjectsTreePanel.superclass.constructor.call(this, {
		region : 'west',
		title : 'Системные объекты',
		split : true,
		width : 300,
		minSize : 175,
		maxSize : 400,
		collapsible : true,
		collapseMode: 'mini',
		margins : '0 0 0 0',
		layout : 'accordion',
		layoutConfig : {
			animate : true
		},
		items : [this.overProjectsPanel, this.overModulesPanel, this.overRolesPanel]
	});
}

Ext.extend(ObjectsTreePanel, Ext.Panel, {});

/* ObjectTabPanel */
ObjectTabPanel = function() {
	this.moduleControlPanel = new ModuleControlPanel();
	this.projectControlPanel = new ProjectControlPanel();
	this.permissionsPanel = new PermissionsPanel();
	this.privilegeControlPanel = new PrivilegeControlPanel();

	ObjectTabPanel.superclass.constructor.call(this, {
		deferredRender : false,
		activeTab : 0,
		region : 'center',
		border : true,
		collapsible : false,
		split : true,
		autoScroll : false,
		items : [this.projectControlPanel, this.moduleControlPanel, this.privilegeControlPanel, this.permissionsPanel]
	});
}

Ext.extend(ObjectTabPanel, Ext.TabPanel, {
	initComponent : function() {

		this.on('tabchange', function() {
			if (this.getActiveTab() == this.moduleControlPanel)
				Systemadmin.objectManagementPanel.objectsTreePanel.overModulesPanel.expand();

			if (this.getActiveTab() == this.projectControlPanel)
				Systemadmin.objectManagementPanel.objectsTreePanel.overProjectsPanel.expand();

			if (this.getActiveTab() == this.permissionsPanel)
				Systemadmin.objectManagementPanel.objectsTreePanel.overRolesPanel.expand();

			if (this.getActiveTab() == this.privilegeControlPanel)
				Systemadmin.objectManagementPanel.objectsTreePanel.collapse();
			else Systemadmin.objectManagementPanel.objectsTreePanel.expand();
		});

		ObjectTabPanel.superclass.initComponent.apply(this, arguments);
	}
});

/* ObjectManagementPanel */
ObjectManagementPanel = function() {
	this.objectTabPanel = new ObjectTabPanel();
	this.objectsTreePanel = new ObjectsTreePanel();

	ObjectManagementPanel.superclass.constructor.call(this, {
		title : 'Управление объектами',
		closable : false,
		autoScroll : false,
		layout : 'border',
		items : [ this.objectsTreePanel, this.objectTabPanel/*, this.projectControlPanel*/]
	});
}

Ext.extend(ObjectManagementPanel, Ext.Panel, {});