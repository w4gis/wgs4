﻿/* Форма основных характеристик роли */
SystemUserForm = function()
{
	var self = this;
	Ext.namespace('Ext.systemUserLock');
    Ext.systemUserLock.states = [
        ['1','Заблокирован'],
        ['0','Разблокирован']
        ];

    this.systemUserLockStates = new Ext.data.SimpleStore({
        fields: ['id', 'state'],
        data: Ext.systemUserLock.states
    });

    this.systemUserLockStates.loadData(Ext.systemUserLock.states);

    this.userLockStatesCombobox = new Ext.form.ComboBox({
        name:'userLocked',
        store: this.systemUserLockStates,
        displayField:'state',
        valueField: 'id',
        fieldLabel: 'Состояние',
        typeAhead: true,
        mode: 'local',
        anchor:'100%',
        triggerAction: 'all',
        editable:false,
        selectOnFocus:true
    });

    this.oldPasswordField = new Ext.form.Field(
            {
	        xtype:'field',
	        fieldLabel: 'Старый пароль',
	        name: 'oldPassword',
	        anchor:'100%',
	        inputType: 'password',
	        listeners:
	        {
	        	change: function()
	        	{
	        		self.passwordField.enable();
	        		self.confirmPasswordField.enable();
	        		self.passwordField.focus();
	        	}
	        }
	        });

    this.passwordField = new Ext.form.Field(
            {
	        xtype:'field',
	        fieldLabel: 'Пароль',
	        name: 'password',
	        anchor:'100%',
	        inputType: 'password',
	        msgTarget: 'under',
            allowBlank:false,
            blankText: 'Данное поле не может быть пустым'
            });


    this.confirmPasswordField = new Ext.form.Field(
			{
	        xtype:'field',
	        fieldLabel: 'Подтверждение',
	        name: 'confirmPassword',
	        anchor:'100%',
	        inputType: 'password'
	        });


	SystemUserForm.superclass.constructor.call(this,{
		id: 'sysUserForm',
        method: 'POST',
        title: 'Общие свойства',
        region: 'north',
        height:185,
        collapsible: false,
        frame:true,
        deferredRender: false,
        buttonAlign: 'right',
        bodyStyle:'padding:3px 3px 0',
        defaults: {width: 230},
        items: [
                {
                xtype:'hidden',
                name: 'userId',
                allowBlank:true,
                anchor:'100%'
                },
                {
                xtype:'textfield',
                fieldLabel: 'Название',
                name: 'userName',
                msgTarget: 'under',
                msgFx: 'normal',
                invalidText : 'Недопустимое значение',
                allowBlank:false,
                blankText: 'Данное поле не может быть пустым',
                regex: /^([\wА-Яа-я_][ ]?)+$/,
                maskRe: /[A-Za-zА-Яа-я_ ]+/,
                maxLengthText: "Число символов не должно превышать 255",
		        anchor:'100%'
                },
                {
                xtype:'textarea',
                fieldLabel: 'Описание',
                name: 'userDescription',
                anchor:'100%',
                autoHeight: true
                },
                {
                xtype:'hidden',
                name: 'userLocked',
                anchor:'100%'
                },
                {
                xtype:'hidden',
                name: 'userPassword',
                anchor:'100%'
                },this.userLockStatesCombobox
        ]
	});
}
Ext.extend(SystemUserForm, Ext.FormPanel, {});

/* Назначение пользователей и дочерних ролей */
SystemRoleTabPanel = function()
{

/*  Дерево доступных ролей */
	this.availableRolesTree = new Ext.tree.TreePanel({
		id: 'availableSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
              text: 'Доступные',
              draggable: false,
              id: 'root',
              expanded: true
              }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Дерево дочерних ролей */
	this.assignRolesTree = new Ext.tree.TreePanel({
        id: 'childrenSysRoleTree',
        root: new Ext.tree.AsyncTreeNode({
            text: 'Назначенные',
            draggable: false,
            id: 'root',
            expanded: true
        }),
        animate: true,
        border: false,
        singleExpand: false,
        autoScroll: true,
        enableDD: true,
        containerScroll: true,
        dropConfig: {appendOnly: true}
    });

/*  Панель доступных ролей */
    this.availableRolesPanel = new Ext.Panel({
    	region: 'west',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
        anchor: '100%',
        items : [ this.availableRolesTree ]
    });

/*  Панель назначенных ролей */
    this.assignRolesPanel = new Ext.Panel({
        region: 'east',
        split: true,
        width: 206,
        height:186,
        collapsible: false,
        anchor: '100%',
        items : [ this.assignRolesTree ]
    });

/*  Роли */
	this.rolesTab = new Ext.Panel({
		title : 'Назначение ролей',
        layout : 'table',
        height:186,
        items : [ this.availableRolesPanel, this.assignRolesPanel]
    });

/*  Конструктор */
	SystemRoleTabPanel.superclass.constructor.call(this,{
		region: 'center',
        deferredRender: false,
        activeTab: 0,
        defaults:{autoScroll:true},
        items : [ this.rolesTab ]
	});
}
Ext.extend(SystemRoleTabPanel, Ext.TabPanel, {});

/* Диалог добавления/изменения системной роли */
SystemUserWindow = function()
{
	this.systemUserForm = new SystemUserForm();
	this.systemRoleTabPanel = new SystemRoleTabPanel();

	this.setMarkInvalidOldPassword = function (element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Необходимо ввести верный пароль');
    	element.focus(true);
	}
	this.setMarkInvalid = function(element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Данное поле не может быть пустым');
    	element.focus();
	}

	this.warningMessage = function (msg, focusedElement)
	{
		Ext.Msg.show({
	    title : 'Предупреждение',
     	msg : msg,
    	buttons : Ext.Msg.OK,
    	fn: function(btn)
    		{
    			if(btn == 'ok')
    	  		{
    			   focusedElement.focus();
    	  		}
    		},
    	icon : Ext.MessageBox.WARNING });
	}

	SystemUserWindow.superclass.constructor.call(this,{
		width : 430,
		height : 470,
		closeAction : 'close',
		openAction : 'hide',
		plain : true,
		autoScroll : true,
		resizable : false,
		modal: true,
		items : [this.systemUserForm, this.systemRoleTabPanel]
    });
    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'ОК',
        handler : function()
        {
            this.fireEvent('ok')
        },
        scope : this,
        disabled: true
    });

    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function()
        {
            this.fireEvent('cancel')
        },
        scope : this,
        disabled: true
    });
}
Ext.extend(SystemUserWindow, Ext.Window, {
	initComponent : function() {

		  var self = this;
		  this.addEvents({ ok: true, cancel: true});
		  /* ok */
		  this.on('ok', function()
		  {
		  	var assignRoleNodes = [];
            var assignRoleIds = [];
            assignRoleNodes = this.systemRoleTabPanel.assignRolesTree.getRootNode().childNodes;
            for(var i = 0; i < assignRoleNodes.length; i++)
                assignRoleIds[i] = assignRoleNodes[i].id;

            var assignNotRoleNodes = [];
            var assignNotRoleIds = [];
            assignNotRoleNodes = this.systemRoleTabPanel.availableRolesTree.getRootNode().childNodes;
            for(var i = 0; i < assignNotRoleNodes.length; i++)
                assignNotRoleIds[i] = assignNotRoleNodes[i].id;

            var  systemUserFormValues = this.systemUserForm.getForm().getValues();

            if(systemUserFormValues.userName != '')
            {
	            if(systemUserFormValues.userId == '')
	            {
	             	if(systemUserFormValues.password != '')
	             	{
			             if (systemUserFormValues.password == systemUserFormValues.confirmPassword)
			             {
			             	Systemadmin.conn.request({
			                    url: '/systemadmin/user/createuser',
			                    method: 'POST',
			                    //form: sysUserForm.id,
			                    params: {
							            'id':			systemUserFormValues.userId,
							            'name':			systemUserFormValues.userName,
							            'description':	systemUserFormValues.userDescription,
							            'password':		systemUserFormValues.password,
							            'locked': 		this.systemUserForm.userLockStatesCombobox.getValue(),
							            'assignRoleIds[]': 		assignRoleIds,
							            'assignNotRoleIds[]': 	assignNotRoleIds
							            },
			                    success: function() {
			                    Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
			                    //Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
			                	self.close();
			                	},
			                	failure: function()
		                        {
		                        	 self.systemUserForm.getForm().findField('userName').focus(true);
		                        }
			            	});
			             }
			             else
			             {

			             	this.warningMessage('Пароли не совпадают! Повторите ввод пароля.',this.systemUserForm.getForm().findField('password'));
			             	this.systemUserForm.getForm().setValues(
			             	{
			             		password: '',
			             		confirmPassword: ''
			             	});
			             }
	             	}
	             	else
	             	{
	             	    this.setMarkInvalid(this.systemUserForm.getForm().findField('password'));
	             	}

            	}

	            else
	            {
			                if(systemUserFormValues.password != '')
			                {
				                if(systemUserFormValues.password == systemUserFormValues.confirmPassword)
			                	{
				                Systemadmin.conn.request({
				                    url: '/systemadmin/user/edituser',
				                    method: 'POST',
				                    params: {
								            'id':          	systemUserFormValues.userId,
								            'name':        	systemUserFormValues.userName,
								            'description': 	systemUserFormValues.userDescription,
								            'password':		systemUserFormValues.password,
								            'locked': 		this.systemUserForm.userLockStatesCombobox.getValue(),
								            'assignRoleIds[]': 	    assignRoleIds,
								            'assignNotRoleIds[]': 	assignNotRoleIds
								            },
				                    success: function() {
				                    Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
				                    //Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
				                	self.close();
				                	},
				                	failure: function()
			                        {
			                        	 self.systemUserForm.getForm().findField('userName').focus(true);
			                        }
					            });
				            	//this.close();
			                	}
			                	else
			                	{
			                		this.warningMessage('Пароли не совпадают! Повторите ввод пароля.',this.systemUserForm.getForm().findField('password'));
					             	this.systemUserForm.getForm().setValues(
					             	{
					             		password: '',
					             		confirmPassword: ''
					             	});
			                	}
			                }
			                else
			                {
			                	Systemadmin.conn.request({
				                    url: '/systemadmin/user/edituser',
				                    method: 'POST',
				                    params: {
								            'id':          	systemUserFormValues.userId,
								            'name':        	systemUserFormValues.userName,
								            'description': 	systemUserFormValues.userDescription,
								            'password':		systemUserFormValues.userPassword,
								            'locked': 		this.systemUserForm.userLockStatesCombobox.getValue(),
								            'assignRoleIds[]': 	    assignRoleIds,
								            'assignNotRoleIds[]': 	assignNotRoleIds
								            },
				                    success: function() {
				                    Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
				                	self.close();
				                	},
				                	failure: function()
			                        {
			                        	 self.systemUserForm.getForm().findField('userName').focus(true);
			                        }
					            });
					          }
	            }
            }
            else
            {
            	this.setMarkInvalid(this.systemUserForm.getForm().findField('userName'));
            }
		  });

		  /* cancel */
		  this.on('cancel', function()
		  {
		  	this.close();
		  });
		SystemUserWindow.superclass.initComponent.apply(this, arguments);
	}
});



/* Таблица пользователей */
SystemUsersGrid = function()
{
	/* buttons */
	this.createButton = new Ext.Button({
		text : 'Создать',
        iconCls: 'icon-add',
		handler : function()
		{
			this.fireEvent('create')
		},
		scope : this
	});

	this.editButton = new Ext.Button({
		text : 'Свойства',
		iconCls: 'icon-properties',
        handler : function()
		{
			this.fireEvent('edit')
		},
		scope : this
	});

	this.removeButton = new Ext.Button({
		text : 'Удалить',
        iconCls: 'icon-delete',
		handler : function()
		{
			this.fireEvent('remove')
		},
		scope : this
	});
	/* constructor */

	this.userSelectionModel = new Ext.grid.CheckboxSelectionModel();

	this.userStore = new Ext.data.JsonStore({
		id: 'sysUserStore',
        url: '/systemadmin/user/getuserlist',
        baseParams: {id: 0},
        autoLoad: true,
        fields: ['userId', 'userName', 'description','assignedRoles']

	});

	SystemUsersGrid.superclass.constructor.call(this, {

		title : 'Управление системными пользователями',
		store : this.userStore,
		cm : new Ext.grid.ColumnModel([//this.userSelectionModel,
			{id: 'userId',header: "Идентификатор пользователя", hidden: true, hideable: false},
            {id: 'userName',header: "Имя пользователя", sortable: true, dataIndex: 'userName', width:240},
            {header: "Описание", sortable: true, dataIndex: 'description',width:500},
            {header: "Назначенные роли", dataIndex: 'assignedRoles',width:500}

		]),
		sm : this.userSelectionModel,
		viewConfig:
	       {
	           forceFit: true
	       },
		autoScroll : true,

		tbar : [ this.createButton, '-', this.editButton, '-', this.removeButton ],
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : this.userStore,
			displayInfo : true
		})

	});
}


Ext.extend(SystemUsersGrid, Ext.grid.EditorGridPanel, {
	initComponent : function() {

	  this.addEvents({ create: true, edit: true, remove: true });

	  /* create */
	  this.on('create', function()
	  {

	  	var systemUserWindow = new SystemUserWindow();

	  	systemUserWindow.systemRoleTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/systemadmin/user/getallroles',
	  		baseParams:{id:0}
	  		});

	  	systemUserWindow.systemRoleTabPanel.assignRolesTree.loader = new Ext.tree.WgsTreeLoader(
	  		{
	  		dataUrl:'/systemadmin/user/getemptytree',
	  		baseParams:{id:0},
	  		listeners:
		  		{
		  			load: function()
		  			{
		  				systemUserWindow.cancelButton.enable();
		  				systemUserWindow.okButton.enable();
		  			}
		  		}
	  		});

	  	systemUserWindow.systemUserForm.userLockStatesCombobox.setValue(0);
	  	systemUserWindow.systemUserForm.add(systemUserWindow.systemUserForm.passwordField);
	  	systemUserWindow.systemUserForm.add(systemUserWindow.systemUserForm.confirmPasswordField);

	  	/**/

	  	systemUserWindow.setTitle('Создание нового пользователя');
	  	systemUserWindow.show();


	  });

	  /* edit */
	  this.on('edit', function()
	  {
	  	var systemUserWindow = new SystemUserWindow();

	  	var reader = new Ext.data.JsonReader({},[
                       {name: 'userId'},
                       {name: 'userName'},
                       {name: 'userDescription'},
                       {name: 'userLocked'},
                       {name: 'userPassword'}

                    ]);

        var userForChange = Systemadmin.systemUserManagementPanel.systemUsersGrid.userSelectionModel.getSelections();


                   if(userForChange.length != 0)
                   {
                       if(userForChange.length == 1)
                       {
                       var msgWait = Ext.Msg.wait('Получение данных о пользователе...', 'Подождите', {interval:32});
	                       Ext.Ajax.request(
	                        {
	                            url: '/systemadmin/user/getuserproperties',
	                            method: 'POST',
	                            params: {'name':userForChange[0].data.userName},
	                            success: function(response) {
	                              systemUserWindow.systemUserForm.userLockStatesCombobox.setValue(reader.read(response).records[0].data.userLocked);
	                              systemUserWindow.systemUserForm.getForm().loadRecord(reader.read(response).records[0]);
	                              systemUserWindow.setTitle('Изменение свойств пользователя : '+userForChange[0].data.userName);
	                              //systemUserWindow.systemUserForm.getForm().findField('password').disable();
	                              //systemUserWindow.systemUserForm.getForm().findField('confirmPassword').disable();
	                              //alert(systemUserWindow.systemUserForm.getForm().getValues().userPassword);
	                              systemUserWindow.show();
	                              msgWait.hide();
	                            }
	                        });

	                      	systemUserWindow.systemRoleTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader(
						  		{
						  		dataUrl:'/systemadmin/user/getavialableroles',
						  		baseParams:{name:userForChange[0].data.userName}
						  		});

						  	systemUserWindow.systemRoleTabPanel.assignRolesTree.loader = new Ext.tree.WgsTreeLoader(
						  		{
						  		dataUrl:'/systemadmin/user/getparentroles',
						  		baseParams:{name:userForChange[0].data.userName},
						  		listeners:
						  		{
						  			load: function()
						  			{
						  				systemUserWindow.cancelButton.enable();
						  				systemUserWindow.okButton.enable();
						  			}
						  		}
						  		});
	                        systemUserWindow.systemUserForm.passwordField.fieldLabel = 'Новый пароль';
						  	//systemUserWindow.systemUserForm.add(systemUserWindow.systemUserForm.oldPasswordField);
						  	systemUserWindow.systemUserForm.add(systemUserWindow.systemUserForm.passwordField);
						  	systemUserWindow.systemUserForm.add(systemUserWindow.systemUserForm.confirmPasswordField);
						  	systemUserWindow.setTitle('Изменение пользователя: ');
						  	systemUserWindow.show();
                       }
                       else
                       Ext.Msg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать одного пользователя для изменения.',
							buttons : Ext.Msg.OK,
							icon : Ext.MessageBox.WARNING
						});
                   }
                   else
                   Ext.Msg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать одного пользователя для изменения.',
							buttons : Ext.Msg.OK,
							icon : Ext.MessageBox.WARNING
						});





	  });

	  /* remove */
	  this.on('remove', function()
	  {
		  	var usersForRemoveIds = [];
		    var usersForRemove = Systemadmin.systemUserManagementPanel.systemUsersGrid.userSelectionModel.getSelections();
		    var message = '';
		    if(usersForRemove.length != 0)
		    {
		    	if(usersForRemove.length == 1)
				message = 'Вы действительно желаете удалить выбранного пользователя?';
				else
				message = 'Вы действительно желаете удалить выбранных пользователей?';

		        Ext.MessageBox.confirm('', message,function(btn)
		        {
		          if (btn == 'yes')
		          {
		              for(var i = 0; i < usersForRemove.length; i++)
		              usersForRemoveIds[i] = usersForRemove[i].data.userId;

		            Systemadmin.conn.request({
		            url: '/systemadmin/user/removeuser',
		            method: 'POST',

		            params: {'usersForRemoveIds[]': usersForRemoveIds},
		            success: function() {
			            Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();


		                }
		            });
		          }

		        });

		    }
		    else
		    Ext.Msg.show({
				title : 'Предупреждение',
				msg : 'Необходимо выбрать пользователя для удаления.',
				buttons : Ext.Msg.OK,
				icon : Ext.MessageBox.WARNING
			});

	  });

	SystemUsersGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* Администратор системных пользователей */
SystemUserManagementPanel = function() {
	this.systemUsersGrid = new SystemUsersGrid();

	SystemUserManagementPanel.superclass.constructor.call(this, {
		title : 'Пользователи',
		closable : false,
		autoScroll : true,
		layout : 'fit',
		items : [this.systemUsersGrid]
	});
}

Ext.extend(SystemUserManagementPanel, Ext.Panel, {});