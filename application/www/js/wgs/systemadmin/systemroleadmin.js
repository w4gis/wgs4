﻿SystemRolesRelationsGrid = function(roleId, parentWindow) {
	// alert(roleId);
	/* public */
	var self = this;
	// this.wdsRoleId = roleId;
	this.changed = false;
	var temp = roleId.split('_');
	this.sysProjectId = temp[0];
	this.wgsRoleId = temp[1];


	var msgWait = Ext.Msg.wait('Генерация списка прав...', 'Подождите', {interval : 50});
	/* constructor */

	this.roleSelectionModel = new Ext.grid.CheckboxSelectionModel();
	this.roleCheckColumn = new Ext.grid.CheckColumn({
		header : 'Назначение',
		dataIndex : 'roleChecked',
		align : 'center'
	});

	this.roleStore = new Ext.data.WgsJsonStore({
		url : '/systemadmin/rolerelation/getsystemroles',
		baseParams : {
			id : roleId
		},
		autoLoad : true,
		fields : ['roleId', 'roleName', 'description', 'roleChecked'],
		listeners : {
			update : function() {
				self.changed = true;
				parentWindow.applyButton.enable();
			}
		}
	});

	SystemRolesRelationsGrid.superclass.constructor.call(this, {
		store : this.roleStore,
		cm : new Ext.grid.ColumnModel([{
			id : 'roleId',
			header : "Идентификатор роли",
			hidden : true,
			hideable : false
		}, {
			id : 'roleName',
			header : "Наименование",
			sortable : true,
			dataIndex : 'roleName',
			width : 220
		}, this.roleCheckColumn]),
		sm : this.roleCheckColumn,
		viewConfig : {
			forceFit : true
		},
		autoScroll : true
	/*
	 * , tbar : [ this.saveButton, '-', this.cancelButton]//,
	 */
	});
	msgWait.hide();
}

Ext.extend(SystemRolesRelationsGrid, Ext.grid.EditorGridPanel, {
	initComponent : function() {

		SystemRolesRelationsGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* Соотноношение ролей */
RoleRelationWindow = function() {

	var self = this;
	this.projectRolesTree = new Ext.tree.TreePanel({

		id : 'availableSysUserTree',
		root : new Ext.tree.AsyncTreeNode({
			text : 'Проекты',
			draggable : false, // disable root node dragging
			id : 'root',
			expanded : true
		}),
		animate : true,
		border : false,
		singleExpand : false,
		autoScroll : true,
		loader : new Ext.tree.WgsTreeLoader({
			dataUrl : '/systemadmin/rolerelation/getprojectswithroles',
			baseParams : {
				id : 0
			}
		}),
		enableDD : false,
		containerScroll : true,
		dropConfig : {
			appendOnly : true
		}
	});

	this.projectRolesPanel = new Ext.Panel({
		title : 'Роли проекта',
		region : 'west',
		split : false,
		width : 200,
		collapsible : false,
		items : [this.projectRolesTree],
		margins : '2 0 2 2',
		cmargins : '2 2 2 2'
	});

	this.systemRolesPanel = new Ext.Panel({
		title : 'Системные роли',
		region : 'center',
		split : false,
		width : 200,
		collapsible : false,
		margins : '2 2 2 2',
		cmargins : '2 2 2 2',
		layout : 'fit'
	});

	this.clearSystemRolesPanel = function() {
		if (this.systemRolesRelationsGrid) {
			this.systemRolesPanel.remove(this.systemRolesRelationsGrid);

			this.systemRolesRelationsGrid = null;
		}
	}

	this.addSystemRolesRelationsGrid = function(grid) {
		this.systemRolesRelationsGrid = grid;
		this.systemRolesPanel.add(this.systemRolesRelationsGrid);
		this.systemRolesPanel.doLayout();
		// alert(grid);
	}

	this.assignSystemRoles = function() {
		var assignedRoles = [];
		var unAssignedRoles = [];
		var wgsRoleId = this.systemRolesRelationsGrid.wgsRoleId;
		var sysProjectId = this.systemRolesRelationsGrid.sysProjectId;
		var roleStore = this.systemRolesRelationsGrid.roleStore;
		this.systemRolesRelationsGrid.roleStore.each(function(record) {
			if (record.data.roleChecked)
				assignedRoles[assignedRoles.length] = record.data.roleId;
			else
				unAssignedRoles[unAssignedRoles.length] = record.data.roleId;
		});

		Systemadmin.conn.request({
			url : '/systemadmin/rolerelation/assignroles',
			method : 'POST',
			params : {
				'wgsRoleId' : wgsRoleId,
				'sysProjectId' : sysProjectId,
				'unAssignedRoles[]' : unAssignedRoles,
				'assignedRoles[]' : assignedRoles
			},
			success : function() {
				roleStore.reload();
				self.systemRolesRelationsGrid.changed = false;
				self.applyButton.disable();

			}
		});
	}

	RoleRelationWindow.superclass.constructor.call(this, {
		title : 'Layout Window',
		closable : true,
		width : 650,
		height : 400,
		resizable : false,
		plain : true,
		layout : 'border',
		items : [this.projectRolesPanel, this.systemRolesPanel/* this.tabs */]//,
		//buttons : [this.okButton, this.cancelButton, this.applyButton]
	});
    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'ОК',
        handler : function() {
            this.fireEvent('ok')
        },
        scope : this
    });

    this.cancelButton = this.addButton({
    	iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function() {
            this.fireEvent('cancel')
        },
        scope : this
    });

    this.applyButton = this.addButton({
        text : 'Применить',
        disabled : true,
        handler : function() {
            this.fireEvent('apply')
        },
        scope : this
    });

}

Ext.extend(RoleRelationWindow, Ext.Window, {
	initComponent : function() {

		var self = this;

		this.addEvents({
			ok : true,
			cancel : true,
			apply : true
		});

		this.projectRolesTree.on('click', function(node) {
			if (node.leaf) {

				var fnUpdateGrid = function(result) {


				if (result == 'yes')
				{
					self.assignSystemRoles();
					self.clearSystemRolesPanel();
				    self.addSystemRolesRelationsGrid(new SystemRolesRelationsGrid(node.id, self));
				    self.applyButton.disable();
				}
				if (result == 'no')
				{
					self.clearSystemRolesPanel();
				    self.addSystemRolesRelationsGrid(new SystemRolesRelationsGrid(node.id, self));
				    self.applyButton.disable();
				}


				};

				if (self.systemRolesRelationsGrid)
				{

					if (self.systemRolesRelationsGrid.changed) {
						Ext.Msg.show({
							title : 'Сохранить изменения?',
							msg : 'Данные были изменены. Сохранить изменения?',
							buttons : Ext.Msg.YESNOCANCEL,
							fn : fnUpdateGrid,
							icon : Ext.MessageBox.QUESTION
						});
					}
					else
					{
						self.clearSystemRolesPanel();
						self.addSystemRolesRelationsGrid(new SystemRolesRelationsGrid(
							node.id, self));
					}
				}
				else
				{
					self.clearSystemRolesPanel();
					self.addSystemRolesRelationsGrid(new SystemRolesRelationsGrid(node.id, self));
				}
            }
		}
		);
		/* ok */
		this.on('ok', function() {
			if (this.systemRolesRelationsGrid) {
				this.assignSystemRoles();
			}
			this.close();

		});

		/* cancel */
		this.on('cancel', function() {
			// this.close();
			var fnUpdateRoleRelationGrid = function(result) {

				if (result == 'no')
					self.close();
				if (result == 'yes') {
					self.assignSystemRoles();
					self.close();
				}

			};

			if (this.systemRolesRelationsGrid) {
				if (this.systemRolesRelationsGrid.changed) {
					Ext.Msg.show({
						title : 'Сохранить изменения?',
						msg : 'Данные были изменены. Сохранить изменения?',
						buttons : Ext.Msg.YESNOCANCEL,
						fn : fnUpdateRoleRelationGrid,
						icon : Ext.MessageBox.QUESTION
					});
				} else {
					this.close();
				}
			} else {
				this.close();
			}

		});

		this.on('apply', function() {
			this.assignSystemRoles();
		});

		RoleRelationWindow.superclass.initComponent.apply(this, arguments);
	}
});

/* Форма основных характеристик роли */
SystemRoleForm = function() {
	SystemRoleForm.superclass.constructor.call(this, {
		id : 'sysRoleForm',
		method : 'POST',
		title : 'Общие свойства',
		region : 'north',
		// width: 200,
		height : 147,
		collapsible : false,
		frame : true,

		bodyStyle : 'padding:5px 5px 0',
		defaults : {
			width : 230
		},
		items : [{
			xtype : 'hidden',
			name : 'roleId',
			allowBlank : true,
			anchor : '100%'
		}, {
			xtype : 'textfield',
			fieldLabel : 'Название',
			name : 'roleName',
			msgTarget: 'under',
            msgFx: 'normal',
            invalidText : 'Недопустимое значение',
            allowBlank:false,
            blankText: 'Данное поле не может быть пустым',
			regex : /^([\wА-Яа-я_][ ]?)+$/,
			maskRe : /[A-Za-zА-Яа-я_ ]+/,
			maxLength: 255,
		    maxLengthText: "Число символов не должно превышать 255",
			anchor : '100%'
		}, {
			xtype : 'textarea',
			fieldLabel : 'Описание',
			name : 'roleDescription',
			anchor : '100%'
		}]
	});
}
Ext.extend(SystemRoleForm, Ext.FormPanel, {});

/* Назначение пользователей и дочерних ролей */
SystemRoleUserTabPanel = function() {

	/* Дерево доступных пользователей */
	this.availableUsersTree = new Ext.tree.TreePanel({
		id : 'availableSysUserTree',
		root : new Ext.tree.AsyncTreeNode({
			text : 'Доступные',
			draggable : false, // disable root node dragging
			id : 'root',
			expanded : true
		}),
		animate : true,
		border : false,
		singleExpand : false,
		autoScroll : true,
		enableDD : true,
		containerScroll : true,
		dropConfig : {
			appendOnly : true
		}
	});

	/* Дерево назначенных пользователей */
	this.membersUsersTree = new Ext.tree.TreePanel({
		id : 'memberSysUserTree',
		root : new Ext.tree.AsyncTreeNode({
			text : 'Назначенные',
			draggable : false,
			id : 'root',
			expanded : true
		}),
		animate : true,
		border : false,
		singleExpand : false,
		autoScroll : true,
		enableDD : true,
		containerScroll : true,
		dropConfig : {
			appendOnly : true
		}
	});

	/* Панель доступных пользователей */
	this.avalableUsersPanel = new Ext.Panel({
		region : 'west',
		split : true,
		width : 206,
		height : 186,
		collapsible : false,
		anchor : '100%',
		items : [this.availableUsersTree]
	});

	/* Панель назначенных пользователей */
	this.membersUsersPanel = new Ext.Panel({
		region : 'east',
		split : true,
		width : 206,
		height : 186,
		collapsible : false,
		anchor : '100%',
		items : [this.membersUsersTree]
	});

	/* Пользователи */
	this.usersTab = new Ext.Panel({
		title : 'Пользователи',
		layout : 'table',
		height : 186,
		items : [this.avalableUsersPanel, this.membersUsersPanel]
	});

	/* Дерево доступных ролей */
	this.availableRolesTree = new Ext.tree.TreePanel({
		id : 'availableSysRoleTree',
		root : new Ext.tree.AsyncTreeNode({
			text : 'Доступные',
			draggable : false,
			id : 'root',
			expanded : true
		}),
		animate : true,
		border : false,
		singleExpand : false,
		autoScroll : true,
		enableDD : true,
		containerScroll : true,
		dropConfig : {
			appendOnly : true
		}
	});

	/* Дерево дочерних ролей */
	this.childrenRolesTree = new Ext.tree.TreePanel({
		id : 'childrenSysRoleTree',
		root : new Ext.tree.AsyncTreeNode({
			text : 'Назначенные',
			draggable : false,
			id : 'root',
			expanded : true
		}),
		animate : true,
		border : false,
		singleExpand : false,
		autoScroll : true,
		enableDD : true,
		containerScroll : true,
		dropConfig : {
			appendOnly : true
		}
	});

	/* Панель доступных ролей */
	this.availableRolesPanel = new Ext.Panel({
		region : 'west',
		split : true,
		width : 206,
		height : 186,
		collapsible : false,
		anchor : '100%',
		items : [this.availableRolesTree]
	});

	/* Панель доступных ролей */
	this.childrenRolesPanel = new Ext.Panel({
		region : 'east',
		split : true,
		width : 206,
		height : 186,
		collapsible : false,
		anchor : '100%',
		items : [this.childrenRolesTree]
	});

	/* Роли */
	this.rolesTab = new Ext.Panel({
		title : 'Дочерние роли',
		layout : 'table',
		items : [this.availableRolesPanel, this.childrenRolesPanel]
	});

	/* Конструктор */
	SystemRoleUserTabPanel.superclass.constructor.call(this, {
		region : 'center',
		deferredRender : false,
		activeTab : 0,
		defaults : {
			autoScroll : true
		},
		items : [this.usersTab, this.rolesTab]
	});
}
Ext.extend(SystemRoleUserTabPanel, Ext.TabPanel, {});

/* Диалог добавления/изменения системной роли */
SystemRoleWindow = function() {
	this.systemRoleForm = new SystemRoleForm();
	this.systemRoleUserTabPanel = new SystemRoleUserTabPanel();

	this.setMarkInvalid = function(element)
	{
		element.msgTarget = 'under';
		element.markInvalid('Данное поле не может быть пустым');
    	element.focus();
	}

	SystemRoleWindow.superclass.constructor.call(this, {
		width : 430,
		height : 432,
		closeAction : 'close',
		openAction : 'hide',
		plain : true,
		autoScroll : true,
		resizable : false,
		animCollapse: true,
		modal: true,
		animateTarget : Systemadmin.systemRoleManagementPanel.systemRolesGrid.createButton,
		items : [this.systemRoleForm, this.systemRoleUserTabPanel]
	});
    this.okButton = this.addButton({
        iconCls: 'icon-accept-tick',
        text : 'ОК',
        handler : function() {
            this.fireEvent('ok')
        },
        scope : this,
        disabled: true
    });

    this.cancelButton = this.addButton({
        iconCls: 'icon-close-cross',
        text : 'Отмена',
        handler : function() {
            this.fireEvent('cancel')
        },
        scope : this,
        disabled: true
    });

}
Ext.extend(SystemRoleWindow, Ext.Window, {
	initComponent : function() {

		var self = this;
		this.addEvents({
			ok : true,
			cancel : true
		});

		/* ok */
		this.on('ok', function() {
			var memberUserNodes = [];
			var memberUserIds = [];
			memberUserNodes = this.systemRoleUserTabPanel.membersUsersTree.getRootNode().childNodes;
			for (var i = 0; i < memberUserNodes.length; i++)
				memberUserIds[i] = memberUserNodes[i].id;

			var childrenRoleNodes = [];
			var childrenRoleIds = [];
			childrenRoleNodes = this.systemRoleUserTabPanel.childrenRolesTree.getRootNode().childNodes;
			for (var i = 0; i < childrenRoleNodes.length; i++)
				childrenRoleIds[i] = childrenRoleNodes[i].id;

			var memberNotUserNodes = [];
			var memberNotUserIds = [];
			memberNotUserNodes = this.systemRoleUserTabPanel.availableUsersTree.getRootNode().childNodes;
			for (var i = 0; i < memberNotUserNodes.length; i++)
				memberNotUserIds[i] = memberNotUserNodes[i].id;

			var childrenNotRoleNodes = [];
			var childrenNotRoleIds = [];
			childrenNotRoleNodes = this.systemRoleUserTabPanel.availableRolesTree.getRootNode().childNodes;
			for (var i = 0; i < childrenNotRoleNodes.length; i++)
				childrenNotRoleIds[i] = childrenNotRoleNodes[i].id;

			var systemRoleForm = this.systemRoleForm.getForm();


			if(systemRoleForm.getValues().roleName != '')
		    {
				if (this.systemRoleForm.getForm().getValues().roleId == '')
				{
				   Systemadmin.conn.request({
						url : '/systemadmin/role/createrole',
						method : 'POST',
						params : {
							'id' : systemRoleForm.getValues().roleId,
							'name' : systemRoleForm.getValues().roleName,
							'description' : systemRoleForm.getValues().roleDescription,
							'memberUserIds[]' : memberUserIds,
							'childrenRoleIds[]' : childrenRoleIds,
							'memberNotUserIds[]' : memberNotUserIds,
							'childrenNotRoleIds[]' : childrenNotRoleIds
						},
						success : function() {
							Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
							self.close();
						},
	                	failure: function()
                        {
                        	 self.systemRoleForm.getForm().findField('roleName').focus(true);
                        }
					});

				}
				else
				{
					Systemadmin.conn.request({
						url : '/systemadmin/role/editrole',
						method : 'POST',
						params : {
							'id' : systemRoleForm.getValues().roleId,
							'name' : systemRoleForm.getValues().roleName,
							'description' : systemRoleForm.getValues().roleDescription,
							'memberUserIds[]' : memberUserIds,
							'childrenRoleIds[]' : childrenRoleIds,
							'memberNotUserIds[]' : memberNotUserIds,
							'childrenNotRoleIds[]' : childrenNotRoleIds
						},
						success : function() {
							Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
							//Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
							self.close();
						},
	                	failure: function()
                        {
                        	 self.systemRoleForm.getForm().findField('roleName').focus(true);
                        }
					});
				}
            }
			else
			{
				this.setMarkInvalid(this.systemRoleForm.getForm().findField('roleName'));
			}


		});
		/* cancel */
		this.on('cancel', function() {
			this.close();
		});
		SystemRoleWindow.superclass.initComponent.apply(this, arguments);
	}
});

/* Таблица ролей */
SystemRolesGrid = function() {
	/* public */
	var self = this;

	/* buttons */
	this.createButton = new Ext.Button({
        text : 'Создать',
        iconCls: 'icon-add',
        handler : function() {
			this.fireEvent('create')
		},
		scope : this
	});

	this.editButton = new Ext.Button({
		text : 'Свойства',
        iconCls: 'icon-properties',
		handler : function() {
			this.fireEvent('edit')
		},
		scope : this
	});

	this.removeButton = new Ext.Button({
		text : 'Удалить',
        iconCls: 'icon-delete',
		handler : function() {
			this.fireEvent('remove')
		},
		scope : this
	});

	this.rolerelationButton = new Ext.Button({
		text : 'Назначение системных ролей',
		handler : function() {
			this.fireEvent('rolerelation')
		},
		scope : this
	});

	/* constructor */

	this.roleSelectionModel = new Ext.grid.CheckboxSelectionModel();
	this.roleStore = new Ext.data.WgsJsonStore({
		id : 'sysRoleStore',
		url : '/systemadmin/role/getrolelist',
		baseParams : {
			id : 0
		},
		autoLoad : true,
		fields : ['roleId', 'roleName', 'description', 'roleUserMembers', 'childrenRoles']

	});

	SystemRolesGrid.superclass.constructor.call(this, {

		title : 'Управление системными ролями',
		store : this.roleStore,
		cm : new Ext.grid.ColumnModel([//this.roleSelectionModel,
		{
			id : 'roleId',
			header : "Идентификатор роли",
			dataIndex : 'roleId',
			hidden : true,
			hideable : false
		}, {
			id : 'roleName',
			header : "Имя роли",
			sortable : true,
			dataIndex : 'roleName',
			width : 220
		}, {
			header : "Описание",
			sortable : true,
			dataIndex : 'description',
			width : 420
		}, {
			header : "Пользователи",
			dataIndex : 'roleUserMembers',
			width : 300
		}, {
			header : "Дочерние роли",
			dataIndex : 'childrenRoles',
			width : 300
		}

		]),
		sm : this.roleSelectionModel,
		viewConfig : {
			forceFit : true
		},
		autoScroll : true,

		tbar : [this.createButton, '-', this.editButton, '-',
				this.removeButton, '-', this.rolerelationButton],
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : this.roleStore,
			displayInfo : true
		})

	});
}
Ext.extend(SystemRolesGrid, Ext.grid.EditorGridPanel, {
	initComponent : function() {

		this.addEvents({
			create : true,
			edit : true,
			remove : true,
			rolerelation : true
		});

		/* create */
		this.on('create', function() {
			var systemRoleWindow = new SystemRoleWindow;

			systemRoleWindow.systemRoleUserTabPanel.availableUsersTree.loader = new Ext.tree.WgsTreeLoader({
				dataUrl : '/systemadmin/role/getavialableusers',
				baseParams : {
					id : 0
				}
			});
			systemRoleWindow.systemRoleUserTabPanel.membersUsersTree.loader = new Ext.tree.WgsTreeLoader({
				dataUrl : '/systemadmin/role/getemptytree',
				baseParams : {
					id : 0
				}
			});
			systemRoleWindow.systemRoleUserTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader({
				dataUrl : '/systemadmin/role/getavialableroles',
				baseParams : {
					id : 0
				}
			});
			systemRoleWindow.systemRoleUserTabPanel.childrenRolesTree.loader = new Ext.tree.WgsTreeLoader({
				dataUrl : '/systemadmin/role/getemptytree',
				baseParams : {
					id : 0
				},
				listeners:
		  		{
		  			load: function()
		  			{
		  				systemRoleWindow.cancelButton.enable();
		  				systemRoleWindow.okButton.enable();
		  			}
		  		}
			});

			systemRoleWindow.setTitle('Создание новой роли');
			systemRoleWindow.show();

		});

		/* edit */
		this.on('edit', function() {

			// alert();
			var systemRoleWindow = new SystemRoleWindow;

			var reader = new Ext.data.JsonReader({}, [
				{name : 'roleId'},
				{name : 'roleName'},
				{name : 'roleDescription'}

			]);

			var roleForChange = Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleSelectionModel.getSelections();

			if (roleForChange.length != 0) {
				if (roleForChange.length == 1) {
					var msgWait = Ext.Msg.wait('Получение данных о роли...', 'Подождите', {interval : 32});

					Systemadmin.conn.request({
						url : '/systemadmin/role/getroleproperties',
						method : 'POST',
						params : {
							'id' : roleForChange[0].data.roleId
						},
						success : function(response) {

							systemRoleWindow.show();
							systemRoleWindow.systemRoleForm.getForm().loadRecord(reader.read(response).records[0]);
							systemRoleWindow.setTitle('Изменение свойств роли : '+ roleForChange[0].data.roleName);

							msgWait.hide();
						}
					});

					systemRoleWindow.systemRoleUserTabPanel.availableUsersTree.loader = new Ext.tree.WgsTreeLoader({
						dataUrl : '/systemadmin/role/getavialableusers',
						baseParams : {
							id : roleForChange[0].data.roleId
						}
					});
					systemRoleWindow.systemRoleUserTabPanel.membersUsersTree.loader = new Ext.tree.WgsTreeLoader({
						dataUrl : '/systemadmin/role/getmemberusers',
						baseParams : {
							id : roleForChange[0].data.roleId
						}
					});
					systemRoleWindow.systemRoleUserTabPanel.availableRolesTree.loader = new Ext.tree.WgsTreeLoader({
						dataUrl : '/systemadmin/role/getavialableroles',
						baseParams : {
							id : roleForChange[0].data.roleId
						}
					});
					systemRoleWindow.systemRoleUserTabPanel.childrenRolesTree.loader = new Ext.tree.WgsTreeLoader({
						dataUrl : '/systemadmin/role/getchildrenroles',
						baseParams : {
							id : roleForChange[0].data.roleId
						},
						listeners:
				  		{
				  			load: function()
				  			{
				  				systemRoleWindow.cancelButton.enable();
				  				systemRoleWindow.okButton.enable();
				  			}
				  		}
					});
				} else
					Ext.Msg.show({
					title : 'Предупреждение',
					msg : 'Необходимо выбрать одну роль для изменения.',
					buttons : Ext.Msg.OK,
					icon : Ext.MessageBox.WARNING
				});

			}
			else
			  Ext.Msg.show({
					title : 'Предупреждение',
					msg : 'Необходимо выбрать одну роль для изменения.',
					buttons : Ext.Msg.OK,
					icon : Ext.MessageBox.WARNING
				});

		});

		/* remove */
		this.on('remove', function() {
			var rolesForRemoveIds = [];
			var rolesForRemove = Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleSelectionModel.getSelections();
			var message = '';
			if (rolesForRemove.length != 0) {
				if(rolesForRemove.length == 1)
				message = 'Вы действительно желаете удалить выбранную роль?';
				else
				message = 'Вы действительно желаете удалить выбранные роли?';
				Ext.MessageBox.confirm('', message,	function(btn) {
							if (btn == 'yes') {
								for (var i = 0; i < rolesForRemove.length; i++)
									rolesForRemoveIds[i] = rolesForRemove[i].data.roleId;

								Systemadmin.conn.request({
									url : '/systemadmin/role/removerole',
									method : 'POST',
									params : {
										'rolesForRemoveIds[]' : rolesForRemoveIds
									},
									success : function() {
										Systemadmin.systemRoleManagementPanel.systemRolesGrid.roleStore.reload();
										//Systemadmin.systemUserManagementPanel.systemUsersGrid.userStore.reload();
									}
								});
							}

						});

			} else
				Ext.Msg.show({
					title : 'Предупреждение',
					msg : 'Необходимо выбрать роль для удаления.',
					buttons : Ext.Msg.OK,
					icon : Ext.MessageBox.WARNING
				});
				//Ext.MessageBox.alert('Статус','Необходимо выбрать роль для удаления.');
		});

		/* rolerelation */
		this.on('rolerelation', function() {
			var roleRelationWindow = new RoleRelationWindow();
			roleRelationWindow.setTitle('Назначение системных ролей')
			roleRelationWindow.show();
		});
		SystemRolesGrid.superclass.initComponent.apply(this, arguments);
	}
});

/* Администратор системных ролей */
SystemRoleManagementPanel = function() {
	this.systemRolesGrid = new SystemRolesGrid();

	SystemRoleManagementPanel.superclass.constructor.call(this, {
		title : 'Роли',
		closable : false,
		autoScroll : true,
		layout : 'fit',
		items : [this.systemRolesGrid]
	});
}

Ext.extend(SystemRoleManagementPanel, Ext.Panel, {});