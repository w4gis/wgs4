Ext.ns('WGS.sys');

WGS.sys.ActiveTable = Ext.extend(Ext.util.Observable, {
	name: 'Active table',
	version: '1.0.0',
	revision: '$Revision: 87 $',
	revdate: '$Date: 2011-02-03 17:55:36 +0600 (×ò, 03 ôåâ 2011) $',
	module: 'Core',

	createFieldConverter: function(f, fieldNum)
	{
		var comboStore = new Ext.data.JsonStore({
            method: 'POST',
            url: f.store.dataUrl || WGS.constants.Table.LIST_URL,
            baseParams: {
				data: Ext.encode({
					object: f.store.object,
					params: f.store.baseParams
					/*params: Ext.apply({
						listname: "combobox"
					},f.store.baseParams)*/
				})
			},
            fields: f.store.fields,
            root: 'records'
        });

        comboStore.on('load', function(store, records){
    		var fs = this.fields[fieldNum].store;
    		if (fs) {
    			fs.comboStore = store;
    			fs.records = {};
        		Ext.each(records, function(record){
        			var v = record.get(fs.valueField),
        				d = record.get(fs.displayField);
        			fs.records[v] = d;
        		}, this);
    		}
    	}, this);

        comboStore.on('load', function(){
        	this.fireEvent('fieldprepared', f, fieldNum);
        }, this, {single: true});

        comboStore.on('loadexception', function(){
    		this.fireEvent('fieldprepared', f, fieldNum);
    	}, this, {single: true});

    	comboStore.load();
	},

	/**
	 * Supported column types:
	 * * string
	 * * date
	 * * select
	 * * link
	 * * int
	 * * checkbox
	 * * password
	 */
	prepareFields: function(fieldNum)
	{
		this.ready = false;

		var fieldNum = fieldNum||0,
			field = this.fields[fieldNum];

		var f = {
			name: field.name,
			mapping: field.mapping||field.name,
			convert: field.convert,
			defaultValue: field.defaultValue,
			type: field.type,
			readonly: field.readonly,
			allowBlank: field.allowBlank,
			maxLength: field.maxLength,
			rmlisteners: field.rmlisteners,
			addMode: field.addMode,
			editMode: field.editMode,
			hidden: field.hidden || false
		};

		switch (field.type) {
			case 'select':
			case 'multiselect':
			case 'linkselect':
				f.type = 'string';
				if (field.store) {
					f.store = field.store;
					f.mapping = null;
					if (field.store.type != 'local') {
						this.createFieldConverter(f, fieldNum);
					} else {
						this.fireEvent('fieldprepared', f, fieldNum);
					}
				}
				break;
			case 'password':
			case 'link':
			case 'date':
				f.dateFormat = field.dateFormat||WGS.constants.Date.TableFormat;
				this.fireEvent('fieldprepared', f, fieldNum);
				break;
			case 'checkbox':
				f.type = 'int';
				this.fireEvent('fieldprepared', f, fieldNum);
				break;
			case 'iconselect':
				f.type = 'string';
				this.fireEvent('fieldprepared', f, fieldNum);
				break;
			default:
				this.fireEvent('fieldprepared', f, fieldNum);
		}
	},

	onFieldPrepared: function(field, fieldNum)
	{
		this.recordFields.push(field);
		if (this.recordFields.length == this.fields.length) {
			// create grid record
			if (!this.record) {
				this.record = Ext.data.Record.create(this.recordFields);
			}
			this.recordFields = [];
			this.ready = true;
			this.fireEvent('ready', this);
		} else {
			this.prepareFields(fieldNum+1);
		}
	},

	createStores: function()
	{
		Ext.MessageBox.hide();

		if (!this.tableStore || !this.loadStore) {
			
			var params = {
				object: this.object,
				params: this.params? this.params: {}
				/*params: Ext.apply({
					listname: this.listname || "extended"}, 
				this.params? this.params: {})*/
			};
			
			if (this.project != undefined) {
				Ext.apply(params, {
					project: this.project
				});
			}
			
			var storesConfig = {
	            reader: new Ext.data.JsonReader({
	            	totalProperty: 'totalCount',
			        fields: this.record,
					root: 'records'
			    }),
	            method: 'POST',
	            url: this.urls.list,
				baseParams: {
					data: Ext.encode(params)
				},
	            sortInfo: this.sortInfo||{field: this.indexField, direction: "ASC"},
	            groupField: this.groupField,
	            nameField: this.nameField,
	            indexField: this.indexField
	        };
		}

		if (!this.tableStore) {
			// store for grid
			if (this.pageSize) {
				storesConfig.baseParams.limit = this.pageSize;
				storesConfig.baseParams.start = 0;
			}
			this.tableStore = new Ext.data.GroupingStore(storesConfig);
		}

		if (!this.loadStore) {
			// store for loading records
			this.loadStore = new Ext.data.GroupingStore(storesConfig);
		}

		if (!this.propertyStore) {
			// store for loading records
			this.propertyStore = new Ext.data.GroupingStore(Ext.apply(storesConfig, {url: this.urls.property}));
		}
	},

	onReady: function(callback, scope)
	{
		if (this.isReady()) {
			callback.call(scope || window, this);
		}
		else {
			this.on('ready', callback, scope || window, {single: true});
		}
	},

	isReady: function()
	{
		return this.ready;
	},

	isLockable: function()
	{
		return this.isObject;
	},

	save: function(record, options)
	{
		var url = this.urls.update;
		options.mode = 'update';

		if (!Ext.isArray(record) && !record.get(this.indexField)) {
			url = this.urls.create;
			options.mode = 'create';
		}

		this.query(url, record, options, 'save');
		//TODO: fire saved event
	},

	remove: function(record, options)
	{
		options = options||{};
		options.mode = options.mode||this.removeMode;
		if (options.mode != 'remove' && options.mode != 'delete') {
			options.mode = 'delete';
		}
		this.query(this.urls[options.mode], record, options);
	},

	query: function(url, record, options, action)
	{
		options = options||{};

		if (Ext.isObject(record)) {
			var records = [record];
		} else
		if (Ext.isArray(record)) {
			var records = record;
		} else {
			return;
		}

		options.scope = options.scope||this;
		options.success = options.success||Ext.emptyFn;
		options.failure = options.failure||Ext.emptyFn;
		if (action) {
			options.responseSuccess = (options.responseSuccess||Ext.emptyFn).createSequence(function(){
				this.fireEvent(action);
			}, this);
		} else {
			options.responseSuccess = options.responseSuccess||Ext.emptyFn;
		}
		options.responseFailure = options.responseFailure||Ext.emptyFn;

		for (var i=0; i<records.length; i++) {
			var cr = this.copyRecord(records[i]);
			cr.set(this.indexField, records[i].get(this.indexField));

			//TODO: refactore it!!
			for (var j=0, f; f = cr.fields.item(j); j++) {
				if (f.type.type == 'date' && cr.get(f.name) && Ext.isDate(cr.get(f.name))) {
					cr.set(f.name, cr.get(f.name).format(WGS.constants.Date.TableFormat));
				}
			}
			
			for (var j=0, f; f = cr.fields.item(j); j++) {
				if ((options.mode == 'update' &&
					f.editMode == false  &&
					f.name != this.indexField && !f.hidden) ||
					(options.mode == 'create' &&
					f.addMode == false && !f.hidden) ||
					((options.mode == 'delete' || options.mode == 'remove' || options.mode == 'lock' || options.mode == 'unlock') &&
					f.name != this.indexField)) {
						delete cr.data[f.name];
				}
			}

			var qp = Ext.apply({}, this.queryParams[options.mode]),
				p = Ext.apply(cr.data, this.params||{});
				data = Ext.applyIf(qp, {
					object: this.object, 
					project: this.project,
					params: p});
				
			WGS.Ajax.request({
				url: url,
				params: {data: Ext.encode(data)},
				success: options.success.createDelegate(options.scope, [records[i], i, records.length], true),
				failure: options.failure.createDelegate(options.scope, [records[i], i, records.length], true),
				responseSuccess: options.responseSuccess.createDelegate(options.scope, [records[i], i, records.length], true),
				responseFailure: options.responseFailure.createDelegate(options.scope, [records[i], i, records.length], true),
				scope: options.scope
			});
		}
	},

	/**
	 * Send array or records
	 */
	queryAll: function(url, records, options)
	{
		options = options||{};

		options.success = options.success||Ext.emptyFn;
		options.failure = options.failure||Ext.emptyFn;
		options.responseSuccess = options.responseSuccess||Ext.emptyFn;
		options.responseFailure = options.responseFailure||Ext.emptyFn;
		options.scope = options.scope||this;
		acr = [];	
		for (var i=0; i<records.length; i++) {
			var cr = this.copyRecord(records[i]);
			cr.set(this.indexField, records[i].get(this.indexField));

			//TODO: refactore it!!
			for (var j=0, f; f = cr.fields.item(j); j++) {
				if (f.type.type == 'date' && cr.get(f.name) && Ext.isDate(cr.get(f.name))) {
					cr.set(f.name, cr.get(f.name).format(WGS.constants.Date.TableFormat));
				}
			}
			acr.push(cr.data);
		}
		WGS.Ajax.request({
			url: url,
			params: {
				data: Ext.encode({
					object: this.object,
					project: this.project,
					locale: true,
					params: acr
				})
			},
			success: options.success.createDelegate(options.scope, [records[i], i, records.length], true),
			failure: options.failure.createDelegate(options.scope, [records[i], i, records.length], true),
			responseSuccess: options.responseSuccess.createDelegate(options.scope, [records[i], i, records.length], true),
			responseFailure: options.responseFailure.createDelegate(options.scope, [records[i], i, records.length], true),
			scope: options.scope
		});
	},
	
	filter: function(record, callback, scope)
	{
		this.tableStore.load({
			callback: callback,
			scope: scope,
			params: {
				data: Ext.encode({
					object: this.object,
					project: this.project,
					params: record.data
					//params: Ext.apply({listname: this.listname || "extended"}, record.data)
				})
			}
		});
	},

	loadRecords: function(record, callback, scope, locale)
	{
		this.loadStore.load({
			//callback: callback,
			callback: function(r, options, success){
				callback.call(scope, r, options, success);
			},
			scope: scope,
			params: {
				data: Ext.encode({
					object: this.object,
					project: this.project,
					locale: locale != undefined ? locale : undefined,
					params: record.data
					//params: Ext.apply({listname: this.listname || "extended"}, record.data)
				})
			}
		});
	},

	loadRecord: function(record, callback, scope)
	{
		this.propertyStore.load({
			callback: function(r, options, success){
				callback.call(scope, r.length? r[0]: r, options, success);
			},
			params: {
				data: Ext.encode({
					object: this.object,
					project: this.project,
					params: Ext.apply({}, record.data)
				})
			}
		});
	},

	lock: function(record, callback, scope)
	{
		callback = callback||Ext.emptyFn;
		if (this.isLockable()) {
			this.query(this.urls.lock, record, {
				responseSuccess: callback.createDelegate(scope, ['success'], true),
				responseFailure: callback.createDelegate(scope, ['failure'], true),
				failure: callback.createDelegate(scope),
				mode: 'lock'
			});
		} else {
			if (Ext.isArray(record)) {
				for (var i=0; i<record.length; i++) {
					callback.call(scope, false, false, record[i], i, record.length, 'success');
				}
			} else {
				callback.call(scope, false, false, record, 0, 1, 'success');
			}
		}
		//TODO: fire locked event
	},

	unlock: function(record, callback, scope)
	{
		callback = callback||Ext.emptyFn;
		if (this.isLockable()) {

			if (Ext.isObject(record)) {
				var records = [record];
			} else
			if (Ext.isArray(record)) {
				var records = record;
			} else {
				return;
			}

			var crecords = [];

			for (var i=0; i<records.length; i++) {
				if (records[i].get('isunlocked') != 1) {
					crecords.push(records[i]);
				}
			}

			this.query(this.urls.unlock, crecords, {
				responseSuccess: callback.createDelegate(scope, ['success'], true),
				responseFailure: callback.createDelegate(scope, ['failure'], true),
				failure: callback.createDelegate(scope),
				mode: 'unlock'
			});
		} else {
			//callback.call(scope, false, false, record, Ext.isArray(record)? record.length-1: false, Ext.isArray(record)? record.length: false, 'success');
			if (Ext.isArray(record)) {
				for (var i=0; i<record.length; i++) {
					callback.call(scope, false, false, record[i], i, record.length, 'success');
				}
			} else {
				callback.call(scope, false, false, record, 0, 1, 'success');
			}
		}
		//TODO: fire unlocked event
	},

	getFields: function()
	{
		return this.fields;
	},

	getRecord: function()
	{
		return this.record;
	},

	getStore: function()
	{
		return this.tableStore;
	},

	getIndexField: function()
	{
		return this.indexField;
	},

	createRecord: function(data)
	{
		/*
		if (this.isObject) {
			var data = data||{};
			Ext.applyIf(data, {
				islocked: '',
				isunlocked:''
			});
		}
		*/
		return new this.record(data);
	},

	copyRecord: function(record)
	{
		var recordData = Ext.apply({}, record.data),
			newRecord = new this.record(recordData);

		newRecord.set(this.indexField, '');

		return newRecord;
	},

	groupBy: function()
	{
		this.tableStore.groupBy(this, arguments);
	},

	constructor: function(config)
	{
		Ext.apply(this, config);

		this.urls = this.urls||{};

		Ext.applyIf(this.urls, {
			list: WGS.constants.Table.LIST_URL,
			property: WGS.constants.Table.PROPERTY_URL,
			create: WGS.constants.Table.CREATE_URL,
			update: WGS.constants.Table.UPDATE_URL,
			remove: WGS.constants.Table.REMOVE_URL,
			'delete': WGS.constants.Table.DELETE_URL,
			restore: WGS.constants.Table.RESTORE_URL,
			lock: WGS.constants.Table.LOCK_URL,
			unlock: WGS.constants.Table.UNLOCK_URL
		});

		Ext.applyIf(this, {
			fields: [],
			recordFields: [],
			record: null,
			store: null,
			ready: false,
			pageSize: 25,
			indexField: 'sid',
			nameField: 'name',
			isObject: false,
			removeMode: 'delete',
			autoUnlock: true,
			queryParams: Ext.applyIf(this.queryParams||{}, {
				'create': {},
				'update': {},
				'delete': {},
				'remove': {},
				'lock'  : {},
				'unlock': {}
			})
		});

		WGS.sys.ActiveTable.superclass.constructor.call(this, config);

		Ext.MessageBox.wait(this.waitMsg, this.waitTitle);

		this.addEvents(
			'fieldprepared',
			'ready',
			////////
			'loaded',
			'created',
			'saved',
			'updated',
			'deleted',
			'removed',
			'restored',
			'locked',
			'unlocked'
		);

		this.on('fieldprepared', this.onFieldPrepared, this);
		this.on('ready', this.createStores, this);

		this.prepareFields();
	}
});

WGS.sys.ActiveTable.getTable = function(object, config) {
	if (object) {
		var objectTable = object[0].toUpperCase() + object.substr(1, object.length).toLowerCase();
		if (WGS.table[objectTable]) {
			return new WGS.table[objectTable](config);
		}
	}
	return false;
};