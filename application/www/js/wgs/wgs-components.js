﻿/* WGS.Topmenu */

WGS.Topmenu = function()
{
    this.timeout = null;
    
    this.handlerModuleOver = function(module, nestedModule)
    {
        $$('.WgsNestedMenu').each(function(menu)
        {
            if (menu.id != nestedModule) menu.hide();
        });
        var x = Position.cumulativeOffset($(module))[0];
        var s = Math.round(x-$(nestedModule).getWidth()/2+$(module).getWidth()/2);
        if (s+$(nestedModule).getWidth() > WGS.Tools.getClientDimensions().width)
            s = WGS.Tools.getClientDimensions().width - $(nestedModule).getWidth() - 1;          
        $(nestedModule).setStyle({left: s + 'px', right: ''});
        Effect.Appear(nestedModule, {duration: 0.2});
        window.clearTimeout(this.timeout);
    }
    
    this.handlerNestedModuleOver = function()
    {
        window.clearTimeout(this.timeout);
    }
    
    this.handlerModulesOut = function(nestedModule)
    {
        this.timeout = window.setTimeout(function(){Effect.Fade(nestedModule, {duration: 0.2})}, 500);      
    }

    var self = this;
    
    return {
        init: function()
        {
            $$('.WgsTopMenuLink').each(function(link){
                if (link.href!='#')
                    link.onclick = function onclick(event) {
                        //WGS.Resource.Open(this.href);
                        //return false;
                        return true;
                    };
            });
            $$('.WgsNestedMenu').each(function(menu){
                if ($(menu.id.split(":")[1]))
                {
                    $(menu.id.split(":")[1]).onmouseover = self.handlerModuleOver.bind(self, menu.id.split(":")[1], menu.id);
                    $(menu.id.split(":")[1]).onmouseout = self.handlerModulesOut.bind(self, menu.id);
                    $(menu.id).onmouseover = self.handlerNestedModuleOver.bind(self);
                    $(menu.id).onmouseout = self.handlerModulesOut.bind(self, menu.id);
                }
            });            
        }
    }
}();

/* WGS.LoginWindow */

WGS.LoginWindow = function()
{
    var self = this;
    
    this.url = '/information';
    
    if (!Ext.get('LoginWindow'))
        Ext.getBody().createChild({tag: 'div', id: 'LoginWindow'});
    
    var usernameField = new Ext.form.TextField({
        fieldLabel: 'Логин',
        name: 'username'
    });
    
    var passwordField = new Ext.form.Field({
        fieldLabel: 'Пароль',
        name: 'password',
        inputType: 'password'
    }); 
        
    this.loginForm =  new Ext.form.WgsFormPanel({                           
        method: 'POST',
        url: '/authorization/login',
        width: 461,
        frame: true,
        border: false,
        buttonAlign: 'right',
        bodyStyle: 'padding: 3px 25px',
        defaults: {width: 205},
        items: [usernameField, passwordField]
    });
    
    WGS.LoginWindow.superclass.constructor.call(this, {
        title: 'ПО Web-ГИС-клиент ЭГП :: Вход в систему',
        constrain: true,
        width: 478,
        autoHeight: true,
        plain: true,
        resizable: false,
        closable: false,
        animateTarget: 'LoginWindow',
        items: [
            new Ext.Panel({  
                width: 461,
                height: 168, 
                html: "<img src='/images/main1.png'>"
            }),
           this.loginForm
        ],
        focus: function()
        {
           usernameField.focus();
        }
    });
    
    this.submitAction = function()
    {
        okButton.disable();
        cancelButton.disable();
        this.loginForm.submit({
		
            success: function(form, action) {
                Ext.DomHelper.append('ext-gen3', {tag: 'div', /*cls: 'new-div-cls'*/ id: 'new-div-id', html: "Связь с Web-ГИС-сервером установлена"});
				setTimeout(function(){
					self.hide();               
								
				var returnUrl = WGS.Cookie.get('return');
			    if (returnUrl) {
                    WGS.Tools.navigate(returnUrl);
                    WGS.Cookie.set('return', '');
                } else {
					if(self.url!='/information')
						WGS.Tools.navigate(self.url);
					else {
						Ext.Ajax.request({
							url : 'map/favoriteview/getCount',
							success : function(response, options){
								if (response.responseText>0)
									WGS.Tools.navigate(self.url);
								else 
									WGS.Tools.navigate('/map');
								
							},
							failure: function(){
								WGS.Tools.navigate(self.url);
							},
							params 		: {								
							}
						});
					}
                }
				}, 3000)
            },
            failure: function(form, action) {
                okButton.enable();
                cancelButton.enable();
                usernameField.focus();
            }
        });
    }
    
    var okButton = this.addButton({
        text: 'OK',
        handler: this.submitAction,
        scope: this
    });
    
    var cancelButton = this.addButton({
        text: 'Отмена',
        handler: function(){
            this.loginForm.getForm().reset();
            usernameField.focus();
        },
        scope: this
    });
    
    usernameField.on('specialkey',function(f, event){
        if(event.getKey() == event.ENTER) {
            this.submitAction();
        }
    }, this);
    
    passwordField.on('specialkey',function(f, event){
        if(event.getKey() == event.ENTER) {
            this.submitAction();
        }
    }, this);
    
    this.on('show', function(){
		var loginInfo = WGS.Tools.Location.GetHash().substr(1);
		var loginArr = loginInfo.split('|');		
        var isPublic = WGS.Cookie.get('public');
        if (isPublic == 1) {
            WGS.Cookie.set('public', '');
            this.loginForm.getForm().setValues({
                username: 'public',
                password: 'public'
            })
            this.submitAction();
        }else{
			if(loginArr[0] && loginArr[1]){
				this.loginForm.getForm().setValues({
                username: loginArr[0],
                password: loginArr[1]
				})
				this.submitAction();
			}
		}
    }, this);
}

Ext.extend(WGS.LoginWindow, Ext.Window);

/* WGS.PluginsWindow */

WGS.PluginsWindow = function()
{
    // Проверяем наличие установленных компонентов в системе 
    var AdobeReader = false;
    var AdobeFlashPlayer = false;
    var DesignReview = false;
    
    if (Ext.isGecko || Ext.isOpera || Ext.isSafari) AdobeReader = 'Adobe Acrobat';
    if (Ext.isIE) AdobeReader = 'AcroPDF.PDF.1';
    
    var AdobeReaderExist = isExistActiveX.call(this, AdobeReader);
    
    if (Ext.isGecko || Ext.isOpera || Ext.isSafari) AdobeFlashPlayer = 'Shockwave Flash';
    if (Ext.isIE) AdobeFlashPlayer = 'ShockwaveFlash.ShockwaveFlash.9';
    
    var AdobeFlashPlayerExist = isExistActiveX.call(this, AdobeFlashPlayer);
    
    var DesignReviewExist = true;
    if (Ext.isIE) {
        DesignReviewExist = false;
        DesignReview = 'AdView.AdViewer.1';
        DesignReviewExist = isExistActiveX.call(this, DesignReview);
    }
    
    
    this.DesignReview = ['/images/DesignReview.png','/downloads/SetupDesignReview2009.exe', '', 'AdView.AdViewer.1', 'Autodesk Design Review'];
    this.AdobeReader = ['/images/AdobeReader.png','/downloads/AdbeRdr90ru.exe', 'Adobe Acrobat', 'AcroPDF.PDF.1', 'Adobe Reader'];
    this.AdobeFlashPlayer = ['/images/AdobeFlashPlayer.png','/downloads/install_flash_player_active_x.exe', '', 'AdView.AdViewer.1', 'Adobe Flash Player'];   
    
    this.data = new Array();
    
    if (!DesignReviewExist) {
        this.data[this.data.length] = this.DesignReview;
    }
    if (!AdobeReaderExist) {
        this.data[this.data.length] = this.AdobeReader;
    }
    if (!AdobeFlashPlayerExist) {
        this.data[this.data.length] = this.AdobeFlashPlayer;
    }
    
    this.pluginsStore = new Ext.data.SimpleStore({
        root: 'documents',
        fields: ['image', 'url', 'plugin', 'activeX', 'name'],
        data: this.data        
    });
    
    this.tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap" id="{name}">',
            '<div class="thumb"><img src="{image}" title="{name}"></div>',
            '</div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );
    
    this.pluginsDataView = new Ext.DataView ({
        store: this.pluginsStore,
        tpl: this.tpl,
        autoHeight: true,
        autoWidth: true, 
        singleSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.thumb-wrap',
        emptyText: 'Нет документов для отображения',
        prepareData: function(data){
            data.name = Ext.util.Format.ellipsis(data.name, 30);
            return data;
        }
    });
    
    this.pluginsPanel = new Ext.Panel ({
        id: 'plugin-view',
        collapsible : false,
        margins : '0 0 0 0',
        layout : 'fit',
        autoScroll : true,
        items: [this.pluginsDataView]
    });
      
    WGS.PluginsWindow.superclass.constructor.call(this, {
        title: 'Установка дополнительных компонентов',
        constrain: true,
        width: 495,
        autoHeight: true,
        plain: true,
        resizable: false,
        closable: false,
        items: [ this.pluginsPanel ]
    });
    
    var installButton = this.addButton({
        text: 'Установить',
        handler: this.install,
        scope: this
    });
    
    var nextButton = this.addButton({
        text: 'Далее',
        handler: this.login,
        scope: this
    });
    
}

Ext.extend(WGS.PluginsWindow, Ext.Window, {
    initComponent : function() {
        
        this.addEvents({
        });
        
        this.install = function() {
            this.pluginsDataView.getSelectedRecords().each(function (record){
                window.location.href = record.data.url;
            });
        }
        
        this.login = function() {
            WGS.Cookie.set('pluginsAgreement', true);
            var loginWindow = new WGS.LoginWindow();
            loginWindow.show();
            this.hide();
        }
        
        this.on('show', function() {
            this.pluginsDataView.selectRange(0, 0, true);
            if (this.data.length == 0) {
                this.hide();
                var loginWindow = new WGS.LoginWindow();
                loginWindow.show();
            }
        }, this);
    
        WGS.PluginsWindow.superclass.initComponent.apply(this, arguments);
    }
});