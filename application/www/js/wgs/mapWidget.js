Map = function()
{
    var viewport = new Ext.Viewport({
        layout: 'fit'
    });
    
    var mapViewer;
    var loadMask;
    
    return {
        init: function()
        {
            Map.config = Ext.decode(Map.config);
            
            if (Map.config.viewerType == 'ajax') {
                this.initMap();
            } else {  
                if (!Ext.isIE) {
                    Ext.WgsMsg.show({
                        title: '���������',
                        msg: '��� ������� �� ������������ DWF-������ �����.',
                        buttons: Ext.MessageBox.OK,
                        fn: function () {
                            window.location.href = '/map/ajax';
                        },
                        icon: Ext.MessageBox.INFO
                    });
                } else {  
                    if (isExistActiveX.call(this, 'AdView.AdViewer.1')) {
                        this.initMap();
                    } else {
                        Ext.WgsMsg.show({
                            title: '���������',
                            msg: '��� ��������� DWF-������ ����� ���������� ���������� ' +
                                 '<A href="/downloads/SetupDesignReview2009.exe">Autodesk Design Review</A>',
                            buttons: Ext.MessageBox.OK,
                            fn: function () {
                                //window.location.href = '/downloads/SetupDesignReview2009.exe';
                            },
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            }
        },
        
        initMap: function()
        {
			window.MAP = this
            window.mapViewer = new Map.Viewer({
                viewerType: Map.config.viewerType,
                
                render: function(viewerPanel) {
                    Ext.apply(viewerPanel, {style: 'padding-top: 0px'});
                    viewport.add(viewerPanel);
                    viewport.doLayout();
                    loadMask = new Ext.LoadMask(viewerPanel.items.first().getEl(),{msg: '��������...'});
                    loadMask.show();
                },
                
                load: function(api, lib, commands) {
                    if (Map.config.callback) {
                        Map[Map.config.callback].call(this, Map.config);
                    }
                    loadMask.hide();
                    
                    var hashParams = Map.getHashParams();
                    if (hashParams) {
                    	Map.zoomToFavoriteView(hashParams);
                    }
					
                },
                
                failure: function() {
                    loadMask.hide();
                }
            });
        },
        
        zoomToFavoriteView: function(config)
        {
			var handler = function(){
                window.mapViewer.Api.Map.ZoomToView(parseFloat(config.centerX), parseFloat(config.centerY), parseFloat(config.scale), true);
                window.mapViewer.Api.Map.StopObservingOnMapLoaded(handler);
                if (Ext.isArray(config.layers)) {
                	window.mapViewer.Lib.setLayerVisibility(config.layers);					
                }
				if (Ext.isArray(config.object)) {
                	var object = {type:config.objectType, coords:config.object}
					//console.log(object)
					if(config.objectType=="polygon"){
						var sr = Map.SelectPolygon.Instance(window.mapViewer)
						sr.open();
						var c = []
						for(var i=0; i<object.coords.length; i++){
							var l = c.length;
							var xy = object.coords[i].split(' ');
							c[l]={x:xy[0],y:xy[1]}						
						}
						sr.drawByStrangePoints(c)
					}else if(config.objectType=="radius"){					
						var sr = Map.SelectRadius.Instance(window.mapViewer)
						sr.open();						
						sr.drawByStrangePoints({x:object.coords[0],y:object.coords[1],r:object.coords[2]})
					}else if(config.objectType=="select"){						
						window.mapViewer.Lib.selectFeatures(object.coords)
						//console.log(object.coords)
					}			
                }
            }
            window.mapViewer.Api.Map.ObserveOnMapLoaded(handler, window.MAP);
			window.mapViewer.Api.Map.Refresh();
        },
        
        zoomToObject: function(config) {
            window.mapViewer.Api.Map.ObserveOnMapLoaded(function(){
                //mapViewer.Api.Map.ZoomToView(parseFloat(config.centerX), parseFloat(config.centerY), parseFloat(config.scale), true);
                window.mapViewer.Lib.selectFeatures([config.objectId]);
            }, this, true);
        },
        		
        getHashParams: function(str) {
			str = str || false
			var hash = str ? str: WGS.Tools.Location.GetHash().substr(1),
				hashArr = hash.split("|");
			
			if (hashArr.length > 0 && hash) {
				//get coordinates and scale
				var xy = hashArr[0].split(","),
					 x = parseFloat(xy[0]),
					 y = parseFloat(xy[1]),
					 s = parseFloat(hashArr[1]),
					 l = hashArr[2]? hashArr[2].split(","): hashArr[2]==undefined? null: [],
					 ct = hashArr[3] ? hashArr[3] : null,
					 cv = hashArr[4]? hashArr[4].split(","): hashArr[4]==undefined? null: [];
					 
				return {
					centerX: x,
					centerY: y,
					scale: s,
					layers: l,
					object: cv,
					objectType: ct
				}
			} else {
				return null;
			}
        }
    }
}();