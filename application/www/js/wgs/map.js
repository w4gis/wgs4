﻿Map = function()
{
    var viewport = new Ext.Viewport({
        layout: 'fit'
    });
    
    var mapViewer;
    var loadMask;
    
    return {
        init: function()
        {
            Map.config = Ext.decode(Map.config);
            
            if (Map.config.viewerType == 'ajax') {
                this.initMap();
            } else {  
                if (!Ext.isIE) {
                    Ext.WgsMsg.show({
                        title: 'Сообщение',
                        msg: 'Ваш браузер не поддерживает DWF-версию карты.',
                        buttons: Ext.MessageBox.OK,
                        fn: function () {
                            window.location.href = '/map/ajax';
                        },
                        icon: Ext.MessageBox.INFO
                    });
                } else {  
                    if (isExistActiveX.call(this, 'AdView.AdViewer.1')) {
                        this.initMap();
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Сообщение',
                            msg: 'Для просмотра DWF-версии карты необходимо установить ' +
                                 '<A href="/downloads/SetupDesignReview2009.exe">Autodesk Design Review</A>',
                            buttons: Ext.MessageBox.OK,
                            fn: function () {
                                //window.location.href = '/downloads/SetupDesignReview2009.exe';
                            },
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            }
        },
        
        initMap: function()
        {
            mapViewer = new Map.Viewer({
                viewerType: Map.config.viewerType,
                
                render: function(viewerPanel) {
                    Ext.apply(viewerPanel, {style: 'padding-top: 46px'});
                    viewport.add(viewerPanel);
                    viewport.doLayout();
                    loadMask = new Ext.LoadMask(viewerPanel.items.first().getEl(),{msg: 'Загрузка...'});
                    loadMask.show();
                },
                
                load: function(api, lib, commands) {
                    if (Map.config.callback) {
                        Map[Map.config.callback].call(this, Map.config);
                    }
                    loadMask.hide();
                    
                    var hashParams = Map.getHashParams();
                    if (hashParams) {
                    	Map.zoomToFavoriteView(hashParams);
                    }
                },
                
                failure: function() {
                    loadMask.hide();
                }
            });
        },
        
        zoomToFavoriteView: function(config)
        {
            var handler = function(){
                mapViewer.Api.Map.ZoomToView(parseFloat(config.centerX), parseFloat(config.centerY), parseFloat(config.scale), true);
                mapViewer.Api.Map.StopObservingOnMapLoaded(handler);
                if (Ext.isArray(config.layers)) {
                	mapViewer.Lib.setLayerVisibility(config.layers);
                }
            }
            mapViewer.Api.Map.ObserveOnMapLoaded(handler, this);
        },
        
        zoomToObject: function(config) {
            mapViewer.Api.Map.ObserveOnMapLoaded(function(){
                //mapViewer.Api.Map.ZoomToView(parseFloat(config.centerX), parseFloat(config.centerY), parseFloat(config.scale), true);
                mapViewer.Lib.selectFeatures([config.objectId]);
            }, this, true);
        },
        
        getHashParams: function() {
			var hash = WGS.Tools.Location.GetHash().substr(1),
				hashArr = hash.split("|");
			
			if (hashArr.length > 0 && hash) {
				//get coordinates and scale
				var xy = hashArr[0].split(","),
					 x = parseFloat(xy[0]),
					 y = parseFloat(xy[1]),
					 s = parseFloat(hashArr[1]),
					 l = hashArr[2]? hashArr[2].split(","): hashArr[2]==undefined? null: [];
				return {
					centerX: x,
					centerY: y,
					scale: s,
					layers: l
				}
			} else {
				return null;
			}
        }
    }
}();