﻿Ext.namespace('Map');

Map.Buffer = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;
    this.wgsConn = new Ext.data.WgsConnection();
    this.conn = new Ext.data.Connection();
    this.panel = new BufferPanel({
        width: 350,
        map: this.viewerApi.Map
    });
    this.panel.bufferUnitsCombobox.setValue('ki');
    this.panel.fillPatternCombobox.setValue('Solid');
    this.panel.linePatternCombobox.setValue('Solid');
    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        width: this.panel.width+12,
        title: 'Построение буферной зоны',
        resizable: false
    });
    this.window.setInnerPanel(this.panel);
    this.window.on('hide', this.onWindowHide, this);
    this.viewerApi.Map.ObserveOnMapLoaded(function () {this.panel.loadLayers();}, this);
}

Map.Buffer.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Buffer(map);
    }
    return this.instances[map.mapFrame];
}

Map.Buffer.prototype = {
    open: function()
    {
        if(this.viewerApi.Map.GetSelectedCount() > 0) {
            this.window.addPanel();
            this.window.show();
            this.panel.show();
            this.panel.loadLayers();
        }
        else
            Ext.WgsMsg.show({
                title: 'Предупреждение',
                msg: 'Необходимо выбрать объекты',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
    },

    onWindowHide: function()
    {
        if(this.panel.colorWindow)
            this.panel.colorWindow.hide()
    }
}

BufferPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config) {
        if (config) {
            Ext.apply(this, config);
        }
        this.colorWindow = null;
        //this.map = this.viewerApi.Map;
        //----Список слоев, относитеоьно которых строится зона

        this.layersStore = new Ext.data.WgsJsonStore({
             fields: ['layerName', 'layerLegend']
        });

        this.layersGrid = new Ext.grid.GridPanel({
            store: this.layersStore,
            cm: new Ext.grid.ColumnModel([
                {id: 'layerName', hidden: true, hideable: false},
                {id: 'layerLegend',sortable: false, dataIndex: 'layerLegend'}
            ]),
            sm : new Ext.grid.RowSelectionModel({}),
            viewConfig:
            {
                forceFit: true
            },
            autoScroll: true,
            hideHeaders: true,
            stripeRows: true,
            frame: true,
            anchor: '100%',
            height: 130
        });

        this.loadLayers = function() {
            this.layersStore.loadData([],false);
            var data;
            var layers = this.map.GetSelectedLayers();
            if (layers.length != 0) {
                for(var i = 0; i < layers.length; i++) {
                    data = [{"layerName":layers[i].name,"layerLegend":layers[i].name}];
                    this.layersStore.loadData(data,true);
                }
                this.okButton.enable();

            } else {
                data = [{"layerName":"","layerLegend":"Нет выбранных слоев"}];
                this.layersStore.loadData(data,true);
                this.okButton.disable();
            }
        };

        this.layersStore.on('load', function(store, records, options){
            if (records.length) {
                if (records[0].data.layerName)
                    this.layersGrid.getSelectionModel().selectFirstRow();
            }
        }, this);

        /* Радиус буферной зоны */
        this.distanceNumberField = new Ext.form.NumberField({
            labelSeparator: '',
            value: '1'
        });

        /* Единицы измерения буферной зоны */
        Ext.namespace('Ext.bufferUnits');
        Ext.bufferUnits.states = [['m','м'],['ki','км']];

        this.bufferUnitsStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: Ext.bufferUnits.states
        });

        this.bufferUnitsStates.loadData(Ext.bufferUnits.states);

        this.bufferUnitsCombobox = new Ext.form.ComboBox({
            name:'bufferUnits',
            store: this.bufferUnitsStates,
            displayField:'state',
            valueField: 'id',
            labelSeparator: '',
            //typeAhead: true,
            mode: 'local',
            anchor:'100%',
            triggerAction: 'all',
            editable:false,
            selectOnFocus:true
        });

        /* Название слоя */
        this.bufferLayerNameTextField = new Ext.form.TextField({
            fieldLabel: 'Название слоя',
            value: 'Буферная зона',
            hideLabel: true,
            anchor:'99%'
        });

        /* Соединить буферные зоны */
        this.mergeBufferAreasCheckbox = new Ext.form.Checkbox({
            name: 'locked',
            anchor: false,
            checked: true,
            hideLabel: true,
            boxLabel: 'Соединить буфферные зоны'
        });

        /* Форма настроек буферной зоны */
        this.bufferSettingsFormPanel = new Ext.FormPanel({
            labelAlign: 'top',
            frame: true,
            autoHeight: true,
            autoWidth: true,
            region : 'center',
            items: [{
                xtype:'fieldset',
                title: 'Название слоя',
                autoHeight: true,
                anchor:'100%',
                items:
                [{
                    layout:'column',
                    border:false,
                    items:[{
                        columnWidth: 1,
                        layout: 'form',
                        items: [ this.bufferLayerNameTextField ]
                    }]
                }]
            },
            {
                xtype:'fieldset',
                title: 'Расстояние вокруг объектов',
                autoHeight: true,
                layout:'column',
                anchor:'100%',
                items:[{
                    columnWidth:.5,
                    layout: 'fit',
                    bodyStyle:'padding:1px 1px 1px 1px',
                    items: [ this.distanceNumberField ]
                },
                {
                    columnWidth:.5,
                    bodyStyle:'padding:1px 1px 1px 1px',
                    layout: 'fit',
                    items: [ this.bufferUnitsCombobox ]
                }]
            },
            {
                xtype: 'fieldset',
                title: 'Слои, входящие в буферную зону',
                autoHeight: true,
                anchor: '100%',
                items: [{
                    layout: 'column',
                    border: false,
                    items:[
                    {
                        columnWidth: 1,
                        layout: 'form',
                        items: [this.layersGrid]
                    }]
                },
                {
                    layout: 'column',
                    style: 'padding-top: 5px; padding-left:5px',
                    items:[{
                        columnWidth: 0.7,
                        layout: 'form',
                        items: [ this.mergeBufferAreasCheckbox ]
                    },
                    {
                        columnWidth: .3,
                        layout: 'form',
                        bodyStyle: 'padding:2px 2px 2px 2px',
                        anchor: '100%',
                        items: [
                            new Ext.Button({
                                iconCls: 'icon-refresh-arrow',
                                text: 'Обновить',
                                handler: function()
                                {
                                    this.loadLayers();
                                },
                                scope: this
                            })
                        ]
                    }]
                }]
            }]
        });

        //----Параметры отображения

        //----Цвет заливки
        this.fillColorTextField = new Ext.form.TextField({
            fieldLabel: 'Цвет заливки',
            value: 'ff0000',
            style: 'background: #ff0000; color: #ff0000',
            anchor:'100%',
            readOnly: true
        });
		
        //----Шаблон заливки
        Ext.namespace('Ext.fillPattern');
        Ext.fillPattern.states = [
            ['Solid','Сплошная'],
            ['Net','Сетка'],
            ['Line','Линии'],
            ['Box','Бокс'],
            ['Steel','Сталь']
        ];

        this.fillPatternStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: Ext.fillPattern.states
        });

        this.fillPatternStates.loadData(Ext.fillPattern.states);

        this.fillPatternCombobox = new Ext.form.ComboBox({
            store: this.fillPatternStates,
            displayField: 'state',
            valueField: 'id',
            fieldLabel: 'Стиль заливки',
            mode: 'local',
            triggerAction: 'all',
            anchor: '100%',
            editable: false,
            selectOnFocus: true
        });

        this.transparensySlider = new Ext.Slider({
            width: 135,
            value:50,
            increment: 1,
            minValue: 0,
            maxValue: 100,
            listeners:
            {
                change: function(element, value)
                {
                    this.transparencyTextField.setValue(Math.round(value)+'%');
                },
                scope: this
            }
        });

        this.transparencyTextField = new Ext.form.TextField({
            labelSeparator: '',
            hideLabel: true,
            readOnly: true,
            value: '50%',
            width: 39
        });

        //----Цвет границы
        this.lineColorTextField = new Ext.form.TextField({
            fieldLabel: 'Цвет границы',
            style: 'background: #000000; color: #000000',
            value: '000000',
            anchor: '100%',
            readOnly: true
        });

        //----Шаблон заливки
        Ext.namespace('Ext.linePattern');
        Ext.linePattern.states = [
            ['Solid','Сплошная'],
            ['Dush','Штриховая'],
            ['Dot','Пунктирная'],
            ['DushDot','Штрихпунктирная']
        ];

        this.linePatternStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: Ext.linePattern.states
        });

        this.linePatternStates.loadData(Ext.linePattern.states);

        this.linePatternCombobox = new Ext.form.ComboBox({
            store: this.linePatternStates,
            displayField: 'state',
            valueField: 'id',
            fieldLabel: 'Стиль границы',
            //typeAhead: true,
            triggerAction: 'all',
            mode: 'local',
            anchor:'100%',
            editable: false,
            selectOnFocus: true
        });

        this.thicknessLineNumberField = new Ext.form.NumberField({
            fieldLabel: 'Толщина линии',
            value: 1,
            anchor:'100%'
        });

        this.bufferStylePanel = new Ext.FormPanel({
            labelAlign: 'left',
            labelWidth: 120,
            //title: 'Параметры отображения',
            bodyStyle: 'padding-top: 3px; padding-left: 5px',
            frame: true,
            autoHeight: true,
            region : 'south',
            anchor: '100%',
            items: [{
            //---- Параметры заливки

                xtype:'fieldset',
                title: 'Заливка',
                autoHeight: true,
                collapsible: true,
                collapsed: true,
                //layout:'column',
                style: 'padding-top: 5px',
                anchor: '99%',
                items: [{
                    layout: 'column',
                    border: false,
                    items: [{
                        columnWidth: 1,
                        layout: 'form',
                        border: false,
                        items: [this.fillPatternCombobox]
                    }]
                },
                {
                    layout:'column',
                    border:false,
                    items: [{
                        columnWidth: 0.87,
                        layout: 'form',
                        border: false,
                        items: [this.fillColorTextField]
                    },
                    {
                        columnWidth: .13,
                        layout: 'form',
                        border:false,
                        bodyStyle:'padding:1px 1px 1px 1px',
                        items: [
                            new Ext.Button(
                            {
                                text: '...',
                                handler: function() {
                                    if(!this.colorWindow) {
										this.colorWindow = new Ext.WgsColorPalette(this.fillColorTextField, '000000');
                                    } else {
                                        this.colorWindow.setSource(this.fillColorTextField);
                                    }
                                    this.colorWindow.show();
                                },
                                scope: this
                            })
                        ]
                    }]
                },
                {
                    layout: 'column',
                    border: false,
                    items: [
                    {
                        columnWidth: .41,
                        layout: 'form',
                        border: false,
                        items: [
                                {
                                    xtype: 'label',
                                    style: 'margin-top: 6px; font-size: 9pt;',
                                    text: 'Прозрачность:'
                                }
                               ]
                    },
                    {
                        columnWidth: .46,
                        layout: 'form',
                        border: false,
                        items: [this.transparensySlider]
                    },
                    {
                        columnWidth: .13,
                        layout: 'form',
                        border: false,
                        //anchor: '100%',
                        bodyStyle: 'padding-right:10px',
                        items: [this.transparencyTextField]
                    }]
                }]
            },
            //---- Параметры границы
            {
                xtype: 'fieldset',
                title: 'Граница',
                autoHeight: true,
                collapsible: true,
                collapsed: true,
                style: 'padding-top: 5px',
                //layout:'column',
                anchor: '99%',
                items: [{
                    layout: 'column',
                    border: false,
                    items: [{
                        columnWidth: 1,
                        layout: 'form',
                        border: false,
                        items: [this.linePatternCombobox]
                    }]
                },
                {
                    layout: 'column',
                    border: false,
                    items: [{
                        columnWidth: 0.87,
                        layout: 'form',
                        border: false,
                        items: [this.lineColorTextField]
                    },
                    {
                        columnWidth: .13,
                        layout: 'form',
                        border: false,
                        bodyStyle: 'padding:1px 1px 1px 1px',
                        items: [
                            new Ext.Button({
                                text: '...',
                                handler: function() {
                                    if(!this.colorWindow) {
                                         this.colorWindow = new Ext.WgsColorPalette(this.lineColorTextField, '000000');
                                    } else {
                                        this.colorWindow.setSource(this.lineColorTextField);
                                    }
                                    this.colorWindow.show();
                                },
                                scope: this
                            })
                        ]
                    }]
                },
                {
                    layout: 'column',
                    border: false,
                    items: [{
                        columnWidth: 1,
                        layout: 'form',
                        border: false,
                        items: [this.thicknessLineNumberField]
                    }]
                }]
            }]
        });

        BufferPanel.superclass.constructor.call(this, {
            title: ' ',
            header: true,
            resizable : false,
            autoHeight: true,
            layout : 'fit',
            items : [this.bufferSettingsFormPanel, this.bufferStylePanel],
            listeners:
            {
                show: function()
                {
                    this.loadLayers();
                },
                scope: this
            }
        });
        this.okButton = this.addButton({
            iconCls: 'icon-accept-tick',
            text : 'ОК',
            handler : function() {
                this.fireEvent('ok');
            },
            scope : this
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Закрыть',
            handler : function() {
                this.fireEvent('cancel');
            },
            scope : this
        });
    },

    initComponent : function() {
        this.addEvents({
            ok : true,
            cancel : true
        });
        this.on('ok', this.onOk);
        this.on('cancel', this.onCancel);
        BufferPanel.superclass.initComponent.apply(this, arguments);
    },

    onOk: function() {
        var xmlSel = this.map.GetSelectionXML();
        var msgWait = Ext.WgsMsg.wait('Идет выполнение операции...', 'Подождите', {interval: 50});
        var conn = new Ext.data.WgsConnection();

        var layerNames = [];
        var selectionLayers = this.layersGrid.getSelectionModel().getSelections();
        for(var i = 0; i < selectionLayers.length; i++) {
            layerNames[layerNames.length] = selectionLayers[i].data.layerName;
        }
        conn.request({
            //url : '/map/buffer/createbuffer',
            url : '/map/fusion/createbuffer',
            method : 'POST',
            params : {
                'session'       : this.map.GetSessionId(),
                'mapname'       : this.map.GetMapName(),
                'buffername'    : this.bufferLayerNameTextField.getValue(),
                'selection'     : xmlSel,
                'layerNames[]'  : layerNames,
                'distance'      : this.distanceNumberField.getValue(),
                'units'         : this.bufferUnitsCombobox.getValue(),
                'locale'        : 'ru',
                'fbcolor'       : 'ffffff',
                'ffcolor'       : this.fillColorTextField.getValue(),
                'fillstyle'     : this.fillPatternCombobox.getValue(),
                'foretrans'     : this.transparensySlider.getValue(),
                'lcolor'        : this.lineColorTextField.getValue(),
                'thickness'     : this.thicknessLineNumberField.getValue(),
                'linestyle'     : this.linePatternCombobox.getValue(),
                'merge'         : this.mergeBufferAreasCheckbox.getValue()? '1': '0'
            },
            success : function(response)
            {
                msgWait.hide();
                this.map.Refresh();
            },
            scope: this
        });
     },

     onCancel: function() {
        this.hide();
     }
});
