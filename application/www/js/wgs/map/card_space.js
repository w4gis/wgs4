﻿Ext.namespace('Map');

Map.Card = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.conn = new Ext.data.Connection();
    this.wgsConn = new Ext.data.WgsConnection();
    this.panel = new Map.Card.CardPanel();
    //this.loadMask = null;
    this.attrCard = null;
    this.modeCard = 0;
    this.featureId = 0;
    this.entityId = 0;
    this.objectId = 0;
    this.objectIdForPrint = 0;
    this.lastConfig = {};
    if (mapViewer) {
        this.window = new Ext.WgsServiceWindow({
            containerPanel: mapViewer.rightPane,
            width: this.panel.width+12,
            layout: 'fit',
            title: 'Карточка',
            resizable: false,
            minimizable: false
        });
    } else {
        this.window = new Ext.WgsServiceWindowSimple({
            width: this.panel.width+12,
            layout: 'fit',
            title: 'Карточка',
            resizable: false,
            minimizable: false
        });
    }
    this.window.setInnerPanel(this.panel);
    this.panel.featuredocButton.on('click', this.openFeatureDoc,  this);
    this.panel.cardCombobox.on('select', this.cardComboboxOnSelect,  this);
    this.panel.printButton.on('click', this.printCard, this);
    this.panel.saveButton.on('click', this.saveCard, this);
    this.panel.cancelButton.on('click', this.onCancel, this);
    this.panel.childCardsStore.on('load', this.onChildCardsLoaded, this);
    /*this.window.on('show', function(){
        this.loadMask = new Ext.LoadMask(this.panel.getEl(),{msg: 'Загрузка...'});
        this.loadMask.show();
    },this);*/
}

Map.Card.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Card(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Card(map);
        }
        return this.instance;
    }
}

Map.Card.Create = function(map)
{
    return new Map.Card(map);
}

Map.Card.prototype = {
    open: function(config)
    {
        if(!WGS.PRIVILEGE.CARD_EDIT)
            this.panel.saveButton.hide();
        var config = config||{};

        this.lastConfig = config;

        this.cardCallback = config.callback||Ext.emptyFn;
        this.cardScope = config.scope||this;

        if (!config.objectId && !config.featureId) {
            if (this.mapViewer) {
                this.mapViewer.Lib.getSelectedFeatures(function(featureLayers){
                    if(featureLayers != -1){
                        if (featureLayers[0].features && featureLayers[0].features.length == 1)
                        {
                            var featureId = featureLayers[0].features[0].featureId;
                            this.entityId = featureLayers[0].features[0].featureId;
                            this.getCards(featureId, null);
                        }
                        else
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Необходимо выбрать один объект',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                    else
                    Ext.WgsMsg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо выбрать один объект',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                },this);
            }
        } else {
            this.mapViewer = config.mapViewer;
            if (config.objectId) {
                this.getCards(null, config.objectId);
                return;
            }
            if (config.featureId) {
                this.getCards(config.featureId, null);
                return;
            }
        }
    },

    getCards: function(featureId, objectId)
    {
        var cardJsonReader =  new Ext.data.JsonReader({}, [
            {name: 'cardId'},
            {name: 'cardName'},
            {name: 'cardIsCurrent'},
            {name: 'cardTypeName'},
            {name: 'cardRelationName'},
            {name: 'cardLabel'}
        ]);

        if(featureId && !objectId){
            this.featureId = featureId;
            this.objectId = null;
            this.wgsConn.request({
                url: '/objectadmin/card/getcardsbyfeatureid',
                method: 'POST',
                params: {'featureId':featureId},
                success: function(response) {
                    if (response.responseText != 'null') {
                        //alert(cardJsonReader.read(response).records.length);
                        if (cardJsonReader.read(response).records.length > 0) {
                            var cards = cardJsonReader.read(response).records;
                            //this.open();
                            this.window.addPanel();
                            this.window.show();
                            this.panel.cardCombobox.clearValue();
                            var objectId = cards[0].data.cardId;
                            this.panel.cardsStore.removeAll();
                            this.panel.cardsStore.add(cards);
                            this.panel.cardCombobox.setValue(cards[0].data.cardLabel);
                            this.removeAttrCardPanel();
                            this.addAttrCardPanel(objectId);
                            this.modeCard = 1;
                            this.loadValueBaseProperties(objectId, this.modeCard);
                        }
                        else {
                            Ext.WgsMsg.show({
                                title: 'Предупреждение',
                                msg: 'Необходимо выбрать один объект',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Описаний не найдено',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            });
        }
        if(objectId && !featureId){
            this.objectId = objectId;
            this.featureId = null;
            this.panel.featuredocButton.hide();
            this.wgsConn.request({
                url: '/objectadmin/card/getentityidbyobject',
                method: 'POST',
                params: {id: this.objectId},
                success: function(response) {
                    if (response.responseText) {
                        if (response.responseText != '0') {
                            this.panel.featuredocButton.show();
                        }
                    }
                },
                scope: this
            });
            this.wgsConn.request({
                url: '/objectadmin/card/getcardsbyobjectid',
                method: 'POST',
                params: {'objectId': objectId},
                success: function(response) {
                    if (response.responseText != 'null') {
                        if (cardJsonReader.read(response).records.length > 0) {
                            var cards = cardJsonReader.read(response).records;
                            this.window.addPanel();
                            this.window.show();
                            this.panel.cardCombobox.clearValue();
                            var objectId = cards[0].data.cardId;
                            this.panel.cardsStore.removeAll();
                            this.panel.cardsStore.add(cards);
                            this.panel.cardCombobox.setValue(cards[0].data.cardLabel);
                            this.removeAttrCardPanel();
                            this.addAttrCardPanel(objectId);
                            this.modeCard = 0;
                            this.loadValueBaseProperties(objectId, this.modeCard);
                        }
                    }
                },
                scope: this
            });
        }
    },

    addAttrCardPanel: function(objectId)
    {
        this.attrCard = new Map.Card.AttrCardFormPanel(this.panel, objectId, this.mapViewer, this);
        this.attrCard.setObjectId(objectId);
        this.panel.attrCardPanel.add(this.attrCard);
        this.panel.doLayout();
    },

    removeAttrCardPanel: function()
    {
        if(this.attrCard != null){
            this.panel.attrCardPanel.remove(this.attrCard);
            this.panel.doLayout();
            this.attrCard = null;
        }
    },

    cardComboboxOnSelect: function(combo, record, index)
    {
        this.panel.cardCombobox.setValue(record.data.cardLabel);
        var objectId = record.get('cardId');
        this.removeAttrCardPanel();
        this.addAttrCardPanel(objectId);
        this.wgsConn.request({
            url: '/objectadmin/card/getentityidbyobject',
            method: 'POST',
            params: {id: objectId},
            success: function(response) {
                if (response.responseText) {
                    if (response.responseText == '0') {
                        this.panel.featuredocButton.hide();
                    } else {
                        this.panel.featuredocButton.show();
                    }
                }
            },
            scope: this
        });
        this.loadValueBaseProperties(objectId, this.modeCard);
    },

    reloadCardsStore: function() {
        var url = '';
        var params = new Object();
        var cardJsonReader =  new Ext.data.JsonReader({}, [
            {name: 'cardId'},
            {name: 'cardName'},
            {name: 'cardIsCurrent'},
            {name: 'cardTypeName'},
            {name: 'cardRelationName'},
            {name: 'cardLabel'}
        ]);

        if(this.featureId && !this.objectId) {
            url = '/objectadmin/card/getcardsbyfeatureid',
            params = {'featureId':this.featureId}
        }
        if (!this.featureId && this.objectId) {
            url = '/objectadmin/card/getcardsbyobjectid',
            params = {'objectId':this.objectId}
        }
        var currentCardValue = this.panel.cardCombobox.getValue();

        this.wgsConn.request({
            url: url,
            method: 'POST',
            params: params,
            success: function(response) {
                if (response.responseText != 'null') {
                    if (cardJsonReader.read(response).records.length > 0) {
                        var cards = cardJsonReader.read(response).records;
                        this.panel.cardCombobox.clearValue();
                        var objectId = cards[0].data.cardId;
                        this.panel.cardsStore.removeAll();
                        this.panel.cardsStore.add(cards);
                        this.panel.cardCombobox.setValue(currentCardValue);
                    }
                }
            },
            scope: this
        });
    },

    /*onChildCardsLoaded: function(store, records, options)
    {
        if (records.length > 0) {
            this.panel.listChildCardFieldSet.show();
        } else {
            this.panel.listChildCardFieldSet.hide();
        }
    },*/

    saveCard: function()
    {
       
        var objectId = this.panel.cardCombobox.getValue();
        
        this.attrCard.saveCard({
            callback: this.cardCallback,
            scope: this.cardScope
        });
      
        if(this.modeCard) {
        
            this.panel.cardsStore.each(function(record) {
                    if(record.get('cardId') == objectId) {
                        if(this.attrCard.getForm().getValues().name != '')
                            record.set('cardName',this.attrCard.getForm().getValues().name+' - '+record.get('cardTypeName'));
                        else
                            record.set('cardName','Без наименования - '+record.get('cardTypeName'));
                        record.commit();
                        this.panel.cardCombobox.setValue(objectId);
                    }
                }, this
            );
        }
        
        else {
           
            if(this.attrCard.getForm().getValues().name != '')
                this.panel.cardCombobox.setValue(this.attrCard.getForm().getValues().name+' - '+this.attrCard.getForm().getValues().typeName);
            else
                this.panel.cardCombobox.setValue('Без наименования - '+this.attrCard.getForm().getValues().typeName);
        }
        
    },

    loadValueBaseProperties: function(objectId, mode)
    {
        //var msgWait = Ext.Msg.wait('Запрос характеристик...', 'Подождите', {interval:50});
        this.objectIdForPrint = objectId;
        this.wgsConn.request({
                url: '/objectadmin/card/getvaluebaseproperties',
                method: 'POST',
                params: {'objectId':objectId},
                success: function(response) {
                    var record = this.panel.basePropertiesReader.read(response).records[0];
                    this.attrCard.getForm().loadRecord(record);
                    if(!this.modeCard) {
                        if(record.data.name != '')
                            this.panel.cardCombobox.setValue(record.data.name+' - '+record.data.typeName);
                        else
                            this.panel.cardCombobox.setValue('Без наименования - '+record.data.typeName);
                    }
                    //this.panel.enable();
                    //this.loadMask.hide();
                    this.attrCard.setReadOnlyCreateDateField();
                    this.panel.doLayout();
                    //msgWait.hide();
                },
                scope: this
            });
    },

    onCancel: function()
    {
        this.panel.hide();
    },

    printCard: function()
    {
       //alert("1"+self);
       //alert("2"+self.object_id);
       //alert("3"+self.relation_id);
       //alert("4"+self.mapViewer);
      // alert("5"+this.objectIdForPrint);
      //  alert(this.panel.BaseParamAttrCardPanel);
        var myY;
         var record;
         var msgWait = Ext.Msg.wait('Загрузка формы...', 'Подождите', {interval: 50});
         this.wgsConn.request({
                url: '/objectadmin/card/getvaluebaseproperties',
                method: 'POST',
                params: {'objectId':this.objectIdForPrint},
                success: function(response) {
                    record= this.panel.basePropertiesReader.read(response).records[0];
                   
                    
                       self.choiceAttrTypeRelationWindowNew = new Map.Card.ChoiceAttrTypeRelationWindowNew
                (self,self.object_id,self.relation_id, self.mapViewer,this.objectIdForPrint,record.data.shortName,this,this.wgsConn );
                        self.choiceAttrTypeRelationWindowNew.show();
                        msgWait.hide();
                },
                scope: this
            });
         //var msgWait = Ext.Msg.wait('Запрос характеристик...', 'Подождите', {interval:50});
        
        
      
        //this.print();
    },

    openFeatureDoc: function ()
    {
        var url = '', id = 0;

        this.featureId = this.featureId || this.lastConfig.featureId;
        this.entityId = this.entityId || this.lastConfig.entityId;
        this.objectId = this.objectId || this.lastConfig.objectId;

        //alert('featureId: ' + this.featureId + '; entityId: ' + this.entityId + '; objectId: ' + this.objectId);

        if (this.entityId) {
            Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(this.entityId, [this.entityId]);
        } else {
            if (this.featureId) {
                url = '/objectadmin/card/getentityidbyfeature';
                id = this.featureId;
            } else {
                if (this.objectId) {
                    url = '/objectadmin/card/getentityidbyobject';
                    id = this.objectId;
                }
            }

            this.wgsConn.request({
                url: url,
                method: 'POST',
                params: {id: id},
                success: function(response) {
                    if (response.responseText) {
                        if (response.responseText != '0') {
                            //this.entityId = response.responseText;
                            var entityId = response.responseText;
                            Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(entityId, [entityId]);
                        }
                    }
                },
                scope: this
            });
        }
    },

    print: function()
    {
        var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
        this.wgsConn.request({
            url: '/objectadmin/card/print',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {objectId: this.objectIdForPrint},
            success: function(response) {
                msgWait.hide();
                if (response.responseText) {
                    //alert(response.responseText);
                    this.openPdf(response.responseText);
                }
            },
            scope: this
        });
    },
    printNew: function()
    {
        var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
        this.wgsConn.request({
            url: '/objectadmin/card/print',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {objectId: this.objectIdForPrint},
            success: function(response) {
                msgWait.hide();
                if (response.responseText) {
                    //alert(response.responseText);
                    this.openPdfNew(response.responseText);
                }
            },
            scope: this
        });
    },
    
    openPdf: function(pdfSeq)
    {   //alert("pdfSeq: "+pdfSeq);
        this.docWindow = new Ext.WgsShimWindow({
            title : 'Печать',
            closable : true,
            width : 550,
            height : 600,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'+pdfSeq+'"></param><EMBED src="/objectadmin/card/getpdf/'+pdfSeq+'" width="100%" height="100%" href="/objectadmin/card/getpdf/'+pdfSeq+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
    },
     openPdfNew: function(pdfSeq)
    {   //alert("pdfSeq: "+pdfSeq);
        this.docWindow = new Ext.WgsShimWindow({
            title : 'Печать',
            closable : true,
            width : 550,
            height : 600,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'+pdfSeq+'"></param><EMBED src="/objectadmin/card/getpdf/'+pdfSeq+'" width="100%" height="100%" href="/objectadmin/card/getpdf/'+pdfSeq+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
    }
}

Map.Card.AdvancedParamRelationPanel = function(owner, objectRelationId)
{
    this.owner = owner;
    this.objectRelationId = objectRelationId;

    var self = this;

    this.storeJS = new Ext.data.JsonStore(
    {
        autoLoad: true,
        fields: ['name', 'value','vulueTypeId','groupId','groupName','id'],
        url: '/objectadmin/card/GetValuePropertiesByObjRel',
        baseParams:{objectRelationId: self.objectRelationId}
    });

    self.setObjectRelationId = function(_ObjectRelationId)
    {
        self.objectRelationId = _ObjectRelationId;
        self.storeJS.baseParams.objectRelationId = _ObjectRelationId;
        self.storeJS.reload();
    };

    Map.Card.AdvancedParamRelationPanel.superclass.constructor.call(this,
    {
        title:'Характеристики связи',
        autoHeight: true,
        autoScroll: true,
        closable: false,
        defaultType: 'textfield',
        items:[ {
            name: 'objectRelationId',
            hidden: true,
            labelSeparator:''
        }]
    });
}
Ext.extend(Map.Card.AdvancedParamRelationPanel,Ext.form.FieldSet,
{
    initComponent : function()
    {
        var self = this;
        self.storeJS.on('load',  function()
        {
            self.domainStores = [];
            var group_id = -1;
            var groupFieldSet;
            self.storeJS.each(function(record)
            {
                if (group_id != record.data.groupId)
                {
                    if (groupFieldSet)
                    {
                        self.add(groupFieldSet);
                    }
                    groupFieldSet = new Ext.form.FieldSet(
                    {
                        title: 'Группа характеристик: '+record.data.groupName,
                        autoHeight:true,
                        collapsible : true,
                        closable: false,
                        anchor: '97%',
                        collapsed: true
                    });
                    if (group_id == -1) {groupFieldSet.collapsed = false}
                    group_id = record.data.groupId;
                }
                if (record.data.vulueTypeId == 1)
                {
                    var propertyEditor = new Ext.form.TextField(
                    {
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 2)
                {
                    var propertyEditor = new Ext.form.DateField(
                    {
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        format: 'Y-m-d'
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 3)
                {
                    var propertyEditor = new Ext.form.NumberField(
                    {
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        allowDecimals: false,
                        maxText : 10
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 4)
                {
                    var propertyEditor = new Ext.form.NumberField(
                    {
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        allowDecimals: true,
                        maxText : 10,
                        decimalSeparator:','
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 5)
                {
                    self.domainStores[self.domainStores.length] = new Ext.data.JsonStore(
                    {
                        //autoLoad: true,
                        fields: ['id', 'name'],
                        url: '/objectadmin/card/GetValueDomainList',
                        baseParams:{propId: record.data.id}
                    });
                    self.domainStores[self.domainStores.length-1].load(
                    {
                        callback : function()
                        {
                            propertyEditor.setValue(record.data.value);
                            propertyHidden.setValue(propertyEditor.getValue());
                        }
                    });
                    var propertyHidden = new Ext.form.TextField(
                    {
                        name: 'propRel-' + record.data.id,
                        hideLabel: true,
                        hidden: true,
                        labelSeparator:'',
                        value: record.data.value
                    });
                    var propertyEditor = new Ext.form.ComboBox(
                    {
                        fieldLabel: record.data.name,
                        name: 'gridRel-' + record.data.id,
                        store: self.domainStores[self.domainStores.length-1],
                        anchor:'97%',
                        displayField:'name',
                        valueField: 'id',
                        typeAhead: true,
                        loadingText:'Список загружается...',
                        triggerAction: 'all',
                        emptyText:'Выберите значение...',
                        selectOnFocus:true
                    });
                    propertyEditor.on('select',function(combo, record, index )
                    {
                        propertyHidden.setValue(propertyEditor.getValue());
                    });
                    groupFieldSet.add(propertyEditor);
                    groupFieldSet.add(propertyHidden);
                }
                else if (record.data.vulueTypeId == 7)
                {
                    var propertyEditor = new Ext.form.TextArea(
                    {
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }
            });
            if (groupFieldSet)
            {
                self.add(groupFieldSet);
                self.owner.doLayout();
            }
        },this);

        Map.Card.AdvancedParamRelationPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-ПАНЕЛЬ СВЯЗИ -*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.RelationCardPanel = function(owner, relation_id, object_id, relationType, mapViewer)
{

    this.owner = owner;
    this.relation_id = relation_id;
    this.object_id = object_id;
    this.relationType = relationType;
    this.mapViewer = mapViewer;
    var self = this;

    this.storeJS = new Ext.data.JsonStore(
    {
        autoLoad: true,
        fields: ['id', 'name', 'shortName', 'typeId', 'typeName', 'invNumber', 'createDate', 'note', 'author', 'label','ObjTypeRelationId','objRelationId'],
        url: '/objectadmin/card/GetListChildObjectByRelation',
        baseParams:{objectId: self.object_id, relationId: self.relation_id}
    });

    this.newButton = new Ext.Button(
    {
          text : 'Создать',
          handler : function()
          {
               this.fireEvent('NewChildCard')
          },
          scope : this
     });
    this.bindButton = new Ext.Button(
    {
          text : 'Найти и привязать',
          handler : function()
          {
               this.fireEvent('BindChildCard')
          },
          scope : this
     });

    this.unBindButton = new Ext.Button(
    {
          text : 'Отвязать',
          handler : function()
          {
               this.fireEvent('UnBindChildCard')
          },
          scope : this
     });
    this.removeButton = new Ext.Button(
    {
          text : 'Удалить',
          handler : function()
          {
               this.fireEvent('RemoveChildCard')
          },
          scope : this
     });

    this.editButton = new Ext.Button(
    {
          text : 'Свойства',
          handler : function()
          {
               this.fireEvent('EditChildCard',self.gridChildCard.getSelectionModel().getSelected())
          },
          scope : this
     });
    this.newButton.disable();
    this.bindButton.disable();
    this.unBindButton.disable();
    this.removeButton.disable();
    this.editButton.disable();
    Map.Card.RelationCardPanel.superclass.constructor.call(this,
    {
        autoScroll: true,
        tbar:[this.newButton,this.bindButton,this.unBindButton,this.removeButton,this.editButton],
        items: [

            self.gridChildCard = new Ext.grid.GridPanel({
                viewConfig:
                {
                    //autoFill :false,
                    forceFit: true
                },
                store:this.storeJS,
                //width: 520,
                height: 100,
                autoScroll:true,
                columns:[
                    { id:'id',                  dataIndex: 'id', hideable: false, hidden: true },
                    { id: 'name',       header: "Полное наименование", sortable: true, dataIndex: 'name' },
                    { id: 'shortName',  header: "Отчетный год", dataIndex: 'shortName' },
                    { id: 'typeId',             dataIndex: 'typeId', hideable: false, hidden: true },
                    { id: 'typeName',   header: "Наименованиe описания", sortable: true, dataIndex: 'typeName' },
                    { id: 'invNumber',          dataIndex: 'invNumber', hideable: false, hidden: true },
                    { id: 'createDate',         dataIndex: 'createDate', hideable: false, hidden: true },
                    { id: 'note',               dataIndex: 'note', hideable: false, hidden: true },
                    { id: 'author',             dataIndex: 'author', hideable: false,  hidden: true },
                    { id: 'label',              dataIndex: 'label', hideable: false, hidden: true },
                    { id: 'ObjTypeRelationId',  dataIndex: 'ObjTypeRelationId', hideable: false, hidden: true },
                    { id: 'objRelationId',      dataIndex: 'objRelationId', hideable: false, hidden: true }
                    ]
            })
            ]
    });
}
Ext.extend(Map.Card.RelationCardPanel,Ext.Panel,
{
    initComponent : function()
    {
        this.addEvents(
        {
            NewChildCard : true,
            UnBindChildCard : true,
            RemoveChildCard : true,
            EditChildCard : true
        });

        var self = this;
        this.storeJS.on('load', function()
        {
            var countRecord = self.storeJS.getCount();
            if (countRecord > 0) {
                self.gridChildCard.getSelectionModel().selectFirstRow();
            }
            if (self.relationType == 1)
            {
                if (countRecord == 1)
                {
                    self.newButton.disable();
                    self.bindButton.disable();
                    self.unBindButton.enable();
                    self.removeButton.enable();
                    self.editButton.enable();
                }
                else
                {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.disable();
                    self.removeButton.disable();
                    self.editButton.disable();
                }
            }
            if (self.relationType == 2)
            {
                if (countRecord > 0)
                {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.enable();
                    self.removeButton.enable();
                    self.editButton.enable();
                }
                else
                {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.disable();
                    self.removeButton.disable();
                    self.editButton.disable();
                }
            }
        });

        this.on('NewChildCard', function()
        {
            this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
                fields: ['id', 'name','objTypeRelationId'],
                url: '/objectadmin/card/GetListChildObjectTypeByRelation'
            });
            this.featureTypeByRelationStore.removeAll();
            this.featureTypeByRelationStore.baseParams = {/*objectId: this.object_id, */relationId: this.relation_id};
            
            this.featureTypeByRelationStore.load({
                callback: function(records) {
                    if (records.length > 0) {
                        self.choiceAttrTypeRelationWindow = new Map.Card.ChoiceAttrTypeRelationWindow(self,self.object_id,self.relation_id, self.mapViewer);
                        self.choiceAttrTypeRelationWindow.show();
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            });
        });
            
          this.on('PRINT_PDF', function()
        {
            this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
                fields: ['id', 'name','objTypeRelationId'],
                url: '/objectadmin/card/GetListChildObjectTypeByRelation'
            });
            this.featureTypeByRelationStore.removeAll();
            this.featureTypeByRelationStore.baseParams = {/*objectId: this.object_id, */relationId: this.relation_id};
            
            this.featureTypeByRelationStore.load({
                callback: function(records) {
                    if (records.length > 0) {
                        self.choiceAttrTypeRelationWindow = new Map.Card.ChoiceYearWindow(self,self.object_id,self.relation_id, self.mapViewer);
                        self.choiceAttrTypeRelationWindow.show();
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            });
        });
        
        this.on('BindChildCard', function()
        {
            this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
                fields: ['id', 'name','objTypeRelationId'],
                url: '/objectadmin/card/GetListChildObjectTypeByRelation'
            });
            this.featureTypeByRelationStore.removeAll();
            this.featureTypeByRelationStore.baseParams = {/*objectId: this.object_id, */relationId: this.relation_id};
            this.featureTypeByRelationStore.load({
                callback: function(records) {
                    if (records.length > 0) {
                        var featureTypeFilter = [];
                        Ext.each(records, function(record){
                            featureTypeFilter.push(record.data.id);
                        });
                        var bindSearch = Map.Bind.Search.Instance(this.mapViewer);
                        bindSearch.open({
                            object: this.object_id,
                            featureTypeFilter: featureTypeFilter,
                            mode: Map.Bind.Search.OBJECTBIND_MODE,
                            callback: function(objectId1, objectId2, featureTypeId) {
                                var objTypeRelationId;
                                this.featureTypeByRelationStore.findBy(function(record){
                                    if (record.data.id == featureTypeId) {
                                        objTypeRelationId = record.data.objTypeRelationId;
                                        return;
                                    }
                                },this);
                                new Ext.data.WgsConnection().request({
                                    url: '/objectadmin/card/BindChildObject',
                                    method: 'POST',
                                    params: {parentObjectId: objectId2, childObjectId: objectId1, objTypeRelationId: objTypeRelationId},
                                    success: function(action)
                                    {
                                        bindSearch.panel.resultTabPanel.resultStore.removeAll();
                                        bindSearch.container.hide();
                                        this.storeJS.removeAll();
                                        this.storeJS.load();
                                        this.owner.owner.reloadCardStore();
                                    },
                                    scope: this
                                });
                            },
                            scope: this
                        });
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            }, this);
        }, this);


        this.on('UnBindChildCard', function(record){
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите отвязать выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId){
                    if (buttonId == 'yes') {
                        var selectedRelations = self.gridChildCard.getSelectionModel().getSelections();
                        if(selectedRelations.length != 0)
                        {
                            var relationsForRemove = [];
                            for (var i=0; i<selectedRelations.length; i++)
                            {
                                relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
                            }
                            var msgWait = Ext.Msg.wait('Отвязка объектов...', 'Подождите', {interval: 50});
                            new Ext.data.WgsConnection().request(
                            {
                                url: '/objectadmin/card/RemoveObjectRelation',
                                method: 'POST',
                                params:
                                {
                                    removeMode: 1,
                                    'relationsId[]': relationsForRemove
                                },
                                success: function()
                                {
                                        self.storeJS.reload();
                                        self.owner.owner.reloadCardStore();
                                        msgWait.hide();
                                }
                            });
                        }

                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        });

        this.on('RemoveChildCard', function(record)
        {
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите удалить выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId){
                    if (buttonId == 'yes') {
                        var selectedRelations = self.gridChildCard.getSelectionModel().getSelections();
                        if(selectedRelations.length != 0)
                        {
                            var relationsForRemove = [];
                            for (var i=0; i<selectedRelations.length; i++)
                            {
                                relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
                            }
                            var msgWait = Ext.Msg.wait('Удаление объектов...', 'Подождите', {interval: 50});
                            new Ext.data.WgsConnection().request(
                            {
                                url: '/objectadmin/card/RemoveObjectRelation',
                                method: 'POST',
                                params:
                                {
                                    removeMode: 0,
                                    'relationsId[]': relationsForRemove
                                },
                                success: function()
                                {
                                    self.storeJS.reload();
                                    self.owner.owner.reloadCardStore();
                                    msgWait.hide();
                                }
                            });
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        });

        this.on('EditChildCard', function(record)
        {
           Map.Card.Create(this.mapViewer).open({
                            objectId: record.data.id,
                            callback: function() {
                                this.storeJS.reload();
                                this.owner.owner.reloadCardStore();
                            },
                            scope: this
                        });
        });
    this.owner.owner.controlReadOnly();
    Map.Card.RelationCardPanel.superclass.initComponent.apply(this, arguments);
    }
});

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ОКНО ВЫБОРА АТРИБУТИВНОГО ТИПА ДЛЯ СВЯЗИ-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
Map.Card.ChoiceAttrTypeRelationWindow = function(owner, parentObjectId, relation_id, mapViewer)
{
    this.owner = owner;
    this.relation_id = relation_id;
    this.parentObjectId = parentObjectId;
    this.mapViewer = mapViewer;
    var self = this;

    this.storeJS = new Ext.data.WgsJsonStore(
    {
        autoLoad: true,
        fields: ['id', 'name','objTypeRelationId'],
        url: '/objectadmin/card/GetListChildObjectTypeByRelation',
        baseParams: {/*objectId: parentObjectId, */relationId: self.relation_id}
    });

    this.saveButton = new Ext.Button(
    {
          text : 'Создать',
          handler : function()
          {
               this.fireEvent('save')
          },
          scope : this
     });
    this.cancelButton = new Ext.Button(
    {
          text : 'Отмена',
          handler : function()
          {
               this.fireEvent('cancel')
          },
          scope : this
     });
     self.saveButton.disable();

    this.attrTypeComboBox = new Ext.form.ComboBox({
        anchor: '100%',
        hideLabel: true,
        labelSeparator: '',
        store: this.storeJS,
        displayField: 'name',
        valueField: 'id',
        loadingText: 'Список загружается...',
        triggerAction: 'all',
        emptyText: 'Выберите описание...',
        allowBlank: true,
        editable: false
    });

    Map.Card.ChoiceAttrTypeRelationWindow.superclass.constructor.call(this,
    {
        title:'Выбор атрибутивного описания',
        layout: 'fit',
        height: 180,
        width: 375,
        modal: true,
        autoScrol: true,
        resizable: false,
        items:[
            new Ext.form.FormPanel({
                frame: true,
                bodyStyle:'padding:5px 5px 0',
                items:  [
                    new Ext.Panel({
                        frame: true,
                        height: 60,
                        layout: 'fit',
                        style: 'margin-bottom: 5px',
                        html: '<p>Если вы уверены, что хотите создать атрибутивный объект и ' +
                              'связать его с данным, выберите атрибутивный тип и нажмите "Создать", ' +
                              'иначе "Отмена"</p>'
                    }),
                    this.attrTypeComboBox]
            })],
        buttons: [this.saveButton,this.cancelButton]
    });
}


Map.Card.ChoiceAttrTypeRelationWindowNew = function(owner, parentObjectId, relation_id,
mapViewer,objectIdForPrint,year,lookedObject, connector)
{
    this.owner = owner;
    this.relation_id = relation_id;
    this.parentObjectId = parentObjectId;
    this.mapViewer = mapViewer;
    var self = this;
    this.objectIdForPrint = objectIdForPrint;
    var myYear = year;
    var loockedObj = lookedObject;
    this.connector = connector;
    
 /*
    this.storeJS = new Ext.data.WgsJsonStore(
    {
        autoLoad: true,
        fields: [],
        url: '/objectadmin/card/GetValueBasePropertiesByObject',
        baseParams: {objectId: this.objectIdForPrint}
    })
    */ 
    
     
     
        // alert(this.objectIdForPrint);
    //alert("6  "+this.objectIdForPrint);
   //alert("7  "+wgsTier->getProjectObjectService()->getAttObjectService()->getValueBasePropertiesByObject(this.objectIdForPrint));
    this.saveButton = new Ext.Button(
    {
          text : 'Сформировать',
          handler : function()
          {
               this.fireEvent('save')
          },
          scope : this
     });
    this.cancelButton = new Ext.Button(
    {
          text : 'Отмена',
          handler : function()
          {
               this.fireEvent('cancel')
          },
          scope : this
     });
    // self.saveButton.disable();
   
    this.attrTypeComboBox = 
    new Ext.form.TextField({
            id:"from",
            fieldLabel:"Отчетный год",            
            allowBlank:false,
            maxLength:4,
            minLength:4,
            value:myYear
         })
    
    /*
    new Ext.form.ComboBox({
        anchor: '100%',
        hideLabel: true,
        labelSeparator: '',
        store: this.storeJS,
        displayField: 'name',
        valueField: 'id',
        loadingText: 'Список загружается...',
        triggerAction: 'all',
        emptyText: 'Выберите описание...',
        allowBlank: true,
        editable: false
    })*/;

    Map.Card.ChoiceAttrTypeRelationWindowNew.superclass.constructor.call(this,
    {
        title:'Выбор отчетного года',
        layout: 'fit',
        height: 120,
        width: 300,
        modal: true,
        autoScrol: true,
        resizable: false,
        items:[
            new Ext.form.FormPanel({
                frame: true,
                bodyStyle:'padding:5px 5px 0',
                items:  [
                  
                    this.attrTypeComboBox]
            })],
        buttons: [this.saveButton,this.cancelButton]
    });
}
Ext.extend(Map.Card.ChoiceAttrTypeRelationWindowNew, Ext.Window,{
    initComponent : function()
    {
        this.addEvents(
        {
            save : true,
            cancel : true
        });

        var self = this;

        self.attrTypeComboBox.on('select',function()
        {
            self.saveButton.enable();
        });

        this.on('save', function()
        {

           // var msgWait = Ext.Msg.wait('Формирование паспорта...', 'Подождите', {interval: 50});
           // alert("YEAR: "+self.attrTypeComboBox.getValue()+" ObjectID: "+self.objectIdForPrint);
            
             //var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
            var msgWait = Ext.Msg.wait('Генерация PDF-документа...', 'Подождите', {interval: 50});
        self.connector.request({
            url: '/objectadmin/card/printNew',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {objectId: this.objectIdForPrint, year:self.attrTypeComboBox.getValue() },
            success: function(response) {
                 // alert("YEAR: "+self.attrTypeComboBox.getValue()+" ObjectID: "+self.objectIdForPrint);
                //msgWait.hide();
                if (response.responseText) {
                   // alert("RESPTEXT: "+response.responseText);
                     this.docWindow = new Ext.WgsShimWindow({
            title : 'Печать',
            closable : true,
            width : 550,
            height : 600,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'+response.responseText+'"></param><EMBED src="/objectadmin/card/getpdf/'+response.responseText+'" width="100%" height="100%" href="/objectadmin/card/getpdf/'+response.responseText+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
        
                }
                
                    self.close();
                    msgWait.hide();
            }
        });
            /*
            new Ext.data.WgsConnection().request(
            {
                url: '/objectadmin/card/CreateChildObject',
                method: 'POST',
                params:
                {
                    objectTypeId: self.attrTypeComboBox.getValue(),
                    parentObjectId: self.parentObjectId,
                    objTypeRelationId: self.storeJS.getAt(self.storeJS.find('id',self.attrTypeComboBox.getValue())).data.objTypeRelationId
                },
                success: function(action)
                {
                    var response = Ext.decode(action.responseText);
                    if (response)
                    {
                        Map.Card.Create(this.mapViewer).open({
                            objectId: response.childObjectId,
                            callback: function() {
                                self.owner.storeJS.reload();
                                self.owner.owner.owner.reloadCardStore();
                            },
                            scope: this
                        });
                    }
                    msgWait.hide();
                    self.close();
                }
                
            });
            */
            
            
        });
        this.on('cancel', function()
        {
            self.close();
        });

        Map.Card.ChoiceAttrTypeRelationWindowNew.superclass.initComponent.apply(this, arguments);
    }
});


Ext.extend(Map.Card.ChoiceAttrTypeRelationWindow, Ext.Window,{
    initComponent : function()
    {
        this.addEvents(
        {
            save : true,
            cancel : true
        });

        var self = this;

        self.attrTypeComboBox.on('select',function()
        {
            self.saveButton.enable();
        });

        this.on('save', function()
        {

            var msgWait = Ext.Msg.wait('Создание карточки...', 'Подождите', {interval: 50});
            new Ext.data.WgsConnection().request(
            {
                url: '/objectadmin/card/CreateChildObject',
                method: 'POST',
                params:
                {
                    objectTypeId: self.attrTypeComboBox.getValue(),
                    parentObjectId: self.parentObjectId,
                    objTypeRelationId: self.storeJS.getAt(self.storeJS.find('id',self.attrTypeComboBox.getValue())).data.objTypeRelationId
                },
                success: function(action)
                {
                    var response = Ext.decode(action.responseText);
                    if (response)
                    {
                        Map.Card.Create(this.mapViewer).open({
                            objectId: response.childObjectId,
                            callback: function() {
                                self.owner.storeJS.reload();
                                self.owner.owner.owner.reloadCardStore();
                            },
                            scope: this
                        });
                    }
                    msgWait.hide();
                    self.close();
                }
            });
        });
        this.on('cancel', function()
        {
            self.close();
        });

        Map.Card.ChoiceAttrTypeRelationWindow.superclass.initComponent.apply(this, arguments);
    }
});


//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-ПАНЕЛЬ ДОЧЕРНИХ КАРТОЧЕК ДЛЯ ДАННОЙ КАРТОЧКИ-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.Card.CildCardAttrCardPanel = function(owner, object_id, mapViewer)
{
    this.owner = owner;
    this.object_id = object_id;
    this.mapViewer = mapViewer;
    //this.loadMask = loadMask;
    var self = this;

    self.setObjectId = function(_Object_id)
    {
        self.object_id = _Object_id;
        self.storeJS.baseParams.objectId = _Object_id;
        self.storeJS.reload();
    };

    this.storeJS = new Ext.data.JsonStore(
    {
        autoLoad: true,
        fields: ['relationId', 'name','relationTypeId','countTypeRelation'],
        url: '/objectadmin/card/GetListRelByObject',
        baseParams:{objectId: self.object_id}
    });

    Map.Card.CildCardAttrCardPanel.superclass.constructor.call(this,
    {
        autoScroll: true,
        closable: false
    });
}
Ext.extend(Map.Card.CildCardAttrCardPanel,Ext.Panel,
{
    initComponent : function()
    {
        var self = this;
        self.storeJS.on('load',  function()
        {
            self.storeJS.each(function(record)
            {
                this.relationFieldSet = new Ext.form.FieldSet(
                    {
                        title: 'Связь: '+record.data.name,
                        height:170,
                        collapsible : true,
                        layout: 'fit',
                        anchor: '100%',
                        items:[
                           this.relationCardPanel = new Map.Card.RelationCardPanel(self, record.data.relationId,self.object_id,record.data.relationTypeId, self.mapViewer)
                        ]
                   });
                   self.add(this.relationFieldSet);
            },this);
            self.owner.controlReadOnly();
            self.owner.doLayout();
        },this);
        Map.Card.CildCardAttrCardPanel.superclass.initComponent.apply(this, arguments);

    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ПАНЕЛЬ ДОПОЛНИТЕЛЬНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.Card.AdvancedParamAttrCardPanel = function(owner, object_id)
{
    this.owner = owner;
    this.object_id = object_id;

    var self = this;

    this.storeJS = new Ext.data.JsonStore(
    {
        autoLoad: true,
        fields: ['name', 'value','vulueTypeId','groupId','groupName','id'],
        url: '/objectadmin/card/GetValueProperties',
        baseParams:{objectId: self.object_id}
    });

    self.setObjectId = function(_Object_id)
    {
        self.object_id = _Object_id;
        self.storeJS.baseParams.objectId = _Object_id;
        self.storeJS.reload();
    };

    Map.Card.AdvancedParamAttrCardPanel.superclass.constructor.call(this,
    {
        closable: false
    });
}
Ext.extend(Map.Card.AdvancedParamAttrCardPanel,Ext.Panel,
{
    initComponent : function()
    {
        var self = this;
        self.storeJS.on('load',  function()
        {
            self.domainStores = [];
            var group_id = -1;
            var groupFieldSet;
            self.storeJS.each(function(record)
            {
                if (group_id != record.data.groupId)
                {
                    if (groupFieldSet)
                    {
                        self.add(groupFieldSet);
                    }
                    var groupId = record.data.groupId;

                    var cExpanded = WGS.Cookie.get('propGroupExpanded')||"",
                        expandedGroups = cExpanded.length>0? (cExpanded.indexOf(",") != -1? cExpanded.split(","): [cExpanded]): [];

                    var expDate = new Date(),
                        days = 180;

                    expDate.setTime(expDate.getTime()+(days*24*60*60*1000));

                    groupFieldSet = new Ext.form.FieldSet(
                    {
                        title: 'Группа характеристик: '+record.data.groupName,
                        autoHeight:true,
                        collapsible : true,
                        closable: false,
                        anchor: '97%',
                        collapsed: !expandedGroups.inArray(groupId)
                    });
                    groupFieldSet.on('collapse', function(){
                        var cookieExpanded = WGS.Cookie.get('propGroupExpanded')||"",
                            expanded = cookieExpanded.length>0? (cookieExpanded.indexOf(",") != -1? cookieExpanded.split(","): [cookieExpanded]): [],
                            cleanExpanded = [];
                        for (var i=0; i<expanded.length; i++) {
                            if (expanded[i] != groupId) {
                                cleanExpanded.push(expanded[i]);
                            }
                        }
                        WGS.Cookie.set('propGroupExpanded', cleanExpanded, expDate);
                    });
                    groupFieldSet.on('expand', function(){
                        var cookieExpanded = WGS.Cookie.get('propGroupExpanded')||"",
                            expanded = cookieExpanded.length>0? (cookieExpanded.indexOf(",") != -1? cookieExpanded.split(","): [cookieExpanded]): [];
                        expanded.push(groupId);
                        WGS.Cookie.set('propGroupExpanded', expanded, expDate);
                    });
                    groupFieldSet.labelWidth = 300;
                    if (group_id == -1 && !expandedGroups.length) {groupFieldSet.collapsed = false}
                    group_id = record.data.groupId;
                }
                if (record.data.vulueTypeId == 1)
                {
                    var propertyEditor = new Ext.form.TextField(
                    {
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 2)
                {

                    var propertyEditor = new Ext.form.DateField(
                    {
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        format: 'Y-m-d'
                    });

                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 3)
                {
                    var propertyEditor = new Ext.form.NumberField(
                    {
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        allowDecimals: false,
                        maxText : 10
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 4)
                {
                    var propertyEditor = new Ext.form.NumberField(
                    {
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value,
                        allowDecimals: true,
                        maxText : 10,
                        decimalSeparator:','
                    });
                    groupFieldSet.add(propertyEditor);
                }
                else if (record.data.vulueTypeId == 5)
                {
                    self.domainStores[self.domainStores.length] = new Ext.data.JsonStore(
                    {
                        fields: ['id', 'name'],
                        url: '/objectadmin/card/GetValueDomainList',
                        baseParams:{propId: record.data.id}
                    });
                    self.domainStores[self.domainStores.length-1].load(
                    {
                        callback : function()
                        {
                            propertyEditor.setValue(record.data.value);
                            propertyHidden.setValue(propertyEditor.getValue());
                        }
                    });
                    var propertyHidden = new Ext.form.TextField(
                    {
                        name: 'property-' + record.data.id,
                        hideLabel: true,
                        hidden: true,
                        labelSeparator:'',
                        value: record.data.value
                    });
                    var propertyEditor = new Ext.form.WgsComboBox(
                    {
                        fieldLabel: record.data.name,
                        name: 'grid-' + record.data.id,
                        store: self.domainStores[self.domainStores.length-1],
                        anchor:'97%',
                        displayField:'name',
                        valueField: 'id',
                        typeAhead: true,
                        loadingText:'Список загружается...',
                        triggerAction: 'all',
                        emptyText:'Выберите значение...',
                        selectOnFocus:true
                    });
                    propertyEditor.on('select',function(combo, record, index )
                    {
                        //alert(propertyEditor.getValue());
                        propertyHidden.setValue(propertyEditor.getValue());
                    });

                    /*if(!WGS.PRIVILEGE.CARD_EDIT)
                        Ext.apply(propertyEditor, {hideTrigger: true});//self.setReadOnly(true, false);
                    else
                        Ext.apply(propertyEditor, {hideTrigger: false});//self.setReadOnly(false, true);*/

                    groupFieldSet.add(propertyEditor);
                    groupFieldSet.add(propertyHidden);
                }
                else if (record.data.vulueTypeId == 7)
                {
                    var propertyEditor = new Ext.form.TextArea(
                    {
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank:true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }
            });
            
            
            if (groupFieldSet)
            {
                self.add(groupFieldSet);
                self.owner.doLayout();
            }
            self.owner.controlReadOnly();
        },this);

        Map.Card.AdvancedParamAttrCardPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-ПАНЕЛЬ ОСНОВНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.BaseParamAttrCardPanel = function(owner, object_id)
{
    this.owner = owner;
    this.object_id = object_id;

    var self = this;

    this.objectIdHidden = new Ext.form.Hidden({
         name: 'id',
         hidden: true,
         labelSeparator:''
    });

    self.setObjectId = function(_Object_id)
    {
        self.object_id = _Object_id;
        this.objectIdHidden.setValue(_Object_id);
        owner.getForm().setValues(
        {
            id: _Object_id
        });
    };

    Map.Card.BaseParamAttrCardPanel.superclass.constructor.call(this,
    {
        title: 'Основные характеристики',
        layout: 'form',
        autoHeight:true,
        autoWidth:true,
        defaultType: 'textfield',
        closable: false,
        items: [
        {
            fieldLabel: 'Наименование',
            name: 'name',
            anchor: '97%',
            allowBlank:true
        },{
            fieldLabel: 'Отчетный год',
            name: 'shortName',
            anchor: '97%',
            allowBlank:true
        },
        {
            fieldLabel: 'Атрибутивное описание',
            name: 'typeName',
            readOnly : true,
            anchor:'97%',
            allowBlank:false
        },

        new Ext.form.DateField(
        {
            fieldLabel: 'Дата изменения',
            name: 'createDate',
            anchor: '97%',
            format: 'Y-m-d H:i:s',//2010-04-05 12:24:25
            readOnly : true,
            allowBlank:false
        }),
        {
            fieldLabel: 'Автор',
            name: 'edit_user',
            readOnly : true,
            anchor:'97%',
            allowBlank: true
        },
        new Ext.form.TextArea(
        {
            fieldLabel: 'Примечание',
            name: 'note',
            anchor: '97%',
            allowBlank:true
        }),
            this.objectIdHidden
        ]
    });
}
Ext.extend(Map.Card.BaseParamAttrCardPanel,Ext.form.FieldSet,{});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ОСНОВНАЯ ПАНЕЛЬ КАРТОЧКИ (АТРИБУТИВНЫХ ОБЪЕКТОВ)-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.AttrCardFormPanel = Ext.extend(Ext.form.WgsFormPanel, {
    constructor: function(owner, object_id, mapViewer, card)
    {
        this.owner = owner;
        this.object_id = object_id;

        var self = this;

        if (object_id == null)
        {
            self.advancedParamAttrCardPanel = new Map.Card.AdvancedParamAttrCardPanel(self, -1);
            self.cildCardAttrCardPanel = new Map.Card.CildCardAttrCardPanel(self, -1, mapViewer);
        }
        else
        {
            self.advancedParamAttrCardPanel = new Map.Card.AdvancedParamAttrCardPanel(self, self.object_id);
            self.cildCardAttrCardPanel = new Map.Card.CildCardAttrCardPanel(self, self.object_id, mapViewer);
        }

        self.baseParamAttrCardPanel = new Map.Card.BaseParamAttrCardPanel(self, self.object_id);

        self.setObjectId = function(_Object_id)
        {
            self.baseParamAttrCardPanel.setObjectId(_Object_id);
            self.advancedParamAttrCardPanel.setObjectId(_Object_id);
            self.cildCardAttrCardPanel.setObjectId(_Object_id);
        };

        self.setReadOnlyCreateDateField = function ()
        {
            if(this.object_id) {
                self.setReadonlyPrivate(self.getForm().findField('createDate'), true);
            }
        }

        self.saveCard = function(config)
        {
            var msgWait = Ext.Msg.wait('Идет сохранение параметров...', 'Подождите', {interval: 50});
            if (object_id != null)
            {
                self.url = '/objectadmin/card/SetCard';
            }
            else
            {
                self.url = '/objectadmin/card/AddCard';
            }
            self.submit(
            {
                success : function(form, action)
                {

                    if (self.owner) {
                        if (self.owner.owner) {
                            self.owner.owner.storeJS.reload();
                        }
                    }

                    msgWait.hide();
                    if (config) {
                        config.callback.call(config.scope);
                    }
                }
            })
        };

        self.reloadCardStore = function()
        {
            card.reloadCardsStore();
        };

        self.controlReadOnly = function()
        {
            if(!WGS.PRIVILEGE.CARD_EDIT)
                self.setReadOnly(true, false);
        };

        self.setReadonlyPrivate = function(f, bReadOnly)
        {
            if(typeof f._readOnly == 'undefined')
            {
                if(f.initEvents)
                    f.initEvents = f.initEvents.createSequence(self.initEventsSequence)
            }
            f._readOnly = bReadOnly;
            f.getEl().dom.readOnly = bReadOnly;
            if (f instanceof Ext.form.TriggerField)
            {
                if (bReadOnly){
                    f.trigger.un('click', f.onTriggerClick, f);
                    if(f.keyNav)
                        f.keyNav.disable();
                }
                else {
                    f.trigger.on('click', f.onTriggerClick, f, {preventDefault:true});
                    if(f.keyNav)
                        f.keyNav.enable();
                }
            }
        };

        self.initEventsSequence = function(){
            if(this._readOnly == true){
                if(this.keyNav)
                    this.keyNav.disable();
            }
            else{
                 if(this.keyNav)
                    this.keyNav.enable();
            }
        };

        self.setReadOnly = function(readOnly, buttonVisible)
        {
            self.getForm().items.each(function(f){
                self.setReadonlyPrivate(f, readOnly);
            });
            if(self.advancedParamAttrCardPanel)
                if(self.advancedParamAttrCardPanel.items)
                    self.advancedParamAttrCardPanel.items.each(function(fieldset){
                        if(fieldset.items)
                            fieldset.items.each(function(field){
                                self.setReadonlyPrivate(field, readOnly);
                            });
                    });
            if(self.cildCardAttrCardPanel)
                if(self.cildCardAttrCardPanel.items)
                    self.cildCardAttrCardPanel.items.each(function(fieldset){
                        if(fieldset.items);
                            fieldset.items.each(function(field){
                                if(readOnly)
                                    if(field.getTopToolbar()) {
                                        var toolbar = field.getTopToolbar();
                                        if(Ext.isArray(toolbar)) {
                                            toolbar[0].setVisible(false);
                                            toolbar[1].setVisible(false);
                                            toolbar[2].setVisible(false);
                                            toolbar[3].setVisible(false);
                                        }
                                    }
                            });
                        });
            owner.saveButton.setVisible(buttonVisible);
            //owner.cancelButton.setVisible(buttonVisible);
        };

        Map.Card.AttrCardFormPanel.superclass.constructor.call(this,
        {
            frame:true,
            url:'/objectadmin/card/SetCard',
            layout:'fit',
            autoWidth: true,
            //height: 400,
            closable: false,
            autoScroll: true,
            bodyStyle:'padding:5px 5px 0',
            items: [self.baseParamAttrCardPanel,self.advancedParamAttrCardPanel,self.cildCardAttrCardPanel]
        });
    }
});
//Ext.extend(Map.Card.AttrCardFormPanel,Ext.form.WgsFormPanel,{});

Map.Card.CardPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function()
    {
        this.cardsStore = new Ext.data.WgsJsonStore({
            fields: ['cardId', 'cardName', 'cardIsCurrent', 'cardTypeName', 'cardRelationName','cardLabel']
        });

        this.childCardsStore = new Ext.data.WgsJsonStore({
            url: '/objectadmin/card/getchildobjectsbyparent',
            fields: ['cardId', 'cardName', 'cardTypeName', 'cardRelationName']
        });

        this.cardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable: false,
            store : this.cardsStore,
            mode: 'local'
        });

        this.childCardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable: false,
            store : this.childCardsStore,
            mode: 'local'
        });

        this.basePropertiesReader = new Ext.data.JsonReader({},[
           {name: 'name'},
           {name: 'shortName'},
           {name: 'typeName'},
           {name: 'edit_user'},
           {name: 'createDate'},
           {name: 'note'}
        ]);

        this.saveButton = new Ext.Button({
            text : 'Сохранить'
        });

        this.cancelButton = new Ext.Button({
            text : 'Закрыть',
            scope : this
        });

        this.attrCardPanel = new Ext.Panel({
            region: 'south',
            height: 530,
            autoWidth: true,
            layout: 'fit'
        });

        this.listCardFieldSet = new Ext.form.FieldSet({
            title: 'Список описаний',
            autoHeight: true,
            items: this.cardCombobox
        });

        this.listChildCardFieldSet = new Ext.form.FieldSet({
            title: 'Список привязанных описаний',
            autoHeight: true,
            hidden: true,
            items: this.childCardCombobox
        });

        Map.Card.CardPanel.superclass.constructor.call(this, {
            width: 600,
            autoHeight: true,
            layout: 'border',
            frame: true,
            maskDisabled: false,
            collapsible: false,
            items: [{
                region: 'center',
                autoHeight: true,
                layout: 'fit',
                items:  [ this.listCardFieldSet, this.listChildCardFieldSet]
            },
            this.attrCardPanel]
        });

        this.featuredocButton = this.addButton({
            iconCls: 'featuredoc',
            text : 'Документы'
        });

        this.printButton = this.addButton({
            iconCls: 'icon-print',
            text : 'Печать'
        });
        /*
        this.printButton = new Ext.Button(
    {
          text : 'Создать',
          handler : function()
          {
               this.fireEvent('NewChildCard')
          },
          scope : this
     });
*/
        this.saveButton = this.addButton({
            iconCls: 'icon-accept-tick',
            text : 'Применить'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Закрыть',
            scope : this
        });
    }
});
