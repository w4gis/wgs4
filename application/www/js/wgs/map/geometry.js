﻿/**
* Geometry.js
*
* @changedby  $Author: oss $
* @version    SVN: $Id: geometry.js 649 2009-10-21 11:09:30Z oss $
* @revision   SVN: $Revision: 649 $
* @link       $HeadURL: https://svn.gis.tusur.ru/wgs3/trunk/application/www/js/wgs/map/geometry.js $
* @date       $Date: 2009-10-21 18:09:30 +0700 (РЎСЂ, 21 РѕРєС‚ 2009) $
*/

Ext.namespace('Map');

Map.Geometry = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;
    this.viewerLib = mapViewer.Lib;
    this.panel = new GeometryPanel({
        width: 350,
        height: 400,
        viewerApi: this.viewerApi,
        viewerLib: this.viewerLib
    });

    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        border : false,
        resizable : true,
        maximizable : true,
        layout: 'fit',
        plain : true,
        width: this.panel.width+12,
        title: 'Геометрия объектов'
    });
    //this.window.addTool('maximize');
    this.window.setInnerPanel(this.panel);

}

Map.Geometry.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Geometry(map);
    }
    return this.instances[map.mapFrame];
}

Map.Geometry.prototype = {
    open: function()
    {
        if(this.viewerApi.Map.GetSelectedCount() > 0) {
            this.window.addPanel();
            this.panel.loadGeometryData();
        }
        else
            Ext.WgsMsg.show({
                title: 'Предупреждение',
                msg: 'Необходимо выбрать объекты',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
    }
}

GeometryPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config) {
        if (config){
            Ext.apply(this, config);
        }
        this.conn = new Ext.data.WgsConnection();

        this.loadGeometryData = function()
        {
            var msgWait = Ext.WgsMsg.wait('Идет выполнение операции...', 'Подождите', {interval: 50});

            if(this.polygonGroupingStore.getCount() > 0)
            {
               this.polygonGroupingStore.removeAll();
               this.polygonGrid.setDisabled(true);
            }
            if(this.lineGroupingStore.getCount() > 0)
            {
               this.lineGroupingStore.removeAll();
               this.lineGrid.setDisabled(true);
            }
            if(this.pointGroupingStore.getCount() > 0)
            {
               this.pointGroupingStore.removeAll();
               this.pointGrid.setDisabled(true);
            }

            this.conn.request({
                url : '/map/geometry/getselectedfeatures',
                method : 'POST',
                params : {
                'selection'     : this.viewerApi.Map.GetSelectionXML(),
                'mapname'       : this.viewerApi.Map.GetMapName()
                },
                success : function(response)
                    {

                        var records = this.polygonJsonReader.read(response).records;
                       // console.log(response,this.polygonJsonReader.read(response.responseText))
                        Ext.each(records, function (record)
                        {
                            switch(record.data.type)
                            {
                                case 'Polygon':
                                    this.polygonGroupingStore.add(record);
                                    break;
                                case 'Line':
                                    this.lineGroupingStore.add(record);
                                    break;
                                case 'Point':
                                    this.pointGroupingStore.add(record);
                                    break;
                            }
                        }, this);

                        if(this.polygonGroupingStore.getCount() != 0)
                        {
                           this.tabPanel.add(this.polygonGrid);
                           this.polygonGrid.setDisabled(false);
                           this.tabPanel.setActiveTab(this.polygonGrid);
                        }
                        if(this.lineGroupingStore.getCount() != 0)
                        {
                            this.tabPanel.add(this.lineGrid);
                            this.lineGrid.setDisabled(false);
                            this.tabPanel.setActiveTab(this.lineGrid);
                        }
                        if(this.pointGroupingStore.getCount() != 0)
                        {
                            this.tabPanel.add(this.pointGrid);
                            this.pointGrid.setDisabled(false);
                            this.tabPanel.setActiveTab(this.pointGrid);
                        }



                        msgWait.hide();
                        this.getParent().show();
                    },
                    scope: this
            });
        }

        this.getSelectionObjects = function(selModel)
        {
            var result = [];
            var records = selModel.getSelections();
            records.each(function(record)
            {
                result[result.length] = record.data.id;
            });
            return result;
        }
//---Элементы
        this.polygonJsonReader = new Ext.data.JsonReader({},[
           {name: 'id'},
           {name: 'layer'},
           {name: 'perimeter'},
           {name: 'area'},
           {name: 'length'},
           {name: 'coords'},
           {name: 'type'},
           {name: 'X'},
           {name: 'Y'},
           {name: 'label'}
        ]);
        this.polygonGroupingStore = new Ext.data.GroupingStore({
            reader: this.polygonJsonReader,
            sortInfo:{field: 'id', direction: "ASC"},
            groupField:'layer'
        });
        this.lineJsonReader = new Ext.data.JsonReader({},[
           {name: 'id'},
           {name: 'layer'},
           {name: 'length'},
           {name: 'coords'},
           {name: 'type'},
           {name: 'label'}
        ]);

        this.lineGroupingStore = new Ext.data.GroupingStore({
            reader: self.lineJsonReader,
            sortInfo:{field: 'id', direction: "ASC"},
            groupField:'layer'
        });

        this.pointJsonReader = new Ext.data.JsonReader({},[
           {name: 'id'},
           {name: 'layer'},
           {name: 'type'},
           {name: 'X'},
           {name: 'Y'},
           {name: 'label'}
        ]);

        this.pointGroupingStore = new Ext.data.GroupingStore({
            reader: self.pointJsonReader,
            sortInfo:{field: 'id', direction: "ASC"},
            groupField:'layer'
        });

        this.okButton = new Ext.Button({
            text : 'ОК',
            handler : function() {
                this.fireEvent('ok')
            },
            scope : this
        });

        this.polygonSelectionModel = new Ext.grid.RowSelectionModel(
        {
            listeners:
            {
                rowselect : function( selModel, rowIndex, record )
                {
                    this.viewerLib.selectFeatures(this.getSelectionObjects(selModel));
                },
                scope: this
            }
        });

        this.lineSelectionModel = new Ext.grid.RowSelectionModel(
        {
            listeners:
            {
                rowselect : function( selModel, rowIndex, record )
                {
                    this.viewerLib.selectFeatures(this.getSelectionObjects(selModel));
                },
                scope: this
            }
        });

        this.pointSelectionModel = new Ext.grid.RowSelectionModel(
        {
            listeners:
            {
                rowselect : function( selModel, rowIndex, record )
                {
                    this.viewerLib.selectFeatures(this.getSelectionObjects(selModel));
                },
                scope: this
            }
        });

        Ext.grid.GroupSummary.Calculations['total'] = function(v, record, field){
            return Math.round((v + (record.data[field]||0))*100)/100;
        }

        this.polygonSummary = new Ext.grid.GroupSummary();
        this.lineSummary = new Ext.grid.GroupSummary();

        this.polygonRowExpander = new Ext.grid.RowExpander({
            tpl : new Ext.Template(
                '<p><b>Координаты:</b> {coords}</p><br>'
                )
        });

        this.lineRowExpander = new Ext.grid.RowExpander({
            tpl : new Ext.Template(
                '<p><b>Координаты:</b> {coords}</p><br>'
                )
        });

//----Гриды с разными типами геометрии
        this.polygonGrid = new Ext.grid.GridPanel({
            title: 'Полигон',
            store : this.polygonGroupingStore,
            cm : new Ext.grid.ColumnModel([
                this.polygonRowExpander,
                {id: 'id',          header: 'Наименование', hideable: false, sortable: false, dataIndex: 'label', summaryRenderer: function(){return 'Общее : ';}},
                {id: 'layer',       hidden: true, header: 'Слой',    hideable: false, sortable: false, dataIndex: 'layer', align: 'center'},
                {id: 'perimeter',   header: 'Периметр (м)', hideable: false, dataIndex: 'perimeter', summaryType:'total'},
                {id: 'area',        header: 'Площадь (м<sup>2</sup>)', hideable: false, dataIndex: 'area', summaryType:'total'}
                ]),
            enableHdMenu: false,
            sm : this.polygonSelectionModel,
            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{group}',
                enableGroupingMenu: false
            }),
            plugins:[ this.polygonSummary, this.polygonRowExpander ],
            autoScroll : true,
            stripeRows: true
        });

        this.lineGrid = new Ext.grid.GridPanel({
            title: 'Линия',
            store : this.lineGroupingStore,
            cm : new Ext.grid.ColumnModel([
                this.lineRowExpander,
                {id: 'id',          header: 'Наименование', hideable: false, sortable: false, dataIndex: 'label', summaryRenderer: function(){return 'Общая длина : ';}},
                {id: 'layer',       hidden: true, header: 'Слой',    hideable: false, sortable: false, dataIndex: 'layer', align: 'center'},
                {id: 'length',      header: 'Длина (м)', hideable: false, dataIndex: 'length', summaryType:'total'}
            ]),
            enableHdMenu: false,
            sm : this.lineSelectionModel,
            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({["Число объектов : "]}{[values.rs.length]})'//,
            }),
            plugins: [this.lineSummary, this.lineRowExpander],
            autoScroll : true,
            stripeRows: true
        });

        this.pointGrid = new Ext.grid.GridPanel({
            title: 'Точка',
            store : this.pointGroupingStore,
            cm : new Ext.grid.ColumnModel([
                {id: 'id',     header: 'Наименование', hideable: false, sortable: false, dataIndex: 'label'},
                {id: 'layer',  hidden: true, header: 'Слой',    hideable: false, sortable: false, dataIndex: 'layer', align: 'center'},
                {id: 'X',      header: 'Координата X', hideable: false, dataIndex: 'X'},
                {id: 'Y',      header: 'Координата Y', hideable: false, dataIndex: 'Y'}
            ]),
            enableHdMenu: false,
            sm : this.pointSelectionModel,
            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({["Число объектов : "]}{[values.rs.length]})'//,
            }),
            autoScroll : true,
            stripeRows: true
        });

        this.tabPanel = new Ext.TabPanel({
            activeTab: 0,
            defaults:{autoScroll: true}
        });

        GeometryPanel.superclass.constructor.call(this, {
            header: true,
            layout: 'fit',
            items: [ this.tabPanel ]
        });

    },
    initComponent: function()
    {
        this.addEvents({
            ok : true
        });

        this.on('ok', function()
        {

        });
        GeometryPanel.superclass.initComponent.apply(this, arguments);
    }
});
