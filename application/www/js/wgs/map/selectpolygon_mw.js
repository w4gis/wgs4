﻿Ext.namespace('Map');

Map.SelectPolygon = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.viewerApi = mapViewer.Api;
    this.viewerLib = mapViewer.Lib;
    this.conn = new Ext.data.WgsConnection();
    this.map = this.viewerApi.map;
    this.mouseButton = null;
    this.isMouseDown = false;
    this.raphael = null;

    this.coords = [];
    this.isPossiblePolygon = true;
    this.appending = false;
}

Map.SelectPolygon.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.SelectPolygon(map);
    }
    return this.instances[map.mapFrame];
}

Map.SelectPolygon.prototype = {
    open: function(config)
    {
        this.map.document.body.onselectstart = function()
        {
            return false;
        };

        this.raphael = this.mapViewer.Lib.getRaphaelDrawing();
        this.raphael.createCanvas();
        this.setDefaultParams();

        this.viewerApi.Map.ObserveOnMapLoaded(function () {
            this.setDefaultParams();
        }, this);

        this.viewerApi.Map.OnContextMenu(function(e){
           return false;
        },this);

        this.viewerApi.Map.OnMouseDown(function(e){
            this.mouseButton = (e.button) ? e.button : e.which;
            if (this.mouseButton == 1) {
                var x = e.clientX - this.map.mapPosX;
                var y = e.clientY;
                this.drawPolygon(x, y);
                this.isMouseDown = true;
            }   else {
                this.setDefaultParams();
            }
            this.appending = e.shiftKey;
            this.map.appending = this.appending;
        },this);

        this.viewerApi.Map.OnMouseMove(function(e){
            if(!this.isMouseDown) {
                var x = e.clientX - this.map.mapPosX;
                var y = e.clientY;
                this.drawTemporaryPolygon(x, y);
            } else {
                return false;
            }
        },this);

        this.viewerApi.Map.OnMouseUp(function(e){
            this.isMouseDown = false;
        },this);

        this.viewerApi.Map.OnDblClick(function(e){
            switch (e.type) {
                case "click":
                    break;
                case "dblclick":
                    this.isMouseDown = false;
                    var x = e.clientX - this.map.mapPosX;
                    var y = e.clientY;
                    this.finishDrawPolygon(x, y, config);
                    break;
                default:
            }
        },this);

        WGS.Tools.addHandler(this.map.document, 'keydown', this.onKeyDown.createDelegate(this), this);
    },

    onKeyDown: function(e)
    {
        if (e.keyCode == 27) {
           this.setDefaultParams();
        }
    },

    drawPolygon: function(x, y)
    {
	    if(this.isPossiblePolygon) {
            var index = this.coords.length;
            if (index != 0) {
                if(this.coords[index-1].x !=x && this.coords[index-1].y !=y) {
                    this.coords[index] = new Object();
                    this.raphael.drawLine(this.coords[index-1].x, this.coords[index-1].y, x, y, {stroke: "#036"});
                    this.coords[index].x = x;
                    this.coords[index].y = y;
                }
            } else {
				this.setDefaultParams(true)
                this.coords[index] = new Object();
                this.coords[index].x = x;
                this.coords[index].y = y;
            }
            this.isMouseDown = true;
        }
    },

    drawTemporaryPolygon: function(x, y)
    {
        var index = this.coords.length;
        if (index > 1) {
            if (this.coords[index-1].x != x && this.coords[index-1].y != y) {
                var mass = [];
                for (var i = 0; i < this.coords.length; i++) {
                    mass[i] = new Object();
                    mass[i].x = this.coords[i].x;
                    mass[i].y = this.coords[i].y;
                }
                var k = mass.length;
                mass[k] = new Object();
                mass[k].x = x;
                mass[k].y = y;
                if (this.isCrossSides(mass)) {
                    this.isPossiblePolygon = false;
                    this.raphael.drawTemporaryLine1(this.coords[index-1].x, this.coords[index-1].y, x, y, {stroke: "#ff3366"});
                    this.raphael.drawTemporaryLine2(x, y, this.coords[0].x, this.coords[0].y, {stroke: "#ff3366"});
                } else {
                    this.isPossiblePolygon = true;
                    this.raphael.drawTemporaryLine1(this.coords[index-1].x, this.coords[index-1].y, x, y, {stroke: "#037"});
                    this.raphael.drawTemporaryLine2(x, y, this.coords[0].x, this.coords[0].y, {stroke: "#037"});
                }
            }
        }
        if (index == 1) {
            this.raphael.drawTemporaryLine1(this.coords[index-1].x, this.coords[index-1].y, x, y, {stroke: "#037"});
        }
    },

    finishDrawPolygon: function(x, y, config)
    {
        if(this.isPossiblePolygon) {
            index = this.coords.length;
            if (index > 2) {
                    this.raphael.drawLine(this.coords[index-1].x, this.coords[index-1].y, x, y, {stroke: "#036"});
                    this.raphael.drawLine(x, y, this.coords[0].x, this.coords[0].y, {stroke: "#036"});
                    this.raphael.drawCircle(x, y, 3, {fill: "#00f", stroke: "#000", "fill-opacity": 0.1});
                    this.coords[index] = new Object();
                    this.coords[index].x = x;
                    this.coords[index].y = y;
                    if(config) {
                        if (!config.selectFeaturesOnMap) {
                            this.requestObjectsPolygonSelection(this.coords, config.callback||Ext.emptyFn, config.scope||{});
                        } else {
                            this.requestPolygonSelection(this.coords);
                        }
                    } else {
                        this.requestPolygonSelection(this.coords);
                    }
            }
        }
    },

    isCrossLine: function(x1L1, y1L1, x2L1, y2L1, x1L2, y1L2, x2L2, y2L2)
    {
        var result = false;
        var resultL1 = false;
        var resultL2 = false;
        var temp = 0;

        if(x1L1 > x2L1) {
            temp = x2L1;
            x2L1 = x1L1;
            x1L1 = temp;
            temp = y2L1;
            y2L1 = y1L1;
            y1L1 = temp;
        }
        if(x1L2 > x2L2) {
            temp = x2L2;
            x2L2 = x1L2;
            x1L2 = temp;
            temp = y2L2;
            y2L2 = y1L2;
            y1L2 = temp;
        }

        var a1 = y2L1 - y1L1;
        var b1 = x1L1 - x2L1;
        var c1 = -(a1*x1L1 + b1*y1L1);

        var a2 = y2L2 - y1L2;
        var b2 = x1L2 - x2L2;
        var c2 = -(a2*x1L2 + b2*y1L2);

        var x = (b1*c2-b2*c1)/(a1*b2-a2*b1);
        var y = (c1*a2-c2*a1)/(a1*b2-a2*b1);

        if ((x >= x1L1)&&(x <= x2L1)&&(x >= x1L2)&&(x <= x2L2)) {
            if  (y1L1 <= y2L1) {
                if ((y >= y1L1) && (y <= y2L1)) {
                    resultL1 = true;
                }
            } else {
                if (y2L1 <= y1L1) {
                    if ((y >= y2L1) && (y <= y1L1)) {
                        resultL1 = true;
                    }
                }
            }
            if  (y1L2 <= y2L2) {
                if ((y >= y1L2) && (y <= y2L2)) {
                    resultL2 = true;
                }
            } else {
                if (y2L2 <= y1L2) {
                    if ((y >= y2L2) && (y <= y1L2)) {
                        resultL2 = true;
                    }
                }
            }
        }

        if (resultL1 && resultL2) {
            result = true;
        }
        return result;
    },

    isCrossSides: function(sides) {
        var n = sides.length;
        sides[n] = new Object();
        sides[n].x = sides[0].x;
        sides[n].y = sides[0].y;
        var str = '';
        if (n > 3) {
            for (var i = 0; i < n; i++) {
                for (var j = 0; j < n; j++) {
                    if ( i!=j && j > i+1 ) {
                        if (!(i == 0 && j == n-1)) {
                            var g = i+1;
                            var q = j+1;

                            if (this.isCrossLine(sides[i].x, sides[i].y, sides[g].x, sides[g].y, sides[j].x, sides[j].y, sides[q].x, sides[q].y)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    },

    requestObjectsPolygonSelection : function (coords, callback, scope)
    {
        this.setDefaultParams();
        this.viewerLib.getFeaturesByPolygon(coords, callback, scope)
        //this.viewerLib.getRaphaelDrawing().clear();
        this.viewerLib.getRaphaelDrawing().destroy();
        WGS.Tools.removeHandler(this.viewerApi.Map.getMap().document, 'keydown', null);
        this.viewerApi.Map.StopMouseDown();
        this.viewerApi.Map.StopMouseMove();
        this.viewerApi.Map.StopMouseUp();
        this.viewerApi.Map.StopContextMenu();
    },
	
	    
		/*if (!bAllowOutsideWindow) {
        if (x > mapDevW - 1) {
            x = mapDevW - 1;
        } else if (x < 0) {
            x = 0;
        }
        if (y > mapDevH - 1) {
            y = mapDevH - 1;
        } else if (y < 0) {
            y = 0;
        }
    }
    x = extX1 + (extX2 - extX1) * (x / mapDevW);
    y = extY1 - (extY1 - extY2) * (y / mapDevH);
    */
	drawByStrangePoints: function(coords, reset){
		var map = window.mapViewer.Api.map;
		var reset = reset || false;
		for(var i = 0; i< coords.length; i++){
			var x = Math.ceil((coords[i].x - map.extX1) * map.mapDevW /(map.extX2-map.extX1))
			var y = Math.ceil((coords[i].y - map.extY1) * (-1) * map.mapDevH /(map.extY1-map.extY2))
			this.drawPolygon(x,y)
			//alert('!')
		}
		this.drawPolygon(Math.ceil((coords[0].x - map.extX1) * map.mapDevW /(map.extX2-map.extX1)),Math.ceil((coords[0].y - map.extY1) * (-1) * map.mapDevH /(map.extY1-map.extY2)))
		//if (reset)
		//	this.coords = []			
	},
	
	drawByPoints: function(coords, reset){
		var reset = reset || false;
		for(var i = 0; i< coords.length; i++)
			this.drawPolygon(coords[i].x,coords[i].y)
		this.drawPolygon(coords[0].x,coords[0].y)
		if (reset)
			this.coords = []			
	},
    requestPolygonSelection: function(coords)
    {
        var tempMass = [];
        for (var i = 0; i < coords.length; i++) {
            tempMass[i] = coords[i];
        }

        var mass = [];
        var strCoords = '';
        var strCoordsSdo = '';
        if (coords.length-1 > 2) {
            for (var i = 0; i < coords.length-1; i++) {
                mass[i] = this.map.ScreenToMapUnits(coords[i].x, coords[i].y);
            }
            for (var j = 0; j < mass.length; j++) {
                strCoords = strCoords + mass[j].X +' '+mass[j].Y+',';
				strCoordsSdo = strCoordsSdo + mass[j].X +','+mass[j].Y+','
            }
            strCoords = strCoords + mass[0].X +' '+mass[0].Y;
            strCoordsSdo = strCoordsSdo + mass[0].X +','+mass[0].Y;
            var geom = strCoords;
			
            window.mapState.selectionFtf = 'POLYGON XY(('+strCoords+'))'
			window.mapState.selectionSdo = strCoordsSdo
			
			//alert(window.mapState.selectionFtf)
			window.mapState.selection = geom
			window.mapState.selectionType = 'polygon'
			//console.log(this.coords)
            this.setDefaultParams();
			this.drawByPoints(coords, true)
			window.socket.postMessage(Ext.util.JSON.encode({type:70}));
			/*
			this.conn.request({
                url: '/mapguide/mapagent/mapagent.fcgi',
                defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
                method: 'POST',
                params: {
                    OPERATION: 'QUERYMAPFEATURES',
                    VERSION: '1.0.0',
                    PERSIST: '1',
                    MAPNAME: this.viewerApi.Map.GetMapName(),
                    SESSION: this.viewerApi.Map.GetSessionId(),
                    SEQ: Math.random(),
                    LAYERNAMES: this.map.GetVisSelLayers(),
                    GEOMETRY: geom,
                    SELECTIONVARIANT: 'INTERSECTS',
                    MAXFEATURES: -1
                },
                success: function(response) {
                    this.map.ProcessFeatureInfo(response.responseXML, this.appending, 7);
                },
                scope: this
            });
			*/
        }
    },

    setDefaultParams: function(reset)
    {
		var reset = reset || false;
		if(this.raphael) {
			if(reset)
			this.raphael.clear();
            this.coords = [];
            this.isPossiblePolygon = true;
        }
    }
}
