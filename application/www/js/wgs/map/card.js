﻿Ext.namespace('Map');

Map.RC = Ext.extend(Ext.util.Observable, {

    cSystems: {
        'wgs': 'WGS84',
        'basic': 'Стандартная',
        'pulkovo': 'Пулково-42',
        'galileo': 'Galileo',
        'glonass': 'ГЛОНАСС'
    },

    lastChanged: 'basic',
    lastChangedField: null,
    lastChangedFieldValue: null,

    lib: {
        'wp': (function() {

            var Pi = 3.14159265358979,
                ro = 206264.8062,

                //Эллипсоид Красовского
                aP = 6378245, // Большая полуось
                alP = 1 / 298.3, // Сжатие
                e2P = 2 * alP - Math.pow(alP, 2), // Квадрат эксцентриситета

                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                aW = 6378137, // Большая полуось
                alW = 1 / 298.257223563, // Сжатие
                e2W = 2 * alW - Math.pow(alW, 2), // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2,
                e2 = (e2P + e2W) / 2,
                da = aW - aP,
                de2 = e2W - e2P,

                // Линейные элементы трансформирования, в метрах
                dx = 28, //23.92,
                dy = -130, //-141.27,
                dz = -95, //-80.9,

                // Угловые элементы трансформирования, в секундах
                wx = 0,
                wy = 0,
                wz = 0,

                // Дифференциальное различие масштабов
                ms = 0,

                dB = function(Bd, Ld, H) {
                    var B = 0,
                        L = 0,
                        M = 0,
                        N = 0;
                    B = Bd * Pi / 180;
                    L = Ld * Pi / 180;
                    M = a * (1 - e2) / Math.pow((1 - e2 * Math.pow(Math.sin(B), 2)), 1.5);
                    N = a * Math.pow((1 - e2 * Math.pow(Math.sin(B), 2)), -0.5);
                    return ro / (M + H) * (N / a * e2 * Math.sin(B) * Math.cos(B) * da + (Math.pow(N, 2) / Math.pow(a, 2) + 1) * N * Math.sin(B) * Math.cos(B) * de2 / 2 - (dx * Math.cos(L) + dy * Math.sin(L)) * Math.sin(B) + dz * Math.cos(B)) - wx * Math.sin(L) * (1 + e2 * Math.cos(2 * B)) + wy * Math.cos(L) * (1 + e2 * Math.cos(2 * B)) - ro * ms * e2 * Math.sin(B) * Math.cos(B);
                },

                dL = function(Bd, Ld, H) {
                    var B = 0,
                        L = 0,
                        N = 0;
                    B = Bd * Pi / 180;
                    L = Ld * Pi / 180;
                    N = a * Math.pow((1 - e2 * Math.pow(Math.sin(B), 2)), -0.5);
                    return ro / ((N + H) * Math.cos(B)) * (-dx * Math.sin(L) + dy * Math.cos(L)) + Math.tan(B) * (1 - e2) * (wx * Math.cos(L) + wy * Math.sin(L)) - wz;
                },

                WGS84Alt = function(Bd, Ld, H) {
                    var B = 0,
                        L = 0,
                        N = 0,
                        dH = 0;
                    B = Bd * Pi / 180;
                    L = Ld * Pi / 180;
                    N = a * Math.pow((1 - e2 * Math.pow(Math.sin(B), 2)), -0.5);
                    var dH = -a / N * da + N * Math.pow(Math.sin(B), 2) * de2 / 2 + (dx * Math.cos(L) + dy * Math.sin(L)) * Math.cos(B) + dz * Math.sin(B) - N * e2 * Math.sin(B) * Math.cos(B) * (wx / ro * Math.sin(L) - wy / ro * Math.cos(L)) + (Math.pow(a, 2) / N + H) * ms;
                    return H + dH;
                };

            return {
                'w2p': function(coords) {
                    return {
                        x: coords.x - dL(coords.y, coords.x, coords.z) / 3600,
                        y: coords.y - dB(coords.y, coords.x, coords.z) / 3600,
                        z: WGS84Alt(coords.y, coords.x, coords.z)
                    }
                },
                'p2w': function(coords) {
                    //console.log(coords.y, coords.x, coords.z)
                    return {
                        x: coords.y + dL(coords.y, coords.x, coords.z) / 3600,
                        y: coords.x + dB(coords.y, coords.x, coords.z) / 3600,
                        z: WGS84Alt(coords.y, coords.x, coords.z)
                    }
                }
            }
        })()
    },

    convert: {
        'wgs': {
            /*'basic': function(coords) {

				},*/
            'pulkovo': function(coords) {
                this.doSetCoords('pulkovo', this.lib['wp']['w2p'](coords));
            }
            /*,
			'galileo': function(coords) {

				},
			'glonass': function(coords) {

				}*/
        },
        /*
		 'basic': {
		 	'wgs': function(coords) {

		 		},
			'pulkovo': function(coords) {

				},
			'galileo': function(coords) {

				},
			'glonass': function(coords) {

				}
		 },*/
        'pulkovo': {
            'wgs': function(coords) {
                //console.log(this.lib['wp']['p2w'](coords))
                this.doSetCoords('wgs', this.lib['wp']['p2w'](coords));
            }
            /*,
			'basic': function(coords) {

				},
			'galileo': function(coords) {

				},
			'glonass': function(coords) {

				}*/
        }
        /*,
		 'galileo': {
		 	'wgs': function(coords) {

		 		},
			'basic': function(coords) {

				},
			'pulkovo': function(coords) {

				},
			'glonass': function(coords) {

				}
		 },
		 'glonass': {
		 	'wgs': function(coords) {

		 		},
			'basic': function(coords) {

				},
			'pulkovo': function(coords) {

				},
			'galileo': function(coords) {

				}
		 }*/
    },

    doSetLastChangedSystem: function() {
        var fieldLabel = this.lastChangedField.fieldLabel;
        if (!fieldLabel) {
            return;
        }
        for (var i in this.cSystems) {
            if (fieldLabel.indexOf(this.cSystems[i]) != -1) {
                this.lastChanged = i;
                return;
            }
        }
    },

    doGetCoords: function(csystem) {
        var result = {};
        Ext.each(this.fields, function(field) {
            switch (field.fieldLabel) {
                case this.cSystems[csystem] + ' (Y)':
                case this.cSystems[csystem] + ' (Долгота)':
                case this.cSystems[csystem] + ' (Longitude)':
                    Ext.apply(result, {
                        x: field.getValue() != '' ? parseFloat(field.getValue()) : 0
                    });
                    break
                case this.cSystems[csystem] + ' (X)':
                case this.cSystems[csystem] + ' (Широта)':
                case this.cSystems[csystem] + ' (Latitude)':
                    Ext.apply(result, {
                        y: field.getValue() != '' ? parseFloat(field.getValue()) : 0
                    });
                    break
                case this.cSystems[csystem] + ' (Z)':
                case this.cSystems[csystem] + ' (Высота)':
                case this.cSystems[csystem] + ' (Height)':
                    Ext.apply(result, {
                        z: field.getValue() != '' ? parseFloat(field.getValue()) : 0
                    });
                    break
            }
        }, this);
        return result;
    },

    doSetCoords: function(csystem, coords) {
        Ext.each(this.fields, function(field) {
            switch (field.fieldLabel) {
                case this.cSystems[csystem] + ' (Y)':
                case this.cSystems[csystem] + ' (Долгота)':
                case this.cSystems[csystem] + ' (Longitude)':
                    field.setValue(coords.x);
                    break
                case this.cSystems[csystem] + ' (X)':
                case this.cSystems[csystem] + ' (Широта)':
                case this.cSystems[csystem] + ' (Latitude)':
                    field.setValue(coords.y);
                    break
                case this.cSystems[csystem] + ' (Z)':
                case this.cSystems[csystem] + ' (Высота)':
                case this.cSystems[csystem] + ' (Height)':
                    field.setValue(coords.z);
                    break
            }
            field.prepareValue();
        }, this);
    },

    validate: function(v) {
        var res = false;
        if (v) {
            var r = /[0-9]{1,2}/g,
                e = r.exec(v);
            if (e) {
                if (+v < 100) {
                    res = true;
                }
            } else {
                res = false;
            }
        } else {
            res = true;
        }
        return res;
    },

    initCoords: function() {

        Ext.each(this.fields, function(field) {
            var fieldLabel = field.fieldLabel;
            //console.log(fieldLabel)
            if (!fieldLabel) {
                return;
            }
            if (fieldLabel.indexOf(this.cSystems['wgs']) != -1 || fieldLabel.indexOf(this.cSystems['pulkovo']) != -1 || fieldLabel.indexOf(this.cSystems['galileo']) != -1 || fieldLabel.indexOf(this.cSystems['glonass']) != -1) {
                field.maskRe = new RegExp("[0-9\.\-]");
                field.filterKeys = function(e) {
                    var v = this.getValue(),
                        dotpos = v.indexOf('.'),
                        minuspos = v.indexOf('-'),
                        curpos = this.getSelection().selectionStart,
                        ch = String.fromCharCode(e.getCharCode());
                    // special keys don't generate charCodes, so leave them alone

                    if ((e.getKey() == e.BACKSPACE || e.getKey() == e.DELETE) && curpos == 0) {
                        return true;
                    }

                    if ((e.ctrlKey || e.isSpecialKey())) {
                        if (e.getKey() == e.BACKSPACE) {
                            if (v[curpos - 1] == '.') {
                                this.setValue(v.substr(0, dotpos + 1));
                            }
                        }
                        return;
                    }
                    if (!this.maskRe.test(ch)) {
                        e.stopEvent();
                    }
                    //console.log(ch,curpos,dotpos,v,v.length)

                    if (ch == '.' && dotpos != -1) {
                        e.stopEvent();
                    }
                    if (ch == '-' && curpos != 0) {
                        e.stopEvent();
                    }
                    if (ch == '-' && minuspos != -1) {
                        e.stopEvent();
                    }
                    if (ch != '-' && minuspos != -1 && curpos == 0) {
                        e.stopEvent();
                    }
                    /*
			        if (dotpos != -1) {
			        	if (curpos <= dotpos) {
			        		if (dotpos>3) {
			        			e.stopEvent();
			        		}
			        	} else {
			        		if (v.length - dotpos > 5) {
			        			e.stopEvent();
			        		}
			        	}
			        } else {
		        		if (v.length>3 && ch!='.') {
		        			e.stopEvent();
		        		}
		        	}*/
                }

                field.prepareValue = (function() {
                    var v = this.getValue(),
                        minuspos = v.indexOf('-'),
                        dotpos = v.indexOf('.');
                    if (dotpos == -1) {
                        v = v + '.';
                        dotpos = v.length - 1;
                    }
                    var lp = v.substr(0, dotpos),
                        rp = v.substr(dotpos + 1, v.length),
                        lpl = lp.length,
                        rpl;

                    if (rp.length > 5) {
                        rp = rp.substr(0, 5);
                    }

                    rpl = rp.length;

                    for (i = 0; i < 4 - lpl; i++) {
                        lp = '0' + lp;
                    }
                    for (i = 0; i < 5 - rpl; i++) {
                        rp = rp + '0';
                    }

                    if (minuspos != -1) {
                        lp = lp.replace('-', '');
                        if (parseInt(lp) > 0) {
                            lp = '-' + lp;
                        }
                    }

                    this.setValue(lp + '.' + rp);
                }).createDelegate(field);


                field.on('blur', field.prepareValue, field);
                field.prepareValue();
            } else {
                field.maskRe = new RegExp("[0-9\.]");
                field.filterKeys = function(e) {
                    var v = this.getValue(),
                        dotpos = v.indexOf('.'),
                        ch = String.fromCharCode(e.getCharCode());
                    // special keys don't generate charCodes, so leave them alone
                    if (e.ctrlKey || e.isSpecialKey()) {
                        return;
                    }
                    if (!this.maskRe.test(ch)) {
                        e.stopEvent();
                    }
                    if (ch == '.' && dotpos != -1) {
                        e.stopEvent();
                    }
                }
                field.prepareValue = Ext.emptyFn;
            }
        }, this);

        Ext.each(this.fields, function(field) {
            field.enableKeyEvents = true;

            field.on('keydown', function(f) {
                //f.lastValue = f.getValue();
                this.lastChangedField = field;
                this.lastChangedFieldValue = field.getValue();

            }, this);

            field.on('keyup', function(f) {
                /*if (!this.validate(f.getValue())) {
					f.setValue(f.lastValue);
				}*/
                if (this.lastChangedFieldValue != field.getValue()) {
                    this.doSetLastChangedSystem();
                }
            }, this);
        }, this);
    },

    initDefaults: function() {
        Ext.each(this.fields, function(field) {
            //field.emptyText = 'DD,DDDDDD';
            //field.decimalPrecision = 6;
        });
    },

    resetCoords: function() {
        this.lastChanged = 'basic';
        this.recalcCoords();
    },

    recalcCoords: function() {
        var coords = this.doGetCoords(this.lastChanged);
        //console.log("!",coords,this.convert,this.lastChanged)
        for (i in this.convert) {
            if (i == this.lastChanged) {
                for (j in this.convert[i]) {
                    //console.log(i,j)
                    this.convert[i][j].apply(this, [coords]);
                }
            }
        }
    },

    constructor: function(config) {
        var config = config || {};
        Ext.apply(this, config);
        Map.RC.superclass.constructor.call(this, config);
        this.initCoords();
        this.initDefaults();
    }
});


Map.Card = function(mapViewer) {
    this.mapViewer = mapViewer;
    this.conn = new Ext.data.Connection();
    this.wgsConn = new Ext.data.WgsConnection();
    this.panel = new Map.Card.CardPanel();
    //this.loadMask = null;
    this.attrCard = null;
    this.modeCard = 0;
    this.featureId = 0;
    this.entityId = 0;
    this.objectId = 0;
    this.objectIdForPrint = 0;
    this.lastConfig = {};
    if (mapViewer) {
        this.window = new Ext.WgsServiceWindow({
            containerPanel: mapViewer.rightPane,
            width: this.panel.width + 12,
            layout: 'fit',
            title: 'Карточка',
            resizable: false,
            minimizable: false
        });
    } else {
        this.window = new Ext.WgsServiceWindowSimple({
            width: this.panel.width + 12,
            layout: 'fit',
            title: 'Карточка',
            resizable: false,
            minimizable: false
        });
    }
    this.window.setInnerPanel(this.panel);
    this.panel.featuredocButton.on('click', this.openFeatureDoc, this);
    this.panel.cardCombobox.on('select', this.cardComboboxOnSelect, this);
    this.panel.printButton.on('click', this.printCard, this);
    this.panel.saveButton.on('click', this.saveCard, this);
    this.panel.cancelButton.on('click', this.onCancel, this);
    this.panel.childCardsStore.on('load', this.onChildCardsLoaded, this);
    /*this.window.on('show', function(){
        this.loadMask = new Ext.LoadMask(this.panel.getEl(),{msg: 'Загрузка...'});
        this.loadMask.show();
    },this);*/
}

Map.Card.Instance = function(map) {
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Card(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Card(map);
        }
        return this.instance;
    }
}

Map.Card.Create = function(map) {
    return new Map.Card(map);
}

Map.Card.prototype = {
    open: function(config) {
        if (!WGS.PRIVILEGE.CARD_EDIT) this.panel.saveButton.hide();
        var config = config || {};

        this.lastConfig = config;

        this.cardCallback = config.callback || Ext.emptyFn;
        this.cardScope = config.scope || this;

        if (!config.objectId && !config.featureId) {
            if (this.mapViewer) {
                this.mapViewer.Lib.getSelectedFeatures(function(featureLayers) {
                    if (featureLayers != -1) {
                        if (featureLayers[0].features && featureLayers[0].features.length == 1) {
                            var featureId = featureLayers[0].features[0].featureId;
                            this.entityId = featureLayers[0].features[0].featureId;
                            this.getCards(featureId, null);
                        } else Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Необходимо выбрать один объект',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    } else Ext.WgsMsg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо выбрать один объект',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }, this);
            }
        } else {
            this.mapViewer = config.mapViewer;
            if (config.objectId) {
                this.getCards(null, config.objectId);
                return;
            }
            if (config.featureId) {
                this.getCards(config.featureId, null);
                return;
            }
        }
    },

    getCards: function(featureId, objectId) {
        var cardJsonReader = new Ext.data.JsonReader({}, [{
            name: 'cardId'
        }, {
            name: 'cardName'
        }, {
            name: 'cardIsCurrent'
        }, {
            name: 'cardTypeName'
        }, {
            name: 'cardRelationName'
        }, {
            name: 'cardLabel'
        }]);

        if (featureId && !objectId) {
            this.featureId = featureId;
            this.objectId = null;
            this.wgsConn.request({
                url: '/objectadmin/card/getcardsbyfeatureid',
                method: 'POST',
                params: {
                    'featureId': featureId
                },
                success: function(response) {
                    if (response.responseText != 'null') {
                        //alert(cardJsonReader.read(response).records.length);
                        if (cardJsonReader.read(response).records.length > 0) {
                            var cards = cardJsonReader.read(response).records;
                            //this.open();
                            this.window.addPanel();
                            this.window.show();
                            this.panel.cardCombobox.clearValue();
                            var objectId = cards[0].data.cardId;
                            this.panel.cardsStore.removeAll();
                            this.panel.cardsStore.add(cards);
                            this.panel.cardCombobox.setValue(cards[0].data.cardLabel);
                            this.removeAttrCardPanel();
                            this.addAttrCardPanel(objectId);
                            this.modeCard = 1;
                            this.loadValueBaseProperties(objectId, this.modeCard);
                        } else {
                            Ext.WgsMsg.show({
                                title: 'Предупреждение',
                                msg: 'Необходимо выбрать один объект',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Описаний не найдено',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            });
        }
        if (objectId && !featureId) {
            this.objectId = objectId;
            this.featureId = null;
            this.panel.featuredocButton.hide();
            this.wgsConn.request({
                url: '/objectadmin/card/getentityidbyobject',
                method: 'POST',
                params: {
                    id: this.objectId
                },
                success: function(response) {
                    if (response.responseText) {
                        if (response.responseText != '0') {
                            this.panel.featuredocButton.show();
                        }
                    }
                },
                scope: this
            });
            this.wgsConn.request({
                url: '/objectadmin/card/getcardsbyobjectid',
                method: 'POST',
                params: {
                    'objectId': objectId
                },
                success: function(response) {
                    if (response.responseText != 'null') {
                        if (cardJsonReader.read(response).records.length > 0) {
                            var cards = cardJsonReader.read(response).records;
                            this.window.addPanel();
                            this.window.show();
                            this.panel.cardCombobox.clearValue();
                            var objectId = cards[0].data.cardId;
                            this.panel.cardsStore.removeAll();
                            this.panel.cardsStore.add(cards);
                            this.panel.cardCombobox.setValue(cards[0].data.cardLabel);
                            this.removeAttrCardPanel();
                            this.addAttrCardPanel(objectId);
                            this.modeCard = 0;
                            this.loadValueBaseProperties(objectId, this.modeCard);
                        }
                    }
                },
                scope: this
            });
        }
    },

    addAttrCardPanel: function(objectId) {
        this.attrCard = new Map.Card.AttrCardFormPanel(this.panel, objectId, this.mapViewer, this);
        //this.attrCard.setObjectId(objectId);
        this.panel.attrCardPanel.add(this.attrCard);
        this.panel.doLayout();
    },

    removeAttrCardPanel: function() {
        if (this.attrCard != null) {
            this.panel.attrCardPanel.remove(this.attrCard);
            this.panel.doLayout();
            this.attrCard = null;
        }
    },

    cardComboboxOnSelect: function(combo, record, index) {
        this.panel.cardCombobox.setValue(record.data.cardLabel);
        var objectId = record.get('cardId');
        this.removeAttrCardPanel();
        this.addAttrCardPanel(objectId);
        this.wgsConn.request({
            url: '/objectadmin/card/getentityidbyobject',
            method: 'POST',
            params: {
                id: objectId
            },
            success: function(response) {
                if (response.responseText) {
                    if (response.responseText == '0') {
                        this.panel.featuredocButton.hide();
                    } else {
                        this.panel.featuredocButton.show();
                    }
                }
            },
            scope: this
        });
        this.loadValueBaseProperties(objectId, this.modeCard);
    },

    reloadCardsStore: function() {
        var url = '';
        var params = new Object();
        var cardJsonReader = new Ext.data.JsonReader({}, [{
            name: 'cardId'
        }, {
            name: 'cardName'
        }, {
            name: 'cardIsCurrent'
        }, {
            name: 'cardTypeName'
        }, {
            name: 'cardRelationName'
        }, {
            name: 'cardLabel'
        }]);

        if (this.featureId && !this.objectId) {
            url = '/objectadmin/card/getcardsbyfeatureid',
            params = {
                'featureId': this.featureId
            }
        }
        if (!this.featureId && this.objectId) {
            url = '/objectadmin/card/getcardsbyobjectid',
            params = {
                'objectId': this.objectId
            }
        }
        var currentCardValue = this.panel.cardCombobox.getValue();

        this.wgsConn.request({
            url: url,
            method: 'POST',
            params: params,
            success: function(response) {
                if (response.responseText != 'null') {
                    if (cardJsonReader.read(response).records.length > 0) {
                        var cards = cardJsonReader.read(response).records;
                        this.panel.cardCombobox.clearValue();
                        var objectId = cards[0].data.cardId;
                        this.panel.cardsStore.removeAll();
                        this.panel.cardsStore.add(cards);
                        this.panel.cardCombobox.setValue(currentCardValue);
                    }
                }
            },
            scope: this
        });
    },

    /*onChildCardsLoaded: function(store, records, options)
    {
        if (records.length > 0) {
            this.panel.listChildCardFieldSet.show();
        } else {
            this.panel.listChildCardFieldSet.hide();
        }
    },*/


    saveCard: function() {
        var objectId = this.panel.cardCombobox.getValue();
        this.attrCard.saveCard({
            callback: this.cardCallback,
            scope: this.cardScope
        });
        if (this.modeCard) {
            this.panel.cardsStore.each(function(record) {
                if (record.get('cardId') == objectId) {
                    if (this.attrCard.getForm().getValues().name != '') record.set('cardName', this.attrCard.getForm().getValues().name + ' - ' + record.get('cardTypeName'));
                    else record.set('cardName', 'Без наименования - ' + record.get('cardTypeName'));
                    record.commit();
                    this.panel.cardCombobox.setValue(objectId);
                }
            }, this);
        } else {
            if (this.attrCard.getForm().getValues().name != '') this.panel.cardCombobox.setValue(this.attrCard.getForm().getValues().name + ' - ' + this.attrCard.getForm().getValues().typeName);
            else this.panel.cardCombobox.setValue('Без наименования - ' + this.attrCard.getForm().getValues().typeName);
        }
    },

    loadValueBaseProperties: function(objectId, mode) {
        //var msgWait = Ext.Msg.wait('Запрос характеристик...', 'Подождите', {interval:50});
        this.objectIdForPrint = objectId;
        this.wgsConn.request({
            url: '/objectadmin/card/getvaluebaseproperties',
            method: 'POST',
            params: {
                'objectId': objectId
            },
            success: function(response) {
                var record = this.panel.basePropertiesReader.read(response).records[0];
                this.attrCard.getForm().loadRecord(record);
                if (!this.modeCard) {
                    if (record.data.name != '') this.panel.cardCombobox.setValue(record.data.name + ' - ' + record.data.typeName);
                    else this.panel.cardCombobox.setValue('Без наименования - ' + record.data.typeName);
                }
                //this.panel.enable();
                //this.loadMask.hide();
                this.attrCard.setReadOnlyCreateDateField();
                this.panel.doLayout();
                //msgWait.hide();
            },
            scope: this
        });
    },

    onCancel: function() {
        this.panel.hide();
    },

    printCard: function() {
        this.print();
    },

    openFeatureDoc: function() {
        var url = '',
            id = 0;

        this.featureId = this.featureId || this.lastConfig.featureId;
        this.entityId = this.entityId || this.lastConfig.entityId;
        this.objectId = this.objectId || this.lastConfig.objectId;

        //alert('featureId: ' + this.featureId + '; entityId: ' + this.entityId + '; objectId: ' + this.objectId);

        if (this.entityId) {
            Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(this.entityId, [this.entityId]);
        } else {
            if (this.featureId) {
                url = '/objectadmin/card/getentityidbyfeature';
                id = this.featureId;
            } else {
                if (this.objectId) {
                    url = '/objectadmin/card/getentityidbyobject';
                    id = this.objectId;
                }
            }

            this.wgsConn.request({
                url: url,
                method: 'POST',
                params: {
                    id: id
                },
                success: function(response) {
                    if (response.responseText) {
                        if (response.responseText != '0') {
                            //this.entityId = response.responseText;
                            var entityId = response.responseText;
                            Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(entityId, [entityId]);
                        }
                    }
                },
                scope: this
            });
        }
    },

    print: function() {
        var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {
            interval: 80
        });
        this.wgsConn.request({
            url: '/objectadmin/card/print',
            defaultHeaders: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: 'POST',
            params: {
                objectId: this.objectIdForPrint
            },
            success: function(response) {
                msgWait.hide();
                if (response.responseText) {
                    //alert(response.responseText);
                    this.openPdf(response.responseText);
                }
            },
            scope: this
        });
    },

    openPdf: function(pdfSeq) {
        this.docWindow = new Ext.WgsShimWindow({
            title: 'Печать',
            closable: true,
            width: 550,
            height: 600,
            minWidth: 320,
            minHeight: 240,
            border: false,
            resizable: true,
            maximizable: true,
            layout: 'fit',
            plain: true,
            closeAction: 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/' + pdfSeq + '"></param><EMBED src="/objectadmin/card/getpdf/' + pdfSeq + '" width="100%" height="100%" href="/objectadmin/card/getpdf/' + pdfSeq + '" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
    }
}

Map.Card.AdvancedParamRelationPanel = function(owner, objectRelationId) {
    this.owner = owner;
    this.objectRelationId = objectRelationId;

    var self = this;

    this.storeJS = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['name', 'value', 'vulueTypeId', 'groupId', 'groupName', 'id'],
        url: '/objectadmin/card/GetValuePropertiesByObjRel',
        baseParams: {
            objectRelationId: self.objectRelationId
        }
    });

    self.setObjectRelationId = function(_ObjectRelationId) {
        self.objectRelationId = _ObjectRelationId;
        self.storeJS.baseParams.objectRelationId = _ObjectRelationId;
        self.storeJS.reload();
    };

    Map.Card.AdvancedParamRelationPanel.superclass.constructor.call(this, {
        title: 'Характеристики связи',
        autoHeight: true,
        autoScroll: true,
        closable: false,
        defaultType: 'textfield',
        items: [{
            name: 'objectRelationId',
            hidden: true,
            labelSeparator: ''
        }]
    });
}
Ext.extend(Map.Card.AdvancedParamRelationPanel, Ext.form.FieldSet, {
    initComponent: function() {
        var self = this;
        self.storeJS.on('load', function() {
            self.domainStores = [];
            var group_id = -1;
            var groupFieldSet;
            self.storeJS.each(function(record) {
                if (group_id != record.data.groupId) {
                    if (groupFieldSet) {
                        self.add(groupFieldSet);
                    }

                    groupFieldSet = new Ext.form.FieldSet({
                        title: 'Группа характеристик: ' + record.data.groupName,
                        autoHeight: true,
                        collapsible: true,
                        closable: false,
                        anchor: '97%',
                        collapsed: true
                    });

                    if (group_id == -1) {
                        groupFieldSet.collapsed = false
                    }
                    group_id = record.data.groupId;
                }
                if (record.data.vulueTypeId == 1) {
                    var propertyEditor = new Ext.form.TextField({
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 2) {
                    var propertyEditor = new Ext.form.DateField({
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        format: 'Y-m-d'
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 3) {
                    var propertyEditor = new Ext.form.NumberField({
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        allowDecimals: false,
                        maxText: 10
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 4) {
                    var propertyEditor = new Ext.form.NumberField({
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        allowDecimals: true,
                        decimalPrecision: 20,
                        maxText: 50,
                        decimalSeparator: ','
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 5) {
                    self.domainStores[self.domainStores.length] = new Ext.data.JsonStore({
                        //autoLoad: true,
                        fields: ['id', 'name'],
                        url: '/objectadmin/card/GetValueDomainList',
                        baseParams: {
                            propId: record.data.id
                        }
                    });
                    self.domainStores[self.domainStores.length - 1].load({
                        callback: function() {
                            propertyEditor.setValue(record.data.value);
                            propertyHidden.setValue(propertyEditor.getValue());
                        }
                    });
                    var propertyHidden = new Ext.form.TextField({
                        name: 'propRel-' + record.data.id,
                        hideLabel: true,
                        hidden: true,
                        labelSeparator: '',
                        value: record.data.value
                    });
                    var propertyEditor = new Ext.form.ComboBox({
                        fieldLabel: record.data.name,
                        name: 'gridRel-' + record.data.id,
                        store: self.domainStores[self.domainStores.length - 1],
                        anchor: '97%',
                        displayField: 'name',
                        valueField: 'id',
                        typeAhead: true,
                        loadingText: 'Список загружается...',
                        triggerAction: 'all',
                        emptyText: 'Выберите значение...',
                        selectOnFocus: true
                    });
                    propertyEditor.on('select', function(combo, record, index) {
                        propertyHidden.setValue(propertyEditor.getValue());
                    });
                    groupFieldSet.add(propertyEditor);
                    groupFieldSet.add(propertyHidden);
                } else if (record.data.vulueTypeId == 7) {
                    var propertyEditor = new Ext.form.TextArea({
                        fieldLabel: record.data.name,
                        name: 'propRel-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }
            });
            if (groupFieldSet) {
                self.add(groupFieldSet);
                self.owner.doLayout();
            }
        }, this);

        Map.Card.AdvancedParamRelationPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-ПАНЕЛЬ СВЯЗИ -*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.RelationCardPanel = function(owner, relation_id, object_id, relationType, mapViewer) {

    this.owner = owner;
    this.relation_id = relation_id;
    this.object_id = object_id;
    this.relationType = relationType;
    this.mapViewer = mapViewer;
    var self = this;

    this.storeJS = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'shortName', 'typeId', 'typeName', 'invNumber', 'createDate', 'note', 'author', 'label', 'ObjTypeRelationId', 'objRelationId'],
        url: '/objectadmin/card/GetListChildObjectByRelation',
        baseParams: {
            objectId: self.object_id,
            relationId: self.relation_id
        }
    });

    this.newButton = new Ext.Button({
        text: 'Создать',
        handler: function() {
            this.fireEvent('NewChildCard')
        },
        scope: this
    });
    this.bindButton = new Ext.Button({
        text: 'Найти и привязать',
        handler: function() {
            this.fireEvent('BindChildCard')
        },
        scope: this
    });

    this.unBindButton = new Ext.Button({
        text: 'Отвязать',
        handler: function() {
            this.fireEvent('UnBindChildCard')
        },
        scope: this
    });
    this.removeButton = new Ext.Button({
        text: 'Удалить',
        handler: function() {
            this.fireEvent('RemoveChildCard')
        },
        scope: this
    });

    this.editButton = new Ext.Button({
        text: 'Свойства',
        handler: function() {
            this.fireEvent('EditChildCard', self.gridChildCard.getSelectionModel().getSelected())
        },
        scope: this
    });
    this.newButton.disable();
    this.bindButton.disable();
    this.unBindButton.disable();
    this.removeButton.disable();
    this.editButton.disable();
    Map.Card.RelationCardPanel.superclass.constructor.call(this, {
        autoScroll: true,
        tbar: [this.newButton, this.bindButton, this.unBindButton, this.removeButton, this.editButton],
        items: [

        self.gridChildCard = new Ext.grid.GridPanel({
            viewConfig: {
                //autoFill :false,
                forceFit: true
            },
            store: this.storeJS,
            //width: 520,
            height: 100,
            autoScroll: true,
            columns: [{
                id: 'id',
                dataIndex: 'id',
                hideable: false,
                hidden: true
            }, {
                id: 'name',
                header: "Полное наименование",
                sortable: true,
                dataIndex: 'name'
            }, {
                id: 'shortName',
                header: "Краткое наименование",
                dataIndex: 'shortName'
            }, {
                id: 'typeId',
                dataIndex: 'typeId',
                hideable: false,
                hidden: true
            }, {
                id: 'typeName',
                header: "Наименованиe описания",
                sortable: true,
                dataIndex: 'typeName'
            }, {
                id: 'invNumber',
                dataIndex: 'invNumber',
                hideable: false,
                hidden: true
            }, {
                id: 'createDate',
                dataIndex: 'createDate',
                hideable: false,
                hidden: true
            }, {
                id: 'note',
                dataIndex: 'note',
                hideable: false,
                hidden: true
            }, {
                id: 'author',
                dataIndex: 'author',
                hideable: false,
                hidden: true
            }, {
                id: 'label',
                dataIndex: 'label',
                hideable: false,
                hidden: true
            }, {
                id: 'ObjTypeRelationId',
                dataIndex: 'ObjTypeRelationId',
                hideable: false,
                hidden: true
            }, {
                id: 'objRelationId',
                dataIndex: 'objRelationId',
                hideable: false,
                hidden: true
            }]
        })]
    });
}
Ext.extend(Map.Card.RelationCardPanel, Ext.Panel, {
    initComponent: function() {
        this.addEvents({
            NewChildCard: true,
            UnBindChildCard: true,
            RemoveChildCard: true,
            EditChildCard: true
        });

        var self = this;
        this.storeJS.on('load', function() {
            var countRecord = self.storeJS.getCount();
            if (countRecord > 0) {
                self.gridChildCard.getSelectionModel().selectFirstRow();
            }
            if (self.relationType == 1) {
                if (countRecord == 1) {
                    self.newButton.disable();
                    self.bindButton.disable();
                    self.unBindButton.enable();
                    self.removeButton.enable();
                    self.editButton.enable();
                } else {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.disable();
                    self.removeButton.disable();
                    self.editButton.disable();
                }
            }
            if (self.relationType == 2) {
                if (countRecord > 0) {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.enable();
                    self.removeButton.enable();
                    self.editButton.enable();
                } else {
                    self.newButton.enable();
                    self.bindButton.enable();
                    self.unBindButton.disable();
                    self.removeButton.disable();
                    self.editButton.disable();
                }
            }
        });

        this.on('NewChildCard', function() {
            this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
                fields: ['id', 'name', 'objTypeRelationId'],
                url: '/objectadmin/card/GetListChildObjectTypeByRelation'
            });
            this.featureTypeByRelationStore.removeAll();
            this.featureTypeByRelationStore.baseParams = { /*objectId: this.object_id, */
                relationId: this.relation_id
            };
            this.featureTypeByRelationStore.load({
                callback: function(records) {
                    if (records.length > 0) {
                        self.choiceAttrTypeRelationWindow = new Map.Card.ChoiceAttrTypeRelationWindow(self, self.object_id, self.relation_id, self.mapViewer);
                        self.choiceAttrTypeRelationWindow.show();
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            });
        });

        this.on('BindChildCard', function() {
            this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
                fields: ['id', 'name', 'objTypeRelationId'],
                url: '/objectadmin/card/GetListChildObjectTypeByRelation'
            });
            this.featureTypeByRelationStore.removeAll();
            this.featureTypeByRelationStore.baseParams = { /*objectId: this.object_id, */
                relationId: this.relation_id
            };
            this.featureTypeByRelationStore.load({
                callback: function(records) {
                    if (records.length > 0) {
                        var featureTypeFilter = [];
                        Ext.each(records, function(record) {
                            featureTypeFilter.push(record.data.id);
                        });
                        var bindSearch = Map.Bind.Search.Instance(this.mapViewer);
                        bindSearch.open({
                            object: this.object_id,
                            featureTypeFilter: featureTypeFilter,
                            mode: Map.Bind.Search.OBJECTBIND_MODE,
                            callback: function(objectId1, objectId2, featureTypeId) {
                                var objTypeRelationId;
                                this.featureTypeByRelationStore.findBy(function(record) {
                                    if (record.data.id == featureTypeId) {
                                        objTypeRelationId = record.data.objTypeRelationId;
                                        return;
                                    }
                                }, this);
                                new Ext.data.WgsConnection().request({
                                    url: '/objectadmin/card/BindChildObject',
                                    method: 'POST',
                                    params: {
                                        parentObjectId: objectId2,
                                        childObjectId: objectId1,
                                        objTypeRelationId: objTypeRelationId
                                    },
                                    success: function(action) {
                                        bindSearch.panel.resultTabPanel.resultStore.removeAll();
                                        bindSearch.container.hide();
                                        this.storeJS.removeAll();
                                        this.storeJS.load();
                                        this.owner.owner.reloadCardStore();
                                    },
                                    scope: this
                                });
                            },
                            scope: this
                        });
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                },
                scope: this
            }, this);
        }, this);


        this.on('UnBindChildCard', function(record) {
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите отвязать выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId) {
                    if (buttonId == 'yes') {
                        var selectedRelations = self.gridChildCard.getSelectionModel().getSelections();
                        if (selectedRelations.length != 0) {
                            var relationsForRemove = [];
                            for (var i = 0; i < selectedRelations.length; i++) {
                                relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
                            }
                            var msgWait = Ext.Msg.wait('Отвязка объектов...', 'Подождите', {
                                interval: 50
                            });
                            new Ext.data.WgsConnection().request({
                                url: '/objectadmin/card/RemoveObjectRelation',
                                method: 'POST',
                                params: {
                                    removeMode: 1,
                                    'relationsId[]': relationsForRemove
                                },
                                success: function() {
                                    self.storeJS.reload();
                                    self.owner.owner.reloadCardStore();
                                    msgWait.hide();
                                }
                            });
                        }

                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        });

        this.on('RemoveChildCard', function(record) {
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите удалить выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId) {
                    if (buttonId == 'yes') {
                        var selectedRelations = self.gridChildCard.getSelectionModel().getSelections();
                        if (selectedRelations.length != 0) {
                            var relationsForRemove = [];
                            for (var i = 0; i < selectedRelations.length; i++) {
                                relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
                            }
                            var msgWait = Ext.Msg.wait('Удаление объектов...', 'Подождите', {
                                interval: 50
                            });
                            new Ext.data.WgsConnection().request({
                                url: '/objectadmin/card/RemoveObjectRelation',
                                method: 'POST',
                                params: {
                                    removeMode: 0,
                                    'relationsId[]': relationsForRemove
                                },
                                success: function() {
                                    self.storeJS.reload();
                                    self.owner.owner.reloadCardStore();
                                    msgWait.hide();
                                }
                            });
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        });

        this.on('EditChildCard', function(record) {
            Map.Card.Create(this.mapViewer).open({
                objectId: record.data.id,
                callback: function() {
                    this.storeJS.reload();
                    this.owner.owner.reloadCardStore();
                },
                scope: this
            });
        });
        this.owner.owner.controlReadOnly();
        Map.Card.RelationCardPanel.superclass.initComponent.apply(this, arguments);
    }
});

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-ОКНО ВЫБОРА АТРИБУТИВНОГО ТИПА ДЛЯ СВЯЗИ-*-*-*-*-*-*-*-*-*-*-*-
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
Map.Card.ChoiceAttrTypeRelationWindow = function(owner, parentObjectId, relation_id, mapViewer) {
    this.owner = owner;
    this.relation_id = relation_id;
    this.parentObjectId = parentObjectId;
    this.mapViewer = mapViewer;
    var self = this;

    this.storeJS = new Ext.data.WgsJsonStore({
        autoLoad: true,
        fields: ['id', 'name', 'objTypeRelationId'],
        url: '/objectadmin/card/GetListChildObjectTypeByRelation',
        baseParams: { /*objectId: parentObjectId, */
            relationId: self.relation_id
        }
    });

    this.saveButton = new Ext.Button({
        text: 'Создать',
        handler: function() {
            this.fireEvent('save')
        },
        scope: this
    });
    this.cancelButton = new Ext.Button({
        text: 'Отмена',
        handler: function() {
            this.fireEvent('cancel')
        },
        scope: this
    });
    self.saveButton.disable();

    this.attrTypeComboBox = new Ext.form.ComboBox({
        anchor: '100%',
        hideLabel: true,
        labelSeparator: '',
        store: this.storeJS,
        displayField: 'name',
        valueField: 'id',
        loadingText: 'Список загружается...',
        triggerAction: 'all',
        emptyText: 'Выберите описание...',
        allowBlank: true,
        editable: false
    });

    Map.Card.ChoiceAttrTypeRelationWindow.superclass.constructor.call(this, {
        title: 'Выбор атрибутивного описания',
        layout: 'fit',
        height: 180,
        width: 375,
        modal: true,
        autoScrol: true,
        resizable: false,
        items: [
        new Ext.form.FormPanel({
            frame: true,
            bodyStyle: 'padding:5px 5px 0',
            items: [
            new Ext.Panel({
                frame: true,
                height: 60,
                layout: 'fit',
                style: 'margin-bottom: 5px',
                html: '<p>Если вы уверены, что хотите создать атрибутивный объект и ' + 'связать его с данным, выберите атрибутивный тип и нажмите "Создать", ' + 'иначе "Отмена"</p>'
            }),
            this.attrTypeComboBox]
        })],
        buttons: [this.saveButton, this.cancelButton]
    });
}
Ext.extend(Map.Card.ChoiceAttrTypeRelationWindow, Ext.Window, {
    initComponent: function() {
        this.addEvents({
            save: true,
            cancel: true
        });

        var self = this;

        self.attrTypeComboBox.on('select', function() {
            self.saveButton.enable();
        });

        this.on('save', function() {

            var msgWait = Ext.Msg.wait('Создание карточки...', 'Подождите', {
                interval: 50
            });
            new Ext.data.WgsConnection().request({
                url: '/objectadmin/card/CreateChildObject',
                method: 'POST',
                params: {
                    objectTypeId: self.attrTypeComboBox.getValue(),
                    parentObjectId: self.parentObjectId,
                    objTypeRelationId: self.storeJS.getAt(self.storeJS.find('id', self.attrTypeComboBox.getValue())).data.objTypeRelationId
                },
                success: function(action) {
                    var response = Ext.decode(action.responseText);
                    if (response) {
                        Map.Card.Create(this.mapViewer).open({
                            objectId: response.childObjectId,
                            callback: function() {
                                self.owner.storeJS.reload();
                                self.owner.owner.owner.reloadCardStore();
                            },
                            scope: this
                        });
                    }
                    msgWait.hide();
                    self.close();
                }
            });
        });
        this.on('cancel', function() {
            self.close();
        });

        Map.Card.ChoiceAttrTypeRelationWindow.superclass.initComponent.apply(this, arguments);
    }
});


//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-ПАНЕЛЬ ДОЧЕРНИХ КАРТОЧЕК ДЛЯ ДАННОЙ КАРТОЧКИ-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.Card.CildCardAttrCardPanel = function(owner, object_id, mapViewer) {
    this.owner = owner;
    this.object_id = object_id;
    this.mapViewer = mapViewer;
    //this.loadMask = loadMask;
    var self = this;

    self.setObjectId = function(_Object_id) {
        self.object_id = _Object_id;
        self.storeJS.baseParams.objectId = _Object_id;
        self.storeJS.reload();
    };

    this.storeJS = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['relationId', 'name', 'relationTypeId', 'countTypeRelation'],
        url: '/objectadmin/card/GetListRelByObject',
        baseParams: {
            objectId: self.object_id
        }
    });

    Map.Card.CildCardAttrCardPanel.superclass.constructor.call(this, {
        autoScroll: true,
        closable: false
    });
}
Ext.extend(Map.Card.CildCardAttrCardPanel, Ext.Panel, {
    initComponent: function() {
        var self = this;
        self.storeJS.on('load', function() {
            self.storeJS.each(function(record) {
                this.relationFieldSet = new Ext.form.FieldSet({
                    title: 'Связь: ' + record.data.name,
                    height: 170,
                    collapsible: true,
                    layout: 'fit',
                    anchor: '100%',
                    items: [
                    this.relationCardPanel = new Map.Card.RelationCardPanel(self, record.data.relationId, self.object_id, record.data.relationTypeId, self.mapViewer)]
                });
                self.add(this.relationFieldSet);
            }, this);
            self.owner.controlReadOnly();
            self.owner.doLayout();
        }, this);
        Map.Card.CildCardAttrCardPanel.superclass.initComponent.apply(this, arguments);

    }
});
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ПАНЕЛЬ ДОПОЛНИТЕЛЬНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.Card.AdvancedParamAttrCardPanel = function(owner, object_id) {
    this.owner = owner;
    this.object_id = object_id;

    var self = this;

    this.storeJS = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['name', 'value', 'vulueTypeId', 'groupId', 'groupName', 'id'],
        url: '/objectadmin/card/GetValueProperties',
        baseParams: {
            objectId: self.object_id
        },
        sortInfo: {
            field: 'groupId',
            direction: 'DESC' // or 'ASC' (case sensitive for local sorting)
        }

    });

    self.setObjectId = function(_Object_id) {
        self.object_id = _Object_id;
        self.storeJS.baseParams.objectId = _Object_id;
        self.storeJS.reload();
    };

    Map.Card.AdvancedParamAttrCardPanel.superclass.constructor.call(this, {
        closable: false
    });
}
Ext.extend(Map.Card.AdvancedParamAttrCardPanel, Ext.Panel, {
    initComponent: function() {
        var self = this;
        self.storeJS.on('load', function() {
            self.domainStores = [];
            var group_id = -1;
            var groupFieldSet, gr;
            self.storeJS.each(function(record) {
                if (group_id != record.data.groupId) {
                    if (groupFieldSet) {
                        self.add(groupFieldSet);
                    }
                    var groupId = record.data.groupId;

                    var cExpanded = WGS.Cookie.get('propGroupExpanded') || "",
                        expandedGroups = cExpanded.length > 0 ? (cExpanded.indexOf(",") != -1 ? cExpanded.split(",") : [cExpanded]) : [];

                    var expDate = new Date(),
                        days = 180;

                    expDate.setTime(expDate.getTime() + (days * 24 * 60 * 60 * 1000));

                    groupFieldSet = new Ext.form.FieldSet({
                        title: 'Группа характеристик: ' + record.data.groupName,
                        autoHeight: true,
                        collapsible: true,
                        closable: false,
                        anchor: '97%',
                        collapsed: !expandedGroups.inArray(groupId)
                    });

                    if (record.data.groupName == "Координаты (гггг.ггггг)") {
                        if (!groupFieldSet.rBtn) {
                            groupFieldSet.rBtn = groupFieldSet.addButton({
                                text: "Сбросить",
                                handler: function() {
                                    //console.log(groupFieldSet)
                                    groupFieldSet.rc.resetCoords();
                                },
                                scope: this,
                                hidden: true
                            });
                        }
                        if (!groupFieldSet.cBtn) {
                            groupFieldSet.cBtn = groupFieldSet.addButton({
                                text: "Пересчитать",
                                handler: function() {
                                    //console.log(groupFieldSet)
                                    groupFieldSet.rc.recalcCoords();
                                },
                                scope: this
                            });
                        }

                    }

                    groupFieldSet.on('collapse', function() {
                        var cookieExpanded = WGS.Cookie.get('propGroupExpanded') || "",
                            expanded = cookieExpanded.length > 0 ? (cookieExpanded.indexOf(",") != -1 ? cookieExpanded.split(",") : [cookieExpanded]) : [],
                            cleanExpanded = [];
                        for (var i = 0; i < expanded.length; i++) {
                            if (expanded[i] != groupId) {
                                cleanExpanded.push(expanded[i]);
                            }
                        }
                        WGS.Cookie.set('propGroupExpanded', cleanExpanded, expDate);
                    });
                    groupFieldSet.on('expand', function() {
                        var cookieExpanded = WGS.Cookie.get('propGroupExpanded') || "",
                            expanded = cookieExpanded.length > 0 ? (cookieExpanded.indexOf(",") != -1 ? cookieExpanded.split(",") : [cookieExpanded]) : [];
                        expanded.push(groupId);
                        WGS.Cookie.set('propGroupExpanded', expanded, expDate);
                    });
                    if (group_id == -1 && !expandedGroups.length) {
                        groupFieldSet.collapsed = false
                    }
                    group_id = record.data.groupId;
                }
                if (record.data.vulueTypeId == 1) {
                    var propertyEditor = new Ext.form.TextField({
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 2) {

                    var propertyEditor = new Ext.form.DateField({
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        format: 'Y-m-d'
                    });

                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 3) {
                    var propertyEditor = new Ext.form.NumberField({
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        allowDecimals: false,
                        maxText: 10
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 4) {
                    var propertyEditor = new Ext.form.NumberField({
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value,
                        allowDecimals: true,
                        decimalPrecision: 20,
                        maxText: 50,
                        decimalSeparator: ','
                    });
                    groupFieldSet.add(propertyEditor);
                } else if (record.data.vulueTypeId == 5) {
                    self.domainStores[self.domainStores.length] = new Ext.data.JsonStore({
                        fields: ['id', 'name'],
                        url: '/objectadmin/card/GetValueDomainList',
                        baseParams: {
                            propId: record.data.id
                        }
                    });
                    self.domainStores[self.domainStores.length - 1].load({
                        callback: function() {
                            propertyEditor.setValue(record.data.value);
                            propertyHidden.setValue(propertyEditor.getValue());
                        }
                    });
                    var propertyHidden = new Ext.form.TextField({
                        name: 'property-' + record.data.id,
                        hideLabel: true,
                        hidden: true,
                        labelSeparator: '',
                        value: record.data.value
                    });
                    var propertyEditor = new Ext.form.WgsComboBox({
                        fieldLabel: record.data.name,
                        name: 'grid-' + record.data.id,
                        store: self.domainStores[self.domainStores.length - 1],
                        anchor: '97%',
                        displayField: 'name',
                        valueField: 'id',
                        typeAhead: true,
                        loadingText: 'Список загружается...',
                        triggerAction: 'all',
                        emptyText: 'Выберите значение...',
                        selectOnFocus: true
                    });
                    propertyEditor.on('select', function(combo, record, index) {
                        //alert(propertyEditor.getValue());
                        propertyHidden.setValue(propertyEditor.getValue());
                    });

                    /*if(!WGS.PRIVILEGE.CARD_EDIT)
                        Ext.apply(propertyEditor, {hideTrigger: true});//self.setReadOnly(true, false);
                    else
                        Ext.apply(propertyEditor, {hideTrigger: false});//self.setReadOnly(false, true);*/

                    groupFieldSet.add(propertyEditor);
                    groupFieldSet.add(propertyHidden);
                } else if (record.data.vulueTypeId == 7) {
                    var propertyEditor = new Ext.form.TextArea({
                        fieldLabel: record.data.name,
                        name: 'property-' + record.data.id,
                        anchor: '97%',
                        allowBlank: true,
                        value: record.data.value
                    });
                    groupFieldSet.add(propertyEditor);
                }

                if (record.data.groupName == "Координаты (гггг.ггггг)") {
                    gr = groupFieldSet
                    if (gr) groupFieldSet.rc = new Map.RC({
                        fields: groupFieldSet.items.items
                    });
                }
            });
            if (groupFieldSet && groupFieldSet.items) {
                self.add(groupFieldSet);



                self.owner.doLayout();
            }

            self.owner.controlReadOnly();
        }, this);

        Map.Card.AdvancedParamAttrCardPanel.superclass.initComponent.apply(this, arguments);
    }
});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-ПАНЕЛЬ ОСНОВНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.BaseParamAttrCardPanel = function(owner, object_id) {
    this.owner = owner;
    this.object_id = object_id;

    var self = this;

    this.objectIdHidden = new Ext.form.Hidden({
        name: 'id',
        hidden: true,
        labelSeparator: ''
    });

    self.setObjectId = function(_Object_id) {
        self.object_id = _Object_id;
        this.objectIdHidden.setValue(_Object_id);
        owner.getForm().setValues({
            id: _Object_id
        });
    };

    Map.Card.BaseParamAttrCardPanel.superclass.constructor.call(this, {
        title: 'Основные характеристики',
        layout: 'form',
        autoHeight: true,
        autoWidth: true,
        defaultType: 'textfield',
        closable: false,
        items: [{
            fieldLabel: 'Наименование',
            name: 'name',
            anchor: '97%',
            allowBlank: true
        }, {
            fieldLabel: 'Краткое наименование',
            name: 'shortName',
            anchor: '97%',
            allowBlank: true
        }, {
            fieldLabel: 'Атрибутивное описание',
            name: 'typeName',
            readOnly: true,
            anchor: '97%',
            allowBlank: false
        },

        new Ext.form.DateField({
            fieldLabel: 'Дата изменения',
            name: 'createDate',
            anchor: '97%',
            format: 'Y-m-d H:i:s', //2010-04-05 12:24:25
            readOnly: true,
            allowBlank: false
        }), {
            fieldLabel: 'Автор',
            name: 'edit_user',
            readOnly: true,
            anchor: '97%',
            allowBlank: true
        },
        new Ext.form.TextArea({
            fieldLabel: 'Примечание',
            name: 'note',
            anchor: '97%',
            allowBlank: true
        }),
        this.objectIdHidden]
    });
}
Ext.extend(Map.Card.BaseParamAttrCardPanel, Ext.form.FieldSet, {});

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ОСНОВНАЯ ПАНЕЛЬ КАРТОЧКИ (АТРИБУТИВНЫХ ОБЪЕКТОВ)-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.Card.AttrCardFormPanel = Ext.extend(Ext.form.WgsFormPanel, {
    constructor: function(owner, object_id, mapViewer, card) {
        this.owner = owner;
        this.object_id = object_id;

        var self = this;

        if (object_id == null) {
            self.advancedParamAttrCardPanel = new Map.Card.AdvancedParamAttrCardPanel(self, -1);
            self.cildCardAttrCardPanel = new Map.Card.CildCardAttrCardPanel(self, -1, mapViewer);
        } else {
            self.advancedParamAttrCardPanel = new Map.Card.AdvancedParamAttrCardPanel(self, self.object_id);
            self.cildCardAttrCardPanel = new Map.Card.CildCardAttrCardPanel(self, self.object_id, mapViewer);
        }

        self.baseParamAttrCardPanel = new Map.Card.BaseParamAttrCardPanel(self, self.object_id);

        self.setObjectId = function(_Object_id) {
            self.baseParamAttrCardPanel.setObjectId(_Object_id);
            self.advancedParamAttrCardPanel.setObjectId(_Object_id);
            self.cildCardAttrCardPanel.setObjectId(_Object_id);
        };

        self.setReadOnlyCreateDateField = function() {
            if (this.object_id) {
                self.setReadonlyPrivate(self.getForm().findField('createDate'), true);
            }
        }

        self.saveCard = function(config) {
            var msgWait = Ext.Msg.wait('Идет сохранение параметров...', 'Подождите', {
                interval: 50
            });
            if (object_id != null) {
                self.getForm().setValues({
                    id: object_id
                })
                self.url = '/objectadmin/card/SetCard';
            } else {
                self.url = '/objectadmin/card/AddCard';
            }

            /*Ext.each(self.advancedParamAttrCardPanel.items.items, function(fs) {
				Ext.each(fs.items.items, function(item) {
					if (item.getValue() == 'DD.DDDDDD') {
						item.setValue("");
					}
				}, this)
			}, this);*/

            self.submit({
                success: function(form, action) {

                    if (self.owner) {
                        if (self.owner.owner) {
                            self.owner.owner.storeJS.reload();
                        }
                    }

                    msgWait.hide();
                    if (config) {
                        config.callback.call(config.scope);
                    }
                }
            })
        };

        self.reloadCardStore = function() {
            card.reloadCardsStore();
        };

        self.controlReadOnly = function() {
            if (!WGS.PRIVILEGE.CARD_EDIT) self.setReadOnly(true, false);
        };

        self.setReadonlyPrivate = function(f, bReadOnly) {
            if (typeof f._readOnly == 'undefined') {
                if (f.initEvents) f.initEvents = f.initEvents.createSequence(self.initEventsSequence)
            }
            f._readOnly = bReadOnly;
            f.getEl().dom.readOnly = bReadOnly;
            if (f instanceof Ext.form.TriggerField) {
                if (bReadOnly) {
                    f.trigger.un('click', f.onTriggerClick, f);
                    if (f.keyNav) f.keyNav.disable();
                } else {
                    f.trigger.on('click', f.onTriggerClick, f, {
                        preventDefault: true
                    });
                    if (f.keyNav) f.keyNav.enable();
                }
            }
        };

        self.initEventsSequence = function() {
            if (this._readOnly == true) {
                if (this.keyNav) this.keyNav.disable();
            } else {
                if (this.keyNav) this.keyNav.enable();
            }
        };

        self.setReadOnly = function(readOnly, buttonVisible) {
            self.getForm().items.each(function(f) {
                self.setReadonlyPrivate(f, readOnly);
            });
            if (self.advancedParamAttrCardPanel) if (self.advancedParamAttrCardPanel.items) self.advancedParamAttrCardPanel.items.each(function(fieldset) {
                if (fieldset.items) fieldset.items.each(function(field) {
                    self.setReadonlyPrivate(field, readOnly);
                });
            });
            if (self.cildCardAttrCardPanel) if (self.cildCardAttrCardPanel.items) self.cildCardAttrCardPanel.items.each(function(fieldset) {
                if (fieldset.items);
                fieldset.items.each(function(field) {
                    if (readOnly) if (field.getTopToolbar()) {
                        var toolbar = field.getTopToolbar();
                        if (Ext.isArray(toolbar)) {
                            toolbar[0].setVisible(false);
                            toolbar[1].setVisible(false);
                            toolbar[2].setVisible(false);
                            toolbar[3].setVisible(false);
                        }
                    }
                });
            });
            owner.saveButton.setVisible(buttonVisible);
            //owner.cancelButton.setVisible(buttonVisible);
        };

        Map.Card.AttrCardFormPanel.superclass.constructor.call(this, {
            frame: true,
            url: '/objectadmin/card/SetCard',
            layout: 'fit',
            autoWidth: true,
            //height: 400,
            closable: false,
            autoScroll: true,
            bodyStyle: 'padding:5px 5px 0',
            items: [self.baseParamAttrCardPanel, self.advancedParamAttrCardPanel, self.cildCardAttrCardPanel]
        });
    }
});
//Ext.extend(Map.Card.AttrCardFormPanel,Ext.form.WgsFormPanel,{});

Map.Card.CardPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function() {
        this.cardsStore = new Ext.data.WgsJsonStore({
            fields: ['cardId', 'cardName', 'cardIsCurrent', 'cardTypeName', 'cardRelationName', 'cardLabel']
        });

        this.childCardsStore = new Ext.data.WgsJsonStore({
            url: '/objectadmin/card/getchildobjectsbyparent',
            fields: ['cardId', 'cardName', 'cardTypeName', 'cardRelationName']
        });

        this.cardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            displayField: 'cardName',
            valueField: 'cardId',
            triggerAction: 'all',
            emptyText: 'Наименование',
            loadingText: 'Загрузка...',
            editable: false,
            store: this.cardsStore,
            mode: 'local'
        });

        this.childCardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            displayField: 'cardName',
            valueField: 'cardId',
            triggerAction: 'all',
            emptyText: 'Наименование',
            loadingText: 'Загрузка...',
            editable: false,
            store: this.childCardsStore,
            mode: 'local'
        });

        this.basePropertiesReader = new Ext.data.JsonReader({}, [{
            name: 'name'
        }, {
            name: 'shortName'
        }, {
            name: 'typeName'
        }, {
            name: 'edit_user'
        }, {
            name: 'createDate'
        }, {
            name: 'note'
        }]);

        this.saveButton = new Ext.Button({
            text: 'Сохранить'
        });

        this.cancelButton = new Ext.Button({
            text: 'Закрыть',
            scope: this
        });

        this.attrCardPanel = new Ext.Panel({
            region: 'south',
            height: 530,
            autoWidth: true,
            layout: 'fit'
        });

        this.listCardFieldSet = new Ext.form.FieldSet({
            title: 'Список описаний',
            autoHeight: true,
            items: this.cardCombobox
        });

        this.listChildCardFieldSet = new Ext.form.FieldSet({
            title: 'Список привязанных описаний',
            autoHeight: true,
            hidden: true,
            items: this.childCardCombobox
        });

        Map.Card.CardPanel.superclass.constructor.call(this, {
            width: 600,
            autoHeight: true,
            layout: 'border',
            frame: true,
            maskDisabled: false,
            collapsible: false,
            items: [{
                region: 'center',
                autoHeight: true,
                layout: 'fit',
                items: [this.listCardFieldSet, this.listChildCardFieldSet]
            },
            this.attrCardPanel]
        });

        this.featuredocButton = this.addButton({
            iconCls: 'featuredoc',
            text: 'Документы'
        });

        this.printButton = this.addButton({
            iconCls: 'icon-print',
            text: 'Печать'
        });

        this.saveButton = this.addButton({
            iconCls: 'icon-accept-tick',
            text: 'Применить'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text: 'Закрыть',
            scope: this
        });
    }
});