﻿Ext.namespace('Map');

Map.CardsJournal = function() {
	var REPAIR = 249,
		CRASH  = 248;
	
		
	var pub = {
		exportToExcel: function(featureType, records) {
			var cards = [];
			for (var i=0; i<records.length; i++) {
				cards.push(records[i].get('objectId'));
			}			
            this.disable();
            new Ext.data.WgsConnection().request({
                url: '/map/cardsjournal/GetExcelResult',
                method: 'POST',
                params: {
                    'featureType': featureType,
                    'cards[]': cards
                },
                success: function(response) {
                    this.enable();
					if (Ext.isIE) {
                        window.open("/map/cardsjournal/GetExcel/?id="+response.responseText, "excel");
                    } else {
                        window.open("/map/cardsjournal/GetExcelFile/"+response.responseText, "excel");
                    }
                },
                scope: this
            });
		},
		
		onFeatureTypeSelect: function(featureType, records, button) {
			featureType = parseInt(featureType);
			switch (featureType) {
				case REPAIR:
				case CRASH:
					button.handler = function() {
						pub.exportToExcel.call(button, featureType, records);
					}
					button.show();
					break;
				default:
					button.hide();
					break;
			}
		}	
	};
	return pub;
}();