﻿Ext.namespace('Map');

Map.FavoriteView = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.conn = new Ext.data.WgsConnection();
}

Map.FavoriteView.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.FavoriteView(map);
    }
    return this.instances[map.mapFrame];
}

Map.FavoriteView.prototype = {
    create: function(windowWidth, windowHeight)
    {
        this.panel = new FavoriteViewPanel({
            autoWidth: true,
            height: windowHeight,
            mapViewer: this.mapViewer,
            conn: this.conn
        });

        this.window = new Ext.WgsServiceWindowRatio({
            containerPanel: this.mapViewer.rightPane,
            layout: 'fit',
            resizable: true,
            width: windowWidth+22,
            title: 'Избранные виды'
        });

        this.window.setInnerPanel(this.panel);
        this.created = true;
    },

    open: function()
    {
        var workHeight = document.body.clientHeight;
        var windowHeight = workHeight * 0.9135; // коэффициент расчитан как соотношение высоты окна с избранными видами / на высоту клиентской части браузера
        var windowWidth = windowHeight * 1.0909; // коэффициент расчитан как соотношение ширины окна с избранными видами / на ширину клиентской части браузера
        this.initialization(windowWidth, windowHeight);
    },

    initialization: function(windowWidth, windowHeight)
    {
        if (!this.created)
        {
            this.create(windowWidth, windowHeight);
            Map.FavoriteView.msgWaitLoad = Ext.WgsMsg.wait('Генерация эскизов...', 'Подождите', {interval: 200});
            this.reload();
        }
        else {
            Map.FavoriteView.window.show();
            Map.FavoriteView.panel.show();
        }

        this.clearselectview();
        this.window.addPanel();
        Map.FavoriteView.window = this.window;
        Map.FavoriteView.panel = this.panel;

        Map.FavoriteView.selected = 0;
    },

    reload: function()
    {
        this.panel.fireEvent('reload');
    },

    clearselectview: function()
    {
        this.panel.fireEvent('clearselectview');
    }
}

FavoriteViewPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config){
        if (config){
            Ext.apply(this, config);
        }

        this.favoriteView = new FavoriteView(this.mapViewer, this.conn);

        this.addEvents({
            reload: true,
            clearselectview: true
        });

        this.on('reload', function(){
            this.favoriteView.fireEvent('reload');
        }, this);

        this.on('clearselectview', function(){
            this.favoriteView.fireEvent('clearselectview');
        }, this);

        FavoriteViewPanel.superclass.constructor.call(this, {
            layout: 'fit',
            items: [this.favoriteView]
        });
    }
});

ViewList = function (mapViewer) {

    this.xd = Ext.data;
    this.viewerApi = mapViewer.Api;
    this.viewerLib = mapViewer.Lib;

    this.store = new Ext.data.JsonStore({
        url: '/map/favoriteview/getgeometryproperty',
        baseParams: {
                     'MAPNAME': encodeURIComponent(this.viewerApi.Map.GetMapName()),
                     'SESSION': this.viewerApi.Map.GetSessionId()
                    },
        root: 'view',
        fields: ['name', 'number', 'url', 'centerX', 'centerY', 'scale', 'width', 'height', 'layerview']
    });

    Map.FavoriteView.store = this.store;

    this.tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap" id="{name}">',
            '<div style="width: {width}px; height: {height}px" class="thumb"><img src="{url}" title="{name}" style="width: {width}px; height: {height}px"></div>',
            '<div id="div_{number}" align="center"><span class="x-editable">{shortName}</span></div></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );

    ViewList.superclass.constructor.call(this, {
        store: this.store,
        tpl: this.tpl,
        autoHeight: true,
        autoWidth: true,
        singleSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.thumb-wrap',
        plugins: [
            new Ext.DataView.LabelEditor({dataIndex: 'name'})
        ],
        prepareData: function(data){
            data.shortName = Ext.util.Format.ellipsis(data.name, data.width*0.175); // 0.175 = 45/257 (45 - ширина текста, 257 - ширина эскиза)
            return data;
        }
    });
}
Ext.extend(ViewList, Ext.DataView, {
    initComponent : function() {

        this.on('click', function() {
            this.getSelectedRecords().each(function (record){
                Map.FavoriteView.selected = record.data.number;
            })
        });

        this.on('show', function() {

        });

        this.store.on('load', function (){
            this.clearSelections();
            Map.FavoriteView.window.show();
            Map.FavoriteView.panel.show();
            Map.FavoriteView.msgWaitLoad.hide();
        }, this);

        this.on('resize', function () {
            var panelHeight = Map.FavoriteView.panel.getInnerHeight();
            var newHeight = (panelHeight-111) * 200/640; // 200 - ширина эскиза, 640 - ширина панели для сохранения пропорции
            var newWidth =  newHeight * 1.285; // 257/200 коэффициент пропорции для эскизов
            this.store.each(function(record) {
                record.data.width = newWidth;
                record.data.height = newHeight;
            });
            this.refresh();
        });

        this.on('dblclick', function() {
            this.getSelectedRecords().each(function (record){
                if (record.data.scale) {
                    Map.FavoriteView.window.hide();
                    Map.FavoriteView.panel.hide();
                    this.viewerApi.Map.ZoomToView(parseFloat(record.data.centerX), parseFloat(record.data.centerY), parseFloat(record.data.scale), true);
                    if (record.data.layerview) {
                    	this.viewerLib.setLayerVisibility(record.data.layerview.split(","));
                    }
                }
            }, this);
        }, this);

        ViewList.superclass.initComponent.apply(this, arguments);
    }
});

// FavoriteView - панель видов
FavoriteView = function(mapViewer, conn) {

    this.mapViewer = mapViewer;
    this.viewList = new ViewList(mapViewer)
    this.conn = conn;

    this.addEvents({
        reload: true,
        clearselectview: true
    });

    this.on('reload', function(){
        this.viewList.store.load();
    }, this);

    this.on('clearselectview', function(){
        this.viewList.clearSelections();
    }, this);

    this.insertButton = new Ext.Button({
        text : 'Вставить',
        iconCls: 'icon-add',
        handler : function()
        {
            this.fireEvent('insert')
        },
        scope : this
    });

    this.removeButton = new Ext.Button({
        text : 'Удалить',
        iconCls: 'icon-delete',
        handler : function()
        {
             this.fireEvent('remove')
        },
        scope : this
    });

    this.updateButton = new Ext.Button({
        text : 'Обновить',
        iconCls: 'icon-refresh',
        handler : function()
        {
             this.fireEvent('update')
        },
        scope : this
    });

    this.clearButton = new Ext.Button({
        text : 'Очистить',
        iconCls: 'icon-clear',
        handler : function()
        {
             this.fireEvent('clear')
        },
        scope : this
    });

    this.saveButton = new Ext.Button({
        iconCls: 'icon-save',
        text : 'Сохранить',
        disabled: true,
        handler : function()
        {
             this.fireEvent('save')
        },
        scope : this
    });

    this.cancelButton = new Ext.Button({
          iconCls: 'icon-close-cross',
          text : 'Отмена',
          disabled: true,
          handler : function()
          {
               this.fireEvent('cancel')
          },
          scope : this
     });

    FavoriteView.superclass.constructor.call(this, {
        id:'favorite-view',
        collapsible : false,
        margins : '0 0 0 0',
        layout : 'fit',
        autoScroll : true,
        tbar: [
                this.insertButton,
                this.removeButton,
                '-',
                this.updateButton,
                this.clearButton,
                '-',
                this.saveButton,
                this.cancelButton
            ]
        /*new Ext.Toolbar({
            //statusAlign: 'right',
            items: [
                this.insertButton,
                this.removeButton,
                '-',
                this.updateButton,
                this.clearButton,
                '-',
                this.saveButton,
                this.cancelButton
            ]
        })*/,
        items: [this.viewList]
    });
}
Ext.extend(FavoriteView, Ext.Panel, {
    initComponent : function() {

        this.addEvents({
            insert : true,
            remove : true,
            save: true,
            cancel: true
        });

        // insert view
        this.on('insert', function() {
            if (Map.FavoriteView.selected != 0)
            {
                var viewerApi = this.mapViewer.Api;
                var number = Map.FavoriteView.selected;
                var scale = viewerApi.Map.GetScale();
                var centerX = viewerApi.Map.GetCenter().X;
                var centerY = viewerApi.Map.GetCenter().Y;

                viewerApi.Map.ClearSelection();

                //var msgWait = Ext.WgsMsg.wait('Добавление вида...', 'Подождите', {interval: 200});
                var loadMask = new Ext.LoadMask(Map.FavoriteView.window.getEl(), {msg: 'Добавление вида...'});
                loadMask.show();
                this.conn.request({
                   url: '/map/favoriteview/insertview',
                   params: { number: number,
                             'scale': scale,
                             'centerX': centerX,
                             'centerY': centerY,
                             'WIDTH': viewerApi.Map.GetMapDimension().width,
                             'HEIGHT': viewerApi.Map.GetMapDimension().height,
                             'MAPNAME': encodeURIComponent(viewerApi.Map.GetMapName()),
                             'SESSION': viewerApi.Map.GetSessionId()
                           },
                   success: function(response){
                        this.viewList.store.each(function(record){
                            if (record.data.number == number) {
                                record.data.url = response.responseText;
                                if (!record.data.name)
                                record.data.name = 'Вид '+number;
                                record.data.centerX = centerX;
                                record.data.centerY = centerY;
                                record.data.scale = scale;
                            }
                        });
                        this.viewList.refresh();
                        this.viewList.clearSelections();
                        Map.FavoriteView.selected = 0;
                        loadMask.hide();
                   },
                   scope: this
                });

            }
            else Ext.WgsMsg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо выбрать ячейку.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                 });
        }, this);

         // remove view
        this.on('remove', function() {
            if (Map.FavoriteView.selected != 0)
            {
                if (this.viewList.store.getAt(Map.FavoriteView.selected-1).data.scale) {
                    var number = Map.FavoriteView.selected;
                    //var msgWait = Ext.WgsMsg.wait('Удаление вида...', 'Подождите', {interval: 50});
                    var loadMask = new Ext.LoadMask(Map.FavoriteView.window.getEl(), {msg: 'Удаление вида...'});
                    loadMask.show();
                    this.conn.request({
                       url: '/map/favoriteview/removeview',
                       params: { number: number},
                       success: function(response){
                            this.viewList.store.each(function(record){
                                if (record.data.number == number) {
                                    record.data.url = '/images/favorite_'+number+'.png';
                                    record.data.scale = '';
                                    record.data.name = 'Вид '+number;
                                }
                            });
                            this.viewList.refresh();
                            this.viewList.clearSelections();
                            Map.FavoriteView.selected = 0;
                            //msgWait.hide();
                            loadMask.hide();
                       },
                       scope: this
                    });
                }
            }
            else Ext.WgsMsg.show({
                    title: 'Предупреждение',
                    msg: 'Необходимо выбрать ячейку.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                 });
        }, this);

        this.on('update', function() {

            var viewerApi = this.mapViewer.Api;
            viewerApi.Map.ClearSelection();
            var params = [];
            this.viewList.store.each(function(record){
                if (record.data.scale) {
                    params[params.length] = record.data;
                }
            });

            //var msgWait = Ext.WgsMsg.wait('Обновление избранных видов...', 'Подождите', {interval: 200});
            var loadMask = new Ext.LoadMask(Map.FavoriteView.window.getEl(), {msg: 'Обновление...'});
            loadMask.show();
            this.conn.request({
               url: '/map/favoriteview/updateviewlist',
               params: { param: Ext.util.JSON.encode(params),
                         'WIDTH': viewerApi.Map.GetMapDimension().width,
                         'HEIGHT': viewerApi.Map.GetMapDimension().height,
                         'MAPNAME': encodeURIComponent(viewerApi.Map.GetMapName()),
                         'SESSION': viewerApi.Map.GetSessionId()
                       },
               success: function(response){
                    var responseArray = Ext.util.JSON.decode(response.responseText);
                    this.viewList.store.each(function(record){
                        if (responseArray[record.data.number]) {
                            record.data.url = responseArray[record.data.number].url;
                        }
                    });
                    this.viewList.refresh();
                    this.viewList.clearSelections();
                    Map.FavoriteView.selected = 0;
                    //msgWait.hide();
                    loadMask.hide();
               },
               scope: this
            });
        }, this);

        // clear all view
        this.on('clear', function() {
            Ext.WgsMsg.confirm('Подтверждение удаления', 'Вы действительно хотите удалить избранные виды?',
                function(btn) {
                    if (btn == 'yes')
                    {
                        var params = [];
                        this.viewList.store.each(function(record){
                            if (record.data.scale) {
                                params[params.length] = record.data;
                            }
                        });

                        var loadMask = new Ext.LoadMask(Map.FavoriteView.window.getEl(), {msg: 'Удаление видов...'});
                        loadMask.show();
                        this.conn.request({
                           url: '/map/favoriteview/clearview',
                           params: {param: Ext.util.JSON.encode(params)},
                           success: function(response){
                                this.viewList.store.each(function(record){
                                    if (record.data.scale) {
                                        record.data.url = '/images/favorite_'+record.data.number+'.png';
                                        record.data.scale = '';
                                        record.data.name = 'Вид '+record.data.number;
                                    }
                                });
                                this.viewList.refresh();
                                this.viewList.clearSelections();
                                Map.FavoriteView.selected = 0;
                                loadMask.hide();
                           },
                           scope: this
                        });
                    }
                }, this)
        }, this);

        this.on('save', function() {
            //var msgWait = Ext.WgsMsg.wait('Сохранение изменений...', 'Подождите', {interval: 32});
            var loadMask = new Ext.LoadMask(Map.FavoriteView.window.getEl(), {msg: 'Сохранение...'});
            loadMask.show();
            var modifiedRecords = this.viewList.store.getModifiedRecords();
            var params = [];
            Ext.each(modifiedRecords, function(record){
                params[params.length] = record.data;
            });
            this.conn.request(
            {
                url: '/map/favoriteview/updatename',
                method: 'POST',
                params: {param: Ext.util.JSON.encode(params)},
                success: function(){
                    //msgWait.hide();
                     loadMask.hide();
                }
            });

            this.viewList.store.commitChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
        })

        this.on('cancel', function() {
            this.viewList.store.rejectChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
        })

        this.viewList.store.on('update', function() {
            this.saveButton.enable();
            this.cancelButton.enable();
        }, this);

        FavoriteView.superclass.initComponent.apply(this, arguments);
    }
});