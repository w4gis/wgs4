﻿Ext.namespace('Map');

Map.ViewerApi = function(mainFrame)
{    
    if (mainFrame) {
        this.main = mainFrame;
    }
    else {
        this.main = frames.WgsMapFrame;
    }
    
    if (!this.main || !this.main.GetMapFrame || !this.main.GetMapFrame()) {
        alert('frame model not found');
        return;
    }

    var map     = this.main.GetMapFrame();
    this.map    = map;
    var isDWF   = this.main.dwf? true: false;
    var isAJAX  = !isDWF;
    this.isDWF  = isDWF;
    this.isAJAX = isAJAX;
    
    // AJAX
    
    Map.ViewerApi.Legend = function(legend) {
        this.legend = legend;
    }
    
    Map.ViewerApi.Legend.prototype = {
        GetBaseGroups: function(onlyVisible)
        {
            if (this.getLegend()) {
                return this.getLegend().GetBaseGroups(onlyVisible);
            }
        },
        GetLayer: function(layerId)
        {
            if (this.getLegend()) {
                return this.getLegend().GetLayer(layerId);
            }
        },
        GetLayers: function(onlyVisible, onlySelectable)
        {
            if (this.getLegend()) {
                return this.getLegend().GetLayers(onlyVisible, onlySelectable);
            }
        },
        InternalStateComplete: function()
        {
            if (this.getLegend()) {
                return this.getLegend().InternalStateComplete();
            }
        },
        Refresh: function()
        {
            if (this.getLegend()) {
                return this.getLegend().InternalStateComplete();
            }
        },
        SetScale: function(scale)
        {
            if (this.getLegend()) {
                return this.getLegend().SetScale(scale);
            }
        },
        ExpandNodes: function(state)
        {
            if (this.getLegend()) {
                return this.getLegend().thisFrame.legendUiFrame.ExpandNodes(this.getLegend().thisFrame.legendUiFrame.tree, state);
            }
        },
        /*
            my functions
        */
        getLegend: function()
        {
            if (!this.legend && isAJAX) {
                this.legend = map.GetLegendCtrl();
            }
            return this.legend;
        }
    }

    Map.ViewerApi.Propertybar = function(propertybar) {
        this.propertybar = propertybar;
    }
    
    Map.ViewerApi.Propertybar.prototype = {
        SetProperties: function(count, properties)
        {

            if (this.getPropertybar()) {
                return this.getPropertybar().SetProperties(count, properties);
            }
        },
        /*
            my functions
        */
        getPropertybar: function()
        {
            if (!this.propertybar && isAJAX) {
                this.propertybar = map.GetPropertyCtrl();
            }
            return this.propertybar;
        }
    }
    
    // AJAX & DWF
    
    Map.ViewerApi.Statusbar = function(main) {
        this.main = main;
    }
    
    Map.ViewerApi.Statusbar.prototype = {
        SetCursorPositionMsg: function(msg)
        {
            if (this.main.sbFrame) {
                return this.main.sbFrame.SetCursorPositionMsg(msg);
            }
        },
        SetFeatureSelectedMsg: function(msg)
        {
            if (this.main.sbFrame) {
                return this.main.sbFrame.SetFeatureSelectedMsg(msg);
            }
        },
        SetMapScaleMsg: function(msg)
        {
            if (this.main.sbFrame) {
                return this.main.sbFrame.SetMapScaleMsg(msg);
            }
        },
        SetMapSizeMsg: function(msg)
        {
            if (this.main.sbFrame) {
                return this.main.sbFrame.SetMapSizeMsg(msg);
            }
        }
    }
    
    Map.ViewerApi.Toolbar = function(main) {
        this.main = main;
    }
    
    Map.ViewerApi.Toolbar.prototype = {
        OnCommandExecuted: function(action)
        {
            if (this.main.tbFrame) {
                return this.main.tbFrame.OnCommandExecuted(action);
            }
        },
        OnCmdStateChanged: function()
        {
            if (this.main.tbFrame) {
                return this.main.tbFrame.OnCmdStateChanged();
            }            
        }
    }
    
    Map.ViewerApi.Map = function(map) {
        this.map = map;
        this.coordinateSystem = 'building'; 
        this.getMap = function()
        {
            return this.map;
        }
    }
    
    Map.ViewerApi.Map.prototype = {
        ClearSelection: function()
        {
            return this.map.ClearSelection();
        },
        DigitizeCircle: function(handler)
        {
            return this.map.DigitizeCircle(handler);
        },
        DigitizePoint: function(handler)
        {
            return this.map.DigitizePoint(handler);
        },
        DigitizeLine: function(handler)
        {
            return this.map.DigitizeLine(handler);
        },
        DigitizePolygon: function(handler)
        {
            return this.map.DigitizePolygon(handler);
        },
        DigitizeLineString: function(handler)
        {
            return this.map.DigitizeLineString(handler);
        },
        DigitizeRectangle: function(handler)
        {
            return this.map.DigitizeRectangle(handler);
        },
        GetCenter: function()
        {
            return this.map.GetCenter();
        },
        GetLayers: function(onlyVisible, onlySelectable)
        {
            return this.map.GetLayers(onlyVisible, onlySelectable);
        },
        GetMapHeight: function()
        {
            return this.map.GetMapHeight();
        },
        GetMapName: function()
        {
            return this.map.GetMapName();
        },
        GetMapUnitsType: function()
        {
            return this.map.GetMapUnitsType();
        },
        GetMapWidth: function()
        {
            return this.map.GetMapWidth();
        },
        GetMetersPerUnits: function()
        {
            return this.map.GetMetersPerUnits();
        },
        GetScale: function()
        {
            return this.map.GetScale();
        },
        GetSelectedLayers: function()
        {
            return this.map.GetSelectedLayers();
        },
        GetSelectionXML: function()
        {
            return this.map.GetSelectionXML();
        },
        GetSelection: function()
        {
            return this.map.selection;
        },
        GetSessionId: function()
        {
            return this.map.GetSessionId();
        },
        GetSelectedCount: function()
        {
            return this.map.GetSelectedCount();
        },
        IsDigitizing: function()
        {
            return this.map.IsDigitizing();
        },
        IsEnglishUnits: function()
        {
            return this.map.IsEnglishUnits();
        },
        IsLatLonDisplayUnits: function()
        {
            return this.map.IsLatLonDisplayUnits();
        },
        MapUnitsToLatLon: function(x, y)
        {
            return this.map.MapUnitsToLatLon(x, y);
        },
        Refresh: function()
        {
            return this.map.Refresh();
        },
        ScreenToMapUnits: function(x, y)
        {
            return this.map.ScreenToMapUnits(x, y);
        },
        SetEnglishUnits: function(usEnglish)
        {
            return this.map.SetEnglishUnits(usEnglish);
        },
        SetLatLonDisplayUnits: function(latLon)
        {
            return this.map.SetLatLonDisplayUnits(latLon);
        },
        SetSelectionXML: function(xmlSet)
        {
            return this.map.SetSelectionXML(xmlSet);
        },
        ZoomToView: function(x, y, scale, refresh)
        {
            return this.map.ZoomToView(x, y, scale, refresh);
        },
        SetCoordinateSystem: function(coordinateSystem)
        {
            this.coordinateSystem = coordinateSystem;
        },
        GetCoordinateSystem: function()
        {
            return this.coordinateSystem;
        },
        /*
            my functions
        */
        // Событие успешного завершения транзакции DWFViewer-а.
        // Обработчик назначается перед той транзакцией, завершение которой необходимо обработать в асинхронном режиме.
        // Например, перед вызовом методов Refresh или ZoomToView.
        OnEndTransaction: function(callback, scope)
        {
            if (isDWF) {
                this.endTransaction = callback.createDelegate(scope);
                var OnEndTransaction = function(itemName, data, res) {
                    if(itemName == 'TRANSACTION') {
                        this.afterEndTransaction.call();
                        this.endTransaction.call();
                    }
                }.createDelegate(this);
                this.afterEndTransaction = function() {
                    Event.stopObserving(this.map.DWFViewer, 'EndLoadItem', OnEndTransaction);
                }.createDelegate(this);
                Event.observe(this.map.DWFViewer, 'EndLoadItem', OnEndTransaction);
            } else {
                callback.call(scope);
            }
        },
        OnClickMap: function(handler, scope)
        {
            if (!this.clickMap) {
                this.clickMap = this.map.parent.ClickMap;
            }
            this.map.parent.ClickMap = function(x, y)
            {
                if (typeof handler == "function") {
                    handler.call(scope, x, y);
                }
                this.clickMap.call(this, x, y);
            }.createDelegate(this);
        },
        StopClickMap: function ()
        {
        	if (this.clickMap) {
        		this.map.parent.ClickMap = this.clickMap;
        	}
        },
        OnCursorPosChanged: function(handler, scope)
        {
            if (!this.cursorPosChanged) {
                this.cursorPosChanged = this.map.parent.OnCursorPosChanged;
            }
            this.map.parent.OnCursorPosChanged = function(x,y)
            {
                this.cursorPosChanged.call(this, x, y);
                
                if (typeof handler == "function") {
                    handler.call(scope||this, x, y);
                }
                
            }.createDelegate(this);
        },
        OnMouseDown: function (handler, scope)
        {
            if(!this.mouseDown) {
                this.mouseDown = this.map.OnMouseDown;
            }
            this.map.OnMouseDown = function(e)
            {
                if (typeof handler == "function") {
                    handler.call(scope, e);
                }
            }.createDelegate(this);
        },
        
        StopMouseDown: function ()
        {
            if (this.mouseDown) {
                this.map.OnMouseDown = this.mouseDown;
            }
        },
        
        OnMouseMove: function (handler, scope)
        {
            if(!this.mouseMove) {
                this.mouseMove = this.map.OnMouseMove;
            }
            this.map.OnMouseMove = function(e)
            {
                if (typeof handler == "function") {
                    handler.call(scope, e);
                }
            }.createDelegate(this);
        },
        
        StopMouseMove: function ()
        {
            if (this.mouseMove) {
                this.map.OnMouseMove = this.mouseMove;
            }
        },
        
        OnMouseUp: function (handler, scope)
        {
            if(!this.mouseUp) {
                this.mouseUp = this.map.OnMouseUp;
            }
            this.map.OnMouseUp = function(e)
            {
                if (typeof handler == "function") {
                    handler.call(scope, e);
                }
            }.createDelegate(this);
        },
                
        StopMouseUp: function ()
        {
            if (this.mouseUp) {
                this.map.OnMouseUp = this.mouseUp;
            }
        },
        
        OnDblClick: function (handler, scope)
        {
            if(!this.dblClick) {
                this.dblClick = this.map.OnDblClick;
            }
            this.map.OnDblClick = function(e)
            {
                if (typeof handler == "function") {
                    handler.call(scope, e);
                }
            }.createDelegate(this);
        },
                
        StopDblClick: function ()
        {
            if (this.dblClick) {
                this.map.OnDblClick = this.dblClick;
            }
        },
                       
        OnContextMenu: function (handler, scope)
        {
            if(!this.contextMenu) {
                this.contextMenu = this.map.OnContextMenu;
            }
            this.map.OnContextMenu = function(e)
            {
                if (typeof handler == "function") {
                    handler.call(scope, e);
                }
            }.createDelegate(this);
        },
        
        StopContextMenu: function ()
        {
            if (this.contextMenu) {
                this.map.OnContextMenu = this.contextMenu;
            }
        },
        
        IsAJAX: function()
        {
            return isAJAX;
        },
        IsDWF: function()
        {
            return !isAJAX;
        },
        // Получить размер карты в пикселях
        GetMapDimension: function()
        {            
            var mapDevH, mapDevW, legendW;
            if (isAJAX) {
                var consolePaneHeight = (Ext.getCmp('consolePane'))? Ext.getCmp('consolePane').getSize().height: 0;
                mapDevH = this.map.mapDevH;// - (consolePaneHeight + 80);
                mapDevW = this.map.mapDevW;
                legendW = this.map.infoWidth;
            } else {
                mapDevH = $(this.map.DWFViewer).getHeight(); 
                mapDevW = $(this.map.DWFViewer).getWidth() - this.map.NavpaneWidth;
                legendW = this.map.NavpaneWidth;
            }
            return {width: mapDevW, height: mapDevH, legendWidth: legendW};
        },
        // Наблюдать событие OnMapLoaded 
        // handler - обработчик
        // scope - контекст
        // single - выполнить обработчик один раз
        // condition - функция условия выполнения события, если true, то выполняется
        ObserveOnMapLoaded: function(handler, scope, single, condition)
        {
            var scope = scope||this;
            var single = single||false;
            var condition = condition||function() {return true};
            if (isAJAX) {
                if (!this.map.OnMapLoaded || !this.map.OnMapLoaded.handlers) {
                    var onMapLoaded = this.map.OnMapLoaded;
                    this.map.OnMapLoaded = function()
                    {
                        onMapLoaded.call();
                        for (var key in onMapLoaded.prototype.handlers) {
                            if (onMapLoaded.prototype.handlers[key]) {
                                if (onMapLoaded.prototype.handlers[key].condition.call(onMapLoaded.prototype.handlers[key].scope)) {
                                    onMapLoaded.prototype.handlers[key].func.call(onMapLoaded.prototype.handlers[key].scope);
                                }
                                if (onMapLoaded.prototype.handlers[key]) {
                                    if (onMapLoaded.prototype.handlers[key].single) {
                                        delete onMapLoaded.prototype.handlers[key];
                                    }
                                }
                            }
                        }
                    }
                    onMapLoaded.prototype.handlers = {};
                    this.map.OnMapLoaded.handlers = onMapLoaded.prototype.handlers;
                }
                var handlerExists = false
                for (var key in this.map.OnMapLoaded.handlers) {
                    if (this.map.OnMapLoaded.handlers[key].func == handler) {
                        handlerExists = true;
                        break;
                    }                    
                }
                if (!handlerExists) {
                    this.map.OnMapLoaded.handlers[handler] = {func: handler, scope: scope, single: single, condition: condition};
                }                
            } else {//TODO: single handling
                if (!this.OnMapLoadedHandlers) {
                    this.OnMapLoadedHandlers = {};
                }
                if (!this.OnMapLoadedHandlers[handler]) {
                    this.OnMapLoadedHandlers[handler] = function(itemName, data, res) {
                        if(itemName == 'TRANSACTION') {
                            handler.call(scope);
                        }
                    };
                    Event.observe(this.map.DWFViewer, 'EndLoadItem', this.OnMapLoadedHandlers[handler]);
                }
            }
        },
        // Прекратить наблюдение события OnMapLoaded
        StopObservingOnMapLoaded: function(handler)
        {
            if (isAJAX) {
                for (var key in this.map.OnMapLoaded.handlers) {
                    if (this.map.OnMapLoaded.handlers[key].func == handler) {
                        delete this.map.OnMapLoaded.handlers[key];
                        break;
                    }
                }
            } else {
                if (this.OnMapLoadedHandlers[handler]) {
                    Event.stopObserving(this.map.DWFViewer, 'EndLoadItem', this.OnMapLoadedHandlers[handler]);
                    delete this.OnMapLoadedHandlers[handler];
                }
            }
        },
        // Наблюдать событие OnSelectionChanged 
        ObserveOnSelectionChanged: function(handler, scope, single)
        {
            var single = single||false;
            if (!this.OnSelectionChangedHandlers) {
                this.OnSelectionChangedHandlers = {};
            }
            if (!this.OnSelectionChangedHandlers[handler]) {
                this.OnSelectionChangedHandlers[handler] = handler;
                var self = this;
                var condition = function() {
                    if (condition.prototype.selectionXML != self.map.GetSelectionXML()) {
                        condition.prototype.selectionXML = self.map.GetSelectionXML();
                        return true;                        
                    }
                    return false;
                }                
                this.ObserveOnMapLoaded(this.OnSelectionChangedHandlers[handler], scope, single, condition);
            }
            
        },
        //Прекратить наблюдение события OnSelectionChanged
        StopObservingOnSelectionChanged: function(handler)
        {
            if (this.OnSelectionChangedHandlers[handler]) {
                this.StopObservingOnMapLoaded(this.OnSelectionChangedHandlers[handler]);
                delete this.OnSelectionChangedHandlers[handler];
            }
        }
    }
        
    this.Legend = new Map.ViewerApi.Legend(this.legend);
    this.Propertybar = new Map.ViewerApi.Propertybar(this.propertybar);
    this.Statusbar = new Map.ViewerApi.Statusbar(this.main);
    this.Toolbar = new Map.ViewerApi.Toolbar(this.main);
    this.Map = new Map.ViewerApi.Map(this.map);
}

Map.ViewerApi.prototype = {
    SetStatusMsg: function(msg)
    {
        return this.main.SetStatusMsg(msg);
    },
    ExecuteCommand: function(index)
    {
        return this.main.ExecuteCommand(index);
    },
    ExecuteMapAction: function(code)
    {
        return this.main.ExecuteCommand(code);
    },
    FormatMessage: function(fmt, params)
    {
        return this.main.FormatMessage(fmt, params);
    },
    ForwardMouseDown: function(doc, evt)
    {
        return this.main.ForwardMouseDown(doc, evt);
    },
    GetCommands: function()
    {
        return this.main.GetCommands();
    },
    GetContextMenuItems: function()
    {
        return this.main.GetContextMenuItems();
    },
    GetToolbarItems: function()
    {
        return this.main.GetToolbarItems();
    },
    GetInMeasure: function()
    {
         return this.main.inMeasure;
    },
    SetInMeasure: function(value)
    {
         this.main.inMeasure = value;
    },
    GetOriginalScale: function(){
        return this.main.scale;
    },
    GetOriginalCenter: function(){
        return this.main.center;
    }
}

Map.ViewerApi.Instance = function(mapFrame, callback, scope)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[mapFrame] && frames[mapFrame]) {
        var timeoutFn = function() { 
            try {
                if (typeof(frames[mapFrame].GetMapFrame().OnMapLoaded) == 'undefined' && typeof(frames[mapFrame].GetMapFrame().DWFViewer) == 'undefined') {
                    throw(1);
                }
            } catch(e) {
                var self = this;
                window.setTimeout(function(){
                    timeoutFn.call(self);
                }, 100);
                return;
            }
            this.instances[mapFrame] = new Map.ViewerApi(frames[mapFrame]);
            if (callback) {
                callback.call(scope||this, this.instances[mapFrame], frames[mapFrame]);
            }
        }
        timeoutFn.call(this);
    } else {
        if (callback) {
            callback.call(scope||this, this.instances[mapFrame], frames[mapFrame]);
        }
    }
}
