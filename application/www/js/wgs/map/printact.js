﻿Ext.namespace('Map');

Map.PrintAct = Ext.extend(Map.Print, {
	constructor: function(arg){
		Map.PrintAct.superclass.constructor.call(this, arg);
		
		this.window.setTitle('Печать акта в PDF');
	},
	
	getParameters: function(){
		var parameters = Map.PrintAct.superclass.getParameters.call(this);
		parameters.act = 1;
		return parameters;
	}
});

Map.PrintAct.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.PrintAct(map);
    }    
    return this.instances[map.mapFrame];
}