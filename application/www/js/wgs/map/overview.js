﻿Ext.namespace('Map');

Map.Overview = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;
    this.wgsConn = new Ext.data.WgsConnection();
    this.conn = new Ext.data.Connection();
    this.panel = new Ext.WgsServicePanel({
        width: 200,
        height: 200
    });
	var self = this
    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        width: this.panel.width+12,
        title: 'Общий вид',
        resizable: false,
		minimizable: false,           
		onPosition: function(){
		var s = self.resizable.el.getSize()
			self.setCurrentMapPosition.call(self);
			self.resizable.resizeTo(s.width,s.height)
			self.resizable.resizeElement()
		}
    });
    this.window.setInnerPanel(this.panel);
	
}

Map.Overview.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Overview(map);		
    }
    return this.instances[map.mapFrame];
}

Map.Overview.prototype = {
    updateMapPosition: function()
    {
        var DPI = 96;
        var metersPerPixel = 0.0254 / DPI;
        var map = this.viewerApi.Map;
        var x   = this.resizableDiv.getX() - this.overviewImage.getX()+2;
        var y   = this.resizableDiv.getY() - this.overviewImage.getY()+1;
        var w   = this.resizableDiv.getWidth();
        var h   = this.resizableDiv.getHeight();
        var nlx = parseFloat(this.parameters.lx) + parseFloat(this.parameters.aw * x / this.parameters.mw);
        var nly = parseFloat(this.parameters.ly) + parseFloat(this.parameters.ah * (this.parameters.mh-y) / this.parameters.mh);
        var nrx = parseFloat(this.parameters.lx) + parseFloat(this.parameters.aw * (x+w) / this.parameters.mw);
        var nry = parseFloat(this.parameters.ly) + parseFloat(this.parameters.ah * (this.parameters.mh-(y+h)) / this.parameters.mh);
        var ncx = (nrx + nlx) / 2;
        var ncy = (nry + nly) / 2;
        var nw  = nrx - nlx;
        var nh  = nly - nry;
        var nscale;
        if (map.GetMapDimension().height * metersPerPixel * nw > map.GetMapDimension().width * metersPerPixel * nh)
            nscale = nw / (map.GetMapDimension().width * metersPerPixel); // width-limited
        else
            nscale = nh / (map.GetMapDimension().height * metersPerPixel); // height-limited
        map.ZoomToView(ncx, ncy, nscale, true);
    },

    getImage: function(parameters)
    {
        var DPI     = 96;
        var metersPerPixel = 0.0254 / DPI;
        var map     = this.viewerApi.Map;
        var centerX = parameters.CenterX;
        var centerY = parameters.CenterY;
        var mapName = parameters.MapName;
        var mw      = this.panel.width;
        var mh      = this.panel.height-25;
        var scale;
        if (parameters.Width && parameters.Height) {
            if (parameters.Height * metersPerPixel * this.panel.width > parameters.Width * metersPerPixel * this.panel.height){
                scale = parameters.Width / (this.panel.width * metersPerPixel); // width-limited
				
			}
            else
                scale = parameters.Height / (this.panel.height * metersPerPixel); // height-limited
        } else {
            if (map.GetMapDimension().width > map.GetMapDimension().height)
                scale = parameters.Scale * (map.GetMapDimension().width / mw);
            else
                scale = parameters.Scale * (map.GetMapDimension().height / mh);
				
        }
		scale = parameters.Scale
        this.parameters = {
            scale: scale,
            centerX: centerX,
            centerY: centerY,
            mapName: mapName,
            mw: mw,
            mh: mh
        };
        this.conn.request({
            url: '/mapguide/mapagent/mapagent.fcgi',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {
                OPERATION: 'GETVISIBLEMAPEXTENT',
                VERSION: '1.0.0',
                PERSIST: 0,
                MAPNAME: this.parameters.mapName,
                SESSION: map.GetSessionId(),
                SEQ: Math.random(),
                SETDISPLAYDPI: 96,
                SETDISPLAYHEIGHT: this.parameters.mh,
                SETDISPLAYWIDTH: this.parameters.mw,
                SETVIEWCENTERX: this.parameters.centerX,
                SETVIEWCENTERY: this.parameters.centerY,
                SETVIEWSCALE: this.parameters.scale
            },
            success: function(response) {
                var xCoordinates = response.responseXML.getElementsByTagName('X');
                var yCoordinates = response.responseXML.getElementsByTagName('Y');
                this.parameters.lx = parseFloat(xCoordinates.item(0).firstChild.nodeValue);
                this.parameters.rx = parseFloat(xCoordinates.item(1).firstChild.nodeValue);
                this.parameters.ly = parseFloat(yCoordinates.item(0).firstChild.nodeValue);
                this.parameters.ry = parseFloat(yCoordinates.item(1).firstChild.nodeValue);
                this.parameters.aw = this.parameters.rx - this.parameters.lx;
                this.parameters.ah = this.parameters.ry - this.parameters.ly;
                this.overviewImage = this.panel.body.createChild({tag: 'div', style: "width:100%;height:100%;background-repeat:no-repeat;background-image:url(/mapguide/mapagent/mapagent.fcgi?OPERATION=GETDYNAMICMAPOVERLAYIMAGE&FORMAT=PNG&VERSION=1.0.0&SESSION="+map.GetSessionId()+"&MAPNAME="+encodeURIComponent(this.parameters.mapName)+"&SEQ="+Math.random()+")"});
                this.resizableDiv  = this.overviewImage.createChild({tag: 'div', id:'overviewDiv'});
                this.resizableDiv.createChild({tag: 'table', cellspacing: 0, cellpadding: 0, style: 'width:100%;height:100%;border:2px solid #e00;cursor:move;', children: [{tag: 'tr', children: [{tag: 'td', html: '&nbsp;', style: 'opacity:0.1;-moz-opacity:0.1;filter:alpha(opacity=10);background-color:#0f0;'}]}]});
                this.resizable = new Ext.Resizable(this.resizableDiv, {
                    //width: this.parameters.mw - 2,
                    //height: this.parameters.mh - 2,
                    pinned: true,
                    maxWidth: this.parameters.mw - 2,
                    maxHeight: this.parameters.mh - 2,
                    minWidth: 20,
                    minHeight: 20,
                    preserveRatio: false,
                    dynamic:true,
                    handles: 'all',
                    draggable: true,
                    transparent: true,
                    constrainTo : this.panel.body
                });
                this.resizableDiv.on('mouseup', this.updateMapPosition, this);
                this.resizable.on('resize', this.updateMapPosition, this);
                this.resizable.on('drag', function(){alert('!')}, this);
                this.setCurrentMapPosition.call(this);
            },
            scope: this
        });
    },

    setCurrentMapPosition: function()
    {
        var map     = this.viewerApi.Map;
        var scale   = map.GetScale();
        var centerX = map.GetCenter().X;
        var centerY = map.GetCenter().Y;
        this.ZoomToView(centerX, centerY, scale);
    },

    ZoomToView: function(cx, cy, scale)
    {
        var map       = this.viewerApi.Map;
        var mapWidth  = map.GetMapWidth();
        var mapHeight = map.GetMapHeight();

        var nlx = cx - mapWidth / 2;
        var nly = cy - mapHeight / 2;
        var nrx = cx + mapWidth / 2;
        var nry = cy + mapHeight / 2;
        var nlx = (nlx < this.parameters.lx)? this.parameters.lx: nlx;
        var nly = (nly < this.parameters.ly)? this.parameters.ly: nly;
        var nrx = (nrx > this.parameters.rx)? this.parameters.rx: nrx;
        var nry = (nry > this.parameters.ry)? this.parameters.ry: nry;

        var x   = ((nlx - this.parameters.lx) / this.parameters.aw) * this.parameters.mw;
        var y   = ((nly - this.parameters.ly) / this.parameters.ah) * this.parameters.mh;
        var rx  = this.parameters.mw - ((this.parameters.rx - nrx) / this.parameters.aw) * this.parameters.mw;
        var ry  = this.parameters.mh - ((this.parameters.ry - nry) / this.parameters.ah) * this.parameters.mh;

        var w   = rx - x;
        var h   = ry - y;

        if (this.resizableDiv) {
            this.resizableDiv.setWidth(w - 2);
            this.resizableDiv.setHeight(h - 2);
            this.resizableDiv.setX(this.overviewImage.getX() + x);
            this.resizableDiv.setY(this.overviewImage.getY() + this.parameters.mh - (y + h));
        }
    },

    getInitParameters: function(callback, scope)
    {
        this.wgsConn.request({
            url: '/map/getoverviewparameters',
            method: 'GET',
            success: function(response) {
                try {
                    var parameters = Ext.util.JSON.decode(response.responseText)
                    this.created = true;
                    callback.call(scope, parameters);
                } catch(e) {
                    var parameters = null;
                    this.created = false;
                }
            },
            scope: this
        });
    },

    create: function()
    {
        this.getInitParameters(function(parameters){
            this.getImage(parameters);
            var onMapLoadedHandler = function() {
                this.setCurrentMapPosition.call(this);
            }
            this.viewerApi.Map.ObserveOnMapLoaded(onMapLoadedHandler, this);
        }, this);
    },

    open: function()
    {
        if (!this.created) {
            this.create();
        } else {
            this.setCurrentMapPosition.call(this);
        }
        this.window.addPanel();
        this.window.show();
		
    }
}