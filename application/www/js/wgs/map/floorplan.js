﻿Ext.namespace('Map');

Map.Floorplan = Ext.extend(Ext.util.Observable, {
	constructor: function(mapViewer)
	{
	    this.mapViewer = mapViewer;
	    this.previewWindow = this.previewWindow||{};
	    Map.Floorplan.superclass.constructor.call(this);
	},
	
	open: function(mapViewer)
	{
        if (this.mapViewer) {
            this.mapViewer.Lib.getSelectedFeatures(function(featureLayers){
                if(featureLayers.length > 0) {
                    var featureId;
                    for (var i = 0; i < featureLayers.length; i++) {
                        for (var j = 0; j < featureLayers[i].features.length; j++) {
                        	featureId = featureLayers[i].features[j].featureId;
                        	this.openFloorplanWindow.defer(1000, this, [featureId]);
                        }
                    }
                } else {
                    Ext.WgsMsg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо выбрать объекты',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }, this);
        }
	},
	
	openFloorplanWindow: function(featureId, selectFeatureId) {
        this.previewWindow[featureId] = new Ext.WgsShimWindow({
            width: 750,
            maximizable: true,
            resizable: true,
            layout: 'fit',
            closeAction: 'hide',
            title: 'Отображение объектов'
        });
        this.previewMapViewer = new Map.Viewer({
            viewerType: 'ajax',
            url: '/map/createfloorplan?featureId='+featureId,
            render: function(viewerPanel) {
                viewerPanel.setWidth(740);
                viewerPanel.setHeight(480);
                this.previewWindow[featureId].add(viewerPanel);
                this.previewWindow[featureId].show();
            },
            load: function(api, lib, commands) {
            	if (selectFeatureId) {
            		lib.selectFeatures([selectFeatureId]);
            	}
            },
            scope: this
        });
	}
});

Map.Floorplan.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    map = map||{mapFrame: true};
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Floorplan(map);
    }    
    return this.instances[map.mapFrame];
}