﻿Ext.namespace('Map');

Map.ReportPPR = function(mapViewer)
{
    ///////////////////////

    Ext.ns('Wgs.Map.ReportPPR');

    ///////////////////////
    //Wgs.Map.ReportPPR.TestArray = 	[ 388787, 388786, 388809, 388853, 388852, 388812, 388851, 388811, 388810, 388850, 388849, 388788 ];
    //Wgs.Map.ReportPPR.TestArray = 	[388853, 388852];
    Wgs.Map.ReportPPR.MapViewer = mapViewer;
    Wgs.Map.ReportPPR.ViewerLib = mapViewer.Lib;
    Wgs.Map.ReportPPR.ViewerApi = mapViewer.Api;
    Wgs.Map.ReportPPR.WgsConn = new Ext.data.WgsConnection();

    Wgs.Map.ReportPPR.ReportPanel = new Map.ReportPPR.ReportPanel;

    Wgs.Map.ReportPPR.Window = new Ext.WgsServiceWindow({
        containerPanel: Wgs.Map.ReportPPR.MapViewer.rightPane,
        width: 670,
        //height: 300,
        layout: 'fit',
        //autoHeight: true,
        title: 'Отчет ППР',
        resizable: true,
        minHeight: 400,
        minWidth: 670,
        maximizable: true
    });

    Wgs.Map.ReportPPR.MapViewer.addChildComponent(Wgs.Map.ReportPPR.Window);
    Wgs.Map.ReportPPR.Window.setInnerPanel(Wgs.Map.ReportPPR.ReportPanel);

    Wgs.Map.ReportPPR.FilterObjects = [];
    //Wgs.Map.ReportPPR.FilterObjectsIndex = {};
    Wgs.Map.ReportPPR.PolygonObjects = [];
    //Wgs.Map.ReportPPR.PolygonObjectsIndex = {};
    Wgs.Map.ReportPPR.AllObjects = [];
    Wgs.Map.ReportPPR.AllObjectsIndex = {};

    ////////METHODS////////

    this.getWindow = function() {
        return Wgs.Map.ReportPPR.Window;
    }

    this.open = function(config) {
        Wgs.Map.ReportPPR.Window.addPanel();
        Wgs.Map.ReportPPR.Window.show();
    }

    with (Wgs.Map.ReportPPR.ReportPanel) {

		exportButton.on('click', function() {
            exportButton.disable();

            new Ext.data.WgsConnection().request({
                url: '/map/report/GetExcelResultPPR',
                method: 'POST',
                params: {
                    'featureIds[]': Wgs.Map.ReportPPR.AllObjects,
                    'year': yearCombo.getValue()
                },
                success: function(response) {
                    exportButton.enable();
                    if (Ext.isIE) {
                    	window.open("/map/report/GetExcel/?id="+response.responseText, "excel");
                    } else {
                    	window.open ("/map/report/GetExcelFile/"+response.responseText, "excel");
                    }
                }
            });
        });


        var calculateObjectsCount = function(radio) {

            Wgs.Map.ReportPPR.AllObjectsIndex = {};
            Wgs.Map.ReportPPR.AllObjects = [];

            if (intersectSelectionRadio.getValue()) {
                for (var i = 0; i < Wgs.Map.ReportPPR.PolygonObjects.length; i++) {
                    for (var j = 0; j < Wgs.Map.ReportPPR.FilterObjects.length; j++) {
                        if (Wgs.Map.ReportPPR.PolygonObjects[i] == Wgs.Map.ReportPPR.FilterObjects[j]) {
                            if (!Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.PolygonObjects[i]]) {
                                Wgs.Map.ReportPPR.AllObjects.push(Wgs.Map.ReportPPR.PolygonObjects[i]);
                                Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.PolygonObjects[i]] = true;
                            }
                        }
                    }
                }

            } else {
                for (var i = 0; i < Wgs.Map.ReportPPR.PolygonObjects.length; i++) {
                    if (!Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.PolygonObjects[i]]) {
                        Wgs.Map.ReportPPR.AllObjects.push(Wgs.Map.ReportPPR.PolygonObjects[i]);
                        Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.PolygonObjects[i]] = true;
                    }
                }
                for (var j = 0; j < Wgs.Map.ReportPPR.FilterObjects.length; j++) {
                    if (!Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.FilterObjects[j]]) {
                        Wgs.Map.ReportPPR.AllObjects.push(Wgs.Map.ReportPPR.FilterObjects[j]);
                        Wgs.Map.ReportPPR.AllObjectsIndex[Wgs.Map.ReportPPR.FilterObjects[j]] = true;
                    }
                }
            }
            selectPolygonLabel.setText('Объектов: ' + Wgs.Map.ReportPPR.PolygonObjects.length);
            objectsFieldSet.setTitle('Выборка объектов ('+ Wgs.Map.ReportPPR.AllObjects.length +')');
            selectFilterLabel.setText('Объектов: ' + Wgs.Map.ReportPPR.FilterObjects.length);

            reportPPRStore.removeAll();
            reportPPRStore.baseParams = {'featureIds[]': Wgs.Map.ReportPPR.AllObjects, 'year': yearCombo.getValue()};
            reportPPRStore.load({
                callback: function(records, options) {
                    //featureTypesFieldSet.setTitle('Описания (' + records.length + ')');
                	exportButton.setDisabled(records.length == 0);
                }

            });
        }

        selectPolygonButton.on('click', function() {
            if (!this.loadMask) {
            	this.loadMask = new Ext.LoadMask(statReportGrid.getEl(), {store: reportPPRStore});
            }
            Wgs.Map.ReportPPR.Window.hide();
            Map.SelectPolygon.Instance(Wgs.Map.ReportPPR.MapViewer).open({
                selectFeaturesOnMap: false,
                callback: function(objects) {
                    Wgs.Map.ReportPPR.Window.on('show', function() {
                    	if(objects!='' && !Ext.isEmpty(objects)) {
                    		Wgs.Map.ReportPPR.PolygonObjects = objects;
                    		calculateObjectsCount();
                    	}
                    	objects = [];
                    });
                    Wgs.Map.ReportPPR.Window.show();
                }
            });
        });

        selectFilterButton.on('click', function() {
            var reportFilter = Map.ReportPPR.Filter.Instance(Wgs.Map.ReportPPR.MapViewer);
            if (!this.loadMask) {
            	this.loadMask = new Ext.LoadMask(statReportGrid.getEl(), {store: reportPPRStore});
            }
            reportFilter.open({
                callback: function(objects) {
                    Wgs.Map.ReportPPR.FilterObjects = objects;
                    calculateObjectsCount();
                },
                scope: this
            });
        });

        resetPolygonButton.on('click', function() {
            Wgs.Map.ReportPPR.PolygonObjects = [];
            calculateObjectsCount();
        });

        resetFilterButton.on('click', function() {
            var reportFilter = Map.ReportPPR.Filter.Instance(Wgs.Map.ReportPPR.MapViewer);
            reportFilter.objects = [];
            reportFilter.objectsIndex = {};
            Wgs.Map.ReportPPR.FilterObjects = [];
            calculateObjectsCount();
        });

        intersectSelectionRadio.on('check', function(checkbox, checked) {
            if (checked) {
                calculateObjectsCount();
            }
        });

        unionSelectionRadio.on('check', function(checkbox, checked) {
            if (checked) {
                calculateObjectsCount();
            }
        });

        yearCombo.on('select', function(combo, record, index) {
        	calculateObjectsCount();
        });

    	cancelButton.on('click', function() {
            Wgs.Map.ReportPPR.Window.hide();
        });

        csModel.on('rowselect', function(sm, rowIndex, record){
			var row = gridView.getRow(rowIndex);
			var element = Ext.get(row);
			element.removeClass(record.data.rowcls);
		});

		csModel.on('rowdeselect', function(sm, rowIndex, record){
			var row = gridView.getRow(rowIndex);
			var element = Ext.get(row);
			element.addClass(record.data.rowcls);
		});
    }
}

Map.ReportPPR.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.ReportPPR(map);
    }
    return this.instances[map.mapFrame];
}

Map.ReportPPR.ReportPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config)
    {
        Ext.QuickTips.init();
        /*
        this.BaseBindPanel = new Map.Bind.BaseBindPanel(config);
        this.InlineBindPanel = new Map.Bind.InlineBindPanel(config);

        this.tabs = new Ext.TabPanel({
            activeTab: 0,
            items: [this.BaseBindPanel, this.InlineBindPanel]
        });
        */
        this.selectPolygonButton = new Ext.Button({
            text: '...'
        });

        this.selectFilterButton = new Ext.Button({
            text: '...'
        });

        this.selectPolygonLabel = new Ext.form.Label({
            text: 'Объектов: 0'
        });

        this.selectFilterLabel = new Ext.form.Label({
            text: 'Объектов: 0'
        });

        this.resetPolygonButton = new Ext.Button({
            text: 'Сброс'
        });

        this.resetFilterButton = new Ext.Button({
            text: 'Сброс'
        });

        this.unionSelectionRadio = new Ext.form.Radio({
            name: 'test',
            checked: true,
            boxLabel: 'Объединение'
        });

        this.intersectSelectionRadio = new Ext.form.Radio({
            name: 'test',
            boxLabel: 'Пересечение'
        });


        var years = [],
			startYear = 2008,
			endYear = 2099;

        for (var i = startYear; i < endYear; i++ ) {
        	years[years.length] = [i];
        }

        this.yearStore = new Ext.data.SimpleStore({
            fields: ['year'],
            data: years
        });

        this.yearCombo = new Ext.form.ComboBox({
	        name:'yearCombo',
	        store: this.yearStore,
	        displayField:'year',
	        valueField: 'year',
	        fieldLabel: 'Год',
	        typeAhead: true,
	        mode: 'local',
	        width: 100,
	        anchor:'100%',
	        triggerAction: 'all',
	        editable:false,
	        selectOnFocus:true
        });

        this.objectsFieldSet = new Ext.form.FieldSet({
            title: 'Выборка объектов (0)',
            autoHeight: true,
            items: [
                new Ext.Panel({
                    layout:'table',
                    defaults: {
                        bodyStyle:'padding: 5px 15px'
                    },
                    layoutConfig: {
                        columns: 6
                    },
                    items: [
                    	{
                        	html: 'Выделить полигоном'
                    	},
                    	this.selectPolygonButton,
                   		this.resetPolygonButton,
                   		{
                        	items: this.selectPolygonLabel,
                        	width: 120
                    	},
                    	this.unionSelectionRadio,
                    	{
                        	html: '<p><center>Год отчета</center></p>'
                    	},
                    	{
                        	html: '<p>Наложить фильтр</p>'
                    	},
                    	this.selectFilterButton,
                    	this.resetFilterButton,
                    	{
                    		items: this.selectFilterLabel,
                    		width: 120
                    	},
                    	this.intersectSelectionRadio,
                    	{
                    		items: this.yearCombo
                    	}

                    ]
                })
            ]
        });

        this.featureTypeStore = new Ext.data.WgsJsonStore({
            autoLoad: true,
            url: '/map/report/getfeaturetypesbyfeatureids',
            fields: ['featureTypeName', 'featureTypeId']
        });

        var record = Ext.data.Record.create([
        	{name: 'objectId'},
        	{name: 'parentObjectId'},
        	{name: 'objectName'},
        	{name: 'objectTypeName'},
        	{name: 'rowcls'},
        	{name: 'price'},
			{name: 'January'},
        	{name: 'February'},
        	{name: 'March'},
        	{name: 'April'},
        	{name: 'May'},
        	{name: 'June'},
        	{name: 'July'},
        	{name: 'August'},
        	{name: 'September'},
        	{name: 'October'},
        	{name: 'November'},
        	{name: 'December'}
        ]);

        this.reportPPRStore = new Ext.ux.maximgb.treegrid.NestedSetStore({
            autoLoad : true,
            reader: new Ext.data.JsonReader({id: 'objectId'}, record),
            url: '/map/report/getresultppr',
            fields: [
            	'objectId',
            	'parentObjectId',
            	'objectTypeName',
            	'objectName',
            	'rowcls',
            	'price',
            	'January',
            	'February',
            	'March',
            	'April',
            	'May',
            	'June',
            	'July',
            	'August',
            	'September',
            	'October',
            	'November',
            	'December'

            ]
        });


          function change(val, metadata, record)
          {
            if (val == '') {
                val = '-';
            } else
            if (val == '0') {
                val = '';
            }
            if (record.get('_level') == 2) {
                val = '<span style="color:green;">' + val + '</span>';
            }
            return val;
          }

        this.csModel = new Ext.grid.CheckboxSelectionModel();

        this.gridView = new Ext.grid.GridView({
	    	forceFit: true,
		    getRowClass : function (row, index, rp) {
		    	if (row.data.rowcls == 'red-grid-row') {
		    		return 'red-grid-row';
		    	}
		    	if (row.data.rowcls == 'total-cost-ppr') {
		    		return 'total-cost-ppr x-grid3-summary-row';
		    	}
		   	}
		});

		this.cellrenderer = function(value, metaData, record, rowIndex, colIndex, store) {
			metaData.css = 'cell-reportppr';
      		return value;
		}

		this.reportColumns = [
            {id: 'objectId', header: "objectId", sortable: true, dataIndex: 'objectId', hidden:true, hideable:false},
            {id: 'parentObjectId', header: "parentObjectId", sortable: true, dataIndex: 'parentObjectId', hidden:true, hideable:false},
            {header: "Наименование", width: 300, sortable: true, dataIndex: 'objectName'},
            {header: "Янв.", dataIndex: 'January'},
            {header: "Фев.", dataIndex: 'February'},
            {header: "Март", dataIndex: 'March'},
            {header: "Апр.", dataIndex: 'April'},
            {header: "Май",  dataIndex: 'May'},
            {header: "Июнь", dataIndex: 'June'},
            {header: "Июль", dataIndex: 'July'},
            {header: "Авг.", dataIndex: 'August'},
            {header: "Сен.", dataIndex: 'September'},
            {header: "Окт.", dataIndex: 'October'},
            {header: "Ноя.", dataIndex: 'November'},
            {header: "Дек.", dataIndex: 'December'}
        ];

        for(var j=0; j<this.reportColumns.length; j++) {
        	this.reportColumns[j].headerStyle = 'padding-right: 1px;';
        	this.reportColumns[j].renderer = this.cellrenderer;
        	this.reportColumns[j].align = 'center';
        }

        this.rowActions = new Ext.ux.grid.RowActions({
        	actions: [
        		{
            		iconCls: 'viewcard',
            		tooltip: 'Показать карточку ППР'
        		},
        		{
            		iconCls: 'viewparent',
            		tooltip: 'Показать карточку родительского объекта'
        		}
        	]
        });

        this.rowActions.on({
            action: function(grid, record, action, row, col) {
            	if (action == 'viewcard') {
                    if (record.data.objectId) {
                        Map.Card.Instance(this.mapViewer).open({
                            objectId: record.data.objectId,
                            callback: function() {
                                this.reportPPRStore.reload();
                            },
                            scope: this
                        });
                    }
                }
                if (action == 'viewparent') {
                	if (record.data.parentObjectId) {
                        Map.Card.Instance(this.mapViewer).open({
                            objectId: record.data.parentObjectId,
                            callback: function() {
                                this.reportPPRStore.reload();
                            },
                            scope: this
                        });
                    }
                }
            },
            scope: this
        });

        this.reportColumns[this.reportColumns.length] = this.rowActions;
		this.reportColumns[this.reportColumns.length] = this.csModel;

        this.statReportGrid = new Ext.grid.GridPanel({
			view: this.gridView,
			store: this.reportPPRStore,
			region: 'center',
			frame: true,
			columns: this.reportColumns,
			sm: this.csModel,
			stripeRows: true,
			plugins:[this.rowActions]
        });

       // this.mask = new Ext.LoadMask(this.statReportGrid.body,{store:this.reportPPRStore});
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Map.ReportPPR.ReportPanel.superclass.constructor.call(this, {
            layout: 'fit',
            height: 600,
            frame: true,
            items: [
            	{
            		layout: 'border',
            		border: false,
            		items: [
            			{
            				region: 'north',
            				height: 90,
            				border: false,
            				items: this.objectsFieldSet
            			},
            			this.statReportGrid
            		]
            	}
            	]
        });

        this.exportButton = this.addButton({
            text : 'Экспорт в Excel',
            disabled: true
        });

        this.cancelButton = this.addButton({
            text : 'Закрыть'
        });
    },

    afterRender: function() {
    	Map.ReportPPR.ReportPanel.superclass.afterRender.call(this);
    	this.yearCombo.setValue(new Date().getFullYear());
    }

});

Map.ReportPPR.Filter = Ext.extend(Map.Search, {
    constructor: function(mapViewer) {
        Map.ReportPPR.Filter.superclass.constructor.call(this, mapViewer);

        this.container.setTitle('Фильтр объектов');
        this.panel.searchInCardsButton.hide();
        this.panel.multipleFillingButton.hide();
        this.panel.cancelButton.hide();

        this.objects = [];
        this.objectsIndex = {};

        this.addSelectedObjectsButton = this.panel.addButton({
            text: 'Добавить объекты',
            disabled: true,
            handler: this.addSelectedObjects,
            scope: this
        });

        this.okButton = this.panel.addButton({
            text: 'OK',
            handler: this.okAddObjects,
            scope: this
        });

        this.cancelButton = this.panel.addButton({
            text: 'Отмена',
            handler: this.cancelAddObjects,
            scope: this
        });

        this.panel.resultTabPanel.resultGrid.getBottomToolbar().pageSize = 100;

        this.panel.resultTabPanel.resultStore.baseParams.limit = 100;

        this.panel.resultTabPanel.resultCheckboxSelectionModel.on('selectionchange', function(SelectionModel){
            var selections = SelectionModel.getSelections();
            this.addSelectedObjectsButton.setDisabled(selections.length == 0);
        },this);
    },

    search: function(mode, limit) {
        Map.ReportPPR.Filter.superclass.search.call(this, mode, 100);
    },

    open: function(config) {
        var config = config||{};
        this.callback = config.callback;
        this.scope = config.scope;
        /*
        this.bindObject = config.object;
        this.bindCallback = config.callback||Ext.emptyFn;
        this.closeSearchCallback = config.closeSearch||Ext.emptyFn;
        this.bindScope = config.scope;
        this.bindMode = config.mode;
        this.featureTypeFilter = config.featureTypeFilter||[];

        this.container.on('hide', this.closeSearchCallback, this.bindScope, {single: true});

        if (!this.bindObject) {
            Ext.WgsMsg.show({
                title: 'Предупреждение',
                msg: 'Объекты не указаны',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        } else {
            this.panel.searchTabPanel.featureTypeStore.removeAll();
            this.panel.searchTabPanel.featureTypeStore.baseParams = {'featureTypeFilter[]': this.featureTypeFilter};
            this.panel.searchTabPanel.featureTypeStore.load();
            Map.Bind.Search.superclass.open.call(this);
        }
        */
        Map.ReportPPR.Filter.superclass.open.call(this);
    },

    addSelectedObjects: function() {
        var selections = this.panel.resultTabPanel.resultCheckboxSelectionModel.getSelections();

        Ext.each(selections, function(item, index){
            if (!this.objectsIndex[item.get('featureId')]) {
                this.objects.push(item.get('featureId'));
                this.objectsIndex[item.get('featureId')] = true;
            }
        },this);

        this.panel.resultTabPanel.resultCheckboxSelectionModel.clearSelections();
        this.addSelectedObjectsButton.setDisabled(true);
    },

    okAddObjects: function()
    {
        this.panel.hide();
        this.callback.call(this.scope, this.objects);
    },

    cancelAddObjects: function()
    {
        this.panel.hide();
        this.objects = [];
        this.objectsIndex = {};
    },

    onShowSearchTabPanel: function()
    {
        Map.ReportPPR.Filter.superclass.onShowSearchTabPanel.call(this);
        this.panel.searchInCardsButton.hide();
        this.panel.multipleFillingButton.hide();
        this.addSelectedObjectsButton.hide();
        this.okButton.hide();
    },

    onShowResultTabPanel: function()
    {
        Map.ReportPPR.Filter.superclass.onShowResultTabPanel.call(this);
        this.panel.previewButton.hide();
        this.panel.multipleFillingButton.hide();
        this.addSelectedObjectsButton.show();
        this.okButton.show();
    }
});

Map.ReportPPR.Filter.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.ReportPPR.Filter(map);
    }
    return this.instances[map.mapFrame];
}
