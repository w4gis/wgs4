﻿Ext.namespace('Map');

Map.Commands = function(mapViewer)
{
    this.mapViewer = mapViewer;
    //this.conn = new Ext.data.WgsConnection();
}

Map.Commands.Actions = {
   WgsMeasure: 101,
   WgsBuffer: 102,
   WgsPrint: 103,
   WgsOverview: 104,
   WgsFeaturedoc: 105,
   WgsGeometry: 106,
   WgsOptions: 107,
   WgsSearch: 108,
   WgsThemes: 109,
   WgsFavoriteView: 110,
   WgsCard: 111,
   WgsBind: 112,
   WgsPan: 113,
   WgsZoomIn: 114,
   WgsZoomOut: 115,
   WgsNextView: 116,
   WgsPreviousView: 117,
   WgsZoom: 118,
   WgsInitialMapView: 119,
   WgsSelect: 120,
   WgsRefreshMap: 121,
   WgsHelp: 122,
   WgsZoomRectangle: 123,
   WgsSelectRadius: 124,
   WgsSelectPolygon: 125,
   WgsGoogleIntegration: 126,
   WgsMultipleFilling: 127,
   WgsReport: 128,
   WgsPassport: 129,
   WgsPrintBottom: 130,
   WgsFloorplan: 131,
   WgsReportPPR: 132,
   WgsThematic: 133,
   WgsNewThematic: 134
}

Map.Commands.prototype = {
    print: function() {
        Map.Print.Instance(this.mapViewer).open();
    },

    printbottom: function() {
        Map.PrintBottom.Instance(this.mapViewer).open();
    },

    printact: function() {
        Map.PrintAct.Instance(this.mapViewer).open();
    },

    search: function()
    {
        Map.Search.Instance(this.mapViewer).open();
    },

    measure: function()
    {
        //Map.Measure.Instance(this.mapViewer).open();
        Map.MeasureClient.Instance(this.mapViewer).open();
    },

    overview: function()
    {
        Map.Overview.Instance(this.mapViewer).open();
    },

    options: function()
    {
        Map.Options.Instance(this.mapViewer).open();
    },

    buffer: function()
    {
        Map.Buffer.Instance(this.mapViewer).open();
    },

    geometry: function()
    {
        Map.Geometry.Instance(this.mapViewer).open();
    },

    card: function()
    {
        Map.Card.Create(this.mapViewer).open();
    },

    bind: function()
    {
        Map.Bind.Instance(this.mapViewer).open();
    },

    featuredoc: function()
    {
        Map.Featuredoc.Instance(this.mapViewer).open();
    },

    themes: function()
    {
        Map.Themes.Window.Instance(this.mapViewer).open();
       // alert("Модуль находится в стадии разработки :)");
    },
    /*
    newThematic: function()
    {
        alert("developing stub");
        Map.CardNew.Create(this.mapViewer).open();
    },
    */
    favoriteview: function()
    {
        Map.FavoriteView.Instance(this.mapViewer).open();
    },

    selectradius: function ()
    {
        Map.SelectRadius.Instance(this.mapViewer).open();
    },

    selectpolygon: function ()
    {
        Map.SelectPolygon.Instance(this.mapViewer).open({selectFeaturesOnMap:true});
    },

    multiplefilling: function ()
    {
        //Map.Redlining.Instance(this.mapViewer).open();
        Map.MultipleFilling.Instance(this.mapViewer).open();
    },

    googleintegration: function ()
    {
        Map.GoogleIntegration.Instance(this.mapViewer).open();
    },

    report: function()
    {
        Map.Report.Instance(this.mapViewer).open();
    },

    tpassport: function()
    {
      Map.Passport.Instance(this.mapViewer).open();
    },

    floorplan: function()
    {
      Map.Floorplan.Instance(this.mapViewer).open();
    },

    reportppr: function()
    {
      Map.ReportPPR.Instance(this.mapViewer).open();
    }

}