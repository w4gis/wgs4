﻿Ext.namespace('Map');

Map.Viewer = function(config)
{
    if (!config || !config.viewerType || config.viewerType == false) {
        alert('Невозможно созать объект [Map.Viewer]');
        return;
    }
    if (config.simpleMode == true) {
        config.url = '/map/createsimplemaparea'
    }
    var url = config.url || '/map/createmaparea';
    
    this.rightPane = new Ext.WgsMapPanel({
        region: 'east',
        width: 350
    });
    /*
    this.bottomPane = new Ext.WgsMapPanel({
        region: 'south',
        width: 150
    });
    */
    this.panel = new Ext.Panel({
        layout: 'border',
        border: false,
        items: [
            this.innerPanel = new Ext.Panel({
            	border: false,
                region: 'center',
                layout: 'border'
            }),
            this.rightPane
        ]
    });
    
    //Список дочерних объектов 
    this.childComponents = [];
    
    this.panel.on('destroy', function(){
        for (var i = 0; i < this.childComponents.length; i++) {
            this.childComponents[i].destroy();
        }
    },this);
    
    this.addChildComponent = function(component) {
        this.childComponents.push(component);
    }.createDelegate(this);
    //
    var setCursor = function(frame, index) {
    	var cursor = 'default';
        switch(parseInt(index)) {
            //Zoom Rectangle
            case Map.Commands.Actions.WgsZoomRectangle: {
                cursor = (Ext.isOpera)? 'crosshair !important': 'url("/images/cursors/zoomin.cur"), crosshair';
            }
            break;
            //Zoom In
            case Map.Commands.Actions.WgsZoomIn: {
                cursor = (Ext.isOpera)? 'default !important': 'url("/images/cursors/zoomin.cur"), default';
            }
            break;
            //Zoom Out
            case Map.Commands.Actions.WgsZoomOut: {
                cursor = (Ext.isOpera)? 'default !important': 'url("/images/cursors/zoomout.cur"), default';
            }
            break;
            //Move
            case Map.Commands.Actions.WgsPan: {
                cursor = 'move';
            }
            break;
            //Select
            case Map.Commands.Actions.WgsSelect: {
                cursor = 'default';
            }
            break;
            //SelectRadius
            case Map.Commands.Actions.WgsSelectRadius: {
                cursor = 'default';
            }
            break;
            //SelectPolygon
            case Map.Commands.Actions.WgsSelectPolygon: {
                cursor = 'default';
            }
            break;
            //Measure
            case Map.Commands.Actions.WgsMeasure: {
                cursor = 'default';
            }
            break;
        }
        if (Ext.isSafari && frame.GetMapFrame().document.getElementById("tbMap")) {
            frame.GetMapFrame().document.getElementById("tbMap").style.cursor = cursor + ' !important';
        } else {
            try {
                frame.GetMapFrame().document.body.style.cursor = cursor;
            } catch(e) {}
        }
    }
    
    var toggleMode = function(index) {
        switch (index) {
            //Измерение расстояния
            case Map.Commands.Actions.WgsMeasure: {
                this.Lib.getRaphaelDrawing().clear();
                break;
            }
            //выделение объектов окружностью
            case Map.Commands.Actions.WgsSelectRadius: {
                this.Lib.getRaphaelDrawing().clear();
                break;
            }
            //выделение объектов полигоном
            case Map.Commands.Actions.WgsSelectPolygon: {
                this.Lib.getRaphaelDrawing().clear();
                break;
            }            
            default: {
                this.Lib.getRaphaelDrawing().clear();
                this.Lib.getRaphaelDrawing().destroy();
            }
        }
        WGS.Tools.removeHandler(this.Api.Map.getMap().document, 'keydown', null);
        this.Api.Map.StopMouseDown();
        this.Api.Map.StopMouseMove();
        this.Api.Map.StopMouseUp();
        this.Api.Map.StopContextMenu();
    }
    
    var loadMask = new Ext.LoadMask(document.body, {msg: 'Загрузка...'});
    loadMask.show();

    new Ext.data.WgsConnection().request({
        url: url,
        method: 'GET',
        params: {
            viewerType: config.viewerType
        },
        success: function(response) {
            loadMask.hide();
            var mapArea = Ext.decode(response.responseText).mapArea;
            var mapFrame = Ext.decode(response.responseText).mapFrame;
            
            if (!mapArea && !mapFrame) {
                (config.load||Ext.emptyFn).call(config.scope||this);
                Ext.WgsMsg.show({
                    title: 'Сообщение',
                    msg: 'Не доступна ни одна карта. Обратитесь к администратору системы.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                return;
            }
            
            this.mapPane = new Ext.Panel({
                id: 'mapPane',
                region: 'center',
                html: mapArea
            });
            
            this.innerPanel.add(this.mapPane);
            
            var panelCount = 3, layoutCount = 0;
            
            this.panel.on('afterlayout', function() {
                if (++layoutCount == panelCount) {
                    Map.ViewerApi.Instance(mapFrame, function(api, frame){
                        this.mapFrame = mapFrame;
                        this.Api = api;
                        this.Lib = new Map.ViewerLib(this.Api);
                        this.Commands = new Map.Commands(this);
                        for (var key in this.Commands) {
                            if (typeof(this.Commands[key]) == 'function') {
                                frame[key] = this.Commands[key].createDelegate(this.Commands);
                            }
                        }
                        
                        var executeCommand = frame['ExecuteCommand'];
                        frame['ExecuteCommand'] = function(index){
                            setCursor(frame, index);
                            toggleMode.call(this, index);
                            return executeCommand.call(frame, index);
                        }.createDelegate(this);
                        //default cursor style
                        if (Ext.isSafari) {
                            if (frame.GetMapFrame().document.getElementById("tbMap")) {        
                                frame.GetMapFrame().document.getElementById("tbMap").style.cursor = 'default !important';
                            }
                        } else {
                            try {
                                frame.GetMapFrame().document.body.style.cursor = 'default';
                            } catch(e) {}
                        }
                        //statusbar
                        this.Api.Map.ObserveOnMapLoaded(function() {
                            var selectedLayers = this.Api.Map.GetSelectedLayers();
                            if (this.Api.Map.GetSelectedCount() == 1) {
                                this.Api.Statusbar.SetFeatureSelectedMsg('Выбрано объектов: 1  (' + selectedLayers[0].legend + ')');
                            } else 
                            if (this.Api.Map.GetSelectedCount() > 1) {
                                if (selectedLayers.length == 1)
                                    this.Api.Statusbar.SetFeatureSelectedMsg('Выбрано объектов: ' + this.Api.Map.GetSelectedCount() + '  (' + selectedLayers[0].legend + ')');
                                else
                                    this.Api.Statusbar.SetFeatureSelectedMsg('Выбрано объектов: ' + this.Api.Map.GetSelectedCount() + '  (Слоёв: ' + selectedLayers.length + ')');
                            }
                        }, this);
                        (config.load||Ext.emptyFn).call(config.scope||this, this.Api, this.Lib, this.Commands);
                    }, this);
                }
            }, this);
            
            if (config && config.render) {
                config.render.call(config.scope||this, this.panel);
            }
        },
        failure: function() {
            loadMask.hide();
        },
        exception: function() {
            loadMask.hide();
        },
        scope: this
    });
}

Map.Viewer.Instance = function(config)
{
    if (!this.instance) {
        this.instance = new Map.Viewer(config);
    }
    return this.instance;
}

Map.Viewer.Create = function(config)
{
    return new Map.Viewer(config);
}
