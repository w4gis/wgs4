Ext.namespace('Map');

Map.Featuredoc = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.conn = new Ext.data.WgsConnection();
}

Map.Featuredoc.prototype = {

    create: function(mapViewer)
    {
        this.panel = new FeaturedocPanel({
            autoWidth: true,
            height: 450,
            conn: this.conn
        });

        if (mapViewer) {
	        this.window = new Ext.WgsServiceWindow({
	            containerPanel: mapViewer.rightPane,
	            layout: 'fit',
	            resizable: true,
	            width: 570+12,
	            title: '����������� ��������',
	            closeAction: 'hide'
	        });
	    } else {
	        this.window = new Ext.WgsServiceWindowSimple({
	            layout: 'fit',
	            resizable: true,
	            width: 570+12,
	            title: '����������� ��������',
	            closeAction: 'hide'
	        });
	    }
        this.window.setInnerPanel(this.panel);
        this.created = true;
    },

    setentityid: function (entityId)
    {
        Map.Featuredoc.entityId = new Array();
        Map.Featuredoc.entityId = entityId;
    },

    open: function()
    {
        this.mapViewer.Lib.getSelectedFeatures(function(featureLayers){
            if (featureLayers != -1)
            {
                var entityIdList = new Array();
                Ext.each(featureLayers,
                function(features) {
                    Ext.each(features.features, function(entityId) {
                        entityIdList[entityIdList.length] = entityId.entityId;
                    });

                });

                entityId = featureLayers[0].features[0].entityId;
                if ((featureLayers[0].features.length > 1 || featureLayers.length > 1) && !WGS.PRIVILEGE.FEATUREDOC_BIND)
	                Ext.WgsMsg.show({
	                    title: '��������������',
	                    msg: '���������� ������� ���� ������',
	                    buttons: Ext.MessageBox.OK,
	                    icon: Ext.MessageBox.WARNING
	                });
                else {
					this.openFeaturedoc.call(this, entityId, entityIdList);
                }
            }
            else Ext.WgsMsg.show({
                    title: '��������������',
                    msg: '������� �� �������',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
        },this);
    },

    openFeaturedoc: function(entityId, entityIdList)
    {
        if (!WGS.PRIVILEGE.FEATUREDOC_BIND)
        {
            this.conn.request({
               url: '/map/featuredoc/getcountdoc',
               params: { entityId: entityId },
               success: function(response){
                   if ( response.responseText > 0 )
                   {
                       this.setentityid(entityId);
                       //this.initialization();
                       this.doLoadFilesToCache();
                   }
                   else Ext.WgsMsg.show({
                       title: '��������������',
                       msg: '��� ���������� ��� �����������',
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.WARNING
                   });
               },
               scope: this
             });
         }
         else {
           if (Ext.isIE7) {
                activeX = 'ShockwaveFlash.ShockwaveFlash.9';
                Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
            }

            if (Ext.isGecko || Ext.isOpera || Ext.isSafari) {
                activeX = 'Shockwave Flash';
                Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
            }

            if (Map.Featuredoc.isExist == false && (Ext.isIE7 /*|| Ext.isGecko || Ext.isOpera || Ext.isSafari*/)) {
                if (Ext.isIE7) this.msg = '��� ����� �������������� ������ ������ ���������� ���������� ' +
                                         '<A href="/downloads/install_flash_player_active_x.exe">Adobe Flash Player</A>.'
                /*if (Ext.isGecko || Ext.isOpera || Ext.isSafari) this.msg = '��� ����� �������������� ������ ������ ���������� ���������� ' +
                                            '<A href="/downloads/install_flash_player.exe">Adobe Flash Player</A>.'*/
                Ext.WgsMsg.show({
                    title: '���������',
                    msg: this.msg,
                    buttons: Ext.MessageBox.OK,
                    fn: function () {
                        this.setentityid(entityIdList);
                        //this.initialization();
                        this.doLoadFilesToCache();
                    },
                    scope: this,
                    icon: Ext.MessageBox.INFO
                });
            }
            else {
                this.setentityid(entityIdList);
                //this.initialization();
                this.doLoadFilesToCache();
            }
         }
    },

    doLoadFilesToCache: function()
    {
    	Map.Featuredoc.msgWaitLoad = new Ext.LoadMask(Ext.getBody(), {msg: '���������� ����������...'});
        Map.Featuredoc.msgWaitLoad.show();
    	this.conn.request({
            url: '/map/featuredoc/getdocexist',
            params: { 'entityId': Map.Featuredoc.entityId },
            success: function(response){
            	this.conn.request({
            		url: '/map/featuredoc/getdocbyentityid',
            		params: { 'entityId': Map.Featuredoc.entityId },
            		success: function () {
						this.initialization();
            		},
            		failure: function() {
            			Map.Featuredoc.msgWaitLoad.hide();
            		},
            		scope: this
            	});
            },
            failure: function() {
    			Map.Featuredoc.msgWaitLoad.hide();
    		},
            scope: this
        }, this);
    },

    initialization: function()
    {
		if (!this.created) {
            this.create(this.mapViewer);
        }
        else this.reload();

        this.window.addPanel();

        Map.Featuredoc.window = this.window;
        Map.Featuredoc.panel = this.panel;

        if (!WGS.PRIVILEGE.FEATUREDOC_BIND) {
            Map.Featuredoc.window.on('show', function(){
                this.reload();
                Map.Featuredoc.msgWaitLoad = new Ext.LoadMask(Map.Featuredoc.window.getEl(), {msg: '��������� �������...'});
                Map.Featuredoc.msgWaitLoad.show();
            }, this);
        	Map.Featuredoc.window.show();
        	Map.Featuredoc.panel.show();
        } else {
        	 Map.Featuredoc.window.on('show', function(){
                if (Map.Featuredoc.msgWaitLoad) {
                	Map.Featuredoc.msgWaitLoad.hide();
                }
            }, this);
        }
    },

    reload: function()
    {
        this.panel.fireEvent('reload');
        this.panel.fireEvent('setActiveTab');
    }
}

Map.Featuredoc.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Featuredoc(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Featuredoc(map);
        }
        return this.instance;
    }
}

FeaturedocPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config){
        if (config){
            Ext.apply(this, config);
        }

        if (WGS.PRIVILEGE.FEATUREDOC_BIND) {
           	this.docTabPanel = new DocTabPanel(this.conn);
        } else {
           	this.docTabPanel = new DocViewPanel();
        }

        this.addEvents({
            reload: true,
            setActiveTab: true
        });

        this.on('reload', function(){
            this.docTabPanel.fireEvent('reload');
        }, this);

        this.on('setActiveTab', function(){
            this.docTabPanel.fireEvent('setActiveTab');
        }, this);

		FeaturedocPanel.superclass.constructor.call(this, {
            layout: 'fit',
            items: [this.docTabPanel]
        });
    }
});

DocListGrid = function(conn)
{
    this.conn = conn;

    Ext.QuickTips.init();
    this.rowActions = new Ext.ux.grid.RowActions({
         actions:[{
             iconCls: 'viewcard',
             tooltip: '�������� ��������'
         }]
    });

    this.storeDocGrid = new Ext.data.GroupingStore({
         autoLoad : true,
         reader: new Ext.data.JsonReader({},[
                       {name: 'id'},
                       {name: 'name'},
                       {name: 'description'},
                       {name: 'entityid'},
                       {name: 'iconClsView'},
                       {name: 'qtipView'},
                       {name: 'empty'},
                       {name: 'urlDocument'},
                       {name: 'objectName'},
                       {name: 'storeType'}
                    ]),
         sortInfo:{field: 'objectName', direction: "ASC"},
         groupField:'objectName',
         remoteGroup: true,
         url : '/map/featuredoc/getstoredoclist',
         baseParams: {'entityId': Ext.util.JSON.encode(Map.Featuredoc.entityId)},
         fields : ['id', 'name', 'description', 'entityid', 'iconClsView', 'qtipView', 'empty', 'urlDocument', 'objectName', 'storeType']
     });

     /* buttons */
     this.createButton = new Ext.Button({
          text : '���������',
          iconCls: 'icon-add',
          handler : function()
          {
               this.fireEvent('addDocument')
          },
          scope : this
     });

     this.editButton = new Ext.Button({
          text : '��������',
          iconCls: 'icon-properties',
          handler : function()
          {
               this.fireEvent('edit')
          },
          scope : this
     });

     this.removeButton = new Ext.Button({
          text : '�������',
          iconCls: 'icon-delete',
          disabled: true,
          handler : function()
          {
               this.fireEvent('remove')
          },
          scope : this
     });

      this.saveButton = new Ext.Button({
          text : '���������',
          iconCls: 'icon-save',
          disabled: true,
          handler : function()
          {
               this.fireEvent('Save')
          },
          scope : this
     });

     this.cancelButton = new Ext.Button({
          text : '������',
          iconCls: 'icon-close-cross',
          disabled: true,
          handler : function()
          {
               this.fireEvent('Cancel')
          },
          scope : this
     });

    this.cellActions = new Ext.ux.grid.CellActions({
       align:'left'
    });

	this.docSelectionModel = new Ext.grid.CheckboxSelectionModel();
	this.docColumnModel = new Ext.grid.ColumnModel([
        { id : 'id', header : "id ���������", hidden : true, hideable : false },
        { id : 'docname', header : "�������� ���������", dataIndex : 'name' , editor :  new Ext.form.TextField({allowBlank:false}) },
        { id : 'docdesc', header : "�������� ���������", dataIndex : 'description', editor :  new Ext.form.TextArea() },
        /*this.rowActions,*/
		{width: 20, dataIndex : 'empty'
                    ,cellActions:[{
                         iconIndex:'iconClsView'
                        ,qtipIndex:'qtipView'
                    },{
                         iconCls:'icon-save'
                        ,qtip:'��������� ��������'
                    }
                    ]
                },
        this.docSelectionModel,
        { id : 'entityid', header : "������", dataIndex : 'entityid', hidden : true, hideable : false },
        { id : 'objectName', header : "������", dataIndex : 'objectName', hidden : true, hideable : false }
	]);
     // constructor

     DocListGrid.superclass.constructor.call(this, {

          store : this.storeDocGrid,
          //viewConfig: { forceFit: true },
          view: new Ext.grid.GroupingView(
          {
              groupTextTpl: '{text}',
              enableGroupingMenu: false,
              forceFit:true
          }),
          cm : this.docColumnModel,
          sm : this.docSelectionModel,
          plugins:[/*this.rowActions*/this.cellActions],
          autoScroll : true,
          stripeRows: true,
          clicksToEdit: 2,
          tbar : [ this.createButton, this.editButton , this.removeButton , '-', this.saveButton, this.cancelButton]
          //tbar : [ this.createButton, this.removeButton]
     });

     this.docSelectionModel.on('selectionchange', function(sm){
     	var sels = sm.getSelections(),
     		disabled = false;

     	Ext.each(sels, function(item){
     		if (item.get('storeType') != '0') {
     			disabled = true;
     			return false;
     		}
     	});

     	this.editButton.setDisabled(disabled);
     	this.removeButton.setDisabled(disabled);
     }, this);
}

Ext.extend(DocListGrid, Ext.grid.EditorGridPanel, {
    initComponent : function() {

        this.addEvents({
            create : true,
            edit : true,
            remove : true,
            save: true,
            cancel: true
        });

        this.cellActions.on(
        	'action', function(grid, record, action, value) {
        		if (action == 'icon-view-image' || action == 'icon-view-pdf' || action == 'icon-view-dwf' || action == 'icon-view-video') {
        			var loadMask = new Ext.LoadMask( Map.Featuredoc.window.getEl(), {msg: '�������� ���������...'});
                    loadMask.show();
                    this.conn.request({
                        url: '/map/featuredoc/getdocbydocid',
                        params: { docId: record.data.id},
                        success: function(response){
                            var description = Ext.util.JSON.decode(response.responseText);
                            description.documents.each(function(desc) {
                                var isExist = BeforeOpenDocument(desc.doctype);
                                var docWindow = new DocumentWindow(desc.object, desc.name);
                                if (isExist == true) docWindow.show();
                                docWindow = false;
                            });
                            loadMask.hide();
                        },
                        scope: this
                    });
        		} else {
        			if (action == 'icon-save') {
        				WGS.Tools.DownloadFile(record.data.urlDocument);
        			}
        		}
        	}, this
        );

        this.rowActions.on(
            'action', function(grid, record, action, row, col) {
                if (action == 'viewcard') {
                    var loadMask = new Ext.LoadMask( Map.Featuredoc.window.getEl(), {msg: '�������� ���������...'});
                    loadMask.show();
                    this.conn.request({
                        url: '/map/featuredoc/getdocbydocid',
                        params: { docId: record.data.id},
                        success: function(response){
                            var description = Ext.util.JSON.decode(response.responseText);
                            description.documents.each(function(desc) {
                                var isExist = BeforeOpenDocument(desc.doctype);
                                var docWindow = new DocumentWindow(desc.object, desc.name);
                                if (isExist == true) docWindow.show();
                                docWindow = false;
                            });
                            loadMask.hide();
                        },
                        scope: this
                    });
                }
            },
            this
        );

        // create doc
        this.on('addDocument', function() {
            var file_upload_limit = '0';

            if (Ext.isIE7) {
            	activeX = 'ShockwaveFlash.ShockwaveFlash.9';
            	Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
            }

            if (Ext.isGecko || Ext.isOpera || Ext.isSafari) {
                activeX = 'Shockwave Flash';
                Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
            }

            if ((Ext.isIE7 /*|| Ext.isGecko || Ext.isOpera || Ext.isSafari*/) && Map.Featuredoc.isExist == true) {
                //alert(1);
            	var addFile = new UploadWindow(null, this.storeDocGrid, file_upload_limit);
            } else {
            	//alert(2);
                var addFile = new UploadDialog(null, this.storeDocGrid);
            }
            addFile.show();
			alert(2);
        }, this);

        // edit doc
        this.on('edit', function() {
            var records = this.docSelectionModel.getSelections();
            if (records.size() == 1)
            {
                var file_upload_limit = '1';

                if (Ext.isIE7) {
                    activeX = 'ShockwaveFlash.ShockwaveFlash.9';
                    Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
                }

                if (Ext.isGecko || Ext.isOpera || Ext.isSafari) {
                    activeX = 'Shockwave Flash';
                    Map.Featuredoc.isExist =  isExistActiveX.call(this, activeX);
                }

                if ((Ext.isIE7 /*|| Ext.isGecko || Ext.isOpera || Ext.isSafari*/)  && Map.Featuredoc.isExist == true) {
                	var updateFile = new UploadWindow(records[0].data.id, this.storeDocGrid, file_upload_limit);
                } else {
                	var updateFile = new UploadDialog(records[0].data.id, this.storeDocGrid);
                }
                updateFile.show();
            }
            else Ext.WgsMsg.show({
                    title: '��������������',
                    msg: '���������� ������� ���� �������� ��� ��������������.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
           });
        }, this);

        // remove doc
        this.on('remove', function() {
            var records = this.docSelectionModel.getSelections();
            if (records[0])
            {
                for (i = 0; i < records.length; i++)
                    var params = params + "_" + records[i].data.id;
                Ext.WgsMsg.confirm('������������� ��������', '�� ������������� ������ ������� ��������� ���������?',
                function(btn) {
                    if (btn == 'yes')
                    {
                      //var msgWait = Ext.WgsMsg.wait('�������� ����������...', '���������', {interval: 50});
                      var loadMask = new Ext.LoadMask(Map.Featuredoc.window.getEl(), {msg: '�������� ����������...'});
                      loadMask.show();
                        this.conn.request({
                           url: '/map/featuredoc/removedoc',
                           params: { id: params},
                           success: function(response){
                                if (records.length == this.storeDocGrid.getCount()) this.storeDocGrid.removeAll();
                                this.storeDocGrid.reload();
                                //msgWait.hide();
                                loadMask.hide();
                           },
                           scope: this
                        });
                    }
                }, this);
            }
            else Ext.WgsMsg.show({
                    title: '��������������',
                    msg: '���������� ������� �������� ��� ��������.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
           });
        }, this);

        this.on('save', function() {
            //var msgWait = Ext.WgsMsg.wait('���������� ���������...', '���������', {interval: 32});
            var loadMask = new Ext.LoadMask(Map.Featuredoc.window.getEl(), {msg: '����������...'});
            loadMask.show();
            var modifiedRecords = this.store.getModifiedRecords();
            var params = [];
            Ext.each(modifiedRecords, function(record){
                params[params.length] = record.data;
            });
            this.conn.request(
            {
                url: '/map/featuredoc/updateparams',
                method: 'POST',
                params: {param: Ext.util.JSON.encode(params), docid: this.id},
                success: function(){
                    //msgWait.hide();
                    loadMask.hide();
                }
            });

            this.store.commitChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
        });

        this.on('cancel', function() {
            this.storeDocGrid.rejectChanges();
            this.saveButton.disable();
            this.cancelButton.disable();
        });

        this.storeDocGrid.on('update', function() {
            this.saveButton.enable();
            this.cancelButton.enable();
        }, this);

       this.storeDocGrid.on('beforeload', function() {

            Map.Featuredoc.window.show();
            Map.Featuredoc.panel.show();

            Ext.getCmp('images-view').setDisabled(true);
        }, this);

        this.storeDocGrid.on('load', function() {
            if (this.storeDocGrid.getCount() > 0 && Map.Featuredoc.entityId.length == 1) Ext.getCmp('images-view').setDisabled(false);
        }, this);

        this.on('rowclick', function() {

        });

        DocListGrid.superclass.initComponent.apply(this, arguments);
    }
});

//------------------------------------------------------------------
UploadDialog = function(doc_id, storeDocGrid)
{
      this.doc_id = doc_id;
      this.store = storeDocGrid;
	  console.log(Ext.util.JSON.encode(Map.Featuredoc.entityId), this.doc_id)
      UploadDialog.superclass.constructor.call(this, {
         url: '/map/featuredoc/uploadfile',
         base_params: {'entityId': Ext.util.JSON.encode(Map.Featuredoc.entityId), 'doc_id' : this.doc_id},
         reset_on_hide: true,
         allow_close_on_upload: false,
         upload_autostart: false,
         //permitted_extensions: ['jpe', 'jpg', 'jpeg', 'png', 'gif', 'dwf', 'pdf', 'tif', 'tiff', 'avi', 'wmv', 'mpg'],
         modal : true
    });
	//this.show(Ext.getBody());
	alert('?')
}

Ext.extend(UploadDialog, Ext.ux.UploadDialog.Dialog, {
    initComponent : function() {
        this.on('fileuploadstart', function(dialog, filename, filecount){
			
        });

        this.on('uploadsuccess', function(dialog, filename) {

        });

        this.on('uploadcomplete', function() {
            this.store.reload();
            this.show();
        }, this);

        UploadDialog.superclass.initComponent.apply(this, arguments);
    }
});

//*************************************************************************************************
UploadDialogSwf = function (doc_id, storeDocGrid, parent, file_upload_limit)
{
    this.doc_id = doc_id;
    this.storeDocGrid = storeDocGrid;
    this.parent = parent;

    // �������� id ������ �� cookie
    this.PHPSESSIDX = null;
    var cookiesInfo = document.cookie.split(";");
    var cookies = cookiesInfo[0].split("=");
    this.PHPSESSIDX = cookies[1];

    UploadDialogSwf.superclass.constructor.call(this, {
        width: 500,
        height: 300,
        border: false,

        // Uploader Params
        upload_url: '/map/featuredoc/uploadfileswf',
        post_params: { 'PHPSESSIDX': this.PHPSESSIDX , 'ENTITYID': Ext.util.JSON.encode(Map.Featuredoc.entityId), 'DOC_ID' : this.doc_id },
        //file_types: '*.jpe; *.jpg; *.jpeg; *.png; *.gif; *.dwf; *.pdf; *.tif; *.tiff; *.avi; *.wmv; *.mpg',
        file_types_description: 'Documents',
        flash_url: "/js/extjs/extensions/swfupload_f9.swf",
        file_size_limit: "20480",
        parent: this.parent,
        storeDocGrid: this.storeDocGrid,
        // Custom Params
        single_file_select: false,
        file_upload_limit: file_upload_limit,
        confirm_delete: false,
        remove_completed: false
    });
}
Ext.extend(UploadDialogSwf, Ext.ux.SwfUploadPanel, {
    initComponent : function() {
        this.on('swfUploadLoaded', function() {
            this.addPostParam( 'PHPSESSIDX', 'ENTITYID', 'DOC_ID' );
        });

        /*this.on('fileUploadComplete', function(panel, file, response) {
        });*/

        this.on('queueUploadComplete', function() {
            this.storeDocGrid.reload();
            this.parent.show();
        },this);

        UploadDialogSwf.superclass.initComponent.apply(this, arguments);
    }
});

UploadWindow = function (doc_id, storeDocGrid, file_upload_limit)
{
    this.doc_id = doc_id;
    this.store = storeDocGrid;
    this.dialog = new UploadDialogSwf(this.doc_id, this.store, this, file_upload_limit);
    UploadWindow.superclass.constructor.call(this, {
        title: '�������� ���������� �� ������',
        width: 514,
        modal: true,
        height: 330,
        resizable: false,
        closable: false,
        items: [ this.dialog ]
    });
}
Ext.extend(UploadWindow, Ext.WgsShimWindow, {});

//*************************************************************************************************

// DocControlPanel - ������ �������� ����������
DocControlPanel = Ext.extend(Ext.Panel, {
    constructor: function(conn) {
        this.conn = conn;
        this.docListGrid = new DocListGrid(this.conn);
        this.addEvents({
            reload: true
        });
        this.on('reload', function(){
            this.docListGrid.storeDocGrid.removeAll();
            this.docListGrid.storeDocGrid.baseParams = {'entityId': Ext.util.JSON.encode(Map.Featuredoc.entityId)};
            this.docListGrid.storeDocGrid.load();
        }, this);

        DocControlPanel.superclass.constructor.call(this, {
            collapsible : false,
            margins : '0 0 0 0',
            layout : 'fit',
            title: '�������� ����������',
            items: [this.docListGrid]
        });
    }
});

DocListDataView = function () {

    this.xd = Ext.data;

    this.store = new Ext.data.JsonStore({
        url: '/map/featuredoc/getdocbyentityid',
        baseParams: {'entityId': Map.Featuredoc.entityId},
        root: 'documents',
        fields: ['name', 'object', 'url_sketch', 'doctype', 'urlDocument', 'url_zip']
    });

    this.tpl = new Ext.XTemplate(
        '<tpl for=".">',
            '<div class="thumb-wrap"  id="{name}">',
            '<div class="thumb"><img src="{url_sketch}" title="{name}"></div>',
            '<span class="x-editable"><a href="#" onClick="WGS.Tools.DownloadFile('+"'"+'{url_zip}'+"'"+')">{shortName}</a></span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    );

    DocListDataView.superclass.constructor.call(this, {
        store: this.store,
        tpl: this.tpl,
        autoHeight: true,
        autoWidth: true,
        singleSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.thumb-wrap',
        emptyText: '��� ���������� ��� �����������',
        prepareData: function(data){
        	if (null == data.object) {
            	data.shortName = "<a href='"+data.urlDocument+"' onclick=\"javascript:WGS.Tools.DownloadFile('"+data.urlDocument+"')\"  target=\"_blank\" >"+Ext.util.Format.ellipsis(data.name, 22)+"</a>";
        	} else {
        		data.shortName = Ext.util.Format.ellipsis(data.name, 22);
        	}
            return data;
        }
    });
}
Ext.extend(DocListDataView, Ext.DataView, {
    initComponent : function() {

        this.store.on('beforeload', function (){
            Ext.getCmp('images-view').hide();
        }, this);

        this.store.on('load', function (p1, p2, msgWaitLoad) {
            var windowShow = false;
            try { msgWaitLoad.hide(); windowShow = true; } catch (e) {}
            if (Map.Featuredoc.msgWaitLoad) { Map.Featuredoc.msgWaitLoad.hide(); windowShow = true; }
            if (windowShow == true) {
            	Map.Featuredoc.window.show();
                Map.Featuredoc.panel.show();
            }
            Ext.getCmp('images-view').show();
        }, this);

        this.on('dblclick', function() {
            this.getSelectedRecords().each(function (record){
                if (null != record.data.object) {
	                var isExist = BeforeOpenDocument(record.data.doctype);
	                var docWindow = new DocumentWindow(record.data.object, record.data.name);
	                if (isExist == true) docWindow.show();
	                docWindow = false;
                }
            });

        }, this);

        DocListDataView.superclass.initComponent.apply(this, arguments);
    }
});

DocumentWindow = function(object, docname) {
    this.documentPanel = new DocumentPanel(object);

    DocumentWindow.superclass.constructor.call(this, {
        title : ''+docname+'',
        closable : true,
        width : 550,
        height : 450,
        minWidth: 320,
        minHeight: 240,
        border : false,
        resizable : true,
        maximizable : true,
        layout: 'fit',
        plain : true,
        closeAction : 'close',
        items: [this.documentPanel]
    });
}
Ext.extend(DocumentWindow, Ext.WgsShimWindowRatio, {});

DocumentPanel = function(object) {

    DocumentPanel.superclass.constructor.call(this, {
        autoWidth: true,
        layout: 'fit',
        html: object
    });
}
Ext.extend(DocumentPanel, Ext.Panel, {});

BeforeOpenDocument = function(doctype) {
    var activeX = false;
    var isExist = false;

    if (doctype == 'pdf')
    {
        if (Ext.isGecko || Ext.isOpera || Ext.isSafari) activeX = 'Adobe Acrobat';
        if (Ext.isIE) activeX = 'AcroPDF.PDF.1';
    }

    if (doctype == 'dwf')
    {
        if (Ext.isGecko || Ext.isOpera || Ext.isSafari)
            Ext.WgsMsg.show({
                title: '��������������',
                msg: '��� ������� �� ������������ ������ ��� ����������.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        if (Ext.isIE) var activeX = 'AdView.AdViewer.1';
    }

    isExist =  isExistActiveX.call(this, activeX);

    if (doctype != 'pdf' && doctype != 'dwf') isExist = true;

    if (isExist == false && Ext.isIE && doctype == 'dwf')
    {
        /*Ext.WgsMsg.show({
            title: '���������',
            msg: '������ �� ����� ���������� ����� ����������� ��������� ������������ ������������ ' +
                 '����������� ��� ��������� ������� ���� ����������. ��� ����� ������ ��������� �����.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        docWindow.show();
        docWindow.hide();*/
        Ext.WgsMsg.show({
            title: '���������',
            msg: '��� ��������� ������� ���� ���������� ���������� ���������� ' +
                 '<A href="/downloads/SetupDesignReview2009.exe">Autodesk Design Review</A>',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
    }

    if (isExist == false && Ext.isIE && doctype == 'pdf')
    {
        Ext.WgsMsg.show({
            title: '���������',
            msg: '��� ��������� ������� ���� ���������� ���������� ���������� ' +
                 '<A href="/downloads/AdbeRdr90ru.exe">Adobe Reader</A>',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
    }

    if (isExist == false && (Ext.isGecko || Ext.isOpera || Ext.isSafari) && doctype == 'pdf')
    {
        Ext.WgsMsg.show({
            title: '���������',
            msg: '��� ��������� ������� ���� ���������� ���������� ���������� ' +
                 '<A href="/downloads/AdbeRdr90ru.exe">Adobe Reader</A>, � ����� ������������� �������.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
    }
    return isExist;
}

// DocViewPanel - ������ ��������� ����������
DocViewPanel = function() {

    this.docListDataView = new DocListDataView();

        this.addEvents({
            reload: true
        });

        this.on('reload', function(msgWaitLoad){
            this.docListDataView.store.baseParams = {'entityId': Map.Featuredoc.entityId};
            this.docListDataView.store.load(msgWaitLoad);
        }, this);

    if (WGS.PRIVILEGE.FEATUREDOC_BIND) title = '�������� ����������';
    else title = '';

    DocViewPanel.superclass.constructor.call(this, {
        id:'images-view',
        collapsible : false,
        margins : '0 0 0 0',
        layout : 'fit',
        autoScroll : true,
        title: title,
        items: [this.docListDataView]
    });
}
Ext.extend(DocViewPanel, Ext.Panel, {});

// DocTabPanel
DocTabPanel = function(conn) {

    this.conn = conn;

    this.docControlPanel = new DocControlPanel(this.conn);
    this.docViewPanel = new DocViewPanel();

    this.addEvents({
        reload: true,
        setActiveTab: true
    });

    this.on('reload', function(){
        this.docControlPanel.fireEvent('reload');
    }, this);

    this.on('setActiveTab', function(){
        this.setActiveTab(this.docControlPanel);
    }, this);

    DocTabPanel.superclass.constructor.call(this, {
        deferredRender : false,
        activeTab : 0,
        region : 'center',
        border : false,
        collapsible : false,
        autoScroll : false,
        items : [this.docControlPanel, this.docViewPanel]
    });
}

Ext.extend(DocTabPanel, Ext.TabPanel, {
    initComponent : function() {
        this.on('tabchange', function() {
            if (this.getActiveTab() == this.docViewPanel)
            {
                var msgWaitLoad = false;
                Ext.getCmp('images-view').hide();
                this.conn.request({
                    url: '/map/featuredoc/getdocexist',
                    params: { 'entityId': Map.Featuredoc.entityId },
                    success: function(response){
                        /*if (response.responseText > 0) {
                            if (Map.Featuredoc.window && Map.Featuredoc.panel) {
                               Map.Featuredoc.window.hide();
                               Map.Featuredoc.panel.hide();
                            }
                            var msgWaitLoad = Ext.WgsMsg.wait('��������� �������...', '���������', {interval: 200});*/
                            var msgWaitLoad = new Ext.LoadMask(Map.Featuredoc.window.getEl(), {msg: '��������� �������...'});
                            msgWaitLoad.show();
                        //}
                        this.docViewPanel.fireEvent('reload', msgWaitLoad);
                    },
                    scope: this
                }, this);
            }
        }, this);
        DocTabPanel.superclass.initComponent.apply(this, arguments);
    }
});