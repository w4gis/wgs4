﻿Ext.namespace('Map');

Map.Passport = function(mapViewer)
{
    Ext.ns('Wgs.Map.Passport');
    Wgs.Map.Passport = this;
    Wgs.Map.Passport.MapViewer = mapViewer;
    Wgs.Map.Passport.ViewerLib = mapViewer.Lib;
    Wgs.Map.Passport.ViewerApi = mapViewer.Api;
    Wgs.Map.Passport.WgsConn = new Ext.data.WgsConnection();

    Wgs.Map.Passport.PassportPanel = new Map.Passport.PassportPanel;

    Wgs.Map.Passport.Window = new Ext.WgsShimWindow({
        //containerPanel: Wgs.Map.Passport.MapViewer.rightPane,
        width: 550,
        //height: 300,
        layout: 'fit',
        maximizable : true,
        //autoHeight: true,
        title: 'Технический паспорт',
        resizable: true,
        closeAction: 'close',
        draggable: true
    });

    Wgs.Map.Passport.PDFPassportPanel = null;
    Wgs.Map.Passport.MapViewer.addChildComponent(Wgs.Map.Passport.Window);
    //Wgs.Map.Passport.Window.setInnerPanel(Wgs.Map.Passport.PassportPanel);
    //Wgs.Map.Passport.PassportId = 0;

    ////////METHODS////////

    this.getWindow = function() {
        return Wgs.Map.Passport.Window;
    }

    this.open = function() {
        Wgs.Map.Passport.Window.add(Wgs.Map.Passport.PassportPanel);
        if (Wgs.Map.Passport.MapViewer) {
                Wgs.Map.Passport.MapViewer.Lib.getSelectedFeatures(function(featureLayers){
                    if(featureLayers != -1){
                        if (featureLayers[0].features && featureLayers[0].features.length == 1)
                        {
                            var featureId = featureLayers[0].features[0].featureId;
                            this.getPdf(featureId);
                        }
                        else
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Необходимо выбрать один объект',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                    else
                    Ext.WgsMsg.show({
                        title: 'Предупреждение',
                        msg: 'Необходимо выбрать один объект',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                },this);
            }
    }

    this.getPdf = function(featureId)
    {
         var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
         Wgs.Map.Passport.WgsConn.request({
            url: '/map/passport/print',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {featureId: featureId},
            success: function(response) {
                if (response) {
                    var reader = new Ext.data.JsonReader({},[
                       {name: 'seq'},
                       {name: 'passport'},
                       {name: 'floorplan'},
                       {name: 'passportId'}
                    ]);

                    var passport = reader.read(response).records[0];
                    if (passport.data.seq != -1) {
                        Wgs.Map.Passport.PassportId = passport.data.passportId;
                        var passportName = passport.data.passport+'-'+passport.data.seq;
                        Wgs.Map.Passport.PDFPassportPanel = new Ext.Panel({
                            layout: 'fit',
                            items: [{
                                html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/map/passport/getpdf/'+passportName+'"></param><EMBED src="/map/passport/getpdf/'+passportName+'" width="100%" height="100%" href="/map/passport/getpdf/'+passportName+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
                            }]
                        });

                        if (passport.data.floorplan != null) {
                            Wgs.Map.Passport.PassportPanel.floorPlanButton.setVisible(true);
                            Wgs.Map.FloorPlanName = passport.data.floorplan+'-'+passport.data.seq;
                        } else {

                            Wgs.Map.Passport.PassportPanel.floorPlanButton.setVisible(false);
                        }
                        Wgs.Map.Passport.PassportPanel.add(Wgs.Map.Passport.PDFPassportPanel);
                        Wgs.Map.Passport.Window.show();
                        msgWait.hide();
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Технический паспорт отсутствует',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }

            },
            scope: this
        });
    }

    this.openFloorPlan = function(floorplanName)
    {
        this.docWindow = new Ext.WgsShimWindow({
            title : 'Поэтажные планы',
            closable : true,
            width : 550,
            height : 600,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide',
            modal: true,
            draggable: false
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/map/passport/getpdf/'+floorplanName+'"></param><EMBED src="/map/passport/getpdf/'+floorplanName+'" width="100%" height="100%" href="/map/passport/getpdf/'+floorplanName+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);

        this.docWindow.show();
    }

    this.getFloorPlanDoc = function(passportId)
    {
        var msgWait = Ext.WgsMsg.wait('<p align="center">Выполняется генерация PDF-документа,</br> содержащего поэтажные планы.<p align="center"> Процесс может занять несколько минут.</p>', 'Подождите, пожалуйста', {interval: 200, style: 'width: 200px'});
        Wgs.Map.Passport.WgsConn.request({
            url: '/map/passport/getfloorplan',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {passportId: passportId},
            success: function(response) {
				msgWait.hide();
                
                if (response) {
                    this.openFloorPlan('floorplan-'+response.responseText);
                }
            },
            scope: this
        });

    }

    with (Wgs.Map.Passport.PassportPanel) {

        cancelButton.on('click', function(){
            //Wgs.Map.Passport.PassportPanel.remove(Wgs.Map.Passport.PDFPassportPanel, true);
            Wgs.Map.Passport.PassportPanel.remove(Wgs.Map.Passport.PassportPanel.items.first(), true);
            Wgs.Map.Passport.Window.hide();
        }, this);

        Wgs.Map.Passport.PassportPanel.floorPlanButton.on('click', function(){
            this.getFloorPlanDoc(Wgs.Map.Passport.PassportId);
        }, this);
    }
}

Map.Passport.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Passport(map);
    }
    return new Map.Passport(map);//this.instances[map.mapFrame];
}

Map.Passport.PassportPanel = Ext.extend(Ext.Panel, {
    constructor: function(config)
    {
        Map.Passport.PassportPanel.superclass.constructor.call(this, {
            //title: 'Технический паспорт',
            layout: 'fit',
            width: 500,
            height: 600,
            collapsible: false,
            //items: [this.objectsFieldSet,this.featureTypesFieldSet,this.propertiesFieldSet,this.statPassportGrid],
            frame: true
        });

        this.floorPlanButton = this.addButton({
            text : 'Поэтажные планы'
        });

        this.cancelButton = this.addButton({
            text : 'Закрыть'
        });
    }
});
