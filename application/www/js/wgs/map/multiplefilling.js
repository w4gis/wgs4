﻿Map.MultipleFilling = function(mapViewer)
{
    Ext.ns('Wgs.Map.MultipleFilling');

    Wgs.Map.MultipleFilling.MapViewer = mapViewer;
    Wgs.Map.MultipleFilling.ViewerLib = mapViewer.Lib;
    Wgs.Map.MultipleFilling.ViewerApi = mapViewer.Api;
    Wgs.Map.MultipleFilling.WgsConn = new Ext.data.WgsConnection();
    Wgs.Map.MultipleFilling.ObjectPanel = new Map.MultipleFilling.ObjectPanel;
    Wgs.Map.MultipleFilling.PatternCardFormPanel = new Map.MultipleFilling.PatternCardFormPanel;
    Wgs.Map.MultipleFilling.Mode = '';
    Wgs.Map.MultipleFilling.CallBack = null;
    Wgs.Map.MultipleFilling.CBScope = null;
    if (mapViewer) {
        Wgs.Map.MultipleFilling.Window = new Ext.WgsServiceWindow({
            containerPanel: mapViewer.rightPane,
            width: Wgs.Map.MultipleFilling.ObjectPanel.width+12,
            layout: 'fit',
            title: 'Множественное заполнение',
            resizable: true,
            minimizable: false
        });
    } else {
        Wgs.Map.MultipleFilling.Window = new Ext.WgsServiceWindowSimple({
            width: Wgs.Map.MultipleFilling.ObjectPanel.width+12,
            layout: 'fit',
            title: 'Множественное заполнение',
            resizable: true,
            minimizable: false
        });
    }
    Wgs.Map.MultipleFilling.Window.setInnerPanel(Wgs.Map.MultipleFilling.ObjectPanel);

    this.open = function(config)
    {
        if(!WGS.PRIVILEGE.CARD_EDIT) {
        	Wgs.Map.MultipleFilling.ObjectPanel.saveButton.hide();
        }
        var config = config||{};
        if (config.callback)
            Wgs.Map.MultipleFilling.CallBack = config.callback||Ext.emptyFn;
        if (config.scope)
            Wgs.Map.MultipleFilling.CBScope = config.scope||this;

        if (!config.objectIds && !config.featureIds) {
            if (Wgs.Map.MultipleFilling.MapViewer) {
                Wgs.Map.MultipleFilling.ViewerLib.getSelectedFeatures(function(featureLayers){
                    if(featureLayers.length > 0) {
                        var featureIds = [];
                        for (var i = 0; i < featureLayers.length; i++) {
                            for (var j = 0; j < featureLayers[i].features.length; j++) {
                                featureIds[featureIds.length] = featureLayers[i].features[j].featureId;
                            }
                        }
                    getFeatureTypes(featureIds, null);
                    } else {
                        Ext.WgsMsg.show({
                            title: 'Предупреждение',
                            msg: 'Необходимо выбрать объекты',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }, this);
            }
        } else {
            if (config.objectIds) {
                getFeatureTypes(null, config.objectIds);
                return;
            }
            if (config.featureIds) {
                getFeatureTypes(config.featureIds, null);
                return;
            }
        }
    }

    var getFeatureTypes = function (featureIds, objectIds)
    {
        if(featureIds && !objectIds) {
            Wgs.Map.MultipleFilling.Mode = 'features';
            Wgs.Map.MultipleFilling.Window.addPanel();
            Wgs.Map.MultipleFilling.Window.show();
            with (Wgs.Map.MultipleFilling.ObjectPanel) {
                objectTypesCombobox.clearValue();
                // Загрузили все карточки по выбранным объектам
                objectsStore.baseParams = {'featureIds[]':featureIds};
                objectsStore.reload();
                // Загрузили типы и слои выбранных объектов
                objectTypesStore.baseParams = {'featureIds[]':featureIds};
                objectTypesStore.reload();
            }
        }
        if(!featureIds && objectIds) {
           // alert(objectIds);
            Wgs.Map.MultipleFilling.Mode = 'objects';
            Wgs.Map.MultipleFilling.Window.addPanel();
            Wgs.Map.MultipleFilling.Window.show();
            with (Wgs.Map.MultipleFilling.ObjectPanel) {
                objectTypesCombobox.clearValue();
                // Загрузили все карточки по выбранным объектам
                objectsStore.baseParams = {'objectIds[]':objectIds};
                objectsStore.reload();
                // Загрузили типы и слои выбранных объектов
                objectTypesStore.baseParams = {'objectIds[]':objectIds};
                objectTypesStore.reload();
            }
        }
    }

    //EVENTS
    with (Wgs.Map.MultipleFilling.ObjectPanel) {
        cancelButton.on('click', function(){
            Wgs.Map.MultipleFilling.ObjectPanel.hide();
            with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
                getForm().setValues({
                            name: '',
                            shortName: '',
                            note: ''
                });
                if (advancedParamPanel) {
                    remove(advancedParamPanel);
                    advancedParamPanel = null;
                    doLayout();
                }
            }
        });

        saveButton.on('click', function(){
            var msgWait = Ext.Msg.wait('Идет сохранение параметров...', 'Подождите', {interval: 50});
            with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
                var propIds = [];
                var propValues = [];
                var name = '';
                var shortName = '';
                var note = '';
                var propertiesValue = getForm().getValues();
                for (i in propertiesValue) {
                    if (i == 'name') {
                        if (propertiesValue[i] != baseProperties[0].data.name) {
                            name = propertiesValue[i];
                        } else {
                            name = '&^&';
                        }
                    }
                    if (i == 'shortName') {
                        if (propertiesValue[i] != baseProperties[0].data.shortName) {
                            shortName = propertiesValue[i];
                        } else {
                            shortName = '&^&';
                        }
                    }
                    if (i == 'note') {
                        if (propertiesValue[i] != baseProperties[0].data.note) {
                            note = propertiesValue[i];
                        } else {
                            note = '&^&';
                        }
                    }
                    for (var j = 0; j < properties.length; j++) {
                        if (i == 'property-'+properties[j].data.propertyId) {
                            if (propertiesValue[i] != properties[j].data.propertyValue) {
                                propIds[propIds.length] = properties[j].data.propertyId;
                                propValues[propValues.length] = propertiesValue[i];
                            }
                        }
                    }
                }
                var params = {};
                if (propIds.length > 0) {
                    params = {'objectIds[]':objectIds, 'propIds[]':propIds, 'propValues[]':propValues, 'name': name, 'shortName':shortName, 'note': note };
                } else {
                    params = {'objectIds[]':objectIds,  'name': name, 'shortName':shortName, 'note': note };
                }

                Wgs.Map.MultipleFilling.WgsConn.request({
                    url: '/map/multiplefilling/setvalueproperties',
                    params: params,
                    method: 'POST',
                    success: function(response) {
                        var basePropertiesJsonReader = new Ext.data.JsonReader({}, [
                            {name: 'name'},
                            {name: 'shortName'},
                            {name: 'note'}
                        ]);

                        Wgs.Map.MultipleFilling.WgsConn.request({
                            url: '/map/multiplefilling/getbasevalueproperties',
                            method: 'POST',
                            params: {'objectIds[]':objectIds},
                            success: function(response) {
                                baseProperties = basePropertiesJsonReader.read(response).records;
                            }
                        });

                        var propertiesJsonReader = new Ext.data.JsonReader({}, [ {name: 'propertyId'}, {name: 'propertyValue'} ]);

                        Wgs.Map.MultipleFilling.WgsConn.request({
                            url: '/map/multiplefilling/getvalueproperties',
                            method: 'POST',
                            params: {'objectIds[]':objectIds},
                            success: function(response) {
                                properties = propertiesJsonReader.read(response).records;
                            }
                        });
                       //Wgs.Map.MultipleFilling.CallBack.call(Wgs.Map.MultipleFilling.CBScope);
                       msgWait.hide();
                     }
                });
            }
        });

        objectTypesCombobox.on('select', function (combo, record, index) {
            combo.setValue(record.data.label);

            with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
                if (advancedParamPanel) {
                    remove(advancedParamPanel);
                    advancedParamPanel = null;
                    doLayout();
                }
                featureLayerId = record.data.featureLayerId;
                objectTypeId = record.data.objectTypeId;
                advancedParamJsonStore.baseParams = {'objectTypeId': record.data.objectTypeId};
                advancedParamJsonStore.reload();
            }
        });
        objectTypesCombobox.on('beforeselect', function (combo, record, index) {
            if (Wgs.Map.MultipleFilling.Mode == 'features') {
                if(record.data.featureLayerId == -1 && record.data.objectTypeId == -1) {
                    return false;
                } else {
                    return true;
                }
            } else
                return true;
        });

        objectTypesStore.on('load', function(store, records, options) {
            if (records.length > 0) {
                var index = 0;
                if (Wgs.Map.MultipleFilling.Mode == 'features')
                    index = 1;
                else
                    index = 0;
                objectTypesCombobox.setValue(records[index].data.label);
                attrCardPanel.add(Wgs.Map.MultipleFilling.PatternCardFormPanel);
                with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
                    if (advancedParamPanel) {
                        remove(advancedParamPanel);
                        advancedParamPanel = null;
                        doLayout();
                    }
                    featureLayerId = records[index].data.featureLayerId;
                    objectTypeId = records[index].data.objectTypeId;
                    advancedParamJsonStore.baseParams = {'objectTypeId': records[index].data.objectTypeId};
                    advancedParamJsonStore.reload();
                }
                doLayout();
            }
        });
    }

    with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
        var getPropertyValue = function (properties, propertyId)
        {
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].data.propertyId == propertyId)
                    return properties[i].data.propertyValue;
            }
            return;
        }

        advancedParamJsonStore.on('load', function(store, records, options) {
            //Получаем идшники объектов
            with (Wgs.Map.MultipleFilling.ObjectPanel) {
                var objIds = [];
                if (Wgs.Map.MultipleFilling.Mode == 'features') {
                    objectsStore.each(function(record) {
                        if(record.data.objectTypeId == objectTypeId && record.data.featureLayerId == featureLayerId) {
                            objIds[objIds.length] = record.data.objectId;
                        }
                    });
                } else {
                    objectsStore.each(function(record) {
                        if(record.data.objectTypeId == objectTypeId) {
                            objIds[objIds.length] = record.data.objectId;
                        }
                    });
                }
            }
            objectIds = objIds;

            var basePropertiesJsonReader = new Ext.data.JsonReader({}, [
                {name: 'name'},
                {name: 'shortName'},
                {name: 'note'}
            ]);

            Wgs.Map.MultipleFilling.WgsConn.request({
                url: '/map/multiplefilling/getbasevalueproperties',
                method: 'POST',
                params: {'objectIds[]':objectIds},
                success: function(response) {
                    baseProperties = basePropertiesJsonReader.read(response).records;
                    with (Wgs.Map.MultipleFilling.PatternCardFormPanel) {
                        getForm().setValues({
                            name: baseProperties[0].data.name,
                            shortName: baseProperties[0].data.shortName,
                            note: baseProperties[0].data.note
                        });
                    }
                }

            });

            var propertiesJsonReader = new Ext.data.JsonReader({}, [
                {name: 'propertyId'},
                {name: 'propertyValue'}
            ]);

            Wgs.Map.MultipleFilling.WgsConn.request({
                url: '/map/multiplefilling/getvalueproperties',
                method: 'POST',
                params: {'objectIds[]':objectIds},
                success: function(response) {

                        properties = propertiesJsonReader.read(response).records;
                        var domainStores = [];
                        var group_id = -1;
                        var groupFieldSet;
                        advancedParamPanel = (advancedParamPanel)? advancedParamPanel: new Ext.Panel({});
                        advancedParamJsonStore.each(function(record)
                        {
                            if (group_id != record.data.groupId)
                            {
                                if (groupFieldSet)
                                {
                                    advancedParamPanel.add(groupFieldSet);
                                }
                                groupFieldSet = new Ext.form.FieldSet(
                                {
                                    title: 'Группа характеристик: '+record.data.groupName,
                                    autoHeight:true,
                                    collapsible : true,
                                    closable: false,
                                    anchor: '97%',
                                    collapsed: true
                                });
                                if (group_id == -1) {groupFieldSet.collapsed = false}
                                group_id = record.data.groupId;
                            }

                            switch (record.data.valueTypeId) {
                                case '1': {
                                    var propertyEditor = new Ext.form.TextField(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'property-' + record.data.id,
                                        anchor: '97%',
                                        allowBlank:true,
                                        value: getPropertyValue(properties, record.data.id)
                                    });
                                    groupFieldSet.add(propertyEditor);
                                    break;
                                }
                                case '2': {
                                    var propertyEditor = new Ext.form.DateField(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'property-' + record.data.id,
                                        allowBlank:true,
                                        anchor: '97%',
                                        value: getPropertyValue(properties, record.data.id),
                                        //altFormats : 'd.m.y|d.m.Y|',
                                        //format: 'd.m.Y'
                                        format: 'Y-m-d'
                                    });
                                    groupFieldSet.add(propertyEditor);
                                    break;
                                }
                                case '3': {
                                    var propertyEditor = new Ext.form.NumberField(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'property-' + record.data.id,
                                        anchor: '97%',
                                        allowBlank:true,
                                        value: getPropertyValue(properties, record.data.id),
                                        allowDecimals: false,
                                        maxText : 10
                                    });
                                    groupFieldSet.add(propertyEditor);
                                    break;
                                }
                                case '4': {
                                    var propertyEditor = new Ext.form.NumberField(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'property-' + record.data.id,
                                        anchor: '97%',
                                        allowBlank:true,
                                        value: getPropertyValue(properties, record.data.id),
                                        allowDecimals: true,
                                        maxText : 10,
                                        decimalSeparator:','
                                    });
                                    groupFieldSet.add(propertyEditor);
                                    break;
                                }
                                case '5': {
                                    domainStores[domainStores.length] = new Ext.data.JsonStore(
                                    {
                                        fields: ['id', 'name'],
                                        url: '/map/multiplefilling/getvaluedomainlist',
                                        baseParams:{propId: record.data.id}
                                    });
                                    domainStores[domainStores.length-1].load(
                                    {
                                        callback : function()
                                        {
                                            propertyEditor.setValue(getPropertyValue(properties, record.data.id));
                                            propertyHidden.setValue(propertyEditor.getValue());
                                        }
                                    });
                                    var propertyHidden = new Ext.form.TextField(
                                    {
                                        name: 'property-' + record.data.id,
                                        hideLabel: true,
                                        hidden: true,
                                        labelSeparator:'',
                                        value: getPropertyValue(properties, record.data.id)
                                    });
                                    var propertyEditor = new Ext.form.WgsComboBox(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'grid-' + record.data.id,
                                        store: domainStores[domainStores.length-1],
                                        anchor:'97%',
                                        displayField:'name',
                                        valueField: 'id',
                                        typeAhead: true,
                                        loadingText:'Список загружается...',
                                        triggerAction: 'all',
                                        emptyText:'Выберите значение...',
                                        selectOnFocus:true
                                    });
                                    propertyEditor.on('select',function(combo, record, index )
                                    {
                                        propertyHidden.setValue(propertyEditor.getValue());
                                    });

                                    groupFieldSet.add(propertyEditor);
                                    groupFieldSet.add(propertyHidden);
                                    break;
                                }
                                case '7': {
                                    var propertyEditor = new Ext.form.TextArea(
                                    {
                                        fieldLabel: record.data.name,
                                        name: 'property-' + record.data.id,
                                        anchor: '97%',
                                        allowBlank:true,
                                        value: getPropertyValue(properties, record.data.id)
                                    });
                                    groupFieldSet.add(propertyEditor);
                                    break;
                                }
                            }
                        });
                        if (groupFieldSet)
                        {
                            advancedParamPanel.add(groupFieldSet);
                            add(advancedParamPanel);
                            doLayout();
                        }
controlReadOnly();

                }
            });

        });
    }
}

Map.MultipleFilling.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.MultipleFilling(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.MultipleFilling(map);
        }
        return this.instance;
    }
}

Map.MultipleFilling.Create = function(map)
{
    return new Map.MultipleFilling(map);
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ПАНЕЛЬ ДОПОЛНИТЕЛЬНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*



//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//*-*-*-*-*-ОСНОВНАЯ ПАНЕЛЬ КАРТОЧКИ (АТРИБУТИВНЫХ ОБЪЕКТОВ)-*-*-*-*-*-*-*
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.MultipleFilling.PatternCardFormPanel = Ext.extend(Ext.form.WgsFormPanel, {

	controlReadOnly: function()
    {
        if(!WGS.PRIVILEGE.CARD_EDIT)
            this.setReadOnly(true, false);
    },

	setReadonlyPrivate: function(f, bReadOnly)
    {
        if(typeof f._readOnly == 'undefined')
        {
            if(f.initEvents)
                f.initEvents = f.initEvents.createSequence(self.initEventsSequence)
        }
        f._readOnly = bReadOnly;
        f.getEl().dom.readOnly = bReadOnly;
        if (f instanceof Ext.form.TriggerField)
        {
            if (bReadOnly){
                f.trigger.un('click', f.onTriggerClick, f);
                if(f.keyNav)
                    f.keyNav.disable();
            }
            else {
                f.trigger.on('click', f.onTriggerClick, f, {preventDefault:true});
                if(f.keyNav)
                    f.keyNav.enable();
            }
        }
    },

	setReadOnly: function(readOnly, buttonVisible)
    {
        this.getForm().items.each(function(f){
            this.setReadonlyPrivate(f, readOnly);
        }, this);
        if(this.advancedParamPanel)
            if(this.advancedParamPanel.items)
                this.advancedParamPanel.items.each(function(fieldset){
                    if(fieldset.items)
                        fieldset.items.each(function(field){
                            this.setReadonlyPrivate(field, readOnly);
                        }, this);
                }, this);

    },

    constructor: function()
    {
        this.featureLayerId = 0;
        this.objectTypeId = 0;
        this.properties = null;
        this.baseProperties = null;
        this.objectIds = [];
        this.baseParamFieldSet = new Ext.form.FieldSet({
            title: 'Основные характеристики',
            layout: 'form',
            autoHeight:true,
            autoWidth:true,
            defaultType: 'textfield',
            closable: false,
            items: [
            {
                fieldLabel: 'Наименование',
                name: 'name',
                anchor: '97%',
                allowBlank:true
            },{
                fieldLabel: 'Краткое наименование',
                name: 'shortName',
                anchor: '97%',
                allowBlank:true
            },
            new Ext.form.TextArea(
            {
                fieldLabel: 'Примечание',
                name: 'note',
                anchor: '97%',
                allowBlank:true
            })
            ]
        });

        this.advancedParamJsonStore = new Ext.data.JsonStore({
            fields: ['name', 'value', 'valueTypeId', 'groupId', 'groupName', 'id'],
            url: '/map/multiplefilling/getpropertiesbyobjecttype'
        });

        this.advancedParamPanel = null;

        Map.MultipleFilling.PatternCardFormPanel.superclass.constructor.call(this,
        {
            frame:true,
            url:'/objectadmin/SetCard',
            layout:'fit',
            autoWidth: true,
            closable: false,
            autoScroll: true,
            items: [this.baseParamFieldSet]
        });
    }
});


Map.MultipleFilling.ObjectPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function()
    {
        this.objectsStore = new Ext.data.WgsJsonStore({
            url:'/map/multiplefilling/getcards',
            method: 'POST',
            fields: ['featureLayerId', 'objectId', 'objectTypeId']
        });

        this.objectTypesStore = new Ext.data.WgsJsonStore({
            url:'/map/multiplefilling/getobjecttypes',
            method: 'POST',
            fields: ['featureLayerId', 'objectTypeId', 'featureLayerName', 'objectTypeName', 'name', 'label']
        });

        this.objectTypesCombobox = new Ext.form.WgsComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            displayField : 'name',
            valueField : 'featureLayerId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable: false,
            store : this.objectTypesStore,
            mode: 'local'
        });

        this.basePropertiesReader = new Ext.data.JsonReader({},[
           {name: 'name'},
           {name: 'shortName'},
           {name: 'typeName'},
           {name: 'createDate'},
           {name: 'note'}
        ]);

        this.attrCardPanel = new Ext.Panel({
            region: 'south',
            height: 530,
            autoWidth: true,
            layout: 'fit'
        });

        this.listCardFieldSet = new Ext.form.FieldSet({
            title: 'Список описаний',
            autoHeight: true,
            items: this.objectTypesCombobox
        });


        Map.MultipleFilling.ObjectPanel.superclass.constructor.call(this, {
            width: 600,
            autoHeight: true,
            layout: 'border',
            frame: true,
            maskDisabled: false,
            collapsible: false,
            items: [{
                region: 'center',
                autoHeight: true,
                layout: 'fit',
                items:  [ this.listCardFieldSet]
            },
            this.attrCardPanel]
        });

        this.saveButton = this.addButton({
			iconCls: 'icon-accept-tick',
            text : 'Применить'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Закрыть',
            scope : this
        });
    }
});