﻿Ext.namespace('Map');

Map.SelectRadius = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.viewerApi = mapViewer.Api;   
    this.conn = new Ext.data.WgsConnection();
    this.map = this.viewerApi.map;
    this.mouseButton = null;
    this.isMouseDown = false;
    this.raphael = null;
    this.centerX = 0;
    this.centerY = 0; 
    this.radius = 0;
    this.appending = false;
}

Map.SelectRadius.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.SelectRadius(map);
    }    
    return this.instances[map.mapFrame];
}

Map.SelectRadius.prototype = {
    open: function()
    {
        this.map.document.body.onselectstart = function()
        {
            return false;
        };
        
        this.raphael = this.mapViewer.Lib.getRaphaelDrawing();
        this.raphael.createCanvas();
               
        this.viewerApi.Map.ObserveOnMapLoaded(function () {
            this.setDefaultParams();
        }, this);
        
        this.viewerApi.Map.OnContextMenu(function(e){
           return false;           
        },this);
        
        this.viewerApi.Map.OnMouseDown(function(e){
            this.mouseButton = (e.button) ? e.button : e.which;
            if (this.mouseButton == 1) {
                var x = e.clientX - this.map.mapPosX;
                var y = e.clientY;
                this.startDrawCircle(x, y);
            }
            this.appending = e.shiftKey;
            this.map.appending = this.appending;
        },this);
        
        this.viewerApi.Map.OnMouseMove(function(e){
            var x = e.clientX - this.map.mapPosX;
            var y = e.clientY;
            this.drawCircle(x, y);
           
        },this);
        
        this.viewerApi.Map.OnMouseUp(function(e){
            this.mouseButton = (e.button) ? e.button : e.which;
            if (this.mouseButton == 1) {
                this.isMouseDown = false;
                var x = e.clientX - this.map.mapPosX;
                var y = e.clientY;
                this.finishDrawCircle(x, y);
            }   else {
                if (this.raphael) {
                    
                }
            }        
        },this);
        
        WGS.Tools.addHandler(this.map.document, 'keydown', this.onKeyDown.createDelegate(this), this);
    },
    
    onKeyDown: function(e)
    {
        if (e.keyCode == 27) {
           this.setDefaultParams();
        }
    },
    
    startDrawCircle: function(x, y) 
    {
        this.isMouseDown = true;
        this.centerX = x;
        this.centerY = y;
        this.raphael.drawTemporaryCircle(this.centerX, this.centerY, 0, {fill: "#00f", stroke: "#036", "fill-opacity": 0.1});
    },
    
    drawCircle: function(x, y) 
    {
        if(this.isMouseDown) {
            var startPoint = this.map.ScreenToMapUnits(this.centerX, this.centerY); 
            var currPoint  = this.map.ScreenToMapUnits(x, y);
            var radiusInMap = Math.round(Math.sqrt(Math.pow(currPoint.X-startPoint.X,2)+Math.pow(currPoint.Y-startPoint.Y,2)));
            var labelR = (radiusInMap < 1000)? radiusInMap+' м' : (Math.round(radiusInMap)/1000).toFixed(2) + ' км';
            this.radius = Math.sqrt(Math.pow(x-this.centerX,2)+Math.pow(y-this.centerY,2));
            this.raphael.drawTemporaryCircle(this.centerX, this.centerY, this.radius, {fill: "#00f", stroke: "#036", "fill-opacity": 0.1});
            this.raphael.drawTemporaryLabel1(this.centerX, this.centerY, labelR, {fill: "#000", "font-family": "Arial", "font-size": 10});
        }
    },
    
    finishDrawCircle: function(x, y) 
    {
        var startPoint = this.map.ScreenToMapUnits(this.centerX, this.centerY); 
        var currPoint  = this.map.ScreenToMapUnits(x, y);
        var radiusInMap = Math.round(Math.sqrt(Math.pow(currPoint.X-startPoint.X,2)+Math.pow(currPoint.Y-startPoint.Y,2)));
        this.requestCircleSelection(startPoint.X, startPoint.Y, radiusInMap, true);
        this.isMouseDown = false;
        //this.setDefaultParams();
    },
    drawByStrangePoints: function(coords, reset){
		var map = window.mapViewer.Api.map;
		var reset = reset || false;
		var x = Math.ceil((coords.x - map.extX1) * map.mapDevW /(map.extX2-map.extX1))
		var y = Math.ceil((coords.y - map.extY1) * (-1) * map.mapDevH /(map.extY1-map.extY2))
		var labelR = (coords.r < 1000)? coords.r+' м' : (Math.round(coords.r)/1000).toFixed(2) + ' км';
        var s = mapViewer.Api.Map.GetScale();
		var r = coords.r* map.mapDevH /(map.extY1-map.extY2)
		
		this.raphael.drawTemporaryCircle(x, y, r, {fill: "#00f", stroke: "#036", "fill-opacity": 0.1});
        this.raphael.drawTemporaryLabel1(x, y, labelR, {fill: "#000", "font-family": "Arial", "font-size": 10});
		if (reset)
			this.coords = []			
	},
    requestCircleSelection: function(x, y, r)
    {
        var x0 = x - r;
        var xn = x + r;
        var k = 10;
        var dr = 2*r/k;
        
        var a = [];
        var b = [];
        var coords = '';
        for(var i = 1; i <= k; i++ ) {
            var xi = x0 + i*dr;
            var yi1 = y - Math.sqrt(Math.pow(r,2) - Math.pow((xi-x), 2));
            var yi2 = y + Math.sqrt(Math.pow(r,2) - Math.pow((xi-x), 2));
            coords = coords + xi +' '+yi1+', '
            a[a.length] = xi +' '+yi1;
            b[b.length] = xi +' '+yi2;
        }
		strCoordsSdo = ''
        for (var j = b.length; j >=0; j--) {
           if(b[j]) {
                if (j == 0) {
                    coords = coords + b[j];
					strCoordsSdo = strCoordsSdo + b[j] +','
        
                } else {
                    coords = coords + b[j]+', ';
					strCoordsSdo = strCoordsSdo + b[j] +','
        
                }
            }
        }
		window.mapState.selectionSdo =  strCoordsSdo + a[0]
        
		window.mapState.selectionFtf = 'POLYGON(('+coords+', '+ a[0]+'))';
        var geom = x+','+y+','+r;
        window.mapState.selection = geom
		window.mapState.selectionType = 'radius'
		window.socket.postMessage(Ext.util.JSON.encode({type:70}));
			
		/*console.log(window)
		alert(window.mapState.selection)
        this.conn.request({
            url: '/mapguide/mapagent/mapagent.fcgi',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {
                OPERATION: 'QUERYMAPFEATURES',
                VERSION: '1.0.0',
                PERSIST: '1',
                MAPNAME: this.viewerApi.Map.GetMapName(),
                SESSION: this.viewerApi.Map.GetSessionId(),
                SEQ: Math.random(),
                LAYERNAMES: this.map.GetVisSelLayers(),
                GEOMETRY: geom,
                SELECTIONVARIANT: 'INTERSECTS',
                MAXFEATURES: -1
            },
            success: function(response) {
                //this.viewerApi.Map.Refresh();
                this.map.ProcessFeatureInfo(response.responseXML, this.appending, 7);
            },
            scope: this
        });*/
			
    },
    
    setDefaultParams: function (reset)
    {
        var reset = reset || false;
		if (this.raphael) {
		if(reset)
            this.raphael.clear();
            this.centerX = 0;
            this.centerY = 0; 
            this.radius = 0;
            this.isMouseDown = false;
        }
    }
    
}
