﻿Ext.namespace('Map');

Map.ViewerLib = function(viewerApi)
{
    this.ViewerApi = viewerApi;
    this.isDWF     = this.ViewerApi.isDWF;
    this.isAJAX    = this.ViewerApi.isAJAX;
}

Map.ViewerLib.prototype = {
    zoomSelectedFeatures: function(selectionXML, callback, scope, useCurrentScale)
    {
        var map = this.ViewerApi.Map.getMap();
        var useCurrentScale = useCurrentScale||false;

        var reqParams = "OPERATION=GETFEATURESETENVELOPE&VERSION=1.0.0&SESSION=" + map.GetSessionId() + "&MAPNAME=" + encodeURIComponent(map.GetMapName()) + "&SEQ=" + Math.random();
        reqParams += "&FEATURESET=" + encodeURIComponent(selectionXML);

        (new Ext.data.Connection()).request({
            url: '/mapguide/mapagent/mapagent.fcgi',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: reqParams,
            success: function(response) {
                if (response.responseXML) {
                    var xmlRoot = response.responseXML.documentElement;
                    if(xmlRoot.tagName != "Envelope")
                        return null;

                    var env = {lowerLeft: {X: null, Y: null}, upperRight: {X: null, Y: null}};
                    var xs = xmlRoot.getElementsByTagName("X");
                    var ys = xmlRoot.getElementsByTagName("Y");
                    env.lowerLeft.X = parseFloat(xs[0].childNodes[0].nodeValue);
                    env.lowerLeft.Y = parseFloat(ys[0].childNodes[0].nodeValue);
                    env.upperRight.X = parseFloat(xs[1].childNodes[0].nodeValue);
                    env.upperRight.Y = parseFloat(ys[1].childNodes[0].nodeValue);

                    var mcsW = env.upperRight.X - env.lowerLeft.X;
                    var mcsH = env.upperRight.Y - env.lowerLeft.Y;
                    var centerX = env.lowerLeft.X + mcsW / 2;
                    var centerY = env.lowerLeft.Y + mcsH / 2;

                    var mapScale;
                    var DPI = 96;
                    var metersPerPixel = 0.0254 / DPI;
                    var metersPerUnit = this.ViewerApi.Map.GetMetersPerUnits();
                    /*
                    var mapDevH, mapDevW;

                    if (this.isAJAX) {
                        var consolePaneHeight = (Ext.getCmp('consolePane'))? Ext.getCmp('consolePane').getSize().height: 0;

                        mapDevH = this.ViewerApi.Map.map.mapDevH - (consolePaneHeight + 80);
                        mapDevW = this.ViewerApi.Map.map.mapDevW;

                        centerY -= centerY * ((consolePaneHeight + 80) * metersPerPixel / 2);
                        mcsH -= mcsH * ((consolePaneHeight + 80) * metersPerPixel);
                    } else {
                        mapDevH = $(this.ViewerApi.Map.map.DWFViewer).getHeight();
                        mapDevW = $(this.ViewerApi.Map.map.DWFViewer).getWidth() - this.ViewerApi.Map.map.NavpaneWidth;
                    }
                    */

                    var mapDimension = this.ViewerApi.Map.GetMapDimension();
                    var mapDevH = mapDimension.height, mapDevW = mapDimension.width;

                    if (this.isAJAX) {
                        //centerY -= centerY * ((consolePaneHeight + 80) * metersPerPixel / 2);
                        //mcsH -= mcsH * ((consolePaneHeight + 80) * metersPerPixel);
                    }

                    if (useCurrentScale) {
                        mapScale = this.ViewerApi.Map.GetScale();
                    } else {
                        if (mapDevH * mcsW*2 > mapDevW * mcsH*2)
                            mapScale = mcsW*2 * metersPerUnit / (mapDevW * metersPerPixel) + (Math.random()/1000000);
                        else
                            mapScale = mcsH*2 * metersPerUnit / (mapDevH * metersPerPixel) + (Math.random()/1000000);
                        // (Math.random()/1000000) - погрешность для форсированного обновления карты
                        //фикс для точек
                        if (mapScale < 1) {
                            mapScale = 200;
                        }
                    }

                    this.ViewerApi.Map.ZoomToView(centerX, centerY, mapScale, true);
                    if (callback) {
                        callback.call();
                    }
                }
            },
            scope: this
        });
    },

    selectFeatures: function(features, callback, scope, useCurrentScale)
    {
        if (features) {
            var mapName = this.ViewerApi.Map.GetMapName();
            var msgWait = Ext.WgsMsg.wait('Запрос данных...', 'Подождите', {interval: 80});
            (new Ext.data.WgsConnection()).request({
                url: '/map/lib/selectfeatures',
                method: 'POST',
                params: Ext.isArray(features)? {mapname: mapName, 'features[]': features}: {mapname: mapName, 'features[]': features.features, 'objects[]': features.objects},
                success: function(response) {
                    this.ViewerApi.Map.OnEndTransaction(function(){
                        this.ViewerApi.Map.SetSelectionXML(response.responseText);
                        msgWait.hide();
                        if (callback) {
                            if (scope) {
                                callback.call(scope);
                            } else {
                                callback.call();
                            }
                        }
                    },this);
                    this.zoomSelectedFeatures(response.responseText, false, false, useCurrentScale);
                },
                scope: this
            });
        }
    },

    getSelectedFeatures: function(callback, scope)
    {
        var selectionXML = this.ViewerApi.Map.GetSelectionXML();
        if (selectionXML) {
            var mapName = this.ViewerApi.Map.GetMapName();
            var msgWait = Ext.WgsMsg.wait('Запрос данных...', 'Подождите', {interval: 80});
            (new Ext.data.WgsConnection()).request({
                url: '/map/lib/getselectedfeatures',
                method: 'POST',
                params: {mapname: mapName, selection: selectionXML},
                success: function(response) {
                    try {
                        var jsonData = Ext.util.JSON.decode(response.responseText)
                    } catch(e) {}
                    msgWait.hide();
                    callback.call(this, jsonData);
                },
                scope: scope
            });
        } else {
            callback.call(scope || this, -1);
        }
    },

    getSelectedFeatureIds: function(/* result of getSelectedFeatures */ featureLayers)
    {
        var features = [];
        for (var i = 0; i < featureLayers.length; i++) {
            for (var j = 0; j < featureLayers[i].features.length; j++) {
                features.push(featureLayers[i].features[j].featureId);
            }
        }
        return features;
    },

    getFeaturesByPolygon: function (coords, callback, scope)
    {
        var mass = [];
        var massCoords = [];
        if (coords.length-1 > 2) {
            for (var i = 0; i < coords.length-1; i++) {
                mass[i] = this.ViewerApi.Map.ScreenToMapUnits(coords[i].x, coords[i].y);
                massCoords.push(mass[i].X);
                massCoords.push(mass[i].Y);
            }
        }

        if((coords.length-1)%2 == 0) {
            massCoords.push(mass[0].X);
            massCoords.push(mass[0].Y);
        }

        if(coords.length-1 == 3) {
            massCoords.push(mass[0].X);
            massCoords.push(mass[0].Y);
        }

        var jsonData = {};
        var mapName = this.ViewerApi.Map.GetMapName();
        var msgWait = Ext.WgsMsg.wait('Запрос данных...', 'Подождите', {interval: 80});
        (new Ext.data.WgsConnection()).request({
            url: '/map/lib/getfeaturesbypolygon',
            method: 'POST',
            params: {mapname: mapName, 'coords[]': massCoords},
            success: function(response) {
                try {
                    jsonData = Ext.util.JSON.decode(response.responseText);
                } catch(e) {}
                msgWait.hide();
                callback.call(scope, jsonData);
            }
        });
    },

    getRaphaelDrawing: function ()
    {
        if(!this.raphaelDrawing)
            this.raphaelDrawing = new Map.ViewerLib.RaphaelDrawing(this.ViewerApi);
        return this.raphaelDrawing;
    },
    
    setLayerVisibility: function (layerView)//layer and layer group crc [1,2,3,4,5]
    {
        var mapName = this.ViewerApi.Map.GetMapName();
        var msgWait = Ext.WgsMsg.wait('Восстановление вида...', 'Подождите', {interval: 80});
        (new Ext.data.WgsConnection()).request({
            url: '/map/lib/setlayervisibility',
            method: 'POST',
            params: {mapname: mapName, 'layerview[]': layerView},
            success: function(response) {
            	msgWait.hide();
                this.ViewerApi.Map.Refresh();
                this.ViewerApi.Legend.Refresh();
            },
            scope: this
        });
    },
    
    displayTheme: function(themeId)
    {
    	//
    }
}