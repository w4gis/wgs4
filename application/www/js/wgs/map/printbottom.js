﻿Ext.namespace('Map');

Map.PrintBottom = Ext.extend(Map.Print, {
	constructor: function(arg){
		Map.PrintBottom.superclass.constructor.call(this, arg);
		
		this.window.setTitle('Печать в PDF (дополнительно)');
	},
	
	getParameters: function(){
		var parameters = Map.PrintBottom.superclass.getParameters.call(this);
		parameters.bottom = 1;
		return parameters;
	}
});

Map.PrintBottom.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.PrintBottom(map);
    }    
    return this.instances[map.mapFrame];
}