﻿Ext.namespace('Map');

Map.CardNew = function(mapViewer) {
	this.mapViewer = mapViewer;
	this.conn = new Ext.data.Connection();
	this.wgsConn = new Ext.data.WgsConnection();
	this.panel = new Map.CardNew.CardPanel();
	// this.loadMask = null;
	this.attrCard = null;
	this.modeCard = 0;
	this.featureId = 0;
	this.entityId = 0;
	this.objectId = 0;
	this.objectIdForPrint = 0;
    
    
    this.reportId = 0;
    this.layerId = 0;
	
    
    if (mapViewer) {
		this.window = new Ext.WgsServiceWindow({
			containerPanel : mapViewer.rightPane,
			width : this.panel.width + 12,
			layout : 'fit',
			title : 'Анализ муниципальных объектов',
			resizable : false,
			minimizable : false
		});
	} else {
		this.window = new Ext.WgsServiceWindowSimple({
			width : this.panel.width + 12,
			layout : 'fit',
			title : 'Анализ муниципальных объектов',
			resizable : false,
			minimizable : false
		});
	}
	this.window.setInnerPanel(this.panel);
	// this.panel.featuredocButton.on('click', this.openFeatureDoc, this);
	this.panel.cardReportCombobox.on('select', this.cardComboboxOnReportSelect,this);            
    this.panel.cardGeometryCombobox.on('select',this.cardComboboxOnGeometrySelect, this);
    this.panel.cardLayerCombobox.on('select', this.cardComboboxOnLayerSelect,this);
    this.panel.printButton.on('click', this.printCard, this);
    this.panel.printButton2.on('click', this.printCard2, this);
    //this.panel.cardLayerCombobox.on('select',this.cardComboboxOnLayerSelect, this);
    
    /*
    this.panel.groupPropCombobox.on('select', this.groupPropComboboxOnSelect,this);
	this.panel.cardLayerCombobox.on('select', this.cardComboboxOnLayerSelect,this);
	
	this.panel.cardCombobox.on('select', this.cardComboboxOnSelect, this);
	this.panel.cardComboboxYear.on('select', this.cardComboboxOnYearSelect,this);
    */
    this.panel.cancelButton.on('click', this.onCancel, this);

}

Map.CardNew.Instance = function(map) {
	if (!this.instances) {
		this.instances = {};
	}
	if (map) {
		if (!this.instances[map.mapFrame]) {
			this.instances[map.mapFrame] = new Map.CardNew(map);
		}
		return this.instances[map.mapFrame];
	} else {
		if (!this.instance) {
			this.instance = new Map.CardNew(map);
		}
		return this.instance;
	}
}

Map.CardNew.Create = function(map) {
	return new Map.CardNew(map);
}

Map.CardNew.prototype = {
	open : function(config) {
		if (!WGS.PRIVILEGE.CARD_EDIT)
			this.panel.saveButton.hide();
		var config = config || {};
		this.cardCallback = config.callback || Ext.emptyFn;
		this.cardScope = config.scope || this;
        

        
            var featureId = 0;
                            this.entityId = 0;
                            this.getCards(featureId, null);

	},

	getCards : function(featureId, objectId) {
		var cardJsonReader = new Ext.data.JsonReader({}, [{
			name : 'cardId'
		}, {
			name : 'cardName'
		}, {
			name : 'cardIsCurrent'
		}, {
			name : 'cardTypeName'
		}, {
			name : 'cardRelationName'
		}, {
			name : 'cardLabel'
		}]);
		var layerJsonReader = new Ext.data.JsonReader({}, [{
			name : 'featureLayerId'
		}, {
			name : 'legend'
		}]);
		
		var reportJsonReader = new Ext.data.JsonReader({}, [{
			name : 'id'
		}, {
			name : 'report_name'
		}]);
		var geometryJsonReader = new Ext.data.JsonReader({}, [{
			name : 'id'
		}, {
			name : 'name'
		}, {
			name : 'object_type_id'
		}, {
			name : 'featurelayer_id'
		}]);
    
		this.wgsConn.request({
			url : '/objectadmin/card/getlistlayers',
			method : 'POST',
			params : {},
			success : function(response) {
				if (response.responseText != 'null') {
					// alert(cardJsonReader.read(response).records.length);
					if (layerJsonReader.read(response).records.length > 0) {
						var layers = layerJsonReader.read(response).records;
						// this.open();
						this.window.addPanel();
						this.window.show();
						this.panel.cardLayerCombobox.clearValue();
						// this.panel.cardComboboxYear.clearValue();
						var objectId = layers[0].data.featureLayerId;
						this.panel.layerStore.removeAll();
						this.panel.layerStore.add(layers);
						this.panel.cardLayerCombobox
								.setValue(layers[0].data.legend);
                        //alert ("current layer: "+ layers[0].data.legend);
                        this.layerId = layers[0].data.featureLayerId;
                        //alert("currentLayerId: "+this.layerId);
						this.wgsConn.request({
							url : '/objectadmin/card/getlistgeometrybylayer',
							method : 'POST',
							params : {
								layerId : objectId
							},
							success : function(response) {
								// alert("objectId: "+objectId);
								if (response.responseText != 'null') {
									// alert(cardJsonReader.read(response).records.length);
									if (geometryJsonReader.read(response).records.length > 0) {
                                       
										var geom = geometryJsonReader
												.read(response).records;
                                              /*   alert("GEOMETRY: "+geom[0].data.name+" | "+
                                                 geom[0].data.object_type_id+" | "+
                                                 geom[0].data.featurelayer_id+" | "+
                                                 geom[0].data.id);*/
										 //alert(response.responseText);
										// this.open();
										this.window.addPanel();
										this.window.show();
										this.panel.cardGeometryCombobox
												.clearValue();
										// this.panel.cardComboboxYear.clearValue();
										// var objectId = layers[0].data.id;
										this.panel.geometryStore.removeAll();
										this.panel.geometryStore.add(geom);
										this.panel.cardGeometryCombobox
												.setValue(geom[0].data.name);

									} else {
										Ext.WgsMsg.show({
											title : 'Предупреждение',
											msg : 'Необходимо выбрать один объект',
											buttons : Ext.MessageBox.OK,
											icon : Ext.MessageBox.WARNING
										});
									}
								} else {
									Ext.WgsMsg.show({
										title : 'Предупреждение',
										msg : 'Слоев не найдено',
										buttons : Ext.MessageBox.OK,
										icon : Ext.MessageBox.WARNING
									});
								}
							},
							scope : this
						});
                        
                        
                        
					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать один объект',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				} else {
					Ext.WgsMsg.show({
						title : 'Предупреждение',
						msg : 'Слоев не найдено',
						buttons : Ext.MessageBox.OK,
						icon : Ext.MessageBox.WARNING
					});
				}
			},
			scope : this
		});
        
        
        var yearJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'value'
        }]);
		this.wgsConn.request({
			url : '/objectadmin/card/getlistyears',
			method : 'POST',
			params : {},
			success : function(response) {
				if (response.responseText != 'null') {
					// alert(cardJsonReader.read(response).records.length);
					if (yearJsonReader.read(response).records.length > 0) {
						var years = yearJsonReader.read(response).records;
						// alert(response.responseText);
		
						this.window.addPanel();
						this.window.show();
						this.panel.cardComboboxYear.clearValue();

						this.panel.yearStore.removeAll();
						this.panel.yearStore.add(years);
						this.panel.cardComboboxYear.setValue(years[0].data.id);

					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать один объект',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				} else {
					Ext.WgsMsg.show({
						title : 'Предупреждение',
						msg : 'Слоев не найдено',
						buttons : Ext.MessageBox.OK,
						icon : Ext.MessageBox.WARNING
					});
				}
			},
			scope : this
		});
  
	
		this.wgsConn.request({
			url : '/objectadmin/card/getlistreports',
			method : 'POST',
			params : {},
			success : function(response) {
				if (response.responseText != 'null') {
					// alert(cardJsonReader.read(response).records.length);
					if (reportJsonReader.read(response).records.length > 0) {
						var layers = reportJsonReader.read(response).records;
						// this.open();
						this.window.addPanel();
						this.window.show();
						this.panel.cardReportCombobox.clearValue();
						// this.panel.cardComboboxYear.clearValue();
						var objectId = layers[0].data.id;
						this.panel.reportStore.removeAll();
						this.panel.reportStore.add(layers);
						this.panel.cardReportCombobox
								.setValue(layers[0].data.report_name);
                        this.reportId = layers[0].data.id;
                       // alert("this.reportId: "+this.reportId);
                        this.addAttrCardPanel(layers[0].data.id, this.layerId);                                

					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'Необходимо выбрать один объект',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				} else {
					Ext.WgsMsg.show({
						title : 'Предупреждение',
						msg : 'Слоев не найдено',
						buttons : Ext.MessageBox.OK,
						icon : Ext.MessageBox.WARNING
					});
				}
			},
			scope : this
		});
	},

	addAttrCardPanel : function(objectId, layerId) {
		this.attrCard = new Map.CardNew.AttrCardFormPanel(this.panel, objectId, layerId,
				this.mapViewer, this);
                //OBJECT_ID = REPORT TYPE
		this.attrCard.setObjectId(objectId);
        
        // this.attrCard.cardGeometryCombobox.on('select',this.cardComboboxOnGeometrySelect, this);
		this.panel.attrCardPanel.add(this.attrCard);
		this.panel.doLayout();
	},

	removeAttrCardPanel : function() {
		if (this.attrCard != null) {
			this.panel.attrCardPanel.remove(this.attrCard);
			this.panel.doLayout();
			this.attrCard = null;
		}
	},

	cardComboboxOnSelect : function(combo, record, index) {
		// alert("combo: "+combo+" record: "+ record+" index: "+index);
		this.panel.cardCombobox.setValue(record.data.cardLabel);
		// this.panel.cardComboboxYear.setValue(record.data.cardLabel);
		var objectId = record.get('cardId');
		this.removeAttrCardPanel();
		this.addAttrCardPanel(objectId);
		/*
		 * this.wgsConn.request({ url: '/objectadmin/card/getentityidbyobject',
		 * method: 'POST', params: {id: objectId}, success: function(response) {
		 * if (response.responseText) { if (response.responseText == '0') {
		 * this.panel.featuredocButton.hide(); } else {
		 * this.panel.featuredocButton.show(); } } }, scope: this });
		 */
		this.loadValueBaseProperties(objectId, this.modeCard);
	},

	cardComboboxOnYearSelect : function(combo, record, index) {
		this.panel.cardComboboxYear.setValue(record.data.id);
		/*
		 * var objectId = record.get('cardId'); this.removeAttrCardPanel();
		 * this.addAttrCardPanel(objectId);
		 * 
		 * this.wgsConn.request({ url: '/objectadmin/card/getentityidbyobject',
		 * method: 'POST', params: {id: objectId}, success: function(response) {
		 * if (response.responseText) { if (response.responseText == '0') {
		 * this.panel.featuredocButton.hide(); } else {
		 * this.panel.featuredocButton.show(); } } }, scope: this });
		 */
		// this.loadValueBaseProperties(objectId, this.modeCard);
	},
	cardComboboxOnLayerSelect : function(combo, record, index) {
        //alert("layer: "+record.data.featureLayerId);
        this.layerId = record.data.featureLayerId;
        this.wgsConnNew = new Ext.data.WgsConnection();
             var yearJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'value'
        }]);
        var geometryJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'name'
        }, {
            name : 'object_type_id'
        }, {
            name : 'featurelayer_id'
        }]);
        
           var attrJsonReader = new Ext.data.JsonReader({}, [{
            name : 'object_type_id'
        }, {
            name : 'name'
        }]);
        var groupPropJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'group_name'
        }]);
         var propJsonReader = new Ext.data.JsonReader({}, [{
            name : 'property_id'
        }, {
            name : 'property_name'
        }]);
        this.wgsConnNew.request({
                            url : '/objectadmin/card/getlistgeometrybylayer',
                            method : 'POST',
                            params : {
                                layerId : record.data.featureLayerId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (geometryJsonReader.read(response).records.length > 0) {
                                       
                                        var geom = geometryJsonReader.read(response).records;                                                             
                        this.attrCard.baseParamAttrCardPanelGisto.geometryCombobox.clearValue();
                        this.attrCard.baseParamAttrCardPanelGisto.geometryStore.removeAll();
                        this.attrCard.baseParamAttrCardPanelGisto.geometryStore.add(geom);
                        this.attrCard.baseParamAttrCardPanelGisto.geometryCombobox.setValue(geom[0].data.name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
         this.wgsConnNew.request({
                            url : '/objectadmin/card/getListAttrByLayer',
                            method : 'POST',
                            params : {
                                layerId : record.data.featureLayerId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (attrJsonReader.read(response).records.length > 0) {
                                       
                                        var attrs = attrJsonReader.read(response).records;
                        this.attrCard.baseParamAttrCardPanelGisto.AttrCombobox.clearValue();
                        this.attrCard.baseParamAttrCardPanelGisto.attrStore.removeAll();
                        this.attrCard.baseParamAttrCardPanelGisto.attrStore.add(attrs);
                        this.attrCard.baseParamAttrCardPanelGisto.AttrCombobox.setValue(attrs[0].data.name);
                /*        alert("attrs[0].data.object_type_id: "+ attrs[0].data.object_type_id+
                        "  attrs[0].data.name: " + attrs[0].data.name );*/
                        this.attrId = attrs[0].data.object_type_id;
                        
                            
                                       this.wgsConnNew.request({
                            url : '/objectadmin/card/getlistgroupbyattr',
                            method : 'POST',
                            params : {
                                attrId : this.attrId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (groupPropJsonReader.read(response).records.length > 0) {
                                       
                                        var groupProp = groupPropJsonReader.read(response).records;
                        this.attrCard.baseParamAttrCardPanelGisto.groupPropertyCombobox.clearValue();
                        this.attrCard.baseParamAttrCardPanelGisto.groupPropStore.removeAll();
                        this.attrCard.baseParamAttrCardPanelGisto.groupPropStore.add(groupProp);
                        this.attrCard.baseParamAttrCardPanelGisto.groupPropertyCombobox.setValue(groupProp[0].data.group_name);

                        this.groupId =groupProp[0].data.id;
                       // alert("this.groupId : "+this.groupId);
                                         this.wgsConnNew.request({
                            url : '/objectadmin/card/GetListPropByGroup',
                            method : 'POST',
                            params : {
                                groupId : this.groupId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (propJsonReader.read(response).records.length > 0) {
                                       
                                        var prop = propJsonReader.read(response).records;
                        this.attrCard.baseParamAttrCardPanelGisto.propertyCombobox.clearValue();
                        this.attrCard.baseParamAttrCardPanelGisto.propStore.removeAll();
                        this.attrCard.baseParamAttrCardPanelGisto.propStore.add(prop);
                        this.attrCard.baseParamAttrCardPanelGisto.propertyCombobox.setValue(prop[0].data.property_name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
                        
                        
                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
     /*   var geometryJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'name'
        }, {
            name : 'object_type_id'
        }, {
            name : 'featurelayer_id'
        }]);
		this.panel.cardLayerCombobox.setValue(record.data.legend);
            this.wgsConn.request({
                            url : '/objectadmin/card/getlistgeometrybylayer',
                            method : 'POST',
                            params : {
                                layerId : record.data.featureLayerId
                            },
                                      
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (geometryJsonReader.read(response).records.length > 0) {
                                        var geom = geometryJsonReader
                                                .read(response).records;

                                        this.window.addPanel();
                                        this.window.show();
                                        this.panel.cardGeometryCombobox
                                                .clearValue();

                                        this.panel.geometryStore.removeAll();
                                        this.panel.geometryStore.add(geom);
                                        this.panel.cardGeometryCombobox
                                                .setValue(geom[0].data.name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });*/
                        
                        
                        

	},
	cardComboboxOnReportSelect : function(combo, record, index) {
		this.panel.cardReportCombobox.setValue(record.data.report_name);
        this.removeAttrCardPanel();
        this.addAttrCardPanel(record.data.id);    
        this.reportId = record.data.id;
       // alert("this.reportId: "+this.reportId);

	},
    groupPropComboboxOnSelect : function(combo, record, index) {
        this.panel.groupPropCombobox.setValue(record.data.report_name);
        // var objectId = record.get('featureLayerId');
        // this.removeAttrCardPanel();
        // this.addAttrCardPanel(objectId);
        /*
         * this.wgsConn.request({ url: '/objectadmin/card/getentityidbyobject',
         * method: 'POST', params: {}, success: function(response) { if
         * (response.responseText) { if (response.responseText == '0') {
         * this.panel.featuredocButton.hide(); } else {
         * this.panel.featuredocButton.show(); } } }, scope: this });
         */
        // this.loadValueBaseProperties(objectId, this.modeCard);
    },
    
	cardComboboxOnGeometrySelect : function(combo, record, index) {
        //alert("GEOMETRY NAME: "+record.data.name);
		this.panel.cardGeometryCombobox.setValue(record.data.name);

	},
    
	reloadCardsStore : function() {
		var url = '';
		var params = new Object();
		var cardJsonReader = new Ext.data.JsonReader({}, [{
			name : 'cardId'
		}, {
			name : 'cardName'
		}, {
			name : 'cardIsCurrent'
		}, {
			name : 'cardTypeName'
		}, {
			name : 'cardRelationName'
		}, {
			name : 'cardLabel'
		}]);

		if (this.featureId && !this.objectId) {
			url = '/objectadmin/card/getcardsbyfeatureid', params = {
				'featureId' : this.featureId
			}
		}
		if (!this.featureId && this.objectId) {
			url = '/objectadmin/card/getcardsbyobjectid', params = {
				'objectId' : this.objectId
			}
		}
		var currentCardValue = this.panel.cardCombobox.getValue();

		this.wgsConn.request({
			url : url,
			method : 'POST',
			params : params,
			success : function(response) {
				if (response.responseText != 'null') {
					if (cardJsonReader.read(response).records.length > 0) {
						var cards = cardJsonReader.read(response).records;
						this.panel.cardCombobox.clearValue();
						// this.panel.cardComboboxYear.clearValue();
						var objectId = cards[0].data.cardId;
						this.panel.cardsStore.removeAll();
						this.panel.cardsStore.add(cards);
						this.panel.cardCombobox.setValue(currentCardValue);
						// this.panel.cardComboboxYear.setValue(currentCardValue);
					}
				}
			},
			scope : this
		});
	},


	saveCard : function() {

		var objectId = this.panel.cardCombobox.getValue();

		this.attrCard.saveCard({
			callback : this.cardCallback,
			scope : this.cardScope
		});

		if (this.modeCard) {

			this.panel.cardsStore.each(function(record) {
				if (record.get('cardId') == objectId) {
					if (this.attrCard.getForm().getValues().name != '')
						record.set('cardName', this.attrCard.getForm()
								.getValues().name
								+ ' - ' + record.get('cardTypeName'));
					else
						record.set('cardName', 'Без наименования - '
								+ record.get('cardTypeName'));
					record.commit();
					this.panel.cardCombobox.setValue(objectId);
					// this.panel.cardComboboxYear.setValue(objectId);
				}
			}, this);
		}

		else {

			if (this.attrCard.getForm().getValues().name != '') {

				this.panel.cardCombobox.setValue(this.attrCard.getForm()
						.getValues().name
						+ ' - ' + this.attrCard.getForm().getValues().typeName);
				// this.panel.cardComboboxYear.setValue(this.attrCard.getForm().getValues().name+'
				// - '+this.attrCard.getForm().getValues().typeName);
			} else
				this.panel.cardCombobox.setValue('Без наименования - '
						+ this.attrCard.getForm().getValues().typeName);
			// this.panel.cardComboboxYear.setValue('Без наименования -
			// '+this.attrCard.getForm().getValues().typeName);
		}

	},

	loadValueBaseProperties : function(objectId, mode) {
		// var msgWait = Ext.Msg.wait('Запрос характеристик...', 'Подождите',
		// {interval:50});
		this.objectIdForPrint = objectId;
		this.wgsConn.request({
			url : '/objectadmin/card/getvaluebaseproperties',
			method : 'POST',
			params : {
				'objectId' : objectId
			},
			success : function(response) {
				var record = this.panel.basePropertiesReader.read(response).records[0];
				this.attrCard.getForm().loadRecord(record);
				if (!this.modeCard) {
					if (record.data.name != '') {

						this.panel.cardCombobox.setValue(record.data.name
								+ ' - ' + record.data.typeName);
						this.panel.cardComboboxNew.setValue(record.data.name
								+ ' - ' + record.data.typeName);
					} else {
						this.panel.cardCombobox.setValue('Без наименования - '
								+ record.data.typeName);
						this.panel.cardComboboxNew
								.setValue('Без наименования - '
										+ record.data.typeName);
					}
				}
				// this.panel.enable();
				// this.loadMask.hide();
				this.attrCard.setReadOnlyCreateDateField();
				this.panel.doLayout();
				// msgWait.hide();
			},
			scope : this
		});
	},

	onCancel : function() {
		this.panel.hide();
	},

	printCard : function() {
		// alert("1"+self);
		// alert("2"+self.object_id);
		// alert("3"+self.relation_id);
		// alert("4"+self.mapViewer);
		// alert("5"+this.objectIdForPrint);
		// alert(this.panel.BaseParamAttrCardPanel);
		var myY;
		var record;
		var msgWait = Ext.Msg.wait('Загрузка формы...', 'Подождите', {
			interval : 50
		});
		this.wgsConn.request({
			url : '/objectadmin/card/getvaluebaseproperties',
			method : 'POST',
			params : {
				'objectId' : this.objectIdForPrint
			},
			success : function(response) {
				record = this.panel.basePropertiesReader.read(response).records[0];

				self.choiceAttrTypeRelationWindowNew = new Map.CardNew.ChoiceAttrTypeRelationWindowNew(
						self, self.object_id, self.relation_id, self.mapViewer,
						this.objectIdForPrint, record.data.shortName, this,
						this.wgsConn);
				self.choiceAttrTypeRelationWindowNew.show();
				msgWait.hide();
			},
			scope : this
		});
		// var msgWait = Ext.Msg.wait('Запрос характеристик...', 'Подождите',
		// {interval:50});

		// this.print();
	},
        printCard2 : function() {
        //alert("value in cb: "+ this.attrCard.baseParamAttrCardPanelGisto.geometryCombobox.getValue());    
        
        var regionId=null;
        var attrId = null;
        var groupPropId = null;
        var propId = null;
        var yearId = null;

         for(var i= 0 ; i< this.attrCard.baseParamAttrCardPanelGisto.geometryStore.getCount() ;i++){
            if(this.attrCard.baseParamAttrCardPanelGisto.geometryStore.getAt(i).data.name==
             this.attrCard.baseParamAttrCardPanelGisto.geometryCombobox.getValue())
             {
                 regionId =
                 this.attrCard.baseParamAttrCardPanelGisto.geometryStore.getAt(i).data.id;
                 break;
             }
         }
         if(regionId ==null){
          regionId =this.attrCard.baseParamAttrCardPanelGisto.geometryCombobox.getValue() ;
         }
         
         
            for(var i= 0 ; i< this.attrCard.baseParamAttrCardPanelGisto.attrStore.getCount() ;i++){
            if(this.attrCard.baseParamAttrCardPanelGisto.attrStore.getAt(i).data.name==
             this.attrCard.baseParamAttrCardPanelGisto.AttrCombobox.getValue())
             {
                 attrId =
                 this.attrCard.baseParamAttrCardPanelGisto.attrStore.getAt(i).data.object_type_id;
                 break;
             }
         }
         if(attrId ==null){
          attrId =this.attrCard.baseParamAttrCardPanelGisto.AttrCombobox.getValue() ;
         }   
         
            for(var i= 0 ; i< this.attrCard.baseParamAttrCardPanelGisto.groupPropStore.getCount() ;i++){
            if(this.attrCard.baseParamAttrCardPanelGisto.groupPropStore.getAt(i).data.group_name==
             this.attrCard.baseParamAttrCardPanelGisto.groupPropertyCombobox.getValue())
             {
                 groupPropId =
                 this.attrCard.baseParamAttrCardPanelGisto.groupPropStore.getAt(i).data.id;
                 break;
             }
         }
         if(groupPropId ==null){
          groupPropId =this.attrCard.baseParamAttrCardPanelGisto.groupPropertyCombobox.getValue() ;
         }
                 
         
            for(var i= 0 ; i< this.attrCard.baseParamAttrCardPanelGisto.propStore.getCount() ;i++){
            if(this.attrCard.baseParamAttrCardPanelGisto.propStore.getAt(i).data.property_name==
             this.attrCard.baseParamAttrCardPanelGisto.propertyCombobox.getValue())
             {
                 propId =
                 this.attrCard.baseParamAttrCardPanelGisto.propStore.getAt(i).data.property_id;
                 break;
             }
         }
         if(propId ==null){
          propId =this.attrCard.baseParamAttrCardPanelGisto.propertyCombobox.getValue() ;
         }
         
         
         
         
            for(var i= 0 ; i< this.attrCard.baseParamAttrCardPanelGisto.yearStore.getCount() ;i++){
            if(this.attrCard.baseParamAttrCardPanelGisto.yearStore.getAt(i).data.value==
             this.attrCard.baseParamAttrCardPanelGisto.yearCombobox.getValue())
             {
                 yearId =
                 this.attrCard.baseParamAttrCardPanelGisto.yearStore.getAt(i).data.id;
                 break;
             }
         }
         if(yearId ==null){
          yearId =this.attrCard.baseParamAttrCardPanelGisto.yearCombobox.getValue() ;
         }
         
       
         
         
         /*
         
             this.attrStore = new Ext.data.WgsJsonStore({
            fields : ['object_type_id', 'name']
        });
           this.groupPropStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'group_name']
        });
          this.propStore = new Ext.data.WgsJsonStore({
            fields : ['property_id', 'property_name']
        });
         this.yearStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'value']
        });
        
        AttrCombobox
        groupPropertyCombobox
        propertyCombobox
        yearCombobox
      
         */
         

         
         alert("reportId: "+this.reportId+" layerId: "+this.layerId);
         alert("geometryId: "+ regionId+" attrId: "+attrId+ " groupPropId: "+groupPropId + " propId: "+
         propId+" yearId: "+yearId);
         
         //this.attrCard.baseParamAttrCardPanelGisto.geometryStore[0].data.name)
this.wgsConn = new Ext.data.WgsConnection();
            var msgWait = Ext.Msg.wait('Генерация PDF-документа...',
                    'Подождите', {
                        interval : 50
                    });
            this.wgsConn.request({
                url : '/objectadmin/card/printGraph',
                defaultHeaders : {
                    "Content-Type" : "application/x-www-form-urlencoded"
                },
                method : 'POST',
                 params : {
                    reportId : this.reportId  , 
                    layerId : this.layerId,
                    objectId : regionId,
                    attrId : attrId,
                    groupPropId : groupPropId, 
                    propId : propId,
                    yearId : yearId
                },
                success : function(response) {

                    if (response.responseText) {
                        // alert("RESPTEXT: "+response.responseText);
                        this.docWindow = new Ext.WgsShimWindow({
                            title : 'Печать',
                            closable : true,
                            width : 550,
                            height : 600,
                            minWidth : 320,
                            minHeight : 240,
                            border : false,
                            resizable : true,
                            maximizable : true,
                            layout : 'fit',
                            plain : true,
                            closeAction : 'hide'
                        });
                        this.pdfPanel = new Ext.Panel({
                            html : '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'
                                    + response.responseText
                                    + '"></param><EMBED src="/objectadmin/card/getpdf/'
                                    + response.responseText
                                    + '" width="100%" height="100%" href="/objectadmin/card/getpdf/'
                                    + response.responseText
                                    + '" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
                        });
                        this.docWindow.add(this.pdfPanel);
                        this.docWindow.show();

                    }

                   // self.close();
                    msgWait.hide();
                }
            });


        
    },

	openFeatureDoc : function() {
		var url = '', id = 0;

		if (this.entityId) {
			Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(
					this.entityId, [this.entityId]);
		} else {
			if (this.featureId) {
				url = '/objectadmin/card/getentityidbyfeature';
				id = this.featureId;
			} else {
				if (this.objectId) {
					url = '/objectadmin/card/getentityidbyobject';
					id = this.objectId;
				}
			}

			this.wgsConn.request({
				url : url,
				method : 'POST',
				params : {
					id : id
				},
				success : function(response) {
					if (response.responseText) {
						if (response.responseText != '0') {
							this.entityId = response.responseText;
							Map.Featuredoc.Instance(this.mapViewer)
									.openFeaturedoc(this.entityId,
											[this.entityId]);
						}
					}
				},
				scope : this
			});
		}
	},

	print : function() {
		var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...',
				'Подождите', {
					interval : 80
				});
		this.wgsConn.request({
			url : '/objectadmin/card/print',
			defaultHeaders : {
				"Content-Type" : "application/x-www-form-urlencoded"
			},
			method : 'POST',
			params : {
				objectId : this.objectIdForPrint
			},
			success : function(response) {
				msgWait.hide();
				if (response.responseText) {
					// alert(response.responseText);
					this.openPdf(response.responseText);
				}
			},
			scope : this
		});
	},
	printNew : function() {
		var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...',
				'Подождите', {
					interval : 80
				});
		this.wgsConn.request({
			url : '/objectadmin/card/print',
			defaultHeaders : {
				"Content-Type" : "application/x-www-form-urlencoded"
			},
			method : 'POST',
			params : {
				objectId : this.objectIdForPrint
			},
			success : function(response) {
				msgWait.hide();
				if (response.responseText) {
					// alert(response.responseText);
					this.openPdfNew(response.responseText);
				}
			},
			scope : this
		});
	},

	openPdf : function(pdfSeq) { // alert("pdfSeq: "+pdfSeq);
		this.docWindow = new Ext.WgsShimWindow({
			title : 'Печать',
			closable : true,
			width : 550,
			height : 600,
			minWidth : 320,
			minHeight : 240,
			border : false,
			resizable : true,
			maximizable : true,
			layout : 'fit',
			plain : true,
			closeAction : 'hide'
		});
		this.pdfPanel = new Ext.Panel({
			html : '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '"></param><EMBED src="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '" width="100%" height="100%" href="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
		});
		this.docWindow.add(this.pdfPanel);
		this.docWindow.show();
	},
	openPdfNew : function(pdfSeq) { // alert("pdfSeq: "+pdfSeq);
		this.docWindow = new Ext.WgsShimWindow({
			title : 'Печать',
			closable : true,
			width : 550,
			height : 600,
			minWidth : 320,
			minHeight : 240,
			border : false,
			resizable : true,
			maximizable : true,
			layout : 'fit',
			plain : true,
			closeAction : 'hide'
		});
		this.pdfPanel = new Ext.Panel({
			html : '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '"></param><EMBED src="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '" width="100%" height="100%" href="/objectadmin/card/getpdf/'
					+ pdfSeq
					+ '" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
		});
		this.docWindow.add(this.pdfPanel);
		this.docWindow.show();
	}
}

Map.CardNew.AdvancedParamRelationPanel = function(owner, objectRelationId) {
	this.owner = owner;
	this.objectRelationId = objectRelationId;

	var self = this;

	this.storeJS = new Ext.data.JsonStore({
		autoLoad : true,
		fields : ['name', 'value', 'vulueTypeId', 'groupId', 'groupName', 'id'],
		url : '/objectadmin/card/GetValuePropertiesByObjRel',
		baseParams : {
			objectRelationId : self.objectRelationId
		}
	});

	self.setObjectRelationId = function(_ObjectRelationId) {
		self.objectRelationId = _ObjectRelationId;
		self.storeJS.baseParams.objectRelationId = _ObjectRelationId;
		self.storeJS.reload();
	};

	Map.CardNew.AdvancedParamRelationPanel.superclass.constructor.call(this, {
		title : 'Характеристики связи',
		autoHeight : true,
		autoScroll : true,
		closable : false,
		defaultType : 'textfield',
		items : [{
			name : 'objectRelationId',
			hidden : true,
			labelSeparator : ''
		}]
	});
}
Ext.extend(Map.CardNew.AdvancedParamRelationPanel, Ext.form.FieldSet, {
	initComponent : function() {
		var self = this;
		self.storeJS.on('load', function() {
			self.domainStores = [];
			var group_id = -1;
			var groupFieldSet;
			self.storeJS.each(function(record) {
				if (group_id != record.data.groupId) {
					if (groupFieldSet) {
						self.add(groupFieldSet);
					}
					groupFieldSet = new Ext.form.FieldSet({
						title : 'Группа характеристик: '
								+ record.data.groupName,
						autoHeight : true,
						collapsible : true,
						closable : false,
						anchor : '97%',
						collapsed : true
					});
					if (group_id == -1) {
						groupFieldSet.collapsed = false
					}
					group_id = record.data.groupId;
				}
				if (record.data.vulueTypeId == 1) {
					var propertyEditor = new Ext.form.TextField({
						fieldLabel : record.data.name,
						name : 'propRel-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 2) {
					var propertyEditor = new Ext.form.DateField({
						fieldLabel : record.data.name,
						name : 'propRel-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						altFormats : 'd.m.y|d.m.Y|',
						format : 'd.m.Y'
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 3) {
					var propertyEditor = new Ext.form.NumberField({
						fieldLabel : record.data.name,
						name : 'propRel-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						allowDecimals : false,
						maxText : 10
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 4) {
					var propertyEditor = new Ext.form.NumberField({
						fieldLabel : record.data.name,
						name : 'propRel-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						allowDecimals : true,
						maxText : 10,
						decimalSeparator : ','
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 5) {
					self.domainStores[self.domainStores.length] = new Ext.data.JsonStore({
						// autoLoad: true,
						fields : ['id', 'name'],
						url : '/objectadmin/card/GetValueDomainList',
						baseParams : {
							propId : record.data.id
						}
					});
					self.domainStores[self.domainStores.length - 1].load({
						callback : function() {
							propertyEditor.setValue(record.data.value);
							propertyHidden.setValue(propertyEditor.getValue());
						}
					});
					var propertyHidden = new Ext.form.TextField({
						name : 'propRel-' + record.data.id,
						hideLabel : true,
						hidden : true,
						labelSeparator : '',
						value : record.data.value
					});
					var propertyEditor = new Ext.form.ComboBox({
						fieldLabel : record.data.name,
						name : 'gridRel-' + record.data.id,
						store : self.domainStores[self.domainStores.length - 1],
						anchor : '97%',
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						loadingText : 'Список загружается...',
						triggerAction : 'all',
						emptyText : 'Выберите значение...',
						selectOnFocus : true
					});
					propertyEditor.on('select', function(combo, record, index) {
						propertyHidden.setValue(propertyEditor.getValue());
					});
					groupFieldSet.add(propertyEditor);
					groupFieldSet.add(propertyHidden);
				} else if (record.data.vulueTypeId == 7) {
					var propertyEditor = new Ext.form.TextArea({
						fieldLabel : record.data.name,
						name : 'propRel-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value
					});
					groupFieldSet.add(propertyEditor);
				}
			});
			if (groupFieldSet) {
				self.add(groupFieldSet);
				self.owner.doLayout();
			}
		}, this);

		Map.CardNew.AdvancedParamRelationPanel.superclass.initComponent.apply(
				this, arguments);
	}
});

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-ПАНЕЛЬ СВЯЗИ -*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.CardNew.RelationCardPanel = function(owner, relation_id, object_id,
		relationType, mapViewer) {

	this.owner = owner;
	this.relation_id = relation_id;
	this.object_id = object_id;
	this.relationType = relationType;
	this.mapViewer = mapViewer;
	var self = this;

	this.storeJS = new Ext.data.JsonStore({
		autoLoad : true,
		fields : ['id', 'name', 'shortName', 'typeId', 'typeName', 'invNumber',
				'createDate', 'note', 'author', 'label', 'ObjTypeRelationId',
				'objRelationId'],
		url : '/objectadmin/card/GetListChildObjectByRelation',
		baseParams : {
			objectId : self.object_id,
			relationId : self.relation_id
		}
	});

	this.newButton = new Ext.Button({
		text : 'Создать',
		handler : function() {
			this.fireEvent('NewChildCard')
		},
		scope : this
	});
	this.bindButton = new Ext.Button({
		text : 'Найти и привязать',
		handler : function() {
			this.fireEvent('BindChildCard')
		},
		scope : this
	});

	this.unBindButton = new Ext.Button({
		text : 'Отвязать',
		handler : function() {
			this.fireEvent('UnBindChildCard')
		},
		scope : this
	});
	this.removeButton = new Ext.Button({
		text : 'Удалить',
		handler : function() {
			this.fireEvent('RemoveChildCard')
		},
		scope : this
	});

	this.editButton = new Ext.Button({
		text : 'Свойства',
		handler : function() {
			this.fireEvent('EditChildCard', self.gridChildCard
					.getSelectionModel().getSelected())
		},
		scope : this
	});
	this.newButton.disable();
	this.bindButton.disable();
	this.unBindButton.disable();
	this.removeButton.disable();
	this.editButton.disable();
	Map.CardNew.RelationCardPanel.superclass.constructor.call(this, {
		autoScroll : true,
		tbar : [this.newButton, this.bindButton, this.unBindButton,
				this.removeButton, this.editButton],
		items : [

		self.gridChildCard = new Ext.grid.GridPanel({
			viewConfig : {
				// autoFill :false,
				forceFit : true
			},
			store : this.storeJS,
			// width: 520,
			height : 100,
			autoScroll : true,
			columns : [{
				id : 'id',
				dataIndex : 'id',
				hideable : false,
				hidden : true
			}, {
				id : 'name',
				header : "Полное наименование",
				sortable : true,
				dataIndex : 'name'
			}, {
				id : 'shortName',
				header : "Отчетный год",
				dataIndex : 'shortName'
			}, {
				id : 'typeId',
				dataIndex : 'typeId',
				hideable : false,
				hidden : true
			}, {
				id : 'typeName',
				header : "Наименованиe описания",
				sortable : true,
				dataIndex : 'typeName'
			}, {
				id : 'invNumber',
				dataIndex : 'invNumber',
				hideable : false,
				hidden : true
			}, {
				id : 'createDate',
				dataIndex : 'createDate',
				hideable : false,
				hidden : true
			}, {
				id : 'note',
				dataIndex : 'note',
				hideable : false,
				hidden : true
			}, {
				id : 'author',
				dataIndex : 'author',
				hideable : false,
				hidden : true
			}, {
				id : 'label',
				dataIndex : 'label',
				hideable : false,
				hidden : true
			}, {
				id : 'ObjTypeRelationId',
				dataIndex : 'ObjTypeRelationId',
				hideable : false,
				hidden : true
			}, {
				id : 'objRelationId',
				dataIndex : 'objRelationId',
				hideable : false,
				hidden : true
			}]
		})]
	});
}
Ext.extend(Map.CardNew.RelationCardPanel, Ext.Panel, {
	initComponent : function() {
		this.addEvents({
			NewChildCard : true,
			UnBindChildCard : true,
			RemoveChildCard : true,
			EditChildCard : true
		});

		var self = this;
		this.storeJS.on('load', function() {
			var countRecord = self.storeJS.getCount();
			if (countRecord > 0) {
				self.gridChildCard.getSelectionModel().selectFirstRow();
			}
			if (self.relationType == 1) {
				if (countRecord == 1) {
					self.newButton.disable();
					self.bindButton.disable();
					self.unBindButton.enable();
					self.removeButton.enable();
					self.editButton.enable();
				} else {
					self.newButton.enable();
					self.bindButton.enable();
					self.unBindButton.disable();
					self.removeButton.disable();
					self.editButton.disable();
				}
			}
			if (self.relationType == 2) {
				if (countRecord > 0) {
					self.newButton.enable();
					self.bindButton.enable();
					self.unBindButton.enable();
					self.removeButton.enable();
					self.editButton.enable();
				} else {
					self.newButton.enable();
					self.bindButton.enable();
					self.unBindButton.disable();
					self.removeButton.disable();
					self.editButton.disable();
				}
			}
		});

		this.on('NewChildCard', function() {
			this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
				fields : ['id', 'name', 'objTypeRelationId'],
				url : '/objectadmin/card/GetListChildObjectTypeByRelation'
			});
			this.featureTypeByRelationStore.removeAll();
			this.featureTypeByRelationStore.baseParams = {/*
															 * objectId:
															 * this.object_id,
															 */
				relationId : this.relation_id
			};

			this.featureTypeByRelationStore.load({
				callback : function(records) {
					if (records.length > 0) {
						self.choiceAttrTypeRelationWindow = new Map.CardNew.ChoiceAttrTypeRelationWindow(
								self, self.object_id, self.relation_id,
								self.mapViewer);
						self.choiceAttrTypeRelationWindow.show();
					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'К данному объекту карточки всех связанных типов уже привязаны',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				},
				scope : this
			});
		});

		this.on('PRINT_PDF', function() {
			this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
				fields : ['id', 'name', 'objTypeRelationId'],
				url : '/objectadmin/card/GetListChildObjectTypeByRelation'
			});
			this.featureTypeByRelationStore.removeAll();
			this.featureTypeByRelationStore.baseParams = {/*
															 * objectId:
															 * this.object_id,
															 */
				relationId : this.relation_id
			};

			this.featureTypeByRelationStore.load({
				callback : function(records) {
					if (records.length > 0) {
						self.choiceAttrTypeRelationWindow = new Map.CardNew.ChoiceYearWindow(
								self, self.object_id, self.relation_id,
								self.mapViewer);
						self.choiceAttrTypeRelationWindow.show();
					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'К данному объекту карточки всех связанных типов уже привязаны',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				},
				scope : this
			});
		});

		this.on('BindChildCard', function() {
			this.featureTypeByRelationStore = new Ext.data.WgsJsonStore({
				fields : ['id', 'name', 'objTypeRelationId'],
				url : '/objectadmin/card/GetListChildObjectTypeByRelation'
			});
			this.featureTypeByRelationStore.removeAll();
			this.featureTypeByRelationStore.baseParams = {/*
															 * objectId:
															 * this.object_id,
															 */
				relationId : this.relation_id
			};
			this.featureTypeByRelationStore.load({
				callback : function(records) {
					if (records.length > 0) {
						var featureTypeFilter = [];
						Ext.each(records, function(record) {
							featureTypeFilter.push(record.data.id);
						});
						var bindSearch = Map.Bind.Search
								.Instance(this.mapViewer);
						bindSearch.open({
							object : this.object_id,
							featureTypeFilter : featureTypeFilter,
							mode : Map.Bind.Search.OBJECTBIND_MODE,
							callback : function(objectId1, objectId2,
									featureTypeId) {
								var objTypeRelationId;
								this.featureTypeByRelationStore.findBy(
										function(record) {
											if (record.data.id == featureTypeId) {
												objTypeRelationId = record.data.objTypeRelationId;
												return;
											}
										}, this);
								new Ext.data.WgsConnection().request({
									url : '/objectadmin/card/BindChildObject',
									method : 'POST',
									params : {
										parentObjectId : objectId2,
										childObjectId : objectId1,
										objTypeRelationId : objTypeRelationId
									},
									success : function(action) {
										bindSearch.panel.resultTabPanel.resultStore
												.removeAll();
										bindSearch.container.hide();
										this.storeJS.removeAll();
										this.storeJS.load();
										this.owner.owner.reloadCardStore();
									},
									scope : this
								});
							},
							scope : this
						});
					} else {
						Ext.WgsMsg.show({
							title : 'Предупреждение',
							msg : 'К данному объекту карточки всех связанных типов уже привязаны',
							buttons : Ext.MessageBox.OK,
							icon : Ext.MessageBox.WARNING
						});
					}
				},
				scope : this
			}, this);
		}, this);

		this.on('UnBindChildCard', function(record) {
			Ext.WgsMsg.show({
				title : 'Подтверждение',
				msg : 'Вы уверены, что хотите отвязать выбранные карточки?',
				buttons : Ext.Msg.YESNO,
				fn : function(buttonId) {
					if (buttonId == 'yes') {
						var selectedRelations = self.gridChildCard
								.getSelectionModel().getSelections();
						if (selectedRelations.length != 0) {
							var relationsForRemove = [];
							for (var i = 0; i < selectedRelations.length; i++) {
								relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
							}
							var msgWait = Ext.Msg.wait('Отвязка объектов...',
									'Подождите', {
										interval : 50
									});
							new Ext.data.WgsConnection().request({
								url : '/objectadmin/card/RemoveObjectRelation',
								method : 'POST',
								params : {
									removeMode : 1,
									'relationsId[]' : relationsForRemove
								},
								success : function() {
									self.storeJS.reload();
									self.owner.owner.reloadCardStore();
									msgWait.hide();
								}
							});
						}

					}
				},
				icon : Ext.MessageBox.QUESTION,
				scope : this
			});
		});

		this.on('RemoveChildCard', function(record) {
			Ext.WgsMsg.show({
				title : 'Подтверждение',
				msg : 'Вы уверены, что хотите удалить выбранные карточки?',
				buttons : Ext.Msg.YESNO,
				fn : function(buttonId) {
					if (buttonId == 'yes') {
						var selectedRelations = self.gridChildCard
								.getSelectionModel().getSelections();
						if (selectedRelations.length != 0) {
							var relationsForRemove = [];
							for (var i = 0; i < selectedRelations.length; i++) {
								relationsForRemove[relationsForRemove.length] = selectedRelations[i].data.objRelationId;
							}
							var msgWait = Ext.Msg.wait('Удаление объектов...',
									'Подождите', {
										interval : 50
									});
							new Ext.data.WgsConnection().request({
								url : '/objectadmin/card/RemoveObjectRelation',
								method : 'POST',
								params : {
									removeMode : 0,
									'relationsId[]' : relationsForRemove
								},
								success : function() {
									self.storeJS.reload();
									self.owner.owner.reloadCardStore();
									msgWait.hide();
								}
							});
						}
					}
				},
				icon : Ext.MessageBox.QUESTION,
				scope : this
			});
		});

		this.on('EditChildCard', function(record) {
			Map.CardNew.Create(this.mapViewer).open({
				objectId : record.data.id,
				callback : function() {
					this.storeJS.reload();
					this.owner.owner.reloadCardStore();
				},
				scope : this
			});
		});
		this.owner.owner.controlReadOnly();
		Map.CardNew.RelationCardPanel.superclass.initComponent.apply(this,
				arguments);
	}
});

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
 //*-*-*-*-*-*-*-*-*-ОКНО ВЫБОРА АТРИБУТИВНОГО ТИПА ДЛЯ СВЯЗИ-*-*-*-*-*-*-*-*-*-*-*-
 //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
Map.CardNew.ChoiceAttrTypeRelationWindow = function(owner, parentObjectId,
		relation_id, mapViewer) {
	this.owner = owner;
	this.relation_id = relation_id;
	this.parentObjectId = parentObjectId;
	this.mapViewer = mapViewer;
	var self = this;

	this.storeJS = new Ext.data.WgsJsonStore({
		autoLoad : true,
		fields : ['id', 'name', 'objTypeRelationId'],
		url : '/objectadmin/card/GetListChildObjectTypeByRelation',
		baseParams : {/* objectId: parentObjectId, */
			relationId : self.relation_id
		}
	});

	this.saveButton = new Ext.Button({
		text : 'Создать',
		handler : function() {
			this.fireEvent('save')
		},
		scope : this
	});
	this.cancelButton = new Ext.Button({
		text : 'Отмена',
		handler : function() {
			this.fireEvent('cancel')
		},
		scope : this
	});
	self.saveButton.disable();

	this.attrTypeComboBox = new Ext.form.ComboBox({
		anchor : '100%',
		hideLabel : true,
		labelSeparator : '',
		store : this.storeJS,
		displayField : 'name',
		valueField : 'id',
		loadingText : 'Список загружается...',
		triggerAction : 'all',
		emptyText : 'Выберите описание...',
		allowBlank : true,
		editable : false
	});

	Map.CardNew.ChoiceAttrTypeRelationWindow.superclass.constructor.call(this,
			{
				title : 'Выбор атрибутивного описания',
				layout : 'fit',
				height : 180,
				width : 375,
				modal : true,
				autoScrol : true,
				resizable : false,
				items : [new Ext.form.FormPanel({
					frame : true,
					bodyStyle : 'padding:5px 5px 0',
					items : [new Ext.Panel({
						frame : true,
						height : 60,
						layout : 'fit',
						style : 'margin-bottom: 5px',
						html : '<p>Если вы уверены, что хотите создать атрибутивный объект и '
								+ 'связать его с данным, выберите атрибутивный тип и нажмите "Создать", '
								+ 'иначе "Отмена"</p>'
					}), this.attrTypeComboBox]
				})],
				buttons : [this.saveButton, this.cancelButton]
			});
}

Map.CardNew.ChoiceAttrTypeRelationWindowNew = function(owner, parentObjectId,
		relation_id, mapViewer, objectIdForPrint, year, lookedObject, connector) {
	this.owner = owner;
	this.relation_id = relation_id;
	this.parentObjectId = parentObjectId;
	this.mapViewer = mapViewer;
	var self = this;
	this.objectIdForPrint = objectIdForPrint;
	var myYear = year;
	var loockedObj = lookedObject;
	this.connector = connector;

this.saveButton = new Ext.Button({
		text : 'Сформировать',
		handler : function() {
			this.fireEvent('save')
		},
		scope : this
	});
	this.cancelButton = new Ext.Button({
		text : 'Отмена',
		handler : function() {
			this.fireEvent('cancel')
		},
		scope : this
	});
	// self.saveButton.disable();

	this.attrTypeComboBox = new Ext.form.TextField({
		id : "from",
		fieldLabel : "Отчетный год",
		allowBlank : false,
		maxLength : 4,
		minLength : 4,
		value : myYear
	})

;

	Map.CardNew.ChoiceAttrTypeRelationWindowNew.superclass.constructor.call(
			this, {
				title : 'Выбор отчетного года',
				layout : 'fit',
				height : 120,
				width : 300,
				modal : true,
				autoScrol : true,
				resizable : false,
				items : [new Ext.form.FormPanel({
					frame : true,
					bodyStyle : 'padding:5px 5px 0',
					items : [

					this.attrTypeComboBox]
				})],
				buttons : [this.saveButton, this.cancelButton]
			});
}
Ext.extend(Map.CardNew.ChoiceAttrTypeRelationWindowNew, Ext.Window, {
	initComponent : function() {
		this.addEvents({
			save : true,
			cancel : true
		});

		var self = this;

		self.attrTypeComboBox.on('select', function() {
			self.saveButton.enable();
		});

		this.on('save', function() {

			var msgWait = Ext.Msg.wait('Генерация PDF-документа...',
					'Подождите', {
						interval : 50
					});
			self.connector.request({
				url : '/objectadmin/card/printNew',
				defaultHeaders : {
					"Content-Type" : "application/x-www-form-urlencoded"
				},
				method : 'POST',
				params : {
					objectId : this.objectIdForPrint,
					year : self.attrTypeComboBox.getValue()
				},
				success : function(response) {

					if (response.responseText) {
						// alert("RESPTEXT: "+response.responseText);
						this.docWindow = new Ext.WgsShimWindow({
							title : 'Печать',
							closable : true,
							width : 550,
							height : 600,
							minWidth : 320,
							minHeight : 240,
							border : false,
							resizable : true,
							maximizable : true,
							layout : 'fit',
							plain : true,
							closeAction : 'hide'
						});
						this.pdfPanel = new Ext.Panel({
							html : '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/objectadmin/card/getpdf/'
									+ response.responseText
									+ '"></param><EMBED src="/objectadmin/card/getpdf/'
									+ response.responseText
									+ '" width="100%" height="100%" href="/objectadmin/card/getpdf/'
									+ response.responseText
									+ '" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
						});
						this.docWindow.add(this.pdfPanel);
						this.docWindow.show();

					}

					self.close();
					msgWait.hide();
				}
			});


		});
		this.on('cancel', function() {
			self.close();
		});

		Map.CardNew.ChoiceAttrTypeRelationWindowNew.superclass.initComponent
				.apply(this, arguments);
	}
});

Ext.extend(Map.CardNew.ChoiceAttrTypeRelationWindow, Ext.Window, {
	initComponent : function() {
		this.addEvents({
			save : true,
			cancel : true
		});

		var self = this;

		self.attrTypeComboBox.on('select', function() {
			self.saveButton.enable();
		});

		this.on('save', function() {

			var msgWait = Ext.Msg.wait('Создание карточки...', 'Подождите', {
				interval : 50
			});
			new Ext.data.WgsConnection().request({
				url : '/objectadmin/card/CreateChildObject',
				method : 'POST',
				params : {
					objectTypeId : self.attrTypeComboBox.getValue(),
					parentObjectId : self.parentObjectId,
					objTypeRelationId : self.storeJS.getAt(self.storeJS.find(
							'id', self.attrTypeComboBox.getValue())).data.objTypeRelationId
				},
				success : function(action) {
					var response = Ext.decode(action.responseText);
					if (response) {
						Map.CardNew.Create(this.mapViewer).open({
							objectId : response.childObjectId,
							callback : function() {
								self.owner.storeJS.reload();
								self.owner.owner.owner.reloadCardStore();
							},
							scope : this
						});
					}
					msgWait.hide();
					self.close();
				}
			});
		});
		this.on('cancel', function() {
			self.close();
		});

		Map.CardNew.ChoiceAttrTypeRelationWindow.superclass.initComponent
				.apply(this, arguments);
	}
});

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-ПАНЕЛЬ ДОЧЕРНИХ КАРТОЧЕК ДЛЯ ДАННОЙ КАРТОЧКИ-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.CardNew.CildCardAttrCardPanel = function(owner, object_id, mapViewer) {
	this.owner = owner;
	this.object_id = object_id;
	this.mapViewer = mapViewer;
	// this.loadMask = loadMask;
	var self = this;

	self.setObjectId = function(_Object_id) {
		self.object_id = _Object_id;
		self.storeJS.baseParams.objectId = _Object_id;
		self.storeJS.reload();
	};

	this.storeJS = new Ext.data.JsonStore({
		autoLoad : true,
		fields : ['relationId', 'name', 'relationTypeId', 'countTypeRelation'],
		url : '/objectadmin/card/GetListRelByObject',
		baseParams : {
			objectId : self.object_id
		}
	});

	Map.CardNew.CildCardAttrCardPanel.superclass.constructor.call(this, {
		autoScroll : true,
		closable : false
	});
}
Ext.extend(Map.CardNew.CildCardAttrCardPanel, Ext.Panel, {
	initComponent : function() {
		var self = this;
		self.storeJS.on('load', function() {
			self.storeJS.each(function(record) {
				this.relationFieldSet = new Ext.form.FieldSet({
					title : 'Связь: ' + record.data.name,
					height : 170,
					collapsible : true,
					layout : 'fit',
					anchor : '100%',
					items : [this.relationCardPanel = new Map.CardNew.RelationCardPanel(
							self, record.data.relationId, self.object_id,
							record.data.relationTypeId, self.mapViewer)]
				});
				self.add(this.relationFieldSet);
			}, this);
			self.owner.controlReadOnly();
			self.owner.doLayout();
		}, this);
		Map.CardNew.CildCardAttrCardPanel.superclass.initComponent.apply(this,
				arguments);

	}
});
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-ПАНЕЛЬ ДОПОЛНИТЕЛЬНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Map.CardNew.AdvancedParamAttrCardPanel = function(owner, object_id) {
	this.owner = owner;
	this.object_id = object_id;

	var self = this;

	this.storeJS = new Ext.data.JsonStore({
		autoLoad : true,
		fields : ['name', 'value', 'vulueTypeId', 'groupId', 'groupName', 'id'],
		url : '/objectadmin/card/GetValueProperties',
		baseParams : {
			objectId : self.object_id
		}
	});

	self.setObjectId = function(_Object_id) {
		self.object_id = _Object_id;
		self.storeJS.baseParams.objectId = _Object_id;
		self.storeJS.reload();
	};

	Map.CardNew.AdvancedParamAttrCardPanel.superclass.constructor.call(this, {
		closable : false
	});
}
Ext.extend(Map.CardNew.AdvancedParamAttrCardPanel, Ext.Panel, {
	initComponent : function() {
		var self = this;
		self.storeJS.on('load', function() {
			self.domainStores = [];
			var group_id = -1;
			var groupFieldSet;
			self.storeJS.each(function(record) {
				if (group_id != record.data.groupId) {
					if (groupFieldSet) {
						self.add(groupFieldSet);
					}
					var groupId = record.data.groupId;

					var cExpanded = WGS.Cookie.get('propGroupExpanded') || "", expandedGroups = cExpanded.length > 0
							? (cExpanded.indexOf(",") != -1 ? cExpanded
									.split(",") : [cExpanded])
							: [];

					var expDate = new Date(), days = 180;

					expDate.setTime(expDate.getTime()
							+ (days * 24 * 60 * 60 * 1000));

					groupFieldSet = new Ext.form.FieldSet({
						title : 'Группа характеристик: '
								+ record.data.groupName,
						autoHeight : true,
						collapsible : true,
						closable : false,
						anchor : '97%',
						collapsed : !expandedGroups.inArray(groupId)
					});
					groupFieldSet.on('collapse', function() {
						var cookieExpanded = WGS.Cookie
								.get('propGroupExpanded')
								|| "", expanded = cookieExpanded.length > 0
								? (cookieExpanded.indexOf(",") != -1
										? cookieExpanded.split(",")
										: [cookieExpanded])
								: [], cleanExpanded = [];
						for (var i = 0; i < expanded.length; i++) {
							if (expanded[i] != groupId) {
								cleanExpanded.push(expanded[i]);
							}
						}
						WGS.Cookie.set('propGroupExpanded', cleanExpanded,
								expDate);
					});
					groupFieldSet.on('expand', function() {
						var cookieExpanded = WGS.Cookie
								.get('propGroupExpanded')
								|| "", expanded = cookieExpanded.length > 0
								? (cookieExpanded.indexOf(",") != -1
										? cookieExpanded.split(",")
										: [cookieExpanded])
								: [];
						expanded.push(groupId);
						WGS.Cookie.set('propGroupExpanded', expanded, expDate);
					});
					groupFieldSet.labelWidth = 300;
					if (group_id == -1 && !expandedGroups.length) {
						groupFieldSet.collapsed = false
					}
					group_id = record.data.groupId;
				}
				if (record.data.vulueTypeId == 1) {
					var propertyEditor = new Ext.form.TextField({
						fieldLabel : record.data.name,
						name : 'property-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 2) {

					var propertyEditor = new Ext.form.DateField({
						fieldLabel : record.data.name,
						name : 'property-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						altFormats : 'd.m.y|d.m.Y|',
						format : 'd.m.Y'
					});

					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 3) {
					var propertyEditor = new Ext.form.NumberField({
						fieldLabel : record.data.name,
						name : 'property-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						allowDecimals : false,
						maxText : 10
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 4) {
					var propertyEditor = new Ext.form.NumberField({
						fieldLabel : record.data.name,
						name : 'property-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value,
						allowDecimals : true,
						maxText : 10,
						decimalSeparator : ','
					});
					groupFieldSet.add(propertyEditor);
				} else if (record.data.vulueTypeId == 5) {
					self.domainStores[self.domainStores.length] = new Ext.data.JsonStore({
						fields : ['id', 'name'],
						url : '/objectadmin/card/GetValueDomainList',
						baseParams : {
							propId : record.data.id
						}
					});
					self.domainStores[self.domainStores.length - 1].load({
						callback : function() {
							propertyEditor.setValue(record.data.value);
							propertyHidden.setValue(propertyEditor.getValue());
						}
					});
					var propertyHidden = new Ext.form.TextField({
						name : 'property-' + record.data.id,
						hideLabel : true,
						hidden : true,
						labelSeparator : '',
						value : record.data.value
					});
					var propertyEditor = new Ext.form.WgsComboBox({
						fieldLabel : record.data.name,
						name : 'grid-' + record.data.id,
						store : self.domainStores[self.domainStores.length - 1],
						anchor : '97%',
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						loadingText : 'Список загружается...',
						triggerAction : 'all',
						emptyText : 'Выберите значение...',
						selectOnFocus : true
					});
					propertyEditor.on('select', function(combo, record, index) {
						// alert(propertyEditor.getValue());
						propertyHidden.setValue(propertyEditor.getValue());
					});


					groupFieldSet.add(propertyEditor);
					groupFieldSet.add(propertyHidden);
				} else if (record.data.vulueTypeId == 7) {
					var propertyEditor = new Ext.form.TextArea({
						fieldLabel : record.data.name,
						name : 'property-' + record.data.id,
						anchor : '97%',
						allowBlank : true,
						value : record.data.value
					});
					groupFieldSet.add(propertyEditor);
				}
			});

			if (groupFieldSet) {
				self.add(groupFieldSet);
				self.owner.doLayout();
			}
			self.owner.controlReadOnly();
		}, this);

		Map.CardNew.AdvancedParamAttrCardPanel.superclass.initComponent.apply(
				this, arguments);
	}
});

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-ПАНЕЛЬ ОСНОВНЫХ ХАРАКТЕРИСТИК КАРТОЧКИ-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.CardNew.BaseParamAttrCardPanel = function(owner, object_id) {
	this.owner = owner;
	this.object_id = object_id;

	var self = this;

	this.objectIdHidden = new Ext.form.Hidden({
		name : 'id',
		hidden : true,
		labelSeparator : ''
	});

	self.setObjectId = function(_Object_id) {
		self.object_id = _Object_id;
		this.objectIdHidden.setValue(_Object_id);
		owner.getForm().setValues({
			id : _Object_id
		});
	};

	Map.CardNew.BaseParamAttrCardPanel.superclass.constructor.call(this, {
		title : 'Условия выборки',
		layout : 'form',
		autoHeight : true,
		//autoWidth : true,
		defaultType : 'textfield',
		closable : false,
		items : [
            /*{
			fieldLabel : '',
			name : 'name',
			anchor : '97%',
			allowBlank : true
		}, 
        */
        
        new Ext.form.WgsComboBox({
           // anchor : '100%',
            //width : 150,
            //labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Исследуемая характеристика',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
        {
        title : 'Временной интервал',
        layout : 'column',
          items: [{
        title: 'Column 1',
        width: 200

        
    },{
        title: 'Column 2',
        columnWidth: .25
    },{
        title: 'Column 3',
        columnWidth: .25
    },
    {
        title: 'Column 4',
        columnWidth: .25
    },
    {
        title: 'Column 5',
        columnWidth: .25
    }
    ]
        }
        ,
           new Ext.form.Label({
        fieldLabel : 'Начальное значение года'
        }),
          new Ext.form.WgsComboBox({
           // anchor : '100%',
            hideLabel : false,
            fieldLabel : 'Начальное значение года',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
         new Ext.form.Label({
        fieldLabel : 'Конечное значение года'
        })
        ,  new Ext.form.WgsComboBox({
            anchor : '100%',
            hideLabel : false,
            fieldLabel : 'Конечное значение года',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        }) ,
                {
			fieldLabel : 'Начальное значение характеристики',
			name : 'shortName',
			anchor : '97%',
			allowBlank : true
		},
             {
            fieldLabel : 'Конечное значение характеристики',
            name : 'shortName',
            anchor : '97%',
            allowBlank : true
        },
        this.objectIdHidden]
	});
}
Ext.extend(Map.CardNew.BaseParamAttrCardPanel, Ext.form.FieldSet, {});




Map.CardNew.BaseParamAttrCardPanelSelect = function(owner, object_id) {
    this.owner = owner;
    this.object_id = object_id;

    var self = this;

    this.objectIdHidden = new Ext.form.Hidden({
        name : 'id',
        hidden : true,
        labelSeparator : ''
    });

    self.setObjectId = function(_Object_id) {
        self.object_id = _Object_id;
        this.objectIdHidden.setValue(_Object_id);
        owner.getForm().setValues({
            id : _Object_id
        });
    };

    Map.CardNew.BaseParamAttrCardPanelSelect.superclass.constructor.call(this, {
        title : 'Параметры пересечения',
        layout : 'form',
        autoHeight : true,
       // height: 300,
        //autoWidth : true,
        defaultType : 'textfield',
        closable : false,
        items : [
            /*{
            fieldLabel : '',
            name : 'name',
            anchor : '97%',
            allowBlank : true
        }, 
        */
                new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Год',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
        new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Атрибутивное описание 1',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
          new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Группа характеристик 1',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
        new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Исследуемая характеристика 1',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
             new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Атрибутивное описание 2',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
          new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Группа характеристик 2',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
        new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Исследуемая характеристика 2',
            labelSeparator : '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local'
        })  ,
this.objectIdHidden]
    });
}
Ext.extend(Map.CardNew.BaseParamAttrCardPanelSelect, Ext.form.FieldSet, {});


Map.CardNew.BaseParamAttrCardPanelGisto = function(owner, object_id,layerId) {
    this.owner = owner;
    this.object_id = object_id;
    this.layerId = layerId;
   // alert("layerID in BaseParamAttrCardPanelGisto conts" +  this.layerId);
    var self = this;
    
    this.objectIdHidden = new Ext.form.Hidden({
        name : 'id',
        hidden : true,
        labelSeparator : ''
    });

    self.setObjectId = function(_Object_id) {
        self.object_id = _Object_id;
        this.objectIdHidden.setValue(_Object_id);
        owner.getForm().setValues({
            id : _Object_id
        });
    };

       
        this.geometryStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'name', 'object_type_id', 'featurelayer_id']
        });
        
        
        this.layerStore = new Ext.data.WgsJsonStore({
            fields : ['featureLayerId', 'legend']
        });
        this.attrStore = new Ext.data.WgsJsonStore({
            fields : ['object_type_id', 'name']
        });
           this.groupPropStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'group_name']
        });
          this.propStore = new Ext.data.WgsJsonStore({
            fields : ['property_id', 'property_name']
        });
         this.yearStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'value']
        });
       
         
        this.geometryCombobox=
               new Ext.form.WgsComboBox({
            anchor : '100%',
            width : 300,
            labelWidth : 300,
            hideLabel : false,
            fieldLabel : 'Объект',
            labelSeparator : '',
            displayField : 'name',
            valueField : 'id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local',
            store : this.geometryStore
        })
         this.geometryCombobox.on('select', function(combo, record, index) {
            
            //alert("ololo!!!!!!! "+record.data.id+" "+record.data.name);
            
                        //propertyHidden.setValue(propertyEditor.getValue());
                    });
        this.AttrCombobox = 
           new Ext.form.WgsComboBox({
            anchor : '100%',
            width : 300,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Атрибутивное описание',
            labelSeparator : '',
            displayField : 'name',
            valueField : 'object_type_id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local',
            store : this.attrStore
        }) ;
        
         this.AttrCombobox.on('select', function(combo, record, index) {
            var groupPropJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'group_name'
        }]);
         var propJsonReader = new Ext.data.JsonReader({}, [{
            name : 'property_id'
        }, {
            name : 'property_name'
        }]);
            
           /* alert("ololo2!!!!!!! "+record.data.object_type_id+" "+record.data.name);
            alert("this: "+ this);*/
            this.wgsConnNew = new Ext.data.WgsConnection();
                   this.wgsConnNew.request({
                            url : '/objectadmin/card/getlistgroupbyattr',
                            method : 'POST',
                            params : {
                                attrId : record.data.object_type_id
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (groupPropJsonReader.read(response).records.length > 0) {
                                       
                                        var groupProp = groupPropJsonReader.read(response).records;
                        this.groupPropertyCombobox.clearValue();
                        this.groupPropStore.removeAll();
                        this.groupPropStore.add(groupProp);
                        this.groupPropertyCombobox.setValue(groupProp[0].data.group_name);

                        this.groupId =groupProp[0].data.id;
                      /*  alert("this.groupId : "+this.groupId);*/
                                         this.wgsConnNew.request({
                            url : '/objectadmin/card/GetListPropByGroup',
                            method : 'POST',
                            params : {
                                groupId : this.groupId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (propJsonReader.read(response).records.length > 0) {
                                       
                                        var prop = propJsonReader.read(response).records;
                        this.propertyCombobox.clearValue();
                        this.propStore.removeAll();
                        this.propStore.add(prop);
                        this.propertyCombobox.setValue(prop[0].data.property_name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
                        
                        
                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        //propertyHidden.setValue(propertyEditor.getValue());
                    },this);
        
        this.groupPropertyCombobox = 
         new Ext.form.WgsComboBox({
            anchor : '100%',
            width : 300,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Группа характеристик',
            labelSeparator : '',
            displayField : 'group_name',
            valueField : 'id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local',
            store : this.groupPropStore
        }) ;
        
        
         this.groupPropertyCombobox.on('select', function(combo, record, index) {
            var groupPropJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'group_name'
        }]);
         var propJsonReader = new Ext.data.JsonReader({}, [{
            name : 'property_id'
        }, {
            name : 'property_name'
        }]);
            
           /* alert("ololo2!!!!!!! "+record.data.object_type_id+" "+record.data.name);
            alert("this: "+ this);*/
            this.wgsConnNew = new Ext.data.WgsConnection();
                           this.wgsConnNew.request({
                            url : '/objectadmin/card/GetListPropByGroup',
                            method : 'POST',
                            params : {
                                groupId : record.data.id
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (propJsonReader.read(response).records.length > 0) {
                                       
                                        var prop = propJsonReader.read(response).records;
                        this.propertyCombobox.clearValue();
                        this.propStore.removeAll();
                        this.propStore.add(prop);
                        this.propertyCombobox.setValue(prop[0].data.property_name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        //propertyHidden.setValue(propertyEditor.getValue());
                    },this);
        
        
            this.propertyCombobox = 
        new Ext.form.WgsComboBox({
            anchor : '100%',
            width : 300,
            labelWidth : 500,
            hideLabel : false,
            fieldLabel : 'Характеристика',
            labelSeparator : '',
            displayField : 'property_name',
            valueField : 'property_id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local',
            store : this.propStore
        }) ;
        
        this.yearCombobox =    new Ext.form.WgsComboBox({
            //anchor : '100%',
            width : 150,
            labelWidth : 100,
            hideLabel : false,
            fieldLabel : 'Год',
            labelSeparator : '',
            displayField : 'value',
            valueField : 'id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,            
            mode : 'local',
            store : this.yearStore
        }) ;
     

    
    Map.CardNew.BaseParamAttrCardPanelGisto.superclass.constructor.call(this, {
        title : 'Параметры гистограммы',
        layout : 'form',
        autoHeight : true,
        autoWidth : true,
        defaultType : 'textfield',
        closable : false,
        items : [
        this.geometryCombobox,
        this.AttrCombobox,
        this.groupPropertyCombobox,
        this.propertyCombobox,
        this.yearCombobox,
        this.objectIdHidden]
    });
    
    
}
Ext.extend(Map.CardNew.BaseParamAttrCardPanelGisto, Ext.form.FieldSet, {});

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// *-*-*-*-*-ОСНОВНАЯ ПАНЕЛЬ КАРТОЧКИ (АТРИБУТИВНЫХ ОБЪЕКТОВ)-*-*-*-*-*-*-*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
Map.CardNew.AttrCardFormPanel = Ext.extend(Ext.form.WgsFormPanel, {
	constructor : function(owner, object_id,layerId, mapViewer, card) {
		this.owner = owner;
		this.object_id = object_id;
       // alert("ObjectId is reportType: "+ this.object_id);
       // alert("!!!!!!!!!!!!!layerId: "+layerId);
		var self = this;
        this.layerId  = layerId;
        this.attrId = 0;
         this.groupId =0;
        this.wgsConnNew = new Ext.data.WgsConnection();
            
		        self.baseParamAttrCardPanel = new Map.CardNew.BaseParamAttrCardPanel(
				self, self.object_id);
                
                self.baseParamAttrCardPanelGisto = new Map.CardNew.BaseParamAttrCardPanelGisto(
                self, self.object_id,self.layerId);
                
                
                self.baseParamAttrCardPanelSelect = new Map.CardNew.BaseParamAttrCardPanelSelect(
                self, self.object_id);

               
                
                
                
                
                
          var yearJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'value'
        }]);
        var geometryJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'name'
        }, {
            name : 'object_type_id'
        }, {
            name : 'featurelayer_id'
        }]);
        
           var attrJsonReader = new Ext.data.JsonReader({}, [{
            name : 'object_type_id'
        }, {
            name : 'name'
        }]);
        var groupPropJsonReader = new Ext.data.JsonReader({}, [{
            name : 'id'
        }, {
            name : 'group_name'
        }]);
         var propJsonReader = new Ext.data.JsonReader({}, [{
            name : 'property_id'
        }, {
            name : 'property_name'
        }]);
        this.wgsConnNew.request({
                            url : '/objectadmin/card/getlistgeometrybylayer',
                            method : 'POST',
                            params : {
                                layerId : self.layerId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (geometryJsonReader.read(response).records.length > 0) {
                                       
                                        var geom = geometryJsonReader.read(response).records;                                                             
                        self.baseParamAttrCardPanelGisto.geometryCombobox.clearValue();
                        self.baseParamAttrCardPanelGisto.geometryStore.removeAll();
                        self.baseParamAttrCardPanelGisto.geometryStore.add(geom);
                        self.baseParamAttrCardPanelGisto.geometryCombobox.setValue(geom[0].data.name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
         this.wgsConnNew.request({
                            url : '/objectadmin/card/getListAttrByLayer',
                            method : 'POST',
                            params : {
                                layerId : self.layerId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (attrJsonReader.read(response).records.length > 0) {
                                       
                                        var attrs = attrJsonReader.read(response).records;
                        self.baseParamAttrCardPanelGisto.AttrCombobox.clearValue();
                        self.baseParamAttrCardPanelGisto.attrStore.removeAll();
                        self.baseParamAttrCardPanelGisto.attrStore.add(attrs);
                        self.baseParamAttrCardPanelGisto.AttrCombobox.setValue(attrs[0].data.name);
                     /*   alert("attrs[0].data.object_type_id: "+ attrs[0].data.object_type_id+
                        "  attrs[0].data.name: " + attrs[0].data.name );*/
                        this.attrId = attrs[0].data.object_type_id;
                        
                            
                                       this.wgsConnNew.request({
                            url : '/objectadmin/card/getlistgroupbyattr',
                            method : 'POST',
                            params : {
                                attrId : this.attrId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (groupPropJsonReader.read(response).records.length > 0) {
                                       
                                        var groupProp = groupPropJsonReader.read(response).records;
                        self.baseParamAttrCardPanelGisto.groupPropertyCombobox.clearValue();
                        self.baseParamAttrCardPanelGisto.groupPropStore.removeAll();
                        self.baseParamAttrCardPanelGisto.groupPropStore.add(groupProp);
                        self.baseParamAttrCardPanelGisto.groupPropertyCombobox.setValue(groupProp[0].data.group_name);

                        this.groupId =groupProp[0].data.id;
                        //alert("this.groupId : "+this.groupId);
                                         this.wgsConnNew.request({
                            url : '/objectadmin/card/GetListPropByGroup',
                            method : 'POST',
                            params : {
                                groupId : this.groupId
                            },
                            success : function(response) {
                                // alert("objectId: "+objectId);
                                if (response.responseText != 'null') {
                                    // alert(cardJsonReader.read(response).records.length);
                                    if (propJsonReader.read(response).records.length > 0) {
                                       
                                        var prop = propJsonReader.read(response).records;
                        self.baseParamAttrCardPanelGisto.propertyCombobox.clearValue();
                        self.baseParamAttrCardPanelGisto.propStore.removeAll();
                        self.baseParamAttrCardPanelGisto.propStore.add(prop);
                        self.baseParamAttrCardPanelGisto.propertyCombobox.setValue(prop[0].data.property_name);

                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
                        
                        
                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                                    } else {
                                        Ext.WgsMsg.show({
                                            title : 'Предупреждение',
                                            msg : 'Необходимо выбрать один объект',
                                            buttons : Ext.MessageBox.OK,
                                            icon : Ext.MessageBox.WARNING
                                        });
                                    }
                                } else {
                                    Ext.WgsMsg.show({
                                        title : 'Предупреждение',
                                        msg : 'Слоев не найдено',
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.WARNING
                                    });
                                }
                            },
                            scope : this
                        });
                        
        this.wgsConnNew.request({
            url : '/objectadmin/card/getlistyears',
            method : 'POST',
            params : {},
            success : function(response) {
                if (response.responseText != 'null') {
                    // alert(cardJsonReader.read(response).records.length);
                    if (yearJsonReader.read(response).records.length > 0) {
                        var years = yearJsonReader.read(response).records;

                        self.baseParamAttrCardPanelGisto.yearCombobox.clearValue();

                        self.baseParamAttrCardPanelGisto.yearStore.removeAll();
                        self.baseParamAttrCardPanelGisto.yearStore.add(years);

                        self.baseParamAttrCardPanelGisto.yearCombobox.setValue(years[0].data.value);


                    } else {
                        Ext.WgsMsg.show({
                            title : 'Предупреждение',
                            msg : 'Ошибка сервера. Обратитесь к администратору',
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.WARNING
                        });
                    }
                } else {
                    Ext.WgsMsg.show({
                        title : 'Предупреждение',
                        msg : 'Слоев не найдено',
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.WARNING
                    });
                }
            },
            scope : this
        });       
                // this.baseParamAttrCardPanelGisto.
                //geometryCombobox.on('select', this.geometryComboboxOnSelect,this);
                
              
       
		self.setObjectId = function(_Object_id) {
			self.baseParamAttrCardPanel.setObjectId(_Object_id);

		};

        
		self.saveCard = function(config) {
			var msgWait = Ext.Msg.wait('Идет сохранение параметров...',
					'Подождите', {
						interval : 50
					});
			if (object_id != null) {
				self.url = '/objectadmin/card/SetCard';
			} else {
				self.url = '/objectadmin/card/AddCard';
			}
			self.submit({
				success : function(form, action) {

					if (self.owner) {
						if (self.owner.owner) {
							self.owner.owner.storeJS.reload();
						}
					}

					msgWait.hide();
					if (config) {
						config.callback.call(config.scope);
					}
				}
			})
		};

		self.reloadCardStore = function() {
			card.reloadCardsStore();
		};

		self.controlReadOnly = function() {
			if (!WGS.PRIVILEGE.CARD_EDIT)
				self.setReadOnly(true, false);
		};

		self.setReadonlyPrivate = function(f, bReadOnly) {
			if (typeof f._readOnly == 'undefined') {
				if (f.initEvents)
					f.initEvents = f.initEvents
							.createSequence(self.initEventsSequence)
			}
			f._readOnly = bReadOnly;
			f.getEl().dom.readOnly = bReadOnly;
			if (f instanceof Ext.form.TriggerField) {
				if (bReadOnly) {
					f.trigger.un('click', f.onTriggerClick, f);
					if (f.keyNav)
						f.keyNav.disable();
				} else {
					f.trigger.on('click', f.onTriggerClick, f, {
						preventDefault : true
					});
					if (f.keyNav)
						f.keyNav.enable();
				}
			}
		};

		self.initEventsSequence = function() {
			if (this._readOnly == true) {
				if (this.keyNav)
					this.keyNav.disable();
			} else {
				if (this.keyNav)
					this.keyNav.enable();
			}
		};

		self.setReadOnly = function(readOnly, buttonVisible) {
			self.getForm().items.each(function(f) {
				self.setReadonlyPrivate(f, readOnly);
			});
			if (self.advancedParamAttrCardPanel)
				if (self.advancedParamAttrCardPanel.items)
					self.advancedParamAttrCardPanel.items.each(function(
							fieldset) {
						if (fieldset.items)
							fieldset.items.each(function(field) {
								self.setReadonlyPrivate(field, readOnly);
							});
					});
			if (self.cildCardAttrCardPanel)
				if (self.cildCardAttrCardPanel.items)
					self.cildCardAttrCardPanel.items.each(function(fieldset) {
						if (fieldset.items)
							;
						fieldset.items.each(function(field) {
							if (readOnly)
								if (field.getTopToolbar()) {
									var toolbar = field.getTopToolbar();
									if (Ext.isArray(toolbar)) {
										toolbar[0].setVisible(false);
										toolbar[1].setVisible(false);
										toolbar[2].setVisible(false);
										toolbar[3].setVisible(false);
									}
								}
						});
					});
			owner.saveButton.setVisible(buttonVisible);
			// owner.cancelButton.setVisible(buttonVisible);
		};
        if(this.object_id ==1)
		Map.CardNew.AttrCardFormPanel.superclass.constructor.call(this, {
			frame : true,
			url : '/objectadmin/card/SetCard',
			layout : 'fit',
			//autoWidth : true,
			 //height: 500,
             autoHeight : true,
             
			closable : false,
			autoScroll : true,
			bodyStyle : 'padding:5px 5px 0',
			items : [self.baseParamAttrCardPanelGisto]
		});
         if(this.object_id ==2)
        Map.CardNew.AttrCardFormPanel.superclass.constructor.call(this, {
            frame : true,
            url : '/objectadmin/card/SetCard',
            layout : 'fit',
            //autoWidth : true,
           //  height: 550,
            autoHeight : true,
            closable : false,
            autoScroll : true,
            bodyStyle : 'padding:5px 5px 0',
            items : [self.baseParamAttrCardPanelSelect]
        });
          if(this.object_id ==3)
        Map.CardNew.AttrCardFormPanel.superclass.constructor.call(this, {
            frame : true,
            url : '/objectadmin/card/SetCard',
            layout : 'fit',
            //autoWidth : true,
             //height: 500,
             autoHeight : true,
            closable : false,
            autoScroll : true,
            bodyStyle : 'padding:5px 5px 0',
            items : [self.baseParamAttrCardPanel]
        });
	}
});
/*Map.CardNew.AttrCardFormPanel.prototype = {
        geometryComboboxOnSelect  : function(combo, record, index) {
        //
       alert("!!!!!!!!!!!_____"+ record.data.id + "____!!!!!!!!!");
    }   
};*/
 Ext.extend(Map.CardNew.AttrCardFormPanel,Ext.form.WgsFormPanel,{});

Map.CardNew.CardPanel = Ext.extend(Ext.WgsServicePanel, {
	constructor : function() {
		this.cardsStore = new Ext.data.WgsJsonStore({
			fields : ['cardId', 'cardName', 'cardIsCurrent', 'cardTypeName',
					'cardRelationName', 'cardLabel']
		});

		this.childCardsStore = new Ext.data.WgsJsonStore({
			url : '/objectadmin/card/getchildobjectsbyparent',
			fields : ['cardId', 'cardName', 'cardTypeName', 'cardRelationName']
		});
		this.yearStore = new Ext.data.WgsJsonStore({
			fields : ['id', 'value']
		});
        this.geometryStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'name', 'object_type_id', 'featurelayer_id']
        });
		this.layerStore = new Ext.data.WgsJsonStore({
			fields : ['featureLayerId', 'legend']
		});
		   this.groupPropStore = new Ext.data.WgsJsonStore({
            fields : ['id', 'group_name']
        });
		this.reportStore = new Ext.data.WgsJsonStore({
			fields : ['id', 'report_name']
		});
     
		this.cardCombobox = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'cardName',
			valueField : 'cardId',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.cardsStore,
			mode : 'local'
		});
        this.groupPropCombobox = new Ext.form.WgsComboBox({
            anchor : '100%',
            hideLabel : true,
            labelSeparator : '',
            displayField : 'group_name',
            valueField : 'id',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable : false,
            store : this.cardsStore,
            mode : 'local'
        });
		this.cardReportCombobox = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'report_name',
			valueField : 'id',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.reportStore,
			mode : 'local'
		});
		this.cardGeometryCombobox = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'name',
			valueField : 'object_id',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.geometryStore,
			mode : 'local'
		});
        
		this.cardComboboxYear = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'value',
			valueField : 'id',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.yearStore,
			mode : 'local'
		});
		this.cardLayerCombobox = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'legend',
			valueField : 'featureLayerId',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.layerStore,
			mode : 'local'
		});

		this.childCardCombobox = new Ext.form.WgsComboBox({
			anchor : '100%',
			hideLabel : true,
			labelSeparator : '',
			displayField : 'cardName',
			valueField : 'cardId',
			triggerAction : 'all',
			emptyText : 'Наименование',
			loadingText : 'Загрузка...',
			editable : false,
			store : this.childCardsStore,
			mode : 'local'
		});

		this.basePropertiesReader = new Ext.data.JsonReader({}, [{
			name : 'name'
		}, {
			name : 'shortName'
		}, {
			name : 'typeName'
		}, {
			name : 'createDate'
		}, {
			name : 'note'
		}]);
/*
		this.saveButton = new Ext.Button({
			text : 'Сохранить'
		});
*/
		this.cancelButton = new Ext.Button({
			text : 'Закрыть',
			scope : this
		});

		this.attrCardPanel = new Ext.Panel({
			region : 'south',
			height : 350,
			autoWidth : true,
			layout : 'fit'
		});

		this.listCardFieldSet = new Ext.form.FieldSet({
			title : 'Список характеристик',
			autoHeight : true,
			items : this.cardCombobox
		});
		this.listCardFieldReportSet = new Ext.form.FieldSet({
			title : 'Способ анализа',
			autoHeight : true,
			items : this.cardReportCombobox
		});
		this.listCardFieldYearSet = new Ext.form.FieldSet({
			title : 'Год',
			autoHeight : true,
			items : this.cardComboboxYear
		});
        this.listGroupPropFieldYearSet = new Ext.form.FieldSet({
            title : 'Группа характеристик',
            autoHeight : true,
            items : this.groupPropCombobox
        });
		this.listCardFieldLayerSet = new Ext.form.FieldSet({
			title : 'Слой',
			autoHeight : true,
			items : this.cardLayerCombobox
		});
		this.listCardFieldGeometrySet = new Ext.form.FieldSet({
			title : 'Объект',
			autoHeight : true,
			items : this.cardGeometryCombobox
		});
		this.listChildCardFieldSet = new Ext.form.FieldSet({
			title : 'Список привязанных описаний',
			autoHeight : true,
			hidden : true,
			items : this.childCardCombobox
		});

		Map.CardNew.CardPanel.superclass.constructor.call(this, {
			width : 600,
            height : 650,
			//autoHeight : true,
			layout : 'border',
			frame : true,
			maskDisabled : false,
			collapsible : false,
			items : [{
				region : 'center',
				autoHeight : true,
				layout : 'fit',
				items : [this.listCardFieldReportSet,
						this.listCardFieldLayerSet,
                        /*
						this.listCardFieldGeometrySet,
                        this.listGroupPropFieldYearSet,
                        this.listCardFieldSet,
						this.listCardFieldYearSet
                        */
                        ]
			}, this.attrCardPanel]
		});
		/*
		 * this.featuredocButton = this.addButton({ iconCls: 'featuredoc', text :
		 * 'Документы' });
		 */
        
		this.printButton = this.addButton({
			iconCls : 'icon-print',
			text : 'Анализ: год - районы'
		});
        this.printButton2 = this.addButton({
            iconCls : 'icon-print',
            text : 'Анализ: район - года'
        });
		/*
		 * this.printButton = new Ext.Button( { text : 'Создать', handler :
		 * function() { this.fireEvent('NewChildCard') }, scope : this });
		 */
        /*
		this.saveButton = this.addButton({
			iconCls : 'icon-accept-tick',
			text : 'Применить'
		});
*/
		this.cancelButton = this.addButton({
			iconCls : 'icon-close-cross',
			text : 'Закрыть',
			scope : this
		});
	}
});