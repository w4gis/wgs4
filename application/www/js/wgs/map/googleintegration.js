﻿Ext.namespace('Map');

Map.GoogleIntegration = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.conn = new Ext.data.WgsConnection();
}

/*PublishMap = function() {
    Map.GoogleIntegration.panel.show();      
    Map.GoogleIntegration.window.show();
    Map.GoogleIntegration.panel.hide();
    Map.GoogleIntegration.panel.show();
    Map.GoogleIntegration.window.show();
}

LoadGoogleEarth = function(){
    if (google) {
        google.load('maps', '2.x');
        google.load('earth', '1.x', {"callback" : PublishMap}); 
    } else {
        Ext.WgsMsg.show({
            title: 'Предупреждение',
            msg: 'Для работы модуля необходим доступ в интернет',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING
        });
    }   
}

InitGoogleLoader = function(){
    // ключ для http://gis.tusur.ru:8008/ 
        apiKey = 'ABQIAAAAImE7IhPR4Lw7a_CFyguPexS9Q3rvQPid6zpRP3RUDZT9o9o74hT_YQ7DgeYyz6TzHRmV6YxuPplLiA';
    var script = document.createElement("script");  
    script.src = "http://www.google.com/jsapi?key="+apiKey+"&callback=LoadGoogleEarth";  
    script.type = "text/javascript";  
    document.getElementsByTagName("body")[0].appendChild(script);
}    
*/    
Map.GoogleIntegration.prototype = {
    
    create: function()
    {
        this.panel = new GoogleIntegrationMainPanel({
            autoWidth: true,
            height: 700,
            mapViewer: this.mapViewer,
            conn: this.conn,
            border: false
        });
        
        this.window = new Ext.Window({
            layout: 'fit',
            resizable: true,
            width: 950+12,
            modal: true,
            maximizable : true,
            closeAction : 'hide',
            title: 'Публикация в Google Earth'
        });
		
		this.window.add(this.panel);
        Map.GoogleIntegration.window = this.window;
        Map.GoogleIntegration.panel = this.panel; 

        try {
            google = google? google: false;
        } catch (e) {
            google = false;
        }
		
		if (!google) {
			Ext.WgsMsg.show({
                        title: 'Предупреждение',
                        msg: 'Отсутствует доступ к серверу Google',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
             }); 
           // jQuery.getScript("http://www.google.com/jsapi?key=AIzaSyA7yGi5gaxGkadWohAUqDFAEEB5-_Xh-EE", function(){
                     
        } else {
			google.load('maps', '2.x', {callback: function(){
                google.load('earth', '1.x', {callback: function(){
                    Map.GoogleIntegration.panel.show();      
                    Map.GoogleIntegration.window.show();
                    Map.GoogleIntegration.panel.hide();
                    Map.GoogleIntegration.panel.show();
                    Map.GoogleIntegration.window.show();
                }});
            }});
                                   
        }
        this.created = true;
    },
    
    setentityid: function (entityId)
    {
        Map.GoogleIntegration.entityId = entityId;
    },
    
    open: function()
    {
        if (!this.created) 
        {
            this.create();
        } else {
            Map.GoogleIntegration.panel.show();      
            Map.GoogleIntegration.window.show();
            Map.GoogleIntegration.panel.hide();
            Map.GoogleIntegration.panel.show();
            Map.GoogleIntegration.window.show();
        }
    } 
}

Map.GoogleIntegration.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.GoogleIntegration(map);
    }    
    return this.instances[map.mapFrame];
}

GoogleIntegrationMainPanel = Ext.extend(Ext.Panel, {
    constructor: function(config){
        if (config){
            Ext.apply(this, config);
        }
        
        this.GoogleIntegrationPanel = new GoogleIntegrationPanel(this.conn);
    
        GoogleIntegrationMainPanel.superclass.constructor.call(this, {
            layout: 'fit',
            items: [this.GoogleIntegrationPanel]
            
        });
    }
});

GoogleIntegrationPanel = function(conn) {
    this.conn = conn;
    
    this.domain = window.location.href.split('map');
    
    this.earthPanel = new Ext.ux.GEarthPanel({
        layout: 'fit',
        region : 'center'
    });
    
    this.zoomColor = new Ext.form.TextField({
        readOnly: true,
        style: 'background: #FF6600; color: #FF6600'
    });
    
    this.colorPalette = new Ext.ColorPalette({
        value: 'FF6600',
        listeners:
        {
            select: function(palette, selColor)
            {
                if (this.layerTreePanel.layerTree.getSelectionModel().getSelectedNode()){
                    var node = this.layerTreePanel.layerTree.getSelectionModel().getSelectedNode();
                    if (node.attributes.layerColor != selColor && node.attributes.layer == true && node.attributes.checked == true) {
                        node.attributes.layerColor = selColor;
                        
                        if (node.attributes.kmlObject) {
                            this.earthPanel.removeKml(node.attributes.kmlObject);
                            node.attributes.kmlObject = false;
                        }
                        
                        LoadKML(this.conn, this.earthPanel, this.colorPalette, this.domain[0], node);                        
                    } 
                    if (node.attributes.layerColor != selColor && node.attributes.layer == true && node.attributes.checked != true){ 
                        node.attributes.layerColor = selColor;
                        if (node.attributes.kmlObject) {
                            this.earthPanel.removeKml(node.attributes.kmlObject);
                            node.attributes.kmlObject = false;
                        }
                    }
                }
                //this.zoomColor.setValue(selColor);
                this.zoomColor.getEl().applyStyles({background: '#'+selColor, color: '#'+selColor });
            },
            scope: this
        }
    });
    
    this.palettePanel = new Ext.Panel({
        region: 'west',
        border: false,
        height: 104,
        width: 157,
        frame: true,
        items: [ this.colorPalette ]
    });
    
    this.colorField = new Ext.Panel({
        region: 'center',
        layout: 'fit',
        border: false,
        height: 104,
        frame: true,
        bodyStyle:'padding:13px 8px 13px 8px',
        items: this.zoomColor
        
    });
    
    this.color = new Ext.Panel({
        layout: 'fit',
        region: 'south',
        border: false,
        height: 105,
        items: new Ext.Panel({
            layout: 'border',
            border: false,
            items: [this.palettePanel, this.colorField]
        })
        
    });
    
    this.layerTreePanel = new LayerTreePanel(this.conn, this.earthPanel, this.colorPalette, this.domain[0]);
    
    this.westPanel = new Ext.Panel({
        layout: 'border',
        region : 'west',
        split : true,
        collapsible : true,
        collapseMode: 'mini',
        width : 250,
        border: false,
        items: [this.layerTreePanel, this.color]
    });
    
    Map.GoogleIntegration.westPanel = this.westPanel;
    
    GoogleIntegrationPanel.superclass.constructor.call(this, {
        collapsible : false,
        margins : '0 0 0 0',
        layout : 'border',
        border: false,
        items: [this.westPanel, this.earthPanel]
    });
}
Ext.extend(GoogleIntegrationPanel, Ext.Panel, {});



/* LayerTreePanel */
LayerTreePanel = function(conn, earthPanel, colorPalette, domain) {
    
    this.conn = conn;
    this.domain = domain;
    this.earthPanel = earthPanel;
    this.colorPalette = colorPalette;
    
    this.layerTree = new Ext.tree.TreePanel({
        checkModel: 'cascade',
        onlyLeafCheckable: false,

        root : new Ext.tree.AsyncTreeNode({
            text : 'Слои',
            draggable : false, 
            id: 'root'
        }),
        
        loader: new Ext.tree.TreeLoader({
            dataUrl:'/map/googleintegration/getlayerlist',
            baseAttrs: { uiProvider: Ext.ux.TreeCheckNodeUI }
        }),
        rootVisible : false,
        animate : true,
        singleExpand : false,
        autoScroll : true,
        enableDD : false,
        border : false,
        containerScroll : true,
        dropConfig : {appendOnly : true}
    });
   
    LayerTreePanel.superclass.constructor.call(this, {
        border : false,
        frame: true,
        region : 'center',
        autoScroll : true,
        iconCls : 'nav',
        items : this.layerTree
    });
}
Ext.extend(LayerTreePanel, Ext.Panel, {
    initComponent : function() {
        
        this.layerTree.on('check', function(node,checked) {
            if (node.attributes.layer == true && checked == true) {
               node.select();
               this.colorPalette.select(node.attributes.layerColor);
               if (node.attributes.kmlObject) {
                    node.attributes.kmlObject.setVisibility(1); 
               } else {
                   LoadKML(this.conn, this.earthPanel, this.colorPalette, this.domain, node);
               }
            }
            if (node.attributes.layer == true && checked == false) {
                node.select();
                this.colorPalette.select(node.attributes.layerColor);
                if (node.attributes.kmlObject) node.attributes.kmlObject.setVisibility(0);
            }
        }, this);
        
        this.layerTree.on('click', function(node) {
            if (node.attributes.layer == true) {
                node.select();
                this.colorPalette.select(node.attributes.layerColor);
            } else { 
                node.select();
                this.colorPalette.select('000000');
            }
        }, this);
        
        LayerTreePanel.superclass.initComponent.apply(this, arguments);
    }
});


LoadKML = function(conn, earthPanel, colorPalette, domain, node) {
    
    this.conn = conn;
    this.domain = domain;
    this.earthPanel = earthPanel;
    this.colorPalette = colorPalette;
    this.node = node; 
    var loadMask = new Ext.LoadMask(this.earthPanel.getEl(), {msg: 'Загрузка...'});
    loadMask.show();
    
    //var kmlName = 'basic_'+Math.round(Math.random()*10000000000000000)+'.kml';
    //VerifyKML(this.conn, this.domain, kmlName, node, loadMask, this.earthPanel, false);
    Map.GoogleIntegration.westPanel.disable();
    
    this.conn.request({
       url: '/map/googleintegration/getfeaturereader',
       method: 'POST',
       timeout : 300000,
       params: {
                'layerFeatureId': node.attributes.layerFeatureId, 
                'featureClassName': node.attributes.featureClassName,
                'layerColor': node.attributes.layerColor,
                //'kmlName': kmlName,
                'domain': this.domain
       },
       success: function(response){
            this.earthPanel.fetchKml(this.domain+'kmlcache/'+response.responseText, node);
            loadMask.hide();
            Map.GoogleIntegration.westPanel.enable();
       }, scope: this
   }, this);    
}

/*
// Проверяет наличие kml в папке на сервере и выполняет его 
VerifyKML = function(conn, domain, kmlName, node, loadMask, earthPanel, kmlexist) {
    this.kmlexist = kmlexist;
    conn.request({
       url: domain+'kmlcache/'+kmlName,
       success: function(response){
            if (!node.attributes.kmlObject && !this.kmlexist) {
                this.kmlexist = true;
                earthPanel.fetchKml(domain+'kmlcache/'+kmlName, node);
                loadMask.hide();
            }
       },
       failure: function(){
           if (!node.attributes.kmlObject && !this.kmlexist) {
                VerifyKML(conn, domain, kmlName, node, loadMask, earthPanel, this.kmlexist);
           }
       }, scope: this
    });
}
*/