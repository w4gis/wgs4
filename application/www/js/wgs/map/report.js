﻿Ext.namespace('Map');

Map.Report = function(mapViewer)
{
    ///////////////////////

    Ext.ns('Wgs.Map.Report');

    ///////////////////////

    Wgs.Map.Report.MapViewer = mapViewer;
    Wgs.Map.Report.ViewerLib = mapViewer.Lib;
    Wgs.Map.Report.ViewerApi = mapViewer.Api;
    Wgs.Map.Report.WgsConn = new Ext.data.WgsConnection();

    Wgs.Map.Report.ReportPanel = new Map.Report.ReportPanel;

    Wgs.Map.Report.Window = new Ext.WgsServiceWindow({
        containerPanel: Wgs.Map.Report.MapViewer.rightPane,
        width: 512,
        //height: 300,
        autoHeight: true,
        title: 'Статистический отчет',
        resizable: false
    });

    Wgs.Map.Report.MapViewer.addChildComponent(Wgs.Map.Report.Window);
    Wgs.Map.Report.Window.setInnerPanel(Wgs.Map.Report.ReportPanel);

    Wgs.Map.Report.FilterObjects = [];
    //Wgs.Map.Report.FilterObjectsIndex = {};
    Wgs.Map.Report.PolygonObjects = [];
    //Wgs.Map.Report.PolygonObjectsIndex = {};
    Wgs.Map.Report.AllObjects = [];
    Wgs.Map.Report.AllObjectsIndex = {};

    ////////METHODS////////

    this.getWindow = function() {
        return Wgs.Map.Report.Window;
    }

    this.open = function(config) {
        //if (!WGS.Permissions.Check(WGS.PRIVILEGE.CARD_BIND)) {
        //    return;
        //}
        /*
        var config = config||{};
        Wgs.Map.Bind.Mode = config.mode||Map.Bind.OBJECT_CARD_MODE;

        switch (Wgs.Map.Bind.Mode) {
            case Map.Bind.CARD_OBJECT_MODE:
                Wgs.Map.Bind.BindPanel.cancelButton.hide();
                Wgs.Map.Bind.BindPanel.tabs.setActiveTab(1);
                Wgs.Map.Bind.BindPanel.BaseBindPanel.disable();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.bindedGrid.hide();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.cardGrid.show();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.cardStore.loadData([
                    [config.cardId, config.cardName, config.featureTypeName, config.featureTypeId]
                ]);
                break;
            case Map.Bind.OBJECT_CARD_MODE:
                break;
        }
        */
        Wgs.Map.Report.Window.addPanel();
        Wgs.Map.Report.Window.show();
        //Wgs.Map.Report.Window.minimize();
        /*
        Wgs.Map.Bind.BindPanel.BaseBindPanel.bindedGrid.disable();
        Wgs.Map.Bind.OnSelectionChangedHandler = function() {
            Wgs.Map.Bind.ViewerLib.getSelectedFeatures(function(featureLayers){
                if (featureLayers != -1) {
                    var entityId = featureLayers[0].features[0].entityId;
                    var featureId = featureLayers[0].features[0].featureId;
                    if (featureLayers[0].features.length > 1 || featureLayers.length > 1) {
                        with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                            bindedStore.removeAll();
                            bindedGrid.disable();
                        }
                    }
                    else {
                        Wgs.Map.Bind.entityId = entityId;
                        Wgs.Map.Bind.featureId = featureId;
                        with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                            bindedStore.baseParams = {featureId: Wgs.Map.Bind.featureId};
                            bindedStore.load();
                            bindedGrid.enable();
                        }
                    }
                    with (Wgs.Map.Bind.BindPanel.InlineBindPanel) {
                        featuresGrid.enable();
                        featuresStore.baseParams = {'featureIds[]': Wgs.Map.Bind.ViewerLib.getSelectedFeatureIds(featureLayers)};
                        featuresStore.load();
                    }
                } else {
                    with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                        bindedStore.removeAll();
                        bindedGrid.disable();
                    }
                    with (Wgs.Map.Bind.BindPanel.InlineBindPanel) {
                        relationPanel.disable();
                        featuresGrid.disable();
                        bindedGrid.disable();
                    }
                }
            });
        }
        Wgs.Map.Bind.ViewerApi.Map.ObserveOnSelectionChanged(Wgs.Map.Bind.OnSelectionChangedHandler, this);
        */
    }

    /////////EVENTS////////
    with (Wgs.Map.Report.ReportPanel) {
        cancelButton.on('click', function() {
            Wgs.Map.Report.Window.hide();
        });

        exportButton.on('click', function() {
            exportButton.disable();
            var featureTypeName = featureTypeComboBox.getCheckedDisplay();
            var featureTypes = featureTypeComboBox.getCheckedValue().split(',');
            var addProperties = (addPropertyCombobox.getCheckedValue())? addPropertyCombobox.getCheckedValue().split(','): [];
            var generalProperty = generalPropertyCombobox.getValue();
            var generalPropertyName = '';
            generalPropertyStore.findBy(function(record){
                if (record.data.propertyId == generalProperty) {
                    generalPropertyName = record.data.propertyName;
                    return;
                }
            },this);
            new Ext.data.WgsConnection().request({
                url: '/map/report/GetExcelResult',
                method: 'POST',
                params: {
                    'featureIds[]': Wgs.Map.Report.AllObjects,
                    'featureTypes[]': featureTypes,
                    'property': generalProperty,
                    'propertyName': generalPropertyName,
                    'featureTypeName': featureTypeName,
                    'addProperties[]': addProperties
                },
                success: function(response) {
                    exportButton.enable();
                    //alert(response.responseText);
                    //window.open('');
                    window.open ("/map/report/GetExcelFile/"+response.responseText, "excel");
                }
            });
        });
    }

    with (Wgs.Map.Report.ReportPanel) {

        var calculateObjectsCount = function(radio) {

            Wgs.Map.Report.AllObjectsIndex = {};
            Wgs.Map.Report.AllObjects = [];

            if (intersectSelectionRadio.getValue()) {
                for (var i = 0; i < Wgs.Map.Report.PolygonObjects.length; i++) {
                    for (var j = 0; j < Wgs.Map.Report.FilterObjects.length; j++) {
                        if (Wgs.Map.Report.PolygonObjects[i] == Wgs.Map.Report.FilterObjects[j]) {
                            if (!Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.PolygonObjects[i]]) {
                                Wgs.Map.Report.AllObjects.push(Wgs.Map.Report.PolygonObjects[i]);
                                Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.PolygonObjects[i]] = true;
                            }
                        }
                    }
                }

            } else {
                for (var i = 0; i < Wgs.Map.Report.PolygonObjects.length; i++) {
                    if (!Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.PolygonObjects[i]]) {
                        Wgs.Map.Report.AllObjects.push(Wgs.Map.Report.PolygonObjects[i]);
                        Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.PolygonObjects[i]] = true;
                    }
                }
                for (var j = 0; j < Wgs.Map.Report.FilterObjects.length; j++) {
                    if (!Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.FilterObjects[j]]) {
                        Wgs.Map.Report.AllObjects.push(Wgs.Map.Report.FilterObjects[j]);
                        Wgs.Map.Report.AllObjectsIndex[Wgs.Map.Report.FilterObjects[j]] = true;
                    }
                }
            }

            selectPolygonLabel.setText('Объектов: ' + Wgs.Map.Report.PolygonObjects.length);
            objectsFieldSet.setTitle('Выборка объектов ('+ Wgs.Map.Report.AllObjects.length +')');
            selectFilterLabel.setText('Объектов: ' + Wgs.Map.Report.FilterObjects.length);


            featureTypeStore.removeAll();
            featureTypeComboBox.clearValue();
            featureTypeStore.baseParams = {'featureIds[]': Wgs.Map.Report.AllObjects};
            featureTypeStore.load({
                callback: function(records, options) {
                    featureTypesFieldSet.setTitle('Описания (' + records.length + ')');
                }
            });
            generalPropertyCombobox.clearValue();
            addPropertyCombobox.clearValue();
            generalPropertyStore.removeAll();
            addPropertyStore.removeAll();
            statReportStore.removeAll();
        }


        selectPolygonButton.on('click', function() {
            Wgs.Map.Report.Window.hide();
            Map.SelectPolygon.Instance(Wgs.Map.Report.MapViewer).open({
                selectFeaturesOnMap: false,
                callback: function(objects) {
                    Wgs.Map.Report.Window.show();
                    Wgs.Map.Report.PolygonObjects = objects;
                    calculateObjectsCount();
                }
            });
        });


        selectFilterButton.on('click', function() {
            //Wgs.Map.Report.Window.hide();

            var reportFilter = Map.Report.Filter.Instance(Wgs.Map.Report.MapViewer);
            reportFilter.open({
                callback: function(objects) {
                    Wgs.Map.Report.FilterObjects = objects;
                    calculateObjectsCount();
                },
                scope: this
            });

            /*
            Map.SelectPolygon.Instance(Wgs.Map.Report.MapViewer).open({
                selectFeaturesOnMap: false,
                callback: function(objects) {
                    Wgs.Map.Report.Window.show();
                    Wgs.Map.Report.FilterObjectsCount = objects.length;
                    selectFilterLabel.getEl().update('<span style="padding-left: 15px;">Объектов: ' + Wgs.Map.Report.FilterObjectsCount + '</span>');
                    this.objectsFieldSet.setTitle('Выборка объектов ('+ Wgs.Map.Report.PolygonObjectsCount + Wgs.Map.Report.FilterObjectsCount +')');
                }
            });
            */
        });


        resetPolygonButton.on('click', function() {
            //Wgs.Map.Report.PolygonObjectsCount = 0;
            Wgs.Map.Report.PolygonObjects = [];
            calculateObjectsCount();
        });

        resetFilterButton.on('click', function() {
            var reportFilter = Map.Report.Filter.Instance(Wgs.Map.Report.MapViewer);
            reportFilter.objects = [];
            reportFilter.objectsIndex = {};
            Wgs.Map.Report.FilterObjects = [];
            calculateObjectsCount();
        });

        intersectSelectionRadio.on('check', function(checkbox, checked) {
            if (checked) {
                calculateObjectsCount();
            }
        });

        unionSelectionRadio.on('check', function(checkbox, checked) {
            if (checked) {
                calculateObjectsCount();
            }
        });

        featureTypeComboBox.on('select', function(combo, record, index) {
            var featureTypes = combo.getCheckedValue().split(',');
            if (featureTypes == '') {
                generalPropertyStore.baseParams = {};
                combo.setValue();
            } else {
                var NUMERIC = 3;
                var REAL = 4;
                generalPropertyStore.baseParams = {'featureTypes[]': featureTypes, baseFilter: true/*, 'propertyTypeFilter[]': [NUMERIC, REAL]*/};
                //propertyComboBox.enable();
            }
            generalPropertyStore.load();
        });

        generalPropertyCombobox.on('select', function(combo, record, index) {
            var featureTypes = featureTypeComboBox.getCheckedValue().split(',');
            var property = combo.getValue();
            addPropertyStore.baseParams = {'featureTypes[]': featureTypes, baseFilter: true, 'propertyFilter[]': [property]};
            addPropertyStore.load({
            });
        });

        addPropertyCombobox.on('select', function(combo, record, index) {
            var featureTypes = featureTypeComboBox.getCheckedValue().split(',');
            var addProperties = (combo.getCheckedValue())? combo.getCheckedValue().split(','): [];
            var generalProperty = generalPropertyCombobox.getValue();
            if (addProperties.length > 0) {
                statReportStore.removeAll();
                statReportStore.baseParams = {'featureIds[]': Wgs.Map.Report.AllObjects, 'featureTypes[]': featureTypes, 'property': generalProperty, 'addProperties[]': addProperties};
                var loadMask = new Ext.LoadMask(statReportGrid.getEl(),{msg: 'Поиск...'});
                loadMask.show();
                statReportStore.load({
                    callback: function(records) {
                        loadMask.hide();
                        exportButton.setDisabled(records.length == 0);
                    }
                });
            }
        });
    }
}

Map.Report.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Report(map);
    }
    return this.instances[map.mapFrame];
}

Map.Report.ReportPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config)
    {
        Ext.QuickTips.init();
        /*
        this.BaseBindPanel = new Map.Bind.BaseBindPanel(config);
        this.InlineBindPanel = new Map.Bind.InlineBindPanel(config);

        this.tabs = new Ext.TabPanel({
            activeTab: 0,
            items: [this.BaseBindPanel, this.InlineBindPanel]
        });
        */
        this.selectPolygonButton = new Ext.Button({
            text: '...'
        });

        this.selectFilterButton = new Ext.Button({
            text: '...'
        });

        this.selectPolygonLabel = new Ext.form.Label({
            text: 'Объектов: 0'//,
            //style: 'padding: 5px 30px 5px 10px;'
        });

        this.selectFilterLabel = new Ext.form.Label({
            text: 'Объектов: 0'//,
            //style: 'padding: 5px 30px 5px 10px; width: 150px;'
        });

        this.resetPolygonButton = new Ext.Button({
            text: 'Сброс'
        });

        this.resetFilterButton = new Ext.Button({
            text: 'Сброс'
        });

        this.unionSelectionRadio = new Ext.form.Radio({
            name: 'test',
            checked: true,
            //value: 'union',
            boxLabel: 'Объединение'
        });

        this.intersectSelectionRadio = new Ext.form.Radio({
            name: 'test',
            //value: 'intersect',
            boxLabel: 'Пересечение'
        });

        this.objectsFieldSet = new Ext.form.FieldSet({
            title: 'Выборка объектов (0)',
            autoHeight: true,
            items: [
                new Ext.Panel({
                    layout:'table',
                    defaults: {
                        bodyStyle:'padding: 5px 15px'
                    },
                    layoutConfig: {
                        columns: 5
                    },
                    items: [{
                        html: 'Выделить полигоном'
                    },this.selectPolygonButton,
                    this.resetPolygonButton
                    ,{
                        items: this.selectPolygonLabel,
                        width: 120
                    },this.unionSelectionRadio,{
                        html: '<p>Наложить фильтр</p>'
                    },this.selectFilterButton,
                    this.resetFilterButton,{
                    items: this.selectFilterLabel,
                    width: 120
                    },
                    this.intersectSelectionRadio]
                })
            ]
        });

        this.featureTypeStore = new Ext.data.WgsJsonStore({
            autoLoad: true,
            url: '/map/report/getfeaturetypesbyfeatureids',
            fields: ['featureTypeName', 'featureTypeId']
        });

        this.featureTypeComboBox = new Ext.ux.form.LovCombo({
            disabled : false,
            editable : false,
            anchor : '100%',
            emptyText : 'Выбрать описания',
            loadingText : 'Загрузка...',
            store : this.featureTypeStore,
            hideLabel: true,
            labelSeparator: '',
            displayField : 'featureTypeName',
            valueField : 'featureTypeId',
            triggerAction : 'all',
            forceSelection: true
        });

        this.featureTypesFieldSet = new Ext.form.FieldSet({
            title: 'Описания (0)',
            //disabled: true,
            autoHeight: true,
            items: [this.featureTypeComboBox]
        });

        this.generalPropertyStore = new Ext.data.WgsJsonStore({
            reader: new Ext.data.JsonReader({}, [
                {name: 'propertyId'},
                {name: 'propertyName'},
                {name: 'propertyGroupName'},
                {name: 'propertyType'},
                {name: 'class'}
            ]),
            autoLoad: true,
            url: '/objectadmin/search/getpropertylist',
            fields: ['propertyName', 'propertyId', 'propertyGroupName', 'propertyType', 'class']
        });

        this.addPropertyStore = new Ext.data.WgsJsonStore({
            reader: new Ext.data.JsonReader({}, [
                {name: 'propertyId'},
                {name: 'propertyName'},
                {name: 'propertyGroupName'},
                {name: 'propertyType'},
                {name: 'class'}
            ]),
            autoLoad: true,
            url: '/objectadmin/search/getpropertylist',
            fields: ['propertyName', 'propertyId', 'propertyGroupName', 'propertyType', 'class']
        });

        this.generalPropertyCombobox = new Ext.form.WgsComboBox({
            tpl: '<tpl for="."><div class="{class} x-combo-list-item">{propertyName}</div></tpl>',
            store: this.generalPropertyStore,
            editable: false,
            emptyText : 'Основная',
            autoLoad: false,
            hideLabel: true,
            displayField: 'propertyName',
            valueField: 'propertyId',
            loadingText: 'Загрузка...',
            labelSeparator: '',
            triggerAction: 'all',
            listClass: 'class'
        });

        this.addPropertyCombobox = new Ext.ux.form.LovCombo({
            tpl: '<tpl for=".">'
                +'<div class="{class} x-combo-list-item">'
                +'<img src="' + Ext.BLANK_IMAGE_URL + '" '
                +'class="ux-lovcombo-icon ux-lovcombo-icon-'
                +'{[values.checked?"checked":"unchecked"]}">'
                +'<div class="ux-lovcombo-item-text">{propertyName}</div>'
                +'</div>'
                +'</tpl>',
            store: this.addPropertyStore,
            editable: false,
            emptyText : 'Дополнительные',
            autoLoad: false,
            hideLabel: true,
            displayField: 'propertyName',
            valueField: 'propertyId',
            loadingText: 'Загрузка...',
            labelSeparator: '',
            triggerAction: 'all',
            listClass: 'class',
            forceSelection: true
        });

        this.propertiesFieldSet = new Ext.form.FieldSet({
            title: 'Характеристики',
            autoHeight: true,
            items: [{
                layout: 'column',
                items: [{
                    layout: 'fit',
                    columnWidth: 0.5,
                    items: this.generalPropertyCombobox
                },{
                    layout: 'fit',
                    columnWidth: 0.5,
                    items: this.addPropertyCombobox
                }]
            }]
        });

        Ext.grid.GroupSummary.Calculations['total'] = function(v, record, field){
            return Math.round((v + (record.data[field]||0))*100)/100;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var record = Ext.data.Record.create([
            {name: 'value'},
            {name: 'count', type: 'float'},
            {name: 'sum', type: 'float'},
            {name: '_id', type: 'int'},
            {name: '_level', type: 'int'},
            {name: '_lft', type: 'int'},
            {name: '_rgt', type: 'int'},
            {name: '_is_leaf', type: 'bool'}
        ]);

        this.statReportStore = new Ext.ux.maximgb.treegrid.NestedSetStore({
            autoLoad : true,
            reader: new Ext.data.JsonReader({id: '_id'}, record),
            url: '/map/report/getresult',
            fields: ['value', 'count', 'sum', '_id', '_level', '_lft', '_rgt', '_is_leaf']
        });

          function change(val, metadata, record)
          {
            if (val == '') {
                val = '-';
            } else
            if (val == '0') {
                val = '';
            }
            if (record.get('_level') == 2) {
                val = '<span style="color:green;">' + val + '</span>';
            }
            return val;
          }

        this.statReportGrid = new Ext.ux.maximgb.treegrid.GridPanel({
          store: this.statReportStore,
          master_column_id: 'value',
          height: 295,
          frame: true,
          columns: [
            {id: 'value', header: "Значение", width: 160, sortable: true, dataIndex: 'value'},
            {header: "Количество", width: 75, sortable: true, renderer: change, dataIndex: 'count', align: 'center'},
            {header: "Сумма", width: 75, sortable: true, renderer: change, dataIndex: 'sum', align: 'center'}
          ],
          stripeRows: true,
          viewConfig : {
            enableRowBody : true,
            forceFit: true
          }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Map.Report.ReportPanel.superclass.constructor.call(this, {
            title: 'Статистический отчет',
            layout: 'fit',
            width: 500,
            height: 600,
            items: [this.objectsFieldSet,this.featureTypesFieldSet,this.propertiesFieldSet,this.statReportGrid],
            frame: true
        });

        this.exportButton = this.addButton({
            text : 'Экспорт в Excel',
            disabled: true
        });

        this.cancelButton = this.addButton({
            text : 'Закрыть'
        });
    }
});

Map.Report.Filter = Ext.extend(Map.Search, {
    constructor: function(mapViewer) {
        Map.Report.Filter.superclass.constructor.call(this, mapViewer);

        this.container.setTitle('Фильтр объектов');
        //this.panel.searchTabPanel.featureLayerPanel.hide();
        //this.panel.searchTabPanel.splitPanel.hide();
        //this.panel.searchTabPanel.featureTypePanel.columnWidth = 1.0;
        this.panel.searchInCardsButton.hide();
        this.panel.multipleFillingButton.hide();
        this.panel.cancelButton.hide();
        //this.panel.searchInCardsButton.setText('Отобразить все карточки');

        this.objects = [];
        this.objectsIndex = {};

        this.addSelectedObjectsButton = this.panel.addButton({
            text: 'Добавить объекты',
            disabled: true,
            handler: this.addSelectedObjects,
            scope: this
        });

        this.okButton = this.panel.addButton({
            text: 'OK',
            handler: this.okAddObjects,
            scope: this
        });

        this.cancelButton = this.panel.addButton({
            text: 'Отмена',
            handler: this.cancelAddObjects,
            scope: this
        });

        this.panel.resultTabPanel.resultGrid.getBottomToolbar().pageSize = 100;

        this.panel.resultTabPanel.resultStore.baseParams.limit = 100;

        this.panel.resultTabPanel.resultCheckboxSelectionModel.on('selectionchange', function(SelectionModel){
            var selections = SelectionModel.getSelections();
            this.addSelectedObjectsButton.setDisabled(selections.length == 0);
        },this);
    },

    search: function(mode, limit) {
        Map.Report.Filter.superclass.search.call(this, mode, 100);
    },

    open: function(config) {
        var config = config||{};
        this.callback = config.callback;
        this.scope = config.scope;
        /*
        this.bindObject = config.object;
        this.bindCallback = config.callback||Ext.emptyFn;
        this.closeSearchCallback = config.closeSearch||Ext.emptyFn;
        this.bindScope = config.scope;
        this.bindMode = config.mode;
        this.featureTypeFilter = config.featureTypeFilter||[];

        this.container.on('hide', this.closeSearchCallback, this.bindScope, {single: true});

        if (!this.bindObject) {
            Ext.WgsMsg.show({
                title: 'Предупреждение',
                msg: 'Объекты не указаны',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        } else {
            this.panel.searchTabPanel.featureTypeStore.removeAll();
            this.panel.searchTabPanel.featureTypeStore.baseParams = {'featureTypeFilter[]': this.featureTypeFilter};
            this.panel.searchTabPanel.featureTypeStore.load();
            Map.Bind.Search.superclass.open.call(this);
        }
        */
        Map.Report.Filter.superclass.open.call(this);
    },

    addSelectedObjects: function() {
        var selections = this.panel.resultTabPanel.resultCheckboxSelectionModel.getSelections();

        Ext.each(selections, function(item, index){
            if (!this.objectsIndex[item.get('featureId')]) {
                this.objects.push(item.get('featureId'));
                this.objectsIndex[item.get('featureId')] = true;
            }
        },this);

        this.panel.resultTabPanel.resultCheckboxSelectionModel.clearSelections();
        this.addSelectedObjectsButton.setDisabled(true);
    },

    okAddObjects: function()
    {
        this.panel.hide();
        this.callback.call(this.scope, this.objects);
    },

    cancelAddObjects: function()
    {
        this.panel.hide();
        this.objects = [];
        this.objectsIndex = {};
    },

    onShowSearchTabPanel: function()
    {
        Map.Report.Filter.superclass.onShowSearchTabPanel.call(this);
        this.panel.searchInCardsButton.hide();
        this.panel.multipleFillingButton.hide();
        this.addSelectedObjectsButton.hide();
        this.okButton.hide();
    },

    onShowResultTabPanel: function()
    {
        Map.Report.Filter.superclass.onShowResultTabPanel.call(this);
        this.panel.previewButton.hide();
        this.panel.multipleFillingButton.hide();
        this.addSelectedObjectsButton.show();
        this.okButton.show();
    }
});

Map.Report.Filter.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Report.Filter(map);
    }
    return this.instances[map.mapFrame];
}
