﻿Ext.namespace('Map');

Map.Redlining = function(mapViewer)
{
    ///////////////////////
    
    Ext.ns('Wgs.Map.Redlining');
    
    ///////////////////////
    
    Wgs.Map.Redlining.MapViewer = mapViewer;
    Wgs.Map.Redlining.ViewerLib = mapViewer.Lib;
    Wgs.Map.Redlining.ViewerApi = mapViewer.Api;
    Wgs.Map.Redlining.WgsConn = new Ext.data.WgsConnection();

    Wgs.Map.Redlining.RedliningPanel = new Map.Redlining.RedliningPanel;
    
    Wgs.Map.Redlining.Window = new Ext.WgsServiceWindow({
        containerPanel: Wgs.Map.Redlining.MapViewer.rightPane,
        width: 512,
        //height: 300,
        autoHeight: true,
        title: 'Статистический отчет',
        resizable: false
    });

    Wgs.Map.Redlining.MapViewer.addChildComponent(Wgs.Map.Redlining.Window);
    Wgs.Map.Redlining.Window.setInnerPanel(Wgs.Map.Redlining.RedliningPanel);

    Wgs.Map.Redlining.FilterObjects = [];
    //Wgs.Map.Redlining.FilterObjectsIndex = {};
    Wgs.Map.Redlining.PolygonObjects = [];
    //Wgs.Map.Redlining.PolygonObjectsIndex = {};
    Wgs.Map.Redlining.AllObjects = [];
    Wgs.Map.Redlining.AllObjectsIndex = {};
    
    ////////METHODS////////

    this.getWindow = function() {
        return Wgs.Map.Redlining.Window;
    }

    this.open = function(config) {
      
        Wgs.Map.Redlining.Window.addPanel();
        Wgs.Map.Redlining.Window.show();
       
    }
        
    /////////EVENTS////////
    with (Wgs.Map.Redlining.RedliningPanel) {
        cancelButton.on('click', function() {
            Wgs.Map.Redlining.Window.hide();
        });
        
        okButton.on('click', function() {
            var viewerApi = Wgs.Map.Redlining.ViewerApi;
            
            Wgs.Map.Redlining.WgsConn.request({
                url : '/map/redline/',
                method : 'POST',
                params : {
                    'mapname' : viewerApi.Map.GetMapName(),
                    'point' : pointLayerCheckbox.getValue(),
                    'line' : lineLayerCheckbox.getValue(),
                    'polygon' : polygonLayerCheckbox.getValue(),
                    'pointLayerLegend' : 'Точечный слой',
                    'lineLayerLegend' : 'Линейный слой',
                    'polygonLayerLegend' : 'Полигональный слой',
                    'pointLayerName' : 'Point',
                    'lineLayerName' : 'Line',
                    'polygonLayerName' : 'Polygon'
                },
                success : function(response) {
                    alert(response.responceText);
                    viewerApi.Map.Refresh();
                    viewerApi.Legend.Refresh();
                }
            });
        });
    }

 
}

Map.Redlining.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Redlining(map);
    }    
    return this.instances[map.mapFrame];
}

Map.Redlining.RedliningPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config)
    {
        this.pointLayerCheckbox = new Ext.form.Checkbox({
        name : 'point',
        anchor : '100%',
        // checked : false,
        labelSeparator : '',
        hideLabel : true,
        boxLabel : 'Точечный слой'
        });
    
        this.lineLayerCheckbox = new Ext.form.Checkbox({
            name : 'line',
            anchor : '100%',
            // checked : false,
            labelSeparator : '',
            hideLabel : true,
            boxLabel : 'Линейный слой'
        });
        
        this.polygonLayerCheckbox = new Ext.form.Checkbox({
            name : 'polygon',
            anchor : '100%',
            // checked : false,
            labelSeparator : '',
            hideLabel : true,
            boxLabel : 'Полигональный слой'
        });
        
        var simple = new Ext.FormPanel({
            items : [this.pointLayerCheckbox, this.lineLayerCheckbox, this.polygonLayerCheckbox]
        });
        
        Map.Redlining.RedliningPanel.superclass.constructor.call(this, {
            title: 'Рисование',
            layout: 'fit',
            width: 500,
            height: 200,
            items: [simple],
            frame: true
        });
        
        this.okButton = this.addButton({
            text : 'ОК'
        });
        
        this.cancelButton = this.addButton({
            text : 'Закрыть'
        });
    }
});



/*

RedlinePanel = function() {
	// ----Обязательно для каждой панели, которая будет в окне и панели, иначе
	// баг
	var self = this;
	this.parent;
	this.parentType;
	// *****************

	this.setParent = function(parent, parentType) {
		self.parent = parent;
		self.parentType = parentType;
	}

	this.getParent = function() {
		return self.parent;
	}

	this.getParentType = function() {
		return self.parentType;
	}

	this.pointLayerCheckbox = new Ext.form.Checkbox({
		name : 'point',
		anchor : '100%',
		// checked : false,
		labelSeparator : '',
		hideLabel : true,
		boxLabel : 'Точечный слой'
	})

	this.lineLayerCheckbox = new Ext.form.Checkbox({
		name : 'line',
		anchor : '100%',
		// checked : false,
		labelSeparator : '',
		hideLabel : true,
		boxLabel : 'Линейный слой'
	})

	this.polygonLayerCheckbox = new Ext.form.Checkbox({
		name : 'polygon',
		anchor : '100%',
		// checked : false,
		labelSeparator : '',
		hideLabel : true,
		boxLabel : 'Полигональный слой'
	})

	this.okButton = new Ext.Button({
		text : 'ОК',
		handler : function() {
			this.fireEvent('ok')
		},
		scope : this
	})

	this.cancelButton = new Ext.Button({
		text : 'Отмена',
		handler : function() {
			this.fireEvent('cancel')
		},
		scope : this
	})
	var simple = new Ext.FormPanel({

		items : [this.pointLayerCheckbox, this.lineLayerCheckbox,
				this.polygonLayerCheckbox]

	})

	// ----Конструктор
	RedlinePanel.superclass.constructor.call(this, {
		id : 'redline',
		header : true,
		height : 150,
		frame : true,
		items : [simple],
		buttons : [this.okButton, this.cancelButton]
	});
}

Ext.extend(RedlinePanel, Ext.Panel, {
	initComponent : function() {
		var self = this;

		this.addEvents({
			ok : true,
			cancel : true
		});

		this.on('ok', function() {
			var conn = new Ext.data.WgsConnection();
			conn.request({
				url : '/map/redline/',
				method : 'POST',
				params : {
					'mapname' : ViewerApi.Instance().Map.GetMapName(),
					'point' : self.pointLayerCheckbox.getValue(),
					'line' : self.lineLayerCheckbox.getValue(),
					'polygon' : self.polygonLayerCheckbox.getValue(),
					'pointLayerLegend' : 'Точечный слой',
					'lineLayerLegend' : 'Линейный слой',
					'polygonLayerLegend' : 'Полигональный слой',
					'pointLayerName' : 'Point',
					'lineLayerName' : 'Line',
					'polygonLayerName' : 'Polygon'
				},
				success : function(response) {
					alert(response.responseText);
					ViewerApi.Instance().Map.Refresh();
					ViewerApi.Instance().Legend.Refresh();
				}
			});
		});

		this.on('cancel', function() {
			if (self.getParentType() == 'panel') {
				self.getParent().collapse();
				self.getParent().removePanel(self);
			} else
				self.getParent().close();

		});

		RedlinePanel.superclass.initComponent.apply(this, arguments);
	}
});*/
