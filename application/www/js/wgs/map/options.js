﻿Ext.namespace('Map');

Map.Options = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;
    this.conn = new Ext.data.WgsConnection();
    this.panel = new OptionsPanel({
        width: 200,
        heigth: 400
    });
    this.coordSystems = null;
    this.radios = false;
    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        resizable: true,
        width: this.panel.width+12,
        heigth: 500,
        title: 'Настройки',
        layout: 'fit'
    });

    this.panel.on('changecoordsystem', this.onChangeCoordinateSystem, this);
    this.window.setInnerPanel(this.panel);
    this.panel.applyButton.on('click', this.changeCoordinateSystem, this);
    this.panel.okButton.on('click', this.onOk, this);
    this.panel.cancelButton.on('click', this.onCancel, this);
}

Map.Options.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Options(map);
    }
    return this.instances[map.mapFrame];
}

Map.Options.prototype = {
    open: function()
    {
        this.window.addPanel();
        if(!this.radios) {
        this.conn.request({
            url : '/map/options/getcoordsystem',
            method : 'POST',
            success : function(response)
                {
                    var json = response.responseText;
                    var items = [];
                    this.coordSystems = Ext.util.JSON.decode(json);
                    var panelHeight = 0;
                    var index = 0;
                    var checked = false;
                    for (var cs in this.coordSystems) {
                        panelHeight = panelHeight + 40;
                        checked = (index == 0)? true: false;
                        index = index+1;
                        items.push(new Ext.form.Radio({
                            inputValue: cs,
                            boxLabel: this.coordSystems[cs].LEGEND+ '('+this.coordSystems[cs].LABELX+' , '+this.coordSystems[cs].LABELY+')',
                            name:'coordinateSystem',
                            checked: checked,
                            listeners:
                                {
                                    'check': function()
                                    {
                                        this.panel.applyButton.setDisabled(false);
                                    },
                                    scope: this
                                }
                        }));
                    }

                    var rg = new Ext.form.RadioGroup({
                        fieldLabel: '',
                        labelSeparator: '',
                        columns: [280],
                        items: items
                    });

                    this.panel.CoordinateSystemFormPanel.add(rg);
                    this.panel.show();
                    this.window.show();
                    this.panel.setHeight(panelHeight+20);
                },
            scope: this
        });
        this.radios = true;
        } else {
            this.panel.show();
            this.window.show();
        }
    },

    changeCoordinateSystem: function ()
    {
        var coordinateSystemName=this.panel.CoordinateSystemFormPanel.getForm().getValues().coordinateSystem;
        var cs = this.coordSystems[coordinateSystemName];
        this.viewerApi.Map.OnCursorPosChanged(function(x, y){
            var cursorPos = this.viewerApi.Map.ScreenToMapUnits(x, y);
            var csX = cs.FACTORS['X'][0] + cs.FACTORS['X'][1] * cursorPos.X + cs.FACTORS['X'][2] * cursorPos.Y;
            var csY = cs.FACTORS['Y'][0] + cs.FACTORS['Y'][1] * cursorPos.X + cs.FACTORS['Y'][2] * cursorPos.Y;
            this.viewerApi.Statusbar.SetCursorPositionMsg(cs.LEGEND+': '+cs.LABELX+' : '+csX.toFixed(6)+', '+cs.LABELY+' : '+csY.toFixed(6));
        },this);
        this.panel.applyButton.setDisabled(true);
    },

    onOk: function () {
        this.changeCoordinateSystem();
        this.panel.hide();
    },

    onCancel: function () {
        this.panel.hide();
    }
}

OptionsPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config){
        if (config){
            Ext.apply(this, config);
        }

        this.CoordinateSystemFormPanel = new Ext.FormPanel({
            xtype: 'form',
            labelWidth: 1,
            frame: true,
            border: false,
            items:  [
                        {   xtype:'hidden'  }
                    ]
        });

        this.okButton = new Ext.Button({
            text: 'ОК'
        });

        this.cancelButton = new Ext.Button({
            text: 'Отмена'
        });

        this.applyButton = new Ext.Button({
            text: 'Применить'
        });

        OptionsPanel.superclass.constructor.call(this, {
            title: '',
            header: true,
            frame: true,
            layout: 'fit',
            items: [
                    {
                        xtype:'fieldset',
                        title: 'Координатная система',
                        width:'100%',
                        height:'90%',
                        items:[ this.CoordinateSystemFormPanel ]
                    }],
            bbar: ['->', this.okButton, this.cancelButton , this.applyButton]
        });

    }
});
