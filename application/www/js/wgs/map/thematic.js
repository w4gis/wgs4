﻿Ext.namespace("Map")
Map.Thematic = function (mapViewer){
  this.mapViewer = mapViewer;
    this.conn = new Ext.data.Connection();
    this.wgsConn = new Ext.data.WgsConnection();
    //this.panel = new Map.Card.CardPanel(); need
    //this.loadMask = null;
    //this.attrCard = null;  need
    this.modeCard = 0;
    this.featureId = 0;
    this.objectId = 0;
    this.objectIdForPrint = 0;
    if (mapViewer) {
        //Здесь по ходу нада свою мега-гриду замутить)
        this.window = new Ext.WgsServiceWindow({
            containerPanel: mapViewer.rightPane,
            width: this.panel.width+12,
            layout: 'fit',
            title: 'Тематика',
            resizable: false,
            minimizable: false
        });
    } else {
        this.window = new Ext.WgsServiceWindowSimple({
            width: this.panel.width+12,
            layout: 'fit',
            title: 'Тематика',
            resizable: false,
            minimizable: false
        });        
    }
    //all commented needed
    /*this.window.setInnerPanel(this.panel); need
    this.panel.cardCombobox.on('select', this.cardComboboxOnSelect,  this);
    this.panel.printButton.on('click', this.printCard, this);
    this.panel.saveButton.on('click', this.saveCard, this);
    this.panel.cancelButton.on('click', this.onCancel, this);
    this.panel.childCardsStore.on('load', this.onChildCardsLoaded, this);   
    */ 
}
Map.Thematic.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Thematic(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Thematic(map);
        }
        return this.instance;
    }
}

Map.Thematic.Create = function(map)
{
    return new Map.Thematic(map);
}
/*
Map.Thematic.ThematicPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function()
    {
        this.cardsStore = new Ext.data.WgsJsonStore({
            fields: ['cardId', 'cardName', 'cardIsCurrent', 'cardTypeName', 'cardRelationName','cardLabel']
        });
        
        this.childCardsStore = new Ext.data.WgsJsonStore({
            url: '/map/card/getchildobjectsbyparent',
            fields: ['cardId', 'cardName', 'cardTypeName', 'cardRelationName']
        });
                       
        this.cardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%', 
            hideLabel: true, 
            labelSeparator: '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable: false,
            store : this.cardsStore,
            mode: 'local'
        });
        
        this.childCardCombobox = new Ext.form.WgsComboBox({
            anchor: '100%', 
            hideLabel: true, 
            labelSeparator: '',
            displayField : 'cardName',
            valueField : 'cardId',
            triggerAction : 'all',
            emptyText : 'Наименование',
            loadingText : 'Загрузка...',
            editable: false,
            store : this.childCardsStore,
            mode: 'local'
        });
        
        this.basePropertiesReader = new Ext.data.JsonReader({},[
           {name: 'name'},
           {name: 'shortName'},
           {name: 'typeName'},
           {name: 'createDate'},
           {name: 'note'}
        ]);
                        
        this.saveButton = new Ext.Button({
            text : 'Сохранить'
        });
    
        this.cancelButton = new Ext.Button({
            text : 'Закрыть',
            scope : this
        });

        this.attrCardPanel = new Ext.Panel({
            region: 'south',
            height: 530,
            autoWidth: true,
            layout: 'fit'
        });
        
        this.listCardFieldSet = new Ext.form.FieldSet({
            title: 'Список описаний',
            autoHeight: true,
            items: this.cardCombobox
        });                
        
        this.listChildCardFieldSet = new Ext.form.FieldSet({
            title: 'Список привязанных описаний',
            autoHeight: true,
            hidden: true,
            items: this.childCardCombobox
        });  
        
        Map.Card.CardPanel.superclass.constructor.call(this, {
            width: 600,
            autoHeight: true,
            layout: 'border',
            frame: true,
            maskDisabled: false,
            collapsible: false,
            items: [{
                region: 'center',
                autoHeight: true,
                layout: 'fit',
                items:  [ this.listCardFieldSet, this.listChildCardFieldSet]
            },
            this.attrCardPanel]
        });
        
        this.printButton = this.addButton({
            text : 'Печать'
        });
        
        this.saveButton = this.addButton({
            text : 'Применить'
        });
    
        this.cancelButton = this.addButton({
            text : 'Закрыть',
            scope : this
        });
    }
}

);
*/






