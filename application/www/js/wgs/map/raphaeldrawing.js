﻿Ext.namespace('Map', 'Map.ViewerLib');

Map.ViewerLib.RaphaelDrawing = function(viewerApi)
{
    this.viewerApi = viewerApi;   
    this.canvas = null;
    this.temporaryLabel1 = null;
    this.temporaryLabel2 = null;
    this.temporaryCircle = null;
    this.tempoparyRoundedRect = null;
    this.temporaryLine1 = null;
    this.temporaryLine2 = null;
}

Map.ViewerLib.RaphaelDrawing.prototype = {
    createCanvas: function()
    {
        if (!this.canvas) {
            this.canvas = this.viewerApi.map.CreateRaphael();
        }
        return this.canvas;
    },
       
    drawTemporaryLabel1: function (x, y, text, attributes) 
    {
        if (this.temporaryLabel1)
            this.temporaryLabel1.remove();
        this.temporaryLabel1 = this.canvas.text(x, y, text);
        this.temporaryLabel1.attr(attributes);    
    },
    
    drawTemporaryLabel2: function(x, y, text, attributes) 
    {
        if (this.temporaryLabel2)
            this.temporaryLabel2.remove();
        this.temporaryLabel2 = this.canvas.text(x, y, text);
        this.temporaryLabel2.attr(attributes);   
    },
    
    drawTemporaryCircle: function (x, y, r, attributes)
    {   
        if (this.temporaryCircle)  
            this.temporaryCircle.remove();
        this.temporaryCircle = this.canvas.circle(x, y, r);
        this.temporaryCircle.attr(attributes);
    },
    
    drawTemporaryRoundedRect: function (x, y, w, h, r, attributes)
    {   
        if (this.tempoparyRoundedRect)  
            this.tempoparyRoundedRect.remove();
        this.tempoparyRoundedRect = this.canvas.rect(x, y, w, h, r);
        this.tempoparyRoundedRect.attr(attributes);
    },
    
    drawTemporaryLine1: function (x0, y0, x1, y1, attributes)
    {
        if (this.temporaryLine1)  
            this.temporaryLine1.remove();
        this.temporaryLine1 = this.canvas.path(attributes).moveTo(x0, y0).lineTo(x1, y1);
    },
    
    drawTemporaryLine2: function(x0, y0, x1, y1, attributes)
    {
        if (this.temporaryLine2)  
            this.temporaryLine2.remove();
        this.temporaryLine2 = this.canvas.path(attributes).moveTo(x0, y0).lineTo(x1, y1);
    },
        
    drawLabel: function(x, y, text, attributes) 
    {
        this.label = this.canvas.text(x, y, text);
        this.label.attr(attributes); 
    },
    
    drawCircle: function(x, y, r, attributes)
    {   
        this.circle = this.canvas.circle(x, y, r);
        this.circle.attr(attributes);
    },
            
    drawRoundedRect: function(x, y, w, h, r, attributes)
    {   
        this.roundedRect = this.canvas.rect(x, y, w, h, r);
        this.roundedRect.attr(attributes);
    },
    
    drawLine: function(x0, y0, x1, y1, attributes)
    {   
        this.canvas.path(attributes).moveTo(x0, y0).cplineTo(x1, y1);
    },
        
    hideLabelDistance: function () {
        if (this.temporaryLine2)
            this.temporaryLine2.hide();
        if (this.tempoparyRoundedRect)  
            this.tempoparyRoundedRect.hide();
        if (this.temporaryLabel1)
            this.temporaryLabel1.hide();
        if (this.temporaryLabel2)
            this.temporaryLabel2.hide(); 
    },
    
    clear: function () 
    {
        if (this.canvas) {
            this.canvas.clear();
            this.temporaryLabel1 = null;
            this.temporaryLabel2 = null;
            this.temporaryCircle = null;
            this.tempoparyRoundedRect = null;
            this.temporaryLine1 = null;
            this.temporaryLine2 = null;
        }
    },
    
    destroy: function() 
    {
        if (this.canvas) {
            this.canvas.remove();
            this.canvas = null;
        }
    }
}
