﻿Ext.namespace('Map');

Map.Search = function(container)
{
    this.wgsConn = new Ext.data.WgsConnection();
    this.loadMask = null;
    if (Map.Viewer && container instanceof Map.Viewer) {
        this.mapViewer = container;
        this.panel = new Map.Search.SearchPanel({
            mapViewer: this.mapViewer
        });
        this.container = new Ext.WgsServiceWindow({
            containerPanel: this.mapViewer.rightPane,
            width: this.panel.width+12,
            title: 'Поиск объектов',
            resizable: true,
            minWidth: 600,
            minHeight: 600,
            layout: 'fit'
        });
        //this.container.setInnerPanel(this.panel);
    }
    else
    if (container instanceof Ext.WgsServiceTabPanel) {
        this.mapViewer = null;
        this.panel = new Map.Search.ObjectAdminSearchPanel({
            mapViewer: this.mapViewer,
            closable: false,
            title: 'Поиск объектов',
            frame: true
        });
        this.container = container;
    }
    else {
        this.mapViewer = null;
        this.panel = new Map.Search.SearchPanel({
            mapViewer: this.mapViewer
        });
        this.container = new Ext.WgsServiceWindowSimple({
            width: this.panel.width+12,
            title: 'Привязка объектов',
            resizable: true,
            minWidth: 600,
            minHeight: 600,
            layout: 'fit'
        });
        //this.container.setInnerPanel(this.panel);
    }

    this.groups = [];

    this.container.setInnerPanel(this.panel);

    this.container.on('resize', function(){
    this.panel.searchTabPanel.constructorQueryPanel.doLayout(); },this);
    this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.on('select', this.onPropertyComboBoxSelect, this);
    this.panel.resultTabPanel.on('show', this.onShowResultTabPanel, this);
    this.panel.searchTabPanel.on('show', this.onShowSearchTabPanel, this);
    this.container.on('beforehide', function(){this.panel.searchTabPanel.show()}, this);
    this.panel.searchOnMapButton.on('click', this.onSearchOnMapButtonClick, this);
    this.panel.searchInCardsButton.on('click', this.onSearchInCardsButtonClick, this);
    this.panel.multipleFillingButton.on('click', this.onMultipleFillingButtonClick, this);
    this.panel.previewButton.on('click', this.onPreviewButtonClick, this);
    this.panel.previewAllButton.on('click', this.onPreviewAllButtonClick, this);
    this.panel.searchTabPanel.featureLayerComboBox.on('select', this.onFeatureLayerComboBoxSelect, this);
    this.panel.searchTabPanel.featureTypeComboBox.on('select', this.onFeatureTypeComboBoxSelect, this);
    this.panel.searchTabPanel.featureTypeComboBox.on('beforeselect', this.onFeatureTypeComboBoxBeforeSelect, this);
    this.panel.searchTabPanel.allSearchTabPanel.on('tabchange', function(tabpanel, tab){
        if(tab.title == 'Расширенный поиск') {
            this.panel.searchTabPanel.featureQueryGrid.setHeight(355);
            this.addEmptyExtendedSearchCondition();
        }
        tab.doLayout();
    }, this);

    /*обработчики расширенного поиска*/
    this.panel.searchTabPanel.propertyCombobox.on('select', function(combo, record, index){
        this.getValuesByPropertyId(combo, record, index);
    }, this);
    this.panel.searchTabPanel.conditionCombobox.on('select', function(combo, record, index){
        this.setConditionOfProrerty(combo, record, index);
    }, this);
    this.panel.searchTabPanel.valueCombobox.on('select', function(combo, record, index){
        this.setValueOfProrerty(combo, record, index);
    }, this);
    this.panel.searchTabPanel.valueCombobox.on('valid', function(combo){
        this.switchSearchIfPossible();
    }, this);
    this.panel.searchTabPanel.valueCombobox.on('specialkey', function(field, e){
        if (e.getKey() == e.ENTER) {
            this.panel.searchTabPanel.valueCombobox.setValue(field.getEl().dom.value);
            this.setValueOfProrertyOnEnter(field.getEl().dom.value);
        }
    }, this);
    this.panel.searchTabPanel.featureQuerySelectionModel.on('selectionchange', function(sm){
        this.featureQueryConditionSelectedChanged(sm);
    }, this);
    this.panel.searchTabPanel.featureQuerySelectionModel.on('rowselect', function(sm, index, record){
        this.featureQueryConditionRowSelect(sm, index, record);
    }, this);
    this.panel.searchTabPanel.addQueryConditionButton.on('click', function(){
        this.addQueryCondition();
    }, this);
    this.panel.searchTabPanel.removeQueryConditionButton.on('click', function(){
        this.removeQueryCondition();
    }, this);
    this.panel.searchTabPanel.groupQueryConditionButton.on('click', function(){
        this.groupQueryCondition();
    }, this);
    this.panel.searchTabPanel.ungroupQueryConditionButton.on('click', function(){
        this.ungroupQueryCondition();
    }, this);
    this.panel.searchTabPanel.operationNotCheckbox.on('check', function(checkbox, checked){
        this.setNotConditionOfProrerty(checkbox, checked);
    }, this);
}

Map.Search.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Search(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Search(map);
        }
        return this.instance;
    }
}

Map.Search.Create = function(map)
{
    return new Map.Search(map);
}

Map.Search.prototype = {
    open: function()
    {
        this.container.addPanel();
        this.container.show();
    },

    searchCardsByType: function(attrType)
    {
        var featureTypeStoreOnLoad = function(){
            this.panel.searchTabPanel.featureTypeComboBox.setValue(attrType);
            this.panel.searchTabPanel.featureTypeStore.un('load', featureTypeStoreOnLoad, this);
            this.search(1);
        }
        this.panel.searchTabPanel.featureTypeStore.on('load', featureTypeStoreOnLoad, this);
        this.panel.cancelButton.on('click', function() {
            this.container.remove(this.panel, false);
        },this);
    },

    searchCardsByLayer: function(layer)
    {
        var featureLayerStoreOnLoad = function(){
            this.panel.searchTabPanel.featureLayerComboBox.setValue(layer);
            this.panel.searchTabPanel.featureLayerStore.un('load', featureLayerStoreOnLoad, this);
            this.search(0);
        }
        this.panel.searchTabPanel.featureLayerStore.on('load', featureLayerStoreOnLoad, this);
        this.panel.cancelButton.on('click', function() {
            this.container.remove(this.panel, false);
        },this);
    },

    search: function(mode, limit)
    {
        var limit = limit||30;
        var featureTypes  = this.panel.searchTabPanel.featureTypeComboBox.getCheckedValue().split(',');
        var featureLayers = this.panel.searchTabPanel.featureLayerComboBox.getCheckedValue().split(',');

        if(this.panel.searchTabPanel.allSearchTabPanel.getActiveTab().title != 'Расширенный поиск')
        {
            var form = this.panel.searchTabPanel.simpleSearchPanel.getForm();
            var values = form.getValues();
            var condition = [];
            for (var key in values) {
                if (key.indexOf('PROP') != -1) {
                    var prop = key.split('_');
                    switch (parseInt(prop[1])) {
                        case 2:
                        case 3:
                        case 4:
                            condition.push('' + prop[0] + '_' + prop[2] + ' = \'' + values[key] + '\'');
                            break;
                        case 5:
                            var field = this.panel.searchTabPanel.simpleSearchPanel.findBy(function(component){
                                if (component.initialConfig.name == key) {
                                    return true;
                                }
                            });
                            if (field && field.length) {
                                condition.push('' + prop[0] + '_' + prop[2] + ' = \'' + field[0].getValue() + '\'');
                            }
                            break;
                        case 1:
                        case 6:
                        case 7:
                            condition.push('UPPER(' + prop[0] + '_' + prop[2] + ') LIKE UPPER(\'%' + values[key] + '%\')');
                            break;
                    }
                }
            }
            condition = condition.join(' AND ');
        }
        else
        {
            var condition = '';
            if(this.isPossibleSearching()) {
                var query = '';
                var lastRecord = this.panel.searchTabPanel.featureQueryStore.getAt(this.panel.searchTabPanel.featureQueryStore.getCount()-1);
                lastRecord.set('queryRelation', '');
                lastRecord.commit();
                this.panel.searchTabPanel.featureQueryStore.each(function(record){
                    var queryCond = record.get('groupStart');
                    var upper = 0;
                    //alert(record.get('propertyValueForQuery'));
                    if(record.get('propertyCondition') != 'нулевое')
                        if (record.get('propertyConditionValue') != 'LIKE')
                            condition = record.get('propertyConditionValue')+" '"+ record.get('propertyValueForQuery')+"'";
                        else {
                            condition = record.get('propertyConditionValue')+" UPPER('%"+ record.get('propertyValueForQuery')+"%')";
                            upper = 1;
                        }
                    else
                        condition = 'IS NULL';

                    var upperProp = '';
                    if(upper == 1)
                        upperProp = 'UPPER(PROP_'+record.get('propertyId')+') ';
                    else
                        upperProp = ' PROP_'+record.get('propertyId')+' ';

                    if(record.get('propertyConditionNOT') != '')
                        queryCond = queryCond+ ' NOT(' + upperProp + condition + ')';
                    else
                        queryCond = queryCond + upperProp + condition;

                    queryCond = queryCond + record.get('groupEnd');
                    if(record.get('queryRelation') == 'И')
                        queryCond = queryCond + ' AND ';
                    if(record.get('queryRelation') == 'ИЛИ')
                        queryCond = queryCond + ' OR ';
                    query = query+ queryCond;
            });
            condition = query;
            }
        }
        this.panel.resultTabPanel.resultStore.removeAll();

        if ( featureTypes.join(',') == '' && featureLayers.join(',') == '' ) {
            this.panel.resultTabPanel.resultStore.baseParams = {condition: condition, mode: mode};
        } else
        if ( featureTypes.join(',') == '') {
            this.panel.resultTabPanel.resultStore.baseParams = {'featureLayers[]': featureLayers, condition: condition, mode: mode};
        } else
        if (featureLayers.join(',') == '') {
            this.panel.resultTabPanel.resultStore.baseParams = {'featureTypes[]': featureTypes, condition: condition, mode: mode};
        } else {
            this.panel.resultTabPanel.resultStore.baseParams = {'featureTypes[]': featureTypes, 'featureLayers[]': featureLayers, condition: condition, mode: mode};
        }

        if (this.featureTypeFilter && this.featureTypeFilter.length > 0) {
            this.panel.resultTabPanel.resultStore.baseParams['featureTypeFilter[]'] = this.featureTypeFilter;
        }

        this.panel.resultTabPanel.resultStore.baseParams['docSearch'] = this.panel.resultTabPanel.docCheckBox.getValue();

        this.loadMask = new Ext.LoadMask(this.panel.getEl(),{msg: 'Поиск...'});
        this.loadMask.show();
        this.panel.resultTabPanel.resultStore.baseParams.limit = limit;
        this.panel.resultTabPanel.resultStore.load({
            callback: function(records) {
                this.panel.resultTabPanel.show();
                this.loadMask.hide();
                if (Map.CardsJournal) {
                	Map.CardsJournal.onFeatureTypeSelect(featureTypes, records, this.panel.exportJournalButton);
                }
            },
            scope: this
        });
    },

    onSearchOnMapButtonClick: function()
    {
        this.search.call(this, 0);
    },

    onSearchInCardsButtonClick: function()
    {
        this.search.call(this, 1);
        if(WGS.PRIVILEGE.CARD_EDIT)
            this.panel.multipleFillingButton.show();
    },

    onMultipleFillingButtonClick: function()
    {
         var selections = this.panel.resultTabPanel.resultCheckboxSelectionModel.getSelections();
         var objectIds = [];
         Ext.each(selections, function(item, index){
            var objectId = item.get('objectId');
            if (objectId)
                objectIds.push(objectId);
        });
        if (objectIds.length > 0) {
            Map.MultipleFilling.Instance(this.mapViewer).open({
            objectIds: objectIds,
            callback: function() {
                this.panel.resultTabPanel.resultStore.reload();
            },
            scope: this
            });
        }
    },

    onPreviewButtonClick: function()
    {
        var selections = this.panel.resultTabPanel.resultCheckboxSelectionModel.getSelections();
        var features = [];
        var hasNull = false;
        Ext.each(selections, function(item, index){
            var entityId = item.get('entityId');
            if (!entityId) {
                hasNull = true;
                return false;
            }
            features.push(entityId);
        });
        if (hasNull) {
            features = {};
            features.features = [];
            features.objects = [];
            Ext.each(selections, function(item, index){
                var entityId = item.get('entityId');
                if (entityId) {
                    features.features.push(item.get('entityId'));
                } else {
                    features.objects.push(item.get('objectId'));
                }
            });
        }

        if (features.length > 0 || !Ext.isArray(features)) {
            if (this.mapViewer) {
                this.mapViewer.Lib.selectFeatures(features, function(){
				   this.container.hide();
                    this.mapViewer.Api.Map.Refresh();
                }, this, true);
            } else {
                if (!this.previewWindow && !this.previewMapViewer) {
                    this.previewWindow = new Ext.WgsShimWindow({
                        width: 750,
                        maximizable: true,
                        resizable: true,
                        layout: 'fit',
                        closeAction: 'hide',
                        title: 'Отображение объектов'
                    });
                    this.previewMapViewer = new Map.Viewer({
                        viewerType: 'ajax',
                        simpleMode: true,
                        render: function(viewerPanel) {
                            viewerPanel.setWidth(740);
                            viewerPanel.setHeight(480);
                            this.previewWindow.add(viewerPanel);
                            this.previewWindow.show();
                        },
                        load: function(api, lib, commands) {
                         	lib.selectFeatures(features);
                        },
                        scope: this
                    });
                } else {
                    this.previewWindow.show();
					this.previewMapViewer.Lib.selectFeatures(features);
                }
            }
        }
    },
	
	onPreviewAllButtonClick: function()
    {
	var self = this;
	var bp = this.panel.resultTabPanel.resultStore.baseParams ;
	bp.limit = this.panel.resultTabPanel.resultStore.totalLength;
        Ext.Ajax.request({
            url: '/objectadmin/search/getresult',
			success    : function(response, options) {
				data = Ext.util.JSON.decode(response.responseText);														
				entities  = []
				objects  = []
				for(i=0;i<data.rows.length;i++){
					if(data.rows[i].entityId)
						entities.push(data.rows[i].entityId)
					else 
						objects.push(data.rows[i].objectId)
				}
				if(objects[0]){
					features = {};
					features.features = entities;
					features.objects = objects;
				}else
					features = entities;
					
				if (features.length > 0 || !Ext.isArray(features)) {
            if (self.mapViewer) {
                self.mapViewer.Lib.selectFeatures(features, function(){
				   self.container.hide();
                    self.mapViewer.Api.Map.Refresh();
                }, self, true);
            } else {
                if (!self.previewWindow && !self.previewMapViewer) {
                    self.previewWindow = new Ext.WgsShimWindow({
                        width: 750,
                        maximizable: true,
                        resizable: true,
                        layout: 'fit',
                        closeAction: 'hide',
                        title: 'Отображение объектов'
                    });
                    self.previewMapViewer = new Map.Viewer({
                        viewerType: 'ajax',
                        simpleMode: true,
                        render: function(viewerPanel) {
                            viewerPanel.setWidth(740);
                            viewerPanel.setHeight(480);
                            self.previewWindow.add(viewerPanel);
                            self.previewWindow.show();
                        },
                        load: function(api, lib, commands) {
							lib.selectFeatures(features);
                        },
                        scope: self
                    });
                } else {
                    self.previewWindow.show();
                    self.previewMapViewer.Lib.selectFeatures(features);
                }
            }
        }
			},
			failure    : function() {
				// Сообщение об ошибке
				Ext.MessageBox.alert('Ошибка поиска','Не удалось загрузить список объектов.');
			},
			params    : bp,
			method	: 'POST'
		});
		
		
        
    },

    onPropertyComboBoxSelect: function()
    {
        var propertyGroupName, propertyId, propertyName, propertyType, propertyField;
        var searchTabPanel          = this.panel.searchTabPanel;
        var propertyIds             = searchTabPanel.simpleSearchPanel.propertyComboBox.getCheckedValue();
        var propertyGroupCollection = searchTabPanel.simpleSearchPanel.propertyGroupCollection;
        var propertyCollection      = searchTabPanel.simpleSearchPanel.propertyCollection;

        if (propertyIds) {
            this.panel.searchOnMapButton.setText('Найти на карте');
            this.panel.searchInCardsButton.setText('Найти в карточках');
        } else {
            this.panel.searchOnMapButton.setText('Все объекты');
            this.panel.searchInCardsButton.setText('Все карточки');
        }

        if (propertyIds.indexOf(',') == -1) {
            propertyIds = [propertyIds];
        } else {
            propertyIds = propertyIds.split(',');
        }
        for (var i = 0; i < propertyIds.length; i++) {
            propertyId = propertyIds[i];
            if (!propertyCollection[propertyId]) {
                break;
            }
        }

        var records = searchTabPanel.propertyStore.query('propertyId', propertyId);
        if (records.length == 1) {
            propertyGroupName = records.item(0).data.propertyGroupName;
            propertyName      = records.item(0).data.propertyName;
            propertyType      = records.item(0).data.propertyType;

            if (!propertyGroupCollection[propertyGroupName]) {
                propertyGroupCollection[propertyGroupName] = new Ext.form.FieldSet({
                    title: propertyGroupName,
                    autoHeight: true
                });
            }

            if (!propertyCollection[propertyId]) {
                // regex : /^([\wА-Яа-я][-_ ]?)+$/,
                // maskRe : /[a-zA-ZА-Яа-я0-9-_ ]/
                var propertyFieldName = 'PROP_' + parseInt(propertyType) + '_' + propertyId;
                switch (parseInt(propertyType)) {
                    case 1:
                    case 3:
                    case 4:
                    case 6:
                    case 7:
                        propertyField = new Ext.form.TextField({
                            name: propertyFieldName,
                            anchor: '100%',
                            fieldLabel: propertyName
                        });
                        break;
                    case 2:
                        propertyField = new Ext.form.DateField({
                            name: propertyFieldName,
                            anchor: '100%',
                            fieldLabel: propertyName,
                            format: 'Y-m-d'
                        });
                        break;
                    case 5:
                        propertyField = new Ext.form.WgsComboBox({
                            editable: false,
                            anchor: '100%',
                            loadingText: 'Загрузка...',
                            fieldLabel: propertyName,
                            name: propertyFieldName,
                            store: new Ext.data.WgsJsonStore({
                                reader: new Ext.data.JsonReader({}, [
                                    {name: 'domainValueId'},
                                    {name: 'domainValue'}
                                ]),
                                autoLoad: false,
                                url: '/objectadmin/search/getdomainvaluelist',
                                baseParams: {propertyId: propertyId},
                                fields: ['domainValueId', 'domainValue']
                            }),
                            displayField: 'domainValue',
                            valueField: 'domainValueId',
                            triggerAction: 'all'
                        });
                        break;
                }
                propertyCollection[propertyId] = new Ext.Panel({
                    border: false,
                    layout: 'column',
                    items: [{
                        columnWidth: 0.935,
                        layout: 'form',
                        items: [propertyField]
                    },{
                        columnWidth: 0.065,
                        align: 'center',
                        bodyStyle: 'padding-left: 5px',
                        items: [
                            new Ext.Button({
                                text: '-',
                                handler:  function() {
                                    this.clearSimpleSearchPanel.call(this, /*propertyGroupName, */propertyId);
                                },
                                scope: this
                            })
                        ]
                    }]
                });
                propertyCollection[propertyId].propertyGroupName = propertyGroupName;
                propertyGroupCollection[propertyGroupName].add(propertyCollection[propertyId]);
                propertyGroupCollection[propertyGroupName].doLayout();
                searchTabPanel.simpleSearchPanel.add(propertyGroupCollection[propertyGroupName]);
                searchTabPanel.simpleSearchPanel.doLayout();
            }
        }
        for (var propId in propertyCollection) {
            if (!propertyIds.inArray(propId)) {
                this.clearSimpleSearchPanel.call(this, propId);
                break;
            }
        }
    },

    onFeatureTypeStoreLoad: function(store, records)
    {
        var properties = [];
        if ( store.getTotalCount() > 0 ) {
            var checkedProperties = this.panel.searchTabPanel.featureTypeComboBox.getCheckedValue().split(',');
            store.each(function(item, index){
                if (checkedProperties.inArray(item.get('featureTypeId'))) {
                    properties.push(item.get('featureTypeId'));
                }
            });
        } else {
            this.panel.searchTabPanel.featureTypeComboBox.disable();
            this.panel.searchTabPanel.propertyStore.baseParams = {};
            this.panel.searchTabPanel.propertyStore.load();
            this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.disable();
            //this.panel.searchTabPanel.simpleSearchPanel.disable();
        }
        this.panel.searchTabPanel.featureTypeComboBox.setValue(properties.join(','));
        this.clearSimpleSearchPanel();
    },

    onPropertyStoreLoad: function(store, records)
    {
        var checkedProperties = this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.checkedProperties;
        var properties = [];
        if (checkedProperties) {
            store.each(function(item, index){
                if (checkedProperties.inArray(item.get('propertyId'))) {
                    properties.push(item.get('propertyId'));
                }
            });
        }
        this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.setValue(properties.join(','));
        this.clearSimpleSearchPanel();
    },

    onFeatureLayerComboBoxSelect: function(combo, record, index)
    {
        var featureLayers = combo.getCheckedValue().split(',');
        this.panel.searchTabPanel.featureTypeComboBox.enable();
        this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.enable();
        if (featureLayers == '') {
            this.panel.searchTabPanel.featureTypeStore.baseParams = {};
            combo.setValue();
        } else {
            this.panel.searchTabPanel.featureTypeStore.baseParams = {'featureLayers[]': featureLayers};
        }
        this.panel.searchTabPanel.featureTypeStore.removeAll();
        this.panel.searchTabPanel.featureTypeStore.load();
        //this.addEmptyExtendedSearchCondition();
    },

    onFeatureTypeComboBoxBeforeSelect: function(combo, record, index)
    {
       var value = combo.getCheckedValue();
       if(this.panel.searchTabPanel.allSearchTabPanel.getActiveTab().title == 'Расширенный поиск') {
            if(this.isPossibleSearching() || this.panel.searchTabPanel.featureQueryStore.getCount() > 1) {
                Ext.WgsMsg.show({
                    title: 'Подтверждение',
                    msg: 'Фильтр поиска будет сброшен. Продолжить операцию?',
                    buttons: Ext.Msg.YESNO,
                    fn: function(buttonId){
                        if (buttonId == 'yes') {
                            this.addEmptyExtendedSearchCondition();
                            this.panel.searchTabPanel.propertyCombobox.clearValue();
                            this.onFeatureTypeComboBoxSelect(combo, record, index);
                        } else {
                            combo.setValue(value);
                            this.onFeatureTypeComboBoxSelect(combo, record, index);
                        }
                    },
                    icon: Ext.MessageBox.QUESTION,
                    scope: this
                });
            }
        }
    },

    onFeatureTypeComboBoxSelect: function(combo, record, index)
    {
        var featureTypes = combo.getCheckedValue().split(',');
        if (featureTypes == '') {
            this.panel.searchTabPanel.propertyStore.baseParams = {};
            //this.panel.searchTabPanel.propertyStore.removeAll();
            combo.setValue();
        } else {
            this.panel.searchTabPanel.propertyStore.baseParams = {'featureTypes[]': featureTypes};
            this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.enable();
        }
        this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.checkedProperties = this.panel.searchTabPanel.simpleSearchPanel.propertyComboBox.getCheckedValue().split(',');
        this.panel.searchTabPanel.propertyStore.load();

    },

    clearSimpleSearchPanel: function(propertyId)
    {
        var simpleSearchPanel = this.panel.searchTabPanel.simpleSearchPanel;
        var propertyGroupName;
        if (propertyId) {
            if (simpleSearchPanel.propertyCollection[propertyId]) {
                propertyGroupName = simpleSearchPanel.propertyCollection[propertyId].propertyGroupName;
            }
        }
        var checkedPropertyIds = simpleSearchPanel.propertyComboBox.getCheckedValue() || [];
        var propertyValues = [];

        if (checkedPropertyIds) {
            if (checkedPropertyIds.indexOf(',') == -1) {
                checkedPropertyIds = [checkedPropertyIds];
            } else {
                checkedPropertyIds = checkedPropertyIds.split(',');
            }
        }
        if (propertyId != null && propertyGroupName != null) {
            simpleSearchPanel.propertyGroupCollection[propertyGroupName].remove(simpleSearchPanel.propertyCollection[propertyId]);
            delete simpleSearchPanel.propertyCollection[propertyId];
            if (!simpleSearchPanel.propertyGroupCollection[propertyGroupName].items.length) {
                simpleSearchPanel.remove(simpleSearchPanel.propertyGroupCollection[propertyGroupName]);
                delete simpleSearchPanel.propertyGroupCollection[propertyGroupName];
            }
            for (var i = 0; i < checkedPropertyIds.length; i++) {
                var propertyId = checkedPropertyIds[i]
                if (simpleSearchPanel.propertyCollection[propertyId]) {
                    propertyValues.push(propertyId);
                }
            }
            if (propertyValues.length) {
                this.panel.searchOnMapButton.setText('Найти на карте');
                this.panel.searchInCardsButton.setText('Найти в карточках');
            } else {
                this.panel.searchOnMapButton.setText('Все объекты');
                this.panel.searchInCardsButton.setText('Все карточки');
            }

            simpleSearchPanel.propertyComboBox.setValue(propertyValues.join(","));
        } else {
            for (var propertyId in simpleSearchPanel.propertyCollection) {
                if (!checkedPropertyIds.inArray(propertyId)) {
                    this.clearSimpleSearchPanel(propertyId);
                }
            }
        }
    },

    onShowResultTabPanel: function()
    {
        this.panel.previewButton.show();
        this.panel.previewAllButton.show();
        this.panel.searchOnMapButton.hide();
        this.panel.searchInCardsButton.hide();
    },

    onShowSearchTabPanel: function()
    {
        this.panel.previewButton.hide();
        this.panel.previewAllButton.hide();
        this.panel.multipleFillingButton.hide();
        this.panel.searchInCardsButton.show();
        this.panel.searchOnMapButton.show();
        this.panel.searchTabPanel.simpleSearchPanel.doLayout();
        this.panel.searchTabPanel.doLayout();

        this.panel.searchTabPanel.propertyStore.on('load', this.onPropertyStoreLoad, this);
        this.panel.searchTabPanel.featureTypeStore.on('load', this.onFeatureTypeStoreLoad, this);
    },

    /*методы расширенного поска*/
    //--Extended Search methods
    addEmptyExtendedSearchCondition: function()
    {
        /*if(this.panel.searchTabPanel.featureQueryStore.getCount() == 0)
        {*/
            this.clearExtendedSearchFilter();
            this.groups.clear();
            this.addQueryCondition();
        /*}*/
    },

    getValuesByPropertyId: function(combo, record, index)
    {
        /*var featureTypes = [];
            Ext.each(this.panel.searchTabPanel.featureTypeGridCheckboxSelectionModel.getSelections(), function(item, index){
                featureTypes.push(item.get('featureTypeId'));
            }, this);*/
        this.loadPropertyValues();
        this.panel.searchTabPanel.valueCombobox.clearValue();
        this.panel.searchTabPanel.valueCombobox.validator = function(){return true};
        this.panel.searchTabPanel.valueCombobox.setDisabled(false);
        this.panel.searchTabPanel.conditionCombobox.clearValue();
        var changeRecord = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        //combo.getEl().dom.value = record.data.propertyName.replace(/(?:<span.*?>)/ig,'').replace(/(?:<\/span>)/ig,'');
        changeRecord.data.propertyName = record.data.propertyName;
        changeRecord.data.propertyId = record.data.propertyId;
        changeRecord.data.propertyConditionValue  = '';
        changeRecord.data.propertyCondition = '<условие>';
        changeRecord.data.propertyValue = '<значение>';
        changeRecord.commit();
    },

    loadPropertyValues: function()
    {
        var featureTypes  = this.panel.searchTabPanel.featureTypeComboBox.getCheckedValue().split(',');
        var featureLayers = this.panel.searchTabPanel.featureLayerComboBox.getCheckedValue().split(',');
        var propertyId = this.panel.searchTabPanel.propertyCombobox.getValue();
        var propertyType = this.getPropertyTypeByPropertyId(propertyId);
        this.setConditionStates(propertyType);
        var baseParams;
        if ( featureTypes.join(',') == '' && featureLayers.join(',') == '' ) {
            baseParams = {'propertyId': propertyId, 'propertyType': propertyType};
        } else
        if ( featureTypes.join(',') == '') {
            baseParams = {'featureLayers[]': featureLayers, 'propertyId': propertyId, 'propertyType': propertyType};
        } else
        if (featureLayers.join(',') == '') {
            baseParams = {'featureTypes[]': featureTypes, 'propertyId': propertyId, 'propertyType': propertyType};
        } else {
            baseParams = {'featureTypes[]': featureTypes, 'featureLayers[]': featureLayers, 'propertyId': propertyId, 'propertyType': propertyType};
        }
        this.panel.searchTabPanel.propertyValueStore.baseParams = baseParams;
        this.panel.searchTabPanel.propertyValueStore.removeAll();
        this.panel.searchTabPanel.propertyValueStore.load();
    },

    getPropertyTypeByPropertyId: function(propertyId)
    {
        var propertyType = 0;
        this.panel.searchTabPanel.propertyStore.each(function(rec){
            if(rec.get('propertyId') == propertyId)
                propertyType = rec.get('propertyType');
        });
        return propertyType;
    },

    setConditionOfProrerty: function(combo, record, index)
    {
        var changeRecord = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        if(record.data.id != 'NULL')
        {
            this.panel.searchTabPanel.valueCombobox.setDisabled(false);
            if(changeRecord.get('propertyValue') == '')
            changeRecord.set('propertyValue', '<значение>');
        }
        else
        {
            this.panel.searchTabPanel.valueCombobox.clearValue();
            this.panel.searchTabPanel.valueCombobox.setDisabled(true);
            changeRecord.set('propertyValue', '');
        }
        changeRecord.set('propertyConditionValue', record.data.id);
        changeRecord.set('propertyCondition', record.data.state);
        changeRecord.commit();
        this.switchSearchIfPossible();
    },

    setValueOfProrerty: function(combo, record, index)
    {
        //alert(record.data.propertyValue);

        var changeRecord = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        changeRecord.set('propertyValue', record.data.propertyValue);
        var propertyId = this.panel.searchTabPanel.propertyCombobox.getValue();
        var propertyType = this.getPropertyTypeByPropertyId(propertyId);
        combo.setValue(record.data.propertyValue);
        if(propertyType != '5')
            changeRecord.set('propertyValueForQuery', record.data.propertyValue);
        else
            changeRecord.set('propertyValueForQuery', record.data.propertyValueId);
        //alert(changeRecord.get('propertyValueForQuery'));
        changeRecord.commit();
        this.switchSearchIfPossible();
    },

    setValueOfProrertyOnEnter: function(value)
    {
        if(this.panel.searchTabPanel.valueCombobox.validate()) {
            var changeRecord = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
            changeRecord.set('propertyValue', value);
            changeRecord.set('propertyValueForQuery', value);
            changeRecord.commit();
            this.switchSearchIfPossible();
        } else
            Ext.Msg.alert('Предупреждение','Неверное значение характеристики!', function(){this.panel.searchTabPanel.valueCombobox.focus(true);},this);
    },

    featureQueryConditionSelectedChanged: function(sm)
    {
        if(sm.getCount() > 1) {
            this.panel.searchTabPanel.constructorQueryPanel.setDisabled(true);
            this.panel.searchTabPanel.removeQueryConditionButton.setDisabled(true);
        } else {
            this.panel.searchTabPanel.constructorQueryPanel.setDisabled(false);
            if(this.panel.searchTabPanel.featureQueryStore.getCount() >  1)
                this.panel.searchTabPanel.removeQueryConditionButton.setDisabled(false);
        }
    },

    featureQueryConditionRowSelect: function(sm, index, record)
    {
        if(sm.getCount() == 1)
        {
            this.panel.searchTabPanel.propertyCombobox.setValue(record.data.propertyId);
            this.setConditionStates(this.getPropertyTypeByPropertyId(record.data.propertyId));
            if(record.data.propertyName != '<характеристика>') {
                this.loadPropertyValues();
                this.panel.searchTabPanel.propertyCombobox.getEl().dom.value = record.data.propertyName.replace(/(?:<span.*?>)/ig,'').replace(/(?:<\/span>)/ig,'');
            }
            if(record.data.propertyCondition != '<условие>')
                this.panel.searchTabPanel.conditionCombobox.setValue(record.data.propertyCondition);
            else
                this.panel.searchTabPanel.conditionCombobox.clearValue();

            if(record.data.propertyValue != '<значение>')
                this.panel.searchTabPanel.valueCombobox.setValue(record.data.propertyValue);
            else
                this.panel.searchTabPanel.valueCombobox.clearValue();
            if(record.data.propertyConditionNOT != '')
                this.panel.searchTabPanel.operationNotCheckbox.setValue(true);
            else
                this.panel.searchTabPanel.operationNotCheckbox.setValue(false);

            if(index == this.panel.searchTabPanel.featureQueryStore.getCount()-1) {
                this.panel.searchTabPanel.relationQueryANDRadio.disable();
                this.panel.searchTabPanel.relationQueryORRadio.disable();
            } else {
                this.panel.searchTabPanel.relationQueryANDRadio.enable();
                this.panel.searchTabPanel.relationQueryORRadio.enable();
                if (!(record.data.queryRelation)) {
                    this.panel.searchTabPanel.relationQueryANDRadio.setValue(true);
                } else {
                    if(record.data.queryRelation != 'И')
                        this.panel.searchTabPanel.relationQueryORRadio.setValue(true);
                    else
                        this.panel.searchTabPanel.relationQueryANDRadio.setValue(true);
                }
            }
        }
            var allRecords = this.panel.searchTabPanel.featureQueryStore.getRange();
            var selectedRecords = this.panel.searchTabPanel.featureQuerySelectionModel.getSelections();
            var group = [];
            for(var i = 0; i < allRecords.length; i++)
                for(var j = 0; j < selectedRecords.length; j++) {
                    if(selectedRecords[j] == allRecords[i])
                        group.push(i);
                }
            if(this.isPossibleGroupping(group))
                this.panel.searchTabPanel.groupQueryConditionButton.setDisabled(false);
            else
                this.panel.searchTabPanel.groupQueryConditionButton.setDisabled(true);
            if(this.isPossipleUngropping(group) != -1) {
                this.panel.searchTabPanel.groupQueryConditionButton.setDisabled(true);
                this.panel.searchTabPanel.ungroupQueryConditionButton.setDisabled(false);
            }
            else
                this.panel.searchTabPanel.ungroupQueryConditionButton.setDisabled(true);
    },

    addQueryCondition: function()
    {
        var record = new Ext.data.Record({});
        record.set('propertyName', '<характеристика>');
        record.set('propertyCondition', '<условие>');
        record.set('propertyValue', '<значение>');
        record.set('groupStart', '');
        record.set('groupEnd', '');
        record.set('query', '');
        record.commit();
        this.panel.searchTabPanel.featureQueryStore.add(record);
        var countRecords = this.panel.searchTabPanel.featureQueryStore.getCount();
        if( countRecords > 1) {
            var prevRecord = this.panel.searchTabPanel.featureQueryStore.getAt(countRecords-2);
            if(!(prevRecord.get('queryRelation'))) {
                if(this.panel.searchTabPanel.relationQueryANDRadio.checked)
                    prevRecord.set('queryRelation','И');
                if(this.panel.searchTabPanel.relationQueryORRadio.checked)
                    prevRecord.set('queryRelation','ИЛИ');
                prevRecord.commit();
            }
        }
        this.panel.searchTabPanel.featureQuerySelectionModel.selectLastRow();
        this.panel.searchTabPanel.operationNotCheckbox.setValue(false);
        if(this.panel.searchTabPanel.featureQueryStore.getCount() >  1)
            this.panel.searchTabPanel.removeQueryConditionButton.setDisabled(false);
        this.switchSearchIfPossible();
    },

    removeQueryCondition: function()
    {
        var allRecords = this.panel.searchTabPanel.featureQueryStore.getRange();
        var selectedRecords = this.panel.searchTabPanel.featureQuerySelectionModel.getSelections();
        var group = this.createGroup(allRecords,selectedRecords);
        this.changeGroups(group);
        if(this.panel.searchTabPanel.featureQueryStore.getCount() == 1)
            this.panel.searchTabPanel.ungroupQueryConditionButton.setDisabled(true);
        if(this.panel.searchTabPanel.featureQueryStore.getCount() <=  1)
            this.panel.searchTabPanel.removeQueryConditionButton.setDisabled(true);
        this.panel.searchTabPanel.featureQuerySelectionModel.selectLastRow();
        var record = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        if(record) {
            record.set('queryRelation','');
            if(record.get('propertyConditionNOT') != '')
                this.panel.searchTabPanel.operationNotCheckbox.setValue(true);
            else
                this.panel.searchTabPanel.operationNotCheckbox.setValue(false);
            record.commit();
        }
        this.switchSearchIfPossible();
    },

    groupQueryCondition: function()
    {
        this.panel.searchTabPanel.groupQueryConditionButton.setDisabled(true);
        this.panel.searchTabPanel.ungroupQueryConditionButton.setDisabled(false);
        var allRecords = this.panel.searchTabPanel.featureQueryStore.getRange();
        var selectedRecords = this.panel.searchTabPanel.featureQuerySelectionModel.getSelections();
        var group = this.createGroup(allRecords, selectedRecords)
        this.groups.push(group);
        this.addBrackets(group);
    },

    ungroupQueryCondition: function()
    {
        this.panel.searchTabPanel.ungroupQueryConditionButton.setDisabled(true);
        this.panel.searchTabPanel.groupQueryConditionButton.setDisabled(false);
        var allRecords = this.panel.searchTabPanel.featureQueryStore.getRange();
        var selectedRecords = this.panel.searchTabPanel.featureQuerySelectionModel.getSelections();
        var group = this.createGroup(allRecords, selectedRecords);
        var indexOfRemoving = this.isPossipleUngropping(group);
        if( indexOfRemoving != -1)
            this.groups.splice(indexOfRemoving,1);
        this.removeBrackets(group);
    },

    setNotConditionOfProrerty: function(checkbox, checked)
    {
        var record = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        if (checked)
            record.set('propertyConditionNOT','НЕ');
        else
            record.set('propertyConditionNOT','');
        record.commit();
    },

    isPossibleGroupping: function(newGroup)
    {
        var leftNewGroup = newGroup.first();
        var rightNewGroup = newGroup.last();
        var result = false;
        var results = [];
        if (leftNewGroup != rightNewGroup)
        {
            if(this.groups.length > 0)
            {
                for(var i = 0; i < this.groups.length; i++)
                {
                    var existGroup = this.groups[i];
                    var leftExistGroup = existGroup.first();
                    var rightExistGroup = existGroup.last();
                    if((leftNewGroup > rightExistGroup)||(rightNewGroup < leftExistGroup)||((leftNewGroup > leftExistGroup)&&(rightNewGroup < rightExistGroup)))
                        results.push(true);
                    else
                        results.push(this.compareBorders(leftNewGroup, rightNewGroup, leftExistGroup, rightExistGroup));

                }
                var chisloTrue = 0;
                for(var j = 0; j < results.length; j++)
                    if(results[j])
                        chisloTrue++;
                if(chisloTrue == this.groups.length)
                    result = true;
            }
            else
                result = true;
        }
        else
            result = false;
        return result;
    },

    isPossipleUngropping: function(group)
    {
        var index = -1;
        for (var i = 0; i < this.groups.length; i++ )
            if(this.compareArray(group, this.groups[i]))
            {
                index = i;
                break;
            }
        return index;
    },

    compareBorders: function(leftNewGroup, rightNewGroup, leftExistGroup, rightExistGroup)
    {
         var result = false;
         if(leftNewGroup < leftExistGroup)
            {
                if(rightNewGroup >= rightExistGroup)
                    result = true;
                else
                    result = false;
            }
            if(leftNewGroup == leftExistGroup)
            {
                if(rightNewGroup >= rightExistGroup)
                    result = true;
                else
                    result = false;
            }
            if((leftNewGroup > leftExistGroup)&&(leftNewGroup < rightExistGroup))
            {
                if(rightNewGroup == rightExistGroup)
                    result = true;
                else
                    result = false;
            }
            if(leftNewGroup == rightExistGroup)
            {
                result = false;
            }
            if(leftNewGroup > rightExistGroup)
            {
                result = true;
            }
         return result;
    },

    createGroup: function(allRecords, grouppedRecords)
    {
        var groupElement = [];
        for(var i = 0; i < allRecords.length; i++)
            for(var j = 0; j < grouppedRecords.length; j++)
            {
                if(grouppedRecords[j] == allRecords[i])
                    groupElement.push(i);
            }
        return groupElement;
    },

    removeGroup: function(group)
    {
        var index = -1;
        for (var i = 0; i < this.groups.length; i++ )
            if(this.compareArray(group, this.groups[i]))
            {
                index = i-1;
                break;
            }
        return index;
    },

    changeGroups: function(removeGroup)
    {
        var removeIndex = removeGroup.first();
        for(var i = 0; i < this.groups.length; i++)
        {
            var group = this.groups[i];
            for(var j = 0; j < group.length; j++)
            {
               if(group[j] == removeIndex)
                    group.splice(j,1);
            }
            for(var k = 0; k < group.length; k++)
            {
                if(group[k] > removeIndex)
                group[k] = group[k]-1;
            }
            if(group.length == 0 || group.length == 1)
                this.groups[i] = null;
        }
        this.groups = this.groups.compact();
        var record = this.panel.searchTabPanel.featureQueryStore.getAt(removeIndex);
        this.panel.searchTabPanel.featureQueryStore.remove(record);
        this.panel.searchTabPanel.featureQueryStore.each(function(record)
            {
            record.set('groupStart','');
            record.set('groupEnd', '');
            record.commit();
            }
        );
        for(var i = 0; i < this.groups.length; i++)
        {
            this.addBrackets(this.groups[i]);
        }
    },

    addBrackets: function(group)
    {
        var startGroupRecord = this.panel.searchTabPanel.featureQueryStore.getAt(group.first());
        var endGroupRecord = this.panel.searchTabPanel.featureQueryStore.getAt(group.last());
        var valueStart = startGroupRecord.get('groupStart');
        var valueEnd = endGroupRecord.get('groupEnd');
        startGroupRecord.set('groupStart', valueStart+'( ');
        endGroupRecord.set('groupEnd', valueEnd+' )');
        startGroupRecord.commit();
        endGroupRecord.commit();
    },

    removeBrackets: function(group)
    {
        var startGroupRecord = this.panel.searchTabPanel.featureQueryStore.getAt(group.first());
        var endGroupRecord = this.panel.searchTabPanel.featureQueryStore.getAt(group.last());
        var valueStart = startGroupRecord.get('groupStart');
        var valueEnd = endGroupRecord.get('groupEnd');
        startGroupRecord.set('groupStart', valueStart.substring(2,valueStart.length));
        endGroupRecord.set('groupEnd', valueEnd.substring(0,valueEnd.length-2));
        startGroupRecord.commit();
        endGroupRecord.commit();
    },

    setConditionStates: function(propertyType)
    {
        var record = this.panel.searchTabPanel.featureQuerySelectionModel.getSelected();
        switch(propertyType)
        {
            case '2':
                this.panel.searchTabPanel.conditionFieldSet.setTitle('Условие и значение (ГГГГ-ММ-ДД)');
                this.panel.searchTabPanel.conditionStates.loadData([['LIKE','равно'],['>','больше'],['<','меньше'],['>=','больше или равно'],['<=','меньше или равно'],['<>','не равно'],['NULL','нулевое']], false);
                this.panel.searchTabPanel.valueCombobox.setEditable(true);
                this.panel.searchTabPanel.valueCombobox.validator = function(text)
                    {
                        var isValid = false;
                        //var regData = /^\d{2}([-.])\d{2}\1\d{2}$/;
                        var regData = /^\d{4}-\d{2}-\d{2}$/;
                        isValid = regData.test(text) ? true : false
                        if(isValid) {
                            var result = text.split('-');
                            if(result[1] == '02') {
                                if((result[0] <= '29')&&(result[0] > '00'))
                                    isValid = true;
                                else
                                    isValid = false;
                            } else {
                                if(result[1] <= '12') {
                                    if((result[1] == '04')||(result[1] == '06')||(result[1] == '08')||(result[1] == '09')||(result[1] == '11')) {
                                        if((result[0] <= '30')&&(result[0] > '00'))
                                            isValid = true;
                                        else
                                            isValid = false;
                                    } else {
                                        if((result[0] <= '31')&&(result[0] > '00'))
                                            isValid = true;
                                        else
                                            isValid = false;
                                    }
                                } else
                                    isValid = false;
                            }
                        }
                        if(isValid) {
                            record.set('propertyValue', text);
                            record.set('propertyValueForQuery', text);
                        } else {
                            record.set('propertyValue', '<значение>');
                            record.set('propertyValueForQuery', '');
                        }
                        record.commit();
                        return isValid;
                    };
                break;

            case '3':

                this.panel.searchTabPanel.conditionStates.loadData([['=','равно'],['>','больше'],['<','меньше'],['>=','больше или равно'],['<=','меньше или равно'],['<>','не равно'],['NULL','нулевое']], false);
                this.panel.searchTabPanel.valueCombobox.setEditable(true);
                this.panel.searchTabPanel.valueCombobox.validator = function(text) {
                        var isValid = false;
                        var regData = /^-?\d+$/;
                        isValid = regData.test(text) ? true : false
                        if(isValid) {
                            record.set('propertyValue', text);
                            record.set('propertyValueForQuery', text);
                        } else {
                            record.set('propertyValue', '<значение>');
                            record.set('propertyValueForQuery', '');
                        }
                        record.commit();
                        return isValid;
                    };
                break;
            case '4':
                this.panel.searchTabPanel.conditionStates.loadData([['=','равно'],['>','больше'],['<','меньше'],['>=','больше или равно'],['<=','меньше или равно'],['<>','не равно'],['NULL','нулевое']], false);
                this.panel.searchTabPanel.valueCombobox.setEditable(true);
                this.panel.searchTabPanel.valueCombobox.validator = function(text) {
                        var isValidFloat = false;
                        var isValidInt = false;
                        var regDataFloat = /^-?\d+[\.]?\d+$/;//|\,
                        var regDataInt = /^-?\d+$/;
                        isValidFloat = regDataFloat.test(text) ? true : false
                        isValidInt = regDataInt.test(text) ? true : false
                        //return isValid;
                        if(isValidFloat || isValidInt) {
                            record.set('propertyValue', text);
                            record.set('propertyValueForQuery', text);
                            record.commit();
                            return true;
                        } else {
                            record.set('propertyValue', '<значение>');
                            record.set('propertyValueForQuery', '');
                            record.commit();
                            return false;
                        }
                    };
                break;

            case '5':
                this.panel.searchTabPanel.conditionStates.loadData([['=','равно'],['<>','не равно'],['NULL','нулевое']], false);
                this.panel.searchTabPanel.valueCombobox.setEditable(false);
                this.panel.searchTabPanel.valueCombobox.validator = function(text) {
                        return true;
                };
                break;
            default:
                this.panel.searchTabPanel.conditionFieldSet.setTitle('Условие и значение');
                this.panel.searchTabPanel.conditionStates.loadData([['=','равно'],['<>','не равно'],['NULL','нулевое'],['LIKE','содержит']], false);
                this.panel.searchTabPanel.valueCombobox.setEditable(true);
                this.panel.searchTabPanel.valueCombobox.validator = function(text) {
                        record.set('propertyValue', text);
                        record.set('propertyValueForQuery', text);
                        record.commit();
                        return true;
                };
                this.switchSearchIfPossible();
        }
    },

    validateValueofProperty: function(text, propertyType)
    {
        var isValid = false;
        return isValid;
    },

    compareArray: function(array1, array2)
    {
        var result = false;
        if((array1.first() == array2.first())&&(array1.last() == array2.last()))
            result = true;
        return result;
    },

    clearExtendedSearchFilter: function()
    {
        if(this.panel.searchTabPanel.featureQueryStore.getCount() > 0)
        {
            this.panel.searchTabPanel.operationNotCheckbox.setValue(false);
            this.panel.searchTabPanel.relationQueryANDRadio.setValue(true);
            this.panel.searchTabPanel.featureQueryStore.removeAll();
            this.panel.searchTabPanel.conditionStates.removeAll();
            this.panel.searchTabPanel.propertyValueStore.removeAll();
            this.panel.searchTabPanel.propertyStore.removeAll();
        }
    },

    extendedSearch: function()
    {
        var query = '';
        if(this.isPossibleSearching())
        {
            var lastRecord = this.panel.searchTabPanel.featureQueryStore.getAt(this.panel.searchTabPanel.featureQueryStore.getCount()-1);
            lastRecord.set('queryRelation', '');
            lastRecord.commit();
            this.panel.searchTabPanel.featureQueryStore.each(function(record){
                query = query + record.get('groupStart') + '( PROP_'+record.get('propertyId');
                var condition = '';
                if(record.get('propertyCondition') != 'нулевое')
                    condition = record.get('propertyConditionValue')+" '"+ record.get('propertyValueForQuery')+"'";
                else
                    condition = ' is null';
                if(record.get('propertyConditionNOT') != '')
                    query = 'NOT' +'('+query+' '+condition+'))';
                else
                    query = query +' '+condition+')';
                query = query + record.get('groupEnd');
                if(record.get('queryRelation') == 'И')
                    query = query + 'AND';
                if(record.get('queryRelation') == 'ИЛИ')
                    query = query + 'OR';
            });
        }
        //this.panel.test.setValue(query);
    },

    isPossibleSearching: function()
    {
        var index = 0;
        var result = true;
        this.panel.searchTabPanel.featureQueryStore.each(function(record){
            if(record.get('propertyName') == '<характеристика>')
            {
                //Ext.WgsMsg.alert('Предупреждение', 'Необходимо выбрать характеристику!');
                //this.panel.searchTabPanel.featureQuerySelectionModel.selectRow(index, false);
                result = false;
                return result;
            }
            if(record.get('propertyCondition') == '<условие>')
            {
                //Ext.WgsMsg.alert('Предупреждение', 'Необходимо выбрать условие!');
                //this.panel.searchTabPanel.featureQuerySelectionModel.selectRow(index, false);
                result = false;
                return result;
            }
            if(record.get('propertyValue') == '<значение>')
            {
                //Ext.WgsMsg.alert('Предупреждение', 'Необходимо установить значение!');
                //this.panel.searchTabPanel.featureQuerySelectionModel.selectRow(index, false);
                result = false;
                return result;
            }
            index++;
        }, this);
    return result;
    },

    switchSearchIfPossible: function()
    {
        if(this.isPossibleSearching()) {
            this.panel.searchOnMapButton.setText('Найти на карте');
            this.panel.searchInCardsButton.setText('Найти в карточках');
        }
        else {
            this.panel.searchOnMapButton.setText('Все объекты');
            this.panel.searchInCardsButton.setText('Все карточки');
        }
    }
}

Map.Search.SimpleSearchPanel = Ext.extend(Ext.form.WgsFormPanel, {
    propertyCollection: {},
    propertyGroupCollection: {},

    constructor: function(config)
    {
        this.propertyComboBox = new Ext.ux.form.LovCombo({
            tpl: '<tpl for=".">'
                +'<div class="{class} x-combo-list-item">'
                +'<img src="' + Ext.BLANK_IMAGE_URL + '" '
                +'class="ux-lovcombo-icon ux-lovcombo-icon-'
                +'{[values.checked?"checked":"unchecked"]}">'
                +'<div class="ux-lovcombo-item-text">{propertyName}</div>'
                +'</div>'
                +'</tpl>',
            editable: false,
            width: 560,
            maxHeight: 400,
            emptyText: 'Выбрать...',
            loadingText: 'Загрузка...',
            store: config.propertyStore,
            displayField: 'propertyName',
            valueField: 'propertyId',
            triggerAction: 'all',
            //fieldLabel: 'Характеристика:',
            anchor: '100%',
            forceSelection: true
        });

        this.selectedFeatureTypesHidden = new Ext.form.Hidden({
            //name: 'featureTypes'
        });

        Map.Search.SimpleSearchPanel.superclass.constructor.call(this, {
            method : 'POST',
            url: '/objectadmin/search/getresult',
            title: 'Простой поиск',
            autoScroll: true,
			listeners:{'show':function(){this.propertyComboBox.store.load()}},
            bodyStyle: 'padding: 5px',
            deferredRender: false,
            tbar:  [
                    '',
                    {xtype: 'tbtext', text: 'Свойство:'},
                    this.propertyComboBox
                   ],
            items: [this.selectedFeatureTypesHidden]
        });
    }
});

Map.Search.SearchTabPanel = Ext.extend(Ext.Panel, {
    constructor: function()
    {
        this.featureLayerStore = new Ext.data.WgsJsonStore({
            autoLoad: true,
            url: '/objectadmin/search/getfeaturelayerlist',
            fields: ['featureLayerName', 'featureLayerId']
        });

        this.featureTypeStore = new Ext.data.WgsJsonStore({
            autoLoad: true,
            url: '/objectadmin/search/getfeaturetypelist',
            fields: ['featureTypeName', 'featureTypeId']
        });

        this.propertyStore = new Ext.data.WgsJsonStore({
            reader: new Ext.data.JsonReader({}, [
                {name: 'propertyId'},
                {name: 'propertyName'},
                {name: 'propertyGroupName'},
                {name: 'propertyType'},
                {name: 'class'}
            ]),
            autoLoad: true,
            url: '/objectadmin/search/getpropertylist',
            fields: ['propertyName', 'propertyId', 'propertyGroupName', 'propertyType', 'class']
        });

        this.simpleSearchPanel = new Map.Search.SimpleSearchPanel({propertyStore: this.propertyStore});

        this.featureLayerComboBox = new Ext.ux.form.LovCombo({
            disabled : false,
            editable : false,
            anchor : '100%',
            emptyText : 'Искать по всем',
            loadingText : 'Загрузка...',
            store : this.featureLayerStore,
            hideLabel: true,
            labelSeparator: '',
            displayField : 'featureLayerName',
            valueField : 'featureLayerId',
            triggerAction : 'all',
            forceSelection: true
        });

        this.featureTypeComboBox = new Ext.ux.form.LovCombo({
            disabled : false,
            editable : false,
            anchor : '100%',
            emptyText : 'Искать по всем',
            loadingText : 'Загрузка...',
            store : this.featureTypeStore,
            hideLabel: true,
            labelSeparator: '',
            displayField : 'featureTypeName',
            valueField : 'featureTypeId',
            triggerAction : 'all',
            forceSelection: true
        });

        /*Расширенный поиск*/

        this.propertyValueStore = new Ext.data.WgsJsonStore({
            reader: new Ext.data.JsonReader({}, [
                {
                    name: 'propertyValue',
                    name: 'propertyValueId'
                }
            ]),
            autoLoad: false,
            url: '/objectadmin/search/getvaluesbypropertyid',
            fields: ['propertyValue', 'propertyValueId']
        });

        this.cancelButton = new Ext.Button({
            text : 'Отмена',
            handler : function() {
                this.propertyStore.load();
            },
            scope : this
        });

        this.addQueryConditionButton = new Ext.Button({
            iconCls: 'icon-add',
            text: 'Добавить'
        });

        this.removeQueryConditionButton = new Ext.Button({
            iconCls: 'icon-delete',
            text: 'Удалить',
            disabled: true
        });

        this.groupQueryConditionButton = new Ext.Button({
            iconCls: 'icon-group',
            text: 'Сгруппировать'
        });

        this.ungroupQueryConditionButton = new Ext.Button({
            iconCls: 'icon-ungroup',
            text: 'Разгруппировать'
        });

        this.propertyCombobox = new Ext.form.WgsComboBox({
            store: this.propertyStore,
            editable: false,
            autoLoad: false,
            hideLabel: true,
            displayField: 'propertyName',
            valueField: 'propertyId',
            loadingText: 'Загрузка...',
            labelSeparator: '',
            triggerAction: 'all'//,
//            listClass: 'x-combo-list-small'
        });

        this.featureQueryStore = new Ext.data.SimpleStore({
             fields: [
                'propertyName',
                'propertyConditionNOT',
                'propertyCondition',
                'propertyValue',
                'query',
                'propertyId',
                'queryRelation',
                'groupStart',
                'groupEnd',
                'propertyValueForQuery',
                'propertyConditionValue']
        });

        this.featureQuerySelectionModel = new Ext.grid.RowSelectionModel({});

        this.featureQueryGrid = new Ext.grid.GridPanel({
            //height: 355,
            region: 'center',
            frame: true,
            //disabled: true,
            maskDisabled: false,
            //title: 'Условия поиска',
            store : this.featureQueryStore,
            cm : new Ext.grid.ColumnModel([
                {id: 'groupStart', header: '', hideable: false, sortable: false, dataIndex: 'groupStart', menuDisabled: true, align: 'center', width: 50},
                {id: 'propertyName', header: 'Наименование', hideable: false, sortable: false, dataIndex: 'propertyName', menuDisabled: true, width: 150},
                {id: 'propertyConditionNOT',header: "", hideable: true, sortable: false, dataIndex: 'propertyConditionNOT', menuDisabled: true, align: 'center',width: 40},
                {id: 'propertyCondition',header: 'Условие', hideable: true, sortable: false, dataIndex: 'propertyCondition', menuDisabled: true, align: 'center'},
                {id: 'propertyValue', header: 'Значение', hideable: true, sortable: false, dataIndex: 'propertyValue', menuDisabled: true, align: 'center', width: 100},
                {id: 'groupEnd', header: '', hideable: false, sortable: false, dataIndex: 'groupEnd', menuDisabled: true, align: 'center', width: 50},
                {id: 'queryRelation', header: '', hideable: true, sortable: false, dataIndex: 'queryRelation', menuDisabled: true, align: 'center', width: 40}
            ]),
            sm: this.featureQuerySelectionModel,
            view: new Ext.grid.GridView(),
            /*viewConfig: {
                   forceFit: true
            },*/
            tbar: [ this.addQueryConditionButton,
                '-',this.removeQueryConditionButton,
                '-',this.groupQueryConditionButton,
                '-',this.ungroupQueryConditionButton ],
            //hideHeaders: true,
            autoScroll: true,
            stripeRows: true
        });

        this.operationNotCheckbox = new Ext.form.Checkbox({
            name: 'operationNot',
            anchor: false,
            checked: false,
            hideLabel: true,
            //style: 'margin-top: 0px',
            boxLabel: "НЕ"
        });

        this.conditionStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            reader: new Ext.data.DataReader({},[
            {
                name: 'id',
                name: 'state'
            }]
            )//,
            //data: [['=','равно'],['>','больше'],['<','меньше'],['>=','больше или равно'],['<=','меньше или равно'],['!=','не равно'],['NULL','нулевое'],['LIKE','содержит'] ]
        });

        this.conditionCombobox = new Ext.form.WgsComboBox({
            store: this.conditionStates,
            displayField: 'state',
            valueField: 'id',
            mode: 'local',
            triggerAction: 'all',
            editable: false,
            selectOnFocus: true,
            listClass: 'x-combo-list-small'
        });



        this.valueCombobox = new Ext.form.WgsComboBox({
            store: this.propertyValueStore,
            displayField: 'propertyValue',
            valueField: 'propertyValueId',
            mode: 'local',
            triggerAction: 'all',
            editable: true,
            selectOnFocus: true,
            listClass: 'x-combo-list-small'
        });


        this.conditionFieldSet = new Ext.form.FieldSet(
                           {
                            xtype:'fieldset',
                            title: 'Условие и значение',
                            //height: 55,
                            autoHeight: true,
                            layout: 'column',
                            items:[ {
                                        columnWidth: 0.49,
                                        layout: 'fit',
                                        autoHeight: true,
                                        items: [ this.conditionCombobox ]
                                    },
                                    {
                                        columnWidth: 0.01
                                    },
                                    {
                                        columnWidth: 0.5,
                                        layout: 'fit',
                                        items: [ this.valueCombobox ]
                                    }
                                  ]
                            });

        this.getValuesButton = new Ext.Button({
            text : '...',
            handler : function() {
            },
            scope : this
        });

        this.relationQueryANDRadio = new Ext.form.Radio({
            checked: true,
            boxLabel: 'И',
            listeners:
                {
                    check : function(radio, value)
                    {
                        var selectRecord = this.featureQuerySelectionModel.getSelected();
                        if(selectRecord)
                        {
                            if(value)
                            {
                                this.relationQueryORRadio.setValue(false);
                                selectRecord.set('queryRelation', 'И');
                            }
                            selectRecord.commit();
                        }
                    },
                    scope: this
                }
        });

        this.relationQueryORRadio = new Ext.form.Radio({
            value: 'OR',
            boxLabel: 'ИЛИ',
            listeners:
                {
                    check : function(radio, value)
                    {
                        var selectRecord = this.featureQuerySelectionModel.getSelected();
                        if(selectRecord)
                        {
                            if(value)
                            {
                                this.relationQueryANDRadio.setValue(false);
                                selectRecord.set('queryRelation', 'ИЛИ');
                            }
                            selectRecord.commit();
                        }
                    },
                    scope: this
                }
        });

        this.constructorQueryPanel = new Ext.Panel({
            maskDisabled: false,
            height: 60,
            region: 'south',
            layout: 'column',
            items: [{
                    columnWidth: 0.23,
                    //height: 100,
                    items:[
                    {
                        xtype:'fieldset',
                        title: 'Операция',
                        //height: 55,
                        autoHeight: true,
                        layout: 'column',
                        items: [{
                                    columnWidth: 0.28,
                                    layout: 'fit',
                                    labelWidth: 1,
                                    style: 'margin-top: 2px; margin-bottom: 2px;',
                                    items: [ this.relationQueryANDRadio ]
                                },
                                {
                                    columnWidth: 0.02
                                },
                                {
                                    columnWidth: 0.40,
                                    style: 'margin-top: 2px; margin-bottom: 2px;',
                                    items: [ this.relationQueryORRadio ]
                                },
                                {
                                    columnWidth: 0.01,
                                    width: 3
                                },
                                {
                                    columnWidth: 0.29,
                                    style: 'margin-top: 2px; margin-bottom: 2px;',
                                    items: [ this.operationNotCheckbox ]
                                } ]
                    }
                   ]
                    },
                    {
                        columnWidth: 0.01
                    },
                    {
                     columnWidth: 0.30,
                     items:[
                        {
                        xtype:'fieldset',
                        title: 'Характеристика',
                        autoHeight: true,
                        layout: 'fit',
                        items: [ this.propertyCombobox ]

                        }]
                     },
                     {
                        columnWidth: 0.01
                     },
                    {
                    	columnWidth: 0.45,
                    	items:[this.conditionFieldSet]
                    }

                    ]
        });

		var self = this
		var _c = this.propertyCombobox
        this.extendedSearchPanel = new Ext.Panel({
            title: 'Расширенный поиск',
            frame: true,
            layout: 'border',
            deferredRender: false,
			listeners:{'show':function(){if(self.constructorQueryPanel && _c) _c.store.load()}},
           
            items:[ this.featureQueryGrid, this.constructorQueryPanel ]
        });
        /*Все конец*/

        this.allSearchTabPanel = new Ext.TabPanel({
                        activeTab: 0,
                        deferredRender: false,
                        items: [this.simpleSearchPanel, this.extendedSearchPanel]
                });

        this.featureLayerPanel = new Ext.Panel({
            columnWidth: 0.495,
            layout: 'fit',
            items: [
                new Ext.form.FieldSet({
                    title: 'Слои',
                    autoHeight: true,
                    items: [ this.featureLayerComboBox ]
                })
            ]
        });

        this.splitPanel = new Ext.Panel({
            columnWidth: 0.01
        });

        this.featureTypePanel = new Ext.Panel({
            columnWidth: 0.495,
            layout: 'fit',
            items: [
                new Ext.form.FieldSet({
                    title: 'Описания',
                    autoHeight: true,
                    items: [ this.featureTypeComboBox ]
                })
            ]
        });

        Map.Search.SearchTabPanel.superclass.constructor.call(this, {
            title: 'Поиск',
            frame: true,
            border: false,
            //deferredRender: false,
            layout: 'border',
            items: [{
                region: 'north',
                height: 70,
                layout: 'column',
                items: [
                    this.featureLayerPanel,
                    this.splitPanel,
                    this.featureTypePanel
                ]
            },{
                region: 'center',
                //height: 475,
                layout: 'fit',
                items: [this.allSearchTabPanel]
            }]
        });
    }
});

Map.Search.ResultTabPanel = Ext.extend(Ext.Panel, {
    constructor: function(config)
    {
        Ext.apply(this, config);
        Ext.QuickTips.init();

        var actions = [];

        if (WGS.PRIVILEGE.FLOORPLAN_VIEW && WGS.MODULES.WGS_FLOORPLAN) {
			actions.push({
                iconCls: 'floorplan',
                tooltip: 'Показать поэтажный план'
            });
        }

		actions.push({
            iconCls: 'viewcard',
            tooltip: 'Показать карточку',
			title: 'Показать карточку',
			showTooltip:true
        });

        if (WGS.PRIVILEGE.CARD_BIND && WGS.MODULES.WGS_BIND) {
        	actions.push({
                iconCls: 'putcard',
                tooltip: 'Вложить карточку'
            },{
                iconCls: 'bindcard',
                tooltip: 'Привязать карточку',
                hideIndex: 'hidebindcard'
            });
        }

        if (WGS.MODULES.WGS_FEATUREDOC) {
			actions.push({
	            iconCls: 'featuredoc',
	            tooltip: 'Просмотр электронных документов',
	            hideIndex: 'hidefeaturedoc'
	        });
        }

        this.rowActions = new Ext.ux.grid.RowActions({
        	actions: actions,
			header: "Карточка"

        });

        this.rowActions.on({
            action: function(grid, record, action, row, col) {
                if (action == 'featuredoc') {
                	if (record.data.entityId) {
                		Map.Featuredoc.Instance(this.mapViewer).openFeaturedoc(record.data.entityId, [record.data.entityId]);
                	}
                } else
                if (action == 'floorplan') {
                	if (record.data.featureId) {
                		Map.Floorplan.Instance().openFloorplanWindow(record.data.featureId, record.data.featureId);
                	}
                } else
                if (action == 'viewcard') {
                    if (record.data.objectId) {
                        Map.Card.Instance(this.mapViewer).open({
                            objectId: record.data.objectId,
                            callback: function() {
                                this.resultStore.reload();
                            },
                            scope: this
                        });
                    }
                    else {
                        Map.Card.Instance(this.mapViewer).open({
                            featureId: record.data.featureId,
                            callback: function() {
                                this.resultStore.reload();
                            },
                            scope: this
                        });
                    }
                } else
                if (action == 'bindcard') {
                    var objectId = record.data.objectId;
                    var win = new Ext.WgsShimWindow({
                        width: 750,
                        maximizable: true,
                        resizable: true,
                        layout: 'fit',
                        title: 'Привязка карточки'
                    });
                    var mapViewer = new Map.Viewer({
                        viewerType: 'ajax',
                        simpleMode: true,
                        render: function(viewerPanel) {
                            viewerPanel.setWidth(740);
                            viewerPanel.setHeight(480);
                            win.add(viewerPanel);
                            win.show();
                        },
                        load: function(api, lib, commands) {
                            api.Map.ObserveOnMapLoaded(function(){
                                lib.getSelectedFeatures(function(featureLayers){
                                    if (featureLayers != -1) {
                                        featureId = featureLayers[0].features[0].featureId;
                                        if (featureLayers[0].features.length > 1 || featureLayers.length > 1)
                                            Ext.WgsMsg.show({
                                                title: 'Предупреждение',
                                                msg: 'Необходимо выбрать один объект',
                                                buttons: Ext.MessageBox.OK,
                                                icon: Ext.MessageBox.WARNING,
                                                fn: function() {
                                                    api.Map.ClearSelection();
                                                }
                                            });
                                        else
                                            Ext.WgsMsg.show({
                                                title: 'Подтверждение',
                                                msg: 'Вы уверены, что хотите привязать выбранный объект?',
                                                buttons: Ext.Msg.YESNO,
                                                fn: function(buttonId){
                                                    if (buttonId == 'yes') {
                                                        new Ext.data.WgsConnection().request({
                                                            url: '/map/bind/bindfeature',
                                                            method: 'POST',
                                                            params: {objectId: objectId, featureId: featureId},
                                                            success: function(){
                                                                this.resultStore.reload();
                                                                win.close();
                                                            },
                                                            failure: function() {
                                                                api.Map.ClearSelection();
                                                            },
                                                            scope: this
                                                        });
                                                    } else {
                                                        api.Map.ClearSelection();
                                                    }
                                                },
                                                icon: Ext.MessageBox.QUESTION,
                                                scope: this
                                            });
                                    }
                                }, this);
                            }, this);
                        },
                        scope: this
                    });
                } else
                if (action == 'putcard') {
                    var win = new Ext.WgsShimWindow({
                        width: 750,
                        maximizable: true,
                        resizable: true,
                        layout: 'fit',
                        title: 'Вложение карточки'
                    });
                    var mapViewer = new Map.Viewer({
                        viewerType: 'ajax',
                        simpleMode: true,
                        render: function(viewerPanel) {
                            viewerPanel.setWidth(740);
                            viewerPanel.setHeight(480);
                            win.add(viewerPanel);
                            win.show();
                        },
                        load: function(api, lib, commands) {
                            win.maximize()
                            api.Map.ObserveOnMapLoaded(function(){
                                var bindSearch = Map.Bind.Instance(mapViewer);
                                bindSearch.open({
                                    mode: Map.Bind.CARD_OBJECT_MODE,
                                    cardId: record.data.objectId,
                                    cardName: record.data.name,
                                    featureTypeName: record.data.type,
                                    featureTypeId: record.data.featureTypeId
                                });
                            }, null, true);
                        },
                        scope: this
                    });
                }
            },
            scope: this
        });

        this.resultStore = new Ext.data.WgsGroupingStore({
            autoLoad: false,
            reader: new Ext.data.JsonReader({
                totalProperty:'totalCount',
                root:'rows'
            },[
                {name: 'name'},
                {name: 'type'},
                {name: 'layer'},
                {name: 'entityId'},
                {name: 'featureId'},
                {name: 'objectId'},
                {name: 'featureTypeId'},
                {name: 'hidebindcard'},
                {name: 'hidefeaturedoc'}
            ]),
            url: '/objectadmin/search/getresult',
            fields: ['name', 'type', 'layer', 'entityId', 'featureId', 'objectId', 'featureTypeId', 'hidebindcard', 'hidefeaturedoc'],
            sortInfo: {field: 'name', direction: "ASC"},
            remoteGroup: true
        });

        this.resultCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

        this.docCheckBox = new Ext.form.Checkbox;

        this.docCheckBox.on('check', function(){
        	this.resultStore.baseParams['docSearch'] = this.docCheckBox.getValue();
	        this.loadMask = new Ext.LoadMask(this.ownerCt.ownerCt.getEl(),{msg: 'Поиск...'});
	        this.loadMask.show();
        	this.resultStore.reload({
        		callback: function(){
        			this.loadMask.hide();
        		},
        		scope: this
        	});
        }, this);

        this.resultGrid = new Ext.grid.GridPanel({
            store: this.resultStore,
            columns:[
                {id: 'entityId', header: "ID объекта", sortable: true, dataIndex: 'entityId', width: 75, hidden: true, align: 'center'},
                {id: 'name', header: "Наименование", sortable: true, dataIndex: 'name', width: 150},
                {id: 'type', header: "Тип", sortable: true, dataIndex: 'type'},
                {id: 'layer', header: "Слой", sortable: true, dataIndex: 'layer'},
                this.rowActions,
                this.resultCheckboxSelectionModel
            ],
            sm: this.resultCheckboxSelectionModel,
            plugins:[this.rowActions],
            view: new Ext.grid.GroupingView({
                forceFit: true,
                groupTextTpl: '{gvalue}'
            }),
            bbar: new Ext.PagingToolbar({
	            store: this.resultStore,
	            displayInfo: true,
	            pageSize: 30,
	            items: ['С документами: ',this.docCheckBox]
	        }),
            stripeRows: true
        });

        Map.Search.ResultTabPanel.superclass.constructor.call(this, {
            title: 'Результат поиска',
            deferredRender: false,
            layout: 'fit',
            items: [this.resultGrid]
        });
    }
});

Map.Search.SearchPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config)
    {
        Ext.apply(this, config);
        this.searchTabPanel = new Map.Search.SearchTabPanel;
        this.resultTabPanel = new Map.Search.ResultTabPanel({mapViewer: this.mapViewer});

        this.tabs = new Ext.TabPanel({
            activeTab: 0,
            items: [this.searchTabPanel, this.resultTabPanel]
        });

        Map.Search.SearchPanel.superclass.constructor.call(this, {
            width: 650,
            height: 650,
            items: this.tabs
        });

        this.exportJournalButton = this.addButton({
        	text: 'Экспортировать в Excel',
        	hidden: true
        });

        this.multipleFillingButton = this.addButton({
            iconCls: 'icon-cards-edit',
            text: 'Редактирование'
        });

        this.previewButton = this.addButton({
            iconCls: 'icon-view-on-map',
            text: 'Отобразить на карте'
        });
		this.previewAllButton = this.addButton({
            iconCls: 'icon-view-on-map',
            text: 'Отобразить все объекты'
        });

        this.searchOnMapButton = this.addButton({
            iconCls: 'icon-objects-search',
            text: 'Все объекты'
        });

        this.searchInCardsButton = this.addButton({
            iconCls: 'icon-cards',
            text: 'Все карточки'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Отмена',
            handler : function() {
                this.hide();
            },
            scope : this
        });
    }
});

Map.Search.ObjectAdminSearchPanel = Ext.extend(Ext.WgsSearthObjectAdminPanel, {
    constructor: function(config)
    {
        Ext.apply(this, config);
        this.searchTabPanel = new Map.Search.SearchTabPanel;
        this.resultTabPanel = new Map.Search.ResultTabPanel({mapViewer: this.mapViewer});

        this.tabs = new Ext.TabPanel({
            activeTab: 0,
            items: [this.searchTabPanel, this.resultTabPanel]
        });

        Map.Search.ObjectAdminSearchPanel.superclass.constructor.call(this, {
            width: 600,
            height: 650,
            closable: true,
            //closeAction: 'hide',
            items: [this.tabs]
        });

        this.multipleFillingButton = this.addButton({
            iconCls: 'icon-cards-edit',
            text: 'Редактирование'
        });

        this.previewButton = this.addButton({
            iconCls: 'icon-view-on-map',
            text: 'Отобразить на карте'
        });
this.previewAllButton = this.addButton({
            iconCls: 'icon-view-on-map',
            text: 'Отобразить все объекты'
        });
        this.searchOnMapButton = this.addButton({
			iconCls: 'icon-objects-search',
            text: 'Все объекты'
        });

        this.searchInCardsButton = this.addButton({
            iconCls: 'icon-cards',
            text: 'Все карточки'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Отмена',
            handler : function() {
                this.hide();
            },
            scope : this
        });
    }
});