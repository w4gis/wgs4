﻿Ext.namespace('Map');

Map.Print = Ext.extend(Ext.util.Observable, {
	constructor: function(mapViewer)
	{
	    this.viewerApi = mapViewer.Api;
	    this.conn = new Ext.data.Connection();
	    this.wgsConn = new Ext.data.WgsConnection();
	    this.panel = new PrintPanel();
	    this.panel.scaleCombobox.setValue('Текущий');
	    this.panel.printSizeCombobox.setValue('4');
	    this.panel.orientationCombobox.setValue('1');
	    this.window = new Ext.WgsServiceWindow({
	        containerPanel: mapViewer.rightPane,
	        width: this.panel.width+12,
	        autoHeight: true,
	        title: 'Печать в PDF',
	        resizable: false
	    });
	    this.window.setInnerPanel(this.panel);
	    this.panel.scaleCombobox.on('select', function(){
	        this.updatePreview();
	    }, this);
	    this.panel.scaleCombobox.on('specialkey', function(field, e){
	        if (e.getKey() == e.ENTER) {
	            this.panel.scaleCombobox.setValue(field.getEl().dom.value);
	            this.updatePreview();
	        }
	    }, this);
	    this.panel.orientationCombobox.on('select', function(){
	        this.updatePreview();
	    }, this);
	    this.panel.legendCheckbox.on('check', function(){
	        this.updatePreview();
	    }, this);
	    this.panel.printSizeCombobox.on('select', function(){
	        this.updatePreview();
	    }, this);
	    this.panel.okButton.on('click', function(){
	        this.print();
	    }, this);
	    this.window.on('hide', function(){
	        this.viewerApi.Map.StopObservingOnMapLoaded(this.updatePreview);
	    }, this);

	    Map.Print.superclass.constructor.call(this);
	},

    getParameters: function()
    {
        var printSizes = {
            5: [0.148, 0.210],
            4: [0.210, 0.297],
            3: [0.297, 0.420],
            2: [0.420, 0.594],
            1: [0.594, 0.840],
            0: [0.840, 1.188]
        };
        var orgScale    = (this.panel.scaleCombobox.getValue()=='Текущий')? parseInt(this.viewerApi.Map.GetScale()): this.panel.scaleCombobox.getValue();
        var printSize   = this.panel.printSizeCombobox.getValue();
        var ratio       = 0.7;
        var pw          = this.panel.previewPanel.width-2;
        var ph          = this.panel.previewPanel.height-2;
        var orientation = this.panel.orientationCombobox.getValue();
        var mw          = Math.round((orientation == 1)? pw: ph * ratio);
        var mh          = Math.round((orientation == 1)? pw * ratio: ph);
        var sessionId   = this.viewerApi.Map.GetSessionId();
        var scale, lw;
        if (mw > mh) {
            scale = orgScale * ((printSizes[printSize][1] / (0.0254 / 96)) / mw);
            lw = mw * (150 / (printSizes[printSize][1] / (0.0254 / 96)));
        }
        else {
            scale = orgScale * ((printSizes[printSize][1] / (0.0254 / 96)) / mh);
            lw = mh * (150 / (printSizes[printSize][0]/ (0.0254 / 96)));
        }
        lw = (this.panel.legendCheckbox.getValue())? lw: 0;
        mw = (this.panel.legendCheckbox.getValue())? (mw-lw): mw;
        return {
            scale: scale,
            centerX: this.viewerApi.Map.GetCenter().X,
            centerY: this.viewerApi.Map.GetCenter().Y,
            mapName: this.viewerApi.Map.GetMapName(),
            printSize: printSize,
            mw: mw,
            mh: mh,
            pw: pw,
            ph: ph,
            lw: lw,
            legendWidth: (lw)?this.viewerApi.Map.GetMapDimension().legendWidth:0,
            orgScale: orgScale,
            orientation: orientation,
            sessionId: sessionId,
			width: this.viewerApi.Map.GetMapDimension().width
        };
		
    },

    getImage: function(parameters)
    {
        this.conn.request({
            url: '/mapguide/mapagent/mapagent.fcgi',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'GET',
            params: {
                MAPNAME: parameters.mapName,
                OPERATION: 'GETVISIBLEMAPEXTENT',
                SEQ: Math.random(),
                SESSION: parameters.sessionId,
                SETDISPLAYDPI: 96,
                SETDISPLAYHEIGHT: parameters.mh,
                SETDISPLAYWIDTH: parameters.mw,
                SETVIEWCENTERX: parameters.centerX,
                SETVIEWCENTERY: parameters.centerY,
                SETVIEWSCALE: parameters.scale,
                VERSION: '1.0.0'
            },
            success: function(response) {
                if (this.previewImage) {
                    this.previewImage.remove();
                }
                var legendDiv = (parameters.lw)? {tag: 'div', style: "margin-left:-1px;margin-top:-1px;border: 1px solid #ccc;float:left;width:"+parameters.lw+"px;height:100%;"}: {tag: 'div'};
                this.previewImage = this.panel.previewPanel.body.createChild({
                    tag: 'div',
                    style: "border: 1px solid #ccc;margin-top:"+Math.round((parameters.ph-parameters.mh)/2)+"px;margin-bottom:auto;margin-left:auto;margin-right:auto;width:"+(parameters.mw+parameters.lw)+"px;height:"+parameters.mh+"px;",
                    children: [legendDiv,{
                        tag: 'div',
                        style: "margin-left:"+parameters.lw+"px;width:"+parameters.mw+"px;height:100%;background-repeat:no-repeat;background-image:url(/mapguide/mapagent/mapagent.fcgi?OPERATION=GETDYNAMICMAPOVERLAYIMAGE&FORMAT=PNG&VERSION=1.0.0&SESSION="+parameters.sessionId+"&MAPNAME="+encodeURIComponent(parameters.mapName)+"&SEQ="+Math.random()+");"
                    }]
                });
                //this.viewerApi.Map.StopObservingOnMapLoaded(this.updatePreview);
                //this.viewerApi.Map.Refresh();
                //this.viewerApi.Map.ObserveOnMapLoaded(this.updatePreview, this);
            },
            scope: this
        });
    },

    updatePreview: function()
    {//alert(1);
        if (this.window && !this.window.hidden) {
            var parameters = this.getParameters()
            this.getImage(parameters);
        }
    },

    print: function()
    {
        var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
        var parameters = this.getParameters();
        this.wgsConn.request({
            url: '/map/print',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: parameters,
            success: function(response) {
                msgWait.hide();
                if (response.responseText) {
                    this.openPdf(response.responseText);
                }
            },
            timeout: 600000,
            scope: this
        });
    },

    openPdf: function(pdfSeq)
    {
        this.docWindow = new Ext.WgsShimWindow({
            title : 'Печать',
            closable : true,
            width : 550,
            height : 450,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/map/print/getpdf/'+pdfSeq+'"></param><EMBED src="/map/print/getpdf/'+pdfSeq+'" width="100%" height="100%" href="/map/print/getpdf/'+pdfSeq+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
        this.docWindow.maximize();
    },

    open: function()
    {
        this.window.addPanel();
        this.window.show();
        this.updatePreview();
        this.viewerApi.Map.ObserveOnMapLoaded(this.updatePreview, this);
        //TODO: on hide stop observing onmaploaded event
    }
});


//
/*
Map.Print = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;
    this.conn = new Ext.data.Connection();
    this.wgsConn = new Ext.data.WgsConnection();
    this.panel = new PrintPanel();
    this.panel.scaleCombobox.setValue('Текущий');
    this.panel.printSizeCombobox.setValue('4');
    this.panel.orientationCombobox.setValue('1');
    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        width: this.panel.width+12,
        autoHeight: true,
        title: 'Печать в PDF',
        resizable: false
    });
    this.window.setInnerPanel(this.panel);
    this.panel.scaleCombobox.on('select', function(){
        this.updatePreview();
    }, this);
    this.panel.scaleCombobox.on('specialkey', function(field, e){
        if (e.getKey() == e.ENTER) {
            this.panel.scaleCombobox.setValue(field.getEl().dom.value);
            this.updatePreview();
        }
    }, this);
    this.panel.orientationCombobox.on('select', function(){
        this.updatePreview();
    }, this);
    this.panel.legendCheckbox.on('check', function(){
        this.updatePreview();
    }, this);
    this.panel.printSizeCombobox.on('select', function(){
        this.updatePreview();
    }, this);
    this.panel.okButton.on('click', function(){
        this.print();
    }, this);
    this.window.on('hide', function(){
        this.viewerApi.Map.StopObservingOnMapLoaded(this.updatePreview);
    }, this);
}
*/
Map.Print.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Print(map);
    }
    return this.instances[map.mapFrame];
}

PrintPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function()
    {
        this.printSizeStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: [['5','A5 (148 x 210)'],['4','A4 (210 x 297)'],['3','A3 (297 x 420)'],['2','A2 (420 x 594)'],['1','A1 (594 x 840)'],['0','A0 (840 x 1188)']]
        });

        this.orientationStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: [['1','Альбомная'],['2','Книжная']]
        });

        this.scaleStates = new Ext.data.SimpleStore({
            fields: ['id', 'state'],
            data: [['Текущий','Текущий'],['100','100'],['500','500'],['1000','1000'],['2000','2000']]
        });

        /* Форма настроек печати */

        this.printSizeCombobox = new Ext.form.ComboBox({
            fieldLabel: 'Размер бумаги',
            store: this.printSizeStates,
            displayField: 'state',
            valueField: 'id',
            mode: 'local',
            anchor: '100%',
            triggerAction: 'all',
            editable: false,
            selectOnFocus: true
        });

        this.scaleCombobox = new Ext.form.ComboBox({
            typeAhead: true,
            fieldLabel: 'Масштаб',
            store: this.scaleStates,
            displayField: 'state',
            valueField: 'id',
            mode: 'local',
            anchor: '100%',
            triggerAction: 'all',
            editable: true,
            selectOnFocus: true
        });

        this.orientationCombobox = new Ext.form.ComboBox({
            fieldLabel: 'Ориентация',
            store: this.orientationStates,
            displayField: 'state',
            valueField: 'id',
            mode: 'local',
            anchor: '100%',
            triggerAction: 'all',
            editable: false,
            selectOnFocus: true
        });

        this.legendCheckbox = new Ext.form.Checkbox({
            fieldLabel: 'Легенда',
            style: 'margin-top: 4px;'
        });

        this.previewPanel = new Ext.Panel({
            width: 215,
            height: 180,
            bodyBorder: false,
            border: false
        });

        PrintPanel.superclass.constructor.call(this, {
            width: 250,
            frame: true,
            buttonAlign: 'center',
            items: [{
                xtype: 'fieldset',
                title: 'Настройки печати',
                autoHeight: true,
                anchor: '100%',
                items: [{
                    border: false,
                    items: [{
                        layout: 'form',
                        border: false,
                        items: [this.scaleCombobox]
                    }]
                },
                {
                    border: false,
                    items: [{
                        layout: 'form',
                        border: false,
                        items: [this.printSizeCombobox]
                    }]
                },
                {
                    border: false,
                    items: [{
                        layout: 'form',
                        border: false,
                        items: [this.orientationCombobox]
                    }]
                },
                {
                    border: false,
                    items: [{
                        layout: 'form',
                        border: false,
                        items: [this.legendCheckbox]
                    }]
                }]
            },
            {
                xtype: 'fieldset',
                title: 'Предварительный просмотр',
                bodyStyle: 'background-color: #fff;',
                autoHeight: true,
                anchor: '100%',
                items: [this.previewPanel]
            }/*,
            {
                layout: 'column',
                border: false,
                style: 'padding-top: 5px',
                items: [{
                    columnWidth: 0.56,
                    layout: 'form',
                    html: '&nbsp;'
                },
                {
                    columnWidth: 0.18,
                    layout: 'form',
                    items: [this.okButton]
                },
                {
                    columnWidth: 0.26,
                    layout: 'form',
                    items: [this.cancelButton]
                }]
            }*/]
        });
        this.okButton = this.addButton({
            iconCls: 'icon-accept-tick',
            text : 'OK'
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Отмена',
            handler : function() {
                this.hide();
            },
            scope : this
        });
    }
});
/*
Map.Print.prototype = {
    getParameters: function()
    {
        var printSizes = {
            5: [0.148, 0.210],
            4: [0.210, 0.297],
            3: [0.297, 0.420],
            2: [0.420, 0.594],
            1: [0.594, 0.840],
            0: [1.188, 1.680]
        };
        var orgScale    = (this.panel.scaleCombobox.getValue()=='Текущий')? parseInt(this.viewerApi.Map.GetScale()): this.panel.scaleCombobox.getValue();
        var printSize   = this.panel.printSizeCombobox.getValue();
        var ratio       = 0.7;
        var pw          = this.panel.previewPanel.width-2;
        var ph          = this.panel.previewPanel.height-2;
        var orientation = this.panel.orientationCombobox.getValue();
        var mw          = Math.round((orientation == 1)? pw: ph * ratio);
        var mh          = Math.round((orientation == 1)? pw * ratio: ph);
        var sessionId   = this.viewerApi.Map.GetSessionId();
        var scale, lw;
        if (mw > mh) {
            scale = orgScale * ((printSizes[printSize][1] / (0.0254 / 96)) / mw);
            lw = mw * (150 / (printSizes[printSize][1] / (0.0254 / 96)));
        }
        else {
            scale = orgScale * ((printSizes[printSize][1] / (0.0254 / 96)) / mh);
            lw = mh * (150 / (printSizes[printSize][0]/ (0.0254 / 96)));
        }
        lw = (this.panel.legendCheckbox.getValue())? lw: 0;
        mw = (this.panel.legendCheckbox.getValue())? (mw-lw): mw;
        return {
            scale: scale,
            centerX: this.viewerApi.Map.GetCenter().X,
            centerY: this.viewerApi.Map.GetCenter().Y,
            mapName: this.viewerApi.Map.GetMapName(),
            printSize: printSize,
            mw: mw,
            mh: mh,
            pw: pw,
            ph: ph,
            lw: lw,
            legendWidth: (lw)?this.viewerApi.Map.GetMapDimension().legendWidth:0,
            orgScale: orgScale,
            orientation: orientation,
            sessionId: sessionId
        };
    },

    getImage: function(parameters)
    {
        this.conn.request({
            url: '/mapguide/mapguide/mapagent.fcgi',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: {
                MAPNAME: parameters.mapName,
                OPERATION: 'GETVISIBLEMAPEXTENT',
                SEQ: Math.random(),
                SESSION: parameters.sessionId,
                SETDISPLAYDPI: 96,
                SETDISPLAYHEIGHT: parameters.mh,
                SETDISPLAYWIDTH: parameters.mw,
                SETVIEWCENTERX: parameters.centerX,
                SETVIEWCENTERY: parameters.centerY,
                SETVIEWSCALE: parameters.scale,
                VERSION: '1.0.0'
            },
            success: function(response) {
                if (this.previewImage) {
                    this.previewImage.remove();
                }
                var legendDiv = (parameters.lw)? {tag: 'div', style: "margin-left:-1px;margin-top:-1px;border: 1px solid #ccc;float:left;width:"+parameters.lw+"px;height:100%;"}: {tag: 'div'};
                this.previewImage = this.panel.previewPanel.body.createChild({
                    tag: 'div',
                    style: "border: 1px solid #ccc;margin-top:"+Math.round((parameters.ph-parameters.mh)/2)+"px;margin-bottom:auto;margin-left:auto;margin-right:auto;width:"+(parameters.mw+parameters.lw)+"px;height:"+parameters.mh+"px;",
                    children: [legendDiv,{
                        tag: 'div',
                        style: "margin-left:"+parameters.lw+"px;width:"+parameters.mw+"px;height:100%;background-repeat:no-repeat;background-image:url(/mapguide/mapagent/mapagent.fcgi?OPERATION=GETDYNAMICMAPOVERLAYIMAGE&FORMAT=PNG&VERSION=1.0.0&SESSION="+parameters.sessionId+"&MAPNAME="+encodeURIComponent(parameters.mapName)+"&SEQ="+Math.random()+");"
                    }]
                });
                //this.viewerApi.Map.StopObservingOnMapLoaded(this.updatePreview);
                //this.viewerApi.Map.Refresh();
                //this.viewerApi.Map.ObserveOnMapLoaded(this.updatePreview, this);
            },
            scope: this
        });
    },

    updatePreview: function()
    {//alert(1);
        if (this.window && !this.window.hidden) {
            var parameters = this.getParameters()
            this.getImage(parameters);
        }
    },

    print: function()
    {
        var msgWait = Ext.WgsMsg.wait('Генерация PDF-документа...', 'Подождите', {interval: 80});
        var parameters = this.getParameters();
        this.wgsConn.request({
            url: '/map/print',
            defaultHeaders: {"Content-Type": "application/x-www-form-urlencoded"},
            method: 'POST',
            params: parameters,
            success: function(response) {
                msgWait.hide();
                if (response.responseText) {
                    this.openPdf(response.responseText);
                }
            },
            scope: this
        });
    },

    openPdf: function(pdfSeq)
    {
        this.docWindow = new Ext.WgsShimWindow({
            title : 'Печать',
            closable : true,
            width : 550,
            height : 450,
            minWidth: 320,
            minHeight: 240,
            border : false,
            resizable : true,
            maximizable : true,
            layout: 'fit',
            plain : true,
            closeAction : 'hide'
        });
        this.pdfPanel = new Ext.Panel({
            html: '<object classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" CODEBASE="/downloads/AdbeRdr90ru.exe" id="custom" width="100%" height="100%"><param name="src" value="/map/print/getpdf/'+pdfSeq+'"></param><EMBED src="/map/print/getpdf/'+pdfSeq+'" width="100%" height="100%" href="/map/print/getpdf/'+pdfSeq+'" PLUGINSPAGE="/downloads/AdbeRdr90ru.exe"></EMBED></object>'
        });
        this.docWindow.add(this.pdfPanel);
        this.docWindow.show();
        this.docWindow.maximize();
    },

    open: function()
    {
        this.window.addPanel();
        this.window.show();
        this.updatePreview();
        this.viewerApi.Map.ObserveOnMapLoaded(this.updatePreview, this);
        //TODO: on hide stop observing onmaploaded event
    }
}*/