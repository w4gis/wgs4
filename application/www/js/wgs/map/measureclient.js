﻿Ext.namespace('Map');

Map.MeasureClient = function(mapViewer)
{
    this.mapViewer = mapViewer;
    this.viewerApi = mapViewer.Api;
    this.conn = new Ext.data.WgsConnection();
    this.map = this.viewerApi.map;
    this.mouseButton = null;
    this.isMouseDown = false;
    this.raphael = null;

    this.startX = 0;
    this.startY = 0;
    this.segment = 0;
    this.totalLength = 0;
}

Map.MeasureClient.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.MeasureClient(map);
    }
    return this.instances[map.mapFrame];
}

Map.MeasureClient.prototype = {
    open: function()
    {
        this.raphael = this.mapViewer.Lib.getRaphaelDrawing();
        this.raphael.createCanvas();
        this.setDefaultParams();

        this.viewerApi.Map.ObserveOnMapLoaded(function () {
            this.setDefaultParams();
        }, this);

        this.viewerApi.Map.OnContextMenu(function(e){
           return false;
        },this);

        this.viewerApi.Map.OnMouseDown(function(e){
            this.mouseButton = (e.button) ? e.button : e.which;
            if (this.mouseButton == 1) {
                this.isMouseDown = true;
                var x = e.clientX - this.map.mapPosX;
                var y = e.clientY;
                this.createSegment(x, y);
            }   else {
                this.setDefaultParams();
            }
        },this);

        this.viewerApi.Map.OnMouseMove(function(e){
            var x = e.clientX - this.map.mapPosX;
            var y = e.clientY;
            this.createTemporarySegment(x, y);
        },this);

        WGS.Tools.addHandler(this.map.document, 'keydown', this.onKeyDown.createDelegate(this), this);
    },

    onKeyDown: function(e)
    {
        if (e.keyCode == 27) {
           this.setDefaultParams();
        }
    },

    createSegment: function (x, y)
    {
       if (this.startX != 0 && this.startY != 0) {
            if(this.startX != x && this.startY != y) {
                this.raphael.hideLabelDistance();
                this.raphael.drawCircle(this.startX, this.startY, 1, {fill: "#ff3333", stroke: "#ff3333", "fill-opacity": 0.1});
                this.raphael.drawLine(this.startX, this.startY, x, y, {stroke: "#ff3333", "stroke-width": 1});

                var startPoint = this.map.ScreenToMapUnits(this.startX, this.startY);
                var currPoint  = this.map.ScreenToMapUnits(x, y);

                this.segment = this.segment+1;
                var segmentLength = Math.sqrt(Math.pow(currPoint.X-startPoint.X,2)+Math.pow(currPoint.Y-startPoint.Y,2));
                this.totalLength = this.totalLength+segmentLength;

                var labelTL = (this.totalLength < 1000)? this.totalLength.toFixed(2)+' м' : (Math.round(this.totalLength)/1000).toFixed(2) + ' км';

                this.startX = x;
                this.startY = y;
                this.raphael.drawLine(this.startX, this.startY, x, y-15, {stroke: "#036"});
                this.raphael.drawRoundedRect(x-30, y-35, 60, 20, 5, {fill: "#ffffcc", stroke: "#036", "fill-opacity": 0.5});
                this.raphael.drawLabel(x, y-22, labelTL, {fill: "#000", "font-family": "Arial", "font-size": 10});
            }
        } else {
            this.startX = x;
            this.startY = y;
            this.raphael.drawCircle(this.startX, this.startY, 1, {fill: "#ff3333", stroke: "#ff3333", "fill-opacity": 0.1});
            // надпись
            this.raphael.drawLine(this.startX, this.startY, x, y-15, {stroke: "#036"});
            this.raphael.drawRoundedRect(x-30, y-35, 60, 20, 5, {fill: "#ffffcc", stroke: "#036", "fill-opacity": 0.5, "text":"2345"});
            this.raphael.drawLabel(x, y-22, '0 м',{fill: "#000", "font-family": "Arial", "font-size": 10});

        }
    },

    createTemporarySegment: function (x, y)
    {
        if (this.startX != 0 && this.startY != 0) {
                this.currentX = x;
                this.currentY = y;
                this.raphael.drawTemporaryLine1(this.startX, this.startY, this.currentX, this.currentY, {stroke: "#ff3333"/*stroke: "#037"*/});
                this.raphael.drawCircle(this.startX, this.startY, 1, {fill: "#ff3333", stroke: "#ff3333", "fill-opacity": 0.1});
                this.raphael.drawTemporaryLine2(this.currentX, this.currentY, this.currentX, this.currentY-20, {stroke: "#036"});
                this.raphael.drawTemporaryRoundedRect(this.currentX-40, this.currentY-50, 100, 30, 5, {fill: "#ffffcc", stroke: "#036", "fill-opacity": 0.5});

                var startPoint = this.map.ScreenToMapUnits(this.startX, this.startY);
                var currPoint  = this.map.ScreenToMapUnits(this.currentX, this.currentY);

                var segmentLength = Math.sqrt(Math.pow(currPoint.X-startPoint.X,2)+Math.pow(currPoint.Y-startPoint.Y,2));
                var segmentLengthLabel = (segmentLength < 1000)? segmentLength.toFixed(2) +' м': (Math.round(segmentLength)/1000).toFixed(2) + ' км';
                var tempTotal = this.totalLength+segmentLength;
                tempTotal = (tempTotal < 1000)? tempTotal.toFixed(2) +' м': (Math.round(tempTotal)/1000).toFixed(2) + ' км';

                this.raphael.drawTemporaryLabel1(this.currentX+10, this.currentY-40, 'Сегмент : '+segmentLengthLabel,{fill: "#000", "font-family": "Arial", "font-size": 10});
                this.raphael.drawTemporaryLabel2(this.currentX+10, this.currentY-25, 'Всего : '+tempTotal,{fill: "#000", "font-family": "Arial", "font-size": 10});
            }
    },

    setDefaultParams: function()
    {
        if (this.raphael) {
            this.raphael.clear();
            this.startX = 0;
            this.startY = 0;
            this.segment = 0;
            this.totalLength = 0;
        }
    }
}
