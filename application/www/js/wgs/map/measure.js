﻿Ext.namespace('Map');

Map.Measure = function(mapViewer)
{
    this.viewerApi = mapViewer.Api;   
    this.conn = new Ext.data.WgsConnection();
    this.panel = new MeasurePanel({
        width: 350,
        height: 354,
        viewerApi: this.viewerApi
    });
    
    this.window = new Ext.WgsServiceWindow({
        containerPanel: mapViewer.rightPane,
        layout: 'fit',
        resizable: false,
        width: this.panel.width+12,
        title: 'Измерение расстояния'
    });
    
    this.window.on('exit', function(){
        this.panel.fireEvent('endmeasure', this.panel);
    },this);
        
    this.panel.on('startmeasure', this.onStartMeasure);
    this.panel.on('addsegment', this.onAddSegment);
    this.panel.on('clearmeasure', this.onClearMeasure);
    this.panel.on('stopmeasure', this.onStopMeasure);
    this.panel.on('endmeasure', this.onEndMeasure);
    this.panel.on('stopresume', this.onStopResume);
    this.panel.on('cancel', this.onCancel);
    
    this.window.setInnerPanel(this.panel);
}

Map.Measure.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Measure(map);
    }    
    return this.instances[map.mapFrame];
}

Map.Measure.prototype = {
    open: function()
    {
        this.window.addPanel();
        this.panel.show();
        this.window.show();
    },
    
    onStartMeasure : function(panel)
    {
       this.viewerApi.Map.OnClickMap(function(x, y){
            
            var newPoint = this.viewerApi.Map.ScreenToMapUnits(x, y);
            if(this.startPoint == null)
            {
                this.startPoint = newPoint;                     
            }
            else    
            {   
                this.segment++;
                this.fireEvent('addsegment', panel, this.startPoint, newPoint, this.segment);
                this.startPoint = newPoint;
            }
        },panel);
    },
    
    onAddSegment : function(panel, startPoint, newPoint, segment)
    {
        this.conn.request({
            url : '/map/measure/addmeashuresegment',
            method : 'POST',
            params : {
                'session'       : this.viewerApi.Map.GetSessionId(),
                'mapname'       : this.viewerApi.Map.GetMapName(),
                'segment'       : segment,
                'total'         : 0,
                'locale'        : 'ru',
                'x1'            : startPoint.X,
                'y1'            : startPoint.Y,
                'x2'            : newPoint.X,
                'y2'            : newPoint.Y,
                'layerlegend'   : 'Измерение расстояния'
            },
            success : function(response) 
                {
                    this.viewerApi.Map.Refresh();
                    this.viewerApi.Legend.Refresh();
                    var responseRecord = this.jsonReader.read(response).records[0];
                    responseRecord.set('segmentName', "Сегмент "+segment);
                    responseRecord.commit();
                    this.segmentStore.add(responseRecord);
                },
            scope: panel    
        });
    },
    
    onStopMeasure : function()
    {
        this.viewerApi.Map.StopClickMap();
    },
    
    onClearMeasure : function(panel)
    {
        this.conn.request({
            url : '/map/measure/clearmeasure',
            method : 'POST',
            success : function(response) 
                {
                    this.viewerApi.Map.Refresh();
                    this.segmentStore.removeAll();
                    this.totalStore.getAt(0).set('total', '<b>'+0+' м</b>');
                    this.totalStore.getAt(0).commit();
                    this.segment = 0;
                    this.startPoint = null;
                    this.total = 0;
                    if(this.stopresumeButton.text == 'Продолжить')
                    {   
                        this.stopresumeButton.toggle();
                        this.stopresumeButton.setText('Остановить');
                    }
                    this.fireEvent('startmeasure', this);
                },
            scope: panel    
        });
    },
   
    onEndMeasure : function(panel)
    {
       this.conn.request({
            url : '/map/measure/endmeasure',
            method : 'POST',
            params: { 'mapname': this.viewerApi.Map.GetMapName() },
            success : function(response) 
            {
                this.fireEvent('stopmeasure');
                this.viewerApi.Map.Refresh();
                this.segmentStore.removeAll();
                this.totalStore.getAt(0).set('total', '<b>'+0+' м</b>');
                this.totalStore.getAt(0).commit();
                this.segment = 0;
                this.startPoint = null;
                this.total = 0;
                if(this.stopresumeButton.text == 'Продолжить')
                {   
                    this.stopresumeButton.toggle();
                    this.stopresumeButton.setText('Остановить');
                }
            },
            scope: panel
        });
    },
     
    onStopResume : function(panel, text)
    {
        if(text == 'Остановить')
        {
            panel.stopresumeButton.setText('Продолжить');
            panel.fireEvent('stopmeasure');
        }
        else
        {
            panel.stopresumeButton.setText('Остановить');
            panel.fireEvent('startmeasure', panel);
        }
    },
    
    onCancel : function(panel)
    {
        panel.fireEvent('endmeasure', panel);
        panel.hide();
    }
}

MeasurePanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config){
        if (config){
            Ext.apply(this, config);
        }
                   
        this.conn = new Ext.data.WgsConnection();
        this.startPoint = null;
        this.segment = 0;
        this.total = 0;
        this.jsonReader = new Ext.data.JsonReader({},[
                           {name: 'segmentName'},
                           {name: 'segmentLength'},
                           {name: 'segmentSelectionXml'}
        ]);
                        
        this.segmentStore = new Ext.data.WgsJsonStore({
             fields: ['segmentName', 'segmentLength', 'segmentSelectionXml'],
             listeners:
             {  
                add : function( store, records, index )
                {
                    records.each( function (record)
                                {
                                    this.total = this.total + record.data.segmentLength;
                                }, this )
                    if(this.total < 1000)
                       this.totalStore.getAt(0).set('total', '<b>'+this.total+' м</b>');
                    else
                       this.totalStore.getAt(0).set('total', '<b>'+(this.total/1000).toFixed(3)+' км</b>');
                    this.totalStore.getAt(0).commit();
                },
                scope: this
             },
             scope: this
        });
        
        this.segmentSelectionModel = new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners:
            {
                rowselect : function( selModel, rowIndex, record )
                {
                    this.viewerApi.Map.SetSelectionXML(record.data.segmentSelectionXml);
                },
                scope: this
            }
        });
        
        this.segmentsGrid = new Ext.grid.GridPanel({
            store : this.segmentStore,
            cm : new Ext.grid.ColumnModel([ 
                new Ext.grid.RowNumberer(), 
                {id: 'segmentName',         header: 'Наименование', hideable: false, sortable: false, dataIndex: 'segmentName', menuDisabled: true},
                {id: 'segmentLength',       header: 'Длина (м)',    hideable: false, sortable: false, dataIndex: 'segmentLength', menuDisabled: true, align: 'center'},
                {id: 'segmentSelectionXml', hidden:true,            hideable: false, dataIndex: 'segmentSelectionXml', menuDisabled: true}
            ]),
            sm : this.segmentSelectionModel,
            viewConfig: 
               {
                   forceFit: true
               },
            autoScroll : true,
            height: '100%',
            stripeRows: true,
            anchor: '100% 91%'       
        });
         
        this.totalStore = new Ext.data.SimpleStore({
             fields: ['title', 'total'],
             data : [['<b>Общее расстояние</b>','<b>'+ 0 + ' м </b>']]
        });
        
        this.totalGrid = new Ext.grid.GridPanel({
            store : this.totalStore,
            cm : new Ext.grid.ColumnModel([ 
                {id: 'title', header: 'Наименование', hideable: false, sortable: false, dataIndex: 'title'},
                {id: 'total', header: 'Длина',        hideable: false, sortable: false, dataIndex: 'total', align: 'center'}
            ]),
            viewConfig: 
               {
                   forceFit: true
               },
            disableSelection: true,
            trackMouseOver: false, 
            hideHeaders: true,
            anchor: '100% 9%'/*,
            bbar:[this.clearButton, this.stopresumeButton, '->', this.cancelButton] */      
        });
        
        MeasurePanel.superclass.constructor.call(this, {
            header: true,
            layout: 'fit',
            items: [ 
                    new Ext.FormPanel({
                    layout: 'form',
                    border: false,
                    items:[ this.segmentsGrid, this.totalGrid ]
                    })
                   ],
            listeners:
                {
                    show: function()
                    {
                        this.fireEvent('startmeasure', this);
                    },
                    scope: this
                }
        });
        
        this.clearButton = this.addButton({
            text : 'Очистить',
            handler : function() {
                this.fireEvent('clearmeasure', this);
            },
            scope : this
        });
        
        this.stopresumeButton = this.addButton({
            text : 'Остановить',
            enableToggle: true,
            handler : function() {
                this.fireEvent('stopresume', this, this.stopresumeButton.text);
            },
            scope : this
        });
        
        this.cancelButton = this.addButton({
            text : 'Отмена',
            handler : function() {
                this.fireEvent('cancel', this)
            },
            scope : this
        });
    }    
});


            
           
            
            