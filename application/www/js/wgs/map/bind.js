﻿Map.Bind = function(mapViewer)
{
    ///////////////////////

    Ext.ns('Wgs.Map.Bind');

    ///////////////////////

    Wgs.Map.Bind.MapViewer = mapViewer;
    Wgs.Map.Bind.ViewerLib = mapViewer.Lib;
    Wgs.Map.Bind.ViewerApi = mapViewer.Api;
    Wgs.Map.Bind.WgsConn = new Ext.data.WgsConnection();

    Wgs.Map.Bind.BindPanel = new Map.Bind.BindPanel;

    Wgs.Map.Bind.Window = new Ext.WgsServiceWindow({
        containerPanel: Wgs.Map.Bind.MapViewer.rightPane,
        width: Wgs.Map.Bind.BindPanel.width+12,
        autoHeight: true,
        title: 'Привязка карточек',
        resizable: false
    });

    Wgs.Map.Bind.MapViewer.addChildComponent(Wgs.Map.Bind.Window);

    Wgs.Map.Bind.Window.setInnerPanel(Wgs.Map.Bind.BindPanel);

    Wgs.Map.Bind.FeatureTypeSelectWindow = new Map.Bind.FeatureTypeSelectWindow;

    ////////METHODS////////
    this.getWindow = function() {
        return Wgs.Map.Bind.Window;
    }

    this.open = function(config) {
        if (!WGS.Permissions.Check(WGS.PRIVILEGE.CARD_BIND)) {
            return;
        }

        var config = config||{};
        Wgs.Map.Bind.Mode = config.mode||Map.Bind.OBJECT_CARD_MODE;

        switch (Wgs.Map.Bind.Mode) {
            case Map.Bind.CARD_OBJECT_MODE:
                Wgs.Map.Bind.BindPanel.cancelButton.hide();
                Wgs.Map.Bind.BindPanel.tabs.setActiveTab(1);
                Wgs.Map.Bind.BindPanel.BaseBindPanel.disable();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.bindedGrid.hide();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.cardGrid.show();
                Wgs.Map.Bind.BindPanel.InlineBindPanel.cardStore.loadData([
                    [config.cardId, config.cardName, config.featureTypeName, config.featureTypeId]
                ]);
                break;
            case Map.Bind.OBJECT_CARD_MODE:
                break;
        }

        Wgs.Map.Bind.Window.addPanel();
        Wgs.Map.Bind.Window.show();
        Wgs.Map.Bind.Window.minimize();
        Wgs.Map.Bind.BindPanel.BaseBindPanel.bindedGrid.disable();
        Wgs.Map.Bind.OnSelectionChangedHandler = function() {
            Wgs.Map.Bind.ViewerLib.getSelectedFeatures(function(featureLayers){
                if (featureLayers != -1) {
                    var entityId = featureLayers[0].features[0].entityId;
                    var featureId = featureLayers[0].features[0].entityId;

                    if (featureLayers[0].features.length > 1 || featureLayers.length > 1) {
                        with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                            bindedStore.removeAll();
                            bindedGrid.disable();
                        }
                    }
                    else {
                        Wgs.Map.Bind.entityId = entityId;
                        Wgs.Map.Bind.featureId = featureId;
                        with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                            bindedStore.baseParams = {featureId: Wgs.Map.Bind.featureId};
                            bindedStore.load();
                            bindedGrid.enable();
                        }
                    }
                    with (Wgs.Map.Bind.BindPanel.InlineBindPanel) {
                        featuresGrid.enable();
                        featuresStore.baseParams = {'featureIds[]': Wgs.Map.Bind.ViewerLib.getSelectedFeatureIds(featureLayers)};
                        featuresStore.load();
                    }
                } else {
                    with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
                        bindedStore.removeAll();
                        bindedGrid.disable();
                    }
                    with (Wgs.Map.Bind.BindPanel.InlineBindPanel) {
                        relationPanel.disable();
                        featuresGrid.disable();
                        bindedGrid.disable();
                    }
                }
            });
        }
        Wgs.Map.Bind.ViewerApi.Map.ObserveOnSelectionChanged(Wgs.Map.Bind.OnSelectionChangedHandler, this);

    }

    /////////EVENTS////////

    /* Wgs.Map.Bind.FeatureTypeSelectWindow */

    with (Wgs.Map.Bind.FeatureTypeSelectWindow) {
        okButton.on('click', function() {
            var featureTypeId = Wgs.Map.Bind.FeatureTypeSelectWindow.attrTypeComboBox.getValue();
            var records = Wgs.Map.Bind.FeatureTypeSelectWindow.featureTypeStore.query('featureTypeId', featureTypeId);
            var isCurrent = records.item(0).data.isCurrent;
            Wgs.Map.Bind.WgsConn.request({
                url: '/map/bind/createcard',
                method: 'POST',
                params: {featureTypeId: featureTypeId},
                success: function(response) {
                    var jsonResponse = Ext.decode(response.responseText);
                    var objectId = jsonResponse.objectId;
                    Wgs.Map.Bind.WgsConn.request({
                        url: '/map/bind/bindcard',
                        method: 'POST',
                        params: {objectId: objectId, featureId: Wgs.Map.Bind.featureId, isCurrent: isCurrent},
                        success: function(response) {
                            Wgs.Map.Bind.FeatureTypeSelectWindow.hide();
                            Wgs.Map.Bind.BindPanel.BaseBindPanel.bindedStore.removeAll();
                            Wgs.Map.Bind.BindPanel.BaseBindPanel.bindedStore.load();
                        }
                    });
                }
            });
        });

        cancelButton.on('click', function() {
            Wgs.Map.Bind.FeatureTypeSelectWindow.hide();
        });

        attrTypeComboBox.on('select', function(combobox) {
            if (combobox.getValue()) {
                Wgs.Map.Bind.FeatureTypeSelectWindow.okButton.enable();
            } else {
                Wgs.Map.Bind.FeatureTypeSelectWindow.okButton.disable();
            }
        });
    }

    /* Wgs.Map.Bind.BindPanel.BaseBindPanel */

    with (Wgs.Map.Bind.BindPanel) {
        cancelButton.on('click', function() {
            Wgs.Map.Bind.ViewerApi.Map.StopObservingOnSelectionChanged(Wgs.Map.Bind.OnSelectionChangedHandler);
            Wgs.Map.Bind.BindPanel.hide();
        });
    }

    /* Wgs.Map.Bind.BindPanel.BaseBindPanel */

    with (Wgs.Map.Bind.BindPanel.BaseBindPanel) {
        bindedCheckboxSelectionModel.on('selectionchange', function(SelectionModel){
            var selections = SelectionModel.getSelections();
            var currentSelected = false;
            Ext.each(selections, function(record){
                if (record.data.cardIsCurrent == '1') {
                    currentSelected = true;
                    return;
                }
            });
            if (selections.length > 0 && !currentSelected) {
                Wgs.Map.Bind.BindPanel.BaseBindPanel.unBindButton.enable();
                Wgs.Map.Bind.BindPanel.BaseBindPanel.removeButton.enable();
            } else {
                Wgs.Map.Bind.BindPanel.BaseBindPanel.unBindButton.disable();
                Wgs.Map.Bind.BindPanel.BaseBindPanel.removeButton.disable()
            }
        });

        removeButton.on('click', function() {
            var selections = this.bindedCheckboxSelectionModel.getSelections();
            var cardIds = [];

            Ext.each(selections, function(record){
                cardIds.push(record.data.cardId);
            });

            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите удалить выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId){
                    if (buttonId == 'yes') {
                        Wgs.Map.Bind.WgsConn.request({
                            url: '/map/bind/removecard',
                            method: 'POST',
                            params: {'cardIds[]': cardIds},
                            success: function(response) {
                                this.bindedStore.removeAll();
                                this.bindedStore.load();
                            },
                            scope: this
                        });
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }, Wgs.Map.Bind.BindPanel.BaseBindPanel);

        unBindButton.on('click', function() {
            var selections = this.bindedCheckboxSelectionModel.getSelections();
            var ADBSDBRelations = [];

            Ext.each(selections, function(record){
                ADBSDBRelations.push(record.data.ADBSDBRelationId);
            });

            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: 'Вы уверены, что хотите отвязать выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId){
                    if (buttonId == 'yes') {
                        Wgs.Map.Bind.WgsConn.request({
                            url: '/map/bind/unbindcard',
                            method: 'POST',
                            params: {'ADBSDBRelations[]': ADBSDBRelations},
                            success: function(response) {
                                this.bindedStore.removeAll();
                                this.bindedStore.load();
                            },
                            scope: this
                        });
                    }
                },
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }, Wgs.Map.Bind.BindPanel.BaseBindPanel);

        bindButton.on('click', function() {
            with (Wgs.Map.Bind.FeatureTypeSelectWindow.featureTypeStore) {
                removeAll();
                baseParams = {entityId: Wgs.Map.Bind.entityId};
                load({
                    callback: function(records) {
                        if (records.length > 0) {
                            var featureTypeFilter = [];
                            Ext.each(records, function(record){
                                featureTypeFilter.push(record.data.featureTypeId);
                            });
                            var bindSearch = Map.Bind.Search.Instance(this.mapViewer);
                            bindSearch.open({
                                object: Wgs.Map.Bind.featureId,
                                featureTypeFilter: featureTypeFilter,
                                mode: Map.Bind.Search.FEATUREBIND_MODE,
                                callback: function(objectId, featureId, featureTypeId) {
                                    var records = Wgs.Map.Bind.FeatureTypeSelectWindow.featureTypeStore.query('featureTypeId', featureTypeId);
                                    var isCurrent = records.item(0).data.isCurrent;
                                    Wgs.Map.Bind.WgsConn.request({
                                        url: '/map/bind/bindcard',
                                        method: 'POST',
                                        params: {objectId: objectId, featureId: featureId, isCurrent: isCurrent},
                                        success: function(response) {
                                            bindSearch.panel.resultTabPanel.resultStore.removeAll();
                                            bindSearch.panel.hide();
                                            this.bindedStore.removeAll();
                                            this.bindedStore.load();
                                        },
                                        scope: this
                                    });
                                },
                                scope: this
                            });
                        } else {
                            Ext.WgsMsg.show({
                                title: 'Предупреждение',
                                msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    },
                    scope: this
                }, this);
            }
        }, Wgs.Map.Bind.BindPanel.BaseBindPanel);

        createButton.on('click', function() {
            with (Wgs.Map.Bind.FeatureTypeSelectWindow.featureTypeStore) {
                removeAll();
                baseParams = {entityId: Wgs.Map.Bind.entityId};
                load({
                    callback: function(records) {
                        if (records.length > 0) {
                            Wgs.Map.Bind.FeatureTypeSelectWindow.show();
                        } else {
                            Ext.WgsMsg.show({
                                title: 'Предупреждение',
                                msg: 'К данному объекту карточки всех связанных типов уже привязаны',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    }
                });
            }
        }, Wgs.Map.Bind.BindPanel.BaseBindPanel);

        rowActions.on({
            action: function(grid, record, action, row, col) {
                if (record.data.cardId) {
                    Map.Card.Instance().open({
                        objectId: record.data.cardId,
                        callback: function() {
                            bindedStore.reload();
                        }
                    });
                }
                else {
                    Map.Card.Instance().open({
                        featureId: record.data.featureId,
                        callback: function() {
                            bindedStore.reload();
                        }
                    });
                }
            }
        });
    }

    /* Wgs.Map.Bind.BindPanel.InlineBindPanel */

    with (Wgs.Map.Bind.BindPanel.InlineBindPanel) {
        var setRelationsFieldSetTitle = function(relationType) {
            bindedGrid.enable();
            if (relationType == 2)
                relationsFieldSet.setTitle('Связь - 1 : &infin;');
            else
                relationsFieldSet.setTitle('Связь - 1 : 1');
        }

        var getInlineBindedCardsByRelation = function() {
            var selections = featuresCheckboxSelectionModel.getSelections();
            var featureIds = [];
            var objTypeRelationId = relationsComboBox.getValue();
            Ext.each(selections, function(record){
                featureIds.push(record.data.featureId);
            });
            bindedStore.baseParams = {objTypeRelationId: objTypeRelationId, 'featureIds[]': featureIds};
            bindedStore.load();
        }

        featuresCheckboxSelectionModel.selectionChangeTimeout = {};

        featuresCheckboxSelectionModel.selectionChangeHanler = function() {
            var selections = featuresCheckboxSelectionModel.getSelections();
            var layer, layerCount = 0;
            Ext.each(selections, function(record){
                if (layer != record.data.featureLayerName) {
                    layer = record.data.featureLayerName;
                    if (++layerCount > 2) {
                        return;
                    }
                }
            });
            if (layerCount != 1) {
                relationPanel.disable();
                bindedGrid.disable();
                bindedStore.baseParams = {};
                bindedStore.removeAll();
            } else {
                relationsStore.removeAll();
                relationsComboBox.clearValue();
                relationsStore.baseParams.featureId = selections[0].data.featureId;
                relationsStore.load({callback: function(records){
                    if (records.length > 0) {
                        relationPanel.enable();
                        relationsComboBox.setValue(records[0].data.objTypeRelationId);
                        setRelationsFieldSetTitle(records[0].data.relationType);
                        relationsComboBox.fireEvent('select', relationsComboBox, records[0]);
                        //getInlineBindedCardsByRelation();
                    }
                }});
            }
        }

        var unbindCardsByRelations = function(mode) {
            var childObjectIds = [], objectTypeRelationIds = [], parentFeatureIds = [];
            var bindedSelections = bindedCheckboxSelectionModel.getSelections();
            Ext.each(bindedSelections, function(record){
                childObjectIds.push(record.data.cardId);
                objectTypeRelationIds.push(record.data.objTypeRelationId);
            });
            var featuresSelections = featuresCheckboxSelectionModel.getSelections();
            Ext.each(featuresSelections, function(record){
                parentFeatureIds.push(record.data.featureId);
            });
            Ext.WgsMsg.show({
                title: 'Подтверждение',
                msg: mode? 'Вы уверены, что хотите отвязать выбранные карточки?': 'Вы уверены, что хотите удалить выбранные карточки?',
                buttons: Ext.Msg.YESNO,
                fn: function(buttonId){
                    if (buttonId == 'yes') {
                        var msgWait = Ext.Msg.wait('Отвязка объектов...', 'Подождите', {interval: 50});
                        new Ext.data.WgsConnection().request({
                            url: '/objectadmin/RemoveObjectRelation',
                            method: 'POST',
                            params: {
                                removeMode: mode,
                                'childObjectIds[]': childObjectIds,
                                'parentFeatureIds[]': parentFeatureIds,
                                'objectTypeRelationIds[]': objectTypeRelationIds
                            },
                            success: function() {
                                bindedStore.load();
                                msgWait.hide();
                            }
                        });
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }

        var createChildCard = function() {
            var msgWait = Ext.Msg.wait('Создание карточки...', 'Подождите', {interval: 50});
            var objTypeRelationId = relationsComboBox.getValue();
            var parentFeatureIds = [];
            var featuresSelections = featuresCheckboxSelectionModel.getSelections();
            var objectTypeId = relationsStore.getAt(relationsStore.find('objTypeRelationId', objTypeRelationId)).data.featureTypeId;
            Ext.each(featuresSelections, function(record){
                parentFeatureIds.push(record.data.featureId);
            });
            new Ext.data.WgsConnection().request(
            {
                url: '/objectadmin/CreateChildObject',
                method: 'POST',
                params:
                {
                    objectTypeId: objectTypeId,
                    'parentFeaturesIds[]': parentFeatureIds,
                    objTypeRelationId: objTypeRelationId
                },
                success: function(action)
                {
                    var response = Ext.decode(action.responseText);
                    if (response) {
                        Map.Card.Create(Wgs.Map.Bind.MapViewer).open({
                            objectId: response.childObjectId,
                            callback: function() {
                                bindedStore.reload();
                            }
                        });
                        bindedStore.reload();
                    }
                    msgWait.hide();
                }
            });
        }

        var searchCard = function() {
            var bindSearch = Map.Bind.Search.Instance(Wgs.Map.Bind.MapViewer);
            var parentFeatureIds = [];
            var featuresSelections = featuresCheckboxSelectionModel.getSelections();
            var objTypeRelationId = relationsComboBox.getValue();
            var objectTypeId = relationsStore.getAt(relationsStore.find('objTypeRelationId', objTypeRelationId)).data.featureTypeId;
            Ext.each(featuresSelections, function(record){
                parentFeatureIds.push(record.data.featureId);
            });
            bindSearch.open({
                mode: Map.Bind.Search.OBJECTBIND_MODE,
                featureTypeFilter: [objectTypeId],
                object: parentFeatureIds,
                callback: function(objectId, parentFeatureIds, featureTypeId) {
                    new Ext.data.WgsConnection().request({
                        url: '/objectadmin/BindChildObject',
                        method: 'POST',
                        params: {'parentFeatureIds[]': parentFeatureIds, childObjectId: objectId, objTypeRelationId: objTypeRelationId},
                        success: function(action) {
                            bindSearch.panel.resultTabPanel.resultStore.removeAll();
                            bindSearch.container.hide();
                            bindedStore.reload();
                        }
                    });
                },
                closeSearch: function() {
                    Wgs.Map.Bind.MapViewer.panel.enable();
                }
            });
        }

        featuresCheckboxSelectionModel.on('selectionchange', function(SelectionModel){
            with (featuresCheckboxSelectionModel) {
                window.clearTimeout(selectionChangeTimeout);
                selectionChangeTimeout = window.setTimeout(selectionChangeHanler, 500);
            }
        });

        relationsComboBox.on('select', function(combo, record){
            if (Wgs.Map.Bind.Mode == Map.Bind.CARD_OBJECT_MODE) {
                bindedStore.on('load', function() {
                    var featureTypeId = cardStore.getAt(0).get('featureTypeId');
                    if (record.data.featureTypeId == featureTypeId) {
                        if (record.data.relationType == 1) {
                            if (bindedStore.getTotalCount() == 0) {
                                bindButton.enable();
                            } else {
                                bindButton.disable();
                            }
                        } else {
                            bindButton.enable();
                        }
                    } else {
                        bindButton.disable();
                    }
                },{
                    single: true
                });
            }
            setRelationsFieldSetTitle(record.data.relationType);
            getInlineBindedCardsByRelation();
        });

        bindedCheckboxSelectionModel.on('selectionchange',  function(SelectionModel){
            var selections = bindedCheckboxSelectionModel.getSelections();
            if (selections.length > 0) {
                unBindButton.enable();
                removeButton.enable();
            } else {
                unBindButton.disable();
                removeButton.disable();
            }
        });

        unBindButton.on('click', function(){
            unbindCardsByRelations(1);
        });

        removeButton.on('click', function(){
            unbindCardsByRelations(0);
        });

        createButton.on('click', function(){
            createChildCard();
        });

        searchCardButton.on('click', function(){
            Wgs.Map.Bind.MapViewer.panel.disable();
            searchCard();
        });

        bindButton.on('click', function(){
            var parentFeatureIds = [];
            var featuresSelections = featuresCheckboxSelectionModel.getSelections();
            var objTypeRelationId = relationsComboBox.getValue();
            var objectTypeId = relationsStore.getAt(relationsStore.find('objTypeRelationId', objTypeRelationId)).data.featureTypeId;
            var objectId = cardStore.getAt(0).get('cardId');
            Ext.each(featuresSelections, function(record){
                parentFeatureIds.push(record.data.featureId);
            });
            new Ext.data.WgsConnection().request({
                url: '/objectadmin/BindChildObject',
                method: 'POST',
                params: {'parentFeatureIds[]': parentFeatureIds, childObjectId: objectId, objTypeRelationId: objTypeRelationId},
                success: function(action) {
                    Ext.WgsMsg.show({
                        title: 'Операция выполнена',
                        msg: 'Карточка вложена',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    bindButton.disable();
                }
            });
        });

        bindedStore.on('load', function(store, records) {
            var objTypeRelationId = relationsComboBox.getValue();
            if (objTypeRelationId) {
                var relationType = relationsStore.getAt(relationsStore.find('objTypeRelationId', objTypeRelationId)).data.relationType;
                if (relationType == 1) {
                    if (records.length == 0) {
                        createButton.enable();
                        searchCardButton.enable();
                    } else {
                        createButton.disable();
                        searchCardButton.disable();
                    }
                }
            }
        });

        Ext.each([rowActions,rowActions1,rowActions2], function(item) {
            item.on({
                action: function(grid, record, action, row, col) {
                    var mapCard = Map.Card.Instance();
                    mapCard.open({
                        objectId: record.data.cardId,
                        callback: function() {
                            bindedStore.reload();
                        }
                    });
                    Wgs.Map.Bind.MapViewer.addChildComponent(mapCard.window);
                }
            });
        });
    }
}

Map.Bind.OBJECT_CARD_MODE = 1;
Map.Bind.CARD_OBJECT_MODE = 2;

Map.Bind.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (!this.instances[map.mapFrame]) {
        this.instances[map.mapFrame] = new Map.Bind(map);
    }
    return this.instances[map.mapFrame];
}

/////////////////////////////////////////////////////////////////
Map.Bind.FeatureTypeSelectWindow = Ext.extend(Ext.WgsShimWindow,{
    constructor: function(config) {
        Ext.apply(this, config);

        this.okButton = new Ext.Button({
            text : 'OK',
            disabled: true
        });

        this.cancelButton = new Ext.Button({
            text : 'Отмена'
        });

        this.featureTypeStore = new Ext.data.WgsJsonStore({
            fields: ['featureTypeId', 'featureTypeName', 'isCurrent'],
            url: '/map/bind/getfeaturetypes'
        });

        this.attrTypeComboBox = new Ext.form.ComboBox({
            anchor: '100%',
            hideLabel: true,
            labelSeparator: '',
            store: this.featureTypeStore,
            displayField: 'featureTypeName',
            valueField: 'featureTypeId',
            loadingText: 'Список загружается...',
            triggerAction: 'all',
            emptyText: 'Выберите описание...',
            allowBlank: true,
            editable: false
        });

        Map.Bind.FeatureTypeSelectWindow.superclass.constructor.call(this,
        {
            title:'Выбор атрибутивного описания',
            layout: 'fit',
            height: 180,
            width: 375,
            modal: true,
            autoScrol: true,
            resizable: false,
            items:[
                new Ext.form.FormPanel({
                    frame: true,
                    bodyStyle:'padding:5px 5px 0',
                    items:  [
                        new Ext.Panel({
                            frame: true,
                            height: 60,
                            layout: 'fit',
                            style: 'margin-bottom: 5px',
                            html: '<p>Если вы уверены, что хотите создать атрибутивный объект и ' +
                                  'связать его с данным, выберите атрибутивный тип и нажмите "Создать", ' +
                                  'иначе "Отмена"</p>'
                        }),
                        this.attrTypeComboBox]
                })],
            buttons: [this.okButton, this.cancelButton]
        });
    }
});


Map.Bind.Search = Ext.extend(Map.Search, {
    constructor: function(mapViewer) {
        Map.Bind.Search.superclass.constructor.call(this, mapViewer);

        this.container.setTitle('Привязка карточек');
        this.panel.searchTabPanel.featureLayerPanel.hide();
        this.panel.searchTabPanel.splitPanel.hide();
        this.panel.searchTabPanel.featureTypePanel.columnWidth = 1.0;
        this.panel.searchOnMapButton.hide();
        this.panel.cancelButton.hide();
        //this.panel.searchInCardsButton.setText('Отобразить все карточки');

        this.bindButton = this.panel.addButton({
            text: 'Привязать',
            disabled: true,
            handler: this.bind,
            scope: this
        });

        this.panel.addButton({
            text: 'Отмена',
            handler:  function() {
                this.hide();
            },
            scope: this.panel
        });

        this.panel.resultTabPanel.resultCheckboxSelectionModel.on('selectionchange', function(SelectionModel){
            var selections = SelectionModel.getSelections();
            var filteredSelected = false;
            Ext.each(selections, function(record){
                if (!this.featureTypeFilter.inArray(record.data.featureTypeId)) {
                    filteredSelected = true;
                    return;
                }
            },this);
            if (SelectionModel.getSelections().length == 1 && !filteredSelected) {
                this.bindButton.enable();
            } else {
                this.bindButton.disable();
            }
        },this);
    },

    open: function(config) {
        var config = config||{};
        this.bindObject = config.object;
        this.bindCallback = config.callback||Ext.emptyFn;
        this.closeSearchCallback = config.closeSearch||Ext.emptyFn;
        this.bindScope = config.scope;
        this.bindMode = config.mode;
        this.featureTypeFilter = config.featureTypeFilter||[];

        this.container.on('hide', this.closeSearchCallback, this.bindScope, {single: true});

        if (!this.bindObject) {
            Ext.WgsMsg.show({
                title: 'Предупреждение',
                msg: 'Объекты не указаны',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        } else {
            this.panel.searchTabPanel.featureTypeStore.removeAll();
            this.panel.searchTabPanel.featureTypeStore.baseParams = {'featureTypeFilter[]': this.featureTypeFilter};
            this.panel.searchTabPanel.featureTypeStore.load();
            Map.Bind.Search.superclass.open.call(this);
        }
    },

    bind: function() {
        var selections = this.panel.resultTabPanel.resultCheckboxSelectionModel.getSelections();
        var featureId = this.bindObject;
        var objectId;
        var featureTypeId;
        Ext.each(selections, function(item, index){
            objectId = item.get('objectId');
            featureTypeId = item.get('featureTypeId');
        });
        this.bindCallback.call(this.bindScope, objectId, featureId, featureTypeId);
    },

    onShowSearchTabPanel: function()
    {
        Map.Bind.Search.superclass.onShowSearchTabPanel.call(this);
        this.panel.searchOnMapButton.hide();
        this.bindButton.hide();
    },

    onShowResultTabPanel: function()
    {
        Map.Bind.Search.superclass.onShowResultTabPanel.call(this);
        this.panel.previewButton.hide();
        this.bindButton.show();
    }
});

Map.Bind.Search.OBJECTBIND_MODE  = 1;
Map.Bind.Search.FEATUREBIND_MODE = 2;

Map.Bind.Search.Instance = function(map)
{
    if (!this.instances) {
        this.instances = {};
    }
    if (map) {
        if (!this.instances[map.mapFrame]) {
            this.instances[map.mapFrame] = new Map.Bind.Search(map);
        }
        return this.instances[map.mapFrame];
    } else {
        if (!this.instance) {
            this.instance = new Map.Bind.Search(map);
        }
        return this.instance;
    }
}

Map.Bind.BaseBindPanel = Ext.extend(Ext.Panel, {
    constructor: function(config)
    {
        Ext.apply(this, config);

        this.rowActions = new Ext.ux.grid.RowActions({
            actions:[{
                iconCls: 'viewcard',
                qtip: 'Просмотр карточки объекта'
            }]
        });

        this.bindedStore = new Ext.data.WgsGroupingStore({
            reader: new Ext.data.JsonReader({},[
                {name: 'cardName'},
                {name: 'cardTypeName'},
                {name: 'cardId'},
                {name: 'cardIsCurrent'},
                {name: 'ADBSDBRelationId'}
            ]),
            autoLoad: false,
            url: '/map/bind/getbinded',
            fields: ['cardName', 'cardTypeName', 'cardId', 'cardIsCurrent', 'ADBSDBRelationId'],
            sortInfo: {field: 'cardName', direction: "ASC"},
            remoteGroup: true
        });

        this.bindedCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

        this.createButton = new Ext.Button({
            text : 'Создать'
        });
        this.bindButton = new Ext.Button({
            text : 'Найти'
        });
        this.unBindButton = new Ext.Button({
            text : 'Отвязать',
            disabled: true
        });
        this.removeButton = new Ext.Button({
            text : 'Удалить',
            disabled: true
        });

        this.bindedGrid = new Ext.grid.GridPanel({
            frame: true,
            store: this.bindedStore,
            columns:[
                {id: 'cardId', header: "ID объекта", sortable: true, dataIndex: 'cardId', width: 75, hidden: true, align: 'center'},
                {id: 'cardName', header: "Наименование", sortable: true, dataIndex: 'cardName'},
                {id: 'cardTypeName', header: "Тип", sortable: true, dataIndex: 'cardTypeName'},
                this.rowActions,
                this.bindedCheckboxSelectionModel
            ],
            sm: this.bindedCheckboxSelectionModel,
            plugins: [this.rowActions],
            view: new Ext.grid.GroupingView({
                forceFit: true,
                groupTextTpl: '{gvalue}'
            }),
            stripeRows:true,
            tbar: [this.createButton,this.bindButton,this.unBindButton,this.removeButton]
        });

        Map.Bind.BaseBindPanel.superclass.constructor.call(this, {
            title: 'Слой - Тип',
            deferedRender: false,
            layout: 'fit',
            items: [this.bindedGrid]
        });
    }
});

Map.Bind.InlineBindPanel = Ext.extend(Ext.Panel, {
    constructor: function(config)
    {
        Ext.apply(this, config);

        this.rowActions = new Ext.ux.grid.RowActions({
            actions:[{
                iconCls: 'viewcard',
                qtip: 'Просмотр карточки объекта'
            }]
        });

        this.rowActions1 = new Ext.ux.grid.RowActions({
            actions:[{
                iconCls: 'viewcard',
                qtip: 'Просмотр карточки объекта'
            }]
        });

        this.rowActions2 = new Ext.ux.grid.RowActions({
            actions:[{
                iconCls: 'viewcard',
                qtip: 'Просмотр карточки объекта'
            }]
        });

        this.featuresStore = new Ext.data.WgsGroupingStore({
            reader: new Ext.data.JsonReader({},[
                {name: 'featureLabel'},
                {name: 'featureLayerName'},
                {name: 'cardId'},
                {name: 'featureId'},
                {name: 'entityId'}
            ]),
            autoLoad: false,
            url: '/map/bind/getfeatures',
            fields: ['featureLabel', 'featureLayerName', 'cardId', 'featureId', 'entityId'],
            sortInfo: {field: 'featureLayerName', direction: "ASC"},
            remoteGroup: true
        });

        this.relationsStore = new Ext.data.WgsJsonStore({
            reader: new Ext.data.JsonReader({},[
                {name: 'objTypeRelationId'},
                {name: 'relationId'},
                {name: 'relationName'},
                {name: 'relationType'},
                {name: 'featureTypeId'},
                {name: 'featureTypeName'}
            ]),
            autoLoad: false,
            url: '/map/bind/getrelations',
            fields: ['objTypeRelationId', 'relationId', 'relationName', 'relationType', 'featureTypeId', 'featureTypeName'],
            baseParams: {featureId: null},
            sortInfo: {field: 'relationName', direction: "ASC"}
        });

        this.bindedStore = new Ext.data.WgsGroupingStore({
            reader: new Ext.data.JsonReader({},[
                {name: 'cardName'},
                {name: 'cardId'},
                {name: 'objTypeRelationId'}
            ]),
            autoLoad: false,
            url: '/map/bind/getinlinebinded',
            fields: ['cardName', 'cardId', 'objTypeRelationId'],
            sortInfo: {field: 'cardName', direction: "ASC"},
            remoteGroup: true
        });

        this.bindedCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

        this.relationsComboBox = new Ext.form.ComboBox({
            //name: 'gridRel-' + record.data.id,
            hideLabel: true,
            store: this.relationsStore,
            anchor: '100%',
            displayField: 'relationName',
            valueField: 'objTypeRelationId',
            //typeAhead: true,
            loadingText:'Список загружается...',
            triggerAction: 'all',
            emptyText: 'Выберите связь...',
            selectOnFocus: true,
            editable: false
        });

        this.featuresCheckboxSelectionModel = new Ext.grid.CheckboxSelectionModel();

        this.featuresGrid = new Ext.grid.GridPanel({
            region: 'north',
            frame: true,
            height: 253,
            store: this.featuresStore,
            disabled: true,
            columns:[
                {id: 'featureLabel', header: "Подпись", sortable: true, dataIndex: 'featureLabel'},
                {id: 'featureLayerName', header: "Слой", sortable: true, dataIndex: 'featureLayerName'},
                this.rowActions,
                this.featuresCheckboxSelectionModel,
                {id: 'cardId', hidden: true, dataIndex: 'cardId'},
                {id: 'featureId', hidden: true, dataIndex: 'featureId'},
                {id: 'entityId', hidden: true, dataIndex: 'entityId'}
            ],
            sm: this.featuresCheckboxSelectionModel,
            plugins: [this.rowActions],
            view: new Ext.grid.GroupingView({
                forceFit: true,
                groupTextTpl: '{gvalue}'
            }),
            stripeRows: true
        });

        this.relationsFieldSet = new Ext.form.FieldSet({
            title: 'Связь',
            autoHeight: true,
            items: [this.relationsComboBox]
        });

        this.relationPanel = new Ext.Panel({
            layout: 'fit',
            region: 'center',
            frame: true,
            items: [this.relationsFieldSet],
            disabled: true
        });

        this.unBindButton = new Ext.Button({
            text: 'Отвязать',
            disabled: true
        });

        this.removeButton = new Ext.Button({
            text: 'Удалить',
            disabled: true
        });

        this.createButton = new Ext.Button({
            text: 'Создать'
        });

        this.searchCardButton = new Ext.Button({
            text: 'Найти'
        });

        this.bindButton = new Ext.Button({
            text: 'Привязать',
            disabled: true
        });

        this.bindedGrid = new Ext.grid.GridPanel({
            layout: 'fit',
            region: 'center',
            frame: true,
            hideHeaders: false,
            store: this.bindedStore,
            columns:[
                {id: 'cardId', header: "ID объекта", sortable: true, dataIndex: 'cardId', width: 75, hidden: true, align: 'center'},
                {id: 'cardName', header: "Наименование", sortable: true, dataIndex: 'cardName'},
                this.rowActions1,
                this.bindedCheckboxSelectionModel
            ],
            sm: this.bindedCheckboxSelectionModel ,
            plugins: [this.rowActions1],
            view: new Ext.grid.GroupingView({
                forceFit: true,
                groupTextTpl: '{gvalue}'
            }),
            stripeRows:true,
            tbar: [this.createButton, this.searchCardButton, this.unBindButton, this.removeButton],
            height: 270,
            disabled: true
        });

        this.cardStore = new Ext.data.SimpleStore({
            fields: ['cardId', 'cardName', 'featureTypeName', 'featureTypeId']
        });

        this.cardGrid = new Ext.grid.GridPanel({
            layout: 'fit',
            region: 'south',
            frame: true,
            hideHeaders: false,
            store: this.cardStore,
            hidden: true,
            columns:[
                {id: 'cardName', header: "Наименование", dataIndex: 'cardName', width: 120},
                {id: 'featureTypeName', header: "Тип", dataIndex: 'featureTypeName', width: 100},
                this.rowActions2,
                {id: 'cardId', header: "ID объекта", dataIndex: 'cardId', hidden: true, align: 'center'}
            ]/*,
            viewConfig: {
                forceFit: true
            }*/,
            plugins: [this.rowActions2],
            stripeRows: true,
            height: 270,
            bbar: ['->',this.bindButton]
        });

        Map.Bind.InlineBindPanel.superclass.constructor.call(this, {
            title: 'Тип - Тип',
            deferedRender: false,
            layout: 'border',
            items: [this.featuresGrid,this.relationPanel,new Ext.Panel({
                height: 270,
                deferedRender: false,
                layout: 'border',
                region: 'south',
                items: [this.bindedGrid,this.cardGrid]
            })]
        });
    }
});

Map.Bind.BindPanel = Ext.extend(Ext.WgsServicePanel, {
    constructor: function(config)
    {
        Ext.QuickTips.init();
        this.BaseBindPanel = new Map.Bind.BaseBindPanel(config);
        this.InlineBindPanel = new Map.Bind.InlineBindPanel(config);

        this.tabs = new Ext.TabPanel({
            activeTab: 0,
            items: [this.BaseBindPanel, this.InlineBindPanel]
        });

        Map.Bind.BindPanel.superclass.constructor.call(this, {
            title: 'Привязка карточек',
            layout: 'fit',
            width: 300,
            height: 700,
            items: [this.tabs],
            frame: true
        });

        this.cancelButton = this.addButton({
            iconCls: 'icon-close-cross',
            text : 'Закрыть'
        });
    }
});
