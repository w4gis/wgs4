Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/default/s.gif';

/* Ext.data.WgsConnection */

Ext.data.WgsConnection = Ext.extend(Ext.data.Connection, {
    request: function()
    {
        var scope = arguments[0].scope;
        var successFn = arguments[0].success||Ext.emptyFn;
        var callbackFn = arguments[0].callback||Ext.emptyFn;
        var failureFn = arguments[0].failure||Ext.emptyFn;
        var exceptionFn = arguments[0].exception||Ext.emptyFn;
        var self = this;

        function hasException(response)
        {
            if (response && response.responseText)
            {
                try {
                    var jsonData = Ext.util.JSON.decode(response.responseText)
                } catch(e) {}
                if (!WGS.Exception.Handle(self, jsonData, failureFn, scope))
                    return true;
            }
            return false;
        }

        if (successFn)
        {
            arguments[0].success = function(response)
            {
                if (hasException(response)) {
                    exceptionFn.call(scope, response);
                    return;
                }
                successFn.call(scope, response);
            }
        }

        if (failureFn)
        {
            arguments[0].failure = function(response)
            {
                if (hasException(response)) {
                    exceptionFn.call(scope, response);
                    return;
                }
                failureFn.call(scope, response);
            }
        }

        if (callbackFn)
        {
            arguments[0].callback = function(options, success, response)
            {
                if (hasException(response)) {
                    exceptionFn.call(scope, response);
                    return;
                }
                callbackFn.call(scope, options, success, response);
            }
        }

        arguments[0].timeout = 300000;

        Ext.data.WgsConnection.superclass.request.apply(this, arguments);
    }
});

/* Ext.tree.WgsTreeLoader */

Ext.tree.WgsTreeLoader = function(config)
{
    Ext.tree.WgsTreeLoader.superclass.constructor.call(this, config);
    var self = this;
    this.addEvents({exception: true});
    this.on('load', function(scope, node, response){
        try {
            var jsonData = Ext.util.JSON.decode(response.responseText)
        } catch(e) {}
        WGS.Exception.Handle(self, jsonData);
    });
}

Ext.extend(Ext.tree.WgsTreeLoader, Ext.tree.TreeLoader);

/* Ext.data.WgsJsonStore */

Ext.data.WgsJsonStore = function(config)
{
    Ext.data.WgsJsonStore.superclass.constructor.call(this, config);
    var self = this;
    this.addEvents({exception: true});
    this.on('load', function(scope, data, arg){
    	var jsonData = [];
    	for(var i = 0; i < data.length; i++) {
    		jsonData.push(data[i].json);
    	}
       WGS.Exception.Handle(self, jsonData);
    });
}

Ext.extend(Ext.data.WgsJsonStore, Ext.data.JsonStore);

/* Ext.data.GroupingStore */

Ext.data.WgsGroupingStore = function(config)
{
    Ext.data.WgsGroupingStore.superclass.constructor.call(this, config);
    var self = this;
    this.addEvents({exception: true});
    this.proxy.on('load', function(scope, data, arg){
        WGS.Exception.Handle(self, data.reader.jsonData)
    });
}

Ext.extend(Ext.data.WgsGroupingStore, Ext.data.GroupingStore);

/* Ext.form.WgsFormPanel */

Ext.form.WgsFormPanel = function(config)
{
    this.addEvents({exception: true});
    Ext.form.WgsFormPanel.superclass.constructor.call(this, config);
    var self = this;
    this.submit = function(config){
        var form = self.getForm();
        if (form)
        {
        	var failureFn = config.failure;
            var scope = config.scope;
        	config.failure = function(form, action)
            {
            	try {
                    var jsonData = Ext.util.JSON.decode(action.response.responseText)
                } catch(e) {}
                WGS.Exception.Handle(self, jsonData, function(){
                	failureFn.call(scope || this, form, action);
                });
            }
            form.submit(config);
        }
    }
}

Ext.extend(Ext.form.WgsFormPanel, Ext.form.FormPanel);

/* Ext.WgsShimResizable */

Ext.WgsShimResizable = Ext.extend(Ext.Resizable, {
    /*DWF*/
    startSizing : function(e, handle){
        if (this.enabled && !this.overlay) {
            this.resizeShim = this.el.createChild({
                tag: 'iframe',
                frameBorder: 'no',
                scrolling: 'no',
                style: 'position: absolute; z-index: 10000; opacity: 1; -moz-opacity: 1; display: none;'
            });
        }
        Ext.fly(this.resizeShim).setX(this.el.getLeft());
        Ext.fly(this.resizeShim).setY(this.el.getTop());
        this.resizeShim.show();
        Ext.WgsShimResizable.superclass.startSizing.call(this, e, handle);
    },
    onMouseMove : function(e){
        Ext.WgsShimResizable.superclass.onMouseMove.call(this, e);
        Ext.fly(this.resizeShim).setWidth(this.proxy.getWidth());
        Ext.fly(this.resizeShim).setHeight(this.proxy.getHeight());
        Ext.fly(this.resizeShim).setX(this.proxy.getLeft());
        Ext.fly(this.resizeShim).setY(this.proxy.getTop());
    },
    onMouseUp : function(e){
        Ext.WgsShimResizable.superclass.onMouseUp.call(this, e);
        this.resizeShim.hide();
    }
    /**/
});

/* Ext.WgsShimWindow */

Ext.WgsShimWindow = Ext.extend(Ext.Window, {
    /*DWF*/
    onRender : function(ct, position){
        Ext.WgsShimWindow.superclass.onRender.call(this, ct, position);
        //if (Ext.isIE) {
            this.el.createChild({
                tag: 'iframe',
                frameBorder: 'no',
                scrolling: 'no',
                style: 'position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; z-index: -1;'
            });
            if(this.modal){
                this.mask.createChild({
                    tag: 'iframe',
                    frameBorder: 'no',
                    scrolling: 'no',
                    style: 'position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; z-index: -1;'
                });
            }
        //}
    },
    createGhost : function(cls, useShim, appendTo){
        var el = Ext.WgsShimWindow.superclass.createGhost.call(this, cls, useShim, appendTo);
        //if (Ext.isIE) {
            el.createChild({
                tag: 'iframe',
                frameBorder: 'no',
                scrolling: 'no',
                style: 'position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; z-index: -1;'
            });
        //}
        return el;
    },

    initEvents : function(){
        Ext.WgsShimWindow.superclass.initEvents.call(this);
        if(this.resizable){
            this.resizer = new Ext.WgsShimResizable(this.el, {
                minWidth: this.minWidth,
                minHeight: this.minHeight,
                handles: this.resizeHandles || "all",
                pinned: true,
                resizeElement : this.resizerAction
            });
            this.resizer.window = this;
            this.resizer.on("beforeresize", this.beforeResize, this);
        }
    }
});

/* Ext.WgsShimWindowRatio */

Ext.WgsShimWindowRatio = Ext.extend(Ext.WgsShimWindow, {
    onRender : function(ct, position){
        Ext.WgsShimWindowRatio.superclass.onRender.call(this, ct, position);
    },
    initEvents : function(){
        Ext.WgsShimWindowRatio.superclass.initEvents.call(this);
        if(this.resizable){
            this.resizer = new Ext.WgsShimResizable(this.el, {
                minWidth: this.minWidth,
                minHeight: this.minHeight,
                handles: this.resizeHandles || "all",
                pinned: true,
                preserveRatio: true,
                resizeElement : this.resizerAction
            });
            this.resizer.window = this;
            this.resizer.on("beforeresize", this.beforeResize, this);
        }
    },
    fitContainer : function(widthWindow, heightWindow){
        var vs = this.container.getViewSize();
            if (!widthWindow || !heightWindow)
            {
                this.restoreSize = this.getSize();
                widthWindow = this.restoreSize.width;
                heightWindow = this.restoreSize.height;
            }
                if (vs.width >= vs.height) {
                    var width = (widthWindow * vs.height) / heightWindow ;
                    var height = vs.height;
                    var widthPos = vs.width / 2 - width / 2;
                    var heightPos = 0;
                }
                if (vs.width < vs.height) {
                    var width = vs.width ;
                    var height = (heightWindow * vs.width) / widthWindow;
                    var widthPos = 0;
                    var heightPos = vs.height / 2 - height / 2;;
                }

                this.setPosition(widthPos, heightPos);
                this.setSize(width, height);
    },
    maximize : function(){
        if(!this.maximized){
            this.expand(false);
            this.restoreSize = this.getSize();
            this.restorePos = this.getPosition(true);
            if (this.maximizable){
                this.tools.maximize.hide();
                this.tools.restore.show();
            }
            this.maximized = true;
            this.el.disableShadow();

            if(this.dd){
                this.dd.lock();
            }
            if(this.collapsible){
                this.tools.toggle.hide();
            }
            this.el.addClass('x-window-maximized');
            this.container.addClass('x-window-maximized-ct');

            this.fitContainer(this.restoreSize.width, this.restoreSize.height);
            this.fireEvent('maximize', this);
        }
    }
});

/* Ext.WgsMsg */

Ext.WgsMsg = Object.clone(Ext.Msg);
Ext.WgsMsg.getDialog = function(titleText)
{
    var dialog = Ext.Msg.getDialog.call(this, titleText);
    /*DWF
    if (Ext.isIE) {
        dialog.mask.createChild({
            tag: 'iframe',
            frameBorder: 'no',
            scrolling: 'no',
            style: 'position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; filter: alpha(opacity=0); z-index: -1;'
        });
    }
    */
    return dialog;
}

/* Ext.WgsServicePanel */

Ext.WgsServicePanel = Ext.extend(Ext.Panel,{
    constructor: function(config) {
        if (config) {
            Ext.apply(this, config);
        }
        Ext.WgsServicePanel.superclass.constructor.call(this, {
            header: true,
            collapsible: true,
            layout: 'fit'
        });
    },
    setParent: function(parent)
    {
        this.parent = parent;
    },
    getParent: function()
    {
       return this.parent;
    },
    getParentType: function()
    {
        return this.parent.getXType();
    },
    setParentWindow: function(parentWindow)
    {
        this.parentWindow = parentWindow;
    },
    getParentWindow: function()
    {
        return this.parentWindow;
    },
    setParentTabPanel: function(parentTabPanel)
    {
        this.parentTabPanel = parentTabPanel;
    },
    getParentTabPanel: function()
    {
        return this.parentTabPanel;
    },
    hide: function()
    {
        if(this.getParentType() == 'panel') {
            this.getParentWindow().addPanel();
            this.getParentWindow().show();
            this.getParentWindow().hide();
        }
        else if (this.getParentType() == 'window') {
            this.getParent().hide();
            this.getParentWindow().fireEvent('exit');
        }
    }
});

// ������ ������ � �������������� ����������
// �� ���� ��� ���� ������ ������, �� ��� �������������� ������ hide � �������� ��� tabpanel
Ext.WgsSearthObjectAdminPanel = Ext.extend(Ext.Panel,{
    constructor: function(config) {
        if (config) {
            Ext.apply(this, config);
        }
        Ext.WgsSearthObjectAdminPanel.superclass.constructor.call(this, {
            header: true,
            collapsible: true,
            layout: 'fit'
        });
    },
    setParent: function(parent)
    {
        this.parent = parent;
    },
    getParent: function()
    {
       return this.parent;
    },
    setParentTabPanel: function(parentTabPanel)
    {
        this.parentTabPanel = parentTabPanel;
    },
    getParentTabPanel: function()
    {
        return this.parentTabPanel;
    }
});

/* Ext.WgsServiceTabPanel */
Ext.WgsServiceTabPanel = Ext.extend(Ext.TabPanel, {
    innerPanel: null,
    constructor: function(config)
    {
        if (config) {
            Ext.apply(this, config);
        }
        this.addEvents({exit: false});
        this.constructor.xtype = 'tabpanel';
        Ext.WgsServiceTabPanel.superclass.constructor.call(this);
    },
    setInnerPanel: function(panel)
    {
        this.innerPanel = panel;
        this.innerPanel.setParentTabPanel(this);
    },
    addPanel: function()
    {
        this.innerPanel.setParent(this);
        this.setActiveTab(this.add(this.innerPanel));
    },
    setParentWindow: function(parentWindow)
    {
        this.parentWindow = parentWindow;
    },
    getParentWindow: function()
    {
        return this.parentWindow;
    },
    setParent: function(parent)
    {
        this.parent = parent;
    },
    getParent: function()
    {
       return this.parent;
    }
});

/* Ext.WgsServiceWindow */

Ext.WgsServiceWindow = Ext.extend(Ext.WgsShimWindow, {
    innerPanel: null,
    constructor: function(config)
    {
        if (config) {
            Ext.apply(this, config);
        }
        this.addEvents({exit: true});
        this.constructor.xtype = 'window';
        Ext.WgsServiceWindow.superclass.constructor.call(this, {
            closable: true,
            //minimizable: true,
            animateTarget: this.containerPanel.body,
            border: false,
            closeAction: 'hide',
            listeners:
            {
                show: this.onShow,
                scope: this
            }
        });

       this.on('minimize', this.onMinimize, this);
       this.on('close', function(){this.hide(); this.fireEvent('exit')}, this);
    },
    setInnerPanel: function(panel)
    {
        this.innerPanel = panel;
        this.innerPanel.setParentWindow(this);
    },
    addPanel: function()
    {
        this.innerPanel.setParent(this);
        this.add(this.innerPanel);
        this.innerPanel.expand();
    },
    show: function()
    {
        Ext.WgsServiceWindow.superclass.show.call(this);
        this.innerPanel.header.setDisplayed(false);
    },
    onMinimize: function()
    {
        this.hide();
        this.innerPanel.header.setDisplayed(true);
        this.innerPanel.setTitle(this.title);
        this.innerPanel.setParent(this.containerPanel);
        this.containerPanel.addPanel(this.innerPanel);
        var width = (this.innerPanel.width)? this.innerPanel.width: this.width-12;//TODO: ������ ��� �����
        this.updateContainerPanel.call(this, width);
        this.containerPanel.expand();
        if (!(this.innerPanel.tools.restore)) {
            this.innerPanel.addTool({
                id: 'restore',
                on: {
                    click: this.onRestore,
                    scope: this
                }
            });
            this.containerPanel.doLayout();
        }
    },
    onRestore: function()
    {
        this.addPanel();
        this.show.call(this);
    },
    onShow: function(component, panelWidth)
    {
        this.updateContainerPanel.call(this, panelWidth);
    },
    updateContainerPanel: function(panelWidth)
    {
        if(this.containerPanel)
			this.containerPanel.update(panelWidth);
    }
});

Ext.WgsServiceWindowSimple = Ext.extend(Ext.WgsServiceWindow, {
    constructor: function(config) {
        if (config) {
            Ext.apply(this, config);
        }
        this.addEvents({exit: true});
        this.constructor.xtype = 'window';
        Ext.WgsServiceWindow.superclass.constructor.call(this, {
            closable: true,
            border: false,
            closeAction: 'hide'
        });
       this.on('close', function(){this.hide(); this.fireEvent('exit')}, this);
    }
});

/* Ext.WgsServiceWindowRatio - ���������������� ��������� ��������*/
Ext.WgsServiceWindowRatio = Ext.extend(Ext.WgsServiceWindow, {
    onRender : function(ct, position){
        Ext.WgsServiceWindowRatio.superclass.onRender.call(this, ct, position);
    },
    initEvents : function(){
        Ext.WgsServiceWindowRatio.superclass.initEvents.call(this);
        if(this.resizable){
            this.resizer = new Ext.WgsShimResizable(this.el, {
                minWidth: this.minWidth,
                minHeight: this.minHeight,
                handles: this.resizeHandles || "all",
                pinned: true,
                preserveRatio: true,
                resizeElement : this.resizerAction
            });
            this.resizer.window = this;
            this.resizer.on("beforeresize", this.beforeResize, this);
        }
    }
});

/* Ext.WgsServicePanel */

Ext.WgsMapPanel = Ext.extend(Ext.Panel, {
    constructor: function(config) {
        if (config) {
            Ext.apply(this, config);
        }
        this.constructor.xtype = 'panel';
        Ext.WgsMapPanel.superclass.constructor.call(this, {
            split: true,
            collapsed: true,
            collapsible: true,
            collapseMode: 'mini',
            autoScroll: true,
            listeners:
            {
                beforeexpand: function(panel, animated)
                {
                    if(panel.items && panel.getComponent(0)) {
                        return true;
                    }
                    return false;
                },
                resize: function()
                {
                    this.layoutPanels();
                }
            }
        });
    },
    collapsePanels: function(expandedPanel)
    {
        if (this.items && this.items.length > 0) {
            this.items.each(function(item) {
                item.collapse();
            });
        }
    },
    layoutPanels: function()
    {
        if (this.items && this.items.length > 0) {
            this.items.each(function(item) {
                item.doLayout();
            });
        }
    },
    addPanel: function(panel)
    {
        this.collapsePanels();
        this.add(panel);
        this.doLayout();
    },
    update: function(panelWidth)
    {
        if (this.items && this.items.length == 0) {
            this.collapse();
        }
        if (this.items && this.items.length > 0) {
           this.items.item(this.items.length-1).expand();
           var itemsCount = this.items.length;
           var maxWidth = (panelWidth)? panelWidth: 0;
           for (var i=0; i < itemsCount; i++) {
               if (maxWidth < this.items.item(i).width) {
                    maxWidth = this.items.item(i).width;
               }
           }
           if (maxWidth == 0) {
               maxWidth = this.width;
           }
           this.setSize(maxWidth+2);
           this.findParentByType('panel').syncSize();
        }
    }
});

/* Ext.WgsColorPalette */

Ext.WgsColorPalette = function(element, currentColor)
{
	if (!currentColor)
	   var currentColor = '000000';
    if (element)
    this.element =  element;

    this.setSource = function(element)
    {
        this.element = element;
        this.colorPalette.select('000000');
    };

	this.colorPalette = new Ext.ColorPalette({
        value: currentColor,
        listeners:
        {
            select: function(palette, selColor)
            {
                this.element.setValue(selColor);
                this.element.getEl().applyStyles({background: '#'+selColor, color: '#'+selColor });
            },
            scope: this
        }
    });

	Ext.WgsColorPalette.superclass.constructor.call(this, {
		title : '����� �����',
		closable : true,
		width : 160,
		height : 120,
        closeAction: 'hide',
		resizable : false,
		border:false,
		items : [ this.colorPalette ]
	});
}
Ext.extend(Ext.WgsColorPalette, Ext.WgsShimWindow);

/* Ext.form.WgsCombobox */

Ext.form.WgsComboBox = Ext.extend(Ext.form.ComboBox, {
    /*DWF
    initList: function()
    {
        Ext.form.WgsComboBox.superclass.initList.call(this);
        this.list.createChild({
            tag: 'iframe',
            frameBorder: 'no',
            scrolling: 'no',
            style: 'position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; z-index: -1;'
        });
    }
    */
});

/* Ext.form.CheckboxGroup Ext.form.RadioGroup*/

Ext.override(Ext.form.CheckboxGroup, {
    getNames: function() {
        var n = [];
        this.items.each(function(item) {
            if (item.getValue()) {
                n.push(item.getName());
            }
        });
        return n;
    },
    getValues: function() {
        var v = [];
        this.items.each(function(item) {
            if (item.getValue()) {
                v.push(item.getRawValue());
            }
        });
        return v;
    },
    setValues: function(v) {
        var r = new RegExp('(' + v.join('|') + ')');
        this.items.each(function(item) {
            item.setValue(r.test(item.getRawValue()));
        });
    }
});
Ext.override(Ext.form.RadioGroup, {
    getName: function() {
        return this.items.first().getName();
    },
    getValue: function() {
        var v;
        this.items.each(function(item) {
            if (!item.getValue())
                return true;
            v = item.getRawValue();
            return false;
        });
        return v;
    },
    setValue: function(v) {
        if (!this.rendered)
            return;
        this.items.each(function(item) {
            item.setValue(item.getRawValue() == v);
        });
    }
});

Ext.override(Ext.form.Action.Submit, {
    // private
    success : function(response){
        var result = this.processResponse(response);
        if(result === true || result === undefined || result.success){
            this.form.afterAction(this, true);
            return;
        }
        if(result && result.errors){
            this.form.markInvalid(result.errors);
            this.failureType = Ext.form.Action.SERVER_INVALID;
        }
        this.form.afterAction(this, false);
    },
    // private
    handleResponse : function(response){
        if(this.form.errorReader){
            var rs = this.form.errorReader.read(response);
            var errors = [];
            if(rs.records){
                for(var i = 0, len = rs.records.length; i < len; i++) {
                    var r = rs.records[i];
                    errors[i] = r.data;
                }
            }
            if(errors.length < 1){
                errors = null;
            }
            return {
                success : rs.success,
                errors : errors
            };
        }
        if (!response.responseText) {
        	return;
        }
        return Ext.decode(response.responseText);
    }
});

// Override Ext.grid.GridView
/*
Ext.override(Ext.grid.GridView, {
	initTemplates : function(){
		var ts = this.templates || {};
		if(!ts.master){
			ts.master = new Ext.Template(
					'<div class="x-grid3" hidefocus="true">',
						'<div class="x-grid3-viewport">',
							'<div class="x-grid3-header"><div class="x-grid3-header-inner"><div class="x-grid3-header-offset">{header}</div></div><div class="x-clear"></div></div>',
							'<div class="x-grid3-scroller"><div class="x-grid3-body">{body}</div><a href="#" class="x-grid3-focus" tabIndex="-1"></a></div>',
						"</div>",
						'<div class="x-grid3-resize-marker"> </div>',
						'<div class="x-grid3-resize-proxy"> </div>',
					"</div>"
					);
		}
		if(!ts.header){
			ts.header = new Ext.Template(
					'<table border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
					'<thead><tr class="x-grid3-hd-row">{cells}</tr></thead>',
					"</table>"
					);
		}
		if(!ts.hcell){
			ts.hcell = new Ext.Template(
					'<td class="x-grid3-hd x-grid3-cell x-grid3-td-{id} {css}" style="{style}"><div {tooltip} {attr} class="x-grid3-hd-inner x-grid3-hd-{id}" unselectable="on" style="{istyle}">', this.grid.enableHdMenu ? '<a class="x-grid3-hd-btn" href="#"></a>' : '',
					'{value}<img class="x-grid3-sort-icon" src="', Ext.BLANK_IMAGE_URL, '" />',
					"</div></td>"
					);
		}
		if(!ts.body){
			ts.body = new Ext.Template('{rows}');
		}
		if(!ts.row){
			ts.row = new Ext.Template(
					'<div class="x-grid3-row {alt}" style="{tstyle}"><table class="x-grid3-row-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
					'<tbody><tr>{cells}</tr>',
					(this.enableRowBody ? '<tr class="x-grid3-row-body-tr" style="{bodyStyle}"><td colspan="{cols}" class="x-grid3-body-cell" tabIndex="0" hidefocus="on"><div class="x-grid3-row-body">{body}</div></td></tr>' : ''),
					'</tbody></table></div>'
					);
		}
		if(!ts.cell){
			ts.cell = new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on" {attr}>{value}</div>',
					"</td>"
					);
		}
		for(var k in ts){
			var t = ts[k];
			if(t && typeof t.compile == 'function' && !t.compiled){
				t.disableFormats = true;
				t.compile();
			}
		}
		this.templates = ts;
		this.colRe = new RegExp("x-grid3-td-([^\\s]+)", "");
	},
	getColumnStyle : function(col, isHeader){
		var style = isHeader ? (this.cm.config[col].headerStyle || '') : (this.cm.config[col].cellStyle || this.cm.config[col].css || '');
		style += 'width:'+this.getColumnWidth(col)+';';
		if(this.cm.isHidden(col)){
			style += 'display:none;';
		}
		var align = this.cm.config[col].align;
		if(align){
			style += 'text-align:'+align+';';
		}
		return style;
	},
	getColumnData : function(){
		var cs = [], cm = this.cm, colCount = cm.getColumnCount();
		for(var i = 0; i < colCount; i++){
			var name = cm.getDataIndex(i);
			cs[i] = {
				name : (typeof name == 'undefined' ? this.ds.fields.get(i).name : name),
				renderer : cm.getRenderer(i),
				id : cm.getColumnId(i),
				style : this.getColumnStyle(i),
				css : cm.config[i].cellCls
			};
		}
		return cs;
	},
	renderHeaders : function(){
		var cm = this.cm, ts = this.templates;
		var ct = ts.hcell;
		var cb = [], sb = [], p = {};
		for(var i = 0, len = cm.getColumnCount(); i < len; i++){
			p.id = cm.getColumnId(i);
			p.value = cm.getColumnHeader(i) || "";
			p.style = this.getColumnStyle(i, true);
			p.css = cm.config[i].headerCls;
			p.tooltip = this.getColumnTooltip(i);
			if(cm.config[i].align == 'right'){
				p.istyle = 'padding-right:16px';
			} else {
				delete p.istyle;
			}
			cb[cb.length] = ct.apply(p);
		}
		return ts.header.apply({cells: cb.join(""), tstyle:'width:'+this.getTotalWidth()+';'});
	},
	doRender : function(cs, rs, ds, startRow, colCount, stripe){
		var ts = this.templates, ct = ts.cell, rt = ts.row, last = colCount-1;
		var tstyle = 'width:'+this.getTotalWidth()+';';
				var buf = [], cb, c, p = {}, rp = {tstyle: tstyle}, r;
		for(var j = 0, len = rs.length; j < len; j++){
			r = rs[j]; cb = [];
			var rowIndex = (j+startRow);
			for(var i = 0; i < colCount; i++){
				c = cs[i];
				p.id = c.id;
				////console.log(c.css);
				p.css = (c.css ? c.css : '') + (i == 0 ? ' x-grid3-cell-first' : (i == last ? ' x-grid3-cell-last' : ''));
				p.attr = p.cellAttr = "";
				p.value = c.renderer(r.data[c.name], p, r, rowIndex, i, ds);
				p.style = c.style;
				if(p.value == undefined || p.value === "") p.value = " ";
				if(r.dirty && typeof r.modified[c.name] !== 'undefined'){
					p.css += ' x-grid3-dirty-cell';
				}
				cb[cb.length] = ct.apply(p);
			}
			var alt = [];
			if(stripe && ((rowIndex+1) % 2 == 0)){
				alt[0] = "x-grid3-row-alt";
			}
			if(r.dirty){
				alt[1] = " x-grid3-dirty-row";
			}
			rp.cols = colCount;
			if(this.getRowClass){
				alt[2] = this.getRowClass(r, rowIndex, rp, ds);
			}
			rp.alt = alt.join(" ");
			rp.cells = cb.join("");
			buf[buf.length] =  rt.apply(rp);
		}
		return buf.join("");
	}
});*/

Ext.form.TextField.prototype.getSelection = function() {
 var domElement = this.getEl().dom;
	if (Ext.isIE) {
		var sel = document.selection;
		var range = sel.createRange();
		if (range.parentElement() != domElement)
			return null;
		var bookmark = range.getBookmark();
		var selection = domElement.createTextRange();
		selection.moveToBookmark(bookmark);
		var before = domElement.createTextRange();
		before.collapse(true);
		before.setEndPoint("EndToStart", selection);
		var after = domElement.createTextRange();
		after.setEndPoint("StartToEnd", selection);
		return {
			selectionStart : before.text.length,
			selectionEnd : before.text.length + selection.text.length,
			beforeText : before.text,
			text : selection.text,
			afterText : after.text
		}
	} else {
		if (domElement.selectionEnd && domElement.selectionStart) {
			if (domElement.selectionEnd > domElement.selectionStart) {
				return {
					selectionStart : domElement.selectionStart,
					selectionEnd : domElement.selectionEnd,
					beforeText : domElement.value.substr(0,
							domElement.selectionStart),
					text : domElement.value
							.substr(domElement.selectionStart,
									domElement.selectionEnd
											- domElement.selectionStart),
					afterText : domElement.value
							.substr(domElement.selectionEnd)
				};
			}
		}
		return {
			selectionStart: domElement.selectionStart,
			selectionEnd : domElement.selectionEnd
		};
	}
	return null;
}

Ext.form.TextField.prototype.getSelectedText = function() {
	var selection = this.getSelection();
	return selection == null ? null : selection.text;
}