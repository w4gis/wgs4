Ext.ns('WGS.Tools');

WGS.Tools.navigate = function(url)
{
    window.location.href = url;
};

WGS.Tools.getClientDimensions = function()
{
    return {
        height: document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight,
        width: document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth
    };
};

WGS.Cookie =
{
    get: function(name)
    {
        var value=null, search=name+"=";
        if (document.cookie.length > 0) {
          var offset = document.cookie.indexOf(search);
          if (offset != -1) {
            offset += search.length;
            var end = document.cookie.indexOf(";", offset);
            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(offset, end));
          }
        }
        return value;
    },
    set: function (name, value, expire, path)
    {
        document.cookie = name + "=" + escape(value) +
        ((!expire) ? "" : ("; expires=" + expire.toGMTString())) +
        "; path=" + ((!path) ? "/" : path);
    }
};

WGS.Tools.css =
{
    imp: function (styles)
    {
        styles.each(function(item) {
            var _style = document.createElement('link');
            _style.setAttribute("type", "text/css");
            _style.setAttribute("rel", "stylesheet");
            _style.setAttribute("href", item);
            document.getElementsByTagName("HEAD")[0].appendChild(_style);
        });
    },
    load: function (resource, parameters)
    {
        if (resource.url && !$(resource.url))
        new Ajax.Request(resource.url,
        {
            method: 'get',
            'parameters': parameters,
            onSuccess: function(transport)
            {
                var _style = document.createElement('style');
                _style.setAttribute("type", "text/css");
                _style.setAttribute("id", resource.url);
                if(_style.styleSheet)
                {// IE
                    _style.styleSheet.cssText = transport.responseText;
                } else
                {// w3c
                    var cssText = document.createTextNode(transport.responseText);
                    _style.appendChild(cssText);
                }
                document.getElementsByTagName("HEAD")[0].appendChild(_style);
                if (resource.onLoaded)
                    resource.onLoaded();
            }
        });
    }
};

WGS.Tools.js =
{
    load: function(url)
    {
        new Ajax.Request(url,
        {
            method: 'get',
            onLoaded: function(transport)
            {
                eval(transport.responseText)
            }
        });
    }
};

WGS.Tools.html =
{
    load: function(resource, parameters)
    {
        if (resource.url)
        new Ajax.Request(resource.url,
        {
            method: 'get',
            'parameters': parameters,
            onSuccess: function(transport)
            {
                var _div = document.createElement('div');
                _div.innerHTML = transport.responseText;

                var childrens = _div.childNodes;
                var childrens_length = childrens.length;

                for (var i = 0; i < childrens_length; i++)
                    document.body.appendChild(childrens.item(0));
                if (resource.onLoaded)
                    resource.onLoaded();
            }
        });
    }
};

WGS.Tools.Content =
{
    load: function(resource, parameters)
    {
        if (resource.onLoaded && resource.url)
        {
            parameters.accessCheckTransparentMode = true;
            new Ajax.Request(resource.url,
            {
                method: 'post',
                parameters: parameters,
                onSuccess: function(transport)
                {
                    document.body.setStyle({'cursor':''});
                    var response = transport.responseText;
                    try
                    {
                        var result = response.evalJSON(true);
                        if (result.status == WGS.Tools.status.message.AUTHORIZATION_REQUIRED.code)
                        {
                            WGS.System.Login.getInstance().setCallback(WGS.Tools.Content.load.bind(WGS.Tools.Content, resource, parameters));
                            WGS.System.Login.getInstance().open();
                        }
                        else
                        //TODO: incufficient privileges
                          throw(1);
                    }
                    catch(error)
                    {
                       resource.onLoaded(response, parameters);
                    }
                },
                onLoading: function()
                {
                    document.body.setStyle({'cursor':'wait'});
                }
            });
        }
    }
};

function applyPNGFilter(o)
{
    var s=o.src;
    o.runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+s+"',sizingMethod='scale')";
};

function decode64(input) {
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length);

   return output;
}

function encode64(input) {
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) +
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);

   return output;
}

WGS.Utf8 = {

    // public method for url encoding
    encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // public method for url decoding
    decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}

WGS.Tools.addHandler = function(object, event, handler, useCapture) {
    if (object.addEventListener) {
        object.addEventListener(event, handler, useCapture ? useCapture : false);
    } else if (object.attachEvent) {
        object.attachEvent('on' + event, handler);
    } else alert("Add handler is not supported");
}

WGS.Tools.removeHandler = function(object, event, handler) {
    if (object.removeEventListener) {
        object.removeEventListener(event, handler, false);
    } else if (object.detachEvent) {
        object.detachEvent('on' + event, handler);
    } else alert("Remove handler is not supported");
}

WGS.Tools.DownloadFile = function(url){
	url = unescape(url);
	var stub = Ext.get('cc-download-stub');
 	if (!stub) {
 		stub = Ext.get(Ext.DomHelper.append(document.body, {tag: 'iframe', cls: 'x-hidden', width:'0', height:'0', frameBorder:0, src: 'about:blank', id: 'cc-download-stub'}));
 	}
 	stub.dom.src = url ; // or url +  '&_dc=' + (new Date().getTime()) to prevent caching
}

WGS.Tools.Location = {
	Observable: new Ext.util.Observable,

	OnHashChange: function(handler, scope, options) {
		WGS.Tools.Location.Observable.on("hashChange", handler, scope, options);
	},

	UnHashChange: function(handler, scope) {
		WGS.Tools.Location.Observable.un("hashChange", handler, scope);
	},

	Hash: null,

	Observe: function() {
		try {
			var hash = document.location.hash;
			if (hash != WGS.Tools.Location.Hash) {
				WGS.Tools.Location.Hash = hash;
				WGS.Tools.Location.Observable.fireEvent("hashChange", hash);
			}
		}
		catch(e) {
		}
		WGS.Tools.Location.Observe.defer(50);
	},

	GetHash: function() {
		return WGS.Tools.Location.Hash;
	},

	SetHash: function(hash, silent) {
		document.location.hash = hash;
		if (silent) {
			WGS.Tools.Location.Hash = document.location.hash;
		}
	}
}

WGS.Tools.Location.Observe.defer(50);

WGS.safeDecode = function(s, debugMode) {
	try {
		var r = Ext.decode(s);
	} catch(e) {
		var r = debugMode? null: {};
	}
	return r||(debugMode? null: {});
};

Ext.ns('WGS.sys');

/**
 * @class WGS.aya.Events
 * Utility to work with events.
 * 
 */
WGS.sys.Events = function() {
	var observable = new Ext.util.Observable;
	
	// private
	function check() {
		var url = WGS.constants.Events.CHECK_URL;
		WGS.Ajax.request({
			url: url,
			success: function(response){
				check.defer(WGS.constants.Events.CHECK_INTERVAL);
			},
			failure: function(){
				check.defer(WGS.constants.Events.CHECK_INTERVAL);
			},
			observable: observable
		});
	}

	return {
		name: 'Events',
		version: '1.0.0',
		
		/**
		 * Start server observing 
		 */
		initBackendObserver: function() {
			check.defer(WGS.constants.Events.CHECK_INTERVAL);
		},

		/**
		 * Appends an event handler to event stream
		 * @param {String} eventName The name of the event.
		 * @param {Function} handler The handler function.
		 * @param {Object} scope If a scope (<b><code>this</code></b> reference) was specified when the listener was added,
         * then this must refer to the same object. 
		 */
		on: function(eventName, handler, scope, options) {
			/*
			 * Delegate to observable
			 */
			observable.addEvents(eventName);
			observable.on.apply(observable, arguments);
		},

		addEvents: function() {
			/*
			 * Delegate to observable
			 */
			observable.addEvents.apply(observable, arguments);
		},
		
		/**
	     * Fires the specified event with the passed parameters (minus the event name).
		 * @param {String} eventName The name of the event to fire.
		 * @param {Object...} args Variable number of parameters are passed to handlers.
		 * @return {Boolean} returns false if any of the handlers return false otherwise it returns true.
		 */
		fireEvent: function() {
			/*
			 * Delegate to observable
			 */
			observable.fireEvent.apply(observable, arguments);
		},

		/**
		 * Analyzes the server response and fire an event.
		 * @param {String} response responseText property.
		 * @param {Object} myObservable Instance of Ext.util.Observable class.
		 */
		parse: function(response, myObservable) {
			if (response) {
				var r = WGS.safeDecode(response);
				if (Ext.isObject(r.events)) {
					var e = r.events, obsrv = myObservable||observable;
					for (var eventName in e) {
						if (Ext.isArray(e[eventName])) {
							var args = e[eventName];
							args.unshift(eventName);
							obsrv.fireEvent.apply(obsrv, args);
						} else {
							obsrv.fireEvent(eventName);
						}
					}
					return e;
				}
			}
		}
	};
}();

Ext.ns('WGS.Ajax');

/**
 * WGS.util.Ajax.request
 *
 */
WGS.Ajax.request = function(options) {
	var observable = options.observable;
	if (!options.success) {
		Ext.apply(options,{
			success : Ext.emptyFn
		});
	}
	var interceptor = function(response) {
		var r = WGS.safeDecode(response.responseText);
		if (response.exception) {
			r.exception = response.exception; 
		}
		if (r.success === true) {
			if (Ext.isFunction(options.responseSuccess)) {
				options.responseSuccess.call(options.responseSuccessScope || options.scope || this, r, options);
			}
		} else 
		if (r.success === false) {
			if (Ext.isFunction(options.responseFailure)) {
				options.responseFailure.call(options.responseFailureScope || options.scope || this, r, options);
			}
		}
	};
	options.success = options.success.createInterceptor(interceptor);
	if (Ext.isFunction(options.success)) {
		var interceptor = function(response) {
			WGS.sys.Events.parse(response.responseText, observable);
		};
		options.success = options.success.createInterceptor(interceptor);
	}
	Ext.Ajax.request.call(Ext.Ajax, options);
};