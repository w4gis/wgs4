﻿Ext.ns('WGS.sys');

WGS.sys.ClosablePanel = Ext.extend(Ext.Panel, {
	constructor: function(config)
	{
		config = config||{};
		config.tools = config.tools||[];
		
		Ext.apply(this, config);
		
		if (this.closable !== false) {
			this.tools.push({
				id: 'close',
				handler: function() {
					this.hide();
					this.fireEvent('close');
				},
				scope: this
			});		
		}
		WGS.sys.ClosablePanel.superclass.constructor.call(this);
	}
});