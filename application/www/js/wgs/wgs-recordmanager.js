Ext.ns('WGS.sys');

WGS.sys.RecordManager = Ext.extend(Ext.Panel, {
	name: 'Record manager',
	version: '1.0.0',
	revision: '$Revision:$',
	revdate: '$Date:$',
	module: 'Core',

	/**
	 * Supported mode types:
	 * * update
	 * * add
	 * * multiedit
	 * * view
	 */

	createEditor: function(field)
	{
		var editor = {};
		var findLabel = function(field) {
			var wrapDiv = null;
			var label = null;	//find form-element and label?
			wrapDiv = field.getEl().up('div.x-form-element');
			if(wrapDiv) {
				label = wrapDiv.child('label');
			}
			if(label) {
				return label;
			}		//find form-item and label
			wrapDiv = field.getEl().up('div.x-form-item');
			if(wrapDiv) {
				label = wrapDiv.child('label');
			}
			if(label) {
				return label;
			}
		};

		var remarksText = this.remarksText;
		var qtipRender = function(scope){
			if (field.tooltip) {
				Ext.QuickTips.register({
					target:  field.type=='checkbox'? scope.imageEl: scope,
					title: remarksText,
					text: field.tooltip,
					enabled: true,
					showDelay: 0
				});
				var label = findLabel(scope);
				Ext.QuickTips.register({
					target:  label,
					title: remarksText,
					text: field.tooltip,
					enabled: true,
					showDelay: 0
				});
			}
		};
		switch (field.type) {
			case 'anchorselect':
			case 'linkselect':
			case 'select':
				editor = new Ext.ux.form.ReloadCombobox({
					editable: false,
					forceSelection: true,
					mode: 'local',
					store: new Ext.data.SimpleStore({
						fields: ['code', 'title']
					}),
					triggerAction: 'all',
					typeAhead: false,
					displayField: 'title',
					valueField: 'code',
					afterRender: function(){
						Ext.form.ComboBox.superclass.afterRender.call(this);
						qtipRender(this);
					},
					validator: field.allowBlank? undefined: function(v) {
						return editor.getValue()? true: false;
					},
					///////////////////////
					anyMatch: true,
					queryIgnoreCase: false,
					matchWordStart: false,
					editable: true,
					selectOnFocus: true
					///////////////////////
				});

				var loadFieldData = function(){
					if (field.store && field.store.records) {
						var data = [];
						for (var value in field.store.records) {
							data.push([value, field.store.records[value]]);
						}
						editor.store.loadData(data);
					}
				};

				editor.reloadData = function() {
					if (field.store.type != 'local') {
						editor.disable();
						field.store.comboStore.reload({
							callback: function(){
								loadFieldData();
								editor.enable();
							}
						});
					}
				};

				loadFieldData();
			break;
			case 'multiselect':
				
				editor = new Ext.ux.form.SuperBoxSelect({
					editable: false,
					forceSelection: true,
					mode: 'local',
					store: new Ext.data.SimpleStore({
						fields: ['code', 'title']
					}),
					triggerAction: 'all',
					typeAhead: false,
					displayField: 'title',
					valueField: 'code',
					afterRender: function(){
						Ext.form.ComboBox.superclass.afterRender.call(this);
						qtipRender(this);
					},
					validator: field.allowBlank? undefined: function(v) {
						return editor.getValue()? true: false;
					},
					disabled: !!field.readonly || this.mode == 'view',
					//maxHeight: 50,
					//maxCustomHeight: 60,
					///////////////////////
					anyMatch: true,
					queryIgnoreCase: false,
					matchWordStart: false,
					editable: true,
					selectOnFocus: true
					///////////////////////
				});
								
				editor.on('beforeadditem', function() {
					this.recordChanged = true;
				}, this);

				editor.on('clear', function() {
					this.recordChanged = true;
				}, this);
								
				editor.on('additem', function(){
					this.setHeight(this.getHeight());
				}, this, {delay: 50});
				
				var loadFieldData = function(){
					if (field.store && field.store.records) {
						var data = [];
						for (var value in field.store.records) {
							data.push([value, field.store.records[value]]);
						}
						editor.store.loadData(data);
					}
				};
				
				loadFieldData();
			break;
			case 'number':
				editor = new Ext.form.NumberField({
					enableKeyEvents: true,
					afterRender: function(){
						Ext.form.NumberField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'checkbox':
				editor = new Ext.form.Checkbox({
					afterRender: function(){
						Ext.form.Checkbox.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'datetime':
				/*
				editor = new Ext.form.DateField({
					format: 'd.m.y',
					afterRender: function(){
						Ext.form.DateField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
				*/
			break;
			case 'date':
				editor = new Ext.form.DateField({
					format: WGS.constants.Date.RecordManagerFormat,
					afterRender: function(){
						Ext.form.DateField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'textarea':
				editor = new Ext.form.TextArea({
					enableKeyEvents: true,
					height: field.height ? field.height : 80,
					afterRender: function(){
						Ext.form.TextArea.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'password':
				editor = new Ext.form.TextField({
					inputType: 'password',
					enableKeyEvents: true,
					afterRender: function(){
						Ext.form.TextField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'iconselect':
				editor = new Ext.ux.form.IconField({
					icon64x64: field.icon64x64,
					enableKeyEvents: true,
					afterRender: function(){
						Ext.form.TextField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
			break;
			case 'htmlarea':
				editor = new Ext.form.HtmlEditor({
					height: 100,
					enableKeyEvents: true,
					afterRender: function(){
						Ext.form.HtmlEditor.superclass.afterRender.call(this);
						qtipRender(this);
						this.addEvents({change: true});
					},
					listeners: {
						push: function(field, string) {
							editor.oldValue = string;
						},
						sync: function(field, string) {
							if (editor.oldValue != string) {
								editor.fireEvent("change");
							}
						}
					}
				});
			break;
			case 'custom':
				Ext.apply(field.customConfig, {
					name: field.name,
					mode: this.mode,
					maxLength: field.maxLength,
					rm: this
				});//'customeditor'
				editor = new Ext.ComponentMgr.create(field.customConfig, 'customeditor');
			break;
			default:
				editor = new Ext.form.TextField({
					enableKeyEvents: true,
					afterRender: function(){
						Ext.form.TextField.superclass.afterRender.call(this);
						qtipRender(this);
					}
				});
		}

		if (field.options) {
			Ext.apply(editor, field.options);
		}

		Ext.apply(editor, {
			anchor: '99%',
			fieldLabel: field.title,
			name: field.name,
			allowBlank: field.allowBlank==undefined? true: field.allowBlank,
			maxLength: field.maxLength,
			style: field.allowBlank==undefined? '': 'background-color: #FFFFC8; background-image: none;',
			readOnly: !!field.readonly || this.mode == 'view',
			disabled: field.disabled && this.mode != 'view'
		});
		

		if (!!field.readonly && field.addMode && this.mode == 'add') {
			Ext.apply(editor, {readOnly: false});
		}

		editor.on('change', function() {
			this.recordChanged = true;
		}, this);

		////EXPERIMENTAL////
		if (field.rmlisteners) {
			for (var e in field.rmlisteners) {
				editor.on(e, field.rmlisteners[e].createDelegate(this), this);
			}
		}
		////////////////////

		if (editor.readOnly) {
			editor.style = 'background-color: #E6ECF9; background-image: none;';
			editor.on('focus', function(){
				editor.blur();
			});

			if (field.type == 'checkbox') {
				editor.autoCreate = Ext.apply({}, editor.defaultAutoCreate);
				Ext.apply(editor.autoCreate, {disabled: ""});
			}
		}

		if (editor.maxLength) {
			if (field.type == 'textarea') {
				editor.on('keypress', function(c, e){
					var v = editor.getValue();
					if (v.length >= editor.maxLength &&
						e.getKey() != e.BACKSPACE &&
						e.getKey() != e.RIGHT &&
						e.getKey() != e.LEFT &&
						e.getKey() != e.DOWN &&
						e.getKey() != e.UP &&
						e.getKey() != e.DELETE &&
						e.getKey() != e.INSERT &&
						e.getKey() != e.HOME &&
						e.getKey() != e.END &&
						e.getKey() != e.PAGE_UP &&
						e.getKey() != e.PAGE_DOWN &&
						e.getKey() != e.F5) {
						e.stopEvent();
					}
				});
				editor.on('specialkey', function(){});
			} else {
				editor.autoCreate = Ext.apply({}, editor.defaultAutoCreate);
				Ext.apply(editor.autoCreate, {maxLength: editor.maxLength});
			}
		}
		
		if(field.value) {
			var v = Ext.isFunction(field.value) ? field.value() : field.value; 
			Ext.apply(editor, {value: v});
		}

		if (this.mode != 'multiedit') {
			return editor;
		} else {
			return {
				layout: 'form',
				items: {
					layout: 'column',
					items: [{
						items: new Ext.form.Checkbox({
							name: 'edit-'+field.name,
							disabled: !!field.readonly
						}),
						columnWidth: 0.05,
						bodyStyle: 'margin-top: -1px'
					},{
						layout: 'form',
						items: editor,
						columnWidth: 0.95
					}]
				}
			};
		}
	},

	getFieldCmp: function(fieldName)
	{
		return this.panel.getForm().findField(fieldName);
	},

	importRecord: function(record, multiEditRecords)
	{
		this.record = record;
		this.multiEditRecords = multiEditRecords;
		this.panel.getForm().loadRecord(record);
		this.fireEvent('importrecord', this.toPreviousRecordBtn,  this.toNextRecordBtn);
	},

	toPreviousRecord: function()
	{
		if (this.recordChanged) {
			Ext.MessageBox.confirm(this.confirmText, this.confirmMsg,
			function(btn) {
				if (btn == 'yes') {
					this.doApply();
				}
				this.fireEvent('topreviousrecord');
				this.recordChanged = false;
			}, this);
		} else {
			this.fireEvent('topreviousrecord');
			this.recordChanged = false;
		}
	},

	toNextRecord: function()
	{
		if (this.recordChanged) {
			Ext.MessageBox.confirm(this.confirmText, this.confirmMsg,
			function(btn) {
				if (btn == 'yes') {
					this.doApply();
				}
				this.fireEvent('tonextrecord');
				this.recordChanged = false;
			}, this);
		} else {
			this.fireEvent('tonextrecord');
			this.recordChanged = false;
		}
	},

	doApply: function()
	{
		if (this.mode == 'multiedit') {
			var f = {};
			var v = {};

			try {
				for (var i=0, item; item=this.panel.items.item(i); i++) {
					if (item.items.item(0)) {
						var c = item.items.item(0).items.item(0).items.item(0);
						f[c.getName()] = c.getValue()? 1: 0;
						var b = item.items.item(0).items.item(1).items.item(0);
						v[b.getName()] = b.getXType() == 'checkbox'? (b.getValue()? 1: 0) : b.getValue();
					}
				}
			} catch(e) {
			}

			Ext.each(this.multiEditRecords, function(rec){
				for (var i in v) {
					if (f['edit-'+i]) {
						rec.set(i, v[i]);
					}
				}
			});

		} else {
			if (this.record) {
				for (var i=0, item; item=this.panel.items.item(i); i++) {
					this.record.set(item.getName(), item.getXType() == 'checkbox'? (item.getValue()? 1: 0) : item.getValue());
				}
			}
		}
		this.recordChanged = false;
		this.fireEvent('changesapplied', this.record, this.multiEditRecords);
	},

	doOk: function()
	{
		this.fireEvent('applychanges', this.record, this.multiEditRecords);
	},

	doCancel: function()
	{
		this.recordChanged = false;
		this.fireEvent('cancelchanges', this.record, this.multiEditRecords);
	},

	recordChangedMonitor: function()
	{
		if (this.recordChanged != this._recordChanged) {
			this.fireEvent('recordstatechanged', this.recordChanged);
			this._recordChanged = !!this.recordChanged;
		}
		this.recordChangedMonitor.defer(50, this);
	},

	onRmResize: function(rm, width, height){
		if (this.rendered) {
			this.previousHeight = this.previousHeight||(this.panel.getHeight());
			var textareas = [];
			if (this.mode == 'multiedit') {
				try {
					for (var i=0, item; item=this.panel.items.item(i); i++) {
						if (item.items.item(0)) {
							var b = item.items.item(0).items.item(1).items.item(0);
							if (b.getXType() == 'htmleditor'/* ||
								item.getXType() == 'textarea'*/) {
								textareas.push(b);
							}
						}
					}
				} catch(e) {
				}
			}
			else {
				for (var i=0, item; item=this.panel.items.item(i); i++) {
					if (item.getXType() == 'htmleditor'/* ||
						item.getXType() == 'textarea'*/) {
						textareas.push(item);
					}
				}
			}
			for (i=0, h = (height - this.previousHeight)/textareas.length; i<textareas.length; i++) {
				var newHeight = parseInt(textareas[i].getHeight() + h);
				if (newHeight>50) {
					this.panel.setHeight(this.panel.getHeight()+h-(38/textareas.length));
					textareas[i].setHeight(newHeight-(38/textareas.length));
				}
			}
			this.previousHeight = this.panel.getHeight();
		}
	},

	constructor: function(config)
	{
		Ext.QuickTips.init();
		Ext.apply(this, config);
		Ext.applyIf(this, {
			mode: 'edit',
			recordChanged: false,
			_recordChanged: false
		});

		this.addEvents(
			'applychanges',
			'changesapplied',
			'cancelchanges',
			'tonextrecord',
			'topreviousrecord',
			'importrecord',
			'recordstatechanged'
		);

		var items = [];

		for (var i=0, field; field=this.fields[i]; i++) {
			if (field[this.mode+'Mode'] != false) {
				items.push(this.createEditor(field));
			}
		}

		this.panel = new Ext.form.FormPanel({
			anchor: '100% 100%',
			bodyStyle: 'padding: 10px',
			labelWidth: 120,
			frame: true,
			monitorValid: this.mode!='multiedit',
			items: items
		});

		WGS.sys.RecordManager.superclass.constructor.call(this, {
			header: false,
			bodyStyle: 'background-color: #EFEBEF;',
			border: false,
			autoScroll: true,
			items: this.iconUrl? new Ext.Panel({
			    layout: 'column',
			    border: false,
			    bodyStyle: "background-color: transparent;",
			    items: [{
			        width: 69,
			        anchor: '100% 100%',
			        border: false,
			        bodyStyle: "background-color: transparent;",
			        html: '<img src="'+this.iconUrl+'"/>'
			    	},
			    	Ext.apply(this.panel, {columnWidth: 1})
			    ]
			}): this.panel,
			buttonAlign: 'center'
		});

		this.on('applychanges', function(){
			if (this.recordChanged) {
				this.doApply();
			} else {
				this.doCancel();
			}
		}, this);


		this.on('afterrender', function(){
			this.on('resize', this.onRmResize, this);
		}, this/*, {delay:100}*/);//TODO:check this


		this.toPreviousRecordBtn = this.addButton({
			width: 75,
			iconCls: 'icon-previous',
			hidden: true,
			handler: this.toPreviousRecord,
			scope: this
		});

		this.panel.on('clientvalidation', function(p, valid){
			this.btnOk.setDisabled(!valid);
		}, this);

		this.btnOk = this.addButton({
			text: 'OK',
			formBind: true,
			handler: this.doOk,
			scope: this
		});

		this.addButton({
			text: '������',
			handler: this.doCancel,
			scope: this
		});

		this.toNextRecordBtn = this.addButton({
			width: 75,
			iconCls: 'icon-next',
			hidden: true,
			handler: this.toNextRecord,
			scope: this
		});

		this.toPreviousRecordBtn.on('dblclick', function(){
			return false;
		});

		this.toNextRecordBtn.on('dblclick', function(){
			return false;
		});

		if (this.mode == 'edit') {
			this.toPreviousRecordBtn.setVisible(!!this.arrows);
			this.toNextRecordBtn.setVisible(!!this.arrows);
		} else
		if (this.mode == 'view') {
			this.toPreviousRecordBtn.setVisible(!!this.arrows);
			this.toNextRecordBtn.setVisible(!!this.arrows);
		}
	}
});

WGS.sys.RecordManagerWindow = Ext.extend(WGS.sys.PinTabWindow, {
	getPinablePanel: function()
	{
		return this.recordManager;
	},

	getRecordManager: function()
	{
		return this.recordManager;
	},

	createPinPanel: function(config)
	{
		config.recordManager = config.pinablePanel;
		var pttp = config.pinToolTabPanel,
			pinPanel = new WGS.sys.RecordManagerPanel(config),
			onCancelChanges = function() {
				pttp.remove(pinPanel);
			};

		pinPanel.on('pinned', function(){
			pinPanel.on('cancelchanges', onCancelChanges, this);
		}, this);

		pinPanel.on('unpinned', function(){
			pinPanel.un('cancelchanges', onCancelChanges, this);
		}, this);

		return pinPanel;
	},

	onReady: function(callback, scope)
	{
		if (this.rendered) {
			callback.call(scope || window, this);
		}
		else {
			this.on('show', callback, scope || window, {single: true});
		}
		this.show();
	},

	constructor: function(config)
	{
		Ext.apply(this, config);

		if (!this.recordManager) {
			this.recordManager = new WGS.sys.RecordManager({
				header: false,
				fields: this.fields,
				mode: this.mode,
				arrows: this.arrows,
				iconUrl: this.iconUrl
			});

			this.recordManager.ptContainer = this;

			this.recordManager.on('cancelchanges', function(){
				if (this.autoClose) {
					this.close();
				}
			}, this);

			this.recordManager.on('applychanges', function(){
				if (this.autoClose) {
					this.close();
				}
			}, this);
		}

		Ext.apply(this, {
			layout: 'fit',
			closeAction: 'close',
			closable: true,
			constrainHeader: true,
			bodyStyle: 'padding: 5px;',
			modal: true,
			width: 600,
			minWidth: 400,
			minHeight: 200,
			maxHeight: 500,
			height: 200,
			items: this.recordManager,
			maximizable: true,
			buttonAlign: 'center',
			autoClose: this.autoClose||false
		});

		WGS.sys.RecordManagerWindow.superclass.constructor.call(this);

		if (this.mode == 'edit') {
			this.setIconClass(this.iconCls||'tf-edit');
			this.setTitle(this.title||'�������������� ������');
		} else
		if (this.mode == 'view') {
			this.setIconClass(this.iconCls||'tf-view');
			this.setTitle(this.title||'�������� ������');
		} else
		if (this.mode == 'multiedit') {
			this.setIconClass(this.iconCls||'tf-edit');
			this.setTitle(this.title||this.multieditModeText);
		} else
		if (this.mode == 'add') {
			this.setIconClass(this.iconCls||'tf-add');
			this.setTitle(this.title||'���������� ������');
		}

		this.on('show', function(){
			this.loadMask = new Ext.LoadMask(this.recordManager.getEl(), {msg: this.waitMsg});
			var size = this.recordManager.panel.getHeight() + 90;
			this.setHeight(size < this.minHeight? this.minHeight: (size > this.maxHeight? this.maxHeight: size));
			var xy = this.getPosition();
			this.setPagePosition(xy[0], xy[1]-100>0?xy[1]-100:100);
		}, this);

		this.on('close', function(){
			this.recordManager.fireEvent('close');
		}, this);
	}
});

WGS.sys.RecordManagerPanel = Ext.extend(Ext.Panel, {
	close: function()
	{
		if (this.recordManager &&
			this.recordManager.ptContainer &&
			this.pinToolTabPanel) {
			this.fireEvent('close');
			this.pinToolTabPanel.remove(this.recordManager.ptContainer);
		}
	},

	onReady: function(callback, scope)
	{
		callback.call(scope || window, this);
	},

	getRecordManager: function()
	{
		return this.recordManager;
	},

	constructor: function(config)
	{
		Ext.apply(this, config);

		if (!this.recordManager) {
			this.recordManager = new WGS.sys.RecordManager({
				fields: this.fields,
				mode: this.mode,
				arrows: this.arrows,
				iconUrl: this.iconUrl
			});

			this.recordManager.ptContainer = this;

			this.recordManager.on('cancelchanges', function(){
				this.recordManager.panel.getForm().reset();
			}, this);
		}

		if (this.recordManager.getEl()) {
			this.loadMask = new Ext.LoadMask(this.recordManager.getEl(), {msg: this.waitMsg});
		}
		else {
			this.recordManager.on('afterrender', function(){
				this.loadMask = new Ext.LoadMask(this.recordManager.getEl(), {msg: this.waitMsg});
			}, this);
		}

		Ext.apply(this, {
			layout: 'fit',
			header: true,
			bodyStyle: 'padding: 5px; background-color: #EFEBEF;',
			style: 'margin: 2px;',
			modal: true,
			items: this.recordManager,
			buttonAlign: 'center'
		});

		WGS.sys.RecordManagerPanel.superclass.constructor.call(this);

		this.on('close', function(){
			this.recordManager.fireEvent('close');
		}, this);

		if (this.mode == 'edit') {
			this.setIconClass(this.iconCls||'tf-edit');
			this.setTitle(this.title||'�������������� ������');
		} else
		if (this.mode == 'view') {
			this.setIconClass(this.iconCls||'tf-view');
			this.setTitle(this.title||'�������� ������');
		} else
		if (this.mode == 'multiedit') {
			this.setIconClass(this.iconCls||'tf-edit');
			this.setTitle(this.title||this.multieditModeText);
		} else
		if (this.mode == 'add') {
			this.setIconClass(this.iconCls||'tf-add');
			this.setTitle(this.title||'���������� ������');
		}
	}
});