Array.prototype.inArray = function(value) {
    for (var i in this)
        if (this[i] === value)
            return true;
    return false;
}

function isExistActiveX(activeX)
{
    if (Ext.isIE) {
        try 
        { 
            activeElement = new ActiveXObject(activeX);
            if (activeElement) return true;
        }
        catch(e) {
           return false;
        }
    }
    if (Ext.isGecko || Ext.isOpera || Ext.isSafari) {
        var count = 0;
        for (var i = 0; i < navigator.plugins.length; i++)
            if (navigator.plugins[i].name == activeX) count++;

        if (count > 0) return true;
        else return false;
    }
}

function getElementPosition(elemId) {
    var elem = (typeof(elemId) == 'string')? document.getElementById(elemId): elemId;
    var w = elem.offsetWidth;
    var h = elem.offsetHeight;
    var l = 0;
    var t = 0;
    while (elem) {
         l += elem.offsetLeft;
         t += elem.offsetTop;
         elem = elem.offsetParent;
    }
    return {left: l, top: t, width: w, height: h};
}

var WGS = {};

/* WGS.Exception */

WGS.Exception = function()
{
    var loginWindow;

    return {
        AUTHORIZATION_REQUIRED: 1,
        INCORRECT_USERNAME_OR_PASSWORD: 2,
        ALREADY_AUTHORIZED: 3,
        ALL_FIELDS_REQUIRED: 4,
        INSUFFICIENT_PRIVILEGES: 5,
        // errors > 1000
        USER_AUTHORIZED: 1001,
        TEST: 20009,
        
        Message:
        {
            // <= 1000
            AUTHORIZATION_REQUIRED: {code: 1, text: ''},
            INCORRECT_USERNAME_OR_PASSWORD: {code: 2, text: '�������� ����� / ������'},
            ALREADY_AUTHORIZED: {code: 3, text: '�� ��� ������������'},
            ALL_FIELDS_REQUIRED: {code: 4, text: '���������� ��������� ��� ����'},
            INSUFFICIENT_PRIVILEGES: {code: 5, text: '������������ ����'},
            // errors > 1000
            USER_AUTHORIZED: {code: 1001, text: '������������ �����������'}
        },
        
        Handle: function(component, jsonResponse, fn, scope)
        {
            if (jsonResponse && jsonResponse.exception)
            {
                var msg;
                var buttons;
                fn = (fn)? fn: function(){};
                scope = scope || this;
                switch(jsonResponse.exception)
                {
                    case WGS.Exception.AUTHORIZATION_REQUIRED:
                    {
                        msg = jsonResponse.message + '!<br><br>���������� ����� �������� �����������. ��������� ���� ������?';
                        buttons = Ext.Msg.YESNO;
                        fn = function(result)
                        {
                            if(result=='yes')
                            {
                                if (!loginWindow)
                                    loginWindow = new WGS.LoginWindow();
                                loginWindow.url = window.location.href;
                                loginWindow.show();
                            }
                        }
                    } break;
                    case WGS.Exception.ALL_FIELDS_REQUIRED:
                    case WGS.Exception.ALREADY_AUTHORIZED:
                    case WGS.Exception.INCORRECT_USERNAME_OR_PASSWORD:
                    case WGS.Exception.INSUFFICIENT_PRIVILEGES:
                    default:
                        msg = jsonResponse.message;
                        buttons = Ext.Msg.OK;
                }
                
                if (msg)                
                    Ext.WgsMsg.show({
                        title: '��������������',
                        msg: msg,
                        buttons: buttons,
                        fn: fn,
                        scope: scope,
                        icon: Ext.MessageBox.WARNING
                    });
                else {
                    //TODO: ��������� ���� �� ��������� �� receiveException � ��������, ��� ���� �������������� ����������
                    if (component.hasListener('exception'))
                    {
                        component.fireEvent('exception', jsonResponse.exception);
                        fn.call(scope);
                    }
                    else
                        Ext.WgsMsg.show({
                            title: '������',
                            msg: '�������������� ���������� � ����� "' + jsonResponse.exception + '"',
                            buttons: buttons,
                            fn: fn,
                            scope: scope,
                            icon: Ext.MessageBox.ERROR
                        });
                }
                
                return false;
            } else
            	if (fn) {
            		fn.call(scope);
            	}
            return true;
        }
    }
}();

WGS.PRIVILEGE = {};

WGS.MODULES = {};

WGS.Permissions = function() {
    return {
        Check: function(privilege) {
            if (privilege)
                return true;
            else
                Ext.WgsMsg.show({
                    title: '��������������',
                    msg: '������������ ���� �� ���������� ������ ��������.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            return false;
        }
    }
}();