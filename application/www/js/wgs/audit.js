var cardNav = function(incr){
        if(incr=='no'){
			Ext.MessageBox.confirm('Аудит :: Настройки','Отменить внесенные изменения?',function(btn){
				//console.log(btn)
				if(btn == 'yes'){
					var l = Ext.getCmp('card-wizard-panel').getLayout();        
					Ext.getCmp('ag').store.reload();
					Ext.getCmp('cg').store.reload();
					Ext.getCmp('pg').store.reload();
					Ext.getCmp('lg').store.reload();
					Ext.getCmp('card-prev').setDisabled(true);
					Ext.getCmp('card-next').setDisabled(false);
					Ext.getCmp('card-no').setDisabled(true);
					l.setActiveItem(0);	
					Ext.getCmp('mainAuditTabs').setActiveTab(0);
					Ext.getCmp('card-next').setText ( 'Далее &raquo;');
							
				}			
			})												
		}else{
		var l = Ext.getCmp('card-wizard-panel').getLayout();
        var i = l.activeItem.id.split('card-')[1];
        var next = parseInt(i, 10) + incr;
        l.setActiveItem(next);
        //console.log(Ext.getCmp('card-next'))
		Ext.getCmp('card-next').setText ((next===0) ? 'Далее &raquo;' : ((next===4) ? 'Применить изменения' : 'Следующий шаг &raquo;'));
		//if(next===3) Ext.getCmp('card-next').setHandler(function(){alert('!')} )
		//else Ext.getCmp('card-next').setHandler(function(incr){alert('!')} )
		Ext.getCmp('card-prev').setDisabled(next===0);
        Ext.getCmp('card-no').setDisabled(next===0);        
		Ext.getCmp('card-next').setDisabled(next===5);
		if(next === 5){
			var modified = Ext.getCmp('ag').store.getModifiedRecords()
			var data = [];
			dataToSend = {};
			
			for (var i in modified){
				data[i] = modified[i].data;				
			}
			console.log(data)
			dataToSend['actions'] = data
			
			modified = Ext.getCmp('cg').store.getModifiedRecords()
			data = [];
			for (var i in modified){
				data[i] = modified[i].data;				
			}
			
			dataToSend['controllers'] = data
			
			modified = Ext.getCmp('pg').store.getModifiedRecords()
			//console.log('!!',Ext.getCmp('pg'))
			data = [];
			for (var i in modified){
				data[i] = modified[i].data;				
			}		
			
			dataToSend['params'] = data
			
			modified = Ext.getCmp('lg').store.getModifiedRecords()
			//console.log('!!',Ext.getCmp('pg'))
			data = [];
			for (var i in modified){
				data[i] = modified[i].data;				
			}
			dataToSend['layers'] = data
			//console.log(Ext.util.JSON.encode(dataToSend))
			Ext.Ajax.request({
								url        : 'audit/updateSettings',
								success    : function(response, options) {
												//console.log('ok')
												Ext.getCmp('ag').store.reload();
												Ext.getCmp('cg').store.reload();
												Ext.getCmp('pg').store.reload();
												Ext.getCmp('lg').store.reload();
												
												Ext.getCmp('card-prev').setDisabled(true);
												
												Ext.getCmp('card-next').setDisabled(false);
												Ext.MessageBox.alert('Аудит :: Настройки','Изменения настроек успешно сохранены и вступают в силу для аудита всех действий пользователей, начиная с текущего момента времени.')
												l.setActiveItem(0);	
												Ext.getCmp('mainAuditTabs').setActiveTab(0)												
											},
								failure    : function() {
													// Сообщение об ошибке
													Ext.MessageBox.alert('Не удалось сохранить изменения. Обратитесь к администратору системы');
											},
								params    : { // параметры передаем
													data: Ext.util.JSON.encode(dataToSend)
											},
								method	: 'POST'
							});
			//console.log(dataToSend)
		}}
    };
Audit = function()
{

window.onresize = function(){
	if(param&&param.win)param.win.setHeight(Ext.getBody().getViewSize().height-48);
}


Grid  = {
        // Базовые параметры нашего окна
        getWinParams: function(){
                return {
				x:310,
				y:48,
				
                        width: Ext.getBody().getViewSize().width-290,
                        height: Ext.getBody().getViewSize().height-48,
					    title: 'Данные',                        
						shadow:false,
						resizable: false,
						draggable: false,
						closable:  false,
                        layout: 'fit',
                        bodyStyle: {
                            'background-color': '#FFFFFF'   
                        }
                }       
        },
        
        // Функция инициализации окна
        getWindow: function(params){
                !params ? params = this.getWinParams() : "";
                if(!this.win){
                        this.win = new Ext.Window(params);
                }
                return this.win;
				
        
				
        },
        
        // Ридер данных из объекта
        getJsonReader: function(){
                this.JsonReader = new Ext.data.JsonReader({
                
						root: 'record',           // Элемент объекта, содержащий данные
                        idProperty: 'id',       // Колнка, содержащая Уникальные данные ID
                        fields: [                       // Описание колонок, выводимых в Grid
                                {name: 'Id'},
                                {name: 'Data'},
								{name: 'User'}, 
								{name: 'Host'}, 
								{name: 'Controller'},								
                                {name: 'Action'},
                                {name: 'Parametr'},
                                {name: 'ZParametr'}
                        ]

                });	
				return this.JsonReader;
        },       		
		
        // Данные для Хранилища
		
		getProxy: function(proxyjs){
		this.proxy=new Ext.data.HttpProxy(    
		{
			url:proxyjs,
			
		});
			return this.proxy;
		},
        // Получаем хранилище данных
        getStore: function(proxyjs){
                reader = this.getJsonReader();
				proxy = this.getProxy(proxyjs);				
                this.store = new Ext.data.GroupingStore({
						totalproperty: 'total',
						id		: 'store',
						proxy	: proxy,
                        reader	: reader,
						remoteGroup : true,
						remoteSort : true,
						groupField  : 'Id',
                });
			return this.store;
        },
        
        // Получаем Таблицу для отображения данных
        getGrid: function(proxyjs){
	
		        store = this.getStore(proxyjs);
				store.load();
                if (!this.grid)
					this.grid = new Ext.grid.GridPanel({
						region: 'center',
			
            id: 'cregion',
            
						title	:'Отчет о пользовательской активности',
                        store	: store,           // хранилище. Важно! Required!
						listeners: {        
						},
                        cm		: new Ext.grid.ColumnModel({  // Модель колонок
                                columns: [              // Колонки
										{header: 'ID', dataIndex: 'Id', hidden: true},
                                        {header: 'Дата', dataIndex: 'Data', hidden: true },
                                        {header: 'Пользователь', dataIndex: 'User', hidden: true},
                                       {header: 'IP-адрес', dataIndex: 'Host', hidden: true},
                                        {header: 'Модуль', dataIndex: 'Controller' , hidden: true},
                                        {header: 'Действие', dataIndex: 'Action' , hidden: true},
                                        {header: 'Параметр', dataIndeLx: 'Parametr' , width: 200},
										{header: 'Значение параметра', dataIndeLx: 'ZParametr', width: 200}                                             
                                ],
								defaultSortable: true   
                        }),
						view : new Ext.grid.GroupingView({
            startCollapsed : false,
            forceFit     : true,
            groupTextTpl : '<div class="myGridTitle"><b>{[values.rs[0].data["Data"]]}</b>. Пользователь "<b>{[values.rs[0].data["User"]]}</b>" (IP-адрес: {[values.rs[0].data["Host"]]}) работал с модулем "<b>{[values.rs[0].data["Controller"]]}</b>". Действие: "<b>{[values.rs[0].data["Action"]]}</b>".</div>'
        }),
						bbar: new Ext.PagingToolbar({
            store: store,
			id: 'pb'			,
			pageSize: 36,
            displayInfo: true,
            displayMsg: 'Записи {0} - {1} из {2}',
            emptyMsg: "Нет записей, удовлетворяющих условиям",
			
        })
                })
			

			return this.grid;
        },
        
        showGrid: function(proxyjs){
                if(this.win) return false;
                
                // Инициализируем окно
                this.getWindow();
                
                // Инициализируем Грид
                this.getGrid(proxyjs);
                
                // Добавляем Грид к Окну
                this.win.add(this.grid);
                // Выводим окно
                this.win.show();
				//console.log(this.win.items.items[0].getView().grid.initialConfig.store.data.items)
                
        }
		
}
//-----------------------------------------------------------------------------------------------
   // Функция инициализации окна
   Filter ={
    
		getActionJsonReader: function(){
            this.JsonReader = new Ext.data.JsonReader({
            	root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'id',       // Коланка, содержащая Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'ID_ACTION'},
					{name: 'NAME_IN_GUI_ACTION'},
					{name: 'NAME_ACTION'},
					{name: 'ID_CONTROLLER'},
					{name: 'NAME_IN_GUI_CONTROLLER'},
					{name: 'NAME_CONTROLLER'}
                ]
            });					
            return this.JsonReader;
        },
		
		getActionProxy: function(){
			this.proxyA=new Ext.data.HttpProxy({
				url:'/audit/getLiActions?all=true'
			});
			return this.proxyA;
		},
        
		getActionStore: function(){
			reader = this.getActionJsonReader();
			proxy = this.getActionProxy();				
            this.storeA = new Ext.data.Store({
			    idIndex: 0,
				proxy:proxy,
                reader: reader
            });
			return this.storeA;
        },
		
		getControllerJsonReader: function(){
            this.JsonReader = new Ext.data.JsonReader({
				root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'id',       // Коланка, содержащая Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'ID_CONTROLLER'},
					{name: 'NAME_IN_GUI_CONTROLLER'},
					{name: 'NAME_CONTROLLER'}
                ]
            });	
			return this.JsonReader;
        },
		
		getControllerProxy: function(){
			this.proxyC = new Ext.data.HttpProxy({
				url:'/audit/getLiControllers?all=true'
			});
			return this.proxyC;
		},
        getControllerStore: function(){
			reader = this.getControllerJsonReader();
			proxy = this.getControllerProxy();				
            this.store = new Ext.data.Store({
				idIndex: 0,
				proxy:proxy,
                reader: reader,
            });
			return this.store;
        },
		getUserJsonReader: function(){
            this.JsonReader = new Ext.data.JsonReader({
             	root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'id',       // Коланка, содержащая Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'ID_USER'},
					{name: 'NAME_IN_GUI_USER'},
					{name: 'NAME_USER'},
					{name: 'USER_ROLE'},
                ]
            });	
            return this.JsonReader;
        },
		
		getUserProxy: function(){
			this.proxyU=new Ext.data.HttpProxy({
				url:'/audit/GetUserList?all=true'
			});
			return this.proxyU;
		},
        getUserStore: function(){
			reader = this.getUserJsonReader();
			proxy = this.getUserProxy();				
            this.store = new Ext.data.Store({
				idIndex: 0,
				proxy:proxy,
                reader: reader,
            });
			return this.store;
        },		
		
		getFilter: function(){
			storeAction = this.getActionStore();
			storeController = this.getControllerStore();
			storeUser = this.getUserStore();
			
			storeAction.load();
			storeController.load();
			storeUser.load();
		 
			this.form =new Ext.form.FormPanel( {		 
				autoScroll:true,
        		collapsible: true,
				split: false,
				region: 'west',
				id: 'wregion',
				title:'Параметры построения отчета',
				url:'/audit/filter',
				frame:false,
				bodyStyle:'padding:5px 2px 0',
				width: 350,
				fieldDefaults: {
					msgTarget: 'side',
					labelWidth: 75
				},        
				items: [
					new Ext.form.Label({
						html: '<h4 style="margin:auto;width:240px;padding-bottom:10px;padding-top:10px; text-align:center">Параметры отчета</h4>', 
						style: 'margin:5px;' 
					}),{
						xtype: 'combo',
						width: 205,
						emptyText: 'Все пользователи',
						fieldLabel: 'Пользователь',
						name: 'user_js',
						id: 'user_js',
						typeAhead: true,
						labelStyle: 'margin-left:10px;',
						triggerAction: 'all',
						lazyRender:true,
						minListWidth: 205,
						listWidth:'auto',
						listAlign: ['tr',[18,0]],
						mode: 'local',
						store:storeUser,
						valueField: 'ID_USER',
						displayField: 'NAME_IN_GUI_USER',	
						allowBlank:true
					},{
						xtype: 'combo',
						fieldLabel: 'Модуль',
						labelStyle: 'margin-left:10px;',
						emptyText: 'Все модули',
						width: 205,
						listWidth:'auto',
						listAlign: ['tr',[18,0]],
						listeners:{
						select: function(view, val){
							//console.log(view, val.data['ID_CONTROLLER'])
							Ext.getCmp('action_js').store.filterBy(
								function(item,id){
									if(!val.data['ID_CONTROLLER'] || item.data['ID_ACTION'] == 0)
										return true;
									else{
										return(item.data['ID_CONTROLLER'] == val.data['ID_CONTROLLER'])										
									}
								}
							)
							return true;
						}	
						},
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						mode: 'local',
						store:storeController,
						valueField: 'ID_CONTROLLER',
						displayField: 'NAME_IN_GUI_CONTROLLER',			
						name: 'controller_js',
						id: 'controller_js',
						allowBlank:true
					},{
						xtype: 'combo',
						fieldLabel: 'Действие',
						emptyText: 'Все действия',
						labelStyle: 'margin-left:10px;',
						width: 205,
						listWidth:'auto',
						listAlign: ['tr',[18,0]],
						queryAction: 'all',
						lastQuery: '',
						
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						mode: 'local',
						store:storeAction,
						valueField: 'ID_ACTION',
						displayField: 'NAME_IN_GUI_ACTION',
						name: 'action_js',
						id: 'action_js',
						allowBlank:true
					},new Ext.form.Label({
						html: '<h4 style="margin:auto;margin-bottom:0;padding-bottom:10px;padding-top:10px;width:240px; text-align:center">Период формирования отчета</h4>', 
						style: 'margin:5px' 
					}),{
						xtype: 'datefield',
						name: 'datebegin_js',
						fieldLabel: 'Начало',
						labelStyle: 'margin-left:10px;',						
						emptyText:'Не ограничено',
						onTriggerClick: 
							function (){
								if(this.disabled){
									return;
								}
							if(this.menu == null){
								this.menu = new Ext.menu.DateMenu({
									hideOnClick: false,
									focusOnSelect: false
								});
							}
							this.onFocus();
							Ext.apply(this.menu.picker,  {
								minDate : this.minValue,
								maxDate : this.maxValue,
								disabledDatesRE : this.disabledDatesRE,
								disabledDatesText : this.disabledDatesText,
								disabledDays : this.disabledDays,
								disabledDaysText : this.disabledDaysText,
								format : this.format,
								showToday : this.showToday,
								startDay: this.startDay,
								minText : String.format(this.minText, this.formatDate(this.minValue)),
								maxText : String.format(this.maxText, this.formatDate(this.maxValue))
							});
							this.menu.picker.setValue(this.getValue() || new Date());
							this.menu.show(this.el, "tl-bl?");
							var self = this;
							if(!Ext.getCmp("dateFromClearButton")){
								clearDateButton = new Ext.Button({
									renderTo: this.menu.picker.el.child("td.x-date-bottom",true),
									text: "Очистить",
									id: "dateFromClearButton",
									handler: function(){self.setValue(null);self.menu.hide();Ext.getCmp('timeFrom_js').setDisabled(true);Ext.getCmp('dateTo_js').setMinValue(null);	},
									style: "float:right; margin-right:0px;"
								})
								var table =  this.menu.picker.el.child("td.x-date-bottom > table",true)
								table.setStyle({float:'left', marginLeft: '0px'})
							}
							this.menuEvents('on');    
						},
						listeners: {
							change: function(value){
								if(value == "" || value == " " || !value){
									Ext.getCmp('timeFrom_js').setDisabled(true)
									Ext.getCmp('dateTo_js').setMinValue(null);									
								}
							},
							select: function(item){
								if(item.value != ""){
									Ext.getCmp('timeFrom_js').setDisabled(false)
									Ext.getCmp('dateTo_js').setMinValue(new Date(item.value.substr(6), item.value.substr(3, 2)-1, item.value.substr(0, 2)));
									
								}
								else{
									Ext.getCmp('timeFrom_js').setDisabled(true)
									Ext.getCmp('dateTo_js').setMinValue(null);
									
								}
							}
						},
						width: 205,
						allowBlank:true,
						id: 'dateFrom_js',
						format: "d.m.Y"
					},{
						xtype: 'timefield',
						name: 'timebegin_js',
						fieldLabel: 'Время',
						labelStyle: 'margin-left:10px;',
						disabled: true,
						width: 205,
						allowBlank:true,
						id: 'timeFrom_js',
						format: 'H:i'
					},new Ext.form.Label({
						html: '<hr style="margin-left:20px;margin-right:20px;margin-bottom:10px;margin-top:10px;"/>', 
						style: 'margin:3px 5px 5px 5px;' 
					}),{
						xtype: 'datefield',
						name: 'dateend_js',
						fieldLabel: 'Окончание',
						labelStyle: 'margin-left:10px;',
						onTriggerClick: 
							function (){
								if(this.disabled){
									return;
								}
							if(this.menu == null){
								this.menu = new Ext.menu.DateMenu({
									hideOnClick: false,
									focusOnSelect: false
								});
							}
							this.onFocus();
							Ext.apply(this.menu.picker,  {
								minDate : this.minValue,
								maxDate : this.maxValue,
								disabledDatesRE : this.disabledDatesRE,
								disabledDatesText : this.disabledDatesText,
								disabledDays : this.disabledDays,
								disabledDaysText : this.disabledDaysText,
								format : this.format,
								showToday : this.showToday,
								startDay: this.startDay,
								minText : String.format(this.minText, this.formatDate(this.minValue)),
								maxText : String.format(this.maxText, this.formatDate(this.maxValue))
							});
							this.menu.picker.setValue(this.getValue() || new Date());
							this.menu.show(this.el, "tl-bl?");
							var self = this;
							if(!Ext.getCmp("dateToClearButton")){
								clearDateButton = new Ext.Button({
									renderTo: this.menu.picker.el.child("td.x-date-bottom",true),
									text: "Очистить",
									id: "dateToClearButton",
									handler: function(){self.setValue(null);self.menu.hide();Ext.getCmp('timeTo_js').setDisabled(true);Ext.getCmp('dateFrom_js').setMaxValue(null);	},
									style: "float:right; margin-right:0px;"
								})
								var table =  this.menu.picker.el.child("td.x-date-bottom > table",true)
								table.setStyle({float:'left', marginLeft: '0px'})
							}
							this.menuEvents('on');    
						},
						listeners: {
							select: function(item){
								if(item.value != ""){
									Ext.getCmp('timeTo_js').setDisabled(false)
									Ext.getCmp('dateFrom_js').setMaxValue(new Date(item.value.substr(6), item.value.substr(3, 2)-1, item.value.substr(0, 2)));									
								}
								else{
									Ext.getCmp('timeTo_js').setDisabled(true)
									Ext.getCmp('dateFrom_js').setMaxValue(new Date());
								}
							}
						},
						width: 205,
						allowBlank:true,
						emptyText:'Не ограничено',
						id: 'dateTo_js',
						format: "d.m.Y"
					},{
						xtype: 'timefield',
						name: 'timeend_js',
						fieldLabel: 'Время',
						labelStyle: 'margin-left:10px;',
						disabled: true,
						
						width: 205,
						allowBlank:true,
						id: 'timeTo_js',
						format: 'H:i'
					},/*new Ext.form.Label({
						html: '<hr/>', 
						style: 'margin:3px 5px 5px 5px;' 
					}),*/new Ext.Button({
						text: '<b>Сформировать отчет</b>',
						style: 'margin:20px auto' ,
						width: 250,
						handler: function() {
							//console.log(Ext.getCmp('dateTo_js').value)
							var params = {
								userId :  ((Ext.getCmp('user_js').getValue() == " ")||(Ext.getCmp('user_js').getValue() == "Все пользователи")) ? null : Ext.getCmp('user_js').getValue(),  
								controller_js :  ((Ext.getCmp('controller_js').getValue() == " ")||(Ext.getCmp('controller_js').getValue() == "Все модули")) ? null : Ext.getCmp('controller_js').getValue(),
								action_js :  ((Ext.getCmp('action_js').getValue() == " ") || (Ext.getCmp('action_js').getValue() == "Все действия")) ? null : Ext.getCmp('action_js').getValue(),
								dateFrom_js :  Ext.getCmp('dateFrom_js').value == " " ? null : Ext.getCmp('dateFrom_js').value,
								dateTo_js :  Ext.getCmp('dateTo_js').value == " " ? null : Ext.getCmp('dateTo_js').value,
								timeFrom_js :  Ext.getCmp('timeFrom_js').getValue() == " " ? null : Ext.getCmp('timeFrom_js').getValue(),
								timeTo_js :  Ext.getCmp('timeTo_js').getValue() == " " ? null : Ext.getCmp('timeTo_js').getValue()
							}
							Grid.grid.store.baseParams = params
							Grid.grid.store.reload({   
								params: params,
								baseParams: params
							});
						}
					})]
			    })
				
			
	
		return this.form;
		}	
	}
//********ActionForm*************************************************************************	
	Action ={
		getJsonReader: function(){
                this.JsonReader = new Ext.data.JsonReader({
                	root: 'record',           // Элемент объекта, содержащий данные
                    idProperty: 'ID_ACTION',       // Коланка, содержащая Уникальные данные ID
                    fields: [                       // Описание колонок, выводимых в Grid								
                        {name: 'ID_CONTROLLER'},
						
						{name: 'ID_ACTION'},
						{name: 'NAME_IN_GUI_ACTION'},
						{name: 'NAME_ACTION'},
						{name: 'INCLUDED_ACTION'},
						{name: 'DESCRIPTION_ACTION'},
						{name: 'NAME_IN_GUI_CONTROLLER'},
						{name: 'NAME_CONTROLLER'},
						{name: 'DESCRIPTION_CONTROLLER'},
						{name: 'INCLUDED_CONTROLLER'}
                    ]
                });	
		    return this.JsonReader;
        },
		getJsonReaderC: function(){
            this.JsonReaderC = new Ext.data.JsonReader({
            url:'/audit/getLiControllers',     				
				root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'id',       // Коланка, содержащая Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'ID_CONTROLLER'},
					{name: 'NAME_IN_GUI_CONTROLLER'},
					{name: 'NAME_CONTROLLER'},
					{name: 'DESCRIPTION_CONTROLLER'},
					{name: 'INCLUDED_CONTROLLER'}
                    ]
            });	
			return this.JsonReaderC;
        },
		getJsonReaderP: function(){
            this.JsonReaderP = new Ext.data.JsonReader({
            	root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'id',       // Коланка, содержащая Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'ID_PARAM'},
					{name: 'NAME_IN_GUI_PARAM'},
					{name: 'NAME_PARAM'},
					{name: 'DESCRIPTION_PARAM'},
					{name: 'INCLUDED_PARAM'},{name: 'ID_ACTION'},
						{name: 'NAME_IN_GUI_ACTION'},
						{name: 'NAME_ACTION'},
						{name: 'INCLUDED_ACTION'},{name: 'DESCRIPTION_ACTION'}
                ]
			});	
			return this.JsonReaderP;
        },
		getJsonReaderL: function(){
            this.JsonReaderL = new Ext.data.JsonReader({
            	root: 'record',           // Элемент объекта, содержащий данные
                idProperty: 'fl_id',       // поле, содержащее Уникальные данные ID
                fields: [                       // Описание колонок, выводимых в Grid								
                    {name: 'fl_id'},
					{name: 'fl_name'},
					{name: 'fl_is_active'},
					{name: 'fl_li_id'},
					{name: 'fl_gr_id'},
					{name: 'fl_gr_name'}
				]
			});	
			return this.JsonReaderL;
        },
		getProxy: function(){
			this.proxy=new Ext.data.HttpProxy({
				url:'/audit/getLiActions'
			});
			return this.proxy;
		},
        getProxyC: function(){
			this.proxyC=new Ext.data.HttpProxy({
				url:'/audit/getLiControllers'
			});
			return this.proxyC;
		},
		getProxyP: function(){
			this.proxyP=new Ext.data.HttpProxy({
				url:'/audit/getLiParams'
			});
			return this.proxyP;
		},
		getProxyL: function(){
			this.proxyL=new Ext.data.HttpProxy({
				url:'/audit/getFeatureLayersList'
			});
			return this.proxyL;
		},
        getStore: function(){
			reader = this.getJsonReader();
			proxy = this.getProxy();				
            this.store = new Ext.data.GroupingStore({
			    proxy:proxy,
                reader: reader,
				//remoteGroup : true,
				groupField  : 'ID_CONTROLLER',
            });
			return this.store;
        },
		getStoreC: function(){
			readerC = this.getJsonReaderC();
			proxyC = this.getProxyC();				
            this.storeC = new Ext.data.Store({
				idIndex: 0,
				proxy:proxyC,
                reader: readerC,
            });
			return this.storeC;
        },
		getStoreP: function(){
			readerP = this.getJsonReaderP();
			proxyP = this.getProxyP();				
            this.storeP = new Ext.data.GroupingStore({
			    proxy:proxyP,
                reader: readerP,
				remoteGroup : true,
				groupField  : 'ID_ACTION',
            });
			return this.storeP;
        },
		getStoreL: function(){
			readerL = this.getJsonReaderL();
			proxyL = this.getProxyL();				
            this.storeL = new Ext.data.GroupingStore({
			    proxy:proxyL,
                reader: readerL,
				remoteGroup : true,
						groupField  : 'fl_gr_name',
            });
			return this.storeL;
        },
	
		getAction: function(){
			this.form =new Ext.form.FormPanel( {
				id:'card-wizard-panel',
				title:'Настройки',
				layout:'card',
				activeItem: 0, // index or id
				bbar: ['->', {
					id: 'card-no',
					text: 'Отменить изменения',
					handler: function(){cardNav('no')},
					disabled: true
				},'-',{
					id: 'card-prev',
					text: '&laquo; Предыдущий шаг',
					handler: function(){cardNav(-1)},
					disabled: true
				},{
					id: 'card-next',
					text: 'Далее &raquo;',
					handler: function(){cardNav(1)}
    			}],
				items: []
				//autoScroll:true
            })

			return this.form;
		},

	showAction: function(){
        /**
		* 	CheckBox'ы для каждого грида
		*/
		var checkColumn = new Ext.grid.CheckColumn({
			header: 'Включено в аудит?',
			dataIndex: 'INCLUDED_ACTION',
			id: 'check', width: 120,
			fixed: true
			//width: 55
		});
		var checkColumnC = new Ext.grid.CheckColumn({
			header: 'Включен в аудит?',
			dataIndex: 'INCLUDED_CONTROLLER',
			id: 'check1', width: 120,
			fixed: true
			//width: 55
		});var checkColumnP = new Ext.grid.CheckColumn({
			header: 'Включен в аудит?',
			dataIndex: 'INCLUDED_PARAM',
			id: 'check2', 
			width: 120,
			fixed: true
			//width: 55
		});
		var checkColumnL = new Ext.grid.CheckColumn({
			header: 'Включен в аудит?',
			dataIndex: 'fl_is_active',
			id: 'check3', 
			width: 120,
			fixed: true
			//width: 55
		});
		
		/**
		* 	Кнопки "Выделить все" и "снять все" для каждого грида
		*/
		
		var selectAllButton = new Ext.Button({
			text: 'Выделить все',
			handler: function() {					
				grid.store.each(function(record){
					record.set('INCLUDED_ACTION', 1);
				});		
			}
		});

        var deselectAllButton = new Ext.Button({
			text: 'Снять все',
			handler: function() {					
				grid.store.each(function(record){
					record.set('INCLUDED_ACTION', 0);
				});		
			}
		});
		
		var selectAllButtonC = new Ext.Button({
			text: 'Выделить все',
			handler: function() {					
				gridC.store.each(function(record){
					record.set('INCLUDED_CONTROLLER', 1);
				})		
			}
		});

        var deselectAllButtonC = new Ext.Button({
			text: 'Снять все',
			handler: function() {					
				gridC.store.each(function(record){
					record.set('INCLUDED_CONTROLLER', 0);
				});		
			}
		});
		
		var selectAllButtonP = new Ext.Button({
			text: 'Выделить все',
			handler: function() {					
				gridP.store.each(function(record){
					record.set('INCLUDED_PARAM', 1);
				});		
			}
		});

        var deselectAllButtonP = new Ext.Button({
			text: 'Снять все',
			handler: function() {					
				gridP.store.each(function(record){
					record.set('INCLUDED_PARAM', 0);
				});		
			}	
		});
		
		var selectAllButtonL = new Ext.Button({
			text: 'Выделить все',
			handler: function() {					
				gridL.store.each(function(record){
					record.set('fl_is_active', 1);
				});		
			}
		});

        var deselectAllButtonL = new Ext.Button({
			text: 'Снять все',
			handler: function() {					
				gridL.store.each(function(record){
					record.set('fl_is_active', 0);
				})		
			}
		});
		var expandAllButtonL = new Ext.Button({
			text: 'Раскрыть все группы слоев',
			handler: function() {					
				gridL.view.expandAllGroups();				
			}
		});
		var collapseAllButtonL = new Ext.Button({
			text: 'Свернуть все группы слоев',
			handler: function() {					
				gridL.view.collapseAllGroups();				
			}
		});
			
		/**
		*	Создаем окно мастера настройки Аудита
		*/		
		
        this.getAction();
        
		function showPrompt(btn,notFirst){
			notFirst = notFirst || false
			var msg = 'Отключение мониторинга пользовательской активности снижает уровень безопасности системы. Если Вы уверены, что хотите продолжить, укажите причину отключения модуля Аудита. Введенная информация будет отражена в отчете о пользовательской активности.'
			if(notFirst)
				msg = 'Отключение мониторинга пользовательской активности снижает уровень безопасности системы. Если Вы уверены, что хотите продолжить, укажите причину отключения модуля Аудита. Введенная информация будет отражена в отчете о пользовательской активности.<p><b>Пожалуйста, укажите причину отключения Аудита</b></p>'
			Ext.MessageBox.prompt(
											'Подтверждение операции',
											msg,
											function(btn2, value){
												if(btn2 == 'ok' && Ext.isEmpty(value)) {showPrompt(true); return false}
												if(btn2 == 'ok'){
													Ext.Ajax.request({
														url        : 'audit/setAuditActivity',
														params	: {is_active: false, reason: value},
														success    : function(response, options) {
															btn.setIconClass('icon-close-cross')
															btn.setText('<b>Мониторинг отключён</b>')									
															Ext.MessageBox.alert('Аудит','Мониторинг пользовательской активности в системе ' + ('отключен'));
														},
														failure : function(){
															Ext.MessageBox.alert('Аудит','Не удалось применить изменения...');
														}
													})
												}else btn.toggle();
											},
											this,
											true
										)
		}
		var intro = new Ext.Window( {
			x:0,
			y:0,
			closable:false,
			id: 'card-0',
			title:'Аудит :: Мастер настройки :: Добро пожаловать',
			layout:'border',
			onShow: function(){
				Ext.Ajax.request({
					url        : 'audit/getAuditActivity',
					success    : function(response, options) {
						var res = response.responseText;
						if(Ext.get('activityBtn').dom == activityBtn)
							activityBtn = new Ext.Button({
								text: ((res>1) ? '<b>Мониторинг активен</b>' : '<b>Мониторинг отключён</b>'),
								iconCls: ((res>1) ? 'icon-accept-tick' : 'icon-close-cross'),		
								renderTo: 'activityBtn',
								enableToggle: true,
								style:'font-weight:bold',
								pressed: (res>1),
								toggleHandler: function(btn, state) {									
									if(!state)
										showPrompt(btn)
									else
									Ext.Ajax.request({
										url        : 'audit/setAuditActivity',
										params	: {is_active: state},
										success    : function(response, options) {
											btn.setIconClass(((state) ? 'icon-accept-tick' : 'icon-close-cross'))
											btn.setText(((state) ? '<b>Мониторинг активен</b>' : '<b>Мониторинг отключён</b>'))									
											Ext.MessageBox.alert('Аудит','Мониторинг пользовательской активности в системе ' + ( (state) ? 'активирован':'отключен'));
										},
										failure : function(){
											Ext.MessageBox.alert('Аудит','Не удалось применить изменения...');
										}
										
									})
				
								}			
							})
					}
				})			
			},
			items:[{
				region: 'center',
				cmargins: '5 5 5 5',
				html: "<div style='text-align:justify;padding:10px; max-width:500px;'>"+
				"&nbsp;&nbsp;&nbsp;&nbsp;Добро пожаловать в мастер настройки мониторинга пользовательской активности в системе WGS3"+
				".<p>&nbsp;&nbsp;&nbsp;&nbsp;Вы можете активировать или отключить модуль мониторинга пользовательской активности в системе."+
				" Отключив данный модуль, Вы больше не сможете отслеживать действия пользователей WGS3."+
				"</p><div style='margin-left:auto; margin-right:auto; padding:10px;'><span style='float:left;line-height:20px;'>Текущее состояние:&nbsp;&nbsp;&nbsp;&nbsp;</span><span id='activityBtn' ></span></div>&nbsp;&nbsp;&nbsp;&nbsp;В ходе работы с мастером вы сможете выбрать системные модули и пользовательские действия, данные о которых будут учитываться"+
				" при формировании отчетности об активности в системе.<br/>&nbsp;&nbsp;&nbsp;&nbsp; Нажмите кнопку <b>\"Далее\"</b> для настройки параметров.</div>"
			}]//,
			//autoScroll:true
		})
	
		this.form.add(intro)  
		
		storeC = this.getStoreC();
		storeC.load();
        var gridC = new Ext.grid.EditorGridPanel({
			viewConfig: {
				forceFit: true
			},
			region: 'center',
			id:'cg',autoHeight	: true,
			plugins: [checkColumnC],
			title	:'Системные модули',
            store	: storeC,           // хранилище. Важно! Required!
			listeners: {        
			},
            cm		: new Ext.grid.ColumnModel({  // Модель колонок
                    columns: [              // Колонки
						{header: 'ID', dataIndex: 'ID_CONTROLLER', hidden: true},
						{header: 'Наименование', dataIndex: 'NAME_IN_GUI_CONTROLLER' , width: 300, fixed:true},
						checkColumnC,
						{header: 'Описание', dataIndex: 'DESCRIPTION_CONTROLLER' , width: 550}
																                                             
                    ],
					defaultSortable: true   
            }),
			tbar: ['-',selectAllButtonC,'-',deselectAllButtonC, '-']
			
						
        })
		store = this.getStore();
		store.load();
        var grid = new Ext.grid.EditorGridPanel({
			view : new Ext.grid.GroupingView({
				startCollapsed : false,
				forceFit     : true,
				groupTextTpl : '{[values.rs[0].data["NAME_IN_GUI_CONTROLLER"]]}'
			}),layout: 'fit',
			id:'ag',
			region: 'center',
			autoHeight	: true,
			plugins: [checkColumn],
			title	:'Действия пользователя',
            store	: store,           // хранилище. Важно! Required!
			listeners: {        
			},
            cm		: new Ext.grid.ColumnModel({  // Модель колонок
                        columns: [              // Колонки
							{header: 'ID', dataIndex: 'ID_ACTION', hidden: true},
                            {header: 'Наименование', dataIndex: 'NAME_IN_GUI_ACTION' , width: 300, fixed:true},
							checkColumn,
							{header: 'Описание', dataIndex: 'DESCRIPTION_ACTION' , width: 550},
							{header: 'ID_CONTROLLER', dataIndex: 'ID_CONTROLLER', hidden: true}
                          
															                                             
                        ],
						defaultSortable: true   
            }),
			tbar: ['-',selectAllButton,'-',deselectAllButton, '-']
		})
					
		var first = new Ext.Window({
			x:0,
			y:0,
			closable:false,
			id: 'card-1',
			title:'Аудит :: Мастер настройки :: Шаг 1 из 4',
			layout:'border',
			items:[{
				region: 'north',
				cmargins: '5 0 0 0',
				html: "<div style='padding:10px;'><h4>&nbsp;&nbsp;&nbsp;&nbsp;Шаг 1. Выбор системных модулей.</h4><br/>&nbsp;&nbsp;&nbsp;&nbsp;Выберите модули, факт взаимодействия пользователя с которыми, будет учитываться при формировании отчетности. После завершения нажмите кнопку \"Следующий шаг\".<br/></div>"
				},
				gridC
			],
			autoScroll:true
		})		   
		
		this.form.add(first)  
		
		
		
		
		var second = new Ext.Window({
			x:0,
			y:0,
			closable:false,
			id: 'card-2',
			title:'Аудит :: Мастер настройки :: Шаг 2 из 4',
			//bodyStyle:'padding:5px 5px 0',
			layout:'border',
			items:[{
				//title: 'Footer',
				region: 'north',
				//height: 150,
				//minSize: 75,
				//maxSize: 250,
				cmargins: '5 0 0 0',
				html: "<div style='padding:10px;'><h4>&nbsp;&nbsp;&nbsp;&nbsp;Шаг 2. Выбор действий пользователя.</h4><br/>&nbsp;&nbsp;&nbsp;&nbsp;Выберите виды деятельности пользователя, данные о которых, будут сохраняться для формирования отчетности. После завершения нажмите кнопку \"Следующий шаг\".<br/></div>"
				},
				grid
			],
			autoScroll:true
			
		})		   
		this.form.add(second)  
		
		storeP = this.getStoreP();
		storeP.load();
        var gridP = new Ext.grid.EditorGridPanel({
			view : new Ext.grid.GroupingView({
				startCollapsed : false,
				forceFit     : true,
				groupTextTpl : '{[values.rs[0].data["NAME_IN_GUI_ACTION"]]}'
			}),region: 'center',id:'pg',
			autoHeight	: true,
			plugins: [checkColumnP],
			title	:'Параметры действий пользователей',
            store	: storeP,           // хранилище. Важно! Required!
			listeners: {        
			},
            cm		: new Ext.grid.ColumnModel({  // Модель колонок
                    columns: [              // Колонки
						{header: 'ID', dataIndex: 'ID_PARAM', hidden: true},
						{header: 'ID_ACTION', dataIndex: 'ID_ACTION', hidden: true},
						{header: 'Наименование', dataIndex: 'NAME_IN_GUI_PARAM', width: 300, fixed:true},
						checkColumnP,
						{header: 'Описание', dataIndex: 'DESCRIPTION_PARAM', width: 550}
															                                             
                    ],
					defaultSortable: true   
            }),
			tbar: ['-',selectAllButtonP,'-',deselectAllButtonP, '-']
			
						
        })
		
		var third = new Ext.Window({
			x:0,
			y:0,
			closable:false,
			id: 'card-3',
			title:'Аудит :: Мастер настройки :: Шаг 3 из 4',
			//bodyStyle:'padding:5px 5px 0',
			layout:'border',
			items:[{
				//title: 'Footer',
				region: 'north',
				//height: 150,
				//minSize: 75,
				//maxSize: 250,
				cmargins: '5 0 0 0',
				html: "<div style='padding:10px;'><h4>&nbsp;&nbsp;&nbsp;&nbsp;Шаг 3. Выбор сохраняемых параметров.</h4><br/>&nbsp;&nbsp;&nbsp;&nbsp;Выберите параметры действий пользователя, которые могут потребоваться для формирования отчетности. После завершения нажмите кнопку \"Следующий шаг\".<br/></div>"
				},
				gridP
			],
			autoScroll:true
			
		})		   
		this.form.add(third)  
		
		storeL = this.getStoreL();
		storeL.load();
        var gridL = new Ext.grid.EditorGridPanel({
			
			region: 'center',id:'lg',
			autoHeight	: true,
			plugins: [checkColumnL],
			title	:'Слои ЭГП',
            store	: storeL,           // хранилище. Важно! Required!
			listeners: {        
			},
			view : new Ext.grid.GroupingView({
				startCollapsed : true,
				forceFit     : true,
				groupTextTpl : '{[values.rs[0].data["fl_gr_name"]]}'
			}),
            cm		: new Ext.grid.ColumnModel({  // Модель колонок
                    columns: [              // Колонки
						{header: 'ID', dataIndex: 'fl_id', hidden: true},
						{header: 'Наименование слоя', dataIndex: 'fl_name',width:300,fixed:true},
						{header: 'Наименование группы', dataIndex: 'fl_gr_name',hidden:true},						
						{header: 'flgr_id', dataIndex: 'fl_gr_id',hidden:true},						
						checkColumnL										                                             
                    ],
					defaultSortable: true   
            }),
			tbar: ['-',selectAllButtonL,'-',deselectAllButtonL,'-','-',expandAllButtonL, '-',collapseAllButtonL, '-']	
						
        })
		var fourth = new Ext.Window({
			x:0,
			y:0,
			closable:false,
			id: 'card-4',
			title:'Аудит :: Мастер настройки :: Шаг 4 из 4',
			//bodyStyle:'padding:5px 5px 0',
			layout:'border',
			items:[{
				region: 'north',
				cmargins: '5 0 0 0',
				html: "<div style='padding:10px;'><h4>&nbsp;&nbsp;&nbsp;&nbsp;Шаг 4. Выбор слоев ЭГП для мониторинга.</h4><br/>&nbsp;&nbsp;&nbsp;&nbsp;Укажитеите слои ЭГП, которые могут потребоваться для формирования отчетности. После завершения нажмите кнопку \"Применить изменения\".<br/></div>"
				
				},
				gridL
			],
			autoScroll:true
			
		})		   
		this.form.add(fourth)		
		return this.form;
		}
	
	}


//********TabPanel***************************************************************************	
	param ={
    getWinParams2: function(){
                return {
				x:0,
				y:48,
				
                        width: '100%',
                        height: Ext.getBody().getViewSize().height-48,
					//	autoHeight: true,
                        title: 'ПО информационной безопасности',
                       
                        shadow:false,
						resizable: false,
						draggable: false,
						closable:  false,
                        layout: 'fit',
                        bodyStyle: {
                                'background-color': '#FFFFFF'   
                        }
                }       
        },
       getWindow2: function(params){
                !params ? params = this.getWinParams2() : "";
                if(!this.win){
                        this.win = new Ext.Window(params);
                }
                return this.win;
        },

		getFilter: function(){
                Filter.getFilter();
                return this.adress;
        },
		 getparam: function(){
			this.form =new Ext.Panel({
				title: 'Формирование отчетности',
				layout: 'border',
				id: 'viewport',
				//renderTo: Ext.getBody(),
				items: [
					Filter.getFilter(), Grid.getGrid('/audit/getTableAudit')]
					});
				
			return this.form;
		},
		showparam: function(){
                if(this.win) return false;
                this.getWindow2();
                this.getparam();
				
				var Tabs =new Ext.TabPanel({
					id: 'mainAuditTabs',
					renderTo: Ext.getBody(),
					activeTab: 0,
					items: [this.form]
				})
				Tabs.add(Action.showAction());
				this.win.add(Tabs);
  			// Выводим окно
			/*clearDateButton = new Ext.Button({
				renderTo: Ext.getCmp('dateFrom_js').el.child("td.x-date-bottom",true"),
				text: "Clear Date",
				handler: function(){alert('!')}
			})*/
                this.win.show();
					
        }
	
	}
	
	
	
	
	
	
    return {
        init: function(){
			param.showparam();
			Grid.grid.store.on('load', function(){
			
			var rows = Grid.grid.store.data.items;
			for (var i in rows)
			if (rows[i].data)
				if (rows[i].data.Parametr == "")
					if(Grid.grid.getView().getRow(i).style)
					Grid.grid.getView().getRow(i).style.display = 'none';
				else
					if(Grid.grid.getView().getRow(i).style)
					Grid.grid.getView().getRow(i).style.display = 'normal';});
															
		}
	}
}();