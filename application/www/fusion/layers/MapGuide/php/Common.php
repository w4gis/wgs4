<?php
//ini_set('display_errors',1);
function SetTrueLocalizedPath(){

	SetLocalizedFilesPath(FUSION_TEXT_DIR);   

}

$isAjax = false;
if(!function_exists ( 'GetClientIp'))
	$isAjax = true;

if(!function_exists ( 'GetClientIp')){
	include 'func/GetClientIp.php';
}

if(!function_exists ( 'GetClientAgent')){
	include 'func/GetClientAgent.php';
}

if(!function_exists ( 'GetDefaultLocale' )){
	include 'func/GetDefaultLocale.php';
}

if(!function_exists ( 'GetLocalizationPath' )){
	include 'func/GetLocalizationPath.php';
}

if(!function_exists ( 'GetLocalizationPath')){
	include 'func/GetLocalizationPath.php';
}

if(!function_exists ( 'GetSurroundVirtualPath')){
	include 'func/GetSurroundVirtualPath.php';
}

if(!function_exists ( 'GetSurroundVirtualPath')){
	include 'func/GetSurroundVirtualPath.php';
}

if(!function_exists ( 'GetClientOS')){
	include 'func/GetClientOS.php';
}

if(!function_exists ( 'EscapeForHtml')){
	include 'func/EscapeForHtml.php';
}

if(!function_exists ( 'GetDecimalFromLocalizedString' )){
	include 'func/GetDecimalFromLocalizedString.php';
}
/**
 * Common
 *
 * $Id: Common.php 2467 2011-11-22 07:07:51Z liuar $
 *
 * Copyright (c) 2007, DM Solutions Group Inc.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 * Purpose: common include file for all server side scripts, responsible for
 *          setting up the user and site connection.
 *****************************************************************************/

/*
 * If MapGuide Web Server Extensions are not installed in the default
 * location, you may need to edit these values to reflect the actual
 * installation path.  You do not need to modify any other values in
 * this file to configure Fusion.
 */

require_once(dirname(__FILE__) . '/../../../../../setup.php');


$defaultExtensionDir = WEB_EXTENSIONS_DIR;//dirname(__FILE__)."/../../../../";
$defaultInstallDir = realpath($defaultExtensionDir)."/../../";
$initializationErrorOccurred = false;
$initializationErrorMessage = null;
$initializationErrorStackTrace = null;
$initializationErrorDetail = null;

/**
 * Developer's notes:
 *
 * This file should be included by all server-side scripts.  It includes some
 * default paths to install locations and manages all session-related and
 * user-related stuff except for actually creating a session.
 *
 * It also creates a MgResourceService instance for use by other scripts
 * and recreates an MgMap instance from the session if a mapname is passed.
 *
 * For widgets that are installed outside of the Fusion directory structure,
 * the value of $extensionDir can be set before including this script to avoid
 * some problems.
 */

//widgets outside Fusion can set the $extensionDir before including Common.php
//TODO: I don't think this is necessary anymore (PS)
if (!isset($extensionDir)){
    $installDir = $defaultInstallDir;
    $extensionDir = $defaultExtensionDir;
}


include dirname(__FILE__)."/Constants.php";
// Initialize
//echo $extensionDir. "/webconfig.ini";
//var_dump($extensionDir. "/webconfig.ini");
//MgInitializeWebTier($extensionDir. "/webconfig.ini");
	//echo file_get_contents($extensionDir. "/webconfig.ini");
global $user;
try
{
	
	//var_dump(MgInitializeWebTier($extensionDir. "/webconfig.ini"));
	//echo '!';
	MgInitializeWebTier($extensionDir. "/webconfig.ini");
	//echo('!');
    /* If no session has been established yet, then we use the credentials passed
     * to this script to connect to the site server.  By default, we use the
     * Anonymous user.
     */
    if (!isset($_REQUEST['session'])) {
       //echo('!');
        $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : MGAUTHOR_USERNAME;
        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : MGAUTHOR_PASSWORD;
        //var_dump($password);
        $user = new MgUserInformation($username, $password);
        $user->SetClientIp(GetClientIp());
        $user->SetClientAgent(GetClientAgent());
        //var_dump($user); 
        $siteConnection = new MgSiteConnection();
        $siteConnection->Open($user);
    } else {
        /* If a session has previously been established, we can connect using the
         * credentials that the user logged in with previously ... these are stored
         * in the MapGuide session.
         * It is possible for the user to re-authenticate using a different set of
         * credentials.  Handle this here, but keep the session the same.
         */
        $sessionID = $_REQUEST['session'];
        //session_id(str_replace('_', '-', $sessionID));
        session_start();

        /* current user is re-authenticating or not? */
        if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {
            $user = new MgUserInformation($_REQUEST['username'], $_REQUEST['password']);
            $user->SetClientIp(GetClientIp());
            $user->SetClientAgent(GetClientAgent());
            $user->SetMgSessionId($sessionID);
        } else {
            $user = new MgUserInformation($sessionID);
            $user->SetClientIp(GetClientIp());
            $user->SetClientAgent(GetClientAgent());
        }

        /* open a connection to the site.  This will generate exceptions if the user
         * is set up properly - not found, invalid password etc
         */
        $siteConnection = new MgSiteConnection();
        $siteConnection->Open($user);

        /* MgUserInformation doesn't tell us who is logged in so we store it
         * in the php session for convenience
         */
        if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {
            $_SESSION['username'] = $_REQUEST['username'];
        }
        //echo "<current user: >".$_SESSION['username']. '</current>';
    }

    //common resource service to be used by all scripts
    $resourceService = $siteConnection->CreateService(MgServiceType::ResourceService);

    if (isset($_REQUEST['mapname'])) {
        $mapName = $_REQUEST['mapname'];
        $mapResourceID = new MgResourceIdentifier( 'Session:'.$sessionID.'//'.$mapName.'.MapDefinition');
        $mapStateID = new MgResourceIdentifier('Session:'.$sessionID.'//'.$mapName.'.'.MgResourceType::Map);
    }
    


}
catch (MgException $e)
{
	echo 'hm' .  $e->GetDetails();
    $initializationErrorMessage = $e->GetExceptionMessage();
    $initializationErrorDetail = $e->GetDetails();
    $initializationErrorStackTrace = $e->GetStackTrace();
    $initializationErrorOccurred = true;
    //echo "{success: false, message:'".$initializationErrorMessage."'}";
    //DisplayInitializationErrorHTML();
}

function InitializationErrorOccurred()
{
    global $initializationErrorOccurred;
    return $initializationErrorOccurred;
}

function DisplayInitializationErrorHTML()
{
    global $initializationErrorMessage, $initializationErrorDetail;
    echo "<table class=\"RegText\" border=\"0\" cellspacing=\"0\" width=\"100%\">";
    echo "<tr><td class=\"Title\">Error<hr></td></tr>";
    if($initializationErrorMessage != null)
    {
        $message = $initializationErrorMessage;
        while(strpos($message, '\n')){
            $message = str_replace('\n', '<br/>', $message);
        }
        echo "<tr><td>" . $message . "</td></tr>";
    }
    if ($initializationErrorDetail != null && (strlen($initializationErrorDetail) - strlen($initializationErrorMessage) > 4))
    {
        $detail = $initializationErrorDetail;
        while(strpos($detail, '\n'))
        {
            $detail = str_replace('\n', '<br/>', $detail);
        }
        echo "<tr><td>" . $detail . "</td></tr>";
    }
    echo "</table>";
}

function DisplayInitializationErrorText()
{
    global $initializationErrorMessage, $initializationErrorDetail, $initializationErrorStackTrace;
    echo "ERROR: " . $initializationErrorMessage . "\n";
    echo $initializationErrorDetail . "\n";
    echo $initializationErrorStackTrace . "\n";
}





?>
