<?php
function GetClientOS() {
    if(!isset($_SERVER['HTTP_USER_AGENT']))
        return 0;
    $agent = $_SERVER['HTTP_USER_AGENT'];
    if(strpos($agent, "Windows") != FALSE)
        return 0;
    else if(strpos($agent, "Macintosh") != FALSE)
        return 1;
    return 2;
}
?>
