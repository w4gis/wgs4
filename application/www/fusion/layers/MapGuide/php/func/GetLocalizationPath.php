<?php
function GetLocalizationPath()
{
    $thisFile = __FILE__;
    $pos = strrpos($thisFile, '\\');
    if ($pos == false)
        $pos = strrpos($thisFile, '/');
    return substr($thisFile, 0, $pos+1) . "../../../text/";
}
?>
