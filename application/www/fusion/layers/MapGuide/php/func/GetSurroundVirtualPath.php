<?php
function GetSurroundVirtualPath()
{
    if (isset($_SERVER["REQUEST_URI"]))
    {
        $path = $_SERVER["REQUEST_URI"];
        $baseuri = substr($path, 0, strrpos($path, '?') + 1);
    }
    else
    {
        $baseuri = $_SERVER["PHP_SELF"];
    }

    return substr($baseuri, 0, strrpos($baseuri, '/') + 1);
}
?>
