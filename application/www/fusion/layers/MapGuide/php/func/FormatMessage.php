<?php
function FormatMessage($id, $locale, $params)
{
    $text = GetLocalizedString($id, $locale);
    for($i = 0; $i < count($params); $i++)
    {
        $tag = "{" . $i . "}";
        $pos1 = strpos($text, $tag);
        if($pos1 == false)
            break;
        if($pos1 > 0)
            $begin = substr($text, 0, $pos1);
        else
            $begin = "";
        $end = substr($text, $pos1 + strlen($tag));
        $text = $begin . $params[$i] . $end;
    }
    return $text;
}
?>
