<?php
/**********************************************************************************
* Copyright 2004 BIT, Ltd. http://limb-project.com, mailto: support@limb-project.com
*
* Released under the LGPL license (http://www.gnu.org/copyleft/lesser.html)
***********************************************************************************
*
* $Id: index.php 4382 2006-10-27 10:06:58Z pachanga $
*
***********************************************************************************/
//phpinfo();
//   ini_set('display_errors',1);
//   error_reporting(E_ALL);

require_once(dirname(__FILE__) . '/../setup.php');

require_once('wgs/toolkit/wgsTools.class.php');
require_once('wgs/toolkit/mgTools.class.php');
require_once('wgs/wgsWebApplication.class.php');
require_once('wgs/lib/wgsErrorHandler.class.php');

lmbToolkit :: merge(new wgsTools());
lmbToolkit :: merge(new mgTools());

/*
 * We need lmb_win1251_to_utf8 for data received with PHP DBAL drivers
 * We can use data received from MG (NO_DRIVERS mode) directly
 * 
 * encode uses appropriate method for converting strings to utf8
 */ 

lmb_require('limb/i18n/utf8.inc.php');
function encode($str){
	return WGS4_NO_DRIVERS_MODE ? $str : lmb_win1251_to_utf8 ( $str );
}

function decode($str){
	return WGS4_NO_DRIVERS_MODE ?  $str  :  lmb_utf8_to_win1251( $str ) ;
}		

$errorHandler = new wgsErrorHandler(); 

$application = new wgsWebApplication();
$application->process();





?>
