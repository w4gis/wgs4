  require(["dijit/layout/ContentPane",
  "dijit/registry",
  "dojo/parser",
  "dojo/domReady!"], function(ContentPane, registry, parser){


    var includes = ["problems", "map", "objectadmin", "projectadmin"];

    var navigation = document.getElementById("navigation");
    var content = document.getElementById("content-manual");

    includes.forEach(function(module, i) {
        //create div for section from "includes"
        var sectionContainer = document.createElement("DIV");
        sectionContainer.setAttribute("id", module+"-container");
        content.appendChild(sectionContainer);

        //create LI in navigation for section from "includes"
        var navItem = document.createElement('LI');
        navItem.classList.add("content-navigation__item");
        navigation.appendChild(navItem);

        //appent content of section from includes to it div
        new ContentPane({
          href: "inc/"+module+".html"
        }, module+"-container").startup();

        //once content is loaded, fill navigarion LI
        registry.byId(module+"-container").set("onDownloadEnd", function(){
              // V 1.0
              var articles = this.domNode.getElementsByClassName("navigation-anchor-sub");

              var navSectionLink = document.createElement('A');
              navSectionLink.innerHTML = (i+1)+" "+(document.getElementById(module).getAttribute("data-title") || module);
              navSectionLink.href = "#"+module;
              navSectionLink.classList.add("content-navigation__link");

              navItem.appendChild(navSectionLink);

              var navSection = document.createElement('UL');

              for (var j=0; j<articles.length; j++) {
                  if (!articles[j].getAttribute('id')) continue;
                  var navSectionItem = document.createElement('LI');
                  navSectionItem.classList.add("content-navigation__item");
                  var navLink = document.createElement('A');
                  navLink.innerHTML = (i+1)+"."+(j+1)+" "+(articles[j].getAttribute('data-title') || articles[j].getAttribute('id'));
                  navLink.href = "#"+articles[j].getAttribute('id');
                  navLink.classList.add("content-navigation__link");

                  navSectionItem.appendChild(navLink);
                  navSection.appendChild(navSectionItem);
              }

              navItem.appendChild(navSection);
            });

    });

  });
