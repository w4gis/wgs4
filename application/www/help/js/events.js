require([
"dojo/domReady!"], function(){

  // var hash = window.location.href.substring(window.location.href.indexOf("#")+1);

  var headerHeight = document.getElementById("header").clientHeight || 0;
  var footerHeight = document.getElementById("footer").clientHeight || 0;
  var manualNavigation = document.getElementById("manual-navigation");

  var windowHeight = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight; //screen heigth
  manualNavigation.style.height = (windowHeight - headerHeight)+"px";


  window.onresize = function() {
    headerHeight = document.getElementById("header").clientHeight || 0;
    footerHeight = document.getElementById("footer").clientHeight || 0;

    windowHeight = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight; //screen heigth
    manualNavigation.style.height = (windowHeight - headerHeight)+"px";
    window.dispatchEvent(new Event('scroll'));
  }


  window.onscroll = function() {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    //Смещение footer может меняться, будем всега высчитывать
    var footerYOffset = document.getElementById("footer").pageYOffset || document.getElementById("footer").offsetTop || document.getElementById("footer").scrollTop || 0;
    //console.log(footerYOffset - windowHeight, scrolled);
    if (headerHeight && (scrolled >= headerHeight)) {
      manualNavigation.style.height = "";
      manualNavigation.classList.add("content-navigation-top");
    } else {
      manualNavigation.style.height = (windowHeight - (headerHeight - scrolled))+"px";
      manualNavigation.classList.remove("content-navigation-top");
    }

    if (footerHeight && footerYOffset && (scrolled >= (footerYOffset - windowHeight))) {
      //manualNavigation.style.height = (windowHeight - footerHeight-2)+"px";
      manualNavigation.style.height = (footerYOffset-scrolled)+"px";
    //  console.log("пора", footerYOffset);
    }

  }

});
