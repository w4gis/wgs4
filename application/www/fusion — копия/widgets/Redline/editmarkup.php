<?php
    $fusionMGpath = '../../layers/MapGuide/php/';
    require_once $fusionMGpath . 'Common.php';
    if(InitializationErrorOccurred())
    {
        DisplayInitializationErrorHTML();
        exit;
    }
    require_once $fusionMGpath . 'Utilities.php';
    require_once $fusionMGpath . 'JSON.php';
    require_once 'classes/markupeditor.php';
    require_once 'classes/editcommand.php';

    $args = ($_SERVER['REQUEST_METHOD'] == "POST") ? $_POST : $_GET;

    $refreshMap = false;
    $errorMsg = null;
    $errorDetail = null;
    $allowPoint = false;
    $allowLine = false;
    $allowPoly = false;
    $markupFeatures = array();
    
    $defaultFormat = null;
    $defaultGeomType = null;
    $checkState = "";

    if (array_key_exists("REDLINEFORMAT", $args) && array_key_exists("REDLINEGEOMTYPE", $args)) {
        if (strcmp($args["REDLINEFORMAT"], "SDF") == 0) {
            $defaultFormat = $args["REDLINEFORMAT"];
            $defaultGeomType = $args["REDLINEGEOMTYPE"];
        } else if (strcmp($args["REDLINEFORMAT"], "SHP") == 0) {
            $defaultFormat = $args["REDLINEFORMAT"];
            $defaultGeomType = $args["REDLINEGEOMTYPE"];
        } else if (strcmp($args["REDLINEFORMAT"], "SQLite") == 0) {
            $defaultFormat = $args["REDLINEFORMAT"];
            $defaultGeomType = $args["REDLINEGEOMTYPE"];
        }
    }

   // SetLocalizedFilesPath(GetLocalizationPath());
   SetTrueLocalizedPath(); //see layers/.../common.php
    if(isset($_REQUEST['LOCALE'])) {
        $locale = $_REQUEST['LOCALE'];
    } else {
        $locale = GetDefaultLocale();
    }

    try
    {
        $markupEditor = new MarkupEditor($args);
        if (array_key_exists('EDITCOMMAND', $args))
        {
            $cmd = $args['EDITCOMMAND'];
            switch ($cmd) {
            case EditCommand::AddPoint:
                $markupEditor->AddPoint();
                $refreshMap = true;
                break;
            case EditCommand::AddLine:
            case EditCommand::AddLineString:
                $markupEditor->AddLineString();
                $refreshMap = true;
                break;
            case EditCommand::AddCircle:
            case EditCommand::AddRectangle:
            case EditCommand::AddPolygon:
                $markupEditor->AddPolygon();
                $refreshMap = true;
                break;
            case EditCommand::Delete:
                $markupEditor->DeleteMarkup();
                $refreshMap = true;
                break;
            case EditCommand::Update:
                $markupEditor->UpdateMarkup();
                $refreshMap = true;
                break;

            }
        }

        $markupFeatures = $markupEditor->GetMarkupFeatures();
        $clsDef = $markupEditor->GetClassDefinition();
        $clsProps = $clsDef->GetProperties();
        if ($clsProps->IndexOf($clsDef->GetDefaultGeometryPropertyName()) >= 0)
        {
            $geomProp = $clsProps->GetItem($clsDef->GetDefaultGeometryPropertyName());
            $geomTypes = $geomProp->GetGeometryTypes();

            if ($geomTypes & MgFeatureGeometricType::Point)
                $allowPoint = true;
            if ($geomTypes & MgFeatureGeometricType::Curve)
                $allowLine = true;
            if ($geomTypes & MgFeatureGeometricType::Surface)
                $allowPoly = true;
        }

        $editLocal = GetLocalizedString('REDLINEEDIT', $locale );
        $defaultHelpLocal = GetLocalizedString('REDLINEEDITDEFAULTHELP', $locale );
        $pointHelpLocal = GetLocalizedString('REDLINEEDITPOINTHELP', $locale );
        $lineHelpLocal = GetLocalizedString('REDLINEEDITLINEHELP', $locale );
        $lineStringHelpLocal = GetLocalizedString('REDLINEEDITLINESTRINGHELP', $locale );
        $rectangleHelpLocal = GetLocalizedString('REDLINEEDITRECTANGLEHELP', $locale );
        $polygonHelpLocal = GetLocalizedString('REDLINEEDITPOLYGONHELP', $locale );
        $circleHelpLocal = GetLocalizedString('REDLINEEDITCIRCLEHELP', $locale );
        $addLocal = GetLocalizedString('REDLINEADD', $locale );
        $digitizeLocal = GetLocalizedString('REDLINEDIGITIZE', $locale );
        $pointLocal = GetLocalizedString('REDLINEOBJECTPOINT', $locale );
        $circleLocal = GetLocalizedString('REDLINEOBJECTCIRCLE', $locale );
        $lineLocal = GetLocalizedString('REDLINEOBJECTLINE', $locale );
        $lineStringLocal = GetLocalizedString('REDLINEOBJECTLINESTRING', $locale );
        $rectangleLocal = GetLocalizedString('REDLINEOBJECTRECTANGLE', $locale );
        $polygonLocal = GetLocalizedString('REDLINEOBJECTPOLYGON', $locale );
        $modifyLocal = GetLocalizedString('REDLINEMODIFY', $locale );
        $selectLocal = GetLocalizedString('REDLINESELECTOBJECT', $locale );
        $deleteLocal = GetLocalizedString('REDLINEDELETEOBJECT', $locale );
        $updateLocal = GetLocalizedString('REDLINEUPDATETEXT', $locale );
        $closeLocal = GetLocalizedString('REDLINEEDITCLOSE', $locale );
        $promptLabelLocal = GetLocalizedString('REDLINEPROMPTLABEL', $locale);
        $promptRedlineLabelsLocal = GetLocalizedString('REDLINEPROMPTFORLABELS', $locale);
        $noTextLocal = GetLocalizedString('REDLINENOTEXT', $locale);
        $multiHelpLocal = GetLocalizedString('REDLINEMULTISELECTHELP', $locale);
        
        if (array_key_exists("REDLINEPROMPT", $args) && strcmp($args["REDLINEPROMPT"], "on") == 0) {
            $checkState = " checked='checked'";
        }
    }
    catch (MgException $e)
    {
        $errorMsg = $e->GetExceptionMessage();
        $errorDetail = $e->GetDetails();
    }
    catch (Exception $e)
    {
        $errorMsg = $e->getMessage();
        $errorDetail = $e->__toString();
    }
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
<?php if ($errorMsg == null) { ?>
    <title><?=$editLocal?></title>
    <link rel="stylesheet" href="Redline.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="/wgs4/js/wgs-release/dojox/mobile/themes/ios7/ios7.css"></link>
    <link rel="stylesheet" href="/wgs4/css/redline.css" type="text/css">
    <link href="/wgs4/icons/font-awesome-4.0.3/css/font-awesome.css" rel="stylesheet"/>
    <script language="javascript" src="../../layers/MapGuide/MapGuideViewerApi.js"></script>
    <script language="javascript" src="../../common/browserdetect.js"></script>
    <script language="javascript">
        var session = '<?= $args['SESSION'] ?>';
        var mapName = '<?= $args['MAPNAME'] ?>';

        var CMD_ADD_POINT		= <?= EditCommand::AddPoint ?>;
        var CMD_ADD_LINE 		= <?= EditCommand::AddLine ?>;
        var CMD_ADD_LINESTRING 	= <?= EditCommand::AddLineString ?>;
        var CMD_ADD_RECTANGLE 	= <?= EditCommand::AddRectangle ?>;
        var CMD_ADD_POLYGON 	= <?= EditCommand::AddPolygon ?>;
        var CMD_ADD_CIRCLE      = <?= EditCommand::AddCircle ?>;
        var CMD_DELETE 			= <?= EditCommand::Delete ?>;
        var CMD_UPDATE 			= <?= EditCommand::Update ?>;

        var EDIT_DEFAULT_HELP = "<?=$defaultHelpLocal?>";
        var EDIT_POINT_HELP = "<?=$pointHelpLocal?>";
        var EDIT_LINE_HELP = "<?=$lineHelpLocal?>";
        var EDIT_LINESTRING_HELP = "<?=$lineStringHelpLocal?>";
        var EDIT_RECTANGLE_HELP = "<?=$rectangleHelpLocal?>";
        var EDIT_POLYGON_HELP = "<?=$polygonHelpLocal?>";
        var EDIT_CIRCLE_HELP = "<?=$circleHelpLocal?>";
        
        var CURRENT_OBJ_COORDS = 1;

        function SetDigitizeInfo(text)
        {
            //var digitizeInfo = document.getElementById("digitizeInfo");
            //digitizeInfo.innerHTML = text;
            var widget = Fusion.getWidgetsByType("Redline")[0];
            if (widget.mapMessagePrompt) {
                var map = Fusion.getMapByName(mapName).mapWidget;
                var msg = map.message;
                //It's a digitization prompt
                if (text != EDIT_DEFAULT_HELP) {
                    msg.info(text + ' <a id="abortDigitizationLink" href="javascript:void(0)">' + OpenLayers.i18n("stop") + '</a>');
                    var link = msg.container.ownerDocument.getElementById("abortDigitizationLink");
                    //Wire the anchor click
                    link.onclick = function() {
                        msg.clear();
                        PromptAndSetMarkupText();
                    };
                } else {
                    msg.info(text);
                }
            }
        }

        function SubmitCommand(cmd)
        {
            var commandInput = document.getElementById("commandInput");
            commandInput.value = cmd;

            var editForm = document.getElementById("editForm");
            editForm.submit();
        }

        function AddPoint()
        {
            SetDigitizeInfo(EDIT_POINT_HELP);
            DigitizePoint(OnPointDigitized);
        }
        
        function AddCircle()
        {
            SetDigitizeInfo(EDIT_CIRCLE_HELP);
            DigitizeCircle(OnCircleDigitized);
        }

        function AddLine()
        {
            SetDigitizeInfo(EDIT_LINE_HELP);
            DigitizeLine(OnLineStringDigitized);
        }

        function AddLineString()
        {
            SetDigitizeInfo(EDIT_LINESTRING_HELP);
            DigitizeLineString(OnLineStringDigitized);
        }

        function AddRectangle()
        {
            SetDigitizeInfo(EDIT_RECTANGLE_HELP);
            DigitizeRectangle(OnRectangleDigitized);
        }

        function AddPolygon()
        {
            SetDigitizeInfo(EDIT_POLYGON_HELP);
            DigitizePolygon(OnPolyonDigitized);
        }
        
        function PromptForRedlineLabels()
        {
            return document.getElementById("chkPromptForRedlineLabels").checked;
        }

        function PromptAndSetMarkupText()
        {
            var widget = Fusion.getWidgetsByType("Redline")[0];
            if (PromptForRedlineLabels()) {
                var textInput = document.getElementById("textInput");

                textLabel = window.prompt("<?=$promptLabelLocal?>", "");
                textInput.value = (textLabel != null) ? textLabel : "";
            }
            ClearDigitization(true);
        }

        function OnPointDigitized(point)
        {
            PromptAndSetMarkupText();

            var geometryInput = document.getElementById("geometryInput");
            geometryInput.value = point.X + "," + point.Y;

            SubmitCommand(CMD_ADD_POINT);
        }
        
        function OnCircleDigitized(circle)
        {
            PromptAndSetMarkupText();
            var geometryInput = document.getElementById("geometryInput");
            var x = circle.Center.X;
            var y = circle.Center.Y;
            var r = circle.Radius;
            
            var coords = [];
            for (var index = 0; index < 2 * simulateCircleHalfPointNumber + 1; index++) {
                coords.push(x + r * simulateCirclePoints[2 * index]);
                coords.push(y + r * simulateCirclePoints[2 * index + 1]);
            }
            geometryInput.value = (coords.length / 2) + "," + coords.join(",");
            SubmitCommand(CMD_ADD_CIRCLE);
        }

        function OnLineStringDigitized(lineString)
        {
            PromptAndSetMarkupText();

            var geomText = lineString.Count;
            for (var i = 0; i < lineString.Count; i++)
            {
                geomText += "," + lineString.Point(i).X + "," + lineString.Point(i).Y;
            }

            var geometryInput = document.getElementById("geometryInput");
            geometryInput.value = geomText;

            SubmitCommand(CMD_ADD_LINESTRING);
        }

        function OnRectangleDigitized(rectangle)
        {
            PromptAndSetMarkupText();

            var geometryInput = document.getElementById("geometryInput");
            geometryInput.value = "5,"
                + rectangle.Point1.X + "," + rectangle.Point1.Y + ","
                + rectangle.Point2.X + "," + rectangle.Point1.Y + ","
                + rectangle.Point2.X + "," + rectangle.Point2.Y + ","
                + rectangle.Point1.X + "," + rectangle.Point2.Y + ","
                + rectangle.Point1.X + "," + rectangle.Point1.Y;

            SubmitCommand(CMD_ADD_RECTANGLE);
        }

        function OnPolyonDigitized(polygon)
        {
            if(polygon.Count < 3)
            {
                // invalid polygon
                ClearDigitization(true);

                return;
            }

            PromptAndSetMarkupText();

            var geomText = polygon.Count;
            for (var i = 0; i < polygon.Count; i++)
            {
                geomText += "," + polygon.Point(i).X + "," + polygon.Point(i).Y;
            }

            var geometryInput = document.getElementById("geometryInput");
            geometryInput.value = geomText;

            SubmitCommand(CMD_ADD_POLYGON);
        }

        function TrimString(str)
        {
            return (typeof String.prototype.trim == 'undefined') ? str.replace(/^\s+|\s+$/g, '') : str.trim();
        }

        function GetSelectedMarkupIds(mkEl)
        {
            /*var ids = [];
            for (var i = 0; i < mkEl.options.length; i++) {
                if (mkEl.options[i].selected) {
                    ids.push(mkEl.options[i].value);
                }
            }*/
           // return ids;
           var res = [];
           res.push(registry.byId('markupFeatures').get('value'))
           return res
        }

        function SelectMarkup()
        {
            markupFeatures = document.getElementById("markupFeatures");
            var featIds = GetSelectedMarkupIds(markupFeatures);
            reqParams = "MAPNAME=" + encodeURIComponent(mapName);
            reqParams += "&SESSION=" + encodeURIComponent(session);
            reqParams += "&OPENMARKUP=" + encodeURIComponent('<?= $args['OPENMARKUP']; ?>');
            //reqParams += "&MARKUPFEATURE=" + markupFeatures.value;
            for (var i = 0; i < featIds.length; i++) {
                reqParams += "&MARKUPFEATURE[]=" + featIds[i];
            }

            if(msie)
                reqHandler = new ActiveXObject("Microsoft.XMLHTTP");
            else
                reqHandler = new XMLHttpRequest();

            reqHandler.open("POST", "getselectionxml.php", false);
            reqHandler.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            reqHandler.send(reqParams);
            if(reqHandler.responseXML)
            {
                SetSelectionXML(reqHandler.responseText, false /*bDoNotZoom*/);
                
                var def = getCordsBySelection(encodeURIComponent(mapName), encodeURIComponent(session), reqHandler.responseText);
                def.then( function(){
					putCoordsToView();
				});
            }
        }
		
		function putCoordsToView() {
			var table = document.getElementById("redline-coords");
			//Очищаем таблицу перед тем, как отобразить координаты нового объекта
            var rowCount = table.rows.length;
               if (rowCount > 2) {
				for (var i = rowCount-1; i>1; i--) {
					table.deleteRow(i);
					rowCount--;
				}
			}
			console.log(CURRENT_OBJ_COORDS)
			for (var j in CURRENT_OBJ_COORDS)
			for (var i=0; i < CURRENT_OBJ_COORDS[j]["coords"].pm.length; i++) {
					console.log("index = " + i);
					var row = table.insertRow(rowCount);
 
					var cell1 = row.insertCell(0);
					var element1 = document.createElement("input");
					element1.type = "text";
					element1.value = CURRENT_OBJ_COORDS[j]["coords"].pm[i].y;
					cell1.appendChild(element1);
					
					var cell2 = row.insertCell(0);
					var element2 = document.createElement("input");
					element2.type = "text";
					element2.value = CURRENT_OBJ_COORDS[j]["coords"].pm[i].x;
					cell2.appendChild(element2);
			
           } 
			
		}
		
		function getCordsBySelection(mapName, sessionId, selText) {
			var deferredResult = new Deferred();
			require(['dojo/request/xhr'], function(xhr){
				var onSuccess = function(result){
					//self._featureInfo = result
					//self.renderFeatureInfo()
					CURRENT_OBJ_COORDS = result
					//console.log(CURRENT_OBJ_COORDS[1]["coords"].pm[0].x)
					deferredResult.resolve();
				}
				var onError = function(error){
					console.log(error)
				}
			
				if(mapName){									
					xhr.post("/map/geometry/GetSelectedFeatures", {
						handleAs: "json",
						data:{'mapname': mapName, 'session': sessionId, 'isXml':1, 'selection': selText, version: 4, isshape : 1}
					}).then(onSuccess, onError);			
				}				
			})
			return deferredResult;
		}
		
		
		
        function DeleteMarkup()
        {
            SubmitCommand(CMD_DELETE);
        }

        function UpdateMarkup()
        {
			require(['dojo/query'], function( query ) {
				var coordInputs = query( '#redline-coords input[type="text"]' ), coordString = '';
				for(var i=0; i<coordInputs.length; i++)
					coordString += coordInputs[i].value + ',';
				
				coordString = coordString.substr( 0, coordString.length-2 );
				console.log(coordString);
				
				document.getElementById('geometryInput').value = coordString;
				SubmitCommand(CMD_UPDATE);
			})            
        }

        function CloseEditor()
        {
            ClearDigitization(true);
            var map = Fusion.getMapByName(mapName).mapWidget;
            map.message.clear();

            var editForm = document.getElementById("editForm");
            editForm.action = "markupmain.php";

            editForm.submit();
        }

        function OnMarkupFeatureChange()
        {
            var markupFeatures = registry.byId("markupFeatures");
            var value = markupFeatures.get('value')
            console.log(value);
            //var selectBtn = document.getElementById("selectBtn");
            var deleteBtn = document.getElementById("deleteBtn");
            var updateBtn = document.getElementById("updateBtn");

            if(value){
				//selectBtn.disabled = false;
                deleteBtn.disabled = false;
                updateBtn.disabled = false;
			}else {
				//updateTextInput.value = '';
                //selectBtn.disabled = true;
                deleteBtn.disabled = true;
                updateBtn.disabled = true;
			}
            //var markupFeatures = document.getElementById("markupFeatures");
            /*var updateTextInput = document.getElementById("updateTextInput");
            
            if (markupFeatures.selectedIndex >= 0)
            {
                value = markupFeatures.options[markupFeatures.selectedIndex].text;
                if (value.indexOf('[<?= $noTextLocal ?>]') < 0) {
                    var tokens = value.split(":");
                    if (tokens.length == 2) {
                        updateTextInput.value = TrimString(tokens[1]);
                    } else {
                        updateTextInput.value = value;
                    }
                } else {
                    updateTextInput.value = '';
                }

                selectBtn.disabled = false;
                deleteBtn.disabled = false;
                updateBtn.disabled = false;
            }
            else
            {
                updateTextInput.value = '';
                selectBtn.disabled = true;
                deleteBtn.disabled = true;
                updateBtn.disabled = true;
            }*/
        }

        function OnLoad()
        {
            OnMarkupFeatureChange();

        <?php if ($refreshMap) { ?>
            var map = parent.Fusion.getMapByName(mapName);
            map.drawMap();
        <?php } ?>
            SetDigitizeInfo(EDIT_DEFAULT_HELP);
        }
        
        function OnUnload()
        {
            ClearDigitization(true);
            var map = Fusion.getMapByName(mapName).mapWidget;
            map.message.clear();
        }
    </script>
    
    <script src="/wgs4/js/etc/config/dojo-config.js"></script> 	
	<script src="/wgs4/js/dojo/dojo.js"></script> 
	<!-- /WGS4-->
	<script>
		require([
			'dojox/mobile/parser',
			'dijit/registry','dojo/on', 'dojo/Deferred', 'dojo/dom-class', 'dojo/dom-style',"dijit/form/Select" ,
			'dojox/mobile/CheckBox',
			'dojox/mobile/Switch',
			'dojox/mobile/compat',
			"dojox/mobile/EdgeToEdgeList",
			"dojox/mobile/ListItem", 
			"dojox/mobile/EdgeToEdgeCategory",
			'dojox/mobile/Button',
			'dojox/mobile/Accordion',
			'dojox/mobile/ContentPane',
			'dojox/mobile/ScrollableView','dojox/mobile/TextBox',
			'dojo/domReady'],function(parser, registry, on, Deferred, domClass, domStyle){
			window.Deferred = Deferred
			window.registry = registry
			window.dojoregistry = registry
			parser.parse()
			OnLoad();			
			window.parent.AppData.taskPaneTitle.set('label','<?= $editLocal ?> - <?= $markupEditor->GetMarkupName() ?>')
			window.parent.AppData.taskPaneBackButton.set('label','Назад')
			var w = window
			
			//window.parent.AppData.taskPanePreviousBack = window.parent.AppData.taskPaneBackButton.get('onClick');
			window.parent.AppData.taskPaneBackButton.set('onClick', function(){
				CloseEditor.call(w);
				domClass.remove(window.parent.AppData.taskPaneBackButton.domNode, 'mblToolBarButtonSelected')
				return false
			})
			
			setTimeout(function(){
				domStyle.set('redlineLoading', 'display', 'none')
				domStyle.set('redlineBody', 'display', 'block')
			}, 250)
		})
	</script>
	<style>
		#redlineBody{
			display:none;
		}
	</style>
	<link rel="stylesheet" href="/wgs4/js/wgs-release/dijit/themes/claro/claro.css" /> 

</head>

<body class="claro" onLoad="OnLoad()" onUnload="OnUnload()" >
<img src="/wgs4/js/dijit/themes/claro/images/loadingAnimation.gif"/ id='redlineLoading'/>
<div data-dojo-type="dojox/mobile/ScrollableView" id="redlineBody">

<form action="editmarkup.php" method="post" enctype="application/x-www-form-urlencoded" id="editForm" target="_self">
	
<!--<div class="redline-title"><?= $editLocal ?> - <?= $markupEditor->GetMarkupName() ?></div>-->
<!-- <div class="redline-subtitle"><?= $addLocal ?></div> -->
<div data-dojo-type="dojox/mobile/RoundRectCategory" class="redline-label"><?= $addLocal ?></div>
<div class="redline-actions">
	<p class="redline-ckeckbox" ><input data-dojo-type="dojox/mobile/CheckBox" type="checkbox" id="chkPromptForRedlineLabels" name="REDLINEPROMPT" <?= $checkState ?>/><label for="chkPromptForRedlineLabels"></label> <span><?= $promptRedlineLabelsLocal ?> </span></p>
	<button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="pointBtn" onClick="AddPoint(); return false" <?= $allowPoint ? '' : 'disabled="disabled"' ?> <?= $allowPoint ? '' : 'style="display:none"' ?> ><i class="fa fa-plus"></i> <?=$pointLocal?></button>
    <button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="circleBtn" onClick="AddCircle(); return false"  <?= $allowPoly ? '' : 'disabled="disabled"' ?> <?= $allowPoly ? '' : 'style="display:none"' ?>  ><i class="fa fa-plus"></i> <?=$circleLocal?>"</button>
	
	<button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="lineBtn" onClick="AddLine(); return false"  <?= $allowLine ? '' : 'disabled="disabled"'  ?> <?= $allowLine ? '' : 'style="display:none"' ?>> <i class="fa fa-plus"></i> <?=$lineLocal?></button>
    <button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="lineStringBtn" onClick="AddLineString(); return false" <?= $allowLine ? '' : 'disabled="disabled"'  ?> <?= $allowLine ? '' : 'style="display:none"' ?>> <i class="fa fa-plus"></i> <?=$lineStringLocal?></button>
	<button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="rectangleBtn" onClick="AddRectangle(); return false" <?= $allowPoly ? '' : 'disabled="disabled"'  ?> <?= $allowPoly ? '' : 'style="display:none"' ?>> <i class="fa fa-plus"></i> <?=$rectangleLocal?></button>
    <button data-dojo-type="dojox/mobile/Button" class="redline-button mblBlueButton" id="polygonBtn" onClick="AddPolygon(); return false" <?= $allowPoly ? '' : 'disabled="disabled"'  ?> <?= $allowPoly ? '' : 'style="display:none"' ?>> <i class="fa fa-plus"></i> <?=$polygonLocal?></button>
</div>
	
<div data-dojo-type="dojox/mobile/RoundRectCategory" class="redline-label"><?=$modifyLocal?></div>
<div class="redline-container">
	<select data-dojo-type="dijit/form/Select" name="MARKUPFEATURE[]" class="Ctrl" id="markupFeatures" onChange="OnMarkupFeatureChange(); SelectMarkup();"  style="width: 100%">
		<?php
			$selected = 'selected';
			foreach($markupFeatures as $markupId => $markupText) {
		?>
				<option value="<?= $markupId ?>" <?=$selected ?> ><?= (strlen($markupText) > 0) ? "$markupId: ".htmlentities($markupText, ENT_COMPAT, 'UTF-8') : "$markupId: [$noTextLocal]" ?></option>
		<?php
				$selected = '';
			}
		?>
	</select>
    <input class="Ctrl" name="UPDATETEXT" type="text" id="updateTextInput" maxlength="255" style="width:100%"/>
	
	<table id="redline-coords">
		<tr>
			<th colspan="2">Координаты</th>
		</tr>
		<tr>
			<th>X</th>
			<th>Y</th>
		</tr>
	</table>
    <input class="Ctrl" id="updateBtn" type="button" onClick="UpdateMarkup()" value="<?=$updateLocal?>" style="width:85px">
    
    <!--<input class="Ctrl" id="selectBtn" type="button" onClick="SelectMarkup()" value="<?=$selectLocal?>" style="width:85px">-->
    <input class="Ctrl" id="deleteBtn" type="button" onClick="DeleteMarkup()" value="<?=$deleteLocal?>" style="width:85px">
        
    <input class="Ctrl" name="" type="button" onClick="CloseEditor()" value="<?=$closeLocal?>" style="width:85px"/>
</div>

<input name="SESSION" type="hidden" value="<?= $args['SESSION'] ?>">
<input name="MAPNAME" type="hidden" value="<?= $args['MAPNAME'] ?>">
<input name="OPENMARKUP" type="hidden" value="<?= $args['OPENMARKUP'] ?>">
<input name="EDITCOMMAND" type="hidden" value="" id="commandInput">
<input name="GEOMETRY" type="hidden" value="" id="geometryInput">
<input name="TEXT" type="hidden" value="" id="textInput">
<?php if ($defaultFormat != null && $defaultGeomType != null) { ?>
<input name="REDLINEFORMAT" type="hidden" value="<?= $defaultFormat ?>" />
<input name="REDLINEGEOMTYPE" type="hidden" value="<?= $defaultGeomType ?>" />
<?php } ?>
<input name="REDLINESTYLIZATION" type="hidden" value="<?= $args['REDLINESTYLIZATION'] ?>">
</form>
<?php } else { ?>
</head>
<body>
<table class="RegText" border="0" cellspacing="0" width="100%%">
    <tr><td class="Title">Error<hr></td></tr>
    <tr><td><?= $errorMsg ?></td></tr>
    <tr><td><?= $errorDetail ?></td></tr>
</table>

<?php } ?>

</body>

</html>
