<?
Class Document
{
	function createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult)
	{
		if (!file_exists($templatePath)) {
            die('File template not found.');
        }
        copy($templatePath, $FileResult);
        
        $zip = new ZipArchive();
        if (!$zip->open($FileResult)) {
            die('File template can not open.');
        }
        $documentXml = $zip->getFromName('word/document.xml');
				
		for ($i = 0; $i < count($templateArray); $i++) {
            $reg = ''.substr($templateArray[$i], 0, 1);
            for ($j = 1; $j < strlen($templateArray[$i]); $j++) 
                $reg .= '(<.*?>)*+' . substr($templateArray[$i], $j, 1);  
            $reg = '#' . str_replace(array('#', '{', '[', ']', '}'), array('#', '{', '[', ']', '}'), $reg) . '#';
            $documentXml = preg_replace($reg, $valueArray[$i], $documentXml);
        }
				
        $zip->deleteName('word/document.xml');
        $zip->addFromString('word/document.xml', $documentXml);
        $zip->close();
	}	
}

/* example */

$templatePath=$_POST[templatePath];
$templateArray=json_decode($_POST[templateArray]);
$valueArray=json_decode($_POST[valueArray]);
$fileResult=$_POST[fileResult];


$obj = new Document;
$obj-> createDocumentFromTemplate($templatePath,$templateArray,$valueArray,$fileResult);


?>
