<?
Class Document
{
	/*
	Params:
	$templatePath - Path to the template file
	$templateArray - Array of template in file $templatePath
	$valueArray - Array value of templates $templateArray 
	$FileResult - Path to file result
	*/
	function createDocumentFromTemplate($templatePath, $templateArray, $valueArray, $FileResult)
	{
		if (!file_exists($templatePath)) {
            die('File template not found.');
        }
        copy($templatePath, $FileResult);
        
        $zip = new ZipArchive();
        if (!$zip->open($FileResult)) {
            die('File template can not open.');
        }
        $documentXml = $zip->getFromName('word/document.xml');
				
		for ($i = 0; $i < count($templateArray); $i++) {
            $reg = ''.substr($templateArray[$i], 0, 1);
            for ($j = 1; $j < strlen($templateArray[$i]); $j++) 
                $reg .= '(<.*?>)*+' . substr($templateArray[$i], $j, 1);  
            $reg = '#' . str_replace(array('#', '{', '[', ']', '}'), array('#', '{', '[', ']', '}'), $reg) . '#';
            $documentXml = preg_replace($reg, $valueArray[$i], $documentXml);
        }
				
        $zip->deleteName('word/document.xml');
        $zip->addFromString('word/document.xml', $documentXml);
        $zip->close();
	}	
}

/* example */
$templatePath='templates/report.docx';
$templateArray=array('%number%', '%name%', '%age%','%work_place%');
$valueArray=array('123', 'Екатерина', '19','ТУСУР, каф.АОИ');
$FileResult='result.docx';



$obj = new Document;
$obj-> createDocumentFromTemplate($templatePath,$templateArray,$valueArray,$FileResult);

echo "For open report click => <a href='$FileResult'/>$FileResult<a/>";

?>
