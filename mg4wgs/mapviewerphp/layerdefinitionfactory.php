<?php

//
//  Copyright (C) 2004-2010 by Autodesk, Inc.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of version 2.1 of the GNU Lesser
//  General Public License as published by the Free Software Foundation.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

class LayerDefinitionFactory
{
    //Creates Area Rule
    //Parameters:
    //$foreGroundColor - color code for the foreground color
    //$legendLabel - string for the legend label
    //$filterText - filter string
    //$textSymbol - use textsymbol.templ to create it
    static function CreateAreaRule($legendLabel, $filterText, $foreGroundColor)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$areaRule = file_get_contents("../../viewerfiles/arearule.templ");
    	} else {
    		$areaRule = file_get_contents(VIEWERFILES_DIR."/arearule.templ");
    	}
        $areaRule = sprintf($areaRule, $legendLabel, $filterText, $foreGroundColor);
        return $areaRule;
    }

    //Creates AreaTypeStyle.
    //Parameters:
    //$areaRules - call CreateAreaRule to create area rules
    static function CreateAreaTypeStyle($areaRules)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$style = file_get_contents("../../viewerfiles/areatypestyle.templ");
    	} else {
    		$style = file_get_contents(VIEWERFILES_DIR."/areatypestyle.templ");
    	}
        $style = sprintf($style, $areaRules);
        return $style;
    }

    //Creates line rule
    //Parameters:
    //$color - color code for the line
    //$legendLabel - string for the legend label
    //$filter - filter string
    static function CreateLineRule($legendLabel, $filter, $color)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$lineRule = file_get_contents("../../viewerfiles/linerule.templ");
    	} else {
    		$lineRule = file_get_contents(VIEWERFILES_DIR."/linerule.templ");
    	}
        $lineRule = sprintf($lineRule, $legendLabel, $filter, $color);
        return $lineRule;
    }

    //Creates LineTypeStyle
    //Parameters:
    //$lineRules - call CreateLineRule to create line rules
    static function CreateLineTypeStyle($lineRules)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$lineStyle = file_get_contents("../../viewerfiles/linetypestyle.templ");
    	} else {
    		$lineStyle = file_get_contents(VIEWERFILES_DIR."/linetypestyle.templ");
    	}
        $lineStyle = sprintf($lineStyle, $lineRules);
        return $lineStyle;
    }

    //Creates mark symbol
    //Parameters:
    //$resourceId - resource identifier for the resource to be used
    //$symbolName - the name of the symbol
    //$width - the width of the symbol
    //$height - the height of the symbol
    //$color - color code for the symbol color
    static function CreateMarkSymbol($resourceId, $symbolName, $width, $height, $color)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$markSymbol = file_get_contents("../../viewerfiles/marksymbol.templ");
    	} else {
    		$markSymbol = file_get_contents(VIEWERFILES_DIR."/marksymbol.templ");
    	}
        $markSymbol = sprintf($markSymbol, $width, $height, $resourceId, $symbolName, $color);
        return $markSymbol;
    }

    //Creates text symbol
    //Parameters:
    //$text - string for the text
    //$fontHeight - the height for the font
    //TODO:Can we pass it as a integer (ex. 10) or string (ex"10")
    //$foregroundColor - color code for the foreground color
    static function CreateTextSymbol($text, $fontHeight, $foregroundColor)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$textSymbol = file_get_contents("../../viewerfiles/textsymbol.templ");
    	} else {
    		$textSymbol = file_get_contents(VIEWERFILES_DIR."/textsymbol.templ");
    	}
        $textSymbol = sprintf($textSymbol, $fontHeight, $fontHeight, $text, $foregroundColor);
        return $textSymbol;
    }

    //Creates a point rule
    //Parameters:
    //$pointSym - point symbolization. Use CreateMarkSymbol to create it
    //$legendlabel - string for the legend label
    //$filter - string for the filter
    //$label - use CreateTextSymbol to create it
    static function CreatePointRule($legendLabel, $filter, $label, $pointSym)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$pointRule = file_get_contents("../../viewerfiles/pointrule.templ");
    	} else {
    		$pointRule = file_get_contents(VIEWERFILES_DIR."/pointrule.templ");
    	}
        $pointRule = sprintf($pointRule, $legendLabel, $filter, $label, $pointSym);
        return $pointRule;
    }

    //Creates PointTypeStyle
    //Parameters:
    //$pointRule - use CreatePointRule to define rules
    static function CreatePointTypeStyle($pointRule)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$pointTypeStyle = file_get_contents("../../viewerfiles/pointtypestyle.templ");
    	} else {
    		$pointTypeStyle = file_get_contents(VIEWERFILES_DIR."/pointtypestyle.templ");
    	}
        $pointTypeStyle = sprintf($pointTypeStyle, $pointRule);
        return $pointTypeStyle;
    }

    //Creates ScaleRange
    //Parameterss
    //$minScale - minimum scale
    //$maxScale - maximum scale
    //$typeStyle - use one CreateAreaTypeStyle, CreateLineTypeStyle, or CreatePointTypeStyle
    static function CreateScaleRange($minScale, $maxScale, $typeStyle)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$scaleRange = file_get_contents("../../viewerfiles/scalerange.templ");
    	} else {
    		$scaleRange = file_get_contents(VIEWERFILES_DIR."/scalerange.templ");
    	}
        $scaleRange = sprintf($scaleRange, $minScale, $maxScale, $typeStyle);
        return $scaleRange;
    }

    //Creates a layer definition
    //$resourceId - resource identifier for the new layer
    //featureClass - the name of the feature class
    //$geometry - the name of the geometry
    //$featureClassRange - use CreateScaleRange to define it.
    static function CreateLayerDefinition($resourceId, $featureClass, $geometry, $featureClassRange)
    {
    	if (!defined('VIEWERFILES_DIR')) {
        	$layerDef = file_get_contents("../../viewerfiles/layerdefinition.templ");
    	} else {
    		$layerDef = file_get_contents(VIEWERFILES_DIR."/layerdefinition.templ");
    	}
        $layerDef = sprintf($layerDef, $resourceId, $featureClass, $geometry, $featureClassRange);
        return $layerDef;
    }
}

?>
